#!/usr/bin/env bash
ENV=${ENV:=dev}
BUILD_DIR=build-${ENV}
cp -rf $BUILD_DIR/* .
nginx -g 'daemon off;'
