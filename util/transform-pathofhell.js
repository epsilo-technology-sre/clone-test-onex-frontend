const path = require('path');

const lazPackagePath = path.join(
  process.cwd(),
  './packages/lazada/src',
);

module.exports = function (fileInfo, api, options) {
  const { jscodeshift: j } = api;
  let root = j(fileInfo.source);
  root.find(j.ImportDeclaration).forEach((fpath) => {
    let { source } = fpath.value;
    if (source.value.indexOf('../../../') > -1) {
      let absPath = path.join(
        path.dirname(path.join(process.cwd(), fileInfo.path)),
        source.value,
      );
      const friendlyPath = absPath.replace(
        lazPackagePath,
        '@ep/lazada/src',
      );
      console.info(
        'transform from [%s] to [%s]',
        source.value,
        friendlyPath,
      );
      source.value = friendlyPath;
    }
  });
  return root.toSource();
};
module.exports.parser = 'tsx';
