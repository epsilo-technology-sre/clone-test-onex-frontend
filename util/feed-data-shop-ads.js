const request = require('node-fetch');

async function createAds(token) {
  key = Math.random().toString().slice(2);
  let response = await request(
    'https://staging-onex-api.epsilo.io/api/v1/shopee/shopads/create-shopads',
    {
      method: 'post',
      headers: {
        Authorization: 'Bearer ' + token,
        Accept: '*/*',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        shop_eid: 3820,
        shop_ads_name: `Shop BonMotMotKo ${key}`,
        total_budget: 100,
        daily_budget: 10,
        start_date: '2021-01-01',
        end_date: null,
        shop_category_eid: null,
        tagline: 'Shop BonMotMotKo Can do all',
        keywords: [
          {
            name: 'xin chao bon110 ' + key,
            match_type: 'exact_match',
            bidding_price: 0.5,
          },
          {
            name: 'hello 4110 ' + key,
            match_type: 'broad_match',
            bidding_price: 1.5,
          },
        ],
      }),
    },
  )
    .then((res) => {
      console.info(res.status);
      console.info(res.headers);
      return res.text();
    })
    .then((res) => {
      console.info(res);
      return res;
    });
}

async function run() {
  for (let i = 0; i < 200; i++) {
    console.info('createing shop ads...', i);
    const res = await createAds(
      'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI5MjA3NDEwOS1mMzVlLTQ4YTEtYjdhMy02ODliMWZmYzEyNGEiLCJqdGkiOiI1MWRhN2U2M2ExZDg2MWUwYmYzZWUyMWMxZDRjMDY0ZmI3N2U5MzFhYzAyMzRlZmZmMWQyYTQzNzhlNDM5NWY3ZTA3OWQ3NDA1N2RkYTVkNiIsImlhdCI6MTYwODc5MDMxOSwibmJmIjoxNjA4NzkwMzE5LCJleHAiOjE2MDg3OTM5MTksInN1YiI6Ijk5OCIsInNjb3BlcyI6W119.Gez_odBht9BGHnej-SCqzP99kg6vIj1AP8aCO42T0GDMbi3LJx_b45aVkSvV3Q18zgIvb1INmmEjjBtjFLx_2KqTzvEMy-cqnU-OVWdsgI-JUEH82jxGWjTLukM7irD79EVShqoYaYROnrt5MYtKGB-cx8cU-Bnao6ZLH-ebrhZ2cNH_Brp2-n6C5OfRcupe1dSMEiojhi1H9Y1kjY0Ry-Y7ACkh_nLOo24GAO-ObUmJkQHb7EB7dczG499BcAojHhJp1mhxpUACDGKq02tcknCgcT4Cxc9jn-IEvJMs7ls8BfMw4a0L__i0v2x8swzD9cOjFL5Fakoa_upWmYNtVEHQzbGWwi3AEJ2V-tqoN7Gnpmb-6xkyYiH9AQodbhsUhfZA4ASed_wh3uU-GWKxsNRt_F7mrPAnjRgyTRay7Ps6jlBQa94y8hVyakz-7Cgn2_iN26W8L3U-AvTDYUa5udJXEEveZgSfYXg97Dh5_UCRW5pj4fXQZ1SAl-f-_MzzzGVs5mYWtjnMpWSd1lEHqGgCB12rHjS_f6-W_TZSqxSmb9I7n7LlzJzsFX8Mfr_UMQi8VZLAARKETr3BfctZky7sMnmT2yNMGTM88eYaj3P0X5TMxQTxJr85BI5KZcpEIV0XF0rrh-JXpN0FUVD8GquMHZFNPakqfv2GdAo0dz0',
    );

    console.info(res);
  }
}

run();
