FROM node:12-alpine

WORKDIR /app

# support docker caching
COPY packages/one/package.json packages/one/package.json
COPY packages/lazada/package.json packages/lazada/package.json
COPY packages/shopee/package.json packages/shopee/package.json
COPY packages/toko/package.json packages/toko/package.json
COPY packages/next/package.json packages/next/package.json
COPY package.json package.json
COPY lerna.json lerna.json
COPY yarn.lock yarn.lock
RUN yarn config set network-timeout 600000 -g
RUN yarn install
RUN yarn bootstrap
