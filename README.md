## Requirements:
- [nodejs 12](https://nodejs.org)
- yarn

## Getting started developing:
- Install dependencies: 
  * `yarn install` 
  * `yarn bootstrap`
- Run storybook: `yarn storybook`
- Visit storybook URL you can see the stories in there, including one application

## Folder structure
- This project follow [monorepos](https://en.wikipedia.org/wiki/Monorepo) by using [lerna](https://lerna.js.org/)
  - /packages
    - /lazada: everything related to lazada
    - /one: all in one

## Deployment
- This repo use the bitbucket pipeline for deploying assets on cloudfront.



