const { createProxyMiddleware } = require('http-proxy-middleware');
const apicache = require('apicache');

module.exports = function expressMiddleware(app) {
  console.info('storybook middleware setup...');
  const onlyStatus200 = (req, res) => {
    console.info(req.path);
    return (
      req.path.indexOf('/api/v1/login') === -1 && 
      res.statusCode === 200
    );
  };
  app.use(
    '/staging-passportx.epsilo.io',
    createProxyMiddleware({
      target: `https://staging-passportx.epsilo.io`,
      changeOrigin: true,
      pathRewrite: (path, req) => {
        return path.replace('/staging-passportx.epsilo.io', '');
      },
      onProxyReq: (proxyReq, req) => {
        proxyReq.removeHeader('referer');
        proxyReq.removeHeader('access-control-allow-origin');
        proxyReq.removeHeader('access-control-allow-credentials');
        proxyReq.removeHeader('sec-fetch-dest');
        proxyReq.removeHeader('sec-fetch-mode');
        proxyReq.removeHeader('sec-fetch-site');
        proxyReq.removeHeader('origin');
      },
    }),
  );

  app.use(
    '/staging-onex-api.epsilo.io',
    // apicache.middleware('1 day', onlyStatus200),
    createProxyMiddleware({
      target: `https://staging-onex-api.epsilo.io`,
      // target: `https://dev-onex-api.epsilo.io`,
      changeOrigin: true,
      pathRewrite: (path, req) => {
        return path.replace('/staging-onex-api.epsilo.io', '');
      },
      onProxyReq: (proxyReq, req) => {
        proxyReq.removeHeader('referer');
        proxyReq.removeHeader('access-control-allow-origin');
        proxyReq.removeHeader('access-control-allow-credentials');
        proxyReq.removeHeader('sec-fetch-dest');
        proxyReq.removeHeader('sec-fetch-mode');
        proxyReq.removeHeader('sec-fetch-site');
        proxyReq.removeHeader('origin');
      },
    }),
  );

  app.use(
    '/staging-insight-api.epsilo.io',
    // apicache.middleware('1 day', onlyStatus200),
    createProxyMiddleware({
      // target: `http://local-insight-api.epsilo.io`,
      target: `http://staging-insight-api.epsilo.io`,
      // target: `https://dev-onex-api.epsilo.io`,
      changeOrigin: true,
      pathRewrite: (path, req) => {
        return path.replace('/staging-insight-api.epsilo.io', '');
      },
      onProxyReq: (proxyReq, req) => {
        proxyReq.removeHeader('referer');
        proxyReq.removeHeader('access-control-allow-origin');
        proxyReq.removeHeader('access-control-allow-credentials');
        proxyReq.removeHeader('sec-fetch-dest');
        proxyReq.removeHeader('sec-fetch-mode');
        proxyReq.removeHeader('sec-fetch-site');
        proxyReq.removeHeader('origin');
      },
    }),
  );
};
