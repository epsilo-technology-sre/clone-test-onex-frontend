const path = require('path');

module.exports = {
  stories: [
    '../packages/one/src/**/*.stories.tsx',
    '../packages/lazada/src/**/*.stories.tsx',
    '../packages/shopee/src/**/*.stories.tsx',
    '../packages/toko/src/**/*.stories.tsx',
    '../packages/next/src/**/*.stories.tsx',
    '../packages/next/lib/**/*.stories.tsx',
  ],
  addons: [
    'storybook-addon-designs',
    '@storybook/addon-actions',
    '@storybook/addon-links',
    '@storybook/addon-knobs',
  ],
  webpackFinal: async (config) => {
    console.dir(config.module.rules, {depth: null});
    config.module.rules.push({
      test: /\.(ts|tsx)$/,
      loader: require.resolve('babel-loader'),
      options: {
        presets: [
          ['react-app', { flow: false, typescript: true }],
          '@babel/preset-typescript',
        ],
      },
    });
    config.resolve.extensions.push('.ts', '.tsx');


    const assetRule = config.module.rules.find(({ test }) =>
      test.test('.svg'),
    );

    assetRule.test = new RegExp(
      assetRule.test.source.replace('svg|', ''),
    );

    const assetLoader = {
      loader: assetRule.loader,
      options: assetRule.options || assetRule.query,
    };

    // Merge our rule with existing assetLoader rules
    config.module.rules.unshift({
      test: /\.svg$/,
      enforce: 'pre',
      use: ['@svgr/webpack', assetLoader],
    });

    config.module.rules.push({
      test: /\.scss$/,
      use: [
        'style-loader',
        {
          loader: 'css-loader',
          options: {
            sourceMap: true,
          },
        },
        {
          loader: 'sass-loader',
          options: {
            sassOptions: {
              includePaths: [
                'node_modules',
                path.resolve(
                  __dirname,
                  '../packages/next/src/assets',
                ),
              ],
            },
            sourceMap: true,
          },
        },
      ],
      include: path.resolve(__dirname, '../'),
    });
    return config;
  },

  typescript: {
    reactDocgen: false,
  },
};
