import React from 'react';
import {
  createMuiTheme,
  ThemeProvider,
  CssBaseline,
} from '@material-ui/core';

const theme = createMuiTheme({
  typography: {
    fontFamily: `Roboto, -apple-system, BlinkMacSystemFont, 'Segoe UI',
       'Oxygen', 'Ubuntu', 'Fira Sans', 'Droid Sans',
      'Helvetica Neue', sans-serif;
    `,
  },
  overrides: {
    MuiCssBaseline: {
      '@global': {
        html: {
          WebkitFontSmoothing: 'auto',
        },
        body: { color: '#253746' },
      },
    },
  },
});

export const parameters = {
  options: {
    storySort: {
      method: 'alphabetical',
      order: ['One', 'Lazada', 'Shopee'],
      locales: '',
    },
  },
  layout: 'fullscreen',
};

export const decorators = [
  (Story) => (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Story />
    </ThemeProvider>
  ),
];

// if (typeof global.process === 'undefined') {
//   const { worker } = require.context('../packages/**/*.mocks.ts');

//   // Start the mocking when each story is loaded.
//   // Repetitive calls to the `.start()` method do not register a new worker,
//   // but check whether there's an existing once, reusing it, if so.
//   worker.start()
// }
