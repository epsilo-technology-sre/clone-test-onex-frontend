FROM node:14-alpine as builder

WORKDIR /app

# support docker caching
COPY packages/one/package.json packages/one/package.json
COPY packages/lazada/package.json packages/lazada/package.json
COPY packages/shopee/package.json packages/shopee/package.json
COPY packages/toko/package.json packages/toko/package.json
COPY packages/next/package.json packages/next/package.json
COPY packages/insight-ui/package.json packages/insight-ui/package.json
COPY package.json package.json
COPY lerna.json lerna.json
COPY yarn.lock yarn.lock
RUN yarn config set network-timeout 600000 -g
RUN yarn install
RUN yarn bootstrap

COPY . /app

ENV API_URL=https://staging-onex-api.epsilo.io
ENV API_PASSPORT_URL=https://staging-passportx.epsilo.io
ENV API_INSIGHT_URL=https://staging-insight-api.epsilo.io
ARG STAGING_RELEASE_BRANCH
ENV STAGING_RELEASE_BRANCH=${STAGING_RELEASE_BRANCH:-master}
ENV CONT_IMG_VER=${CONT_IMG_VER:-v1.0.0}
RUN yarn build
RUN mv /app/build /app/build-staging

ENV API_URL=https://onex-api.epsilo.io
ENV API_PASSPORT_URL=https://passportx.epsilo.io
ENV API_INSIGHT_URL=https://insight-api.epsilo.io
RUN yarn build
RUN mv /app/build /app/build-prod

# copy 
#ENTRYPOINT [ "echo" ]
#CMD [ "please copy frontend assets from `to-be-deploy` folder" ]

### define your new stage below

FROM nginx:alpine
# Set timezone
ENV TZ=Asia/Saigon
RUN apk add --no-cache bash vim
RUN /bin/sh -c "apk add --no-cache bash"
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
WORKDIR /usr/share/nginx/html
COPY --from=builder /app/docker_start.sh .
COPY --from=builder /app/build-staging build-staging
COPY --from=builder /app/build-prod build-prod
COPY nginx/default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["bash", "./docker_start.sh"]