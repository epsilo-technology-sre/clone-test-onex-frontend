const express = require('express');
const middleware = require('./.storybook/middleware');

const app = express();

middleware(app);

app.listen(5001, () => {
  console.info('proxy started...');
});
