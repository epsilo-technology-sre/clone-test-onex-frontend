// ** React Imports
// ** React Toastify
// ** PrismJS
import 'prismjs';
import 'prismjs/components/prism-jsx.min';
import 'prismjs/themes/prism-tomorrow.css';
import React, { lazy, Suspense } from 'react';
// ** React Perfect Scrollbar
import 'react-perfect-scrollbar/dist/css/styles.css';
// ** Core styles
import './app.scss';

// ** Lazy load app
const LazyApp = lazy(() => import('./dashboard-app'));

export default function App() {
  return (
    <div style={{ background: '#fff', padding: 12 }}>
      <Suspense fallback={<h1>Loading...</h1>}>
        <LazyApp />
      </Suspense>
    </div>
  );
}
