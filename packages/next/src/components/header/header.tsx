import React from 'react';

export const Header = () => {
  return (
    <header
      id="header"
      className="navbar navbar-expand-lg navbar-fixed navbar-height navbar-container navbar-light"
    >
      <div className="row justify-content-lg-center flex-grow-1">
        <div className="col-lg-9">
          <div className="navbar-nav-wrap">
            <div className="navbar-brand-wrapper navbar-brand-wrapper-width">
              {/* Logo */}
              <a
                className="navbar-brand d-flex align-items-center"
                href="../index.html"
                aria-label="Front"
              >
                <img
                  className="navbar-brand-logo d-none d-sm-block"
                  src="../assets/svg/logos/logo.svg"
                  alt="Logo"
                />
                <img
                  className="navbar-brand-logo-short d-block d-sm-none"
                  src="../assets/svg/logos/logo-short.svg"
                  alt="Logo"
                />
              </a>
              {/* End Logo */}
            </div>
            <div className="navbar-nav-wrap-content-left">
              {/* Search Form */}
              <form
                id="docsSearch"
                className="position-relative"
                data-hs-list-options='{
                  "searchMenu": true,
                  "keyboard": true,
                  "item": "searchTemplate",
                  "valueNames": ["component", "category", {"name": "link", "attr": "href"}],
                  "empty": "#searchNoResults"
                }'
              >
                {/* Input Group */}
                <div className="input-group input-group-merge navbar-input-group">
                  <div className="input-group-prepend">
                    <div className="input-group-text">
                      <i className="tio-search" />
                    </div>
                  </div>
                  <input
                    type="search"
                    className="search form-control"
                    placeholder="Search in front"
                    aria-label="Search in front"
                  />
                  <a
                    className="input-group-append"
                    href="javascript:;"
                  >
                    <span className="input-group-text">
                      <i
                        id="clearSearchResultsIcon"
                        className="tio-clear"
                        style={{ display: 'none' }}
                      />
                    </span>
                  </a>
                </div>
                {/* End Input Group */}
                {/* List */}
                <div
                  className="list dropdown-menu w-100 overflow-auto"
                  style={{ maxHeight: '16rem' }}
                />
                {/* End List */}
                {/* Empty */}
                <div id="searchNoResults" style={{ display: 'none' }}>
                  <div className="text-center p-4">
                    <img
                      className="mb-3"
                      src="../assets/svg/illustrations/sorry.svg"
                      alt="Image Description"
                      style={{ width: '7rem' }}
                    />
                    <p className="mb-0">No Results</p>
                  </div>
                </div>
                {/* End Empty */}
              </form>
              {/* List Item Template */}
              <div className="d-none">
                <div id="searchTemplate" className="dropdown-item">
                  <a className="d-block link" href="#">
                    <span className="category d-block text-muted mb-1" />
                    <span className="component font-weight-bold text-dark" />
                  </a>
                </div>
              </div>
              {/* End List Item Template */}
              {/* End Search Form */}
            </div>
            {/* Secondary Content */}
            <div className="navbar-nav-wrap-content-right">
              {/* Navbar */}
              <ul className="navbar-nav align-items-center flex-row">
                <li className="nav-item d-lg-block">
                  {/* Navbar Vertical Toggle */}
                  <button
                    type="button"
                    className="js-navbar-vertical-aside-toggle-invoker navbar-vertical-aside-toggle btn btn-icon btn-ghost-secondary rounded-circle"
                    data-toggle="tooltip"
                    data-placement="right"
                    title="Collapse"
                  >
                    <i className="tio-first-page navbar-vertical-aside-toggle-short-align" />
                    <i className="tio-last-page navbar-vertical-aside-toggle-full-align" />
                  </button>
                  {/* End Navbar Vertical Toggle */}
                </li>
                <li className="nav-item d-none d-md-block">
                  <a className="btn btn-primary" href="../index.html">
                    Go to Dashboard
                  </a>
                </li>
              </ul>
              {/* End Navbar */}
            </div>
            {/* End Secondary Content */}
          </div>
        </div>
      </div>
    </header>
  );
};
