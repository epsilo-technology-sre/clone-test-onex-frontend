import React from 'react';
import Select from 'react-select';
import { Button, Form } from 'reactstrap';
import styled from 'styled-components';
import {
  DBFPanelContextType,
  DBFPanelContext,
} from '../dashboard/panel';
import { mapValues, get } from 'lodash';
import { NodeData } from '../dashboard/type';
import { useFilter } from './hooks';

type FilterProps = {
  attributes: NodeData['mainAttributes'];
  onChangeAttributes: (
    attributes: Partial<NodeData['mainAttributes']>,
  ) => void;
};

let Row = styled('div')`
  margin-bottom: 0.5rem;
`;

export function DefaultFilters(props: FilterProps) {
  const { attributes: attrs } = props;
  const { filters, onChangeFilter, availFilters } = useFilter({});
  const context = React.useContext<DBFPanelContextType>(
    DBFPanelContext,
  );

  let selected = mapValues(availFilters, (items, key) => {
    if (get(attrs, ['dimensions', key])) {
      return items.filter(
        (i) => [].concat(attrs.dimensions[key]).indexOf(i.value) > -1,
      );
    }
    return [];
  });

  return (
    <Form>
      <Row>
        <Select
          placeholder={'Channels'}
          options={availFilters.channels}
          isMulti
          value={get(selected, 'channels', [])}
          onChange={(values) => {
            props.onChangeAttributes({
              dimensions: {
                ...attrs.dimensions,
                channels: values.map((i) => i.value),
              },
            });
            onChangeFilter(
              'channels',
              values.map((i) => i.value),
            );
          }}
        />
      </Row>
      <Row>
        <Select
          placeholder={'Countries'}
          options={availFilters.countries}
          isMulti
          value={get(selected, 'countries', [])}
          onChange={(values) => {
            props.onChangeAttributes({
              dimensions: {
                ...attrs.dimensions,
                countries: values.map((i) => i.value),
              },
            });
            onChangeFilter(
              'countries',
              values.map((i) => i.value),
            );
          }}
        />
      </Row>
      <Row>
        <Select
          placeholder={'Tools'}
          options={availFilters.tools}
          isMulti
          value={get(selected, 'tools', [])}
          onChange={(values) => {
            props.onChangeAttributes({
              dimensions: {
                ...attrs.dimensions,
                tools: values.map((i) => i.value),
              },
            });
            onChangeFilter(
              'countries',
              values.map((i) => i.value),
            );
          }}
        />
      </Row>
      <Row>
        <Select
          placeholder={'Shops'}
          options={availFilters.shops}
          isMulti
          value={get(selected, 'shops', [])}
          onChange={(values) => {
            props.onChangeAttributes({
              dimensions: {
                ...attrs.dimensions,
                shops: values.map((i) => i.value),
              },
            });
          }}
        />
      </Row>
      <Row>
        <Select
          placeholder={'Cohort'}
          options={availFilters.cohorts}
          value={availFilters.cohorts.find(
            (i) => i.value === get(attrs, 'cohort.0', null),
          )}
          onChange={(values) => {
            props.onChangeAttributes({
              cohort: [].concat(values).map((i) => i.value),
            });
          }}
        />
      </Row>
      <Row>
        <Select
          placeholder={'Metrics'}
          options={availFilters.metrics}
          isMulti
          value={availFilters.metrics.filter(
            (i) => get(attrs, 'metrics', []).indexOf(i.value) > -1,
          )}
          onChange={(values) => {
            props.onChangeAttributes({
              metrics: values ? values.map((i) => i.value) : [],
            });
          }}
        />
      </Row>
    </Form>
  );
}
