import React from 'react';
import { request } from '@ep/one/src/utils';
import { ONE_EP } from '@ep/one/endpoint';
import { uniq, uniqBy } from 'lodash';
import { API_INSIGHT_URL, API_URL } from '@ep/one/global';
import moment from 'moment';
import { getAllShops } from '../../util/async-resources';
import { DataQueryContext } from '../dashboard';
import { useMetricServer } from '../../hooks';

type FilterState = {
  channels?: any[];
  countries?: any[];
  tools?: any[];
  shops?: any[];
  metrics?: any[];
  cohort?: string;
};

let defaultCohorts = [
  { value: 'mtd', label: 'MTD' },
  { value: '7-days', label: 'Last 7 days' },
  { value: '30-days', label: 'Last 30 days' },
  { value: '3-months', label: 'Last 3 months' },
  { value: '6-months', label: 'Last 6 months' },
  { value: '9-months', label: 'Last 9 months' },
];

export function useFilter(valueFilters: FilterState) {
  const [allShops, setAllShops] = React.useState([]);
  const { demoMetricRange } = React.useContext(DataQueryContext);

  const { getList: getCustomMetricList } = useMetricServer(
    API_INSIGHT_URL,
  );

  const [availFilters, setAvailFilters] = React.useState<
    Omit<FilterState, 'cohort'> & {
      cohorts: any[];
    }
  >({
    channels: [],
    countries: [],
    tools: [],
    shops: [],
    metrics: [],
    cohorts: defaultCohorts,
  });

  const [filters, setFilters] = React.useState<FilterState>({
    channels: valueFilters.channels || [],
    countries: valueFilters.countries || [],
    tools: valueFilters.tools || [],
    shops: valueFilters.shops || [],
    metrics: valueFilters.metrics || [],
    cohort: valueFilters.cohort || 'mtd',
  });

  React.useEffect(() => {
    getAllShops()
      .then((allShops) => {
        setAllShops(allShops);
        let availFilters = {
          channels: uniqBy(allShops, (i: any) => i.channel_code).map(
            (i) => ({
              value: i.channel_code,
              label: i.channel_name,
            }),
          ),
          tools: uniqBy(
            allShops
              .map((i) => i.features)
              .reduce((accum, i) => {
                return accum.concat(i);
              }, []),
            (i: any) => i.feature_code,
          ).map((i) => ({
            value: i.feature_code,
            label: i.feature_name,
          })),
          countries: uniqBy(allShops, (i: any) => i.country_code).map(
            (i) => ({
              value: i.country_code,
              label: i.country_name,
            }),
          ),
          shops: allShops.map((i) => ({
            value: i.shop_eid,
            label: i.shop_name,
          })),
        };
        setAvailFilters((state) => ({
          ...state,
          ...availFilters,
        }));
        return availFilters;
      })
      .then((availFilters) => {
        Promise.all([
          getCustomMetricList(),
          request.get(API_URL + '/api/v1/dashboard/metrics', {
            from: moment().subtract(7, 'day').format('YYYY-MM-DD'),
            to: moment().subtract(1, 'day').format('YYYY-MM-DD'),
            shop_eids: availFilters.shops.map((i) => i.value),
            features: availFilters.tools.map((i) => i.value),
            currency: 'USD',
          }),
        ]).then(([customMetricList, res]) => {
          const allMetrics = res.data;

          setAvailFilters((state) => ({
            ...state,
            metrics: allMetrics
              .map((i) => ({
                ...i,
                value: i.code,
                label: i.title,
                origin: i,
              }))
              .concat(
                demoMetricRange.map((i) => ({
                  value: i.metric,
                  label: i.metric,
                  origin: i,
                })),
              )
              .concat(
                customMetricList.map((i) => ({
                  value: i.eid,
                  label: i.name + ' (custom)',
                  origin: i,
                })),
              ),
          }));
        });
      });
  }, []);

  React.useEffect(() => {
    setAvailFilters((state) => ({
      ...state,
      metrics: uniqBy(
        state.metrics.concat(
          demoMetricRange.map((i) => ({
            value: i.metric,
            label: i.metric,
          })),
        ),
        'value',
      ),
    }));
  }, [demoMetricRange]);

  const onChangeFilter = (key: keyof FilterState, values: any[]) => {
    console.info('change filter', { key, values });

    // switch(key){
    //   case 'channels': {
    //   }
    // }
  };

  return {
    filters,
    availFilters,
    onChangeFilter,
  };
}
