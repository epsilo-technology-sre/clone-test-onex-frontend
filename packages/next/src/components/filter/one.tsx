import React from 'react';
import Select from 'react-select';
import { Button, Col, Form, Row } from 'reactstrap';
import { request } from '@ep/one/src/utils';
import { ONE_EP } from '@ep/one/endpoint';
import { uniq, uniqBy } from 'lodash';
import { API_URL } from '@ep/one/global';
import moment from 'moment';

type FilterProps = {
  dimensions: any[];
  metrics: any[];
  cohort: any[];
  onUpdateFilter: (dimensions, metrics, cohort) => void;
};

export function DefaultFilters(props: FilterProps) {
  const { filters, onChangeFilter, availFilters } = useFilter({});

  return (
    <Form>
      <Row form="" className="mb-2">
        <Col md={3}>
          <Select
            placeholder={'Channels'}
            options={availFilters.channels}
          />
        </Col>
        <Col md={3}>
          <Select
            placeholder={'Countries'}
            options={availFilters.countries}
          />
        </Col>
        <Col md={3}>
          <Select
            placeholder={'Tools'}
            options={availFilters.tools}
          />
        </Col>
        <Col md={3}>
          <Select
            placeholder={'Shops'}
            options={availFilters.shops}
          />
        </Col>
      </Row>
      <Row className="mb-2">
        <Col md={3}>
          <Select
            placeholder={'Cohort'}
            options={availFilters.cohorts}
          />
        </Col>
        <Col md={9}>
          <Select
            placeholder={'Metrics'}
            options={availFilters.metrics}
          />
        </Col>
      </Row>
      <Row className="mb-2">
        <Col className={'text-left'}>
          <Button color={'secondary'} size={'sm'}>
            Preview
          </Button>
        </Col>
      </Row>
    </Form>
  );
}

type FilterState = {
  channels?: any[];
  countries?: any[];
  tools?: any[];
  shops?: any[];
  metrics?: any[];
  cohort?: string;
};

let defaultCohorts = [
  { value: 'mtd', label: 'MTD' },
  { value: '7days', label: 'Last 7 days' },
  { value: '30days', label: 'Last 30 days' },
  { value: '3months', label: 'Last 3 months' },
  { value: '6months', label: 'Last 6 months' },
  { value: '9months', label: 'Last 9 months' },
];

function useFilter(valueFilters: FilterState) {
  const [allShops, setAllShops] = React.useState([]);

  const [availFilters, setAvailFilters] = React.useState<
    Omit<FilterState, 'cohort'> & {
      cohorts: any[];
    }
  >({
    channels: [],
    countries: [],
    tools: [],
    shops: [],
    metrics: [],
    cohorts: defaultCohorts,
  });
  const [filters, setFilters] = React.useState<FilterState>({
    channels: valueFilters.channels || [],
    countries: valueFilters.countries || [],
    tools: valueFilters.tools || [],
    shops: valueFilters.shops || [],
    metrics: valueFilters.metrics || [],
    cohort: valueFilters.cohort || 'mtd',
  });

  React.useEffect(() => {
    request
      .get(ONE_EP.GET_SHOPS())
      .then((res) => {
        const allShops = res.data;
        setAllShops(res.data);
        let availFilters = {
          channels: uniqBy(allShops, (i) => i.channel_code).map(
            (i) => ({
              value: i.channel_code,
              label: i.channel_name,
            }),
          ),
          tools: uniqBy(
            allShops
              .map((i) => i.features)
              .reduce((accum, i) => {
                return accum.concat(i);
              }, []),
            (i) => i.feature_code,
          ).map((i) => ({
            value: i.feature_code,
            label: i.feature_name,
          })),
          countries: uniqBy(allShops, (i) => i.country_code).map(
            (i) => ({
              value: i.country_code,
              label: i.country_name,
            }),
          ),
          shops: allShops.map((i) => ({
            value: i.shop_eid,
            label: i.shop_name,
          })),
        };
        setAvailFilters((state) => ({
          ...state,
          ...availFilters,
        }));
        return availFilters;
      })
      .then((availFilters) => {
        request
          .get(API_URL + '/api/v1/dashboard/metrics', {
            from: moment().subtract(7, 'day').format('YYYY-MM-DD'),
            to: moment().subtract(1, 'day').format('YYYY-MM-DD'),
            shop_eids: availFilters.shops.map((i) => i.value),
            features: availFilters.tools.map((i) => i.value),
            currency: 'USD',
          })
          .then((res) => {
            const allMetrics = res.data;
            setAvailFilters((state) => ({
              ...state,
              metrics: allMetrics.map((i) => ({
                ...i,
                value: i.code,
                label: i.title,
              })),
            }));
          });
      });
  }, []);

  const onChangeFilter = (
    key: keyof FilterState,
    values: any[],
  ) => {};

  return {
    filters,
    availFilters,
    onChangeFilter,
  };
}
