import React from 'react';
import LoadingSrc from './loading.gif';
import './loading.scss';

export const ChartLoading = (props: { loading?: Boolean }) => {
  return props.loading ? (
    <div className="loading">
      <img src={LoadingSrc} />
    </div>
  ) : null;
};
