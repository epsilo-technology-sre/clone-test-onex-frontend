import { makeStore } from '@ep/one/src/utils';
import { reducer, actions } from '../dashboard-redux';
import { DashboardState } from '../type';

describe('dashboard reducer', () => {
  it('should remove container correctly', () => {
    const store = makeStore({
      storeName: 'dashboard-full',
      reducer,
      rootSaga: function* rootSaga() {
        console.info('dashboard => no saga setup yet');
      },
      initState: demoState(),
    });
    const containerId = '5LZbtAYDRE0mn8fM-NCXC';

    store.dispatch(actions.unGroupNodes({ nodeId: containerId }));

    let state: DashboardState = store.getState();

    expect(
      state.layouts['lg'].find((i) => i.id === containerId),
    ).toBeUndefined();
    expect(state.layouts['lg']).toHaveLength(2);
    expect(state.nodes).toHaveLength(2);
    expect(
      state.nodes.every((n) => n.containerId === undefined),
    ).toBeTruthy();

    let nodeLayout1 = state.layouts['lg'].find(i => i.id === 'QIFlVbEpWiWWXjHW5R6TN');
    let nodeLayout2 = state.layouts['lg'].find(i => i.id === 'pvMK81lmikMTEM80PBb0t');

    expect([nodeLayout1.layout.x, nodeLayout1.layout.y]).toEqual([1,1]);
    expect([nodeLayout2.layout.x, nodeLayout2.layout.y]).toEqual([1,3]);


  });
});

function demoState() {
  return {
    layouts: {
      lg: [
        {
          id: '5LZbtAYDRE0mn8fM-NCXC',
          layout: {
            w: 4,
            h: 4,
            x: 1,
            y: 1,
            moved: false,
            static: false,
          },
        },
      ],
      md: [
        {
          id: 'QIFlVbEpWiWWXjHW5R6TN',
          layout: {
            x: 1,
            y: 0,
            w: 4,
            h: 2,
          },
        },
        {
          id: 'pvMK81lmikMTEM80PBb0t',
          layout: {
            x: 0,
            y: 2,
            w: 4,
            h: 2,
          },
        },
      ],
      sm: [
        {
          id: 'QIFlVbEpWiWWXjHW5R6TN',
          layout: {
            x: 1,
            y: 0,
            w: 4,
            h: 2,
          },
        },
        {
          id: 'pvMK81lmikMTEM80PBb0t',
          layout: {
            x: 0,
            y: 2,
            w: 4,
            h: 2,
          },
        },
      ],
      xxs: [
        {
          id: 'QIFlVbEpWiWWXjHW5R6TN',
          layout: {
            x: 1,
            y: 0,
            w: 4,
            h: 2,
          },
        },
        {
          id: 'pvMK81lmikMTEM80PBb0t',
          layout: {
            x: 0,
            y: 2,
            w: 4,
            h: 2,
          },
        },
      ],
    },
    nodes: [
      {
        id: 'QIFlVbEpWiWWXjHW5R6TN',
        chartLibId: 'metric',
        containerId: '5LZbtAYDRE0mn8fM-NCXC',
      },
      {
        id: 'pvMK81lmikMTEM80PBb0t',
        chartLibId: 'metricTotal',
        containerId: '5LZbtAYDRE0mn8fM-NCXC',
      },
      {
        id: '5LZbtAYDRE0mn8fM-NCXC',
        chartLibId: 'container',
        mainAttributes: null,
        customAttributes: {
          nodeIds: ['QIFlVbEpWiWWXjHW5R6TN', 'pvMK81lmikMTEM80PBb0t'],
          nodeLayouts: [
            {
              id: 'QIFlVbEpWiWWXjHW5R6TN',
              layout: {
                w: 4,
                h: 2,
                x: 0,
                y: 0,
                moved: false,
                static: false,
              },
            },
            {
              id: 'pvMK81lmikMTEM80PBb0t',
              layout: {
                x: 0,
                y: 2,
                w: 4,
                h: 2,
              },
            },
          ],
          w: 4,
          h: 4,
        },
      },
    ],
    mode: {
      type: 'select',
      activeNode: '5LZbtAYDRE0mn8fM-NCXC',
    },
    breakpoint: 'lg',
    baseBreakpoint: 'lg',
    layoutColumns: {
      lg: 24,
      md: 18,
      sm: 6,
      xs: 2,
      xxs: 2,
    },
  };
}
