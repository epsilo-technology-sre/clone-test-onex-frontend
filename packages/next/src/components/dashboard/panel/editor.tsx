import { makeStyles } from '@material-ui/core';
import clsx from 'clsx';
import React from 'react';
import ReactDOM from 'react-dom';
import Select from 'react-select';
import Split from 'react-split';
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap';
import { get } from 'lodash';
import styled from 'styled-components';
import { DashboardContext } from '../context';
import ChartLibs from '../node/chartlibs';
import { NodeData } from '../type';
import { DBFPanelContext } from './context';
import { NodeFactory } from '../node/factory';

type NodePanelProps = {
  nodeId: string;
  isExpanded: boolean;
  nodeData: NodeData;
  componentFilter:
    | React.Component<any>
    | React.FunctionComponent<any>;
  onCancel: () => void;
};

export function NodePanel({
  nodeId,
  isExpanded = true,
  nodeData,
  componentFilter = () => <div>no filter</div>,
  onCancel = () => {},
}: NodePanelProps) {
  const classes = useStyle();
  const context = React.useContext(DashboardContext);
  const [chartLibId, setChartLibId] = React.useState(
    nodeData ? nodeData.chartLibId : null,
  );
  const [chartData, setChartData] = React.useState<NodeData>(
    nodeData,
  );
  const Filter = React.useMemo(() => componentFilter, []);

  const chartLibs = React.useMemo(() => {
    return ChartLibs.asList().map((i) => ({
      ...i,
      value: i.chartLibId,
    }));
  }, []);

  const ref = React.useRef();
  const refPreview = React.useRef();

  React.useEffect(() => {
    console.info('**** props change....', nodeId, nodeData);
    if (nodeId) {
      setChartLibId(nodeData ? nodeData.chartLibId : null);
      setChartData(nodeData);
    }
  }, [nodeId, nodeData]);

  React.useEffect(() => {
    console.info(ref.current, { nodeId, chartData, chartLibId });
    if (!isExpanded) return;

    const displayChartConfiguration = () => {
      console.info('effect render form', {
        nodeId,
        chartData,
        chartLibId,
      });
      let chartLib = ChartLibs.find(chartLibId);

      // FIXME: more fine tunes on reuse react component
      ReactDOM.unmountComponentAtNode(ref.current);
      window.requestAnimationFrame(() => {
        chartLib.component.renderConfigurationForm(
          ref.current,
          get(chartData, 'customAttributes', {}),
          (settings) => {
            console.info('update chart settings...', settings);
            setChartData({
              ...chartData,
              id: chartData.id,
              customAttributes: settings,
              chartLibId: chartLib.chartLibId,
            });
          },
        );
      });

      // ReactDOM.unmountComponentAtNode(refPreview.current);
      // window.requestAnimationFrame(() => {
      //   chartLib.component.render(
      //     refPreview.current,
      //     chartData,
      //     null,
      //   );
      // });
    };
    let tid = 0;
    if (!ref.current) {
      tid = window.setInterval(() => {
        if (ref.current) {
          window.clearInterval(tid);
          displayChartConfiguration();
        }
      }, 500);
    } else if (chartData && chartLibId) {
      displayChartConfiguration();
    }
    return () => window.clearInterval(tid);
  }, [isExpanded, chartData, chartLibId]);

  function handleChangeChartLib(value, action) {
    console.info('chartlib', { value, action });
    setChartLibId(value.chartLibId);
  }

  function handleUpdateNodeData() {
    context.onNodeRequestUpdate(nodeId, {
      ...chartData,
      chartLibId: chartLibId,
    });
  }

  return (
    <DBFPanelContext.Provider
      value={{
        onChangeChartLib: (chart) =>
          handleChangeChartLib(chart, null),
        onChangeMainAttributes: (attr) => {
          console.info('onchangemainattributes', attr);
          setChartData((state) => ({
            ...state,
            mainAttributes: { ...state.mainAttributes, ...attr },
          }));
        },
      }}
    >
      <Modal
        isOpen={isExpanded}
        backdrop={true}
        size={'xl'}
        className={classes.panelModal}
        zIndex={1200}
      >
        <ModalBody>
          <StyledSplit
            direction="horizontal"
            sizes={[80, 20]}
            gutterSize={1}
          >
            <div
              className={clsx(
                'mt-3',
                'ml-3',
                'mr-3',
                'preview-section',
              )}
            >
              <h3 className={clsx('text-black-50')}>Preview</h3>
              <div className={clsx('chart-preview')}>
                <div
                  className={clsx(
                    'chart-preview-item',
                    'sidebar-scrollbar',
                  )}
                >
                  <div
                    ref={refPreview}
                    className="chart-preview-item-wrapper"
                  >
                    <NodeFactory
                      data={{ ...chartData, chartLibId }}
                      idPrefix="chart-preview"
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className={clsx('configuration', 'ml-3')}>
              <Nav tabs>
                <NavItem active>
                  <NavLink href="#">Data</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="#">Connection</NavLink>
                </NavItem>
              </Nav>
              <div className={clsx('mt-3')}>
                <Filter
                  attributes={get(chartData, 'mainAttributes', {})}
                />
              </div>
              <div>
                <Select
                  style={{ marginTop: 18 }}
                  options={chartLibs}
                  value={chartLibs.find(
                    (i) => i.chartLibId === chartLibId,
                  )}
                  getOptionLabel={(option) => option.label}
                  onChange={(value, action) =>
                    handleChangeChartLib(value, action)
                  }
                  placeholder={'Choose charts'}
                />
                <div className="configuration-form" ref={ref}></div>
              </div>
            </div>
          </StyledSplit>
        </ModalBody>
        <ModalFooter>
          <Button
            color={'secondary'}
            outline
            onClick={() => onCancel()}
          >
            Cancel
          </Button>
          <Button
            color={'primary'}
            onClick={() => {
              handleUpdateNodeData();
              onCancel();
            }}
          >
            Done
          </Button>
        </ModalFooter>
      </Modal>
    </DBFPanelContext.Provider>
  );
}

const useStyle = makeStyles(() => {
  const panelWidth = 390;
  return {
    bodyExpanded: {
      width: `calc(100vw + ${panelWidth}px)`,
      marginRight: `${panelWidth}px !important`,
    },
    panelPaper: {
      fontSize: `12px`,
      width: `${panelWidth}px`,
    },
    panelModal: {
      zIndex: 21,
      maxWidth: 'calc(100vw - 3.5rem)',
      '& .modal-content': {
        height: 'calc(100vh - 3.5rem)',
      },
    },
    '@media (min-width:1200px)': {
      panelModal: {
        '&.modal-dialog.modal-xl': {
          maxWidth: 'calc(100vw - 3.5rem)',
        },
      },
    },
  };
});

const StyledSplit = styled(Split)`
  display: flex;
  height: 100%;
  flex-direction: ${(props) =>
    props.direction === 'vertical' ? 'column' : 'row'};
  .gutter {
    background-color: #f1f1f1;
    background-repeat: no-repeat;
    background-position: 50%;
  }

  .gutter.gutter-horizontal {
    background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAFAQMAAABo7865AAAABlBMVEVHcEzMzMzyAv2sAAAAAXRSTlMAQObYZgAAABBJREFUeF5jOAMEEAIEEFwAn3kMwcB6I2AAAAAASUVORK5CYII=');
    cursor: col-resize;
  }

  .gutter.gutter-vertical {
    background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAeCAYAAADkftS9AAAAIklEQVQoU2M4c+bMfxAGAgYYmwGrIIiDjrELjpo5aiZeMwF+yNnOs5KSvgAAAABJRU5ErkJggg==');
    cursor: row-resize;
  }
  .configuration-form {
    padding-top: 1rem;
    padding-right: 1rem;
  }
  .preview-section {
    display: flex;
    flex-direction: column;
  }
  .chart-preview {
    flex-grow: 1;
    flex-shrink: 0;
    border-radius: 5px;
    position: relative;
  }
  .chart-preview-item {
    width: 100%;
    height: 100%;
    position: absolute;
    padding: 1px;
    display: flex;
    justify-content: center;
    align-items: center;
    overflow: auto;
    div.chart-preview-item-wrapper {
      margin: auto;
      maxheight: 100%;
      width: 100%;
    }
  }
`;
