import { Drawer, makeStyles } from '@material-ui/core';
import clsx from 'clsx';
import React from 'react';
import ReactDOM from 'react-dom';
import Select from 'react-select';
import Split from 'react-split';
import { Nav, NavItem, NavLink } from 'reactstrap';
import styled from 'styled-components';
import { get } from 'lodash';
import { DashboardContext } from '../context';
import ChartLibs from '../node/chartlibs';
import {
  ChartLib,
  DashboardState,
  NodeData,
  NodeId,
  NodePanelProps,
} from '../type';
import { useDispatch, useSelector } from 'react-redux';
import { actions } from '../dashboard-redux';

export function NodePanelContainer(
  props: Omit<NodePanelProps, 'nodeData' | 'activeTab'>,
) {
  const dispatch = useDispatch();
  const chartData = useSelector((state: DashboardState) => {
    let node = state.nodes.find((n) => n.id === props.nodeId);
    let parentList = [];
    let childList = [];
    if (node) {
      parentList = state.nodes.filter((n) =>
        (node.parentList || []).some((n1) => n1.nodeId === n.id),
      );
      childList = state.nodes.filter((n) =>
        (node.childList || []).some((n1) => n1.nodeId === n.id),
      );
    }
    return { node, parentList, childList };
  });

  return (
    <NodePanelUI
      nodeId={props.nodeId}
      nodeData={chartData.node}
      parentList={chartData.parentList}
      childList={chartData.childList}
      isExpanded={props.isExpanded}
      componentFilter={props.componentFilter}
      onCancel={props.onCancel}
      onAddConnection={(nodeId, eventId) => {
        dispatch(actions.connect({ nodeId, eventId }));
      }}
    />
  );
}
export { NodePanelContainer as NodePanel };

function NodePanelUI({
  nodeId,
  childList,
  parentList,
  nodeData: chartData,
  isExpanded = true,
  componentFilter = () => <div>no filter</div>,
  onCancel = () => {},
  onAddConnection = (nodeId, eventId) => {},
}: NodePanelProps) {
  const classes = useStyle();
  const context = React.useContext(DashboardContext);
  const Filter = React.useMemo(() => componentFilter, []);
  const [activeTab, setActiveTab] = React.useState<
    'data' | 'connection'
  >('data');
  // const [activeTab, setActiveTab] = React.useState<
  //   'data' | 'connection'
  // >('connection');

  let chartLibId = get(chartData, 'chartLibId', null);

  const chartLibs = React.useMemo(() => {
    return ChartLibs.asList().map((i) => ({
      ...i,
      value: i.chartLibId,
      label: i.label,
    }));
  }, []);
  let chartLib = chartLibs.find((i) => i.value === chartLibId);
  let chartLibEvents =
    chartLib && chartLib.component.getInteractionEvents
      ? chartLib.component.getInteractionEvents()
      : [];

  const ref = React.useRef();
  React.useEffect(() => {
    if (isExpanded) {
      document.body.classList.add(classes.bodyExpanded);
    } else {
      document.body.classList.remove(classes.bodyExpanded);
    }
  }, [isExpanded]);

  React.useEffect(() => {
    console.info('form render >>>', {
      nodeId,
      chartData,
      chartLibId,
    });
    if (ref.current && nodeId && chartData && chartLibId) {
      console.info('effect render form', {
        nodeId,
        chartData,
        chartLibId,
      });
      let chartLib = ChartLibs.find(chartLibId);

      // FIXME: more fine tunes on reuse react component
      ReactDOM.unmountComponentAtNode(ref.current);
      window.requestAnimationFrame(() => {
        chartLib.component.renderConfigurationForm(
          ref.current,
          chartData.customAttributes,
          (settings) => {
            context.onNodeSubmitUpdate(chartData.id, {
              ...chartData,
              customAttributes: settings,
            });
            return Promise.resolve(true);
          },
        );
      });
    }
  }, [nodeId, chartData, chartLibId]);

  function handleChangeChartLib(value, action) {
    console.info('chartlib', { value, action });
    context.onNodeSubmitUpdate(chartData.id, {
      ...chartData,
      chartLibId: value.value,
    });
  }

  function handleUpdateNodeData() {
    // context.onNodeRequestUpdate(nodeId, chartData);
  }

  return (
    <Drawer
      open={isExpanded}
      anchor={'right'}
      variant={'persistent'}
      classes={{ paper: classes.panelPaper }}
      className={clsx(classes.editorPanel)}
    >
      <div
        className={clsx(
          'card card-lg sidebar-card sidebar-footer-fixed',
        )}
      >
        <div className="card-body sidebar-body sidebar-scrollbar">
          <Nav tabs>
            <NavItem active={activeTab === 'data'}>
              <NavLink href="#" onClick={() => setActiveTab('data')}>
                Data
              </NavLink>
            </NavItem>
            <NavItem active={activeTab === 'connection'}>
              <NavLink
                href="#"
                onClick={() => setActiveTab('connection')}
              >
                Connection
              </NavLink>
            </NavItem>
          </Nav>
          {/** start data configuration */}
          <div
            className={clsx('mt-3', activeTab !== 'data' && 'd-none')}
          >
            <Filter
              attributes={get(chartData, 'mainAttributes', {})}
              onChangeAttributes={(attr) => {
                console.info('onchangemainattributes', attr);
                context.onNodeSubmitUpdate(chartData.id, {
                  ...chartData,
                  mainAttributes: {
                    ...chartData.mainAttributes,
                    ...attr,
                  },
                });
              }}
            />
            <div className={clsx('mt-2')}>
              <Select
                style={{ marginTop: 18 }}
                options={chartLibs}
                value={chartLib}
                getOptionLabel={(option) => option.label}
                onChange={(value, action) =>
                  handleChangeChartLib(value, action)
                }
                placeholder={'Choose charts'}
              />
            </div>
            <div className="configuration-form" ref={ref}></div>
          </div>
          {/** start conneciton configuration */}
          <div
            className={clsx(
              'mt-3',
              activeTab !== 'connection' && 'd-none',
            )}
          >
            <NodeConnectionUI
              nodeId={nodeId}
              parentList={parentList}
              childList={childList}
              onAddConnection={onAddConnection}
              chartLibEvents={chartLibEvents}
            ></NodeConnectionUI>
          </div>
        </div>
      </div>
      <div className="card-footer sidebar-footer">
        <button
          type="button"
          className="btn btn-primary"
          onClick={handleUpdateNodeData}
        >
          Update
        </button>
        <button
          type="button"
          onClick={() => onCancel()}
          className="ml-2 btn btn-outline-secondary"
        >
          Close
        </button>
      </div>
      {/* End Footer */}
    </Drawer>
  );
}

const useStyle = makeStyles(() => {
  const panelWidth = 390;
  return {
    bodyExpanded: {
      width: `calc(100vw + ${panelWidth}px)`,
      marginRight: `${panelWidth}px !important`,
    },
    panelPaper: {
      fontSize: `12px`,
      width: `${panelWidth}px`,
    },
    panelModal: {
      zIndex: 21,
    },
    editorPanel: {
      '& .card-lg > .card-body': {
        paddingTop: 0,
      },
      '& .configuration-form': {
        marginTop: '1rem',
      },
    },
  };
});

function NodeConnectionUI({
  nodeId,
  parentList = [],
  childList = [],
  chartLibEvents = [],
  onAddConnection,
}: {
  nodeId: NodeId;
  parentList: NodeData[];
  childList: NodeData[];
  chartLibEvents: {
    eventId: string;
    label: string;
  }[];
  onAddConnection: (nodeId: NodeId, eventId: string) => void;
}) {
  const [eventId, setEventId] = React.useState(null);

  return (
    <React.Fragment>
      <div
        className={clsx(
          'parentList',
          parentList && parentList.length > 0 ? '' : 'd-none',
        )}
      >
        <h3>In</h3>
        {parentList.map((i) => (
          <div className="list-unstyled-py-3" key={i.id}>{i.id}</div>
        ))}
      </div>
      <div className={clsx('childList')}>
        <h3 className={'h3 mb-1'}>When trigger an interaction</h3>
        <div className="list-group list-group-flush list-group-no-gutters">
          {childList.map((i) => (
            <div
              key={i.id}
              className={'list-group-item d-flex align-items-center'}
            >
              {i.id}
            </div>
          ))}
          <div
            className={'list-group-item d-flex align-items-center'}
          >
            <div className={'flex-fill'}>
              {chartLibEvents.map((c) => {
                return (
                  <button
                    key={c.eventId}
                    type="button"
                    className="btn btn-light btn-sm"
                    onClick={() => {
                      onAddConnection(nodeId, c.eventId);
                    }}
                  >
                    {c.label}
                  </button>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

const StyledSplit = styled(Split)`
  display: flex;
  height: 100%;
  flex-direction: ${(props) =>
    props.direction === 'vertical' ? 'column' : 'row'};
  .gutter {
    background-color: #f1f1f1;
    background-repeat: no-repeat;
    background-position: 50%;
  }

  .gutter.gutter-horizontal {
    background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAFAQMAAABo7865AAAABlBMVEVHcEzMzMzyAv2sAAAAAXRSTlMAQObYZgAAABBJREFUeF5jOAMEEAIEEFwAn3kMwcB6I2AAAAAASUVORK5CYII=');
    cursor: col-resize;
  }

  .gutter.gutter-vertical {
    background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAeCAYAAADkftS9AAAAIklEQVQoU2M4c+bMfxAGAgYYmwGrIIiDjrELjpo5aiZeMwF+yNnOs5KSvgAAAABJRU5ErkJggg==');
    cursor: row-resize;
  }
`;
