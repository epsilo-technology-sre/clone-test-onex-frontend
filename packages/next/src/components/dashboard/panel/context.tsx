import React from 'react';

export type DBFPanelContextType = {
  onChangeMainAttributes: (attrs: { [key: string]: any }) => void;
  onChangeChartLib: (chartId: string) => void;
};

export const DBFPanelContext = React.createContext<DBFPanelContextType>(
  {
    onChangeMainAttributes: () => {},
    onChangeChartLib: () => {},
  },
);
