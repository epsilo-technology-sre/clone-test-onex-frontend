import React from 'react';
import { action, actions } from '@storybook/addon-actions';
import { NodePanel } from './editor';
import { NodePanel as DrawerPanel } from './editor-vertical';
import 'react-perfect-scrollbar/dist/css/styles.css';
// ** Core styles
import '@epi/next/src/app.scss';
import { DefaultFilters } from '../../filter/one';
import { DefaultFilters as VerticalFilters } from '../../filter/one-vertical';
import { Provider } from 'react-redux';
import { makeStore } from '@ep/one/src/utils';
import { nanoid } from 'nanoid';

export default {
  title: 'DBF / Panel',
};

export function Primary() {
  let nodeId = '130488574';
  let childList = [
    {
      eventId: 'clickDataPoint',
      nodeId: nanoid(),
    },
    {
      eventId: 'clickDataPoint',
      nodeId: nanoid(),
    },
  ];
  const store = makeStore({
    storeName: 'panel-drawer-right',
    reducer: (state, actions) => {
      return state;
    },
    rootSaga: function* rootSaga() {
      console.info('not setup yet');
    },
    initState: {
      nodes: [
        {
          id: nodeId,
          chartLibId: 'globalFilters',
          parentList: [
            {
              eventId: 'clickDataPoint',
              nodeId: nanoid(),
            },
          ],
          childList: childList,
        },
        ...childList.map((i) => ({
          id: i.nodeId,
        })),
      ],
    },
  });

  return (
    <Provider store={store}>
      <NodePanel
        nodeId={'130488574'}
        nodeData={{
          id: nodeId,
          chartLibId: 'globalFilters',
          mainAttributes: {
            dimensions: {
              channels: ['LAZADA'],
              countries: ['SG', 'VN'],
              tools: ['M_LZD_SS'],
              shops: [4172, 4162, 4152],
            },
            cohort: '7days',
            metrics: ['cost'],
          },
          customAttributes: {},
        }}
        isExpanded={true}
        componentFilter={VerticalFilters}
        onCancel={actions('onCancel')}
      />
    </Provider>
  );
}

export function PrimaryMetric() {
  let nodeId = '130488574';
  let childList = [
    {
      eventId: 'clickDataPoint',
      nodeId: nanoid(),
    },
    {
      eventId: 'clickDataPoint',
      nodeId: nanoid(),
    },
  ];
  const store = makeStore({
    storeName: 'panel-drawer-right',
    reducer: (state, actions) => {
      return state;
    },
    rootSaga: function* rootSaga() {
      console.info('not setup yet');
    },
    initState: {
      nodes: [
        {
          id: nodeId,
          chartLibId: 'metricDetail',
          parentList: [
            {
              eventId: 'clickDataPoint',
              nodeId: nanoid(),
            },
          ],
          childList: childList,
        },
        ...childList.map((i) => ({
          id: i.nodeId,
        })),
      ],
    },
  });

  return (
    <Provider store={store}>
      <NodePanel
        nodeId={'130488574'}
        nodeData={{
          id: nodeId,
          // chartLibId: 'metricDetail',
          chartLibId: 'divider',
          mainAttributes: {
            dimensions: {
              channels: ['LAZADA'],
              countries: ['SG', 'VN'],
              tools: ['M_LZD_SS'],
              shops: [4172, 4162, 4152],
            },
            cohort: '7days',
            metrics: ['cost'],
          },
          customAttributes: {},
        }}
        isExpanded={true}
        componentFilter={VerticalFilters}
        onCancel={actions('onCancel')}
      />
    </Provider>
  );
}

export function Drawer() {
  let nodeId = '130488574';
  let childList = [
    {
      eventId: 'clickDataPoint',
      nodeId: nanoid(),
    },
    {
      eventId: 'clickDataPoint',
      nodeId: nanoid(),
    },
  ];

  const store = makeStore({
    storeName: 'panel-drawer-right',
    reducer: (state, actions) => {
      return state;
    },
    rootSaga: function* rootSaga() {
      console.info('not setup yet');
    },
    initState: {
      nodes: [
        {
          id: nodeId,
          chartLibId: 'metricDetail',
          parentList: [
            {
              eventId: 'clickDataPoint',
              nodeId: nanoid(),
            },
          ],
          childList: childList,
        },
        ...childList.map((i) => ({
          id: i.nodeId,
        })),
      ],
    },
  });

  return (
    <Provider store={store}>
      <DrawerPanel
        nodeId={'130488574'}
        isExpanded={true}
        onAddConnection={action('onAddConnection')}
        onCancel={action('onCancel')}
      />
    </Provider>
  );
}
