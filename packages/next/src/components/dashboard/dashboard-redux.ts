import { actionNative } from '@ep/one/src/utils/redux';
import { produce } from 'immer';
import { cloneDeep, mapValues, omit, sortBy } from 'lodash';
import { DashboardState, NodeData } from './type';
import { reArrangeLayout } from '@epi/next/int/auto-layout';
import { nanoid } from 'nanoid';

function isContainer(nodeData: NodeData) {
  return nodeData.chartLibId === 'container';
}

const actions = {
  set: actionNative({
    actname: 'SET',
    fun: ({ layouts, nodes }) => ({ layouts, nodes }),
  }),
  select: actionNative({
    actname: 'SELECT',
    fun: ({ nodeId }) => ({ nodeId, mode: 'select' }),
  }),
  selectMultiple: actionNative({
    actname: 'SELECT_MULTIPLE',
    fun: ({ nodeIds }) => ({ nodeIds }),
  }),
  edit: actionNative({
    actname: 'EDIT',
    fun: ({ nodeId }) => ({ nodeId, mode: 'edit' }),
  }),
  connect: actionNative({
    actname: 'CONNECT',
    fun: ({ nodeId, eventId, drilldownPointId }) => ({
      nodeId,
      mode: 'connect',
      eventId,
      drilldownPointId,
    }),
  }),
  connectCancel: actionNative({
    actname: 'CONNECT_CANCEL',
    fun: () => {},
  }),
  editText: actionNative({
    actname: 'EDIT_TEXT',
    fun: ({ nodeId }) => ({ nodeId, mode: 'edit_text' }),
  }),
  addNewNode: actionNative({
    actname: 'ADD_NEW_NODE',
    fun: ({ node }) => {
      return { node };
    },
  }),
  removeNode: actionNative({
    actname: 'REMOVE_NODE',
    fun: ({ nodeId }) => {
      return { nodeId };
    },
  }),
  updateNode: actionNative({
    actname: 'UPDATE_NODE',
    fun: ({ nodeId, data }) => ({ nodeId, data }),
  }),
  groupNodes: actionNative({
    actname: 'GROUP_NODES',
    fun: ({ nodeIds }) => ({ nodeIds }),
  }),
  unGroupNodes: actionNative({
    actname: 'UNGROUP_NODES',
    fun: ({ nodeId }) => ({ nodeId }),
  }),
  submitDrilldownConnect: actionNative({
    actname: 'SUBMIT_DRILLDOWN_CONNECT',
    fun: ({ nodeSrc, nodeDest, eventId, added }) => {
      return { nodeSrc, nodeDest, eventId, added };
    },
  }),
  updateBreakpoint: actionNative({
    actname: 'UPDATE_BREAKPOINT',
    fun: ({ breakpoint }) => ({ breakpoint }),
  }),
  updateLayout: actionNative({
    actname: 'UPDATE_LAYOUT',
    fun: ({ layout }) => ({ layout }),
  }),
  duplicateNode: actionNative({
    actname: 'DUPLICATE_NODE',
    fun: ({ nodeId }) => ({ nodeId }),
  }),
};

export { actions };

export const initState = {
  layouts: { md: [] },
  nodes: [],
  mode: { type: null, activeNode: null },
  breakpoint: 'lg',
  baseBreakpoint: 'lg',
  layoutColumns: {
    lg: 24,
    md: 18,
    sm: 6,
    xs: 2,
    xxs: 2,
  },
};

export function reducer(
  state: DashboardState = {
    layouts: { md: [] },
    nodes: [],
    mode: { type: null, activeNode: null },
    breakpoint: 'lg',
    baseBreakpoint: 'lg',
    layoutColumns: {
      lg: 24,
      md: 18,
      sm: 6,
      xs: 2,
      xxs: 2,
    },
    // mode: { type: 'connect', activeNode: 'OvpbiH8VikHYavSU1TQI2' },
  },
  action: { type: string; payload: any },
) {
  let payload = action.payload;

  switch (action.type) {
    case actions.edit.type():
    case actions.editText.type():
    case actions.select.type(): {
      state = produce(state, (draft) => {
        draft.mode.type = payload.mode;
        draft.mode.activeNode = payload.nodeId;
        if (payload.nodeId === null) {
          draft.mode.type = null;
        }
      });
      break;
    }
    case actions.connect.type(): {
      state = produce(state, (draft) => {
        draft.mode.type = 'connect';
        draft.mode.activeNode = payload.nodeId;
        draft.mode.eventId = payload.eventId;
        draft.mode.drilldownPointId = payload.drilldownPointId;
        draft.mode.activeNodeData = state.nodes.find(
          (i) => i.id === payload.nodeId,
        );
      });
      break;
    }
    case actions.set.type(): {
      state = produce(state, (draft) => {
        draft.layouts = payload.layouts;
        draft.nodes = payload.nodes;
      });
      break;
    }
    case actions.addNewNode.type(): {
      state = produce(state, (draft) => {
        let { node } = payload;
        draft.layouts = mapValues(state.layouts, (layout) => {
          return layout.concat(node);
        });
        draft.nodes = state.nodes.concat({
          id: node.id,
          chartLibId: undefined,
        });
      });
      break;
    }
    case actions.removeNode.type(): {
      let id = payload.nodeId;
      state = produce(state, (draft) => {
        let nuLayouts = mapValues(state.layouts, (layout) => {
          return (layout || []).filter((i) => i.id !== id);
        });

        let nuNodes = state.nodes.filter((n) => {
          return n.id !== id && n.containerId !== id;
        });

        draft.layouts = nuLayouts;
        draft.nodes = nuNodes;
      });
      break;
    }
    case actions.updateNode.type(): {
      let { nodeId, data } = payload;
      state = produce(state, (draft) => {
        let index = state.nodes.findIndex((n) => n.id === nodeId);
        if (index > -1) {
          draft.nodes[index] = data;
        }
      });
      break;
    }
    case actions.groupNodes.type(): {
      let { nodeIds } = payload;
      state = produce(state, (draft) => {
        let containerId = nanoid();
        let layout = state.layouts[state.breakpoint];
        let nodeLayouts = layout.filter(
          (n) => nodeIds.indexOf(n.id) > -1,
        );
        let rect = nodeLayouts.reduce(
          (acc, n) => {
            return {
              x:
                acc.x === null
                  ? n.layout.x
                  : Math.min(acc.x, n.layout.x),
              y:
                acc.y === null
                  ? n.layout.y
                  : Math.min(acc.y, n.layout.y),
              x1:
                acc.y1 === null
                  ? n.layout.x + n.layout.w
                  : Math.max(acc.x1, n.layout.x + n.layout.w),
              y1:
                acc.x === null
                  ? n.layout.y + n.layout.h
                  : Math.max(acc.y1, n.layout.y + n.layout.h),
            };
          },
          { x: null, y: null, x1: null, y1: null },
        );

        let cLayout = {
          x: rect.x,
          y: rect.y,
          w: rect.x1 - rect.x,
          h: rect.y1 - rect.y,
        };

        for (let nid of nodeIds) {
          let li = layout.findIndex((l) => l.id === nid);
          let ni = state.nodes.findIndex((n) => n.id === nid);
          draft.layouts[state.breakpoint][
            li
          ].containerId = containerId;
          draft.nodes[ni].containerId = containerId;
        }

        draft.nodes.push({
          id: containerId,
          chartLibId: 'container',
          mainAttributes: null,
          customAttributes: {
            nodeIds: nodeIds,
            nodeLayouts: nodeLayouts.map((i) => ({
              ...i,
              layout: {
                ...i.layout,
                x: i.layout.x - cLayout.x,
                y: i.layout.y - cLayout.y,
              },
            })),
            w: cLayout.w,
            h: cLayout.h,
          },
        });
        draft.layouts[state.breakpoint].push({
          id: containerId,
          layout: cLayout,
        });
      });
      break;
    }
    case actions.unGroupNodes.type(): {
      let { nodeId } = payload;
      state = produce(state, (draft) => {
        let breakpoint = state.breakpoint;
        let containerId = nodeId;
        let layout = state.layouts[breakpoint];
        let container = state.nodes.find((i) => i.id === containerId);
        let containerLayout = layout.find(
          (i) => i.id === containerId,
        );

        let childLayout = container.customAttributes.nodeLayouts;

        let nodes = state.nodes.reduce((ls, node) => {
          if (node.id === containerId) {
            return ls;
          }
          if (node.containerId === containerId) {
            return [...ls, omit(node, 'containerId')];
          } else {
            return ls.concat(node);
          }
        }, []);

        layout = layout
          .concat(
            childLayout.map((i) => ({
              ...i,
              layout: {
                ...i.layout,
                x: i.layout.x + containerLayout.layout.x,
                y: i.layout.y + containerLayout.layout.y,
              },
            })),
          )
          .filter((i) => i.id !== containerId);
        draft.layouts[breakpoint] = layout;
        draft.nodes = nodes;
      });
      break;
    }
    case actions.submitDrilldownConnect.type(): {
      let {
        nodeSrc: nodeSrcId,
        nodeDest: nodeDestId,
        eventId,
        added,
      } = payload;
      state = produce(state, (draft) => {
        const nodes = state.nodes;
        let nodeSrcIndex = nodes.findIndex((n) => n.id === nodeSrcId);
        let nodeDestIndex = nodes.findIndex(
          (n) => n.id === nodeDestId,
        );

        console.info({ nodeSrcIndex, nodeDestIndex });

        let nodeSrc = nodes[nodeSrcIndex];
        let nodeDest = nodes[nodeDestIndex];

        let childList = nodeSrc.childList || [];
        let parentList = nodeDest.parentList || [];

        nodeSrc.childList = childList
          .filter(
            (i) => i.eventId !== eventId || i.nodeId !== nodeDestId,
          )
          .concat(
            added
              ? {
                  nodeId: nodeDestId,
                  eventId: eventId,
                }
              : [],
          );
        nodeDest.parentList = parentList
          .filter(
            (i) => i.eventId !== eventId || i.nodeId !== nodeSrcId,
          )
          .concat(
            added
              ? {
                  nodeId: nodeSrcId,
                  eventId: eventId,
                }
              : [],
          );
        nodes[nodeSrcIndex] = nodeSrc;
        nodes[nodeDestIndex] = nodeDest;

        draft.nodes = [...nodes];

        draft.mode.type = 'select';
      });
      break;
    }
    case actions.connectCancel.type(): {
      state = produce(state, (draft) => {
        draft.mode.type = 'select';
      });
      break;
    }

    case actions.updateBreakpoint.type(): {
      let { breakpoint } = payload;
      state = produce(state, (draft) => {
        draft.breakpoint = breakpoint;
        if (breakpoint !== state.baseBreakpoint) {
          let layout: any[] = state.layouts[state.baseBreakpoint];
          let baseCols = state.layoutColumns[state.baseBreakpoint];
          let bpCols = state.layoutColumns[breakpoint];

          let sortedLayouts = reArrangeLayout({
            layout,
            base: baseCols,
            viewport: bpCols,
          });

          console.info({ sortedLayouts });

          draft.layouts[breakpoint] = sortedLayouts;
        }
      });
      break;
    }
    case actions.updateLayout.type(): {
      state = produce(state, (draft) => {
        draft.layouts[state.breakpoint] = payload.layout;
      });
      break;
    }
    case actions.selectMultiple.type(): {
      state = produce(state, (draft) => {
        draft.mode.type = 'select_multiple';
        draft.mode.activeNodes = payload.nodeIds;
      });
      break;
    }
    case actions.duplicateNode.type(): {
      const { nodeId } = payload;

      state = produce(state, (draft) => {
        let bp = state.breakpoint;
        let layout = state.layouts[bp];

        let nLayout = layout.find((i) => i.id === nodeId);
        const dupNode = cloneDeep(
          state.nodes.find((n) => n.id === nodeId),
        );

        dupNode.id = nanoid();

        const dupLayout = {
          ...nLayout,
          layout: { ...nLayout.layout },
        };

        dupLayout.id = dupNode.id;
        dupLayout.layout = {
          ...dupLayout.layout,
          y: nLayout.layout.y + nLayout.layout.h,
        };

        (function pushOtherDown() {
          draft.layouts[bp] = layout.map((i) => {
            if (
              i.layout.y > nLayout.layout.y &&
              i.layout.x < nLayout.layout.x + nLayout.layout.w
            ) {
              console.info('push layout down, item', i);
              return {
                ...i,
                layout: { ...i.layout, y: dupLayout.layout.y + 1 },
              };
            }
            return i;
          });
        })();

        if (dupNode.chartLibId === 'container') {
          let childNodeIds = dupNode.customAttributes.nodeLayouts.map(
            (i) => i.id,
          );

          let dupChildNodes = state.nodes
            .filter((i) => childNodeIds.indexOf(i.id) > -1)
            .map((i) => ({ ...i, id: nanoid(), originId: i.id }));

          dupNode.customAttributes.nodeLayouts = dupNode.customAttributes.nodeLayouts.map(
            (i) => ({
              ...i,
              id: dupChildNodes.find((i1) => i1.originId === i.id).id,
            }),
          );
          draft.nodes.push(...dupChildNodes);
        }

        draft.layouts[bp].push(dupLayout);
        draft.nodes.push(dupNode);
      });
    }
  }

  // console.info('updated state >>', { state, action });

  return state;
}
