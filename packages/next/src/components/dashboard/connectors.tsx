import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { DashboardState } from './type';
import LeaderLine from 'leader-line-new';
import { actions } from './dashboard-redux';

export function NodeConnectors() {
  const { mode } = useSelector((state: DashboardState) => {
    return { mode: state.mode };
  });
  const dispatch = useDispatch();

  // dispatch(
  //   actions.submitDrilldownConnect({
  //     nodeDest: nodeData.id,
  //     nodeSrc: editorMode.activeNode,
  //     eventId: editorMode.eventId,
  //     added: true,
  //   }),
  // );

  useEffect(() => {
    let leaderLines = [];
    let otherDomNodes;
    let hoverLines = {};
    let drawLineFun;
    let removeLineFun;
    let addConnectFun;

    if (mode.type === 'connect') {
      const nodeData = mode.activeNodeData;
      let childList = nodeData.childList || [];

      drawLineFun = function (evt) {
        let id = this.id;
        hoverLines[id] = drawLeaderLineSingle(
          mode.drilldownPointId,
          this,
        );
      };
      removeLineFun = function (evt) {
        let id = this.id;
        hoverLines[id].remove();
        delete hoverLines[id];
      };
      addConnectFun = function (evt) {
        let id = this.id;
        const nodeId = id.replace('chart-container-', '');
        if (childList.find((i) => i.nodeId === nodeId)) {
          dispatch(
            actions.submitDrilldownConnect({
              nodeSrc: mode.activeNode,
              eventId: mode.eventId,
              nodeDest: nodeId,
              added: false,
            }),
          );
        } else {
          dispatch(
            actions.submitDrilldownConnect({
              nodeSrc: mode.activeNode,
              eventId: mode.eventId,
              nodeDest: nodeId,
              added: true,
            }),
          );
        }
      };
      leaderLines = drawLeaderLine(
        nodeData.id,
        childList,
        mode.drilldownPointId,
      );

      otherDomNodes = document.querySelectorAll(
        '.single-chart-container',
      );

      otherDomNodes.forEach((dom: HTMLDivElement) => {
        if (dom.id !== 'chart-container-' + nodeData.id) {
          dom.addEventListener('mouseover', drawLineFun);
          dom.addEventListener('mouseout', removeLineFun);
          dom.addEventListener('click', addConnectFun);
        }
      });
    }

    return () => {
      leaderLines.forEach((i) => {
        i.remove();
      });
      if (otherDomNodes) {
        otherDomNodes.forEach((dom: HTMLDivElement) => {
          dom.removeEventListener('mouseover', drawLineFun);
          dom.removeEventListener('mouseout', removeLineFun);
          dom.removeEventListener('click', addConnectFun);
        });
      }
      Object.values(hoverLines).forEach((line) => line.remove());
    };
  }, [mode]);

  if (mode.type !== 'connect') return null;

  return (
    <React.Fragment>
      <div
        className="alert alert-warning position-fixed"
        role="alert"
        style={{ zIndex: 1300, top: 56, left: 80 }}
      >
        Please choose node will be effected drilldown interaction
        <button
          className="btn btn-sm btn-default"
          onClick={() => {
            dispatch(actions.connectCancel());
          }}
        >
          Done
        </button>
      </div>
    </React.Fragment>
  );
}

function drawLeaderLine(nodeId, childList, drilldownPointId) {
  let leaderLines = [];
  for (let c of childList) {
    leaderLines = leaderLines.concat(
      new LeaderLine(
        document.getElementById(drilldownPointId),
        LeaderLine.pointAnchor(
          document.getElementById('chart-container-' + c.nodeId),
          { x: '50%', y: '50%' },
        ),
        {
          dash: { animation: true },
          path: 'fluid',
        },
      ),
    );
  }
  return leaderLines;
}

function drawLeaderLineSingle(drilldownPointId, dom) {
  return new LeaderLine(
    document.getElementById(drilldownPointId),
    LeaderLine.pointAnchor(dom, { x: '50%', y: '50%' }),
    {
      dash: { animation: true },
      path: 'fluid',
    },
  );
}
