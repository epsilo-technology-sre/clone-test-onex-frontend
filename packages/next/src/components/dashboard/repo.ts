import { Dashboard } from './type';

const DashboardStoreKey = 'epi_dashboard';

const dbVersion = 1;

const simpleEngine = (function () {
  return {
    get(key: string) {
      let str = window.localStorage.getItem(key);
      let data = null;
      try {
        if (str) {
          data = JSON.parse(str);
        }
      } catch (error) {}

      if (data && !data.dbVersion) {
        window.localStorage.removeItem(key);
        data = null;
      }

      return Promise.resolve(data);
    },

    set(key: string, val: any) {
      window.localStorage.setItem(key, JSON.stringify(val));
      return Promise.resolve();
    },
  };
})();

type DashboardDataType = Dashboard & {
  dbVersion: number | string;
  presetEid?: string;
  updatedAt: Date;
};

const defaultLayout = {
  lg: [],
  md: [],
};

class DashboardRepo {
  engine = simpleEngine;
  key: string;
  db: DashboardDataType;
  ready: boolean;

  constructor(engine = simpleEngine, key = DashboardStoreKey) {
    this.engine = engine;
    this.key = key;
    this.ready = false;
  }

  isReady() {
    return this.ready;
  }

  async init(presetId, ready = true) {
    this.db = await this.engine.get(
      [this.key, presetId || ''].join(''),
    );
    console.info('dashboard repo init....', this.db);
    if (!this.db) {
      this.db = {
        layouts: defaultLayout,
        nodes: [],
        dbVersion,
        updatedAt: null,
        presetEid: null,
      };
    }
    this.ready = ready;
    return true;
  }

  setId(presetId) {
    this.db.presetEid = presetId;
  }

  getId() {
    return this.db.presetEid;
  }

  getLayouts() {
    // console.info(this.db.layouts, defaultLayout);
    if (!this.isReady()) return null;

    return {
      lg: this.getResponsiveLayout(
        this.db.layouts || defaultLayout,
        'lg',
      ),
      md: this.getResponsiveLayout(
        this.db.layouts || defaultLayout,
        'md',
      ),
      sm: this.getResponsiveLayout(
        this.db.layouts || defaultLayout,
        'sm',
      ),
      xxs: this.getResponsiveLayout(
        this.db.layouts || defaultLayout,
        'xxs',
      ),
    };
  }

  getNodes() {
    if (!this.isReady()) return null;
    return this.db.nodes || [];
  }

  getDemoMetricRange() {
    if (!this.isReady()) return null;
    return this.db.demoMetricRange || [];
  }
  setDemoMetricRange(value) {
    this.db.demoMetricRange = value;
    return this.persist();
  }

  updateLayouts(layouts: DashboardDataType['layouts']) {
    if (!this.db) return;
    this.db.layouts = layouts;
    return this.persist();
  }

  setNodes(nodes: DashboardDataType['nodes']) {
    if (!this.db) return;
    this.db.nodes = nodes;
    return this.persist();
  }

  getResponsiveLayout(
    layouts: DashboardDataType['layouts'],
    bp: keyof DashboardDataType['layouts'],
  ) {
    if (!layouts[bp]) console.info('missing layouts setup', bp);
    return layouts[bp] ? layouts[bp] : layouts.md ? layouts.md : [];
  }

  async persist() {
    // console.info('persist ===> ', this.db);
    this.db.updatedAt = new Date();
    await this.engine.set(
      [this.key, this.db.presetEid || ''].join(''),
      this.db,
    );
  }
}

let _cached: DashboardRepo;
export const getDashboardRepo = () => {
  if (_cached && _cached instanceof DashboardRepo) return _cached;

  console.info({ _cached });
  _cached = new DashboardRepo();
  return _cached;
};
