import clsx from 'clsx';
import React from 'react';
import {
  BsPlusCircle,

  BsThreeDotsVertical
} from 'react-icons/bs';
import { useDispatch, useSelector } from 'react-redux';
import {
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle
} from 'reactstrap';
import styled from 'styled-components';
import { DashboardContext, DashboardItemContext } from './context';
import { actions } from './dashboard-redux';
import { NodeFactory } from './node/factory';
import { DashboardState, NodeData } from './type';

const StyledCard = styled('div')`
  position: relative;
  display: flex;
  background: rgba(255, 255, 255, 0.79);
  &.drag-enabled {
    outline: 1px solid rgba(237, 92, 16, 1) !important;
    z-index: 10;
  }

  &.selected {
    outline: 1px solid rgba(237, 92, 16, 1) !important;
    z-index: 10;
  }

  &:hover {
    outline: 1px solid rgba(37, 55, 70, 0.39);
    & .controls {
      visibility: visible;
    }
  }

  .dashboard-item-card {
    overflow: hidden;
    padding: 0;
    width: 100%;
    height: 100%;
    flex: 1 1 auto;
  }
  .dashboard-item-card.center {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .controls {
    display: flex;
    align-items: center;
    visibility: hidden;
    position: absolute;
    top: 0;
    right: 0;
    z-index: 1;
  }
  .controls-dropdown {
    z-index: 1000;
  }

  .btn-control {
    color: var(--secondary-21);
    &:hover {
      color: var(--primary);
    }
  }
  .btn-add-chart {
    color: var(--secondary-21);
    cursor: pointer;
    &:hover {
      color: var(--secondary);
    }
  }
  .card-body.center {
    display: flex;
    align-items: center;
    justify-content: center;
  }
  .connection {
  }
`;

const DashboardItem = React.forwardRef(
  (
    {
      nodeData,
      onToggleFocus = Function.prototype,
    }: { nodeData: NodeData; onToggleFocus?: () => void },
    ref,
  ) => {
    const [menuOpen, setMenuOpen] = React.useState(false);
    const [isDraggable, setDraggable] = React.useState(true);

    const { isSelect, isConnectSource, editorMode } = useSelector(
      (state: DashboardState) => {
        const nodeId = nodeData.id;
        const mode: DashboardState['mode'] = state.mode || {
          type: null,
          activeNode: null,
        };
        return {
          isSelect:
            mode.type === 'select' && mode.activeNode === nodeId,
          isConnectSource:
            mode.type === 'connect' && mode.activeNode === nodeId,
          editorMode: mode,
        };
      },
    );
    let isFocus = isSelect || isConnectSource;
    const dispatch = useDispatch();

    const dbContext = React.useContext(DashboardContext);
    const itemRef = React.useRef();
    React.useEffect(() => {
      try {
        if (isFocus && itemRef.current) {
          itemRef.current.parentElement.classList.add('js-active');
        } else if (!isFocus && itemRef.current) {
          itemRef.current.parentElement.classList.remove('js-active');
        }
      } catch (err) {}
    }, [isFocus]);

    const setFocus = (nodeId) => {
      dispatch(actions.select({ nodeId }));
    };

    const unGroup = (nodeId) => {
      dispatch(actions.unGroupNodes({ nodeId }));
    };

    const renderAddChartBtn = (
      <BsPlusCircle
        fontSize="39"
        className="btn-add-chart"
        onClick={() =>
          dbContext.onNodeRequestUpdate(nodeData.id, nodeData)
        }
      />
    );

    const handleToggle = (...args) => {
      onToggleFocus(!menuOpen);
      setMenuOpen(!menuOpen);
    };

    return (
      <DashboardItemContext.Provider
        value={{
          enableRootDraggable: (status) => {
            setDraggable(status);
          },
        }}
      >
        <StyledCard
          style={{
            height: '100%',
            width: '100%',
          }}
          onClick={() => {
            if (editorMode.type !== 'connect') {
              setFocus(nodeData.id);
            } else if (!isConnectSource) {
            }
          }}
          ref={itemRef}
          className={clsx(
            isFocus && isDraggable && 'drag-enabled',
            isConnectSource && 'connect-source',
            editorMode.type === 'select_multiple' &&
              editorMode.activeNodes.indexOf(nodeData.id) > -1 &&
              'selected',
          )}
        >
          <div
            ref={(dom) => {
              if (dom && ref) ref.current = dom.parentElement;
            }}
          ></div>
          <div className="controls">
            <Dropdown toggle={handleToggle} isOpen={menuOpen}>
              <DropdownToggle
                color=""
                className="bg-transparent btn-sm border-0 p-50"
              >
                <BsThreeDotsVertical
                  size={12}
                  className="cursor-pointer btn-control"
                />
              </DropdownToggle>
              <DropdownMenu className="controls-dropdown">
                {nodeData.chartLibId === 'container' ? (
                  <DropdownItem
                    className="w-100"
                    onClick={() => {
                      unGroup(nodeData.id);
                    }}
                  >
                    Ungroup
                  </DropdownItem>
                ) : (
                  <DropdownItem
                    className="w-100"
                    onClick={() => {
                      dbContext.onNodeRequestUpdate(
                        nodeData.id,
                        nodeData,
                      );
                    }}
                  >
                    Edit
                  </DropdownItem>
                )}

                <DropdownItem
                  className="w-100"
                  onClick={() => {
                    dbContext.onRemoveNode(nodeData.id);
                  }}
                >
                  Remove
                </DropdownItem>
                <DropdownItem
                  className="w-100"
                  onClick={() => {
                    dbContext.onDuplicateNode(nodeData.id);
                  }}
                >
                  Duplicate
                </DropdownItem>
              </DropdownMenu>
            </Dropdown>
          </div>
          <div
            className={clsx(
              !nodeData.chartLibId && 'center',
              'dashboard-item-card',
            )}
          >
            {nodeData.chartLibId ? (
              <NodeFactory data={nodeData} />
            ) : (
              renderAddChartBtn
            )}
          </div>
        </StyledCard>
      </DashboardItemContext.Provider>
    );
  },
);

export default DashboardItem;
