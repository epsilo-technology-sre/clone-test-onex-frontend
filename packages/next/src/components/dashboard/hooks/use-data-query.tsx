import { useFilter } from '@epi/next/src/components/filter/hooks';
import { get } from 'lodash';
import moment from 'moment';
import React from 'react';
import { calculateMetric } from '../../../../int/sea/execution';
import { useLog } from '../../../../lib/log';
import { dataQuery, translateCohort } from '../../../util/data-query';
import { formatNumber } from '@ep/one/src/utils/currency';
import { CHART_TYPE } from '../node/chartlibs';
import type { demoMetricRangeType, NodeData } from '../type';

const log = useLog('hooks:useDataQuery');

export const QUERY_STATUS = {
  SUCCESS: 'success',
  FAIL: 'fail',
  LOADING: 'loading',
};

export const CHART_DATA_TYPE = {
  NUMBER: 'number',
  PERCENT: 'percent',
  CURRENCY: 'currency',
  FLOAT: 'float',
};

export const DataQueryContext = React.createContext<{
  demoMetricRange: demoMetricRangeType[];
}>({
  demoMetricRange: [],
});

export const useDataQuery = (nodeData: NodeData) => {
  const dataQueryContext = React.useContext(DataQueryContext);
  const { availFilters } = useFilter({});
  console.log('*******  DQContext:', dataQueryContext);

  const [queries, setQueries] = React.useState(getInitialData());

  const requestData = React.useMemo(() => {
    return getAsyncData(getRandomMetricValue);
  }, []);

  React.useEffect(() => {
    requestData(nodeData, availFilters.metrics).then((data) =>
      setQueries(data),
    );
  }, [availFilters.metrics, requestData]);

  const setStatus = (index, value) => {
    setQueries((oldQueries) => {
      oldQueries[index].status = value;
      return [...oldQueries];
    });
  };

  const setData = (index, data) => {
    setQueries((oldQueries) => {
      oldQueries[index].data = data;
      return [...oldQueries];
    });
  };

  const reloadData = (index) => {
    setStatus(index, QUERY_STATUS.LOADING);

    const metrics = get(nodeData, 'mainAttributes.metrics', []).slice(
      0,
      1,
    )[0];
    const cohort = get(nodeData, 'mainAttributes.cohort', ['mtd'])[0];
    const dimensions = get(nodeData, 'mainAttributes.dimensions', {});
    const { current, previous } = translateCohort(cohort);

    switch (index) {
      case 0:
        dataQuery({
          metrics: metrics,
          dateRange: current,
          dimensions: dimensions,
        })
          .then((data) => {
            setStatus(index, QUERY_STATUS.SUCCESS);
            setData(index, data);
          })
          .catch((err) => {
            setStatus(index, QUERY_STATUS.FAIL);
            setData(index, null);
          });
        break;
      case 1:
        dataQuery({
          metrics: metrics,
          dateRange: previous,
          dimensions: dimensions,
        })
          .then((data) => {
            setStatus(index, QUERY_STATUS.SUCCESS);
            setData(index, data);
          })
          .catch((err) => {
            setStatus(index, QUERY_STATUS.FAIL);
            setData(index, null);
          });
        break;
      case 2:
        dataQuery({
          metrics: metrics,
          dateRange: current,
          dimensions: dimensions,
          isSummary: true,
        })
          .then((data) => {
            setStatus(index, QUERY_STATUS.SUCCESS);
            setData(index, data);
          })
          .catch((err) => {
            setStatus(index, QUERY_STATUS.FAIL);
            setData(index, null);
          });
        break;
      case 3:
        dataQuery({
          metrics: metrics,
          dateRange: previous,
          dimensions: dimensions,
          isSummary: true,
        })
          .then((data) => {
            setStatus(index, QUERY_STATUS.SUCCESS);
            setData(index, data);
          })
          .catch((err) => {
            setStatus(index, QUERY_STATUS.FAIL);
            setData(index, null);
          });
    }
  };

  function getInitialData() {
    // Get metric range from local storage
    let initialQueries = [];
    switch (nodeData.chartLibId) {
      case CHART_TYPE.METRIC_DETAIL:
        initialQueries = initMetricDetailData();
        break;
      case CHART_TYPE.METRIC_TOTAL:
        initialQueries = initMetricTotalData();
        break;
      case CHART_TYPE.TABLE:
        initialQueries = initTableData();
        break;
      default:
    }
    return initialQueries;
  }

  function initMetricDetailData() {
    return [...Array(4)].map((val, index) => ({
      status: '',
      data: {
        headers: [],
        rows: [],
      },
    }));
  }

  function getRandomMetricValue(metricType) {
    let metricRange = dataQueryContext.demoMetricRange.find(
      (i) => i.metric === metricType,
    );

    if (!metricRange) {
      metricRange = {
        type: CHART_DATA_TYPE.NUMBER,
        from: 10,
        to: 1000,
      };
    }

    return randomDataValue(
      metricRange.type,
      metricRange.from,
      metricRange.to,
    );
  }

  function initMetricTotalData() {
    const metricType = get(
      nodeData.mainAttributes,
      'metrics[0]',
      'GMV',
    );

    let metricRange = dataQueryContext.demoMetricRange.find(
      (i) => i.metric === metricType,
    );
    if (!metricRange) {
      metricRange = {
        type: CHART_DATA_TYPE.NUMBER,
        from: 100,
        to: 10000,
      };
    }

    const totalNumber = randomDataValue(
      metricRange.type,
      metricRange.from,
      metricRange.to,
    );
    const date = moment().date();
    const today = moment();
    const currentMonthData = [...Array(today.daysInMonth())].map(
      (val, i) => ({
        date: new Date(today.year(), today.month(), i + 1),
        value: randomDataValue(metricRange.type, 0, totalNumber / 2),
      }),
    );

    const previousMonth = moment().subtract(1, 'month');
    const previousMonthData = [
      ...Array(previousMonth.daysInMonth()),
    ].map((val, i) => ({
      date: new Date(
        previousMonth.year(),
        previousMonth.month(),
        i + 1,
      ),
      value: randomDataValue(metricRange.type, 0, totalNumber / 2),
    }));

    const percent = randomDataValue(CHART_DATA_TYPE.PERCENT, 1, 200);

    const queryData = [
      {
        status: '',
        data: {
          headers: ['currency', 'amount'],
          rows: ['USD', totalNumber],
        },
      },
      {
        status: '',
        data: {
          headers: ['percent'],
          rows: [percent],
        },
      },
      {
        status: '',
        data: {
          headers: ['chart_labels'],
          rows: getDaysOfMonth('DD MMM, YYYY'),
        },
      },
      {
        status: '',
        data: {
          headers: ['last_month'],
          rows: previousMonthData,
        },
      },
      {
        status: '',
        data: {
          headers: ['current_month'],
          rows: currentMonthData.map((item, index) =>
            index <= date ? item : { ...item, value: undefined },
          ),
        },
      },
      {
        status: '',
        data: {
          headers: ['predict_current_month'],
          rows: currentMonthData.map((item, index) =>
            index >= date ? item : { ...item, value: undefined },
          ),
        },
      },
    ];

    return queryData;
  }

  function initTableData() {
    const headerIds = availFilters.metrics.map((i) => i.value);

    const tableRows = [...Array(100)].map((val, i) => {
      const row = {
        shopEid: i,
        shop: 'Shop ' + i,
      };

      headerIds.forEach((id, index) => {
        row[id] = getRandomMetricValue(id);
      });
      return row;
    });

    const queryData = [
      {
        status: '',
        data: {
          headers: ['shop', ...headerIds],
          rows: tableRows,
        },
      },
      {
        status: '',
        data: {
          headers: ['pagination'],
          rows: [
            {
              page: 1,
              itemCount: 0,
              limit: 10,
            },
          ],
        },
      },
    ];

    console.log('TABLE QUERIES ===> ', queryData);
    return queryData;
  }

  return {
    queries,
    reloadData,
  };
};

const randomDataValue = (type, from, to) => {
  const numFrom = parseFloat(from);
  const numTo = parseFloat(to);
  switch (type) {
    case CHART_DATA_TYPE.CURRENCY:
      return Math.random() * (numTo - numFrom) + numFrom;
    case CHART_DATA_TYPE.NUMBER:
      return Math.floor(Math.random() * (numTo - numFrom) + numFrom);
    case CHART_DATA_TYPE.FLOAT:
      return (
        parseFloat((Math.random() * (numTo - numFrom)).toFixed(2)) +
        numFrom
      );
    case CHART_DATA_TYPE.PERCENT:
      return (
        parseFloat((Math.random() * (numTo - numFrom)).toFixed(2)) +
        numFrom
      );
    default:
      return 0;
  }
};

const getDaysOfMonth = (dateFormat: string = 'DD/MM/YYYY') => {
  const days = [];
  const firstDay = moment().startOf('month');
  const daysInMonth = moment().daysInMonth();
  for (let i = 0; i < daysInMonth; i++) {
    days.push(firstDay.clone().add(i, 'days').format(dateFormat));
  }
  return days;
};

function getAsyncData(metricRequest) {
  return (nodeData: NodeData, allMetrics): Promise<any> => {
    log('getAsyncData', nodeData);
    let initialQueries = Promise.resolve(null);
    switch (nodeData.chartLibId) {
      case CHART_TYPE.METRIC_DETAIL:
        initialQueries = Promise.resolve(initMetricDetailData());
        break;
      case CHART_TYPE.METRIC_TOTAL:
        initialQueries = Promise.resolve(
          initMetricTotalData(nodeData, allMetrics),
        );
        break;
      case CHART_TYPE.TABLE:
        initialQueries = Promise.resolve(
          getTableData(allMetrics, metricRequest),
        );
        break;
      default:
    }
    return initialQueries;
  };
}

function initMetricTotalData(nodeData, allMetrics) {
  const metricType = get(
    nodeData.mainAttributes,
    'metrics[0]',
    'GMV',
  );

  let metricRange = allMetrics.find(
    (i) =>
      String(i.value).toUpperCase() ===
      String(metricType).toUpperCase(),
  );

  if (!metricRange) {
    metricRange = {
      type: CHART_DATA_TYPE.NUMBER,
      from: 100,
      to: 10000,
    };
  }

  const totalNumber = randomDataValue(
    metricRange.type,
    metricRange.from,
    metricRange.to,
  );
  const date = moment().date();
  const today = moment();
  const currentMonthData = [...Array(today.daysInMonth())].map(
    (val, i) => ({
      date: new Date(today.year(), today.month(), i + 1),
      value: randomDataValue(metricRange.type, 0, totalNumber / 2),
    }),
  );

  const previousMonth = moment().subtract(1, 'month');
  const previousMonthData = [
    ...Array(previousMonth.daysInMonth()),
  ].map((val, i) => ({
    date: new Date(
      previousMonth.year(),
      previousMonth.month(),
      i + 1,
    ),
    value: randomDataValue(metricRange.type, 0, totalNumber / 2),
  }));

  const percent = randomDataValue(CHART_DATA_TYPE.PERCENT, 1, 200);

  const queryData = [
    {
      status: '',
      data: {
        headers: ['currency', 'amount'],
        rows: ['USD', totalNumber],
      },
    },
    {
      status: '',
      data: {
        headers: ['percent'],
        rows: [percent],
      },
    },
    {
      status: '',
      data: {
        headers: ['chart_labels'],
        rows: getDaysOfMonth('DD MMM, YYYY'),
      },
    },
    {
      status: '',
      data: {
        headers: ['last_month'],
        rows: previousMonthData,
      },
    },
    {
      status: '',
      data: {
        headers: ['current_month'],
        rows: currentMonthData.map((item, index) =>
          index <= date ? item : { ...item, value: undefined },
        ),
      },
    },
    {
      status: '',
      data: {
        headers: ['predict_current_month'],
        rows: currentMonthData.map((item, index) =>
          index >= date ? item : { ...item, value: undefined },
        ),
      },
    },
  ];

  return queryData;
}

function isCustomMetric(metricId) {
  return metricId.indexOf('EM_') === 0;
}

async function getTableData(allMetrics, metricRequest) {
  const headerIds = allMetrics.map((i) => i.value);

  const allCustomMetrics = allMetrics.reduce((accum, i) => {
    if (isCustomMetric(i.value)) {
      return { ...accum, [i.value]: i.origin };
    }
    return accum;
  }, {});

  const tableRows = [...Array(100)].map((val, i) => {
    const row = {
      shopEid: i,
      shop: 'Shop ' + i,
    };

    headerIds
      .filter((i) => !isCustomMetric(i))
      .forEach((id, index) => {
        row[id] = metricRequest(id);
      });

    headerIds
      .filter((i) => isCustomMetric(i))
      .forEach((id, index) => {
        let val = calculateMetric(allCustomMetrics[id])(
          (metricId) => {
            return row[metricId.replace('@', '')];
          },
          () => {
            throw new Error('unimplemented yet!');
          },
        );

        row[id] = isNaN(val) ? val : formatNumber(val);
      });

    return row;
  });

  const queryData = [
    {
      status: '',
      data: {
        headers: ['shop', ...headerIds],
        rows: tableRows,
      },
    },
    {
      status: '',
      data: {
        headers: ['pagination'],
        rows: [
          {
            page: 1,
            itemCount: 0,
            limit: 10,
          },
        ],
      },
    },
  ];

  console.log('TABLE QUERIES ===> ', queryData);
  return queryData;
}

function initMetricDetailData() {
  return [...Array(4)].map((val, index) => ({
    status: '',
    data: {
      headers: [],
      rows: [],
    },
  }));
}
