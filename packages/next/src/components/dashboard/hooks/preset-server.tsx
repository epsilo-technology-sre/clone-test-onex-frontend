import { request } from '@ep/one/src/utils';
import { Dashboard } from '../type';

export function usePresetServer(serverURL) {
  function getList() {
    return request
      .get(`${serverURL}/api/insight/presets`)
      .then((res) => {
        let list = [];
        list = res.data.map((i) => {
          let preset: any = {};
          let payload =
            typeof i.payload === 'string'
              ? JSON.parse(i.payload)
              : i.payload;
          preset.layouts = payload.layouts;
          preset.nodes = payload.nodes;
          preset.demoMetricRange = payload.demoMetricRange;
          preset.updatedAt = i.updated_at;
          preset.dbVersion = i.payload_schema_version;
          preset.presetEid = i.preset_eid;
          preset.title = i.title;
          return preset;
        });

        console.info({ getList: list });
        return list;
      })
      .catch((err) => {
        return [];
      });
  }

  function get() {
    return request
      .get(`${serverURL}/api/insight/presets`)
      .then((res) => {
        let preset = null;
        if (res.data.length > 0) {
          preset = res.data[0];
          let payload =
            typeof preset.payload === 'string'
              ? JSON.parse(preset.payload)
              : preset.payload;
          preset.layouts = payload.layouts;
          preset.nodes = payload.nodes;
          preset.demoMetricRange = payload.demoMetricRange;
          preset.updatedAt = preset.updated_at;
          preset.dbVersion = preset.payload_schema_version;
          preset.presetEid = preset.preset_eid;
          preset.title = preset.title;
        }

        return preset;
      })
      .catch((err) => {
        return null;
      });
  }

  function getSingle(presetEid) {
    return request
      .get(`${serverURL}/api/insight/presets/${presetEid}`)
      .then((res) => {
        let preset = null;
        if (res.data) {
          preset = res.data;
          let payload =
            typeof preset.payload === 'string'
              ? JSON.parse(preset.payload)
              : preset.payload;
          preset.layouts = payload.layouts;
          preset.nodes = payload.nodes;
          preset.demoMetricRange = payload.demoMetricRange;
          preset.updatedAt = preset.updated_at;
          preset.dbVersion = preset.payload_schema_version;
          preset.presetEid = preset.preset_eid;
          preset.title = preset.title;
        }

        return preset;
      })
      .catch((err) => {
        return null;
      });
  }

  function update(presetId, payload: Dashboard): Promise<Dashboard> {
    return request
      .post(`${serverURL}/api/insight/presets`, {
        title: payload.title,
        payload: JSON.stringify({
          layouts: payload.layouts,
          nodes: payload.nodes,
          demoMetricRange: payload.demoMetricRange,
        }),
        preset_eid: presetId,
      })
      .then((res) => {
        let preset = res.data;
        preset = res.data;
        let payload =
          typeof preset.payload === 'string'
            ? JSON.parse(preset.payload)
            : preset.payload;
        preset.layouts = payload.layouts;
        preset.nodes = payload.nodes;
        preset.updatedAt = preset.updated_at;
        preset.dbVersion = preset.payload_schema_version;
        preset.presetEid = preset.preset_eid;
        preset.title = preset.title;
        return preset;
      });
  }

  return {
    get,
    getList,
    getSingle,
    update,
  };
}
