import React, { useState, useMemo, useRef, useEffect } from 'react';
import {
  Slate,
  Editable,
  ReactEditor,
  withReact,
  useSlate,
} from 'slate-react';
import {
  Editor,
  Transforms,
  Text,
  createEditor,
  Node,
  Element as SlateElement,
  Descendant,
} from 'slate';
import { css } from 'emotion';
import {
  BsTypeBold,
  BsTypeItalic,
  BsTypeUnderline,
  BsTypeH1,
  BsTypeH2,
  BsTypeH3,
  BsTextLeft,
  BsTextCenter,
  BsTextRight,
  BsHr,
} from 'react-icons/bs';
import {
  AiOutlineVerticalAlignBottom as VerticalBottom,
  AiOutlineVerticalAlignMiddle as VerticalMiddle,
  AiOutlineVerticalAlignTop as VerticalTop,
} from 'react-icons/ai';
import { makeStyles } from '@material-ui/core';

import { Button, Icon, Menu, Portal } from './components';
import { Range } from 'slate';
import clsx from 'clsx';

const useStyle = makeStyles(() => ({
  textContainer: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    '& > div': {
      display: 'flex',
    },
    '& div:last-child': {
      flexGrow: 1,
    },
    '&:focus': {
      outline: '1px solid rgba(37, 55, 70, 0.39)',
    },
    '&.readonly:hover': {
      cursor: 'pointer',
      outline: '1px solid rgba(37, 55, 70, 0.39) !important',
    }
  },
}));

const PopoverEditor = ({
  onTextChange,
  defaultValue = initialValue,
  onFocus = (evt) => {},
  onBlur = (evt) => {},
}) => {
  const [value, setValue] = useState<Descendant[]>(defaultValue);
  const [allowEdit, setAllowEdit] = useState(false);
  const classes = useStyle();
  const renderElement = React.useCallback(
    (props) => <Element {...props} />,
    [],
  );
  const prev = useRef(value);
  const editor = useMemo(() => withReact(createEditor()), []);

  React.useEffect(() => {
    setValue(defaultValue);
  }, [defaultValue]);

  return (
    <Slate
      editor={editor}
      value={value}
      onChange={(value) => {
        setValue(value);
        if (JSON.stringify(value) !== JSON.stringify(prev.current)) {
          onTextChange(value);
          prev.current = value;
        }
      }}
    >
      <HoveringToolbar />
      <Editable
        readOnly={!allowEdit}
        className={clsx(classes.textContainer, !allowEdit && 'readonly')}
        renderElement={renderElement}
        renderLeaf={(props) => <Leaf {...props} />}
        placeholder="Enter some text..."
        onFocus={onFocus}
        onBlur={(evt) => {onBlur(evt); setAllowEdit(false);}}
        onDoubleClick={() => {
          if(!allowEdit){
            setAllowEdit(true);
          }
        }}
        onKeyDown={(evt) => {
          if(evt.key === 'Escape'){
            setAllowEdit(false);
            onBlur(evt);
          }
        }}
        onDOMBeforeInput={(event: InputEvent) => {
          switch (event.inputType) {
            case 'formatBold':
              event.preventDefault();
              return toggleFormat(editor, 'bold');
            case 'formatItalic':
              event.preventDefault();
              return toggleFormat(editor, 'italic');
            case 'formatUnderline':
              event.preventDefault();
              return toggleFormat(editor, 'underline');
          }
        }}
      />
    </Slate>
  );
};

const toggleFormat = (editor, format) => {
  const isActive = isFormatActive(editor, format);
  Transforms.setNodes(
    editor,
    { [format]: isActive ? null : true },
    { match: Text.isText, split: true },
  );
};

const isFormatActive = (editor, format) => {
  const [match] = Editor.nodes(editor, {
    match: (n) => n[format] === true,
    mode: 'all',
  });
  return !!match;
};

const isBlockActive = (editor, format) => {
  const [match] = Editor.nodes(editor, {
    match: (n) =>
      !Editor.isEditor(n) &&
      SlateElement.isElement(n) &&
      n.type === format,
  });

  return !!match;
};

const toggleBlock = (editor, format) => {
  const isActive = isBlockActive(editor, format);

  const newProperties: Partial<SlateElement> = {
    type: isActive ? 'paragraph' : format,
  };
  Transforms.setNodes(editor, newProperties);
};

const isAlignActive = (editor, format) => {
  const [match] = Editor.nodes(editor, {
    match: (n) =>
      !Editor.isEditor(n) &&
      SlateElement.isElement(n) &&
      n.align === format,
  });

  return !!match;
};
const toggleAlign = (editor, format) => {
  const isActive = isAlignActive(editor, format);
  const newProperties: Partial<SlateElement> = {
    align: isActive ? 'left' : format,
  };
  Transforms.setNodes(editor, newProperties);
};

const isVerticalAlignActive = (editor, format) => {
  const [match] = Editor.nodes(editor, {
    match: (n) =>
      !Editor.isEditor(n) &&
      SlateElement.isElement(n) &&
      n.verticalAlign === format,
  });

  return !!match;
};
const toggleVerticalAlign = (editor, format) => {
  const isActive = isAlignActive(editor, format);
  const newProperties: Partial<SlateElement> = {
    verticalAlign: isActive ? 'left' : format,
  };
  Transforms.setNodes(editor, newProperties);
};

const verticalAlignMapping = {
  top: 'flex-start',
  middle: 'center',
  bottom: 'flex-end',
};
const Element = ({ attributes, children, element }) => {
  // console.info({ element });
  let style = {};
  if (element.align) {
    style = { ...style, justifyContent: element.align };
  }
  if (element.verticalAlign) {
    style = {
      ...style,
      alignItems: verticalAlignMapping[element.verticalAlign],
    };
  }

  switch (element.type) {
    case 'heading-one':
      return (
        <div {...attributes} style={style}>
          <h1>{children}</h1>
        </div>
      );
    case 'heading-two':
      return (
        <div {...attributes} style={style}>
          <h2>{children}</h2>
        </div>
      );
    case 'heading-three':
      return (
        <div {...attributes} style={style}>
          <h3>{children}</h3>
        </div>
      );
    case 'hr': {
      return <hr {...attributes} style={style} />;
    }
    default:
      return (
        <div {...attributes} style={style}>
          {children}
        </div>
      );
  }
};

const Leaf = ({ attributes, children, leaf }) => {
  if (leaf.bold) {
    children = <strong>{children}</strong>;
  }

  if (leaf.italic) {
    children = <em>{children}</em>;
  }

  if (leaf.underlined) {
    children = <u>{children}</u>;
  }

  return <span {...attributes}>{children}</span>;
};

const HoveringToolbar = () => {
  const ref = useRef<HTMLDivElement | null>();
  const editor = useSlate();

  useEffect(() => {
    const el = ref.current;
    const { selection } = editor;

    if (!el) {
      return;
    }

    if (
      !selection ||
      !ReactEditor.isFocused(editor) ||
      ReactEditor.isReadOnly(editor) ||
      Range.isCollapsed(selection) ||
      Editor.string(editor, selection) === ''
    ) {
      el.removeAttribute('style');
      return;
    }

    const domSelection = window.getSelection();
    const domRange = domSelection.getRangeAt(0);
    const rect = domRange.getBoundingClientRect();
    el.style.opacity = '1';
    el.style.top = `${
      rect.top + window.pageYOffset - el.offsetHeight
    }px`;

    const leftBound = Math.max(
      10,
      rect.left +
        window.pageXOffset -
        el.offsetWidth / 2 +
        rect.width / 2,
    );

    el.style.left = `${leftBound}px`;
  });

  return (
    <Portal>
      <Menu
        ref={ref}
        className={css`
          padding: 8px 7px 6px;
          position: absolute;
          z-index: 1201;
          top: -10000px;
          left: -10000px;
          margin-top: -6px;
          opacity: 0;
          background-color: #222;
          border-radius: 4px;
          transition: opacity 0.75s;
        `}
      >
        <BlockButton format="heading-one" icon={<BsTypeH1 />} />
        <BlockButton format="heading-two" icon={<BsTypeH2 />} />
        <BlockButton format="heading-three" icon={<BsTypeH3 />} />
        <span>&nbsp;</span>
        <FormatButton format="bold" icon={<BsTypeBold />} />
        <FormatButton format="italic" icon={<BsTypeItalic />} />
        <FormatButton
          format="underlined"
          icon={<BsTypeUnderline />}
        />
        <span>&nbsp;</span>
        <AlignButton format="left" icon={<BsTextLeft />} />
        <AlignButton format="center" icon={<BsTextCenter />} />
        <AlignButton format="right" icon={<BsTextRight />} />
        <VAlignButton format="top" icon={<VerticalTop />} />
        <VAlignButton format="middle" icon={<VerticalMiddle />} />
        <VAlignButton format="bottom" icon={<VerticalBottom />} />
        <span>&nbsp;</span>
        {/* <Button
          reverse
          onClick={() => {
            editor.insertNode({
              type: 'hr',
              children: [{ text: '' }],
            });
          }}
        >
          <Icon>
            <BsHr />
          </Icon>
        </Button> */}
      </Menu>
    </Portal>
  );
};

const FormatButton = ({ format, icon }) => {
  const editor = useSlate();
  return (
    <Button
      reversed
      active={isFormatActive(editor, format)}
      onMouseDown={(event) => {
        event.preventDefault();
        toggleFormat(editor, format);
      }}
    >
      <Icon>{icon}</Icon>
    </Button>
  );
};

const BlockButton = ({ format, icon }) => {
  const editor = useSlate();
  return (
    <Button
      reversed
      active={isBlockActive(editor, format)}
      onMouseDown={(event) => {
        event.preventDefault();
        toggleBlock(editor, format);
      }}
    >
      <Icon>{icon}</Icon>
    </Button>
  );
};

const AlignButton = ({ format, icon }) => {
  const editor = useSlate();
  return (
    <Button
      reversed
      active={isAlignActive(editor, format)}
      onMouseDown={(event) => {
        event.preventDefault();
        toggleAlign(editor, format);
      }}
    >
      <Icon>{icon}</Icon>
    </Button>
  );
};

const VAlignButton = ({ format, icon }) => {
  const editor = useSlate();
  return (
    <Button
      reversed
      active={isVerticalAlignActive(editor, format)}
      onMouseDown={(event) => {
        event.preventDefault();
        toggleVerticalAlign(editor, format);
      }}
    >
      <Icon>{icon}</Icon>
    </Button>
  );
};

const initialValue: SlateElement[] = [
  {
    type: 'paragraph',
    children: [
      {
        text: 'editable',
      },
    ],
  },
];

export default PopoverEditor;
