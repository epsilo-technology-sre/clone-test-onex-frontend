import type { NodeData } from '../type';
import { get, isPlainObject } from 'lodash';
import escapeHtml from 'escape-html';

const isInEditingMode = true;

export function getCustomText(
  nodeData: NodeData,
  contentId: string,
  defaults: string,
) {
  let customText = get(
    nodeData,
    `customAttributes.customText.${contentId}`,
    null,
  );
  const finText = customText
    ? serialize({ children: customText })
    : defaults;

  return finText;
}

function isText(value) {
  return isPlainObject(value) && typeof value.text === 'string';
}

const serialize = (node) => {
  if (isText(node)) {
    let text = escapeHtml(node.text);
    if (node.bold) {
      text = `<strong>${text}</strong>`;
    }
    if (node.italic) {
      text = `<em>${text}</em>`;
    }
    if (node.underlined) {
      text = `<u>${text}</u>`;
    }
    return text;
  }

  const children = node.children.map((n) => serialize(n)).join('');

  switch (node.type) {
    case 'paragraph':
      return `<span class="dbf-text-editable">${children}</span>`;
    default:
      return children;
  }
};
