import { CurrencyCode } from '@ep/one/src/components/common/currency-code/currency-code';
import {
  QUERY_STATUS,
  useDataQuery,
} from '@epi/next/src/components/dashboard/hooks';
import {
  ChartLibComponent,
  NodeData,
} from '@epi/next/src/components/dashboard/type';
import { ChartLoading } from '@epi/next/src/components/loading/loading';
import { RELOAD_TIME } from '@epi/next/src/constant';
import MetricBg from '@epi/next/src/images/metric-detail-bg.svg';
import {
  formatCurrency,
  formatPercent,
} from '@epi/next/src/util/utils';
import { EventEmitter } from 'events';
import { get } from 'lodash';
import moment from 'moment';
import React from 'react';
import { Line } from 'react-chartjs-2';
import ReactDOM from 'react-dom';
import {
  BsArrowDown,
  BsArrowRepeat,
  BsArrowUp,
} from 'react-icons/bs';
import { translateCohort } from '../../../../../util/data-query';
import './metric-detail.scss';

const randomDataValue = () => {
  return Math.floor(Math.random() * 100);
};

const getDaysOfMonth = (dateFormat: string = 'DD/MM/YYYY') => {
  const days = [];
  const firstDay = moment().startOf('month');
  const daysInMonth = moment().daysInMonth();
  for (let i = 0; i < daysInMonth; i++) {
    days.push(firstDay.clone().add(i, 'days').format(dateFormat));
  }
  return days;
};

const getCharData = () => {
  const date = moment().date();
  const currentMonthData = [
    ...Array(moment().daysInMonth()),
  ].map((i) => randomDataValue());

  const previousMonthData = [
    ...Array(moment().add(-1, 'month').daysInMonth()),
  ].map((i) => randomDataValue());

  const labels = getDaysOfMonth('DD MMM, YYYY');
  return {
    labels: labels,
    datasets: [
      {
        label: moment().format('MMMM'),
        fill: false,
        backgroundColor: '#204D77',
        borderColor: '#204D77',
        borderWidth: 2,
        pointRadius: 0,
        pointHoverRadius: 2,
        data: currentMonthData.map((item, index) =>
          index <= date ? item : undefined,
        ),
      },
      {
        label: moment().format('MMMM'),
        fill: false,
        backgroundColor: '#204D77',
        borderColor: '#204D77',
        borderWidth: 2,
        pointRadius: 0,
        pointHoverRadius: 2,
        borderDash: [3, 3],
        data: currentMonthData.map((item, index) =>
          index >= date ? item : undefined,
        ),
      },
      {
        label: moment().add(-1, 'month').format('MMMM'),
        fill: false,
        backgroundColor: '#D4DDED',
        borderColor: '#D4DDED',
        borderWidth: 2,
        pointRadius: 0,
        pointHoverRadius: 2,
        data: previousMonthData,
      },
    ],
  };
};

export default class MetricDetailChartLib
  implements ChartLibComponent {
  getInteractionEvents() {
    return [
      {
        eventId: 'clickDataPoint',
        label: 'Click data point',
      },
    ];
  }

  render(
    dom: HTMLDivElement,
    data: NodeData,
    eventBus: EventEmitter,
  ) {
    const metric = {
      title: 'DIRECT ADS GMV',
    };
    const currency = 'USD';
    const value = Math.floor(Math.random() * 10000000);
    const percent =
      (Math.round(Math.random()) * 2 - 1) *
      Math.floor(Math.random() * 100);

    const chartData = getCharData();
    let chartOptions = {
      maintainAspectRatio: false,
      legend: {
        display: false,
      },
      layout: {
        padding: {
          left: 5,
          right: 5,
          top: 5,
          bottom: 5,
        },
      },
      tooltips: {
        mode: 'index',
        intersect: false,
      },
      hover: {
        mode: 'nearest',
        intersect: false,
      },
      scales: {
        xAxes: [
          {
            display: false,
          },
        ],
        yAxes: [
          {
            display: false,
          },
        ],
      },
    };
    chartOptions = customTooltip(chartOptions);

    return (
      <ChartShape
        metric={metric}
        value={value}
        currency={currency}
        variancePercent={percent}
        chartData={chartData}
        nodeData={data}
        chartOptions={chartOptions}
      ></ChartShape>
    );
  }

  renderConfigurationForm(
    dom: HTMLDivElement,
    data: NodeData['customAttributes'],
    handleSubmit,
  ) {
    return (
      <ChartConfigForm
        data={data}
        onSubmit={handleSubmit}
      ></ChartConfigForm>
    );
  }
}

const ChartConfigForm = ({ data, onSubmit }) => {
  return <div>Chart detail config</div>;
};

const ChartShape = (props: {
  metric: Object;
  currency?: string;
  value: number;
  variancePercent: number;
  chartOptions: Object;
  nodeData: NodeData;
}) => {
  const [title, setTitle] = React.useState('Direct Ads GMV');
  const [chartData, setChartData] = React.useState(getCharData());
  const [showRefresh, setShowRefresh] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [commonStatus, setCommonStatus] = React.useState(
    QUERY_STATUS.FAIL,
  );
  const [
    summaryCurrentValue,
    setSummaryCurrentValue,
  ] = React.useState(props.value);
  const [variancePercent, setVariancePercent] = React.useState(
    props.variancePercent,
  );
  let nodeData = props.nodeData;

  const metric = get(nodeData, 'mainAttributes.metrics', []).slice(
    0,
    1,
  )[0];
  const cohort = get(nodeData, 'mainAttributes.cohort', ['mtd'])[0];
  const { current, previous } = translateCohort(cohort);
  console.info({ cohort, current, previous });
  const { queries, reloadData } = useDataQuery(props.nodeData);
  const [
    currentData,
    previousData,
    summaryCurrent,
    summaryPrevious,
  ] = queries;

  const loadChartData = () => {
    if (
      currentData &&
      previousData &&
      summaryCurrent &&
      summaryPrevious
    ) {
      setLoading(true);
      setShowRefresh(false);
      reloadData(0);
      reloadData(1);
      reloadData(2);
      reloadData(3);
    }
  };

  React.useEffect(() => {
    if (commonStatus === QUERY_STATUS.LOADING) {
      setLoading(true);
    } else {
      setLoading(false);
      if (commonStatus === QUERY_STATUS.FAIL) {
        setShowRefresh(true);
      } else {
        if (commonStatus === QUERY_STATUS.SUCCESS) {
          console.log('Success loading');
          // let chartData = buildChartJsData(
          //   currentData.data,
          //   previousData.data,
          //   cohort,
          //   metric,
          // );
          // let metricDetails = currentData.metricLabels.find(
          //   (m) => m.id === metric,
          // );
          // const currentValueIndex = summaryCurrent.data.headers.indexOf(
          //   metric,
          // );
          // let currentValue = Number(
          //   summaryCurrent.data.rows[0][currentValueIndex],
          // );
          // let previousValue = Number(
          //   summaryPrevious.data.rows[0][currentValueIndex],
          // );
          // let variancePercent =
          //   (1 - previousValue / currentValue) * 100;
          // setSummaryCurrentValue(
          //   Number(summaryCurrent.data.rows[0][currentValueIndex]),
          // );
          // setVariancePercent(variancePercent);
          // setChartData(chartData);
          // setTitle(metricDetails.label);
        }
      }
    }
  }, [commonStatus]);

  React.useEffect(() => {
    console.log('Common status', commonStatus);
    const isLoading =
      currentData.status === QUERY_STATUS.LOADING ||
      previousData.status === QUERY_STATUS.LOADING ||
      summaryCurrent.status === QUERY_STATUS.LOADING ||
      summaryPrevious.status === QUERY_STATUS.LOADING;
    if (isLoading) {
      setCommonStatus(QUERY_STATUS.LOADING);
    } else {
      const isFail =
        currentData.status === QUERY_STATUS.FAIL ||
        previousData.status === QUERY_STATUS.FAIL ||
        summaryCurrent.status === QUERY_STATUS.FAIL ||
        summaryPrevious.status === QUERY_STATUS.FAIL;
      if (isFail) {
        setCommonStatus(QUERY_STATUS.FAIL);
      } else {
        setCommonStatus(QUERY_STATUS.SUCCESS);
      }
    }
  }, [
    currentData.status,
    previousData.status,
    summaryCurrent.status,
    summaryPrevious.status,
  ]);

  // React.useEffect(() => {
  //   console.log('HHHHHHHHHHHHH', status, data);
  //   if (status === QUERY_STATUS.LOADING) {
  //     setLoading(true);
  //   } else {
  //     setLoading(false);
  //     if (status === QUERY_STATUS.FAIL) {
  //       setShowRefresh(true);
  //     } else if (status === QUERY_STATUS.SUCCESS) {
  //       const {
  //         currentData,
  //         previousData,
  //         summaryCurrent,
  //         summaryPrevious,
  //       } = data;
  //       let chartData = buildChartJsData(
  //         currentData.data,
  //         previousData.data,
  //         cohort,
  //         metric,
  //       );
  //       let metricDetails = currentData.metricLabels.find(
  //         (m) => m.id === metric,
  //       );

  //       const currentValueIndex = summaryCurrent.data.headers.indexOf(
  //         metric,
  //       );
  //       let currentValue = Number(
  //         summaryCurrent.data.rows[0][currentValueIndex],
  //       );
  //       let previousValue = Number(
  //         summaryPrevious.data.rows[0][currentValueIndex],
  //       );
  //       let variancePercent =
  //         (1 - previousValue / currentValue) * 100;

  //       setSummaryCurrentValue(
  //         Number(summaryCurrent.data.rows[0][currentValueIndex]),
  //       );
  //       setVariancePercent(variancePercent);

  //       setChartData(chartData);
  //       setTitle(metricDetails.label);
  //     }
  //   }
  // }, [status]);

  React.useEffect(() => {
    loadChartData();
    const interval = setInterval(() => {
      if (!loading && showRefresh) {
        loadChartData();
      }
    }, RELOAD_TIME);
    return () => clearInterval(interval);
  }, []);

  return (
    <div className="metric-detail">
      <ChartLoading loading={loading}></ChartLoading>
      <img className="metric-bg" src={MetricBg} />
      {showRefresh && (
        <BsArrowRepeat
          title="Reload chart"
          className="refresh"
          onClick={loadChartData}
        ></BsArrowRepeat>
      )}
      {!loading && !showRefresh && (
        <div className="card card-hover-shadow h-100">
          <div className="card-body">
            <div className="align-items-center chartlib-header">
              <div className="card-subtitle">{title}</div>
            </div>
            <div className="chartlib-body">
              <div className="row align-items-center">
                <div className="col-6">
                  <div className="card-title h2">
                    <CurrencyCode
                      currency={props.currency}
                    ></CurrencyCode>
                    {formatCurrency(summaryCurrentValue)}
                  </div>
                  <div className="variance">
                    {props.variancePercent >= 0 && (
                      <BsArrowUp className="arrow up"></BsArrowUp>
                    )}
                    {props.variancePercent < 0 && (
                      <BsArrowDown className="arrow down"></BsArrowDown>
                    )}
                    <span
                      className={variancePercent >= 0 ? 'up' : 'down'}
                    >
                      {formatPercent(Math.abs(variancePercent))}%
                    </span>
                  </div>
                </div>
                <div className="col-6">
                  <div className="chartjs-custom">
                    <Line
                      data={chartData}
                      options={props.chartOptions}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

type TableData = {
  headers: string[];
  rows: any[];
};

function buildChartJsData(
  current: TableData,
  previous: TableData,
  cohort: string,
  metric: string,
) {
  const date = moment().date();

  let dateIndex = current.headers.indexOf('date');
  let metricIndex = current.headers.indexOf(metric);
  const currentMonthData = current.rows.map((i) => i[metricIndex]);
  const previousMonthData = previous.rows.map((i) => i[metricIndex]);

  let labels;
  if (cohort === 'mtd') {
    labels = getDaysOfMonth('YYYY-MM-DD');
  } else {
    labels = current.rows.map((r) => r[dateIndex]);
  }

  return {
    labels: labels,
    datasets: [
      {
        label: 'Current',
        fill: false,
        backgroundColor: '#204D77',
        borderColor: '#204D77',
        borderWidth: 2,
        pointRadius: 0,
        pointHoverRadius: 2,
        data: currentMonthData.map((item, index) =>
          index <= date ? item : undefined,
        ),
      },
      {
        label: 'Previous',
        fill: false,
        backgroundColor: '#D4DDED',
        borderColor: '#D4DDED',
        borderWidth: 2,
        pointRadius: 0,
        pointHoverRadius: 2,
        data: previousMonthData,
      },
    ],
  };
}

function customTooltip(chartOptions) {
  let defaultOptions = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      display: false,
    },
    tooltips: {
      enabled: false,
      mode: 'nearest',
      prefix: '',
      postfix: '',
      hasIndicator: false,
      indicatorWidth: '8px',
      indicatorHeight: '8px',
      transition: '0.2s',
      lineWithLineColor: null,
      yearStamp: true,
    },
    gradientPosition: {
      x0: 0,
      y0: 0,
      x1: 0,
      y1: 0,
    },
  };

  const defaultLineOptions = {
    scales: {
      yAxes: [
        {
          ticks: {
            callback: function (value, index, values) {
              var metric =
                  settings.options.scales.yAxes[0].ticks.metric,
                prefix =
                  settings.options.scales.yAxes[0].ticks.prefix,
                postfix =
                  settings.options.scales.yAxes[0].ticks.postfix;

              if (metric && value > 100) {
                if (value < 1000000) {
                  value = value / 1000 + 'k';
                } else {
                  value = value / 1000000 + 'kk';
                }
              }

              if (prefix && postfix) {
                return prefix + value + postfix;
              } else if (prefix) {
                return prefix + value;
              } else if (postfix) {
                return value + postfix;
              } else {
                return value;
              }
            },
          },
        },
      ],
    },
    elements: {
      line: {
        borderWidth: 3,
      },
      point: {
        pointStyle: 'circle',
        radius: 5,
        hoverRadius: 7,
        borderWidth: 3,
        hoverBorderWidth: 3,
        backgroundColor: '#ffffff',
        hoverBackgroundColor: '#ffffff',
      },
    },
  };

  let options = {
    ...defaultOptions,
    ...defaultLineOptions,
    ...chartOptions,
    tooltips: {
      ...defaultOptions.tooltips,
      ...chartOptions.tooltips,
      enabled: false,
      custom: function (tooltipModel) {
        // Tooltip Element
        var tooltipEl = document.getElementById('chartjsTooltip');

        // Create element on first render
        if (!tooltipEl) {
          tooltipEl = document.createElement('div');
          tooltipEl.id = 'chartjsTooltip';
          tooltipEl.style.opacity = 0;
          tooltipEl.classList.add('hs-chartjs-tooltip-wrap');
          tooltipEl.innerHTML =
            '<div class="hs-chartjs-tooltip"></div>';
          if (options.tooltips.lineMode) {
            // FIXME
            el.parent('.chartjs-custom').append(tooltipEl);
          } else {
            document.body.appendChild(tooltipEl);
          }
        }

        // Hide if no tooltip
        if (tooltipModel.opacity === 0) {
          tooltipEl.style.opacity = 0;

          tooltipEl.parentNode.removeChild(tooltipEl);

          return;
        }

        // Set caret Position
        tooltipEl.classList.remove('above', 'below', 'no-transform');
        if (tooltipModel.yAlign) {
          tooltipEl.classList.add(tooltipModel.yAlign);
        } else {
          tooltipEl.classList.add('no-transform');
        }

        function getBody(bodyItem) {
          return bodyItem.lines;
        }

        // Set Text
        if (tooltipModel.body) {
          var titleLines = tooltipModel.title || [],
            bodyLines = tooltipModel.body.map(getBody),
            today = new Date();

          var innerHtml =
            '<header class="hs-chartjs-tooltip-header">';

          titleLines.forEach(function (title) {
            innerHtml += options.tooltips.yearStamp
              ? title + ', ' + today.getFullYear()
              : title;
          });

          innerHtml +=
            '</header><div class="hs-chartjs-tooltip-body">';

          bodyLines.forEach(function (body, i) {
            innerHtml += '<div>';

            var oldBody = body[0],
              newBody = oldBody,
              color =
                tooltipModel.labelColors[i].backgroundColor instanceof
                Object
                  ? tooltipModel.labelColors[i].borderColor
                  : tooltipModel.labelColors[i].backgroundColor;

            innerHtml +=
              (options.tooltips.hasIndicator
                ? '<span class="d-inline-block rounded-circle mr-1" style="width: ' +
                  options.tooltips.indicatorWidth +
                  '; height: ' +
                  options.tooltips.indicatorHeight +
                  '; background-color: ' +
                  color +
                  '"></span>'
                : '') +
              options.tooltips.prefix +
              (oldBody.length > 3 ? newBody : body) +
              options.tooltips.postfix;

            innerHtml += '</div>';
          });

          innerHtml += '</div>';

          var tooltipRoot = tooltipEl.querySelector(
            '.hs-chartjs-tooltip',
          );
          tooltipRoot.innerHTML = innerHtml;
        }

        // `this` will be the overall tooltip
        var position = this._chart.canvas.getBoundingClientRect();

        // Display, position, and set styles for font
        tooltipEl.style.opacity = 1;
        if (options.tooltips.lineMode) {
          tooltipEl.style.left = tooltipModel.caretX + 'px';
        } else {
          tooltipEl.style.left =
            position.left +
            window.pageXOffset +
            tooltipModel.caretX -
            tooltipEl.offsetWidth / 2 -
            3 +
            'px';
        }
        tooltipEl.style.top =
          position.top +
          window.pageYOffset +
          tooltipModel.caretY -
          tooltipEl.offsetHeight -
          25 +
          'px';
        tooltipEl.style.pointerEvents = 'none';
        tooltipEl.style.transition = options.tooltips.transition;
      },
    },
  };

  return options;
}
