import { CurrencyCode } from '@ep/one/src/components/common/currency-code/currency-code';
import { DemoDataType } from '@epi/next/src/components/common/demo-data-type';
import { useDataQuery } from '@epi/next/src/components/dashboard/hooks';
import {
  ChartLibComponent,
  NodeData,
} from '@epi/next/src/components/dashboard/type';
import { EventEmitter } from 'events';
import moment from 'moment';
import React from 'react';
import { Line } from 'react-chartjs-2';
import { BsArrowDown, BsArrowUp } from 'react-icons/bs';
import { formatCurrency } from '../../../../../util/utils';
import './metric-total.scss';

const interactionEvents = {
  onDrilldownDataPoint: {
    eventId: 'onDrilldownDataPoint',
    label: 'Drilldown when click',
  },
};

export default class MetricTotalChartLib
  implements ChartLibComponent {
  getInteractionEvents() {
    return Object.values(interactionEvents);
  }
  render(
    dom: HTMLDivElement,
    data: NodeData,
    eventBus: EventEmitter,
  ) {
    return <ChartShape nodeData={data}></ChartShape>;
  }

  renderConfigurationForm(
    dom: HTMLDivElement,
    data: NodeData['customAttributes'],
    handleSubmit,
  ) {
    return (
      <ChartConfigForm
        data={data}
        onSubmit={handleSubmit}
      ></ChartConfigForm>
    );
  }
}

const ChartConfigForm = ({ data, onSubmit }) => {
  return <DemoDataType data={data} onSubmit={onSubmit} />;
};

const ChartShape = (props: { nodeData: NodeData }) => {
  const title = 'GMV';
  const { queries, reloadData } = useDataQuery(props.nodeData);

  console.log('Metric total DATA', queries);

  const [percent, setPercent] = React.useState(0);
  const [currency, setCurrency] = React.useState('USD');
  const [amount, setAmount] = React.useState(0);
  const [chartData, setCharData] = React.useState({});

  // console.log('NODE DATA', props.nodeData);
  // console.log('Chart total ====>', queries);

  // const { currency, value } = useDemoData(props.nodeData);

  React.useEffect(() => {
    if (queries) {
      setCurrency(queries[0].data.rows[0]);
      setAmount(queries[0].data.rows[1]);

      setPercent(queries[1].data.rows[0]);

      const newChartData = {
        labels: queries[2].data.rows,
        datasets: [
          {
            label: moment().format('MMMM'),
            fill: false,
            backgroundColor: '#204D77',
            borderColor: '#204D77',
            borderWidth: 2,
            pointRadius: 0,
            pointHoverRadius: 2,
            data: queries[4].data.rows.map((i) => i.value),
          },
          {
            label: moment().format('MMMM'),
            fill: false,
            backgroundColor: '#204D77',
            borderColor: '#204D77',
            borderWidth: 2,
            pointRadius: 0,
            pointHoverRadius: 2,
            borderDash: [3, 3],
            data: queries[5].data.rows.map((i) => i.value),
          },
          {
            label: moment().add(-1, 'month').format('MMMM'),
            fill: false,
            backgroundColor: '#D4DDED',
            borderColor: '#D4DDED',
            borderWidth: 2,
            pointRadius: 0,
            pointHoverRadius: 2,
            data: queries[3].data.rows.map((i) => i.value),
          },
        ],
      };
      setCharData(newChartData);
    }
  }, [queries]);

  let chartOptions = {
    maintainAspectRatio: false,
    legend: {
      display: false,
    },
    layout: {
      padding: {
        left: 5,
        right: 5,
        top: 5,
        bottom: 5,
      },
    },
    tooltips: {
      mode: 'index',
      hasIndicator: true,
      intersect: false,
      yearStamp: false,
      // lineMode: true,
    },
    hover: {
      mode: 'nearest',
      intersect: false,
    },
    scales: {
      xAxes: [
        {
          display: false,
        },
      ],
      yAxes: [
        {
          display: false,
        },
      ],
    },
  };

  chartOptions = customTooltip(chartOptions);

  return (
    <div className="metric-total">
      <div className="card card-hover-shadow h-100">
        <div className="card-body">
          <div className="align-items-center chartlib-header">
            <div
              className="card-subtitle"
              data-dbf-text-editable="title"
            >
              {title}
            </div>
            <div className="row align-items-center">
              <div className="col h2 card-title">
                <CurrencyCode currency={currency}></CurrencyCode>
                {formatCurrency(amount)}
                {currency === '%' ? '%' : ''}
              </div>
              <div className="col">
                <span className="variance">
                  {percent >= 0 && (
                    <BsArrowUp className="arrow up"></BsArrowUp>
                  )}
                  {percent < 0 && (
                    <BsArrowDown className="arrow down"></BsArrowDown>
                  )}
                  <span className={percent >= 0 ? 'up' : 'down'}>
                    {Math.abs(percent)}%
                  </span>
                </span>
              </div>
            </div>
          </div>

          <div
            className="chartjs-custom chartlib-body"
            data-dbf-event-drilldown={
              interactionEvents.onDrilldownDataPoint.eventId
            }
          >
            <Line data={chartData} options={chartOptions} />
          </div>
        </div>
      </div>
    </div>
  );
};

function customTooltip(chartOptions) {
  let defaultOptions = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      display: false,
    },
    tooltips: {
      enabled: false,
      mode: 'nearest',
      prefix: '',
      postfix: '',
      hasIndicator: false,
      indicatorWidth: '8px',
      indicatorHeight: '8px',
      transition: '0.2s',
      lineWithLineColor: null,
      yearStamp: true,
    },
    gradientPosition: {
      x0: 0,
      y0: 0,
      x1: 0,
      y1: 0,
    },
  };

  const defaultLineOptions = {
    scales: {
      yAxes: [
        {
          ticks: {
            callback: function (value, index, values) {
              var metric = options.scales.yAxes[0].ticks.metric,
                prefix = options.scales.yAxes[0].ticks.prefix,
                postfix = options.scales.yAxes[0].ticks.postfix;

              if (metric && value > 100) {
                if (value < 1000000) {
                  value = value / 1000 + 'k';
                } else {
                  value = value / 1000000 + 'kk';
                }
              }

              if (prefix && postfix) {
                return prefix + value + postfix;
              } else if (prefix) {
                return prefix + value;
              } else if (postfix) {
                return value + postfix;
              } else {
                return value;
              }
            },
          },
        },
      ],
    },
    elements: {
      line: {
        borderWidth: 3,
      },
      point: {
        pointStyle: 'circle',
        radius: 5,
        hoverRadius: 7,
        borderWidth: 3,
        hoverBorderWidth: 3,
        backgroundColor: '#ffffff',
        hoverBackgroundColor: '#ffffff',
      },
    },
  };

  let options = {
    ...defaultOptions,
    ...defaultLineOptions,
    ...chartOptions,
    tooltips: {
      ...defaultOptions.tooltips,
      ...chartOptions.tooltips,
      enabled: false,
      custom: function (tooltipModel) {
        // Tooltip Element
        var tooltipEl = document.getElementById('chartjsTooltip');

        // Create element on first render
        if (!tooltipEl) {
          tooltipEl = document.createElement('div');
          tooltipEl.id = 'chartjsTooltip';
          tooltipEl.style.opacity = 0;
          tooltipEl.classList.add('hs-chartjs-tooltip-wrap');
          tooltipEl.innerHTML =
            '<div class="hs-chartjs-tooltip"></div>';
          if (options.tooltips.lineMode) {
            el.parent('.chartjs-custom').append(tooltipEl);
          } else {
            document.body.appendChild(tooltipEl);
          }
          console.info('no tootlip');
        }

        // Hide if no tooltip
        if (tooltipModel.opacity === 0) {
          tooltipEl.style.opacity = 0;

          tooltipEl.parentNode.removeChild(tooltipEl);

          return;
        }

        // Set caret Position
        tooltipEl.classList.remove('above', 'below', 'no-transform');
        if (tooltipModel.yAlign) {
          tooltipEl.classList.add(tooltipModel.yAlign);
        } else {
          tooltipEl.classList.add('no-transform');
        }

        function getBody(bodyItem) {
          return bodyItem.lines;
        }

        // Set Text
        if (tooltipModel.body) {
          var titleLines = tooltipModel.title || [],
            bodyLines = tooltipModel.body.map(getBody),
            today = new Date();

          var innerHtml =
            '<header class="hs-chartjs-tooltip-header">';

          titleLines.forEach(function (title) {
            innerHtml += options.tooltips.yearStamp
              ? title + ', ' + today.getFullYear()
              : title;
          });

          innerHtml +=
            '</header><div class="hs-chartjs-tooltip-body">';

          bodyLines.forEach(function (body, i) {
            innerHtml += '<div>';

            var oldBody = body[0],
              newBody = oldBody,
              color =
                tooltipModel.labelColors[i].backgroundColor instanceof
                Object
                  ? tooltipModel.labelColors[i].borderColor
                  : tooltipModel.labelColors[i].backgroundColor;

            innerHtml +=
              (options.tooltips.hasIndicator
                ? '<span class="d-inline-block rounded-circle mr-1" style="width: ' +
                  options.tooltips.indicatorWidth +
                  '; height: ' +
                  options.tooltips.indicatorHeight +
                  '; background-color: ' +
                  color +
                  '"></span>'
                : '') +
              options.tooltips.prefix +
              (oldBody.length > 3 ? newBody : body) +
              options.tooltips.postfix;

            innerHtml += '</div>';
          });

          innerHtml += '</div>';

          var tooltipRoot = tooltipEl.querySelector(
            '.hs-chartjs-tooltip',
          );
          tooltipRoot.innerHTML = innerHtml;
        }

        // `this` will be the overall tooltip
        var position = this._chart.canvas.getBoundingClientRect();

        // Display, position, and set styles for font
        tooltipEl.style.opacity = 1;
        if (options.tooltips.lineMode) {
          tooltipEl.style.left = tooltipModel.caretX + 'px';
        } else {
          tooltipEl.style.left =
            position.left +
            window.pageXOffset +
            tooltipModel.caretX -
            tooltipEl.offsetWidth / 2 -
            3 +
            'px';
        }
        tooltipEl.style.top =
          position.top +
          window.pageYOffset +
          tooltipModel.caretY -
          tooltipEl.offsetHeight -
          25 +
          'px';
        tooltipEl.style.pointerEvents = 'none';
        tooltipEl.style.transition = options.tooltips.transition;
      },
    },
  };

  return options;
}
