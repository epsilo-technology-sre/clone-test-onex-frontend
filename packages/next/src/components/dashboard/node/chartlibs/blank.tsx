import { EventEmitter } from 'events';
import React from 'react';
import ReactDOM from 'react-dom';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import { ChartLibComponent, NodeData } from '../../type';

export default class BlankChartLib implements ChartLibComponent {
  render(
    dom: HTMLDivElement,
    data: NodeData,
    eventBus: EventEmitter,
  ) {
    // dom.innerHTML = '<div style="height: 1000px; width: 100px; background: tomato;"><div style="height: 100px; background: black"></div></div>';
    return null;
  }

  renderConfigurationForm(
    dom: HTMLDivElement,
    data: NodeData['customAttributes'],
    handleSubmit,
  ) {
    return <ChartForm data={data} onSubmit={handleSubmit} />;
  }
}

function ChartForm({ data, onSubmit }) {
  return (
    <Form>
      <FormGroup>
        <Label for="chartlib_blank_type">Display as</Label>
        <Input
          type="select"
          name="blankType"
          id="chartlib_blank_type"
          onChange={(evt) => {
            onSubmit({ displayAs: evt.target.value });
          }}
        >
          <option value={'white_space'}>White space</option>
          <option value={'horizontal_line'}>Horizontal line</option>
          <option value={'vertical_line'}>Vertical line</option>
        </Input>
      </FormGroup>
    </Form>
  );
}
