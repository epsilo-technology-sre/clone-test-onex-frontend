import { ChartLib } from '../../type';
import BlankChartLib from './blank';
import ContainerChartLib from './container';
import DividerChartLib from './divider';
import EmbedChartLib from './embed';
import { makeGlobalFiltersChartLib } from './global-filters';
import JsonAttrChartLib from './json-attr';
import MetricDetailChartLib from './metric-detail/metric-detail';
import MetricTotalChartLib from './metric-total/metric-total';
import MetricChartLib from './metric/metric';
import TableChartLib from './table/table';
import TextChartLib from './text';

export const CHART_TYPE = {
  BLANK: 'blank',
  EMBED: 'embed',
  METRIC: 'metric',
  METRIC_TOTAL: 'metricTotal',
  METRIC_DETAIL: 'metricDetail',
  TABLE: 'table',
  TEXT_PLAIN: 'textPlain',
  DIVIDER: 'divider',
  JSON_VIEW: 'jsonview',
};

const chartLib = (() => {
  let libs: { [key: string]: ChartLib } = {
    blank: {
      chartLibId: CHART_TYPE.BLANK,
      label: 'Blank',
      component: new BlankChartLib(),
      scale: 0.072916,
    },
    embed: {
      chartLibId: CHART_TYPE.EMBED,
      label: 'Embed (WILL REMOVE Production)',
      component: new EmbedChartLib(),
      scale: 0.072916,
    },
    metric: {
      chartLibId: CHART_TYPE.METRIC,
      label: 'Metric chart',
      component: new MetricChartLib(),
      scale: 0.18421,
      // scale: 0,
    },
    metricTotal: {
      chartLibId: CHART_TYPE.METRIC_TOTAL,
      label: 'Metric total',
      component: new MetricTotalChartLib(),
      scale: 0.072916,
      // scale: 0,
    },
    metricDetail: {
      chartLibId: CHART_TYPE.METRIC_DETAIL,
      label: 'Metric detail',
      component: new MetricDetailChartLib(),
      scale: 0.145833,
      // scale: 0,
    },
    table: {
      chartLibId: CHART_TYPE.TABLE,
      label: 'Table',
      component: new TableChartLib(),
      // scale: 0.012916,
      scale: 0,
    },
    globalFilters: makeGlobalFiltersChartLib(
      'globalFilters',
      'Global Filters',
    ),
    textPlain: {
      chartLibId: CHART_TYPE.TEXT_PLAIN,
      label: 'Text only',
      component: new TextChartLib(),
      scale: 0,
    },
    divider: {
      chartLibId: CHART_TYPE.DIVIDER,
      label: 'Divider',
      component: new DividerChartLib(),
      scale: 0,
    },
    jsonview: {
      chartLibId: CHART_TYPE.JSON_VIEW,
      label: 'JSON view',
      component: new JsonAttrChartLib(),
      scale: 0,
      // scale: 0.072916,
    },
    container: {
      chartLibId: 'container',
      label: 'Container',
      component: new ContainerChartLib(),
      scale: 0,
    },
  };

  if (process.env.NODE_ENV === 'production') {
    delete libs.embed;
  }

  libs = {
    ...libs,
    notAvailable: {
      chartLibId: 'not_available',
      label: '',
      component: {
        render: (dom, nodeData) => {
          console.error('missing chart for', nodeData);
          return null;
        },
        renderConfigurationForm: (dom, nodeData) => {
          console.error('missing chart for form', nodeData);
          return null;
        },
      },
      scale: 0,
    },
  };

  return {
    find: (chartId) => {
      return libs[chartId] ? libs[chartId] : libs['notAvailable'];
    },
    asList: () => {
      return Object.values(libs).filter(
        (i) => i.chartLibId !== 'not_available',
      );
    },
  };
})();

export default chartLib;
