import { EventEmitter } from 'events';
import React from 'react';
import ReactDOM from 'react-dom';
import JSONView from 'react-json-view';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import { get } from 'lodash';
import { ChartLibComponent, NodeData } from '../../type';

export default class JsonAttrChartLib implements ChartLibComponent {
  render(
    dom: HTMLDivElement,
    data: NodeData,
    eventBus: EventEmitter,
  ) {
    // dom.innerHTML = get(data, 'customAttributes.embedCode', '- no embed code -');
    ReactDOM.render(<JSONView src={data} theme="rjv-default" />, dom);
  }

  renderConfigurationForm(
    dom: HTMLDivElement,
    data: NodeData['customAttributes'],
    handleSubmit,
  ) {
    console.info('render form >>>', { dom, data });
    return null;
    // window.requestAnimationFrame(() => {
    //   ReactDOM.render(
    //     <ChartForm data={data} onSubmit={handleSubmit} />,
    //     dom,
    //   );
    // });
  }
}

