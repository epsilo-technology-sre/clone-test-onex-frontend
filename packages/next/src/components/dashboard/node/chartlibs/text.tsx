import { EventEmitter } from 'events';
import React from 'react';
import ReactDOM from 'react-dom';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import { ChartLibComponent, NodeData } from '../../type';

export default class TextChartLib implements ChartLibComponent {
  render(
    dom: HTMLDivElement,
    data: NodeData,
    eventBus: EventEmitter,
  ) {
    // dom.innerHTML = '<div style="height: 1000px; width: 100px; background: tomato;"><div style="height: 100px; background: black"></div></div>';

    return (
      <div style={{ padding: '12px' }}>
        <div data-dbf-text-editable="content">
          Click save, then go back to dashboard to edit your text.
        </div>
      </div>
    );
  }

  renderConfigurationForm(
    dom: HTMLDivElement,
    data: NodeData['customAttributes'],
    handleSubmit,
  ) {
    return <ChartForm data={data} onSubmit={handleSubmit} />;
  }
}

function ChartForm({ data, onSubmit }) {
  return (
    <Form>
      <FormGroup>
        <Label for="chartlib_blank_type">Display as</Label>
      </FormGroup>
    </Form>
  );
}
