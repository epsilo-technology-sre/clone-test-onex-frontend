import { CurrencyCode } from '@ep/shopee/src/components/common/currency-code';
import { formatCurrency } from '@ep/shopee/src/utils/utils';
import { Grid } from '@material-ui/core';
import { EventEmitter } from 'events';
import React from 'react';
import ReactDOM from 'react-dom';
import { BsArrowDown, BsArrowUp } from 'react-icons/bs';
import {
  DemoDataType,
  useDemoData,
} from '../../../../common/demo-data-type';
import { ChartLibComponent, NodeData } from '../../../type';
import './metric.scss';

export default class MetricChartLib implements ChartLibComponent {
  render(
    dom: HTMLDivElement,
    data: NodeData,
    eventBus: EventEmitter,
  ) {
    const metric = {
      title: 'Cost',
    };

    const { currency, value } = useDemoData(data);

    const percent =
      (Math.round(Math.random()) * 2 - 1) *
      Math.floor(Math.random() * 100);
    return (
      <ChartShape
        metric={metric}
        value={value}
        currency={currency}
        variancePercent={percent}
      ></ChartShape>
    );
  }

  renderConfigurationForm(
    dom: HTMLDivElement,
    data: NodeData['customAttributes'],
    handleSubmit,
  ) {
    return (
      <ChartConfigForm
        data={data}
        onSubmit={handleSubmit}
      ></ChartConfigForm>
    );
  }
}

const ChartConfigForm = ({ data, onSubmit }) => {
  return <DemoDataType data={data} onSubmit={onSubmit} />;
};

const ChartShape = (props: {
  metric: Object;
  currency?: string;
  value: number;
  variancePercent: number;
}) => {
  return (
    <div className="metric">
      <div className="card card-hover-shadow h-100">
        <div className="card-body">
          <div className="align-items-center chartlib-header">
            <div
              className="card-subtitle"
              data-dbf-text-editable="title"
            >
              {props.metric.title}
            </div>
          </div>
          <Grid
            container
            className="chartlib-body align-items-center"
          >
            <Grid
              item
              xs={9}
              style={{ padding: 1, maxHeight: '100%' }}
            >
              <CurrencyCode currency={props.currency}></CurrencyCode>
              {formatCurrency(props.value)}
            </Grid>
            <Grid
              item
              xs={3}
              className="text-nowrap"
              style={{ padding: 1, maxHeight: '100%' }}
            >
              <span className="variance">
                {props.variancePercent >= 0 && (
                  <BsArrowUp className="arrow up"></BsArrowUp>
                )}
                {props.variancePercent < 0 && (
                  <BsArrowDown className="arrow down"></BsArrowDown>
                )}
                <span
                  className={
                    props.variancePercent >= 0 ? 'up' : 'down'
                  }
                >
                  {props.variancePercent}%
                </span>
              </span>
            </Grid>
          </Grid>
        </div>
      </div>
    </div>
  );
};
