import { TableSettingMenu } from '@epi/next/src/components/common/table-setting-menu';
import * as React from 'react';
import {
  useColumnOrder,
  useFlexLayout,
  useResizeColumns,
  useRowSelect,
  useTable,
} from 'react-table';
import styled from 'styled-components';
import { useLog } from '../../../../../../lib/log';
import { InlineHeaderMenu } from '../../../../common/inline-header-menu';
import { TableFilterUI } from '../../../../common/table-filter';

const log = useLog('chartlib:table');

const Styles = styled.div`
  display: block;
  overflow: auto;

  .table {
    border-spacing: 0;
    margin: 0;

    .thead {
      overflow-y: auto;
      overflow-x: hidden;
      background: #f8fafd;
    }

    .tbody {
      overflow-y: scroll;
      overflow-x: hidden;
      height: 250px;
    }

    .tr {
      :last-child {
        .td {
          border-bottom: 0;
        }
      }
    }

    .th {
      font-weight: 500;
      font-size: 12px;
      color: #8c98a4;
    }

    .th,
    .td {
      margin: 0;
      padding: 18px;
      position: relative;

      :last-child {
        border-right: 0;
      }

      .resizer {
        display: none;
        background: gray;
        width: 6px;
        height: 100%;
        position: absolute;
        right: 0;
        top: 0;
        transform: translateX(50%);
        z-index: 1;
        touch-action: none;
      }

      &:first-child {
        justify-content: center;
      }

      &:hover {
        .resizer {
          display: inline-block;
        }
      }
    }
  }
`;

const headerProps = (props, { column }) =>
  getStyles(props, column.align);

const cellProps = (props, { cell }) =>
  getStyles(props, cell.column.align);

const getStyles = (props, align = 'left') => [
  props,
  {
    style: {
      justifyContent: align === 'right' ? 'flex-end' : 'flex-start',
      alignItems: 'flex-start',
      display: 'flex',
    },
  },
];

const IndeterminateCheckbox = React.forwardRef(
  ({ indeterminate, ...rest }, ref) => {
    const defaultRef = React.useRef();
    const resolvedRef = ref || defaultRef;

    React.useEffect(() => {
      resolvedRef.current.indeterminate = indeterminate;
    }, [resolvedRef, indeterminate]);

    return (
      <>
        <input type="checkbox" ref={resolvedRef} {...rest} />
      </>
    );
  },
);

// FIXME: add PropTypes for table

function Table({
  columns,
  columnOrder = [],
  data,
  onSelectInlineMenu,
  onUpdateColumnOrder,
}) {
  const defaultColumn = React.useMemo(
    () => ({
      minWidth: 30,
      width: 150,
      maxWidth: 200,
    }),
    [],
  );

  log('columnOrder', columnOrder);

  const tableInstance = useTable(
    {
      columns,
      data,
      defaultColumn,
      initialState: {
        columnOrder,
      },
    },
    useResizeColumns,
    useFlexLayout,
    useRowSelect,
    useColumnOrder,
    // (hooks) => {
    //   hooks.allColumns.push((columns) => [
    //     {
    //       id: 'selection',
    //       disableResizing: true,
    //       minWidth: 35,
    //       width: 35,
    //       maxWidth: 35,
    //       Header: ({ getToggleAllRowsSelectedProps }) => (
    //         <div>
    //           <IndeterminateCheckbox
    //             {...getToggleAllRowsSelectedProps()}
    //           />
    //         </div>
    //       ),
    //       Cell: ({ row }) => (
    //         <div>
    //           <IndeterminateCheckbox
    //             {...row.getToggleRowSelectedProps()}
    //           />
    //         </div>
    //       ),
    //     },
    //     ...columns,
    //   ]);
    //   hooks.useInstanceBeforeDimensions.push(({ headerGroups }) => {
    //     const selectionGroupHeader = headerGroups[0].headers[0];
    //     selectionGroupHeader.canResize = false;
    //   });
    // },
  );

  let tableSetColumnOrder = tableInstance.setColumnOrder;

  tableInstance.setColumnOrder = React.useCallback((orders) => {
    tableSetColumnOrder(orders);
    console.info('setColumnOrder', orders);
    onUpdateColumnOrder(orders);
  }, []);

  const {
    getTableProps,
    headerGroups,
    rows,
    prepareRow,
    toggleSortBy,
  } = tableInstance;

  const handleSortColumn = (item, column) => {
    console.log('Sort column', item, column);
    console.log('Table INSTANCE', tableInstance);
    let isDesc = item === 'desc';
    toggleSortBy(column.id, isDesc, true);
  };

  return (
    <div>
      <TableSettingMenu table={tableInstance}></TableSettingMenu>
      <div {...getTableProps()} className="table">
        <div className="thead">
          {headerGroups.map((headerGroup) => (
            <div
              {...headerGroup.getHeaderGroupProps({})}
              className="tr"
            >
              {headerGroup.headers.map((column) => (
                <div
                  {...column.getHeaderProps(headerProps)}
                  // {...column.getHeaderProps(
                  //   column.getSortByToggleProps(),
                  // )}
                  className="th"
                >
                  <InlineHeaderMenu
                    onSelectMenu={(item) =>
                      // onSelectInlineMenu(item, column)
                      handleSortColumn(item, column)
                    }
                  >
                    {column.render('Header')}
                    <span>
                      {column.isSorted
                        ? column.isSortedDesc
                          ? ' 🔽'
                          : ' 🔼'
                        : ''}
                    </span>
                  </InlineHeaderMenu>

                  {column.canResize && (
                    <div
                      {...column.getResizerProps()}
                      className={`resizer ${
                        column.isResizing ? 'isResizing' : ''
                      }`}
                    />
                  )}
                </div>
              ))}
            </div>
          ))}
        </div>
        <div className="tbody">
          {rows.map((row) => {
            prepareRow(row);
            return (
              <div {...row.getRowProps()} className="tr">
                {row.cells.map((cell) => {
                  return (
                    <div
                      {...cell.getCellProps(cellProps)}
                      className="td"
                    >
                      {cell.render('Cell')}
                    </div>
                  );
                })}
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}

// FIXME: add PropTypes for table

export const SimpleTable = (props) => {
  const {
    columns,
    columnOrder = [],
    rows,
    resultTotal,
    page,
    pageSize,
    onChangePage,
    onChangePageSize,
    onSort,
    noData,
    getRowId,
    onSelectInlineMenu,
  } = props;

  return (
    <Styles>
      <Table
        columns={columns}
        columnOrder={columnOrder}
        data={rows}
        onUpdateColumnOrder={props.onUpdateColumnOrder}
        onSelectInlineMenu={onSelectInlineMenu}
      />
      {page && page >= 1 && (
        <div className="footer-container">
          <TableFilterUI
            resultTotal={resultTotal}
            page={page}
            pageSize={pageSize}
            onChangePage={onChangePage}
            onChangePageSize={onChangePageSize}
          />
        </div>
      )}
    </Styles>
  );
};
