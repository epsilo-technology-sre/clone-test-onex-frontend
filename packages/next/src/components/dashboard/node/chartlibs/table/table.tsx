import { useDataQuery } from '@epi/next/src/components/dashboard/hooks';
import { EventEmitter } from 'events';
import React from 'react';
import { useFilter } from '../../../../filter/hooks';
import { ChartLibComponent, NodeData } from '../../../type';
import { NodeEditContext } from '../../context';
import { SimpleTable } from './simple-table';
import './table.scss';
import { useLog } from '@epi/next/lib/log';
import { get } from 'lodash';

let log = useLog('chartlib:table');

const COLUMNS = [
  {
    Header: 'SHOP',
    accessor: 'shop',
  },
  {
    Header: 'ADS ITEMS SOLD',
    accessor: 'adsItemSold',
    align: 'right',
  },
  {
    Header: 'DIRECT ADS ITEM SOLD',
    accessor: 'directItemSold',
    // disableSortBy: true,
    align: 'right',
  },
  {
    Header: 'ROAS',
    accessor: 'roas',
    // disableSortBy: true,
    align: 'right',
  },
  {
    Header: 'COST',
    accessor: 'cost',
    // disableSortBy: true,
    align: 'right',
  },
  {
    Header: 'ADS GMV',
    accessor: 'adsGMV',
    // disableSortBy: true,
    align: 'right',
  },
];

export default class TableChartLib implements ChartLibComponent {
  render(
    dom: HTMLDivElement,
    data: NodeData,
    eventBus: EventEmitter,
  ) {
    return <ChartShape nodeData={data} container={dom}></ChartShape>;
  }

  renderConfigurationForm(
    dom: HTMLDivElement,
    data: NodeData['customAttributes'],
    handleSubmit,
  ) {
    return <ChartConfigForm></ChartConfigForm>;
  }
}

const ChartConfigForm = () => {
  return <div>Chart config form</div>;
};

const ChartShape = ({
  nodeData,
  container,
}: {
  nodeData: NodeData;
  container: any;
}) => {
  const { availFilters } = useFilter({});
  const { queries, reloadData } = useDataQuery(nodeData);
  const [dataColumns, setDataColumns] = React.useState(COLUMNS);
  const [dataRows, setDataRows] = React.useState([]);
  const [pagination, setPagination] = React.useState({
    page: 1,
    itemCount: 0,
    limit: 10,
  });

  React.useEffect(() => {
    let newColumns = [
      {
        Header: 'SHOP',
        accessor: 'shop',
      },
    ];
    newColumns = newColumns.concat(
      availFilters.metrics.map((i) => ({
        Header: i.label,
        accessor: i.value,
        align: 'right',
      })),
    );

    setDataColumns(newColumns);
  }, [availFilters.metrics]);

  React.useEffect(() => {
    setPagination({
      ...pagination,
      itemCount: queries[0].data.rows.length,
    });
    getData(pagination.page, pagination.limit);
  }, [queries[0].data.rows]);

  React.useEffect(() => {
    setPagination({
      ...queries[1].data.rows[0],
      itemCount: queries[0].data.rows.length,
    });
  }, [queries[1].data.rows]);

  React.useEffect(() => {
    const ro = new ResizeObserver((entries) => {
      for (let entry of entries) {
        window.requestAnimationFrame(() => {
          const titleHeight = container.querySelector(
            '.title-container',
          ).offsetHeight;
          const footerHeight = container.querySelector(
            '.footer-container',
          ).offsetHeight;
          const headerHeight = container.querySelector('.thead')
            .offsetHeight;
          const newHeight =
            entry.contentRect.height -
            (titleHeight + headerHeight + footerHeight);
          container.querySelector('.tbody').style.height =
            newHeight + 'px';
        });
      }
    });

    ro.observe(container);

    return () => {
      ro.disconnect();
    };
  }, []);

  const nodeEditContext = React.useContext(NodeEditContext);

  const getRowId = React.useCallback((row) => {
    return String(row.shopEid);
  }, []);

  const handleUpdateColumnOrder = React.useCallback(
    (order) => {
      log('handleUpdateColumnOrder', order);
      nodeEditContext.onUpdateCustomAttributes(nodeData, {
        columnOrder: order,
      });
    },
    [nodeData],
  );

  const getData = (page, limit) => {
    const start = (page - 1) * limit;
    const end = start + limit;
    const newData = queries[0].data.rows.slice(start, end);
    setDataRows(newData);
  };

  const handleSorting = () => {
    console.log('Sorting');
  };

  const handleChangePage = (page) => {
    setPagination({
      ...pagination,
      page,
    });
    getData(page, pagination.limit);
  };

  const handleChangePageSize = (pageSize) => {
    setPagination({
      ...pagination,
      page: 1,
      limit: pageSize,
    });
    getData(1, pageSize);
  };

  const handleSelectInlineMenu = (item, column) => {
    console.log('Select inline menu', item, column);
  };

  return (
    <div className="table-chartlib">
      <SimpleTable
        columns={dataColumns}
        columnOrder={get(
          nodeData,
          'customAttributes.columnOrder',
          [],
        )}
        rows={dataRows}
        resultTotal={pagination.itemCount}
        page={pagination.page}
        pageSize={pagination.limit}
        getRowId={getRowId}
        onSort={handleSorting}
        onChangePage={handleChangePage}
        onChangePageSize={handleChangePageSize}
        onSelectInlineMenu={handleSelectInlineMenu}
        onUpdateColumnOrder={handleUpdateColumnOrder}
      ></SimpleTable>
    </div>
  );
};
