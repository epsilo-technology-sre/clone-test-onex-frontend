import { EventEmitter } from 'events';
import { get } from 'lodash';
import React from 'react';
import ReactDOM from 'react-dom';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import { ChartLibComponent, NodeData } from '../../type';

export default class DividerChartLib implements ChartLibComponent {
  render(
    dom: HTMLDivElement,
    data: NodeData,
    eventBus: EventEmitter,
  ) {
    let position = get(data, 'customAttributes.position', 'middle');
    const alignMaping = {
      top: 'flex-start',
      bottom: 'flex-end',
      middle: 'center',
    };
    return (
      <div
        style={{
          color: '#E4E7E9',
          height: '100%',
          width: '100%',
          minWidth: '100px',
          minHeight: '20px',
          display: 'flex',
          justifyContent: 'center',
          alignItems: alignMaping[position],
        }}
      >
        <div
          style={{
            background: '#E4E7E9',
            height: '1px',
            width: '100%',
            flexGrow: 1,
          }}
        />
      </div>
    );
  }

  renderConfigurationForm(
    dom: HTMLDivElement,
    data: NodeData['customAttributes'],
    handleSubmit,
  ) {
    return <ChartForm data={data} onSubmit={handleSubmit} />;
  }
}

function ChartForm({ data, onSubmit }) {
  console.info({ data });
  return (
    <Form>
      <FormGroup>
        <Label for="chartlib_blank_type">Position</Label>
        <Input
          type="select"
          name="position"
          id="chartlib_divider_position"
          value={get(data, 'position', 'middle')}
          onChange={(evt) => {
            console.info(evt.target.value);
            onSubmit({ position: evt.target.value });
          }}
        >
          <option value={'middle'}>Middle</option>
          <option value={'top'}>Top</option>
          <option value={'bottom'}>Bottom</option>
        </Input>
      </FormGroup>
    </Form>
  );
}
