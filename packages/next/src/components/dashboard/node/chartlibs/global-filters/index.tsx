import React from 'react';
import { EventEmitter } from 'events';
import ReactDOM from 'react-dom';
import { ChartLibComponent, NodeData } from '../../../type';
import { GlobalFilters } from './global-filters';

export default class GlobalFiltersChartLib
  implements ChartLibComponent {
  getInteractionEvents() {
    return [];
  }

  render(
    dom: HTMLDivElement,
    data: NodeData,
    eventBus: EventEmitter,
  ) {
    return <GlobalFilters></GlobalFilters>;
  }

  renderConfigurationForm(
    dom: HTMLDivElement,
    data: NodeData['customAttributes'],
    handleSubmit,
  ) {
    return (
      <ChartConfigForm
        data={data}
        onSubmit={handleSubmit}
      ></ChartConfigForm>
    );
  }
}

const ChartConfigForm = ({ data, onSubmit }) => {
  return <div></div>;
};

export function makeGlobalFiltersChartLib(chartLibId, label) {
  return {
    chartLibId,
    label,
    component: new GlobalFiltersChartLib(),
    // scale: 0.012916,
    scale: 0,
  };
}
