import { makeStyles } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import React from 'react';
import { uniqBy } from 'lodash';
import { MultiSelectUI } from '@epi/next/src/components/common/multi-select';
import { getAllShops } from '@epi/next/src/util/async-resources';

export function GlobalFilters() {
  const [state, setState] = React.useState({
    channels: [],
    tools: [],
    countries: [],
    shops: [],
  });
  React.useEffect(() => {
    getAllShops().then((allShops) => {
      let availFilters = {
        channels: uniqBy(allShops, (i: any) => i.channel_code).map(
          (i) => ({
            id: i.channel_code,
            text: i.channel_name,
          }),
        ),
        tools: uniqBy(
          allShops
            .map((i) => i.features)
            .reduce((accum, i) => {
              return accum.concat(i);
            }, []),
          (i: any) => i.feature_code,
        ).map((i) => ({
          id: i.feature_code,
          text: i.feature_name,
        })),
        countries: uniqBy(allShops, (i: any) => i.country_code).map(
          (i) => ({
            id: i.country_code,
            text: i.country_name,
          }),
        ),
        shops: allShops.map((i) => ({
          id: i.shop_eid,
          text: i.shop_name,
        })),
      };

      setState(availFilters);
    });
  }, []);

  return (
    <GlobalFiltersUI
      channels={state.channels}
      features={state.tools}
      countries={state.countries}
      shops={state.shops}
      selectedChannels={state.channels.map((i) => i.id)}
      selectedFeatures={state.tools.map((i) => i.id)}
      selectedCountries={state.countries.map((i) => i.id)}
      selectedShops={state.shops.map((i) => i.id)}
    />
  );
}

export const GlobalFiltersUI = (props: any) => {
  const {
    channels = [],
    features = [],
    countries = [],
    shops = [],
    selectedChannels = [],
    selectedFeatures = [],
    selectedCountries = [],
    selectedShops = [],
    onChangeChannels,
    onChangeFeatures,
    onChangeCountries,
    onChangeShops,
    onSubmitChange,
    hideChannel = false,
  } = props;

  const { filterItem } = useStyle();

  return (
    <div className="card card-hover-shadow h-100">
      <div
        className="card-body"
        style={{
          padding: '0 12px',
          display: 'flex',
          alignItems: 'center',
        }}
      >
        <Box display="flex" width="100%">
          <Grid container justify="flex-start" alignItems="center">
            <Grid item>
              <div data-dbf-text-editable="title">Master filter</div>
            </Grid>
            <Grid item style={{ flexGrow: 1 }}></Grid>
            {hideChannel === true ? null : (
              <Grid item className={filterItem}>
                <MultiSelectUI
                  prefix="Channel"
                  suffix="channels"
                  items={channels}
                  selectedItems={selectedChannels}
                  onSaveChange={onChangeChannels}
                />
              </Grid>
            )}
            <Grid item className={filterItem}>
              <MultiSelectUI
                prefix="Tool"
                suffix="tools"
                items={features}
                selectedItems={selectedFeatures}
                onSaveChange={onChangeFeatures}
              />
            </Grid>
            <Grid item className={filterItem}>
              <MultiSelectUI
                prefix="Country"
                suffix="countries"
                items={countries}
                selectedItems={selectedCountries}
                onSaveChange={onChangeCountries}
              />
            </Grid>
            <Grid item className={filterItem}>
              <MultiSelectUI
                prefix="Shop"
                suffix="shops"
                items={shops}
                selectedItems={selectedShops}
                onSaveChange={onChangeShops}
              />
            </Grid>
          </Grid>
        </Box>
      </div>
    </div>
  );
};

const useStyle = makeStyles((theme) => ({
  filterItem: {
    marginTop: theme.spacing(0),
    marginBottom: theme.spacing(0),
  },
}));
