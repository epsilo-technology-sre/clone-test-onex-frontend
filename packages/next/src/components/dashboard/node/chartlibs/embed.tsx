import { EventEmitter } from 'events';
import React from 'react';
import ReactDOM from 'react-dom';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import { get } from 'lodash';
import { ChartLibComponent, NodeData } from '../../type';

export default class EmbedChartLib implements ChartLibComponent {
  render(
    dom: HTMLDivElement,
    data: NodeData,
    eventBus: EventEmitter,
  ) {
    return (
      <div
        dangerouslySetInnerHTML={{
          __html: get(
            data,
            'customAttributes.embedCode',
            '- no embed code -',
          ),
        }}
      ></div>
    );
  }

  renderConfigurationForm(
    dom: HTMLDivElement,
    data: NodeData['customAttributes'],
    handleSubmit,
  ) {
    return <ChartForm data={data} onSubmit={handleSubmit} />;
  }
}

function ChartForm({ data, onSubmit }) {
  const embedCode = get(data, 'embedCode', '');

  return (
    <Form>
      <FormGroup>
        <Label for="chartlib_type_embed_input">Embed code</Label>
        <Input
          type="textarea"
          name="text"
          id="chartlib_type_embed_input"
          value={embedCode}
          onChange={(evt) => {
            console.info({ embedCode: evt.target.value });
            onSubmit({ embedCode: evt.target.value });
          }}
        />
      </FormGroup>
    </Form>
  );
}
