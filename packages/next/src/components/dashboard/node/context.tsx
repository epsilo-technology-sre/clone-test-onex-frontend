import React from 'react';
import { NodeData, NodeId } from '../type';

const log = require('debug')('context:NodeEdit');

export const NodeEditContext = React.createContext({
  onTextChange: (
    nodeData: NodeData,
    contentId: string,
    contentValue: string,
  ) => {
    log('onTextChange', { contentId, contentValue });
  },
  isEditMode: true,
  onUpdateCustomAttributes: (
    nodeData: NodeData,
    attributes: NodeData['customAttributes'],
  ) => {
    log('onUpdateCustomAttributes', nodeData, attributes);
  },
});
