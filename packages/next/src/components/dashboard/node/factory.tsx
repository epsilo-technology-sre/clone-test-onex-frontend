import { get } from 'lodash';
import { nanoid } from 'nanoid';
import React from 'react';
import ReactDOM from 'react-dom';
import { useDispatch } from 'react-redux';
import styled from 'styled-components';
import { DashboardItemContext } from '../context';
import { NodeData } from '../type';
import ChartLibs from './chartlibs';

// FIXME: inline editor only available on editmode
import { NodeEditContext } from './context';
import InlineEditor from './slate';
import clsx from 'clsx';

const bpWidth = [176, 224, 270, 416, Number.POSITIVE_INFINITY];
const bpHeight = [80, 128, 176, 272, Number.POSITIVE_INFINITY];

const NodeContainer = styled('div')`
  overflow: hidden;
  height: 100%;
  width: 100%;
  font-size: 16px;
  & [data-dbf-event-drilldown] {
    position: relative;
    &:hover .dot-connector {
      visibility: visible;
    }

    .connect-source & .dot-connector {
      visibility: visible;
    }
    & .dot-connector {
      cursor: pointer;
      position: absolute;
      width: 1em;
      height: 1em;
      background: rgba(237, 92, 16, 0.79);
      border-radius: 1em;
      left: calc(50% - 0.5em);
      top: calc(50% - 0.5em);
      visibility: hidden;
      z-index: 1;
    }
  }
`;

export function NodeFactory(props: {
  data: NodeData;
  idPrefix?: string;
  refreshTime?: number;
}) {
  const chartLib = ChartLibs.find(props.data.chartLibId).component;
  const chartLibId = props.data.chartLibId;
  const context = React.useContext(NodeEditContext);
  const rootNodeContext = React.useContext(DashboardItemContext);
  const containerRef = React.useRef<HTMLDivElement>();
  const [
    containerBreakpointClass,
    setContainerBreakpointClass,
  ] = React.useState({ width: null, height: null });

  React.useEffect(() => {
    if (context.isEditMode) {
      let mountedEls;
      let tid = window.setInterval(() => {
        if (containerRef.current) {
          window.clearInterval(tid);
          window.requestAnimationFrame(() => {
            mountedEls = setupInlineEditor({
              containerRef,
              chartLibId,
              rootNodeContext,
              context,
              nodeData: props.data,
            });
          });
        }
      }, 500);

      return () => {
        window.clearInterval(tid);
      };
    }
  }, [chartLibId]);


  React.useEffect(() => {
    let animationFrameIds = [];
    let ro = new ResizeObserver((entries) => {
      for (let entry of entries) {
        const cr = entry.contentRect;
        let width = cr.width;
        let height = cr.height;
        // let size = Math.min(cr.height, cr.width);

        // size = Math.max(14, chartScale * size);
        // if (size !== fontSize) {
        //   animationFrameIds.push(
        //     window.requestAnimationFrame(() => setFontSize(size)),
        //   );
        // }

        const bpWidthIndex = bpWidth.findIndex((i) => width < i);
        const bpHeightIndex = bpHeight.findIndex((i) => height < i);
        setContainerBreakpointClass({
          width: 'ep-sw-' + bpWidthIndex,
          height: 'ep-sh-' + bpHeightIndex,
        });
      }
    });

    // Observe one or multiple elements
    ro.observe(containerRef.current);

    return () => {
      ro.disconnect();
      animationFrameIds.forEach((id) =>
        window.cancelAnimationFrame(id),
      );
    };
  }, []);

  let renderChartLib = React.useMemo<JSX.Element>(() => {
    if (containerRef.current && props.data.chartLibId) {
      return chartLib.render(containerRef.current, props.data, null);
    } else {
      return null;
    }
  }, [props.data, containerRef.current, props.data.chartLibId]);

  return (
    <React.Fragment>
      <NodeContainer
        id={[
          props.idPrefix || '',
          'chart-container-',
          props.data.id,
        ].join('')}
        // style={{ fontSize: `${fontSize}px` }}
        style={{ fontSize: `14px` }}
        className={clsx(
          'single-chart-container',
          !!containerBreakpointClass.width &&
            containerBreakpointClass.width,
          !!containerBreakpointClass.height &&
            containerBreakpointClass.height,
        )}
        ref={containerRef}
      >
        <ErrorBoundary>{renderChartLib}</ErrorBoundary>
      </NodeContainer>
    </React.Fragment>
  );
}

function setupInlineEditor({
  containerRef,
  chartLibId,
  rootNodeContext,
  context,
  nodeData,
}) {
  let editableList = (containerRef.current as HTMLDivElement).querySelectorAll(
    '[data-dbf-text-editable]',
  );

  editableList;
  for (let el of editableList) {
    let contentId = el.getAttribute('data-dbf-text-editable');
    let defaultValue = get(
      nodeData,
      `customAttributes.customText.${contentId}`,
      [
        {
          type: 'paragraph',
          children: [
            {
              text: el.textContent,
            },
          ],
        },
      ],
    );

    if (chartLibId === 'textPlain') {
      el.setAttribute('style', 'height: 100%');
    }

    ReactDOM.render(
      <InlineEditor
        defaultValue={defaultValue}
        onFocus={() => {
          rootNodeContext.enableRootDraggable(false);
        }}
        onBlur={() => {
          rootNodeContext.enableRootDraggable(true);
        }}
        onTextChange={(value) => {
          context.onTextChange(nodeData, contentId, value);
        }}
      />,
      el,
    );
  }

  return editableList;
}

class ErrorBoundary extends React.Component {
  state = { error: false };

  constructor(props) {
    super(props);
    this.state = { error: false };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { error: true };
  }

  componentDidCatch(error, errorInfo) {
    console.error(error, errorInfo);
  }

  render() {
    const { children } = this.props;
    if (this.state.error) {
      return 'error...';
    }
    return children;
    return <div>componetn</div>;
  }
}
