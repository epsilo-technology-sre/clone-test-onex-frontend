import { Menu, MenuItem } from '@material-ui/core';
import SelectionArea from '@simonwep/selection-js';
import { debounce } from 'lodash';
import * as React from 'react';
import ReactDOM from 'react-dom';
import { useDispatch, useSelector, useStore } from 'react-redux';
import { actions } from './dashboard-redux';
import { DashboardState } from './type';

export function NodeSelection() {
  let store = useStore();
  let dispatch = useDispatch();
  const [state, setState] = React.useState({
    mouseX: null,
    mouseY: null,
  });

  let selectMultiple = React.useCallback(
    debounce(
      (elements: HTMLDivElement[]) => {
        dispatch(
          actions.selectMultiple({
            nodeIds: elements.map((e) =>
              e.getAttribute('data-node-id'),
            ),
          }),
        );
      },
      200,
      { trailing: true, maxWait: 100 },
    ),
    [],
  );

  React.useEffect(() => {
    const selection = new SelectionArea({
      document: window.document,
      selectables: ['.ep-dashboard-item'],
      singleTap: {
        allow: false,
        intersect: 'native',
      },
    })
      .on('beforestart', (event) => {
        let state: DashboardState = store.getState();
        console.info(state);
        if (
          state.mode.type === 'select' ||
          event.event.button === 2
        ) {
          return false;
        } else {
          return true;
        }
      })
      .on('move', (evt) => {
        let elements = evt.store.selected;
        if (elements.length > 0) {
          selectMultiple(elements);
        }
      })
      .on('stop', (evt) => {
        console.info('selected', evt.store);
        // dispatch(actions.select({nodeId: }))
      });

    window.document.body.addEventListener('contextmenu', (event) => {
      let state: DashboardState = store.getState();

      if (state.mode.type === 'select_multiple') {
        event.preventDefault();
        setState({
          mouseX: event.clientX - 2,
          mouseY: event.clientY - 4,
        });
      }
    });

    return () => {
      selection.destroy();
    };
  }, []);

  const handleClose = () => {
    setState({ mouseX: null, mouseY: null });
  };

  return ReactDOM.createPortal(
    <ContextMenu
      onClose={handleClose}
      mouseX={state.mouseX}
      mouseY={state.mouseY}
    />,
    window.document.body,
  );
}

function ContextMenu({ onClose, mouseX, mouseY }) {
  const mode = useSelector((state: DashboardState) => {
    return state.mode;
  });
  const dispatch = useDispatch();

  const handleClose = () => {
    onClose();
  };

  const handleGroupNodes = () => {
    if (mode.type === 'select_multiple') {
      dispatch(actions.groupNodes({ nodeIds: mode.activeNodes }));
      onClose();
    }
  };

  return (
    <Menu
      keepMounted
      open={mouseY !== null}
      onClose={onClose}
      anchorReference="anchorPosition"
      anchorPosition={
        mouseY !== null && mouseX !== null
          ? { top: mouseY, left: mouseX }
          : undefined
      }
    >
      <MenuItem onClick={handleGroupNodes}>Group</MenuItem>
    </Menu>
  );
}
