import React, { useState } from 'react';
import { omit, mapValues, get, debounce } from 'lodash';
import { set, merge } from 'lodash/fp';
import { Responsive as ResponsiveLayoutGrid } from 'react-grid-layout';
import GridProvider from './grid-adapter';
import 'react-grid-layout/css/styles.css';
import 'react-resizable/css/styles.css';
import { NodePanel } from './panel/editor-vertical';
import { DefaultFilters } from '../filter/one-vertical';
import {
  NodeLayout,
  DashboardBreakpoints,
  NodeData,
  DashboardState,
} from './type';
import { makeStyles } from '@material-ui/core';
import { NodeEditContext } from './node/context';
import { DashboardContext } from './context';
import { makeStore } from '@ep/one/src/utils';
import {
  reducer as dashboardReducer,
  actions,
  initState,
} from './dashboard-redux';
import { Provider, useDispatch, useSelector } from 'react-redux';
import DashboardItem from './dashboard-item';
import { NodeConnectors } from './connectors';
import { useHotkeys } from 'react-hotkeys-hook';
import { useOnClickOutside } from './hooks';
import { NodeSelection } from './node-selection';

const ReactGridLayout = GridProvider(ResponsiveLayoutGrid);
const DragField = ReactGridLayout;

type DashboardScreenLayout = NodeLayout;

type DashboardResponsiveLayoutType = {
  [key: DashboardBreakpoints]: DashboardScreenLayout[];
};

type DashboardProps = {
  layouts: DashboardResponsiveLayoutType;
  nodes: NodeData[];
  filterComponent?: React.Component<any>;
};

function DashboardFrame({
  layouts,
  nodes,
  filterComponent = DefaultFilters,
}: DashboardProps) {
  const [isDragging, setIsDragging] = useState(false);
  const [editMode, setEditMode] = useState<{
    isExpanded: boolean;
    nodeId: string | null;
    nodeData: NodeData | null;
  }>({ isExpanded: false, nodeId: null, nodeData: null });

  const { mode: dbMode } = useSelector((state: DashboardState) => {
    return {
      mode: state.mode,
      nodes: state.nodes,
      layouts: state.layouts,
    };
  });
  const dispatch = useDispatch();

  const statusRef = React.useRef({ isChangeBreakpoint: false });

  const classes = useStyle();
  const context = React.useContext(DashboardContext);
  const containerRef = React.useRef();
  useOnClickOutside(containerRef, () => {
    if (dbMode.activeNode !== null) {
      dispatch(actions.select({ nodeId: null }));
    }
  });

  let rLayouts = React.useMemo(
    () =>
      mapValues(layouts, (layout: any[]) =>
        layout.map((i: any) => ({ i: i.id, ...i.layout })),
      ),
    [layouts],
  );

  useHotkeys('esc', () => {
    dispatch(actions.select({ nodeId: null }));
  });

  React.useEffect(() => {
    console.info('effects.... update nodes', nodes);
    context.onUpdateNodes(nodes);
  }, [nodes]);

  let handleAddNewNode = ({ x, y }: { x: number; y: number }) => {
    let newNode = context.onAddNewNode(x, y);
    if (newNode) {
      dispatch(actions.addNewNode({ node: newNode }));
    }
  };

  let handleRemoveNode = (id) => {
    dispatch(actions.removeNode({ nodeId: id }));
    context.onRemoveNode(id);
  };

  let onBreakpointChange = (bp, cols) => {
    statusRef.current.isChangeBreakpoint = true;
    dispatch(actions.updateBreakpoint({ breakpoint: bp }));
    context.onBreakpointChange(bp, cols);
  };

  const handleLayoutChange = debounce(
    (newLayout: any, previousLayout: any) => {
      let nuLayouts = newLayout.map((i: any) => ({
        id: i.i,
        layout: omit(i, ['i']),
      }));
      if (!statusRef.current.isChangeBreakpoint) {
        console.info('onlayoutchange', nuLayouts);
        dispatch(actions.updateLayout({ layout: nuLayouts }));
        context.onLayoutChange(
          nuLayouts,
          mapValues(previousLayout, (layout) =>
            layout.map((i: any) => ({
              id: i.i,
              layout: omit(i, ['i']),
            })),
          ),
        );
      } else {
        statusRef.current.isChangeBreakpoint = false;
      }
    },
    200,
    { trailing: true },
  );

  const handleNodeRequestUpdate = (nodeId, payload) => {
    console.info('editmode', { nodeId, payload });
    if (get(payload, 'formControl.isExpanded', null) === false) {
      setEditMode((state) => ({ ...state, isExpanded: false }));
    } else {
      setEditMode({
        nodeData: payload,
        isExpanded: true,
        nodeId: nodeId,
      });
      // context.onNodeSubmitUpdate(nodeId, payload);
    }
  };

  const handleNodeSubmit = (nodeId, payload) => {
    console.info('handleNodeSubmit', { nodeId, payload });
    dispatch(actions.updateNode({ nodeId, data: payload }));
    context.onNodeSubmitUpdate(nodeId, payload);
  };

  const handleDuplicateNode = (nodeId) => {
    dispatch(actions.duplicateNode({ nodeId }));
  };

  return (
    <div
      className={nodes.length === 0 ? classes.dashboardBlank : ''}
      ref={containerRef}
    >
      <DashboardContext.Provider
        value={{
          ...context,
          onRemoveNode: handleRemoveNode,
          onNodeSubmitUpdate: handleNodeSubmit,
          onNodeRequestUpdate: handleNodeRequestUpdate,
          onDuplicateNode: handleDuplicateNode,
        }}
      >
        <NodeEditContext.Provider
          value={{
            isEditMode: true,
            onTextChange: debounce(
              (nodeData: NodeData, contentId, contentValue) => {
                let data = set(
                  `customAttributes.customText.${contentId}`,
                  contentValue,
                )(nodeData);
                handleNodeSubmit(data.id, data);
              },
              1000,
            ),
            onUpdateCustomAttributes: (nodeData, attributes) => {
              let customAttributes = merge(
                get(nodeData, 'customAttributes', {}),
                attributes,
              );
              handleNodeSubmit(nodeData.id, {
                ...nodeData,
                customAttributes,
              });
            },
          }}
        >
          <DragField
            isDraggable={dbMode.type !== 'connect'}
            isResizable={dbMode.type !== 'connect'}
            margin={12}
            containerPadding={[0, 0]}
            onAddNewNode={handleAddNewNode}
            onLayoutChange={handleLayoutChange}
            onBreakpointChange={onBreakpointChange}
            breakpoints={{
              lg: 1600,
              md: 960,
              sm: 768,
              xs: 480,
              xxs: 0,
            }}
            cols={{ lg: 24, md: 18, sm: 6, xs: 2, xxs: 2 }}
            onDrag={() => {
              if (!isDragging) {
                setIsDragging(true);
              }
            }}
            onDragStop={() => {
              console.info('dragstop');
              setIsDragging(false);
            }}
            autoSize={true}
            isBounded={false}
            onResizeStart={() => {
              console.info('resizestart');
              setIsDragging(true);
            }}
            onResizeStop={() => setIsDragging(false)}
            isDragging={isDragging}
            layouts={rLayouts}
            verticalCompact={true}
            draggableHandle={'.drag-enabled'}
            className={'dashboard-grid-layout'}
          >
            {nodes
              .filter((i) => !i.containerId)
              .map((i: NodeData) => (
                <div
                  key={i.id}
                  className={'ep-dashboard-item'}
                  data-node-id={i.id}
                >
                  <DashboardItem
                    key={i.id}
                    nodeData={i}
                  ></DashboardItem>
                </div>
              ))}
          </DragField>
          <NodePanel
            componentFilter={filterComponent}
            isExpanded={editMode.isExpanded}
            nodeId={dbMode.activeNode}
            onCancel={() =>
              setEditMode((state) => ({
                ...state,
                isExpanded: false,
              }))
            }
          />
        </NodeEditContext.Provider>
        <NodeConnectors></NodeConnectors>
        <NodeSelection></NodeSelection>
      </DashboardContext.Provider>
    </div>
  );
}

export default function DashboardFull(
  props: Omit<
    React.ComponentProps<typeof DashboardFrame>,
    'children'
  > & {
    nodes: NodeData[];
  },
) {
  const store = React.useMemo(
    () =>
      makeStore({
        storeName: 'dashboard-full',
        reducer: dashboardReducer,
        rootSaga: function* rootSaga() {
          console.info('dashboard => no saga setup yet');
        },
        initState: {
          ...initState,
          layouts: props.layouts,
          nodes: props.nodes,
        },
      }),
    [],
  );
  const init = React.useRef(false);

  React.useEffect(() => {
    if (init.current === false) {
      store.dispatch(
        actions.set({ layouts: props.layouts, nodes: props.nodes }),
      );
      init.current = true;
    }
  }, [props.layouts, props.nodes]);

  return (
    <Provider store={store}>
      <DashboardContainer {...props}></DashboardContainer>
    </Provider>
  );
}

function DashboardContainer(props) {
  const { layouts, nodes } = useSelector((state: DashboardState) => {
    return { layouts: state.layouts, nodes: state.nodes };
  });

  return (
    <DashboardFrame
      {...props}
      layouts={layouts}
      nodes={nodes}
    ></DashboardFrame>
  );
}

const useStyle = makeStyles(() => {
  return {
    dashboardBlank: {
      position: 'relative',
      '& > div': {
        position: 'relative',
        zIndex: 1,
      },
      '&:after': {
        top: 0,
        left: 0,
        zIndex: 0,
        width: '100%',
        height: '100%',
        position: 'absolute',
        content: '"Double click."',
        fontSize: '5em',
        color: '#eee',
      },
    },
    banner: {
      position: 'fixed',
      top: 56,
      zIndex: 1300,
    },
  };
});

// export function GridItem({ item }) {
//   const ref = React.createRef();
//   const handleToggleFocus = (focus) => {
//     // if (ref.current && focus) {
//     //   let parent = ref.current.parentElement;
//     //   console.info(parent);
//     //   parent.style.zIndex = 10;
//     // } else {
//     //   let parent = ref.current.parentElement;
//     //   parent.style.zIndex = undefined;
//     // }
//   };

//   return (
//     <DashboardItem
//       ref={ref}
//       key={item.id}
//       onToggleFocus={handleToggleFocus}
//       nodeData={item}
//     ></DashboardItem>
//   );
// }
