import React from 'react';
import { DashboardBreakpoints, NodeData, NodeId, NodeLayout } from './type';

type DashboardScreenLayout = NodeLayout;

type DashboardResponsiveLayoutType = {
  [key: DashboardBreakpoints]: DashboardScreenLayout[];
};

type DashboardContextType = {
  onAddNewNode: (x: number, y: number) => NodeData;
  onRemoveNode: (id: string) => void;
  onLayoutChange: (
    newLayout: DashboardScreenLayout[],
    previousLayout: DashboardResponsiveLayoutType,
  ) => void;
  onBreakpointChange: (
    bp: DashboardBreakpoints,
    cols: number,
  ) => void;
  onNodeRequestUpdate: (nodeId: string, payload: NodeData) => void;
  onNodeSubmitUpdate: (nodeId: string, payload: any) => any;
  onNodeRequestConnection?: (nodeData: NodeData) => any;
  onUpdateNodes: (nodes: NodeData[]) => void;
  onDuplicateNode: (nodeId: NodeId) => void;
};

export const DashboardContext = React.createContext<DashboardContextType>(
  {
    onUpdateNodes: (nodes) => {
      return nodes;
    },
    onAddNewNode: (x, y) => {
      console.info({ x, y });
      return null;
    },
    onRemoveNode: (id) => {
      console.info({ id });
    },
    onLayoutChange: (newLayout, prevLayout) => {
      console.info({ newLayout, prevLayout });
    },
    onBreakpointChange: (bp, cols) => console.info({ bp, cols }),
    onNodeRequestUpdate: (nodeId, payload) => {
      console.info({ nodeId, payload });
    },
    onNodeSubmitUpdate: (nodeId, payload) => {
      console.info({ nodeId, payload });
    },
    onNodeRequestConnection: (nodeData) => {
      console.info('node Request connection', { nodeData });
    },
    onDuplicateNode: (nodeId) => {
      console.info("duplicate node", nodeId);
    }
  },
);

export const DashboardItemContext = React.createContext({
  enableRootDraggable: (status: boolean) => {},
});
