import { EventEmitter } from 'events';

export type DashboardBreakpoints = keyof Dashboard['layouts'];

export type NodeLayout = {
  id: NodeId;
  layout: {
    x: number;
    y: number;
    h: number;
    w: number;
    isBounded: boolean;
    isDraggable: boolean;
    isResizable: boolean;
    maxH: number;
    maxW: number;
    minH: number;
    minW: number;
    moved: boolean;
    resizeHandles: boolean;
    static: false;
  };
};

export type NodeId = string;

export type NodeConnection = {
  eventId: string;
  nodeId: NodeId;
};

type chartLibId = string;
type customAttributesType = {
  [key: string]: any;
};

export type NodeData = {
  id: NodeId;
  chartLibId?: chartLibId;
  mainAttributes?: {
    dimensions: {
      [key: string]: number[] | string[];
    };
    metrics: string[];
    cohort: string[];
  };
  customAttributes?: customAttributesType;
  containerId?: NodeId;
  childList?: NodeConnection[];
  parentList?: NodeConnection[];
  [key: string]: any;
};

type ChartLibConfigureResult = any;

export interface ChartLibComponent {
  render: (
    dom: HTMLDivElement,
    data: NodeData,
    eventBus: EventEmitter,
  ) => JSX.Element;
  renderConfigurationForm: (
    dom: HTMLDivElement,
    data: customAttributesType,
    submitHandle: (
      attr: customAttributesType,
    ) => Promise<ChartLibConfigureResult>,
  ) => JSX.Element;

  getInteractionEvents?: () => {
    eventId: string;
    label: string;
  }[];
}

export type ChartLib = {
  chartLibId: chartLibId;
  label: string;
  component: ChartLibComponent;
  scale: number;
};

export type Dashboard = {
  title?: string;
  layouts: {
    md: any[];
    lg?: any[];
    sm?: any[];
    xs?: any[];
    xxs?: any[];
  };
  layoutColumns?: {
    [key: string]: number;
  };
  nodes: NodeData[];
  presetEid?: string;
  breakpoint?: string;
  baseBreakpoint?: string;
  demoMetricRange?: demoMetricRangeType[];
};

export type demoMetricRangeType = {
  metric: string;
  type: string;
  from: number;
  to: number;
};

type modeGeneral = {
  type: 'select' | 'edit' | 'edit_text';
  activeNode: NodeId;
};
type modeConnect = {
  type: 'connect';
  activeNode: NodeId;
  activeNodeData: NodeData;
  eventId: string;
  drilldownPointId: string;
};

type modeGroupSelect = {
  type: 'select_multiple';
  activeNodes: NodeId[];
};

export type DashboardState = Dashboard & {
  mode?: modeGeneral | modeConnect | modeGroupSelect;
};

export type NodePanelProps = {
  nodeId: string;
  isExpanded: boolean;
  nodeData: NodeData;
  componentFilter:
    | React.Component<any>
    | React.FunctionComponent<any>;
  onCancel: () => void;
};

export type GridAdapterContextType = {
  cols: number;
  squareWidth: number;
  margin: number;
};
