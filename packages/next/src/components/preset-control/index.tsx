import { API_INSIGHT_URL } from '@ep/one/global';
import { request } from '@ep/one/src/utils';
import { ButtonFileDownload } from '@ep/shopee/src/components/common/button/button-file-download';
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from '@material-ui/core';
import { Formik } from 'formik';
import { get } from 'lodash';
import moment from 'moment';
import React from 'react';
import { BsPencil, BsThreeDotsVertical, BsX } from 'react-icons/bs';
import { toast } from 'react-toastify';
import {
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  UncontrolledDropdown,
} from 'reactstrap';
import styled from 'styled-components';
import { MetricRangeModal } from '../common/metric-range-modal';
import { CustomMetricList } from '../custom-metric';

const PresetContainer = styled('div')`
  display: flex;
  .btn-export {
    margin-right: 1em;
  }
  .menu-content {
    max-height: 50vh;
    overflow: auto;
  }
  .menu-item {
    width: 40vw;
    display: flex;
    align-items: center;

    & .btn {
      margin: 0 0.5em;
    }
  }
`;

export function PresetControl({
  presetList,
  onRefreshPresetList,
  onClickPreset,
  currentPreset,
  onClickAddNew,
  onUpdatePreset,
  presetRepo,
}) {
  let [exportPassword, setExportPassword] = React.useState(null);
  let [openExportModal, setOpenExportModal] = React.useState(false);
  let [openImportModal, setOpenImportModal] = React.useState(false);
  let [openCreateModal, setOpenCreateModal] = React.useState(false);
  let [
    openMetricRangeModal,
    setOpenMetricRangeModal,
  ] = React.useState(false);
  let [
    openCustomMetricList,
    setOpenCustomMetricList,
  ] = React.useState(false);

  let [openRemoveModal, setOpenRemoveModal] = React.useState({
    display: false,
    item: null,
  });

  let [openEditModal, setOpenEditModal] = React.useState({
    display: false,
    item: null,
  });

  const [metricRanges, setMetricRanges] = React.useState([]);

  let [newPresetName, setNewPresetName] = React.useState('');

  const refImportForm = React.useRef(null);

  React.useEffect(() => {
    if (openMetricRangeModal) {
      setMetricRanges(presetRepo.getDemoMetricRange());
    }
  }, [openMetricRangeModal]);

  const handleImportPreset = () => {
    let formData = new FormData(refImportForm.current);
    request
      .upload(
        API_INSIGHT_URL + '/api/insight/import/preset',
        formData,
      )
      .then((res) => {
        toast.success('Preset import successfully.');
        setOpenImportModal(false);
        onRefreshPresetList();
      });
  };

  const handleUpdatePreset = ({ title, presetEid }) => {
    request
      .post(API_INSIGHT_URL + '/api/insight/presets', {
        title,
        preset_eid: presetEid,
      })
      .then((res) => {
        onRefreshPresetList();
        setOpenEditModal((state) => ({ ...state, display: false }));
      });
  };

  const handleRemovePreset = (presetEid) => {
    request
      .deleteFetch(
        API_INSIGHT_URL + '/api/insight/presets/' + presetEid,
      )
      .then(() => {
        onRefreshPresetList();
        setOpenRemoveModal((state) => ({ ...state, display: false }));
      });
  };

  const handleCreatePreset = () => {
    onClickAddNew(newPresetName);
    setOpenCreateModal(false);
  };

  const handleUpdateMetricRange = (range) => {
    // window.localStorage.setItem(
    //   'epi_data_range_' + currentPreset,
    //   JSON.stringify(range),
    // );
    onUpdatePreset({ demoMetricRange: range });
  };

  const handleSaveMetricRange = (metricRange) => {
    handleUpdateMetricRange(metricRange);
    setOpenMetricRangeModal(false);
  };

  return (
    <PresetContainer className={'noprint'}>
      <UncontrolledDropdown>
        <DropdownToggle
          size="sm"
          className="btn btn-ghost-primary"
          style={{ border: 'none' }}
        >
          <BsThreeDotsVertical color={'#253746'} />
        </DropdownToggle>
        <DropdownMenu right>
          <div className="menu-content">
            {presetList.map((p, index) => {
              return (
                <DropdownItem
                  key={p.presetEid}
                  onClick={() => {
                    onClickPreset(p);
                  }}
                  className="menu-item"
                >
                  <span>{p.title || 'Preset ' + (index + 1)}</span>
                  <div
                    className="btn btn-icon btn-sm btn-ghost-secondary"
                    onClick={(evt) => {
                      setOpenEditModal({ display: true, item: p });
                      evt.stopPropagation();
                    }}
                  >
                    <BsPencil />
                  </div>
                  <span style={{ flexGrow: 1 }}></span>
                  <span className={'text-muted'}>
                    {moment(p.updatedAt).format(
                      'YYYY MMMM DD HH:mm:ss',
                    )}
                  </span>
                  <div
                    className="btn btn-icon btn-sm btn-ghost-secondary"
                    onClick={(evt) => {
                      setOpenRemoveModal({ display: true, item: p });
                      evt.stopPropagation();
                    }}
                  >
                    <BsX />
                  </div>
                </DropdownItem>
              );
            })}
          </div>
          <DropdownItem divider />
          <DropdownItem onClick={() => setOpenImportModal(true)}>
            Import preset
          </DropdownItem>
          <DropdownItem
            onClick={() => {
              setOpenCreateModal(true);
            }}
          >
            Create new preset
          </DropdownItem>
          <DropdownItem
            onClick={() => {
              setOpenExportModal(true);
            }}
          >
            Export preset
          </DropdownItem>
          <DropdownItem onClick={() => setOpenMetricRangeModal(true)}>
            Manage metric value range
          </DropdownItem>
          <DropdownItem onClick={() => setOpenCustomMetricList(true)}>
            Manage custom metrics
          </DropdownItem>
        </DropdownMenu>
      </UncontrolledDropdown>
      <CustomMetricList
        open={openCustomMetricList}
        onClose={() => setOpenCustomMetricList(false)}
      />
      <Dialog
        open={openExportModal}
        onClose={() => {
          setOpenExportModal(false);
        }}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">
          Export with password
        </DialogTitle>
        <DialogContent>
          <div className="form-group">
            <label className="input-label" htmlFor="presetpassword">
              Password
            </label>
            <input
              type="password"
              id="presetpassword"
              html-autocomplete="off"
              defaultValue=""
              className="form-control"
              onChange={(evt) => setExportPassword(evt.target.value)}
            />
          </div>
        </DialogContent>
        <DialogActions>
          <button
            className="btn btn-outline-secondary"
            onClick={() => setOpenExportModal(false)}
          >
            Cancel
          </button>
          <ButtonFileDownload
            label="Download"
            color={'secondary'}
            getFileName={() => 'preset.epsilo'}
            downloadFetch={() =>
              request.download(
                API_INSIGHT_URL +
                  `/api/insight/presets/${currentPreset}/export`,
                {
                  password: exportPassword,
                },
                'POST',
              )
            }
            onAfterDownload={() => {
              setOpenExportModal(false);
            }}
          ></ButtonFileDownload>
        </DialogActions>
      </Dialog>
      <MetricRangeModal
        items={metricRanges}
        isOpen={openMetricRangeModal}
        onSave={handleSaveMetricRange}
        onSetOpenModal={setOpenMetricRangeModal}
      ></MetricRangeModal>

      <Dialog
        open={openImportModal}
        onClose={() => {
          setOpenImportModal(false);
        }}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">
          Import your preset
        </DialogTitle>
        <DialogContent>
          <form id="preset-import-form" ref={refImportForm}>
            <div className="form-group">
              <label className="input-label" htmlFor="presetpassword">
                Choose file to import
              </label>
              <input
                type="file"
                id="preset-file-import"
                name="import"
              />
            </div>
            <div className="form-group">
              <label className="input-label" htmlFor="presetpassword">
                Password
              </label>
              <input
                type="password"
                id="presetpassword"
                html-autocomplete="off"
                defaultValue=""
                className="form-control"
                name="password"
                onChange={(evt) =>
                  setExportPassword(evt.target.value)
                }
              />
            </div>
          </form>
        </DialogContent>
        <DialogActions>
          <button
            className="btn btn-outline-secondary"
            onClick={() => setOpenImportModal(false)}
          >
            Cancel
          </button>
          <button
            className="btn btn-primary"
            onClick={() => {
              handleImportPreset();
            }}
          >
            Import
          </button>
        </DialogActions>
      </Dialog>
      <Dialog
        open={openCreateModal}
        onClose={() => {
          setOpenCreateModal(false);
        }}
      >
        <DialogTitle id="form-dialog-title">New Preset</DialogTitle>
        <DialogContent>
          <div className="form-group">
            <label className="input-label" htmlFor="newpresetname">
              Name
            </label>
            <input
              type="text"
              id="newpresetname"
              html-autocomplete="off"
              defaultValue=""
              className="form-control"
              onChange={(evt) => setNewPresetName(evt.target.value)}
            />
          </div>
        </DialogContent>
        <DialogActions>
          <button
            className="btn btn-outline-secondary"
            onClick={() => setOpenCreateModal(false)}
          >
            Cancel
          </button>
          <button
            className="btn btn-primary"
            onClick={handleCreatePreset}
          >
            Submit
          </button>
        </DialogActions>
      </Dialog>
      <Dialog
        open={openEditModal.display}
        onClose={() => {
          setOpenEditModal((state) => ({ ...state, display: false }));
        }}
      >
        <DialogTitle id="form-dialog-title">
          Update preset name
        </DialogTitle>
        <Formik
          initialValues={{
            name: get(openEditModal, 'item.title', ''),
            presetEid: get(openEditModal, 'item.presetEid', ''),
          }}
          onSubmit={(values) => {
            handleUpdatePreset({
              title: values.name,
              presetEid: values.presetEid,
            });
          }}
        >
          {({ values, setFieldValue, submitForm }) => (
            <React.Fragment>
              <DialogContent>
                <div className="form-group">
                  <label
                    className="input-label"
                    htmlFor="newpresetname"
                  >
                    Name
                  </label>
                  <input
                    type="text"
                    html-autocomplete="off"
                    value={values.name}
                    className="form-control"
                    onChange={(evt) => {
                      setFieldValue('name', evt.target.value);
                    }}
                  />
                </div>
              </DialogContent>
              <DialogActions>
                <button
                  className="btn btn-outline-secondary"
                  onClick={() =>
                    setOpenEditModal((state) => ({
                      ...state,
                      display: false,
                    }))
                  }
                >
                  Cancel
                </button>
                <button
                  className="btn btn-primary"
                  onClick={() => submitForm()}
                >
                  Submit
                </button>
              </DialogActions>
            </React.Fragment>
          )}
        </Formik>
      </Dialog>
      <Dialog
        open={openRemoveModal.display}
        onClose={() => {
          setOpenRemoveModal((state) => ({
            ...state,
            display: false,
          }));
        }}
      >
        <DialogContent>
          <p>
            Are you sure to delete `
            {get(openRemoveModal, 'item.title')}` ?
          </p>
        </DialogContent>
        <DialogActions>
          <button
            className="btn btn-outline-secondary"
            onClick={() =>
              setOpenRemoveModal((state) => ({
                ...state,
                display: false,
              }))
            }
          >
            Cancel
          </button>
          <button
            className="btn btn-primary"
            onClick={() =>
              handleRemovePreset(
                get(openRemoveModal, 'item.presetEid'),
              )
            }
          >
            Submit
          </button>
        </DialogActions>
      </Dialog>
    </PresetContainer>
  );
}
