import { get, random } from 'lodash';
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';

export function DemoDataType({ data, onSubmit }) {
  return (
    <Form>
      <FormGroup>
        <Label for="chartlib_demodata_type">Data type</Label>
        <Input
          type="select"
          name="position"
          id="chartlib_divider_position"
          value={get(data, 'demoDataType', 'currency')}
          onChange={(evt) => {
            console.info(evt.target.value);
            onSubmit({ demoDataType: evt.target.value });
          }}
        >
          <option value={'currency'}>Currency</option>
          <option value={'number'}>Number</option>
          <option value={'percent'}>%</option>
        </Input>
      </FormGroup>
    </Form>
  );
}

export function useDemoData(data) {
  let currency = 'USD';
  let value;

  const demoDataType = get(
    data,
    'customAttributes.demoDataType',
    'currency',
  );

  switch (demoDataType) {
    case 'number':
      currency = '';
      value = random(10_000, 1000_000) ;
      break;
    case 'percent':
      currency = '%';
      value = Math.floor(Math.random() * 1000) / 10;
      break;
    default:
      currency = 'USD';
      value = Math.floor(Math.random() * 1_0000_000);
  }

  return {
    currency,
    value,
  }


}
