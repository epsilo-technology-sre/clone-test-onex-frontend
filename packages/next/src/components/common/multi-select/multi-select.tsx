import { ButtonUI } from '@ep/shopee/src/components/common/button';
import { COLORS } from '@ep/shopee/src/constants';
import Box from '@material-ui/core/Box';
import Checkbox from '@material-ui/core/Checkbox';
import InputAdornment from '@material-ui/core/InputAdornment';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Popover from '@material-ui/core/Popover';
import {
  createStyles,
  makeStyles,
  Theme,
  useTheme,
  withStyles,
} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import SearchIcon from '@material-ui/icons/Search';
import clsx from 'clsx';
import PopupState, {
  bindPopover,
  bindTrigger,
} from 'material-ui-popup-state';
import React, { useEffect, useState } from 'react';
import { Button } from 'reactstrap';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    body: {
      padding: 19,
    },
    checkboxWrapper: {
      maxHeight: 300,
      paddingTop: '1rem',
      overflow: 'auto',
    },
    footer: {
      display: 'flex',
      justifyContent: 'flex-end',
      padding: theme.spacing(1),
      borderTop: '2px solid #E4E7E9',
    },
    btnDropdown: {
      '.btn&': {
        display: 'flex',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderColor: '#fff',
      },
    },
    itemName: {
      paddingLeft: 5,
    },
    popoverPaper: {
      boxShadow: '0px 6px 12px rgba(140, 152, 164, 0.08)',
    }
  }),
);

const SearchBox = withStyles({
  root: {
    marginRight: 5,
    width: '100%',
    '&:hover .MuiOutlinedInput-notchedOutline': {
      borderColor: '#C2C7CB',
    },
    '&.MuiOutlinedInput-adornedStart': {
      paddingLeft: 0,
    },
  },
  notchedOutline: {
    border: 'none',
  },
  input: {
    padding: 8,
    fontSize: 14,
  },
})(OutlinedInput);

export const MultiSelectUI = (props: any) => {
  const classes = useStyles();
  const theme = useTheme();
  const [checkAll, setCheckAll] = useState(false);
  const [searchText, setSearchText] = useState('');
  const [selectingItems, setSelectingItems] = useState([]);
  const [displayingItems, setDisplayingItems] = useState([]);

  const {
    prefix = '',
    suffix = '',
    items,
    selectedItems,
    onSaveChange,
    className,
    hasFooter = false,
  } = props;

  useEffect(() => {
    const result = searchText
      ? items.filter((item) =>
          item.text.toLowerCase().includes(searchText.toLowerCase()),
        )
      : items;
    setDisplayingItems(result);
  }, [items, searchText]);

  useEffect(() => {
    setSelectingItems(selectedItems);
    setCheckAll(selectedItems.length === items.length);
  }, [selectedItems]);

  const handleChangeSearchText = (e: any) => {
    e.stopPropagation();
    setSearchText(e.target.value);
  };

  const handleCheckAll = () => {
    const nexState = !checkAll;
    const newSelectingItems = nexState ? items : [];
    setSelectingItems(newSelectingItems);
    setCheckAll(nexState);
    if (!hasFooter) {
      onSaveChange(newSelectingItems);
    }
  };

  const handleCheckItem = (menuItem: any) => {
    const newSelectingItems = [...selectingItems];

    const index = selectingItems.findIndex(
      (item) => item.id === menuItem.id,
    );
    if (index === -1) {
      newSelectingItems.push(menuItem);
    } else {
      newSelectingItems.splice(index, 1);
    }

    setSelectingItems(newSelectingItems);
    setCheckAll(newSelectingItems.length === items.length);
    if (!hasFooter) {
      onSaveChange(newSelectingItems);
    }
  };

  const applyChange = (popupState: any) => {
    popupState.close();
    onSaveChange(selectingItems);
  };

  const TriggerButton = () => {
    let display = '';
    if (selectedItems.length === items.length) {
      display = 'All';
    } else {
      display =
        selectedItems.length === 1
          ? selectedItems[0].text
          : `${selectedItems.length} selected ${suffix}`;
    }
    return (
      <Box>
        {display} {prefix && <span>{prefix}</span>}
      </Box>
    );
  };

  const isChecked = (target: any) => {
    return selectingItems.some((item) => item.id === target.id);
  };

  return (
    <PopupState variant="popover">
      {(popupState) => {
        const openIcon = popupState.isOpen ? (
          <ExpandLessIcon style={{ fontSize: 12 }} />
        ) : (
          <ExpandMoreIcon style={{ fontSize: 12 }} />
        );
        return (
          <Box ml={1} className={className}>
            <button
              type="button"
              className={clsx(
                'btn btn-light btn-xs',
                classes.btnDropdown,
              )}
              {...bindTrigger(popupState)}
            >
              <TriggerButton /> {openIcon}
            </button>
            <Popover
              {...bindPopover(popupState)}
              classes={{
                paper: classes.popoverPaper,
              }}
              elevation={0}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
              }}
            >
              <Box className={classes.body}>
                <Box>
                  <SearchBox
                    id="multi-select-searchbox"
                    placeholder="Search"
                    value={searchText}
                    onChange={handleChangeSearchText}
                    classes={{
                    }}
                    startAdornment={
                      <InputAdornment position="start">
                        <SearchIcon fontSize="small" />
                      </InputAdornment>
                    }
                    labelWidth={0}
                    autoComplete="off"
                  ></SearchBox>
                </Box>
                <Box className={classes.checkboxWrapper}>
                  <Box>
                    <label onClick={handleCheckAll}>
                      <input type="checkbox" checked={checkAll} />{' '}
                      <span className={classes.itemName}>
                        {`All ${suffix}`}
                      </span>
                    </label>
                  </Box>
                  {displayingItems.map((item: any, index: number) => (
                    <Box mt={'0.5rem'} key={item.id}>
                      <label onClick={() => handleCheckItem(item)}>
                        <input
                          type="checkbox"
                          checked={isChecked(item)}
                        />{' '}
                        <span className={classes.itemName}>
                          {item.text}
                        </span>
                      </label>
                    </Box>
                  ))}
                </Box>
              </Box>
            </Popover>
          </Box>
        );
      }}
    </PopupState>
  );
};
