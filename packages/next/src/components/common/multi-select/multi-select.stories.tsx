import React, { useState } from 'react';
import { MultiSelectUI } from './multi-select';

export default {
  title: 'Shopee/MultiSelect',
};

export const MultiSelect = () => {
  const countries = [
    { id: 1, text: 'Vietnam' },
    { id: 2, text: 'Singapore' },
    { id: 3, text: 'Thailand' },
    { id: 4, text: 'Maylaysia' },
    { id: 5, text: 'Phillipine' },
  ];

  const [selectedCountries, setSelectedCountries] = useState(countries);

  const saveChangeCountries = (selected: any) => {
    console.log('UUUUU', selected)
    setSelectedCountries(selected);
  }

  return (
    <div>
      <MultiSelectUI
        prefix="Country"
        suffix="countries"
        items={countries} 
        selectedItems={selectedCountries}
        onSaveChange={saveChangeCountries}
      ></MultiSelectUI>
    </div>
      
  );
};
