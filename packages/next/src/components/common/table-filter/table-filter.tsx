import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Pagination from '@material-ui/lab/Pagination';
import clsx from 'clsx';
import React, { useState } from 'react';

const useStyles = makeStyles({
  root: {
    padding: '10px 16px',
    fontWeight: 600,
    color: '#596772',
    backgroundColor: '#fff',
    position: 'sticky',
    bottom: 0,
    borderTop: '1px solid #E4E7E9',
    // zIndex: 5,
  },
  customPage: {
    width: 50,
  },
  total: {
    color: '#8C98A4',
    fontSize: 14,
    fontWeight: 'normal',
  },
  pageSize: {
    fontSize: 14,
    fontWeight: 'normal',
    color: '#596772',
    '& select': {
      border: '1px solid #F6F7F8',
      borderRadius: 4,
      padding: 5,
      background: '#F6F7F8',
    },
  },
  leftActions: {
    textAlign: 'left',
  },
  centerActions: {
    textAlign: 'center',
  },
  rightActions: {
    textAlign: 'right',
    color: '#596772',
    fontWeight: 'normal',
  },
  contained: {
    background: '#F6F7F8',
    marginLeft: '5px',
    boxShadow: 'none !important',
  },
});

const NumberTextbox = withStyles({
  root: {
    width: 50,
    '& input': {
      padding: '5px',
    },
  },
})(TextField);

const StyledPagination = withStyles({
  ul: {
    '& .Mui-selected': {
      background: '#253746',
      color: '#ffffff',
      '&:hover': {
        background: '#253746',
      },
    },
  },
})(Pagination);

export const TableFilterUI = (props: any) => {
  const classes = useStyles();
  const {
    resultTotal,
    page,
    pageSize,
    onChangePage,
    onChangePageSize,
  } = props;
  const [pageSizes] = useState([10, 20, 50]);
  const pageCount = Math.ceil(resultTotal / pageSize);

  const [customPage, setCustomPage] = useState(1);

  const handleChangePageSize = (event: any) => {
    onChangePageSize(Number(event.target.value));
  };

  const handleChangePage = (
    e: React.ChangeEvent<unknown>,
    value: number,
  ) => {
    onChangePage(value);
  };

  const handleKeyUp = (event: any) => {
    if (event.key === 'Enter') {
      onChangePage(Number(customPage));
    }
  };

  const handleChangeCustomPage = (event: any) => {
    let value = event.target.value;
    if (0 < value && value <= pageCount) {
      setCustomPage(value);
    }
  };

  const handleSubmitChangeCustomPage = () => {
    onChangePage(Number(customPage));
  };

  return (
    <Grid container className={clsx(classes.root, 'noprint')}>
      <Grid item xs={4} className={classes.leftActions}>
        <Box component="span" mr={2} className={classes.total}>
          {resultTotal} Results
        </Box>
        <Box component="span" className={classes.pageSize}>
          Row{' '}
          <select
            name="page-size"
            value={pageSize}
            onChange={handleChangePageSize}
          >
            {pageSizes.map((item, index) => (
              <option key={index} value={item}>
                {item}
              </option>
            ))}
          </select>
        </Box>
      </Grid>
      <Grid item container xs={4} justify="center">
        {pageCount > 0 && (
          <StyledPagination
            shape="rounded"
            count={pageCount}
            page={page}
            size="small"
            onChange={handleChangePage}
          />
        )}
      </Grid>
      <Grid item xs={4} className={classes.rightActions}>
        Go to page{' '}
        <NumberTextbox
          type="number"
          variant="outlined"
          value={customPage}
          InputProps={{
            inputProps: { min: 1, max: pageCount },
          }}
          className={classes.customPage}
          onChange={handleChangeCustomPage}
          onKeyUp={handleKeyUp}
        >
          {' '}
        </NumberTextbox>
        <Button
          variant="contained"
          component="span"
          className={classes.contained}
          size="small"
          onClick={handleSubmitChangeCustomPage}
        >
          Go
        </Button>
      </Grid>
    </Grid>
  );
};
