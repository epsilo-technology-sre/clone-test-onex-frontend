import React from 'react'
import Paper from '@material-ui/core/Paper'
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'
import { TableFilterUI } from './table-filter'

export default {
  title: 'Shopee/TableFilter'
}

export const Main = () => {
  const total = 165;
  const [page, setPage] = React.useState(1)
  const [pageSize, setPageSize] = React.useState(10)

  const handleChangePage = (value: any) => {
    setPage(value);
  }

  const handleChangePageSize = (value: any) => {
    setPageSize(value);
  }

  return (
    <Paper>
      <Box p={3}>
        <Box>
          <TableFilterUI 
            resultTotal={total}
            page={page} 
            pageSize={pageSize}
            onChangePage={handleChangePage}
            onChangePageSize={handleChangePageSize}
          ></TableFilterUI>
        </Box>
        <Box mt={2}>
          Page size: <b>{pageSize}</b> 
        </Box>
        <Box>
          Current page: <b>{page}</b>
        </Box>
      </Box>
    </Paper>
  )
}