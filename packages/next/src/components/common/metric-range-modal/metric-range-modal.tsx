import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  MenuItem,
  Select,
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import React from 'react';
import { usePagination, useRowSelect, useTable } from 'react-table';
import styled from 'styled-components';
import { CHART_DATA_TYPE } from '../../dashboard/hooks/use-data-query';

const Styles = styled.div`
  .action-btn {
    margin-right: 5px;
  }

  table {
    border-spacing: 0;
    border: 1px solid #a7a7a7;

    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th,
    td {
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid #a7a7a7;
      border-right: 1px solid #a7a7a7;

      :last-child {
        border-right: 0;
      }

      input {
        font-size: 1rem;
        padding: 0;
        margin: 0;
        border: 0;
      }
    }

    th {
      text-align: center;
      font-weight: bold;
    }
  }
`;

const EditableCell = ({
  value: initialValue,
  row: { index },
  column: { id },
  updateMyData, // This is a custom function that we supplied to our table instance
}) => {
  const [value, setValue] = React.useState(initialValue);

  const onChange = (e) => {
    setValue(e.target.value);
  };

  const onBlur = () => {
    updateMyData(index, id, value);
  };

  React.useEffect(() => {
    setValue(initialValue);
  }, [initialValue]);

  return <input value={value} onChange={onChange} onBlur={onBlur} />;
};

const ListboxCell = (props) => {
  const { value, onChange } = props.value;
  const { index } = props.row;
  const [types, setTypes] = React.useState(
    Object.values(CHART_DATA_TYPE),
  );
  const handleChange = (evt) => {
    onChange(index, evt.target.value);
  };
  return (
    <Select
      labelId="demo-simple-select-label"
      id="demo-simple-select"
      value={value}
      onChange={handleChange}
    >
      {types.map((type) => (
        <MenuItem value={type}>{type}</MenuItem>
      ))}
    </Select>
  );
};

const IndeterminateCheckbox = React.forwardRef(
  ({ indeterminate, ...rest }, ref) => {
    const defaultRef = React.useRef();
    const resolvedRef = ref || defaultRef;

    React.useEffect(() => {
      resolvedRef.current.indeterminate = indeterminate;
    }, [resolvedRef, indeterminate]);

    return (
      <>
        <input type="checkbox" ref={resolvedRef} {...rest} />
      </>
    );
  },
);

const defaultColumn = {
  Cell: EditableCell,
};

function Table({ columns, data, updateMyData, onSelectItem }) {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    selectedFlatRows,
  } = useTable(
    {
      columns,
      data,
      defaultColumn,
      updateMyData,
      getRowId: React.useCallback((row, index) => index, []),
    },
    usePagination,
    useRowSelect,
    (hooks) => {
      hooks.visibleColumns.push((columns) => [
        {
          id: 'selection',
          Header: ({ getToggleAllPageRowsSelectedProps }) => (
            <div>
              <IndeterminateCheckbox
                {...getToggleAllPageRowsSelectedProps()}
              />
            </div>
          ),
          Cell: ({ row }) => (
            <div>
              <IndeterminateCheckbox
                {...row.getToggleRowSelectedProps()}
              />
            </div>
          ),
        },
        ...columns,
      ]);
    },
  );

  React.useEffect(() => {
    if (onSelectItem) {
      onSelectItem(selectedFlatRows);
    }
  }, [selectedFlatRows]);

  return (
    <>
      <table {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th {...column.getHeaderProps()}>
                  {column.render('Header')}
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {page.map((row, i) => {
            prepareRow(row);
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map((cell) => {
                  return (
                    <td {...cell.getCellProps()}>
                      {cell.render('Cell')}
                    </td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

interface DataFormatModalProps {
  items: Array<Object>;
  isOpen: boolean;
  onSetOpenModal: Function;
  onSave: Function;
}

export const MetricRangeModal = ({
  items,
  isOpen,
  onSetOpenModal,
  onSave,
}: DataFormatModalProps) => {
  const columns = React.useMemo(
    () => [
      {
        Header: 'Metric',
        accessor: 'metric',
      },
      {
        Header: 'Type',
        accessor: (row) => ({
          value: row.type,
          onChange: handleChangeType,
        }),
        Cell: ListboxCell,
      },
      {
        Header: 'From',
        accessor: 'from',
      },
      {
        Header: 'To',
        accessor: 'to',
      },
    ],
    [],
  );

  const [data, setData] = React.useState([]);
  const [selectedRows, setSelectedRows] = React.useState([]);

  React.useEffect(() => {
    setData(items);
  }, [items]);

  const updateMyData = (rowIndex, columnId, value) => {
    setData((old) =>
      old.map((row, index) => {
        if (index === rowIndex) {
          return {
            ...old[rowIndex],
            [columnId]: value,
          };
        }
        return row;
      }),
    );
  };

  const handleChangeType = (rowIndex, value) => {
    setData((old) =>
      old.map((row, index) => {
        if (index === rowIndex) {
          return {
            ...row,
            type: value,
          };
        }
        return row;
      }),
    );
  };

  const addNewRow = () => {
    setData(
      data.concat({ metric: '', type: 'number', from: '', to: '' }),
    );
  };

  const deleteRows = () => {
    const selectedIds = selectedRows.map((i) => i.id);
    const newData = data.filter((value, index) => {
      return !selectedIds.includes(index);
    });
    setData(newData);
  };

  const handleSave = () => {
    const metricRange = data.filter((i) => i.metric.trim() !== '');
    onSave(metricRange);
  };

  return (
    <>
      <Dialog
        open={isOpen}
        onClose={() => {
          onSetOpenModal(false);
        }}
        aria-labelledby="form-dialog-title"
        maxWidth="lg"
      >
        <DialogTitle id="form-dialog-title">Metric range</DialogTitle>

        <DialogContent>
          <Styles>
            <Box pb={2}>
              <Button
                variant="outlined"
                size="small"
                className="action-btn"
                startIcon={<AddIcon />}
                onClick={addNewRow}
              >
                Add metric
              </Button>
              <Button
                variant="outlined"
                size="small"
                className="action-btn"
                startIcon={<DeleteIcon />}
                onClick={deleteRows}
              >
                Delete metrics
              </Button>
            </Box>
            <Table
              columns={columns}
              data={data}
              updateMyData={updateMyData}
              onSelectItem={(rows) => setSelectedRows(rows)}
            />
          </Styles>
        </DialogContent>
        <DialogActions>
          <button
            className="btn btn-outline-secondary"
            onClick={() => onSetOpenModal(false)}
          >
            Cancel
          </button>
          <button
            className="btn btn-outline-secondary"
            onClick={handleSave}
          >
            Save
          </button>
        </DialogActions>
      </Dialog>
    </>
  );
};
