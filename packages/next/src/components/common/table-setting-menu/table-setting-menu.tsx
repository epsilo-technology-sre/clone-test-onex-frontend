import { Box, IconButton } from '@material-ui/core';
import Popover from '@material-ui/core/Popover';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import {
  bindMenu,
  bindTrigger,
  usePopupState,
} from 'material-ui-popup-state/hooks';
import React from 'react';
import { ColumnSelection } from '../column-selection';
import { TableMainMenu } from '../table-main-menu';

export const TableSettingMenu = (props) => {
  const [menuType, setMenuType] = React.useState('main');
  const [dimension, setDimension] = React.useState();

  const popupState = usePopupState({
    variant: 'popover',
    popupId: 'table-setting-menu',
  });

  React.useEffect(() => {
    if (popupState.isOpen) {
      setMenuType('main');
    }
  }, [popupState]);

  const handleSelectMenu = (item) => {
    setMenuType(item);
  };

  let menuContent = null;
  switch (menuType) {
    case 'main':
      menuContent = (
        <TableMainMenu
          onSelectItem={handleSelectMenu}
        ></TableMainMenu>
      );
      break;
    case 'Properties':
      menuContent = (
        <ColumnSelection
          table={props.table}
          dimension={dimension}
          onChangeDimension={(value) => setDimension(value)}
        ></ColumnSelection>
      );
      break;
    default:
      menuContent = <Box p={2}>Menu is not available</Box>;
  }

  return (
    <div>
      <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center p-1 pt-2 title-container">
        <h1 className="h2" data-dbf-text-editable="title">
          Breakdown
        </h1>
        <Box ml={1}>
          <IconButton {...bindTrigger(popupState)}>
            <MoreVertIcon></MoreVertIcon>
          </IconButton>
          <Popover
            {...bindMenu(popupState)}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'left',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'left',
            }}
          >
            {menuContent}
          </Popover>
        </Box>
      </div>
    </div>
  );
};
