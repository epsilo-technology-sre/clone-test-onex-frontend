import React from 'react';
import { TableFilterUI } from '../table-filter';
import { PureTableStickyUI } from './table-ui-sticky';

export const TableUISticky = (props: any) => {
  const {
    columns,
    rows,
    resultTotal,
    page,
    pageSize,
    onChangePage,
    onChangePageSize,
    onSort,
    noData,
    onSelectItem,
    onSelectAll,
    selectedIds,
    getRowId,
    className,
  } = props;

  const tableInstance = React.useMemo(() => {
    return (
      <PureTableStickyUI
        columns={columns}
        className={className}
        rows={rows}
        noData={noData}
        onSort={onSort}
        onSelectItem={onSelectItem}
        onSelectAll={onSelectAll}
        selectedIds={selectedIds}
        getRowId={getRowId}
      />
    );
  }, [columns, className, rows, noData, selectedIds]);

  return (
    <div>
      {tableInstance}
      <TableFilterUI
        resultTotal={resultTotal}
        page={page}
        pageSize={pageSize}
        onChangePage={onChangePage}
        onChangePageSize={onChangePageSize}
      />
    </div>
  );
};
