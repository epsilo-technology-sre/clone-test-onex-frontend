import styled from 'styled-components';

export const Styles = styled.div`
  display: block;
  overflow: auto;
  .table {
    &.table-permission {
      .th:nth-child(n + 2),
      .td:nth-child(n + 2) {
        justify-content: center;
        text-overflow: ellipsis;
        overflow: hidden;
        display: block !important;
        white-space: nowrap;
      }
      .tr {
        border-bottom: 0 !important;
      }
      &.sticky [data-sticky-last-left-td] {
        box-shadow: 1px 0px 1px 0px rgb(204 204 204 / 0.3) !important;
      }
      &.sticky .td {
        font-size: 12px;
        line-height: 16px;
        color: #596772;
        padding-left: 34px;
      }
      .headerPermission .td {
        background: rgba(251, 251, 251, 1);
        font-weight: 600;
        font-size: 16px;
        line-height: 20px;
        color: #253746;
        padding-left: 10px;
        &:nth-child(n + 2) {
          padding-left: 34px;
        }
      }
    }
    &:not(.user-table) {
      height: 500px;
      .tr {
        border-bottom: 1px solid #e4e7e9;
        :last-child {
          .td {
            border-bottom: 0;
          }
        }
        :hover {
          .td {
            background: #e4e7e9;
          }
          .edit-btn {
            display: block;
          }
        }
        .edit-btn {
          display: none;
        }
      }
    }

    &.user-table,
    &.table-permission {
      .th,
      .td {
        &:first-child {
          justify-content: flex-start;
        }
      }
    }
    .th,
    .td {
      padding: 5px 10px;
      background-color: #fff;
      overflow: hidden;
      position: relative;
      display: inline-flex !important;
      align-items: center;

      :last-child {
        border-right: 0;
      }

      &.disabled {
        background: #e4e7e9;
        &:not(.status) {
          pointer-events: none;
        }
      }

      .resizer {
        display: none;
        background: gray;
        width: 8px;
        height: 100%;
        position: absolute;
        right: 0;
        top: 0;
        transform: translateX(50%);
        z-index: 1;
        touch-action: none;
      }

      &:first-child {
        justify-content: center;
      }

      &:hover {
        .resizer {
          display: inline-block;
        }
      }
    }

    .th {
      font-weight: 600;
    }

    &.sticky {
      overflow: auto;
      .header,
      .footer {
        position: sticky;
        z-index: 1;
        width: fit-content;
      }

      .header {
        top: 0;
      }

      .footer {
        bottom: 0;
        box-shadow: 0px -3px 3px #ccc;
      }

      .body {
        position: relative;
        z-index: 0;
      }

      [data-sticky-td] {
        position: sticky;
      }

      &:not(.user-table) {
        [data-sticky-last-left-td] {
          box-shadow: 2px 0px 3px #ccc;
        }
      }

      [data-sticky-first-right-td] {
        box-shadow: -2px 0px 3px #ccc;
      }
    }
    &.tableConditionRule {
      height: auto;
      .th,
      .td {
        justify-content: left;
      }
    }
    &.log-rule-filter {
      .th,
      .td {
        justify-content: left;
      }
    }
    &.setHeightModal {
      height: 335px;
    }
    &.setHeightModalAddkeyword {
      height: 317px;
    }
    &.add-rule {
      height: auto;
      margin-top: 8px;
      .th {
        font-weight: 600;
        font-size: 12px;
        line-height: 16px;
        color: #596772;
        justify-content: start;
      }
      .td {
        font-size: 14px;
        color: #253746;
        line-height: 20px;
        justify-content: start;
      }
    }
  }

  .no-data {
    width: 100%;
    text-align: center;
  }

  .table-actions {
    padding: 10px 0;
    .custom-page {
      width: 50px;
    }
  }
`;
