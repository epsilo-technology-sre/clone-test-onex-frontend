import ArrowDownIcon from '@ep/shopee/src/images/arrow-down.svg';
import ArrowUpIcon from '@ep/shopee/src/images/arrow-up.svg';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import React, { useEffect } from 'react';
import {
  useBlockLayout,
  useColumnOrder,
  useResizeColumns,
  useRowSelect,
  useSortBy,
  useTable,
} from 'react-table';
import { useSticky } from 'react-table-sticky';
import { ColumnSelection } from '../column-selection';
import { TableFilterUI } from '../table-filter';
import { Styles } from './style';

const useStyle = makeStyles({
  arrowUp: {
    color: '#3F4F5C',
    position: 'absolute',
    bottom: 10,
    right: 10,
  },
  arrowDown: {
    color: '#3F4F5C',
    position: 'absolute',
    bottom: 10,
    right: 10,
  },
});

type TableProps = {
  columns: any[];
  getRowId: (
    row: any,
    relativeIndex: string,
    parentIndex?: string,
  ) => string;
  onSort: (a: any) => void;
  rows: any[];
  noData?: any;
  className?: string;
};

export const PureTableUI = (props: TableProps) => {
  const classes = useStyle();
  const columns = React.useMemo(() => props.columns, []);
  const { noData, onSort, getRowId, className } = props;

  const tableInstance = useTable(
    {
      columns,
      data: props.rows,
      manualSortBy: true,
      initialState: {},
      useControlledState: (state: any) => {
        return React.useMemo(() => {
          return {
            ...state,
          };
        }, [state]);
      },
      getRowId: getRowId
        ? getRowId
        : React.useCallback((row, index) => index, []),
    },
    useSortBy,
    useBlockLayout,
    useSticky,
    useRowSelect,
    useColumnOrder,
    useResizeColumns,
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    state: { sortBy },
  } = tableInstance;

  useEffect(() => {
    onSort(sortBy);
  }, [sortBy, onSort]);

  useEffect(() => {
    window.setTimeout(() => {}, 1000);
  }, []);

  const getDirectionIcon = (column: any) => {
    if (column.isSorted) {
      return column.isSortedDesc ? (
        <img
          src={ArrowDownIcon}
          className={classes.arrowDown}
          width={12}
          height={12}
        />
      ) : (
        <img
          src={ArrowUpIcon}
          className={classes.arrowUp}
          width={12}
          height={12}
        />
      );
    }
    return '';
  };

  return (
    <Styles>
      <ColumnSelection table={tableInstance}></ColumnSelection>
      <div
        {...getTableProps()}
        className={clsx('table sticky', { [className]: className })}
      >
        <div className="header">
          {headerGroups.map((headerGroup: any) => (
            <div
              {...headerGroup.getHeaderGroupProps()}
              className="tr"
            >
              {headerGroup.headers.map((column: any) => (
                <div
                  {...column.getHeaderProps(
                    column.getSortByToggleProps(),
                  )}
                  className="th"
                >
                  {column.render('Header')}
                  {getDirectionIcon(column)}
                  <div
                    {...column.getResizerProps()}
                    className={`resizer ${
                      column.isResizing ? 'isResizing' : ''
                    }`}
                  />
                </div>
              ))}
            </div>
          ))}
        </div>
        <div {...getTableBodyProps()} className="body">
          {rows.length > 0 &&
            rows.map((row: any) => {
              prepareRow(row);
              return (
                <div {...row.getRowProps()} className="tr">
                  {row.cells.map((cell: any) => (
                    <div {...cell.getCellProps()} className="td">
                      {cell.render('Cell')}
                    </div>
                  ))}
                </div>
              );
            })}
          {rows.length === 0 && (
            <div className="no-data">{noData}</div>
          )}
        </div>
      </div>
    </Styles>
  );
};

type Pagination = {
  resultTotal: number;
  page: number;
  pageSize: number;
  onChangePage: (a: any) => void;
  onChangePageSize: (a: any) => void;
};

export const TableUINoCheckbox = (props: TableProps & Pagination) => {
  const {
    columns,
    rows,
    resultTotal,
    page,
    pageSize,
    onChangePage,
    onChangePageSize,
    onSort,
    noData,
    getRowId,
    className,
  } = props;
  return (
    <div>
      <PureTableUI
        columns={columns}
        className={className}
        rows={rows}
        noData={noData}
        onSort={onSort}
        getRowId={getRowId}
      />
      {page && page >= 1 && (
        <TableFilterUI
          resultTotal={resultTotal}
          page={page}
          pageSize={pageSize}
          onChangePage={onChangePage}
          onChangePageSize={onChangePageSize}
        />
      )}
    </div>
  );
};
