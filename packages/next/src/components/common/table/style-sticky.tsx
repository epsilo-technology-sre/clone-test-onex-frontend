import styled from 'styled-components';

export const Styles = styled.div`
  .table {
    .tr {
      border-bottom: 1px solid #e4e7e9;
      :last-child {
        .td {
          border-bottom: 0;
        }
      }
      :hover {
        .td {
          background: #e4e7e9;
        }
        .edit-btn {
          display: block;
        }
      }
      .edit-btn {
        display: none;
      }
    }

    .th,
    .td {
      padding: 5px 10px;
      background-color: #fff;
      overflow: hidden;
      position: relative;
      display: inline-flex !important;
      align-items: center;

      :last-child {
        border-right: 0;
      }

      &.disabled {
        background: #e4e7e9;
        &:not(.status) {
          pointer-events: none;
        }
      }

      .resizer {
        display: none;
        background: gray;
        width: 10px;
        height: 100%;
        position: absolute;
        right: 0;
        top: 0;
        transform: translateX(50%);
        z-index: 1;
        touch-action: none;
      }

      &:first-child {
        justify-content: center;
      }

      &:hover {
        .resizer {
          display: inline-block;
        }
      }
    }

    .th {
      font-weight: 600;
    }

    &.ngsticky.sticky {
      position: relative;
      overscroll-behavior: contain;
      .header {
        position: sticky;
        top: 56px;
        z-index: 5;
        .scrollable {
          overflow-y: hidden;
          overflow-x: hidden;
        }
      }
    }

    &.sticky {
      .footer {
        position: sticky;
        z-index: 5;
        width: fit-content;
      }

      .footer {
        bottom: 0;
        box-shadow: 0px -3px 3px #ccc;
      }

      .body {
        overflow: auto;
        overflow-y: hidden;
        z-index: 0;
      }

      [data-sticky-td] {
        position: sticky;
      }

      [data-sticky-last-left-td] {
        box-shadow: 2px 0px 3px #ccc;
      }

      [data-sticky-first-right-td] {
        box-shadow: -2px 0px 3px #ccc;
      }
    }
    &.setHeightModal {
      height: 335px;
    }
    &.setHeightModalAddkeyword {
      height: 317px;
    }
    &.add-rule {
      height: auto;
      margin-top: 8px;
      .th {
        font-weight: 600;
        font-size: 12px;
        line-height: 16px;
        color: #596772;
        justify-content: start;
      }
      .td {
        font-size: 14px;
        color: #253746;
        line-height: 20px;
        justify-content: start;
      }
    }
  }

  .no-data {
    width: 100%;
    text-align: center;
  }

  .table-actions {
    padding: 10px 0;
    .custom-page {
      width: 50px;
    }
  }
`;
