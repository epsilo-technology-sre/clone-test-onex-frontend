import { List, ListItem } from '@material-ui/core';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { makeStyles } from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import FilterListIcon from '@material-ui/icons/FilterList';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import GetAppIcon from '@material-ui/icons/GetApp';
import ImportExportIcon from '@material-ui/icons/ImportExport';
import SettingsIcon from '@material-ui/icons/Settings';
import React from 'react';

const useStyle = makeStyles((theme) => ({
  itemIcon: {
    minWidth: 30,
  },
  itemText: {
    '& .MuiListItemText-primary': {
      fontSize: 14,
    },
  },
}));

const MENU_ITEMS = [
  {
    name: 'Edit',
    icon: <EditIcon fontSize="small" />,
  },
  {
    name: 'Duplicate',
    icon: <FileCopyIcon fontSize="small" />,
  },
  {
    name: 'Remove',
    icon: <DeleteIcon fontSize="small" />,
  },
  {
    name: 'Properties',
    icon: <FormatListBulletedIcon fontSize="small" />,
  },
  {
    name: 'Customize',
    icon: <SettingsIcon fontSize="small" />,
  },
  {
    name: 'Sort',
    icon: <ImportExportIcon fontSize="small" />,
  },
  {
    name: 'Filter',
    icon: <FilterListIcon fontSize="small" />,
  },
  {
    name: 'Export',
    icon: <GetAppIcon fontSize="small" />,
  },
];

export const TableMainMenu = (props: { onSelectItem: Function }) => {
  const classes = useStyle();
  const [items, setItems] = React.useState(MENU_ITEMS);

  const handleSelectItem = (item) => {
    props.onSelectItem(item.name);
  };

  return (
    <div>
      <List component="nav" aria-label="main">
        {items.map((i, index) => (
          <>
            <ListItem
              key={index}
              button
              onClick={() => handleSelectItem(i)}
            >
              <ListItemIcon className={classes.itemIcon}>
                {i.icon}
              </ListItemIcon>
              <ListItemText
                className={classes.itemText}
                primary={i.name}
              />
            </ListItem>
          </>
        ))}
      </List>
    </div>
  );
};
