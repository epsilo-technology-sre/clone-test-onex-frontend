import {
  Box,
  Button,
  createStyles,
  Divider,
  FormControlLabel,
  InputAdornment,
  makeStyles,
  Radio,
  RadioGroup,
  Theme,
  withStyles,
} from '@material-ui/core';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import AddIcon from '@material-ui/icons/Add';
import SearchIcon from '@material-ui/icons/Search';
import clsx from 'clsx';
import React from 'react';
import { ReactSortable } from 'react-sortablejs';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    body: {
      minWidth: 200,
      padding: '10px 14px 4px 14px',
      '& .MuiFormControlLabel-label': {
        fontSize: 14,
      },
    },
    checkboxWrapper: {
      maxHeight: 300,
      overflow: 'auto',
    },
    checkItem: {
      padding: '5px 0',
      '& input': {
        verticalAlign: 'middle',
      },
    },
    itemContainer: {
      maxHeight: 250,
      overflow: 'auto',
    },
    itemName: {
      paddingLeft: 5,
    },
    footer: {
      display: 'flex',
      justifyContent: 'flex-end',
      padding: theme.spacing(1),
      borderTop: '2px solid #E4E7E9',
    },
    button: {
      border: '1px dashed #C2C7CB',
      textTransform: 'none',
      color: '#8C98A4',
      fontSize: 12,
    },
  }),
);

const SearchBox = withStyles({
  root: {
    marginBottom: 10,
    '&:hover .MuiOutlinedInput-notchedOutline': {
      borderColor: '#C2C7CB',
    },
  },
  notchedOutline: {
    border: '2px solid #C2C7CB',
  },
  input: {
    padding: 8,
    fontSize: 14,
  },
})(OutlinedInput);

const IndeterminateCheckbox = React.forwardRef(
  ({ indeterminate, ...rest }, ref) => {
    const defaultRef = React.useRef();
    const resolvedRef = ref || defaultRef;

    React.useEffect(() => {
      resolvedRef.current.indeterminate = indeterminate;
    }, [resolvedRef, indeterminate]);

    return <input type="checkbox" ref={resolvedRef} {...rest} />;
  },
);

const useRadioStyles = makeStyles({
  root: {
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  icon: {
    borderRadius: '50%',
    width: 16,
    height: 16,
    boxShadow:
      'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
    backgroundColor: '#f5f8fa',
    backgroundImage:
      'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
    '$root.Mui-focusVisible &': {
      outline: '2px auto rgba(19,124,189,.6)',
      outlineOffset: 2,
    },
    'input:hover ~ &': {
      backgroundColor: '#ebf1f5',
    },
    'input:disabled ~ &': {
      boxShadow: 'none',
      background: 'rgba(206,217,224,.5)',
    },
  },
  checkedIcon: {
    backgroundColor: '#137cbd',
    backgroundImage:
      'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
    '&:before': {
      display: 'block',
      width: 16,
      height: 16,
      backgroundImage:
        'radial-gradient(#fff,#fff 28%,transparent 32%)',
      content: '""',
    },
    'input:hover ~ &': {
      backgroundColor: '#106ba3',
    },
  },
});

const StyledRadio = (props) => {
  const classes = useRadioStyles();

  return (
    <Radio
      className={classes.root}
      disableRipple
      color="default"
      checkedIcon={
        <span className={clsx(classes.icon, classes.checkedIcon)} />
      }
      icon={<span className={classes.icon} />}
      {...props}
    />
  );
};

const DIMENSIONS = [
  {
    name: 'Shop',
    columns: [],
  },
  {
    name: 'Campaign',
    columns: [
      'shop',
      'impression',
      'click',
      'item_sold',
      'gmv',
      'roas',
      'direct_cr',
      'direct_cpi',
      'direct_cir',
      'direct_percent_gmv_over_total',
      'direct_percent_item_sold_over_total',
      'direct_roas',
    ],
  },
  {
    name: 'SKU',
    columns: [
      'shop',
      'direct_gmv',
      'direct_item_sold',
      'ads_order',
      'direct_ads_order',
      'total_gmv',
      'total_item_sold',
      'total_order',
      'ctr',
      'cr',
      'cpc',
      'cpi',
      'cir',
      'percent_gmv_over_total',
      'percent_item_sold_over_total',
      'roas',
    ],
  },
  {
    name: 'RULE',
    columns: [
      'shop',
      'impression',
      'click',
      'item_sold',
      'direct_cr',
      'direct_cpi',
      'direct_cir',
      'direct_percent_gmv_over_total',
      'direct_percent_item_sold_over_total',
      'direct_roas',
    ],
  },
];

export const ColumnSelection = (props: {
  table: any;
  dimension: String;
  onChangeDimension: Function;
}) => {
  const classes = useStyles();
  const {
    getToggleHideAllColumnsProps,
    allColumns,
    setColumnOrder,
  } = props.table;

  const { dimension = 'Shop' } = props;

  const [searchText, setSearchText] = React.useState('');
  const [dimensions, setDimensions] = React.useState(DIMENSIONS);
  const [displayColumns, setDisplayColumns] = React.useState([]);

  React.useEffect(() => {
    let columns = [];
    if (dimension !== 'Shop') {
      columns = dimensions.find((i) => i.name === dimension).columns;
    } else {
      columns = allColumns.map((i) => i.id);
    }

    // const columns = allColumns.filter((i) =>
    //   columnCodes.includes(i.id),
    // );
    setDisplayColumns(columns);

    // Hide columns are not belong to display columns
  }, [dimension]);

  const handleOrderColumns = (newColumns) => {
    const newOrders = newColumns.map((c) => c.id);
    setColumnOrder(newOrders);
  };

  const handleChangeSearchText = (e: any) => {
    e.stopPropagation();
    setSearchText(e.target.value);
  };

  return (
    <Box className={classes.body}>
      <div>
        <SearchBox
          id="multi-select-searchbox"
          placeholder="Search"
          value={searchText}
          onChange={handleChangeSearchText}
          startAdornment={
            <InputAdornment position="start">
              <SearchIcon fontSize="small" />
            </InputAdornment>
          }
          labelWidth={0}
          autoComplete="off"
        ></SearchBox>
      </div>
      <div>
        <RadioGroup
          value={dimension}
          aria-label="dimension"
          name="customized-radios"
          onChange={(event) =>
            props.onChangeDimension(event.target.value)
          }
        >
          {dimensions.map((i) => (
            <FormControlLabel
              value={i.name}
              control={<StyledRadio />}
              label={i.name}
            />
          ))}
        </RadioGroup>
      </div>
      <Divider></Divider>
      <div className={classes.checkItem}>
        <label>
          <IndeterminateCheckbox
            {...getToggleHideAllColumnsProps()}
          />{' '}
          <span className={classes.itemName}>All</span>
        </label>
      </div>
      <div className={classes.itemContainer}>
        <ReactSortable list={allColumns} setList={handleOrderColumns}>
          {allColumns
            .filter(
              (column) =>
                column.id !== 'selection' &&
                displayColumns.includes(column.id) &&
                column.Header.toLowerCase().includes(
                  searchText.toLowerCase(),
                ),
            )
            .map((column) => {
              return (
                <div key={column.id} className={classes.checkItem}>
                  <label>
                    <input
                      type="checkbox"
                      {...column.getToggleHiddenProps()}
                    />{' '}
                    <span className={classes.itemName}>
                      {column.Header}
                    </span>
                  </label>
                </div>
              );
            })}
        </ReactSortable>
      </div>

      <Box pb={1}>
        <Button
          variant="outlined"
          fullWidth
          className={classes.button}
          startIcon={<AddIcon />}
        >
          Create new metric
        </Button>
      </Box>
    </Box>
  );
};
