import {
  Box,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  makeStyles,
} from '@material-ui/core';
import Popover from '@material-ui/core/Popover';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import FilterListIcon from '@material-ui/icons/FilterList';
import PopupState, {
  bindPopover,
  bindTrigger,
} from 'material-ui-popup-state';
import React from 'react';

const useStyle = makeStyles((theme) => ({
  itemIcon: {
    minWidth: 30,
  },
  itemText: {
    '& .MuiListItemText-primary': {
      fontSize: 14,
    },
  },
}));

const MENU_ITEMS = [
  {
    name: 'asc',
    text: 'Sort Ascending',
    icon: <ArrowUpwardIcon fontSize="small" />,
  },
  {
    name: 'desc',
    text: 'Sort Descending',
    icon: <ArrowDownwardIcon fontSize="small" />,
  },
  {
    name: 'filter',
    text: 'Add Filter',
    icon: <FilterListIcon fontSize="small" />,
  },
];

interface InlineHeaderMenuProps {
  onSelectMenu: Function;
}

export const InlineHeaderMenu = (
  props: React.PropsWithChildren<InlineHeaderMenuProps>,
) => {
  const classes = useStyle();
  const [items, setItems] = React.useState(MENU_ITEMS);
  return (
    <PopupState variant="popover" popupId="popup-header">
      {(popupState) => {
        return (
          <Box>
            <Box {...bindTrigger(popupState)}>{props.children}</Box>
            <Popover
              {...bindPopover(popupState)}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
              }}
            >
              <List component="nav" aria-label="main">
                {items.map((item, index) => (
                  <>
                    <ListItem
                      key={index}
                      button
                      onClick={() => {
                        popupState.close();
                        props.onSelectMenu(item.name);
                      }}
                    >
                      <ListItemIcon className={classes.itemIcon}>
                        {item.icon}
                      </ListItemIcon>
                      <ListItemText
                        className={classes.itemText}
                        primary={item.text}
                      />
                    </ListItem>
                  </>
                ))}
              </List>
            </Popover>
          </Box>
        );
      }}
    </PopupState>
  );
};
