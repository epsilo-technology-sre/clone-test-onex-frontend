import { action } from '@storybook/addon-actions';
import * as React from 'react';
import { CustomMetricFormEdit } from './index';
import { CustomMetricList } from './list';

export default {
  title: 'DBF/Custom Metric',
};

export function Primary() {
  return (
    <CustomMetricFormEdit
      handleUpdate={action('updateCustomMetric')}
      metric={null}
    />
  );
}

export function MetricList() {
  return <CustomMetricList open={true} />;
}
