import { API_INSIGHT_URL } from '@ep/one/global';
import { useMetricServer } from '@epi/next/src/hooks';
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  ListItem,
  ListItemText,
  Typography,
} from '@material-ui/core';
import List from '@material-ui/core/List';
import * as React from 'react';
import { CustomMetricFormEdit } from './form';

type CustomMetricListPropsType = {
  open: boolean;
  onClose: () => void;
};

export function CustomMetricList({
  open = true,
  onClose = () => {},
}: CustomMetricListPropsType) {
  const [
    selectedMetric,
    setSelectedMetric,
  ] = React.useState<CustomMetric>(null);
  const [metricList, setMetricList] = React.useState([]);

  const { getList, update, getSingle } = useMetricServer(
    API_INSIGHT_URL,
  );

  const reloadMetricList = React.useCallback(() => {
    getList().then((metricList) => {
      setMetricList(metricList);
    });
  }, []);

  const handleUpdate = React.useCallback((metric) => {
    return update({
      name: metric.name,
      formula: metric.formula,
      eid: metric.eid,
    }).then(() => {
      reloadMetricList();
      return true;
    });
  }, []);

  React.useEffect(() => {
    reloadMetricList();
  }, []);

  const handleAddNew = React.useCallback(() => {
    setSelectedMetric({
      eid: null,
      name: '',
      formula: '',
    });
  }, []);

  return (
    <Dialog
      open={open}
      fullWidth={true}
      onClose={onClose}
      maxWidth="lg"
    >
      <DialogTitle>
        <Typography variant="h2">Custom Metrics</Typography>
      </DialogTitle>
      <DialogContent>
        <Grid container>
          <Grid item md={3} style={{ display: 'flex' }}>
            <Box height={'100%'} overflow={'hidden'}>
              <Box minHeight={'70vh'}>
                <List>
                  {metricList.map((m) => (
                    <ListItem
                      key={m.eid}
                      button
                      onClick={() => {
                        setSelectedMetric(m);
                      }}
                      selected={
                        selectedMetric && m.eid === selectedMetric.eid
                      }
                    >
                      <ListItemText
                        primary={m.name}
                        secondary={`@${m.eid}`}
                      />
                    </ListItem>
                  ))}
                </List>
              </Box>
            </Box>
          </Grid>
          <Divider orientation="vertical" flexItem />
          <Grid item md={8} style={{ paddingLeft: '2em' }}>
            {selectedMetric && (
              <CustomMetricFormEdit
                handleUpdate={handleUpdate}
                metric={selectedMetric}
              />
            )}
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button color="primary" onClick={handleAddNew}>
          Create Metric
        </Button>
        <Box flexGrow={1}></Box>
        <Button color="secondary" onClick={onClose}>
          Close
        </Button>
      </DialogActions>
    </Dialog>
  );
}
