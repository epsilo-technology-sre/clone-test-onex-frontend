import * as React from 'react';
import { Formik } from 'formik';
import {
  Box,
  Button,
  Divider,
  FormControl,
  Input,
  InputAdornment,
  InputLabel,
  Typography,
} from '@material-ui/core';
import { genSimpleId } from '../../hooks';

type CustomMetricFormEditProps = {
  handleUpdate: (customMetric: Partial<CustomMetric>) => Promise<any>;
  metric?: CustomMetric;
};

function generateCodeFromName(name, length = 5) {
  let code = name
    .split(' ')
    .map((i) => i[0])
    .join('')
    .toUpperCase();
  const randStr = genSimpleId(length - code.length);
  return 'EM_' + code + randStr;
}

export function CustomMetricFormEdit({
  handleUpdate,
  metric,
}: CustomMetricFormEditProps) {
  const [submitting, setSubmitting] = React.useState(false);
  return (
    <Formik
      initialValues={{
        name: metric ? metric.name : '',
        formula: metric ? metric.formula : '',
        id: metric ? metric.id : null,
        eid:
          metric && metric.eid ? metric.eid.replace(/^EM_/, '') : '',
      }}
      enableReinitialize={true}
      onSubmit={(values) => {
        setSubmitting(true);
        handleUpdate({
          name: values.name,
          formula: values.formula,
          eid: values.eid,
        }).then(() => {
          setSubmitting(false);
        });
      }}
    >
      {({ values, setFieldValue, submitForm }) => (
        <React.Fragment>
          <Box mb={1}>
            <Typography variant="h3">
              {values.id ? 'Edit Metric' : 'Create Metric'}
            </Typography>
          </Box>
          <FormControl fullWidth>
            <InputLabel shrink>Metric name</InputLabel>
            <Input
              value={values.name}
              onChange={(evt) => {
                let name = evt.target.value;
                setFieldValue('name', evt.target.value);
                if (!values.id) {
                  setFieldValue('eid', generateCodeFromName(name));
                }
              }}
            />
          </FormControl>
          <Box height={'2em'} />
          <FormControl fullWidth>
            <InputLabel shrink>Metric code</InputLabel>
            <Input
              value={values.eid}
              onChange={(evt) =>
                setFieldValue('eid', evt.target.value)
              }
              startAdornment={
                <InputAdornment position="start">@EM_</InputAdornment>
              }
            />
          </FormControl>
          <Box height={'2em'} />
          <FormControl fullWidth>
            <InputLabel shrink>Metric formula</InputLabel>
            <Input
              multiline
              value={values.formula}
              onChange={(evt) =>
                setFieldValue('formula', evt.target.value)
              }
            />
          </FormControl>
          <Box height={'2em'} />
          <Button
            color="primary"
            variant="contained"
            onClick={() => submitForm()}
            disabled={submitting}
          >
            {!submitting ? 'Submit' : 'Loading...'}
          </Button>
          <Button variant="text" onClick={() => {}}>
            Cancel
          </Button>
        </React.Fragment>
      )}
    </Formik>
  );
}
