import { produce } from 'immer';
import { debounce, mapValues } from 'lodash';
import { nanoid } from 'nanoid';
import React from 'react';
import { useHotkeys } from 'react-hotkeys-hook';
import { DashboardFrame } from './components/dashboard';
import { DashboardContext as DashboardFrameContext } from './components/dashboard/context';
import { getDashboardRepo } from './components/dashboard/repo';
import { Dashboard, NodeData } from './components/dashboard/type';
import {
  usePresetServer,
  useDashboardHistory,
  DataQueryContext,
} from './components/dashboard/hooks';
import { API_INSIGHT_URL } from '@ep/one/global';
import styled from 'styled-components';
import { PresetControl } from './components/preset-control';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { ReactComponent as PrintLogo } from './images/epsilo-logo-light.svg';
import {
  generatePath,
  Route,
  useHistory,
  useLocation,
  useParams,
  useRouteMatch,
} from 'react-router';
import { Box } from '@material-ui/core';
import { PrintControl } from './print-control';

const DEFAULT_LAYOUT = 'lg';

type actionType =
  | 'SET'
  | 'SET_LAYOUT'
  | 'ADD_NODE'
  | 'REMOVE_NODE'
  | 'UPDATE_NODE'
  | 'CHANGE_BREAKPOINT';
function dashboardReducer(
  state: Dashboard & { breakpoint: string } = {
    layouts: null,
    nodes: null,
    breakpoint: null,
  },
  action: { type: actionType; payload: any },
) {
  console.info('state update ===> ', { state, action });
  let { payload } = action;
  switch (action.type) {
    case 'SET': {
      state = produce(state, (draft) => {
        if (payload.layouts !== undefined) {
          draft.layouts = payload.layouts;
        }
        if (payload.nodes !== undefined) {
          draft.nodes = payload.nodes;
        }
      });
      break;
    }
    case 'SET_LAYOUT': {
      state = produce(state, (draft) => {
        draft.layouts[state.breakpoint] = payload.layouts;
      });
      break;
    }
    case 'CHANGE_BREAKPOINT': {
      state = produce(state, (draft) => {
        draft.breakpoint = payload.breakpoint;
      });
      break;
    }
    case 'ADD_NODE': {
      state = produce(state, (draft) => {
        draft.nodes = state.nodes.concat(payload.node);
      });
      break;
    }
    case 'REMOVE_NODE': {
      state = produce(state, (draft) => {
        let id = payload.nodeId;
        let nuLayouts = mapValues(state.layouts, (layout) => {
          return (layout || []).filter((i) => i.id !== id);
        });

        let nuNodes = state.nodes.filter((n) => {
          return n.id !== id;
        });

        draft.layouts = nuLayouts;
        draft.nodes = nuNodes;
      });
      break;
    }
    case 'UPDATE_NODE': {
      state = produce(state, (draft) => {
        let id = payload.nodeId;
        let index = state.nodes.findIndex((i) => i.id === id);
        draft.nodes[index] = payload.data;
      });
      break;
    }
  }
  return state;
}

export default function DashboardPageRouted() {
  const match = useRouteMatch();
  console.info({ match });

  return (
    <Route path={[match.path + '/:presetId', match.path]}>
      <DashboardSinglePreset />
    </Route>
  );
}

function DashboardSinglePreset() {
  const pageHistory = useHistory();
  const match = useRouteMatch();
  const { presetId: pagePresetId } = useParams();
  const preset = usePresetServer(API_INSIGHT_URL);

  const [presetList, setPresetList] = React.useState([]);
  const [isLoading, setLoading] = React.useState(true);
  const [dashboardRepo, setDashboardRepo] = React.useState(
    getDashboardRepo(),
  );

  const [demoMetricRange, setDemoMetricRange] = React.useState([]);

  function handleRefreshList() {
    preset.getList().then((list) => {
      setPresetList(list);
    });
  }

  React.useEffect(() => {
    handleRefreshList();
  }, []);

  React.useEffect(() => {
    dashboardRepo.init(pagePresetId);

    console.info({ pagePresetId });
    if (pagePresetId) {
      setLoading(true);
      preset.getSingle(pagePresetId).then((ps) => {
        console.info({ ps });
        dashboardRepo.setId(ps.presetEid);
        dashboardRepo.setNodes(ps.nodes);
        dashboardRepo.updateLayouts(ps.layouts);
        dashboardRepo.setDemoMetricRange(ps.demoMetricRange);

        setDemoMetricRange(dashboardRepo.getDemoMetricRange());
        setLoading(false);
      });
    } else {
      setLoading(true);
      preset.get().then((ps: Dashboard) => {
        if (ps) {
          dashboardRepo.setId(ps.presetEid);
          dashboardRepo.setNodes(ps.nodes);
          dashboardRepo.updateLayouts(ps.layouts);
          dashboardRepo.setDemoMetricRange(ps.demoMetricRange);
        }

        if (pagePresetId !== ps.presetEid) {
          pageHistory.push(`/next/dashboard/${ps.presetEid}`);
        }

        setDemoMetricRange(dashboardRepo.getDemoMetricRange());
        setLoading(false);
      });
    }
  }, [pagePresetId]);

  const handleAddNewPreset = (presetName) => {
    setLoading(true);
    preset
      .update(undefined, {
        title: presetName,
        layouts: {
          lg: [],
          md: [],
        },
        nodes: [],
      })
      .then((ps: Dashboard) => {
        if (ps) {
          dashboardRepo.setId(ps.presetEid);
          dashboardRepo.setNodes(ps.nodes);
          dashboardRepo.updateLayouts(ps.layouts);
          dashboardRepo.setDemoMetricRange(ps.demoMetricRange);
        }

        if (pagePresetId !== ps.presetEid) {
          pageHistory.push(`/next/dashboard/${ps.presetEid}`);
        }

        setDemoMetricRange(dashboardRepo.getDemoMetricRange());
        setLoading(false);
      });
  };

  const handleUpdatePreset = ({ demoMetricRange }) => {
    dashboardRepo.setDemoMetricRange(demoMetricRange);
    preset.update(pagePresetId, {
      layouts: dashboardRepo.getLayouts(),
      nodes: dashboardRepo.getNodes(),
      demoMetricRange: dashboardRepo.getDemoMetricRange(),
    });

    setDemoMetricRange(demoMetricRange);
  };

  console.info({ demoMetricRange });

  return (
    <DashboardAppContainer className="dashboard-container">
      <div className="app-controls">
        <PresetControl
          presetList={presetList}
          onRefreshPresetList={handleRefreshList}
          onClickPreset={(preset) => {
            if (preset.presetEid !== pagePresetId) {
              setLoading(true);
            }
            pageHistory.push(
              generatePath(match.path, {
                presetId: preset.presetEid,
              }),
            );
          }}
          currentPreset={pagePresetId}
          presetRepo={dashboardRepo}
          onClickAddNew={handleAddNewPreset}
          onUpdatePreset={handleUpdatePreset}
        ></PresetControl>
      </div>
      {isLoading && <div>Loading...</div>}
      {!isLoading && (
        <DataQueryContext.Provider
          value={{
            demoMetricRange: demoMetricRange,
          }}
        >
          <DashboardPage
            key={pagePresetId}
            presetId={pagePresetId}
            dashboardRepo={dashboardRepo}
          />
        </DataQueryContext.Provider>
      )}
      <ToastContainer closeOnClick={false} hideProgressBar={true} />
    </DashboardAppContainer>
  );
}

export function DashboardPage({
  defaultLayout = DEFAULT_LAYOUT,
  dashboardRepo,
  presetId,
}) {
  const [state, dispatch] = React.useReducer(dashboardReducer, {
    layouts: dashboardRepo.getLayouts(),
    nodes: dashboardRepo.getNodes(),
    breakpoint: defaultLayout,
  });

  const { current, push, undo, redo } = useDashboardHistory();
  const preset = usePresetServer(API_INSIGHT_URL);
  const updateRef = React.useRef({
    isJustAddedNewNode: false,
    isUsingHistory: false,
  });
  const { layouts, nodes } = state;
  console.info('current=>', layouts, nodes);

  useHotkeys('ctrl+z,command+z', () => {
    console.info('undo');
    updateRef.current.isUsingHistory = true;
    undo();
  });
  useHotkeys('ctrl+shift+z,command+shift+z', () => {
    updateRef.current.isUsingHistory = true;
    redo();
  });

  React.useEffect(() => {
    if (dashboardRepo.isReady()) {
      console.info({ dashboardId: dashboardRepo.getId() });
      dashboardRepo.setNodes(state.nodes);
      dashboardRepo.updateLayouts(state.layouts);
      push({ layouts: state.layouts, nodes: state.nodes });
      preset
        .update(dashboardRepo.getId(), {
          layouts: dashboardRepo.getLayouts(),
          nodes: dashboardRepo.getNodes(),
        })
        .then((ps) => {
          dashboardRepo.setId(ps.presetEid);
          dashboardRepo.updateLayouts(state.layouts);
        });
    }
  }, [state]);

  React.useEffect(() => {
    if (current && updateRef.current.isUsingHistory) {
      let record: Dashboard = JSON.parse(current);
      dispatch({
        type: 'SET',
        payload: {
          nodes: record.nodes,
          layouts: record.layouts,
          reason: 'history',
        },
      });
    }
  }, [current]);

  const handleLayoutChange = React.useCallback(
    debounce(
      (newLayout, previousLayout) => {
        console.info(
          'handle change layout ==> ',
          newLayout,
          updateRef.current,
        );
        if (
          !updateRef.current.isJustAddedNewNode &&
          !updateRef.current.isUsingHistory &&
          dashboardRepo.isReady()
        ) {
          dispatch({
            type: 'SET_LAYOUT',
            payload: { layouts: newLayout },
          });
        } else {
          updateRef.current.isJustAddedNewNode = false;
          updateRef.current.isUsingHistory = false;
        }
      },
      700,
      { trailing: true },
    ),
    [],
  );

  if (!layouts || !nodes) return <div>Loading...</div>;

  const handleAddNewNode = (x: number, y: number) => {
    const node = {
      id: nanoid(),
      layout: {
        x,
        y,
        w: 4,
        h: 2,
      },
    };
    updateRef.current.isJustAddedNewNode = true;
    dispatch({
      type: 'SET',
      payload: {
        layouts: mapValues(layouts, (layout) => {
          return layout.concat(node);
        }),
        nodes: nodes.concat({
          id: node.id,
          chartLibId: undefined,
        }),
      },
    });
    return node;
  };

  const handleBreakpointChange = (newBp, newCols) => {
    console.info('change breakpoint', { newBp, newCols });
    dispatch({
      type: 'CHANGE_BREAKPOINT',
      payload: { breakpoint: newBp },
    });
  };

  const handleRemoveNode = (id: string) => {
    dispatch({ type: 'REMOVE_NODE', payload: { nodeId: id } });
  };

  const handleNodeSubmit = (nodeId: string, payload: NodeData) => {
    dispatch({
      type: 'UPDATE_NODE',
      payload: {
        nodeId,
        data: payload,
      },
    });
  };

  const handleUpdateNodes = (nodes) => {
    dispatch({
      type: 'SET',
      payload: {
        nodes: nodes,
      },
    });
    dashboardRepo.setNodes(nodes);
  };

  return (
    <DashboardFrameContext.Provider
      value={{
        onAddNewNode: handleAddNewNode,
        onBreakpointChange: handleBreakpointChange,
        onLayoutChange: handleLayoutChange,
        onRemoveNode: handleRemoveNode,
        onNodeSubmitUpdate: handleNodeSubmit,
        onUpdateNodes: handleUpdateNodes,
        onNodeRequestUpdate: () => {},
      }}
    >
      <React.Fragment>
        <Box className={'onlyprint'} marginBottom={2}>
          <PrintLogo />
        </Box>
        <DashboardFrame layouts={layouts} nodes={nodes} />
        <PrintControl />
      </React.Fragment>
    </DashboardFrameContext.Provider>
  );
}

const DashboardAppContainer = styled('div')`
  margin-bottom: 5em;
  & .app-controls {
    z-index: 1200;
    display: flex;
    flex-direction: row-reverse;
    margin-bottom: 2em;
  }
`;
