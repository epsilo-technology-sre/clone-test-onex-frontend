// ** React Perfect Scrollbar
import 'react-perfect-scrollbar/dist/css/styles.css';

// ** Core styles
import '../app.scss';

import React from 'react';
import DashboardPage from '../dashboard-app';

export default function MainPage() {
  return <DashboardPage />;
}

