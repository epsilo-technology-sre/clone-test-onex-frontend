import React from 'react';
export default {
  title: 'DBF / APP',
};
import {
  BrowserRouter as AppRouter,
  Route,
  Switch,
} from 'react-router-dom';

let App = React.lazy(() => import('./app'));

export function Primary() {
  return (
    <AppRouter>
      <Switch>
        <Route
          path={'/'}
          render={() => (
            <React.Suspense fallback={<div>Loading...</div>}>
              <App />
            </React.Suspense>
          )}
        ></Route>
      </Switch>
    </AppRouter>
  );
}
Primary.decorators = [Story => <Story />]
