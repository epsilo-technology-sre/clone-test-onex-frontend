import * as React from 'react';
import { request } from '@ep/one/src/utils';
import { customAlphabet } from 'nanoid';
import moment from 'moment';

const genId = customAlphabet(
  '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_',
  21,
);

export function genMetricId() {
  return 'EM_' + genId();
}

export function genSimpleId(length = 5) {
  const genId = customAlphabet('0123456789', length);
  return genId();
}

function objectMapper(payload) {
  let metric: CustomMetric = {};
  metric.formula = payload.formula;
  metric.id = payload.metric_id;
  metric.eid = payload.metric_eid;
  metric.transpiledCode = payload.code_generated;
  metric.requiredMetrics = payload.metric_required;
  metric.requiredFunctions = payload.function_required;
  metric.name = payload.metric_name;
  metric.createdAt = moment(payload.created_at);
  metric.updatedAt = moment(payload.updated_at);
  return metric;
}

export function useMetricServer(serverURL) {
  const cached = React.useRef(null);

  function getList() {
    if (cached.current !== null) {
      return Promise.resolve(cached.current);
    }
    return request
      .get(`${serverURL}/api/insight/custom_metrics`)
      .then((res) => {
        let list = [];
        list = res.data.map(objectMapper);
        cached.current = list;
        return list;
      });
  }

  function getSingle(metricEid) {
    let cachedItem = null;
    if (cached.current) {
      cachedItem = cached.current.find((m) => m.eid === metricEid);
    }

    if (cachedItem) {
      return Promise.resolve(cachedItem);
    }

    return request
      .get(`${serverURL}/api/insight/custom_metrics/${metricEid}`)
      .then((res) => {
        let preset = null;
        let metric: CustomMetric;
        if (res.data) {
          metric = objectMapper(res.data);
        }

        return preset;
      })
      .catch((err) => {
        return null;
      });
  }

  function update(
    payload: Partial<CustomMetric>,
  ): Promise<CustomMetric> {
    return request
      .post(`${serverURL}/api/insight/custom_metrics`, {
        name: payload.name,
        metric_eid: payload.eid,
        formula: payload.formula,
      })
      .then((res) => {
        cached.current = null;
        return objectMapper(res.data);
      });
  }

  return {
    getList,
    getSingle,
    update,
  };
}
