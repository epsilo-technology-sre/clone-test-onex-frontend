import { API_URL } from '@ep/one/global';
import { request } from '@ep/one/src/utils';

const EPI_ENDPOINT = {
  getMetricsDaily: () => `${API_URL}/api/v1/dashboard/charts`,
  getMetricSummary: () => `${API_URL}/api/v1/dashboard/metrics`,
};

export function metricRequest({
  from,
  to,
  metrics,
  shop_eids,
  features = ['M_TOK_PA', 'M_LZD_SS', 'M_SHP_KB'],
  currency = 'USD',
  isSummary = false,
}) {
  if (isSummary) {
    return request
      .get(EPI_ENDPOINT.getMetricSummary(), {
        from,
        to,
        features,
        currency,
        shop_eids,
      })
      .then((res) => {
        const { data } = res;
        let qMetrics = [].concat(metrics);
        let result = qMetrics.map((m) =>
          data.find((r) => r.code === m),
        );

        let headers = ['date'].concat(metrics);
        let rows = [[[from, to], ...result.map((i) => i.value)]];

        return {
          data: {
            headers,
            rows,
          },
        };
      });
  }
  return request
    .get(EPI_ENDPOINT.getMetricsDaily(), {
      from,
      to,
      metrics: [].concat(metrics),
      features,
      shop_eids,
      currency,
    })
    .then((res) => {
      const { data } = res;
      const headers = Object.keys(data).filter((i) => i !== 'labels');
      const rows = data[headers[0]].map((i, index) => {
        return headers.map((h) => data[h][index]);
      });

      return { data: { headers, rows } };
    });
}
