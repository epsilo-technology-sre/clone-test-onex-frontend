import numeral from 'numeral';

export const formatCurrency = (number, currency) => {
  if (currency === 'VND' || currency === 'IDR') {
    return numeral(number).format('0,0');
  }
  return numeral(number).format('0,0[.]00');
};

export const formatNumber = (number) => {
  return numeral(number).format('0[,]0[.]00a');
};

export const formatPercent = (number) => {
  return numeral(number).format('0[,]0[.]00');
}

export const parseObjectToParam = (obj: any) => {
  let str = '';
  for (let key in obj) {
    if (str != '') {
      str += '&';
    }
    if (obj[key] !== undefined) {
      str += key + '=' + encodeURIComponent(obj[key]);
    }
  }
  return str;
};
