import moment from 'moment';
import { metricRequest } from './metric-query';
import { getAllMetrics, getAllShops } from './async-resources';

let defaultCohorts = [
  { value: 'mtd', label: 'MTD' },
  { value: '7-days', label: 'Last 7 days' },
  { value: '30-days', label: 'Last 30 days' },
  { value: '3-months', label: 'Last 3 months' },
  { value: '6-months', label: 'Last 6 months' },
  { value: '9-months', label: 'Last 9 months' },
];

export function translateCohort(cohort) {
  let dateRange = { from: null, to: null };
  let prevRange = { from: null, to: null };
  let yesterday = moment().subtract(1, 'day');
  switch (cohort) {
    case 'mtd': {
      dateRange.from = moment().startOf('month').format('YYYY-MM-DD');
      dateRange.to = yesterday.format('YYYY-MM-DD');
      prevRange.from = moment()
        .subtract(1, 'month')
        .startOf('month')
        .format('YYYY-MM-DD');
      prevRange.to = moment()
        .startOf('month')
        .subtract(1, 'day')
        .format('YYYY-MM-DD');
      break;
    }
    default: {
      const [number, period] = String(cohort).split('-');
      dateRange.from = moment()
        .subtract(number, period)
        .format('YYYY-MM-DD');
      dateRange.to = yesterday.format('YYYY-MM-DD');
      prevRange.from = moment()
        .subtract(number * 2, period)
        .format('YYYY-MM-DD');
      prevRange.to = moment(yesterday)
        .subtract(number, period)
        .format('YYYY-MM-DD');
    }
  }

  return { current: dateRange, previous: prevRange };
}

async function getShopIds(dimensions) {
  if (dimensions.shops && dimensions.shops.length > 0) {
    return dimensions.shops;
  } else {
    let allShops = await getAllShops();
    return filterShopIdsBy(allShops, dimensions).map(
      (s) => s.shop_eid,
    );
  }
}

function filterShopIdsBy(allShops, { channels, countries, tools }) {
  let fShops = allShops;
  if (channels && channels.length > 0) {
    fShops = fShops.filter(
      (s) => channels.indexOf(s.channel_code) >= 0,
    );
  } else if (countries && countries.length > 0) {
    fShops = fShops.filter(
      (s) => countries.indexOf(s.country_code) >= 0,
    );
  } else if (tools && tools > 0) {
    fShops = fShops.filter((s) =>
      tools.some((t) => s.features.find((f) => f.feature_code === t)),
    );
  }
  return fShops;
}

async function dataQuery({
  metrics,
  dateRange,
  dimensions,
  isSummary,
}: {
  metrics: string[];
  dateRange: { from: string; to: string };
  dimensions: any;
  isSummary?: boolean;
}) {
  let shopIds = await getShopIds(dimensions);

  let data = await metricRequest({
    from: dateRange.from,
    to: dateRange.to,
    metrics,
    shop_eids: shopIds,
    isSummary,
  });
  let metricLabels = await getAllMetrics(shopIds);

  const result = {
    data: data.data,
    metricLabels,
  };

  return result;
}

export { dataQuery };
