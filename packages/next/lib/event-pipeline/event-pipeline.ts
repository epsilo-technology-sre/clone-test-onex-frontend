import React from 'react';
import { nanoid } from 'nanoid';
import { EventEmitter } from 'events';

type topic = string;
type subTopic = 'loading' | 'success' | 'error';

type EventHandler = (
  evtInstance: EventInstance,
  args: any[],
  next?: () => void,
) => void;

export type EventFeedback = {
  when(
    topic: subTopic,
    handle: (args?: any[]) => void,
  ): EventFeedback;
};

export type EventPipeType = {
  on: (topic: topic, handler: EventHandler) => any;
  trap: (topic: topic, handler: EventHandler) => void;
  pipe: (topic: topic, handler: EventHandler) => any;
  bindTrigger: (topic: topic) => EventInstance;
  trigger: (topic, args: any[]) => EventFeedback;
};

export type EventInstance = {
  token: string;
  topic: topic;
  trigger: (subTopic: subTopic, args?: any[]) => void;
};

export type EventContextType = { ep: EventPipe };

export class EventPipe implements EventPipeType {
  ee: EventEmitter;

  constructor() {
    this.ee = new EventEmitter();
    this.ee.on('newListener', this.handleAddNewListener);
    this.ee.on('removeListener', this.handleRemoveListener);
  }

  extend(ep: EventPipe) {
    let selfOn = this.on;
    this.on = (topic: string, handler) => {
      if (this.ee.listenerCount(topic) === 0) {
        ep.on(topic, handler);
      } else {
        selfOn(topic, (evtInstance: EventInstance, args: any[]) => {
          handler(evtInstance, args, () => ep.trigger(topic, args));
        });
      }
    };
    ep.handleRemoveListener('eventName');
  }

  handleAddNewListener(eventName: string) {
    console.info('add listener', eventName);
  }

  handleRemoveListener(eventName: string) {
    console.info('remove listener', eventName);
  }

  trap: (topic: string, handler: EventHandler) => void;
  pipe: (topic: string, handler: EventHandler) => any;
  bindTrigger: (topic: string) => EventInstance;

  getToken() {
    return 'ep_' + nanoid(10);
  }

  getEventName(topic: topic, subTopic: subTopic, token: string) {
    return `${topic}/${token}/${subTopic}`;
  }

  on(topic: topic, handler: EventHandler) {
    this.ee.on(topic, handler);
  }

  getEventFeedback(topic: topic, token: string) {
    const self = this;
    const ee = this.ee;
    let eventList = [];
    let evtFeedback = {
      when(
        subTopic: subTopic,
        handler: (args: any[]) => void,
      ): EventFeedback {
        let eventName = self.getEventName(topic, subTopic, token);
        eventList.push(eventName);
        ee.on(eventName, handler);
        return evtFeedback;
      },
      destroy() {
        eventList.forEach((e) => ee.removeAllListeners(e));
      },
    };

    return evtFeedback;
  }

  // FIXME: memory leak
  trigger(topic: string, args: any[]): EventFeedback {
    let token = this.getToken();
    let ee = this.ee;
    let getEventName = this.getEventName;
    const eventInstance: EventInstance = {
      topic,
      token,
      trigger: (stopic, args: any[]) => {
        ee.emit(getEventName(topic, stopic, token), args);
      },
    };

    window.setTimeout(() => ee.emit(topic, eventInstance, args), 0);

    if (ee.listenerCount(topic) === 0) {
      console.warn('no listener available', topic, args);
    }
    return this.getEventFeedback(topic, token);
  }

  createTrigger(
    topic: string,
    register?: (feedback: EventFeedback) => void,
  ): { trigger: (...args: any[]) => void; destroy: () => void } {
    let token = this.getToken();
    let ee = this.ee;
    let getEventName = this.getEventName;

    const eventInstance: EventInstance = {
      topic,
      token,
      trigger: (stopic, args: any[]) => {
        ee.emit(getEventName(topic, stopic, token), args);
      },
    };
    let evtFeedback = this.getEventFeedback(topic, token);

    if (register) {
      register(evtFeedback);
    }

    return {
      trigger: (...args: any[]) => {
        window.setTimeout(
          () => ee.emit(topic, eventInstance, args),
          0,
        );
      },
      destroy: () => {
        evtFeedback.destroy();
      },
    };
  }
}

export const EventContext = React.createContext<EventContextType>({
  ep: new EventPipe(),
});
