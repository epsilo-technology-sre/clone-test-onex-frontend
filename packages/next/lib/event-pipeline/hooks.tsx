import React from 'react';
import { EventContext } from './event-pipeline';
import type { EventFeedback } from './event-pipeline';

export function useEventPipelineLegacy() {
  const { ep } = React.useContext(EventContext);
  const triggerList = React.useRef([]);

  React.useEffect(() => {
    return () => {
      triggerList.current.forEach((destroy) => destroy());
    };
  }, []);

  const createTrigger = (
    event: string,
    handleFeedback?: (evtFeedback: EventFeedback) => void,
  ) => {
    const { trigger, destroy } = ep.createTrigger(
      event,
      handleFeedback,
    );
    triggerList.current = triggerList.current.concat(destroy);
    return trigger;
  };

  return createTrigger;
}

export function useEventPipeline(
  eventRegister: (fn) => { [key: string]: any },
): { [key: string]: any } {
  const { ep } = React.useContext(EventContext);
  const triggerList = React.useRef([]);

  const createTrigger = (
    event: string,
    handleFeedback?: (evtFeedback: EventFeedback) => void,
  ) => {
    const { trigger, destroy } = ep.createTrigger(
      event,
      handleFeedback,
    );
    triggerList.current = triggerList.current.concat(destroy);
    return trigger;
  };
  const triggers = React.useMemo(() => {
    return eventRegister(createTrigger);
  }, []);
  React.useEffect(() => {
    return () => {
      triggerList.current.forEach((destroy) => destroy());
    };
  }, []);

  return triggers;
}
