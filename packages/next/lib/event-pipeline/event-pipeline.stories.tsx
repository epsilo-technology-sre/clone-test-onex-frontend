import React from 'react';
import styled from 'styled-components';
import { EventContext, EventPipe } from './event-pipeline';
import { useEventPipeline } from './hooks';

export default {
  title: 'DBF / EventPipeline',
};

const Container = styled('div')`
  display: flex;
  padding: 5em;
  align-items: flex-end;
  border: 1px solid #333;
  border-radius: 10px;
`;

const Btn = styled(`button`)`
  margin: 0.5em;
`;

export function Primary() {
  let ep = React.useMemo(() => new EventPipe(), []);

  React.useEffect(() => {
    ep.on('clickButton', (evtInstance, [text]) => {
      alert(`You just click "${text}"`);
    });
    ep.on('clickAndWait', (evtInstance, [text]) => {
      evtInstance.trigger('loading', []);
      window.setTimeout(() => {
        evtInstance.trigger('success', []);
      }, 1000);
    });
  }, []);

  return (
    <EventContext.Provider value={{ ep }}>
      <Comp1 />
    </EventContext.Provider>
  );
}

function Comp1() {
  return (
    <Container>
      <Comp2 />
    </Container>
  );
}

function Comp2() {
  return (
    <Container>
      <Comp3 />
    </Container>
  );
}

function Comp3() {
  const [switchOn, setSwitchOn] = React.useState(true);
  return (
    <Container>
      {switchOn ? <Comp4 /> : <Comp5 />}
      <Btn
        onClick={() => {
          setSwitchOn(!switchOn);
        }}
      >
        Switch
      </Btn>
    </Container>
  );
}

function Comp4() {
  const [loading, setLoading] = React.useState(false);
  const [loading1, setLoading1] = React.useState(false);
  // const createTrigger = useEventPipeline(['clickMe', 'clickAndWait', 'clickAndWait']);
  const { onClickMe, onClickAndWait, onClickAndWait1 } = useEventPipeline(
    (createTrigger) => {
      return {
        onClickMe: createTrigger('clickButton'),
        onClickAndWait: createTrigger('clickAndWait', (evtFeedback) => {
          evtFeedback
            .when('loading', () => {
              setLoading(true);
            })
            .when('success', () => {
              setLoading(false);
            });
        }),
        onClickAndWait1: createTrigger(
          'clickAndWait',
          (evtFeedback) => {
            evtFeedback
              .when('loading', () => {
                setLoading1(true);
              })
              .when('success', () => {
                setLoading1(false);
              });
          },
        ),
      };
    },
  );

  return (
    <Container>
      <Btn onClick={() => onClickMe('clickMe')}>Click me</Btn>
      <Btn
        onClick={() => {
          onClickAndWait('click and wait');
        }}
      >
        {loading ? 'loading...' : 'Click and wait'}
      </Btn>
      <Btn
        onClick={() => {
          onClickAndWait1('click and wait');
        }}
      >
        {loading1 ? 'loading...' : 'Click and wait1'}
      </Btn>
    </Container>
  );
}

function Comp5() {
  return <Container>Component 5</Container>;
}
