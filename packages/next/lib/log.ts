const createDebug = require('debug');
createDebug.log = console.info.bind(console);

export function useLog(namespace: string) {
  return createDebug(namespace);
}
