import React from 'react';
import clsx from 'clsx';

export function TextScale({ children, ...rest }) {
  return (
    <div {...rest} className={clsx(rest.className, 'js-textscale')}>
      {children}
    </div>
  );
}
