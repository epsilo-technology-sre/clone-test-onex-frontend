import antlr4 from 'antlr4';
import SeaGrammarLexer from '../seaLexer';
import SeaParser from '../seaParser';
import { CaseChangingStream } from '../CaseChangingStream';
import SeaVisitor from '../implSeaVisitor';

const log = require('debug')('sea:test');
// import SeaListener from '../seaListener';

function parseTree(input: string) {
  const chars = new antlr4.InputStream(input);
  const upperChars = new CaseChangingStream(chars, true);
  const lexer = new SeaGrammarLexer(upperChars);
  const tokens = new antlr4.CommonTokenStream(lexer);
  const parser = new SeaParser(tokens);
  parser.buildParseTrees = true;
  const tree = parser.program();
  return tree;
}

function calculate(tree) {
  const visitor = new SeaVisitor();
  return visitor.visit(tree);
}

describe('SEA valid syntax', () => {
  let testCases = [
    '79',
    '39.39',
    '"a string"',
    '"một chuỗi kí tự trong tiếng việt"',
    '"ไทย(thai)"',
    '@gmv',
    '@gmv(3d)',
    '@gmv / @cost',
    'a_function_call_0_argument()',
    'a_function_call_1_argument(1234)',
    'a_function_call_n_argument(1,2,3,4,5)',
    'IF @GMV(7d) < @GMV(30d) / 5 THEN 1 ELSE 0',
    'IF @gmv(7d) < @gmv(30d) / 5 THEN 1 ELSE 0',
    'if @gmv(7d) < @gmv(30d) / 5 then 1 else 0',
  ];

  let consoleError;
  beforeAll(() => {
    consoleError = jest.spyOn(console, 'error');
  });
  afterEach(() => {
    consoleError.mockReset();
  });
  afterAll(() => {
    consoleError.mockRestore();
  });

  for (let testcase of testCases) {
    it(testcase, () => {
      parseTree(testcase + '\n');
      expect(consoleError).not.toHaveBeenCalled();
    });
  }
});

describe('SEA invalid syntax', () => {
  let testCases = ['gmv', 'gmv / cost', '@ gvm'];

  let consoleError;
  beforeAll(() => {
    consoleError = jest.spyOn(console, 'error');
  });
  afterEach(() => {
    consoleError.mockReset();
  });
  afterAll(() => {
    consoleError.mockRestore();
  });

  for (let testcase of testCases) {
    it(testcase, () => {
      parseTree(testcase + '\n');
      expect(consoleError).toHaveBeenCalled();
    });
  }
});

describe('SEA formula evaluate correctly:', () => {
  const testCases: [string, any][] = [
    ['79', 79],
    ['79 - 39', 40],
    ['12 + 21', 33],
    ['21/3', 7],
    ['79 - 39 / 3', 66],
    ['(79 - 39) / 2', 20],
    ['IF 1 then 3 ELSE 2', 3],
  ];
  for (let [item, expectResult] of testCases) {
    it(item + ' = ' + expectResult, () => {
      let [funStr] = calculate(parseTree(item + '\n'));

      let fun = new Function('eMetric', funStr);
      let result = fun.call(null, () => {
        return null;
      });
      expect(result).toEqual([].concat(expectResult).slice(-1)[0]);
    });
  }

  let metricTestCases = [
    ['if @gmv / 30 < 1000 then 1 else 0', [['@gmv', null, 3000]], 1],
    ['if @gmv / 30 < 1000 then 1 else 0', [['@gmv', null, 30000]], 0],
    [
      'if @gmv(7d) / 30 < 1000 then 1 else 0',
      [['@gmv', '7d', 3000]],
      1,
    ],
    [
      'if @gmv(7d) / 30 < 1000 and @roas(7d) < 1 then 1 else 0',
      [
        ['@gmv', '7d', 3000],
        ['@roas', '7d', 0.1],
      ],
      1,
    ],
    [
      'if @gmv(7d) / 30 < 1000 and @roas(7d) < 1 then 1 + 1 else 0',
      [
        ['@gmv', '7d', 3000],
        ['@roas', '7d', 0.1],
      ],
      2,
    ],
    [
      'if @gmv(7d) / 30 < 1000 and @roas(7d) < 1 then if @gmv(7d) / 10 < 400 then 1 + 1 else 1 else 0',
      [
        ['@gmv', '7d', 3000],
        ['@roas', '7d', 0.1],
      ],
      2,
    ],
    [
      'if @gmv(7d) / 30 < 1000 and @roas(7d) < 1 then (if @gmv(7d) / 10 < 400 then 1 + 1 else 1) else 0',
      [
        ['@gmv', '7d', 3000],
        ['@roas', '7d', 0.1],
      ],
      2,
    ],
    [
      'if @gmv(7d) / 30 < 1000 and @roas(7d) < 1 then (if @gmv(7d) / 10 < 400 then 1 + 1 else 1) else raise_bidding_price(0.1)',
      [
        ['@gmv', '7d', 3000],
        ['@roas', '7d', 0.1],
      ],
      2,
    ],
    [
      'if @gmv(7d) / 30 < 1000 and @roas(7d) < 1 then (if @gmv(7d) / 10 < 400 then 1 + 1 else 1) else raise_bidding_price(0.1)',
      [
        ['@gmv', '7d', 30000],
        ['@roas', '7d', 0.1],
      ],
      'execute function raise_bidding_price with arguments 0.1',
    ],
  ];

  for (let testcase of metricTestCases) {
    let item: string = testcase[0];
    let metricMocks: [string, string, any][] = testcase[1];
    let expectResult: any = testcase[2];
    it(item + ' = ' + JSON.stringify(expectResult), () => {
      let [funStr, requiredMetrics] = calculate(
        parseTree(item + '\n'),
      );

      log(funStr);

      let fun = new Function('eMetric', 'eFunction', funStr);
      let result = fun.call(
        null,
        (metricId, period) => {
          let metricParams = metricMocks.find(
            (i) => i[0] === metricId && i[1] === period,
          );

          if (
            metricId === metricParams[0] &&
            period === metricParams[1]
          ) {
            return metricParams[2];
          }

          throw Error('Not valid');
        },
        (funName, args) => {
          return `execute function ${funName} with arguments ${args.join(
            ',',
          )}`;
        },
      );

      log(
        requiredMetrics,
        metricMocks.map((i) => [i[0], i[1]]),
      );

      expect(requiredMetrics).toEqual(
        metricMocks.map((i) => [i[0], i[1]]),
      );
      expect(result).toEqual(expectResult);
    });
  }
});
