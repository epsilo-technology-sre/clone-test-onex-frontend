import { get } from 'lodash';
import { useLog } from '@epi/next/lib/log';

const log = useLog('sea:exec');
const error = useLog('error:sea:exec');

export function calculateMetric(customMetric: CustomMetric) {
  let codeJS = get(customMetric, 'transpiledCode.js');
  let calFun = new Function('$eMetric', '$eFunction', codeJS);

  return ($eMetric, $eFunction) => {
    try {
      log('do calculation', calFun, $eMetric, $eFunction);
      return calFun($eMetric, $eFunction);
    } catch (err) {
      error('error executation', customMetric, err);
    }
  };
}
