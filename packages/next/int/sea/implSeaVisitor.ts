// Generated from sea.g4 by ANTLR 4.9.2
// jshint ignore: start
import GeneratedSeaVisitor from './seaVisitor';
import uniqBy from 'lodash/uniqBy';

const log = require('debug')('sea:syntax');

// This class defines a complete generic visitor for a parse tree produced by seaParser.

export default class SeaVisitor extends GeneratedSeaVisitor {
  requiredMetrics = [];
  requiredFunctions = [];

  // Visit a parse tree produced by seaParser#program.
  visitProgram(ctx) {
    let transpiledCode = `return ${ctx.stmt().accept(this)};`;
    const uniqueMetrics = uniqBy(this.requiredMetrics, (i) =>
      i.join(''),
    );

    return [
      transpiledCode,
      uniqueMetrics,
      uniqBy(this.requiredFunctions, (i) => i),
    ];
  }

  // Visit a parse tree produced by seaParser#stmt.
  visitStmt(ctx) {
    return ctx.getChild(0).accept(this);
  }

  visitExpression(ctx) {
    return ctx.expr().accept(this);
  }

  // Visit a parse tree produced by seaParser#NumberLiteralExpression.
  visitNumberLiteral(ctx) {
    return ctx.Number().getText();
  }

  // Visit a parse tree produced by seaParser#ParenthesizedExpression.
  visitParenthesizedExpression(ctx) {
    return '(' + ctx.stmt().accept(this) + ')';
  }

  // Visit a parse tree produced by seaParser#AdditiveExpression.
  visitAdditiveExpression(ctx) {
    let left = ctx.expr(0).accept(this);
    let right = ctx.expr(1).accept(this);

    switch (ctx.op.text) {
      case '-': {
        return [left, '-', right].join(' ');
      }
      case '+': {
        return [left, '+', right].join(' ');
      }
    }
  }

  // Visit a parse tree produced by seaParser#RelationalExpression.
  visitRelationalExpression(ctx) {
    let left = ctx.expr(0).accept(this);
    let right = ctx.expr(1).accept(this);
    let optext = ctx.op.text;

    switch (optext) {
      case '=': {
        return [left, '==', right].join(' ');
      }
      default: {
        return [left, optext, right].join(' ');
      }
    }
  }

  // Visit a parse tree produced by seaParser#LogicalAndExpression.
  visitLogicalAndExpression(ctx) {
    let left = ctx.expr(0).accept(this);
    let right = ctx.expr(1).accept(this);

    return left + ' && ' + right;
  }

  // Visit a parse tree produced by seaParser#StringLiteral.
  visitStringLiteral(ctx) {
    return ['"', String(ctx.StringLiteral().getText()), '"'].join('');
  }

  // Visit a parse tree produced by seaParser#LogicalOrExpression.
  visitLogicalOrExpression(ctx) {
    let left = ctx.expr(0).accept(this);
    let right = ctx.expr(1).accept(this);

    return left + ' || ' + right;
  }

  visitBooleanLiteral(ctx) {
    return ctx.getText();
  }

  // Visit a parse tree produced by seaParser#MetricExpression.
  visitMetricExpression(ctx) {
    const metricId = ctx.getText();
    return `eMetric("${metricId}")`;
  }

  // Visit a parse tree produced by seaParser#MutilplicativeExpression.
  visitMutilplicativeExpression(ctx) {
    let left = ctx.expr(0).accept(this);
    let right = ctx.expr(1).accept(this);

    switch (ctx.op.text) {
      case '/': {
        return [left, '/', right].join('');
      }
      case '*': {
        return [left, '*', right].join('');
      }
    }
  }

  // Visit a parse tree produced by seaParser#FunctionCallExpression.
  visitFunctionCallExpression(ctx) {
    return ctx.getChild(0).accept(this);
  }

  // Visit a parse tree produced by seaParser#expr_list.
  visitExpr_list(ctx) {
    const exprList = ctx.expr().map((i) => i.accept(this));
    return exprList.join(', ');
  }

  // Visit a parse tree produced by seaParser#IfStatement.
  visitIfStatement(ctx) {
    const cond = ctx.expr(0).accept(this);
    const trueResult = ctx.stmt(0).accept(this);
    const falseResult = ctx.stmt(1).accept(this);
    return `${cond} ? ${trueResult} : ${falseResult}`;
  }

  // Visit a parse tree produced by seaParser#function_call.
  visitFunction_call(ctx) {
    let funName = ctx.VarName().getText();
    let expressionList = ctx.expr_list(0).accept(this);

    this.requiredFunctions.push(funName);
    return `eFunction("${funName}", [${expressionList}] )`;
  }

  visitMetricQueryExpression(ctx) {
    return ctx.getChild(0).accept(this);
  }

  visitMetric_query(ctx) {
    this.requiredMetrics.push([
      ctx.Metric().getText(),
      ctx.Cohort() ? ctx.Cohort().getText() : null,
    ]);
    return `eMetric("${ctx.Metric().getText()}", ${
      ctx.Cohort() ? '"' + ctx.Cohort().getText() + '"' : 'null'
    })`;
  }
}
