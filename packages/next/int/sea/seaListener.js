// Generated from sea.g4 by ANTLR 4.9.2
// jshint ignore: start
import antlr4 from 'antlr4';

// This class defines a complete listener for a parse tree produced by seaParser.
export default class seaListener extends antlr4.tree.ParseTreeListener {

	// Enter a parse tree produced by seaParser#program.
	enterProgram(ctx) {
	}

	// Exit a parse tree produced by seaParser#program.
	exitProgram(ctx) {
	}


	// Enter a parse tree produced by seaParser#stmt.
	enterStmt(ctx) {
	}

	// Exit a parse tree produced by seaParser#stmt.
	exitStmt(ctx) {
	}


	// Enter a parse tree produced by seaParser#ParenthesizedExpression.
	enterParenthesizedExpression(ctx) {
	}

	// Exit a parse tree produced by seaParser#ParenthesizedExpression.
	exitParenthesizedExpression(ctx) {
	}


	// Enter a parse tree produced by seaParser#AdditiveExpression.
	enterAdditiveExpression(ctx) {
	}

	// Exit a parse tree produced by seaParser#AdditiveExpression.
	exitAdditiveExpression(ctx) {
	}


	// Enter a parse tree produced by seaParser#RelationalExpression.
	enterRelationalExpression(ctx) {
	}

	// Exit a parse tree produced by seaParser#RelationalExpression.
	exitRelationalExpression(ctx) {
	}


	// Enter a parse tree produced by seaParser#LogicalAndExpression.
	enterLogicalAndExpression(ctx) {
	}

	// Exit a parse tree produced by seaParser#LogicalAndExpression.
	exitLogicalAndExpression(ctx) {
	}


	// Enter a parse tree produced by seaParser#StringLiteral.
	enterStringLiteral(ctx) {
	}

	// Exit a parse tree produced by seaParser#StringLiteral.
	exitStringLiteral(ctx) {
	}


	// Enter a parse tree produced by seaParser#BooleanLiteral.
	enterBooleanLiteral(ctx) {
	}

	// Exit a parse tree produced by seaParser#BooleanLiteral.
	exitBooleanLiteral(ctx) {
	}


	// Enter a parse tree produced by seaParser#LogicalOrExpression.
	enterLogicalOrExpression(ctx) {
	}

	// Exit a parse tree produced by seaParser#LogicalOrExpression.
	exitLogicalOrExpression(ctx) {
	}


	// Enter a parse tree produced by seaParser#MetricQueryExpression.
	enterMetricQueryExpression(ctx) {
	}

	// Exit a parse tree produced by seaParser#MetricQueryExpression.
	exitMetricQueryExpression(ctx) {
	}


	// Enter a parse tree produced by seaParser#MutilplicativeExpression.
	enterMutilplicativeExpression(ctx) {
	}

	// Exit a parse tree produced by seaParser#MutilplicativeExpression.
	exitMutilplicativeExpression(ctx) {
	}


	// Enter a parse tree produced by seaParser#FunctionCallExpression.
	enterFunctionCallExpression(ctx) {
	}

	// Exit a parse tree produced by seaParser#FunctionCallExpression.
	exitFunctionCallExpression(ctx) {
	}


	// Enter a parse tree produced by seaParser#NumberLiteral.
	enterNumberLiteral(ctx) {
	}

	// Exit a parse tree produced by seaParser#NumberLiteral.
	exitNumberLiteral(ctx) {
	}


	// Enter a parse tree produced by seaParser#expr_list.
	enterExpr_list(ctx) {
	}

	// Exit a parse tree produced by seaParser#expr_list.
	exitExpr_list(ctx) {
	}


	// Enter a parse tree produced by seaParser#IfStatement.
	enterIfStatement(ctx) {
	}

	// Exit a parse tree produced by seaParser#IfStatement.
	exitIfStatement(ctx) {
	}


	// Enter a parse tree produced by seaParser#function_call.
	enterFunction_call(ctx) {
	}

	// Exit a parse tree produced by seaParser#function_call.
	exitFunction_call(ctx) {
	}


	// Enter a parse tree produced by seaParser#metric_query.
	enterMetric_query(ctx) {
	}

	// Exit a parse tree produced by seaParser#metric_query.
	exitMetric_query(ctx) {
	}



}