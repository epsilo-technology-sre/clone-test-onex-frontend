// Generated from sea.g4 by ANTLR 4.9.2
// jshint ignore: start
import antlr4 from 'antlr4';
import seaListener from './seaListener.js';
import seaVisitor from './seaVisitor.js';


const serializedATN = ["\u0003\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786",
    "\u5964\u0003\u001cW\u0004\u0002\t\u0002\u0004\u0003\t\u0003\u0004\u0004",
    "\t\u0004\u0004\u0005\t\u0005\u0004\u0006\t\u0006\u0004\u0007\t\u0007",
    "\u0004\b\t\b\u0003\u0002\u0003\u0002\u0003\u0002\u0003\u0003\u0003\u0003",
    "\u0005\u0003\u0016\n\u0003\u0003\u0004\u0003\u0004\u0003\u0004\u0003",
    "\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003",
    "\u0004\u0005\u0004\"\n\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003",
    "\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003",
    "\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0007",
    "\u00043\n\u0004\f\u0004\u000e\u00046\u000b\u0004\u0003\u0005\u0003\u0005",
    "\u0003\u0005\u0007\u0005;\n\u0005\f\u0005\u000e\u0005>\u000b\u0005\u0003",
    "\u0006\u0003\u0006\u0003\u0006\u0003\u0006\u0003\u0006\u0003\u0006\u0003",
    "\u0006\u0003\u0007\u0003\u0007\u0003\u0007\u0007\u0007J\n\u0007\f\u0007",
    "\u000e\u0007M\u000b\u0007\u0003\u0007\u0003\u0007\u0003\b\u0003\b\u0003",
    "\b\u0003\b\u0005\bU\n\b\u0003\b\u0002\u0003\u0006\t\u0002\u0004\u0006",
    "\b\n\f\u000e\u0002\u0005\u0003\u0002\u0003\u0004\u0003\u0002\u0005\u0006",
    "\u0003\u0002\u0007\u000b\u0002]\u0002\u0010\u0003\u0002\u0002\u0002",
    "\u0004\u0015\u0003\u0002\u0002\u0002\u0006!\u0003\u0002\u0002\u0002",
    "\b7\u0003\u0002\u0002\u0002\n?\u0003\u0002\u0002\u0002\fF\u0003\u0002",
    "\u0002\u0002\u000eP\u0003\u0002\u0002\u0002\u0010\u0011\u0005\u0004",
    "\u0003\u0002\u0011\u0012\u0007\u0014\u0002\u0002\u0012\u0003\u0003\u0002",
    "\u0002\u0002\u0013\u0016\u0005\u0006\u0004\u0002\u0014\u0016\u0005\n",
    "\u0006\u0002\u0015\u0013\u0003\u0002\u0002\u0002\u0015\u0014\u0003\u0002",
    "\u0002\u0002\u0016\u0005\u0003\u0002\u0002\u0002\u0017\u0018\b\u0004",
    "\u0001\u0002\u0018\"\u0005\f\u0007\u0002\u0019\"\u0005\u000e\b\u0002",
    "\u001a\"\u0007\u0015\u0002\u0002\u001b\"\u0007\u0016\u0002\u0002\u001c",
    "\"\u0007\u0017\u0002\u0002\u001d\u001e\u0007\u000e\u0002\u0002\u001e",
    "\u001f\u0005\u0004\u0003\u0002\u001f \u0007\u000f\u0002\u0002 \"\u0003",
    "\u0002\u0002\u0002!\u0017\u0003\u0002\u0002\u0002!\u0019\u0003\u0002",
    "\u0002\u0002!\u001a\u0003\u0002\u0002\u0002!\u001b\u0003\u0002\u0002",
    "\u0002!\u001c\u0003\u0002\u0002\u0002!\u001d\u0003\u0002\u0002\u0002",
    "\"4\u0003\u0002\u0002\u0002#$\f\f\u0002\u0002$%\t\u0002\u0002\u0002",
    "%3\u0005\u0006\u0004\r&\'\f\u000b\u0002\u0002\'(\t\u0003\u0002\u0002",
    "(3\u0005\u0006\u0004\f)*\f\n\u0002\u0002*+\t\u0004\u0002\u0002+3\u0005",
    "\u0006\u0004\u000b,-\f\t\u0002\u0002-.\u0007\f\u0002\u0002.3\u0005\u0006",
    "\u0004\n/0\f\b\u0002\u000201\u0007\r\u0002\u000213\u0005\u0006\u0004",
    "\t2#\u0003\u0002\u0002\u00022&\u0003\u0002\u0002\u00022)\u0003\u0002",
    "\u0002\u00022,\u0003\u0002\u0002\u00022/\u0003\u0002\u0002\u000236\u0003",
    "\u0002\u0002\u000242\u0003\u0002\u0002\u000245\u0003\u0002\u0002\u0002",
    "5\u0007\u0003\u0002\u0002\u000264\u0003\u0002\u0002\u00027<\u0005\u0006",
    "\u0004\u000289\u0007\u0010\u0002\u00029;\u0005\u0006\u0004\u0002:8\u0003",
    "\u0002\u0002\u0002;>\u0003\u0002\u0002\u0002<:\u0003\u0002\u0002\u0002",
    "<=\u0003\u0002\u0002\u0002=\t\u0003\u0002\u0002\u0002><\u0003\u0002",
    "\u0002\u0002?@\u0007\u0011\u0002\u0002@A\u0005\u0006\u0004\u0002AB\u0007",
    "\u0012\u0002\u0002BC\u0005\u0004\u0003\u0002CD\u0007\u0013\u0002\u0002",
    "DE\u0005\u0004\u0003\u0002E\u000b\u0003\u0002\u0002\u0002FG\u0007\u0019",
    "\u0002\u0002GK\u0007\u000e\u0002\u0002HJ\u0005\b\u0005\u0002IH\u0003",
    "\u0002\u0002\u0002JM\u0003\u0002\u0002\u0002KI\u0003\u0002\u0002\u0002",
    "KL\u0003\u0002\u0002\u0002LN\u0003\u0002\u0002\u0002MK\u0003\u0002\u0002",
    "\u0002NO\u0007\u000f\u0002\u0002O\r\u0003\u0002\u0002\u0002PT\u0007",
    "\u001a\u0002\u0002QR\u0007\u000e\u0002\u0002RS\u0007\u001b\u0002\u0002",
    "SU\u0007\u000f\u0002\u0002TQ\u0003\u0002\u0002\u0002TU\u0003\u0002\u0002",
    "\u0002U\u000f\u0003\u0002\u0002\u0002\t\u0015!24<KT"].join("");


const atn = new antlr4.atn.ATNDeserializer().deserialize(serializedATN);

const decisionsToDFA = atn.decisionToState.map( (ds, index) => new antlr4.dfa.DFA(ds, index) );

const sharedContextCache = new antlr4.PredictionContextCache();

export default class seaParser extends antlr4.Parser {

    static grammarFileName = "sea.g4";
    static literalNames = [ null, "'*'", "'/'", "'+'", "'-'", "'<'", "'>'", 
                            "'<='", "'>='", "'='", "'OR'", "'AND'", "'('", 
                            "')'", "','", "'IF'", "'THEN'", "'ELSE'" ];
    static symbolicNames = [ null, null, null, null, null, null, null, null, 
                             null, null, null, null, null, null, null, null, 
                             null, null, "Newline", "Number", "Boolean", 
                             "StringLiteral", "UnterminatedStringLiteral", 
                             "VarName", "Metric", "Cohort", "WS" ];
    static ruleNames = [ "program", "stmt", "expr", "expr_list", "if_stmt", 
                         "function_call", "metric_query" ];

    constructor(input) {
        super(input);
        this._interp = new antlr4.atn.ParserATNSimulator(this, atn, decisionsToDFA, sharedContextCache);
        this.ruleNames = seaParser.ruleNames;
        this.literalNames = seaParser.literalNames;
        this.symbolicNames = seaParser.symbolicNames;
    }

    get atn() {
        return atn;
    }

    sempred(localctx, ruleIndex, predIndex) {
    	switch(ruleIndex) {
    	case 2:
    	    		return this.expr_sempred(localctx, predIndex);
        default:
            throw "No predicate with index:" + ruleIndex;
       }
    }

    expr_sempred(localctx, predIndex) {
    	switch(predIndex) {
    		case 0:
    			return this.precpred(this._ctx, 10);
    		case 1:
    			return this.precpred(this._ctx, 9);
    		case 2:
    			return this.precpred(this._ctx, 8);
    		case 3:
    			return this.precpred(this._ctx, 7);
    		case 4:
    			return this.precpred(this._ctx, 6);
    		default:
    			throw "No predicate with index:" + predIndex;
    	}
    };




	program() {
	    let localctx = new ProgramContext(this, this._ctx, this.state);
	    this.enterRule(localctx, 0, seaParser.RULE_program);
	    try {
	        this.enterOuterAlt(localctx, 1);
	        this.state = 14;
	        this.stmt();
	        this.state = 15;
	        this.match(seaParser.Newline);
	    } catch (re) {
	    	if(re instanceof antlr4.error.RecognitionException) {
		        localctx.exception = re;
		        this._errHandler.reportError(this, re);
		        this._errHandler.recover(this, re);
		    } else {
		    	throw re;
		    }
	    } finally {
	        this.exitRule();
	    }
	    return localctx;
	}



	stmt() {
	    let localctx = new StmtContext(this, this._ctx, this.state);
	    this.enterRule(localctx, 2, seaParser.RULE_stmt);
	    try {
	        this.state = 19;
	        this._errHandler.sync(this);
	        switch(this._input.LA(1)) {
	        case seaParser.T__11:
	        case seaParser.Number:
	        case seaParser.Boolean:
	        case seaParser.StringLiteral:
	        case seaParser.VarName:
	        case seaParser.Metric:
	            this.enterOuterAlt(localctx, 1);
	            this.state = 17;
	            this.expr(0);
	            break;
	        case seaParser.T__14:
	            this.enterOuterAlt(localctx, 2);
	            this.state = 18;
	            this.if_stmt();
	            break;
	        default:
	            throw new antlr4.error.NoViableAltException(this);
	        }
	    } catch (re) {
	    	if(re instanceof antlr4.error.RecognitionException) {
		        localctx.exception = re;
		        this._errHandler.reportError(this, re);
		        this._errHandler.recover(this, re);
		    } else {
		    	throw re;
		    }
	    } finally {
	        this.exitRule();
	    }
	    return localctx;
	}


	expr(_p) {
		if(_p===undefined) {
		    _p = 0;
		}
	    const _parentctx = this._ctx;
	    const _parentState = this.state;
	    let localctx = new ExprContext(this, this._ctx, _parentState);
	    let _prevctx = localctx;
	    const _startState = 4;
	    this.enterRecursionRule(localctx, 4, seaParser.RULE_expr, _p);
	    var _la = 0; // Token type
	    try {
	        this.enterOuterAlt(localctx, 1);
	        this.state = 31;
	        this._errHandler.sync(this);
	        switch(this._input.LA(1)) {
	        case seaParser.VarName:
	            localctx = new FunctionCallExpressionContext(this, localctx);
	            this._ctx = localctx;
	            _prevctx = localctx;

	            this.state = 22;
	            this.function_call();
	            break;
	        case seaParser.Metric:
	            localctx = new MetricQueryExpressionContext(this, localctx);
	            this._ctx = localctx;
	            _prevctx = localctx;
	            this.state = 23;
	            this.metric_query();
	            break;
	        case seaParser.Number:
	            localctx = new NumberLiteralContext(this, localctx);
	            this._ctx = localctx;
	            _prevctx = localctx;
	            this.state = 24;
	            this.match(seaParser.Number);
	            break;
	        case seaParser.Boolean:
	            localctx = new BooleanLiteralContext(this, localctx);
	            this._ctx = localctx;
	            _prevctx = localctx;
	            this.state = 25;
	            this.match(seaParser.Boolean);
	            break;
	        case seaParser.StringLiteral:
	            localctx = new StringLiteralContext(this, localctx);
	            this._ctx = localctx;
	            _prevctx = localctx;
	            this.state = 26;
	            this.match(seaParser.StringLiteral);
	            break;
	        case seaParser.T__11:
	            localctx = new ParenthesizedExpressionContext(this, localctx);
	            this._ctx = localctx;
	            _prevctx = localctx;
	            this.state = 27;
	            this.match(seaParser.T__11);
	            this.state = 28;
	            this.stmt();
	            this.state = 29;
	            this.match(seaParser.T__12);
	            break;
	        default:
	            throw new antlr4.error.NoViableAltException(this);
	        }
	        this._ctx.stop = this._input.LT(-1);
	        this.state = 50;
	        this._errHandler.sync(this);
	        var _alt = this._interp.adaptivePredict(this._input,3,this._ctx)
	        while(_alt!=2 && _alt!=antlr4.atn.ATN.INVALID_ALT_NUMBER) {
	            if(_alt===1) {
	                if(this._parseListeners!==null) {
	                    this.triggerExitRuleEvent();
	                }
	                _prevctx = localctx;
	                this.state = 48;
	                this._errHandler.sync(this);
	                var la_ = this._interp.adaptivePredict(this._input,2,this._ctx);
	                switch(la_) {
	                case 1:
	                    localctx = new MutilplicativeExpressionContext(this, new ExprContext(this, _parentctx, _parentState));
	                    this.pushNewRecursionContext(localctx, _startState, seaParser.RULE_expr);
	                    this.state = 33;
	                    if (!( this.precpred(this._ctx, 10))) {
	                        throw new antlr4.error.FailedPredicateException(this, "this.precpred(this._ctx, 10)");
	                    }
	                    this.state = 34;
	                    localctx.op = this._input.LT(1);
	                    _la = this._input.LA(1);
	                    if(!(_la===seaParser.T__0 || _la===seaParser.T__1)) {
	                        localctx.op = this._errHandler.recoverInline(this);
	                    }
	                    else {
	                    	this._errHandler.reportMatch(this);
	                        this.consume();
	                    }
	                    this.state = 35;
	                    this.expr(11);
	                    break;

	                case 2:
	                    localctx = new AdditiveExpressionContext(this, new ExprContext(this, _parentctx, _parentState));
	                    this.pushNewRecursionContext(localctx, _startState, seaParser.RULE_expr);
	                    this.state = 36;
	                    if (!( this.precpred(this._ctx, 9))) {
	                        throw new antlr4.error.FailedPredicateException(this, "this.precpred(this._ctx, 9)");
	                    }
	                    this.state = 37;
	                    localctx.op = this._input.LT(1);
	                    _la = this._input.LA(1);
	                    if(!(_la===seaParser.T__2 || _la===seaParser.T__3)) {
	                        localctx.op = this._errHandler.recoverInline(this);
	                    }
	                    else {
	                    	this._errHandler.reportMatch(this);
	                        this.consume();
	                    }
	                    this.state = 38;
	                    this.expr(10);
	                    break;

	                case 3:
	                    localctx = new RelationalExpressionContext(this, new ExprContext(this, _parentctx, _parentState));
	                    this.pushNewRecursionContext(localctx, _startState, seaParser.RULE_expr);
	                    this.state = 39;
	                    if (!( this.precpred(this._ctx, 8))) {
	                        throw new antlr4.error.FailedPredicateException(this, "this.precpred(this._ctx, 8)");
	                    }
	                    this.state = 40;
	                    localctx.op = this._input.LT(1);
	                    _la = this._input.LA(1);
	                    if(!((((_la) & ~0x1f) == 0 && ((1 << _la) & ((1 << seaParser.T__4) | (1 << seaParser.T__5) | (1 << seaParser.T__6) | (1 << seaParser.T__7) | (1 << seaParser.T__8))) !== 0))) {
	                        localctx.op = this._errHandler.recoverInline(this);
	                    }
	                    else {
	                    	this._errHandler.reportMatch(this);
	                        this.consume();
	                    }
	                    this.state = 41;
	                    this.expr(9);
	                    break;

	                case 4:
	                    localctx = new LogicalOrExpressionContext(this, new ExprContext(this, _parentctx, _parentState));
	                    this.pushNewRecursionContext(localctx, _startState, seaParser.RULE_expr);
	                    this.state = 42;
	                    if (!( this.precpred(this._ctx, 7))) {
	                        throw new antlr4.error.FailedPredicateException(this, "this.precpred(this._ctx, 7)");
	                    }
	                    this.state = 43;
	                    localctx.op = this.match(seaParser.T__9);
	                    this.state = 44;
	                    this.expr(8);
	                    break;

	                case 5:
	                    localctx = new LogicalAndExpressionContext(this, new ExprContext(this, _parentctx, _parentState));
	                    this.pushNewRecursionContext(localctx, _startState, seaParser.RULE_expr);
	                    this.state = 45;
	                    if (!( this.precpred(this._ctx, 6))) {
	                        throw new antlr4.error.FailedPredicateException(this, "this.precpred(this._ctx, 6)");
	                    }
	                    this.state = 46;
	                    localctx.op = this.match(seaParser.T__10);
	                    this.state = 47;
	                    this.expr(7);
	                    break;

	                } 
	            }
	            this.state = 52;
	            this._errHandler.sync(this);
	            _alt = this._interp.adaptivePredict(this._input,3,this._ctx);
	        }

	    } catch( error) {
	        if(error instanceof antlr4.error.RecognitionException) {
		        localctx.exception = error;
		        this._errHandler.reportError(this, error);
		        this._errHandler.recover(this, error);
		    } else {
		    	throw error;
		    }
	    } finally {
	        this.unrollRecursionContexts(_parentctx)
	    }
	    return localctx;
	}



	expr_list() {
	    let localctx = new Expr_listContext(this, this._ctx, this.state);
	    this.enterRule(localctx, 6, seaParser.RULE_expr_list);
	    var _la = 0; // Token type
	    try {
	        this.enterOuterAlt(localctx, 1);
	        this.state = 53;
	        this.expr(0);
	        this.state = 58;
	        this._errHandler.sync(this);
	        _la = this._input.LA(1);
	        while(_la===seaParser.T__13) {
	            this.state = 54;
	            this.match(seaParser.T__13);
	            this.state = 55;
	            this.expr(0);
	            this.state = 60;
	            this._errHandler.sync(this);
	            _la = this._input.LA(1);
	        }
	    } catch (re) {
	    	if(re instanceof antlr4.error.RecognitionException) {
		        localctx.exception = re;
		        this._errHandler.reportError(this, re);
		        this._errHandler.recover(this, re);
		    } else {
		    	throw re;
		    }
	    } finally {
	        this.exitRule();
	    }
	    return localctx;
	}



	if_stmt() {
	    let localctx = new If_stmtContext(this, this._ctx, this.state);
	    this.enterRule(localctx, 8, seaParser.RULE_if_stmt);
	    try {
	        localctx = new IfStatementContext(this, localctx);
	        this.enterOuterAlt(localctx, 1);
	        this.state = 61;
	        this.match(seaParser.T__14);
	        this.state = 62;
	        this.expr(0);
	        this.state = 63;
	        this.match(seaParser.T__15);
	        this.state = 64;
	        this.stmt();
	        this.state = 65;
	        this.match(seaParser.T__16);
	        this.state = 66;
	        this.stmt();
	    } catch (re) {
	    	if(re instanceof antlr4.error.RecognitionException) {
		        localctx.exception = re;
		        this._errHandler.reportError(this, re);
		        this._errHandler.recover(this, re);
		    } else {
		    	throw re;
		    }
	    } finally {
	        this.exitRule();
	    }
	    return localctx;
	}



	function_call() {
	    let localctx = new Function_callContext(this, this._ctx, this.state);
	    this.enterRule(localctx, 10, seaParser.RULE_function_call);
	    var _la = 0; // Token type
	    try {
	        this.enterOuterAlt(localctx, 1);
	        this.state = 68;
	        this.match(seaParser.VarName);
	        this.state = 69;
	        this.match(seaParser.T__11);
	        this.state = 73;
	        this._errHandler.sync(this);
	        _la = this._input.LA(1);
	        while((((_la) & ~0x1f) == 0 && ((1 << _la) & ((1 << seaParser.T__11) | (1 << seaParser.Number) | (1 << seaParser.Boolean) | (1 << seaParser.StringLiteral) | (1 << seaParser.VarName) | (1 << seaParser.Metric))) !== 0)) {
	            this.state = 70;
	            this.expr_list();
	            this.state = 75;
	            this._errHandler.sync(this);
	            _la = this._input.LA(1);
	        }
	        this.state = 76;
	        this.match(seaParser.T__12);
	    } catch (re) {
	    	if(re instanceof antlr4.error.RecognitionException) {
		        localctx.exception = re;
		        this._errHandler.reportError(this, re);
		        this._errHandler.recover(this, re);
		    } else {
		    	throw re;
		    }
	    } finally {
	        this.exitRule();
	    }
	    return localctx;
	}



	metric_query() {
	    let localctx = new Metric_queryContext(this, this._ctx, this.state);
	    this.enterRule(localctx, 12, seaParser.RULE_metric_query);
	    try {
	        this.enterOuterAlt(localctx, 1);
	        this.state = 78;
	        this.match(seaParser.Metric);
	        this.state = 82;
	        this._errHandler.sync(this);
	        var la_ = this._interp.adaptivePredict(this._input,6,this._ctx);
	        if(la_===1) {
	            this.state = 79;
	            this.match(seaParser.T__11);
	            this.state = 80;
	            this.match(seaParser.Cohort);
	            this.state = 81;
	            this.match(seaParser.T__12);

	        }
	    } catch (re) {
	    	if(re instanceof antlr4.error.RecognitionException) {
		        localctx.exception = re;
		        this._errHandler.reportError(this, re);
		        this._errHandler.recover(this, re);
		    } else {
		    	throw re;
		    }
	    } finally {
	        this.exitRule();
	    }
	    return localctx;
	}


}

seaParser.EOF = antlr4.Token.EOF;
seaParser.T__0 = 1;
seaParser.T__1 = 2;
seaParser.T__2 = 3;
seaParser.T__3 = 4;
seaParser.T__4 = 5;
seaParser.T__5 = 6;
seaParser.T__6 = 7;
seaParser.T__7 = 8;
seaParser.T__8 = 9;
seaParser.T__9 = 10;
seaParser.T__10 = 11;
seaParser.T__11 = 12;
seaParser.T__12 = 13;
seaParser.T__13 = 14;
seaParser.T__14 = 15;
seaParser.T__15 = 16;
seaParser.T__16 = 17;
seaParser.Newline = 18;
seaParser.Number = 19;
seaParser.Boolean = 20;
seaParser.StringLiteral = 21;
seaParser.UnterminatedStringLiteral = 22;
seaParser.VarName = 23;
seaParser.Metric = 24;
seaParser.Cohort = 25;
seaParser.WS = 26;

seaParser.RULE_program = 0;
seaParser.RULE_stmt = 1;
seaParser.RULE_expr = 2;
seaParser.RULE_expr_list = 3;
seaParser.RULE_if_stmt = 4;
seaParser.RULE_function_call = 5;
seaParser.RULE_metric_query = 6;

class ProgramContext extends antlr4.ParserRuleContext {

    constructor(parser, parent, invokingState) {
        if(parent===undefined) {
            parent = null;
        }
        if(invokingState===undefined || invokingState===null) {
            invokingState = -1;
        }
        super(parent, invokingState);
        this.parser = parser;
        this.ruleIndex = seaParser.RULE_program;
    }

	stmt() {
	    return this.getTypedRuleContext(StmtContext,0);
	};

	Newline() {
	    return this.getToken(seaParser.Newline, 0);
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterProgram(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitProgram(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitProgram(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}



class StmtContext extends antlr4.ParserRuleContext {

    constructor(parser, parent, invokingState) {
        if(parent===undefined) {
            parent = null;
        }
        if(invokingState===undefined || invokingState===null) {
            invokingState = -1;
        }
        super(parent, invokingState);
        this.parser = parser;
        this.ruleIndex = seaParser.RULE_stmt;
    }

	expr() {
	    return this.getTypedRuleContext(ExprContext,0);
	};

	if_stmt() {
	    return this.getTypedRuleContext(If_stmtContext,0);
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterStmt(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitStmt(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitStmt(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}



class ExprContext extends antlr4.ParserRuleContext {

    constructor(parser, parent, invokingState) {
        if(parent===undefined) {
            parent = null;
        }
        if(invokingState===undefined || invokingState===null) {
            invokingState = -1;
        }
        super(parent, invokingState);
        this.parser = parser;
        this.ruleIndex = seaParser.RULE_expr;
    }


	 
		copyFrom(ctx) {
			super.copyFrom(ctx);
		}

}


class ParenthesizedExpressionContext extends ExprContext {

    constructor(parser, ctx) {
        super(parser);
        super.copyFrom(ctx);
    }

	stmt() {
	    return this.getTypedRuleContext(StmtContext,0);
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterParenthesizedExpression(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitParenthesizedExpression(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitParenthesizedExpression(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}

seaParser.ParenthesizedExpressionContext = ParenthesizedExpressionContext;

class AdditiveExpressionContext extends ExprContext {

    constructor(parser, ctx) {
        super(parser);
        this.op = null; // Token;
        super.copyFrom(ctx);
    }

	expr = function(i) {
	    if(i===undefined) {
	        i = null;
	    }
	    if(i===null) {
	        return this.getTypedRuleContexts(ExprContext);
	    } else {
	        return this.getTypedRuleContext(ExprContext,i);
	    }
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterAdditiveExpression(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitAdditiveExpression(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitAdditiveExpression(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}

seaParser.AdditiveExpressionContext = AdditiveExpressionContext;

class RelationalExpressionContext extends ExprContext {

    constructor(parser, ctx) {
        super(parser);
        this.op = null; // Token;
        super.copyFrom(ctx);
    }

	expr = function(i) {
	    if(i===undefined) {
	        i = null;
	    }
	    if(i===null) {
	        return this.getTypedRuleContexts(ExprContext);
	    } else {
	        return this.getTypedRuleContext(ExprContext,i);
	    }
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterRelationalExpression(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitRelationalExpression(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitRelationalExpression(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}

seaParser.RelationalExpressionContext = RelationalExpressionContext;

class LogicalAndExpressionContext extends ExprContext {

    constructor(parser, ctx) {
        super(parser);
        this.op = null; // Token;
        super.copyFrom(ctx);
    }

	expr = function(i) {
	    if(i===undefined) {
	        i = null;
	    }
	    if(i===null) {
	        return this.getTypedRuleContexts(ExprContext);
	    } else {
	        return this.getTypedRuleContext(ExprContext,i);
	    }
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterLogicalAndExpression(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitLogicalAndExpression(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitLogicalAndExpression(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}

seaParser.LogicalAndExpressionContext = LogicalAndExpressionContext;

class StringLiteralContext extends ExprContext {

    constructor(parser, ctx) {
        super(parser);
        super.copyFrom(ctx);
    }

	StringLiteral() {
	    return this.getToken(seaParser.StringLiteral, 0);
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterStringLiteral(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitStringLiteral(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitStringLiteral(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}

seaParser.StringLiteralContext = StringLiteralContext;

class BooleanLiteralContext extends ExprContext {

    constructor(parser, ctx) {
        super(parser);
        super.copyFrom(ctx);
    }

	Boolean() {
	    return this.getToken(seaParser.Boolean, 0);
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterBooleanLiteral(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitBooleanLiteral(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitBooleanLiteral(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}

seaParser.BooleanLiteralContext = BooleanLiteralContext;

class LogicalOrExpressionContext extends ExprContext {

    constructor(parser, ctx) {
        super(parser);
        this.op = null; // Token;
        super.copyFrom(ctx);
    }

	expr = function(i) {
	    if(i===undefined) {
	        i = null;
	    }
	    if(i===null) {
	        return this.getTypedRuleContexts(ExprContext);
	    } else {
	        return this.getTypedRuleContext(ExprContext,i);
	    }
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterLogicalOrExpression(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitLogicalOrExpression(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitLogicalOrExpression(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}

seaParser.LogicalOrExpressionContext = LogicalOrExpressionContext;

class MetricQueryExpressionContext extends ExprContext {

    constructor(parser, ctx) {
        super(parser);
        super.copyFrom(ctx);
    }

	metric_query() {
	    return this.getTypedRuleContext(Metric_queryContext,0);
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterMetricQueryExpression(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitMetricQueryExpression(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitMetricQueryExpression(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}

seaParser.MetricQueryExpressionContext = MetricQueryExpressionContext;

class MutilplicativeExpressionContext extends ExprContext {

    constructor(parser, ctx) {
        super(parser);
        this.op = null; // Token;
        super.copyFrom(ctx);
    }

	expr = function(i) {
	    if(i===undefined) {
	        i = null;
	    }
	    if(i===null) {
	        return this.getTypedRuleContexts(ExprContext);
	    } else {
	        return this.getTypedRuleContext(ExprContext,i);
	    }
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterMutilplicativeExpression(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitMutilplicativeExpression(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitMutilplicativeExpression(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}

seaParser.MutilplicativeExpressionContext = MutilplicativeExpressionContext;

class FunctionCallExpressionContext extends ExprContext {

    constructor(parser, ctx) {
        super(parser);
        super.copyFrom(ctx);
    }

	function_call() {
	    return this.getTypedRuleContext(Function_callContext,0);
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterFunctionCallExpression(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitFunctionCallExpression(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitFunctionCallExpression(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}

seaParser.FunctionCallExpressionContext = FunctionCallExpressionContext;

class NumberLiteralContext extends ExprContext {

    constructor(parser, ctx) {
        super(parser);
        super.copyFrom(ctx);
    }

	Number() {
	    return this.getToken(seaParser.Number, 0);
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterNumberLiteral(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitNumberLiteral(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitNumberLiteral(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}

seaParser.NumberLiteralContext = NumberLiteralContext;

class Expr_listContext extends antlr4.ParserRuleContext {

    constructor(parser, parent, invokingState) {
        if(parent===undefined) {
            parent = null;
        }
        if(invokingState===undefined || invokingState===null) {
            invokingState = -1;
        }
        super(parent, invokingState);
        this.parser = parser;
        this.ruleIndex = seaParser.RULE_expr_list;
    }

	expr = function(i) {
	    if(i===undefined) {
	        i = null;
	    }
	    if(i===null) {
	        return this.getTypedRuleContexts(ExprContext);
	    } else {
	        return this.getTypedRuleContext(ExprContext,i);
	    }
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterExpr_list(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitExpr_list(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitExpr_list(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}



class If_stmtContext extends antlr4.ParserRuleContext {

    constructor(parser, parent, invokingState) {
        if(parent===undefined) {
            parent = null;
        }
        if(invokingState===undefined || invokingState===null) {
            invokingState = -1;
        }
        super(parent, invokingState);
        this.parser = parser;
        this.ruleIndex = seaParser.RULE_if_stmt;
    }


	 
		copyFrom(ctx) {
			super.copyFrom(ctx);
		}

}


class IfStatementContext extends If_stmtContext {

    constructor(parser, ctx) {
        super(parser);
        super.copyFrom(ctx);
    }

	expr() {
	    return this.getTypedRuleContext(ExprContext,0);
	};

	stmt = function(i) {
	    if(i===undefined) {
	        i = null;
	    }
	    if(i===null) {
	        return this.getTypedRuleContexts(StmtContext);
	    } else {
	        return this.getTypedRuleContext(StmtContext,i);
	    }
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterIfStatement(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitIfStatement(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitIfStatement(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}

seaParser.IfStatementContext = IfStatementContext;

class Function_callContext extends antlr4.ParserRuleContext {

    constructor(parser, parent, invokingState) {
        if(parent===undefined) {
            parent = null;
        }
        if(invokingState===undefined || invokingState===null) {
            invokingState = -1;
        }
        super(parent, invokingState);
        this.parser = parser;
        this.ruleIndex = seaParser.RULE_function_call;
    }

	VarName() {
	    return this.getToken(seaParser.VarName, 0);
	};

	expr_list = function(i) {
	    if(i===undefined) {
	        i = null;
	    }
	    if(i===null) {
	        return this.getTypedRuleContexts(Expr_listContext);
	    } else {
	        return this.getTypedRuleContext(Expr_listContext,i);
	    }
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterFunction_call(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitFunction_call(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitFunction_call(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}



class Metric_queryContext extends antlr4.ParserRuleContext {

    constructor(parser, parent, invokingState) {
        if(parent===undefined) {
            parent = null;
        }
        if(invokingState===undefined || invokingState===null) {
            invokingState = -1;
        }
        super(parent, invokingState);
        this.parser = parser;
        this.ruleIndex = seaParser.RULE_metric_query;
    }

	Metric() {
	    return this.getToken(seaParser.Metric, 0);
	};

	Cohort() {
	    return this.getToken(seaParser.Cohort, 0);
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterMetric_query(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitMetric_query(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitMetric_query(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}




seaParser.ProgramContext = ProgramContext; 
seaParser.StmtContext = StmtContext; 
seaParser.ExprContext = ExprContext; 
seaParser.Expr_listContext = Expr_listContext; 
seaParser.If_stmtContext = If_stmtContext; 
seaParser.Function_callContext = Function_callContext; 
seaParser.Metric_queryContext = Metric_queryContext; 
