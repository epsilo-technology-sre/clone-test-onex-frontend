grammar sea;

program: stmt Newline;

stmt: expr | if_stmt; // a statement will always return a value

expr:
    function_call                                    # FunctionCallExpression
    | expr op = ('*' | '/') expr                     # MutilplicativeExpression
    | expr op = ('+' | '-') expr                     # AdditiveExpression
    | expr op = ('<' | '>' | '<=' | '>=' | '=') expr # RelationalExpression
    | expr op = 'OR' expr                            # LogicalOrExpression
    | expr op = 'AND' expr                           # LogicalAndExpression
    | metric_query                                   # MetricQueryExpression
    | Number                                         # NumberLiteral
    | Boolean                                        # BooleanLiteral
    | StringLiteral                                  # StringLiteral
    | '(' stmt ')'                                   # ParenthesizedExpression;

expr_list: expr (',' expr)*;

if_stmt: 'IF' expr 'THEN' stmt 'ELSE' stmt # IfStatement;

function_call: VarName '(' expr_list? ')';
metric_query: Metric ('(' Cohort ')')?;

Newline: [\r\n]+;
Number: [0-9]+ | [0-9]+ '.' [0-9]+;
Boolean: 'true' | 'false';

StringLiteral: UnterminatedStringLiteral '"';
UnterminatedStringLiteral: '"' (~["\\\r\n] | '\\' (. | EOF))*;

fragment IDENTIFIER: [a-zA-Z_]+ [0-9a-zA-Z_]*;

VarName: IDENTIFIER;

Metric: '@' IDENTIFIER;
Cohort: [0-9]+ ('h' | 'd' | 'm' | 'q' | 'H' | 'D' | 'M' | 'Q');

WS: [ \t]+ -> skip;
