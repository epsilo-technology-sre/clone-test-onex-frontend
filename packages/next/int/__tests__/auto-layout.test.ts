import { layouts } from './resources';
import { sortBy, get } from 'lodash';
import { reArrangeLayout } from '../auto-layout';

describe('Auto re-arrange layout xcols -> ycols', () => {
  it('24 -> 18', () => {
    expect(layouts).toHaveLength(59);
    const viewport = 18;

    let nextLayout = reArrangeLayout({
      layout: layouts,
      base: 24,
      viewport: 18,
    });

    let sortedLayouts = sortBy(layouts, [
      (i) => i.layout.y,
      (i) => i.layout.x,
    ]);

    let refRow = 0;
    let prevRefItem = null;

    sortedLayouts.forEach((item, index) => {
      let nextItem = nextLayout[index];
      expect(nextItem.id).toEqual(item.id);

      if (refRow === 0) {
        refRow = nextItem.layout.y;
        prevRefItem = nextItem;
      }

      if (nextItem.layout.y > refRow) {
        let x = prevRefItem.layout.x;
        let w = prevRefItem.layout.w;
        if (x + w !== viewport) {
          expect({ x, w, otherx: x + w }).toEqual(
            JSON.stringify(prevRefItem),
          );
        }
      }

      refRow = nextItem.layout.y;
      prevRefItem = nextItem;
    });
  });

  it('24 -> 6', () => {
    expect(layouts).toHaveLength(59);
    const viewport = 18;

    let nextLayout = reArrangeLayout({
      layout: layouts,
      base: 24,
      viewport,
    });

    let sortedLayouts = sortBy(layouts, [
      (i) => i.layout.y,
      (i) => i.layout.x,
    ]);

    let refRow = 0;
    let prevRefItem = null;

    sortedLayouts.forEach((item, index) => {
      let nextItem = nextLayout[index];
      expect(nextItem.id).toEqual(item.id);

      if (refRow === 0) {
        refRow = nextItem.layout.y;
        prevRefItem = nextItem;
      }

      if (nextItem.layout.y > refRow) {
        let x = prevRefItem.layout.x;
        let w = prevRefItem.layout.w;
        if (x + w !== viewport) {
          expect({ x, w, otherx: x + w }).toEqual(
            JSON.stringify(prevRefItem),
          );
        }
      }

      refRow = nextItem.layout.y;
      prevRefItem = nextItem;
    });
  });
  it('24 -> 2', () => {
    expect(layouts).toHaveLength(59);
    const viewport = 2;

    let nextLayout = reArrangeLayout({
      layout: layouts,
      base: 24,
      viewport,
    });

    let sortedLayouts = sortBy(layouts, [
      (i) => i.layout.y,
      (i) => i.layout.x,
    ]);

    let refRow = 0;
    let prevRefItem = null;

    sortedLayouts.forEach((item, index) => {
      let nextItem = nextLayout[index];
      expect(nextItem.id).toEqual(item.id);

      if (refRow === 0) {
        refRow = nextItem.layout.y;
        prevRefItem = nextItem;
      }

      if (nextItem.layout.y > refRow) {
        let x = prevRefItem.layout.x;
        let w = prevRefItem.layout.w;
        if (x + w !== viewport) {
          expect({ x, w, otherx: x + w }).toEqual(
            JSON.stringify(prevRefItem),
          );
        }
      }

      refRow = nextItem.layout.y;
      prevRefItem = nextItem;
    });
  });
});
