
import antlr4 from 'antlr4';

// This class defines a complete generic visitor for a parse tree produced by seaParser.

export default class SeaVisitor extends antlr4.tree.ParseTreeVisitor {

  constructor(){
    super();
  }

	// Visit a parse tree produced by seaParser#FormulaDefinition.
	visitFormulaDefinition(ctx) {
	  return this.visitChildren(ctx);
	}

}