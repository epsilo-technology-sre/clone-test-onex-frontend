import antlr4 from 'antlr4';
import SeaGrammarLexer from '../seaLexer';
import SeaParser from '../seaParser';
import { CaseChangingStream } from '../CaseChangingStream';
import SeaVisitor from '../seaVisitor';
// import SeaListener from '../seaListener';

function parseTree(input: string) {
  const chars = new antlr4.InputStream(input);
  const upperChars = new CaseChangingStream(chars, true);
  const lexer = new SeaGrammarLexer(upperChars);
  const tokens = new antlr4.CommonTokenStream(lexer);
  const parser = new SeaParser(tokens);
  parser.buildParseTrees = true;
  const tree = parser.formula();
  return tree;
}

function calculate(tree) {
  const visitor = new SeaVisitor();
  return visitor.visit(tree);
}

describe('SEA valid syntax', () => {
  let testCases = [
    '79',
    '39.39',
    '"a string"',
    '"một chuỗi kí tự trong tiếng việt"',
    '"ไทย(thai)"',
    '@gmv',
    '@gmv(3d)',
    '@gmv / @cost',
    'a_function_call_0_argument()',
    'a_function_call_1_argument(1234)',
    'a_function_call_n_argument(1,2,3,4,5)',
    'IF @GMV(7d) < @GMV(30d) / 5 THEN 1 ELSE 0',
    'IF @gmv(7d) < @gmv(30d) / 5 THEN 1 ELSE 0',
    'if @gmv(7d) < @gmv(30d) / 5 then 1 else 0',
  ];

  let consoleError;
  beforeAll(() => {
    consoleError = jest.spyOn(console, 'error');
  });
  afterEach(() => {
    consoleError.mockReset();
  });
  afterAll(() => {
    consoleError.mockRestore();
  });

  for (let testcase of testCases) {
    it(testcase, () => {
      parseTree(testcase + '\n');
      expect(consoleError).not.toHaveBeenCalled();
    });
  }
});

describe('SEA invalid syntax', () => {
  let testCases = ['gmv', 'gmv / cost', '@ gvm'];

  let consoleError;
  beforeAll(() => {
    consoleError = jest.spyOn(console, 'error');
  });
  afterEach(() => {
    consoleError.mockReset();
  });
  afterAll(() => {
    consoleError.mockRestore();
  });

  for (let testcase of testCases) {
    it(testcase, () => {
      parseTree(testcase + '\n');
      expect(consoleError).toHaveBeenCalled();
    });
  }
});

describe('SEA formula evaluate', () => {
  test('79', () => {
    let result = calculate(parseTree('79' + '\n'));

    expect(result).toEqual(79);
  });
});
