// Generated from sea.g4 by ANTLR 4.9.2
// jshint ignore: start
import antlr4 from 'antlr4';
import seaListener from './seaListener.js';
import seaVisitor from './seaVisitor.js';


const serializedATN = ["\u0003\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786",
    "\u5964\u0003\u001bN\u0004\u0002\t\u0002\u0004\u0003\t\u0003\u0004\u0004",
    "\t\u0004\u0004\u0005\t\u0005\u0004\u0006\t\u0006\u0004\u0007\t\u0007",
    "\u0003\u0002\u0003\u0002\u0003\u0002\u0003\u0003\u0003\u0003\u0005\u0003",
    "\u0014\n\u0003\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003",
    "\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0005\u0004\u001f",
    "\n\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004",
    "\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004",
    "\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0007\u00040\n\u0004",
    "\f\u0004\u000e\u00043\u000b\u0004\u0003\u0005\u0003\u0005\u0003\u0005",
    "\u0007\u00058\n\u0005\f\u0005\u000e\u0005;\u000b\u0005\u0003\u0006\u0003",
    "\u0006\u0003\u0006\u0003\u0006\u0003\u0006\u0003\u0006\u0003\u0006\u0003",
    "\u0007\u0003\u0007\u0003\u0007\u0003\u0007\u0003\u0007\u0003\u0007\u0003",
    "\u0007\u0003\u0007\u0005\u0007L\n\u0007\u0003\u0007\u0002\u0003\u0006",
    "\b\u0002\u0004\u0006\b\n\f\u0002\u0005\u0003\u0002\u0003\u0004\u0003",
    "\u0002\u0005\u0006\u0003\u0002\u0007\u000b\u0002S\u0002\u000e\u0003",
    "\u0002\u0002\u0002\u0004\u0013\u0003\u0002\u0002\u0002\u0006\u001e\u0003",
    "\u0002\u0002\u0002\b4\u0003\u0002\u0002\u0002\n<\u0003\u0002\u0002\u0002",
    "\fK\u0003\u0002\u0002\u0002\u000e\u000f\u0005\u0004\u0003\u0002\u000f",
    "\u0010\u0007\u0014\u0002\u0002\u0010\u0003\u0003\u0002\u0002\u0002\u0011",
    "\u0014\u0005\u0006\u0004\u0002\u0012\u0014\u0005\n\u0006\u0002\u0013",
    "\u0011\u0003\u0002\u0002\u0002\u0013\u0012\u0003\u0002\u0002\u0002\u0014",
    "\u0005\u0003\u0002\u0002\u0002\u0015\u0016\b\u0004\u0001\u0002\u0016",
    "\u001f\u0005\f\u0007\u0002\u0017\u001f\u0007\u0019\u0002\u0002\u0018",
    "\u001f\u0007\u0015\u0002\u0002\u0019\u001f\u0007\u0016\u0002\u0002\u001a",
    "\u001b\u0007\u000e\u0002\u0002\u001b\u001c\u0005\u0006\u0004\u0002\u001c",
    "\u001d\u0007\u000f\u0002\u0002\u001d\u001f\u0003\u0002\u0002\u0002\u001e",
    "\u0015\u0003\u0002\u0002\u0002\u001e\u0017\u0003\u0002\u0002\u0002\u001e",
    "\u0018\u0003\u0002\u0002\u0002\u001e\u0019\u0003\u0002\u0002\u0002\u001e",
    "\u001a\u0003\u0002\u0002\u0002\u001f1\u0003\u0002\u0002\u0002 !\f\u000b",
    "\u0002\u0002!\"\t\u0002\u0002\u0002\"0\u0005\u0006\u0004\f#$\f\n\u0002",
    "\u0002$%\t\u0003\u0002\u0002%0\u0005\u0006\u0004\u000b&\'\f\t\u0002",
    "\u0002\'(\t\u0004\u0002\u0002(0\u0005\u0006\u0004\n)*\f\b\u0002\u0002",
    "*+\u0007\f\u0002\u0002+0\u0005\u0006\u0004\t,-\f\u0007\u0002\u0002-",
    ".\u0007\r\u0002\u0002.0\u0005\u0006\u0004\b/ \u0003\u0002\u0002\u0002",
    "/#\u0003\u0002\u0002\u0002/&\u0003\u0002\u0002\u0002/)\u0003\u0002\u0002",
    "\u0002/,\u0003\u0002\u0002\u000203\u0003\u0002\u0002\u00021/\u0003\u0002",
    "\u0002\u000212\u0003\u0002\u0002\u00022\u0007\u0003\u0002\u0002\u0002",
    "31\u0003\u0002\u0002\u000249\u0005\u0006\u0004\u000256\u0007\u0010\u0002",
    "\u000268\u0005\u0006\u0004\u000275\u0003\u0002\u0002\u00028;\u0003\u0002",
    "\u0002\u000297\u0003\u0002\u0002\u00029:\u0003\u0002\u0002\u0002:\t",
    "\u0003\u0002\u0002\u0002;9\u0003\u0002\u0002\u0002<=\u0007\u0011\u0002",
    "\u0002=>\u0005\u0006\u0004\u0002>?\u0007\u0012\u0002\u0002?@\u0005\u0006",
    "\u0004\u0002@A\u0007\u0013\u0002\u0002AB\u0005\u0006\u0004\u0002B\u000b",
    "\u0003\u0002\u0002\u0002CD\u0007\u0018\u0002\u0002DE\u0007\u000e\u0002",
    "\u0002EL\u0007\u000f\u0002\u0002FG\u0007\u0018\u0002\u0002GH\u0007\u000e",
    "\u0002\u0002HI\u0005\b\u0005\u0002IJ\u0007\u000f\u0002\u0002JL\u0003",
    "\u0002\u0002\u0002KC\u0003\u0002\u0002\u0002KF\u0003\u0002\u0002\u0002",
    "L\r\u0003\u0002\u0002\u0002\b\u0013\u001e/19K"].join("");


const atn = new antlr4.atn.ATNDeserializer().deserialize(serializedATN);

const decisionsToDFA = atn.decisionToState.map( (ds, index) => new antlr4.dfa.DFA(ds, index) );

const sharedContextCache = new antlr4.PredictionContextCache();

export default class seaParser extends antlr4.Parser {

    static grammarFileName = "sea.g4";
    static literalNames = [ null, "'*'", "'/'", "'+'", "'-'", "'<'", "'>'", 
                            "'<='", "'>='", "'='", "'OR'", "'AND'", "'('", 
                            "')'", "','", "'IF'", "'THEN'", "'ELSE'" ];
    static symbolicNames = [ null, null, null, null, null, null, null, null, 
                             null, null, null, null, null, null, null, null, 
                             null, null, "NEWLINE", "Number", "StringLiteral", 
                             "UnterminatedStringLiteral", "VarName", "Metric", 
                             "Cohort", "WS" ];
    static ruleNames = [ "formula", "stmt", "expr", "expr_list", "if_stmt", 
                         "function_call" ];

    constructor(input) {
        super(input);
        this._interp = new antlr4.atn.ParserATNSimulator(this, atn, decisionsToDFA, sharedContextCache);
        this.ruleNames = seaParser.ruleNames;
        this.literalNames = seaParser.literalNames;
        this.symbolicNames = seaParser.symbolicNames;
    }

    get atn() {
        return atn;
    }

    sempred(localctx, ruleIndex, predIndex) {
    	switch(ruleIndex) {
    	case 2:
    	    		return this.expr_sempred(localctx, predIndex);
        default:
            throw "No predicate with index:" + ruleIndex;
       }
    }

    expr_sempred(localctx, predIndex) {
    	switch(predIndex) {
    		case 0:
    			return this.precpred(this._ctx, 9);
    		case 1:
    			return this.precpred(this._ctx, 8);
    		case 2:
    			return this.precpred(this._ctx, 7);
    		case 3:
    			return this.precpred(this._ctx, 6);
    		case 4:
    			return this.precpred(this._ctx, 5);
    		default:
    			throw "No predicate with index:" + predIndex;
    	}
    };




	formula() {
	    let localctx = new FormulaContext(this, this._ctx, this.state);
	    this.enterRule(localctx, 0, seaParser.RULE_formula);
	    try {
	        localctx = new FormulaDefinitionContext(this, localctx);
	        this.enterOuterAlt(localctx, 1);
	        this.state = 12;
	        this.stmt();
	        this.state = 13;
	        this.match(seaParser.NEWLINE);
	    } catch (re) {
	    	if(re instanceof antlr4.error.RecognitionException) {
		        localctx.exception = re;
		        this._errHandler.reportError(this, re);
		        this._errHandler.recover(this, re);
		    } else {
		    	throw re;
		    }
	    } finally {
	        this.exitRule();
	    }
	    return localctx;
	}



	stmt() {
	    let localctx = new StmtContext(this, this._ctx, this.state);
	    this.enterRule(localctx, 2, seaParser.RULE_stmt);
	    try {
	        this.enterOuterAlt(localctx, 1);
	        this.state = 17;
	        this._errHandler.sync(this);
	        switch(this._input.LA(1)) {
	        case seaParser.T__11:
	        case seaParser.Number:
	        case seaParser.StringLiteral:
	        case seaParser.VarName:
	        case seaParser.Metric:
	            this.state = 15;
	            this.expr(0);
	            break;
	        case seaParser.T__14:
	            this.state = 16;
	            this.if_stmt();
	            break;
	        default:
	            throw new antlr4.error.NoViableAltException(this);
	        }
	    } catch (re) {
	    	if(re instanceof antlr4.error.RecognitionException) {
		        localctx.exception = re;
		        this._errHandler.reportError(this, re);
		        this._errHandler.recover(this, re);
		    } else {
		    	throw re;
		    }
	    } finally {
	        this.exitRule();
	    }
	    return localctx;
	}


	expr(_p) {
		if(_p===undefined) {
		    _p = 0;
		}
	    const _parentctx = this._ctx;
	    const _parentState = this.state;
	    let localctx = new ExprContext(this, this._ctx, _parentState);
	    let _prevctx = localctx;
	    const _startState = 4;
	    this.enterRecursionRule(localctx, 4, seaParser.RULE_expr, _p);
	    var _la = 0; // Token type
	    try {
	        this.enterOuterAlt(localctx, 1);
	        this.state = 28;
	        this._errHandler.sync(this);
	        switch(this._input.LA(1)) {
	        case seaParser.VarName:
	            localctx = new FunctionCallExpressionContext(this, localctx);
	            this._ctx = localctx;
	            _prevctx = localctx;

	            this.state = 20;
	            this.function_call();
	            break;
	        case seaParser.Metric:
	            localctx = new MetricExpressionContext(this, localctx);
	            this._ctx = localctx;
	            _prevctx = localctx;
	            this.state = 21;
	            this.match(seaParser.Metric);
	            break;
	        case seaParser.Number:
	            localctx = new NumberLiteralExpressionContext(this, localctx);
	            this._ctx = localctx;
	            _prevctx = localctx;
	            this.state = 22;
	            this.match(seaParser.Number);
	            break;
	        case seaParser.StringLiteral:
	            localctx = new StringLiteralContext(this, localctx);
	            this._ctx = localctx;
	            _prevctx = localctx;
	            this.state = 23;
	            this.match(seaParser.StringLiteral);
	            break;
	        case seaParser.T__11:
	            localctx = new ParenthesizedExpressionContext(this, localctx);
	            this._ctx = localctx;
	            _prevctx = localctx;
	            this.state = 24;
	            this.match(seaParser.T__11);
	            this.state = 25;
	            this.expr(0);
	            this.state = 26;
	            this.match(seaParser.T__12);
	            break;
	        default:
	            throw new antlr4.error.NoViableAltException(this);
	        }
	        this._ctx.stop = this._input.LT(-1);
	        this.state = 47;
	        this._errHandler.sync(this);
	        var _alt = this._interp.adaptivePredict(this._input,3,this._ctx)
	        while(_alt!=2 && _alt!=antlr4.atn.ATN.INVALID_ALT_NUMBER) {
	            if(_alt===1) {
	                if(this._parseListeners!==null) {
	                    this.triggerExitRuleEvent();
	                }
	                _prevctx = localctx;
	                this.state = 45;
	                this._errHandler.sync(this);
	                var la_ = this._interp.adaptivePredict(this._input,2,this._ctx);
	                switch(la_) {
	                case 1:
	                    localctx = new MutilplicativeExpressionContext(this, new ExprContext(this, _parentctx, _parentState));
	                    this.pushNewRecursionContext(localctx, _startState, seaParser.RULE_expr);
	                    this.state = 30;
	                    if (!( this.precpred(this._ctx, 9))) {
	                        throw new antlr4.error.FailedPredicateException(this, "this.precpred(this._ctx, 9)");
	                    }
	                    this.state = 31;
	                    _la = this._input.LA(1);
	                    if(!(_la===seaParser.T__0 || _la===seaParser.T__1)) {
	                    this._errHandler.recoverInline(this);
	                    }
	                    else {
	                    	this._errHandler.reportMatch(this);
	                        this.consume();
	                    }
	                    this.state = 32;
	                    this.expr(10);
	                    break;

	                case 2:
	                    localctx = new AdditiveExpressionContext(this, new ExprContext(this, _parentctx, _parentState));
	                    this.pushNewRecursionContext(localctx, _startState, seaParser.RULE_expr);
	                    this.state = 33;
	                    if (!( this.precpred(this._ctx, 8))) {
	                        throw new antlr4.error.FailedPredicateException(this, "this.precpred(this._ctx, 8)");
	                    }
	                    this.state = 34;
	                    _la = this._input.LA(1);
	                    if(!(_la===seaParser.T__2 || _la===seaParser.T__3)) {
	                    this._errHandler.recoverInline(this);
	                    }
	                    else {
	                    	this._errHandler.reportMatch(this);
	                        this.consume();
	                    }
	                    this.state = 35;
	                    this.expr(9);
	                    break;

	                case 3:
	                    localctx = new RelationalExpressionContext(this, new ExprContext(this, _parentctx, _parentState));
	                    this.pushNewRecursionContext(localctx, _startState, seaParser.RULE_expr);
	                    this.state = 36;
	                    if (!( this.precpred(this._ctx, 7))) {
	                        throw new antlr4.error.FailedPredicateException(this, "this.precpred(this._ctx, 7)");
	                    }
	                    this.state = 37;
	                    _la = this._input.LA(1);
	                    if(!((((_la) & ~0x1f) == 0 && ((1 << _la) & ((1 << seaParser.T__4) | (1 << seaParser.T__5) | (1 << seaParser.T__6) | (1 << seaParser.T__7) | (1 << seaParser.T__8))) !== 0))) {
	                    this._errHandler.recoverInline(this);
	                    }
	                    else {
	                    	this._errHandler.reportMatch(this);
	                        this.consume();
	                    }
	                    this.state = 38;
	                    this.expr(8);
	                    break;

	                case 4:
	                    localctx = new LogicalOrExpressionContext(this, new ExprContext(this, _parentctx, _parentState));
	                    this.pushNewRecursionContext(localctx, _startState, seaParser.RULE_expr);
	                    this.state = 39;
	                    if (!( this.precpred(this._ctx, 6))) {
	                        throw new antlr4.error.FailedPredicateException(this, "this.precpred(this._ctx, 6)");
	                    }
	                    this.state = 40;
	                    this.match(seaParser.T__9);
	                    this.state = 41;
	                    this.expr(7);
	                    break;

	                case 5:
	                    localctx = new LogicalAndExpressionContext(this, new ExprContext(this, _parentctx, _parentState));
	                    this.pushNewRecursionContext(localctx, _startState, seaParser.RULE_expr);
	                    this.state = 42;
	                    if (!( this.precpred(this._ctx, 5))) {
	                        throw new antlr4.error.FailedPredicateException(this, "this.precpred(this._ctx, 5)");
	                    }
	                    this.state = 43;
	                    this.match(seaParser.T__10);
	                    this.state = 44;
	                    this.expr(6);
	                    break;

	                } 
	            }
	            this.state = 49;
	            this._errHandler.sync(this);
	            _alt = this._interp.adaptivePredict(this._input,3,this._ctx);
	        }

	    } catch( error) {
	        if(error instanceof antlr4.error.RecognitionException) {
		        localctx.exception = error;
		        this._errHandler.reportError(this, error);
		        this._errHandler.recover(this, error);
		    } else {
		    	throw error;
		    }
	    } finally {
	        this.unrollRecursionContexts(_parentctx)
	    }
	    return localctx;
	}



	expr_list() {
	    let localctx = new Expr_listContext(this, this._ctx, this.state);
	    this.enterRule(localctx, 6, seaParser.RULE_expr_list);
	    var _la = 0; // Token type
	    try {
	        this.enterOuterAlt(localctx, 1);
	        this.state = 50;
	        this.expr(0);
	        this.state = 55;
	        this._errHandler.sync(this);
	        _la = this._input.LA(1);
	        while(_la===seaParser.T__13) {
	            this.state = 51;
	            this.match(seaParser.T__13);
	            this.state = 52;
	            this.expr(0);
	            this.state = 57;
	            this._errHandler.sync(this);
	            _la = this._input.LA(1);
	        }
	    } catch (re) {
	    	if(re instanceof antlr4.error.RecognitionException) {
		        localctx.exception = re;
		        this._errHandler.reportError(this, re);
		        this._errHandler.recover(this, re);
		    } else {
		    	throw re;
		    }
	    } finally {
	        this.exitRule();
	    }
	    return localctx;
	}



	if_stmt() {
	    let localctx = new If_stmtContext(this, this._ctx, this.state);
	    this.enterRule(localctx, 8, seaParser.RULE_if_stmt);
	    try {
	        localctx = new IfStatementContext(this, localctx);
	        this.enterOuterAlt(localctx, 1);
	        this.state = 58;
	        this.match(seaParser.T__14);
	        this.state = 59;
	        this.expr(0);
	        this.state = 60;
	        this.match(seaParser.T__15);
	        this.state = 61;
	        this.expr(0);
	        this.state = 62;
	        this.match(seaParser.T__16);
	        this.state = 63;
	        this.expr(0);
	    } catch (re) {
	    	if(re instanceof antlr4.error.RecognitionException) {
		        localctx.exception = re;
		        this._errHandler.reportError(this, re);
		        this._errHandler.recover(this, re);
		    } else {
		    	throw re;
		    }
	    } finally {
	        this.exitRule();
	    }
	    return localctx;
	}



	function_call() {
	    let localctx = new Function_callContext(this, this._ctx, this.state);
	    this.enterRule(localctx, 10, seaParser.RULE_function_call);
	    try {
	        this.state = 73;
	        this._errHandler.sync(this);
	        var la_ = this._interp.adaptivePredict(this._input,5,this._ctx);
	        switch(la_) {
	        case 1:
	            this.enterOuterAlt(localctx, 1);
	            this.state = 65;
	            this.match(seaParser.VarName);
	            this.state = 66;
	            this.match(seaParser.T__11);
	            this.state = 67;
	            this.match(seaParser.T__12);
	            break;

	        case 2:
	            this.enterOuterAlt(localctx, 2);
	            this.state = 68;
	            this.match(seaParser.VarName);
	            this.state = 69;
	            this.match(seaParser.T__11);
	            this.state = 70;
	            this.expr_list();
	            this.state = 71;
	            this.match(seaParser.T__12);
	            break;

	        }
	    } catch (re) {
	    	if(re instanceof antlr4.error.RecognitionException) {
		        localctx.exception = re;
		        this._errHandler.reportError(this, re);
		        this._errHandler.recover(this, re);
		    } else {
		    	throw re;
		    }
	    } finally {
	        this.exitRule();
	    }
	    return localctx;
	}


}

seaParser.EOF = antlr4.Token.EOF;
seaParser.T__0 = 1;
seaParser.T__1 = 2;
seaParser.T__2 = 3;
seaParser.T__3 = 4;
seaParser.T__4 = 5;
seaParser.T__5 = 6;
seaParser.T__6 = 7;
seaParser.T__7 = 8;
seaParser.T__8 = 9;
seaParser.T__9 = 10;
seaParser.T__10 = 11;
seaParser.T__11 = 12;
seaParser.T__12 = 13;
seaParser.T__13 = 14;
seaParser.T__14 = 15;
seaParser.T__15 = 16;
seaParser.T__16 = 17;
seaParser.NEWLINE = 18;
seaParser.Number = 19;
seaParser.StringLiteral = 20;
seaParser.UnterminatedStringLiteral = 21;
seaParser.VarName = 22;
seaParser.Metric = 23;
seaParser.Cohort = 24;
seaParser.WS = 25;

seaParser.RULE_formula = 0;
seaParser.RULE_stmt = 1;
seaParser.RULE_expr = 2;
seaParser.RULE_expr_list = 3;
seaParser.RULE_if_stmt = 4;
seaParser.RULE_function_call = 5;

class FormulaContext extends antlr4.ParserRuleContext {

    constructor(parser, parent, invokingState) {
        if(parent===undefined) {
            parent = null;
        }
        if(invokingState===undefined || invokingState===null) {
            invokingState = -1;
        }
        super(parent, invokingState);
        this.parser = parser;
        this.ruleIndex = seaParser.RULE_formula;
    }


	 
		copyFrom(ctx) {
			super.copyFrom(ctx);
		}

}


class FormulaDefinitionContext extends FormulaContext {

    constructor(parser, ctx) {
        super(parser);
        super.copyFrom(ctx);
    }

	stmt() {
	    return this.getTypedRuleContext(StmtContext,0);
	};

	NEWLINE() {
	    return this.getToken(seaParser.NEWLINE, 0);
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterFormulaDefinition(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitFormulaDefinition(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitFormulaDefinition(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}

seaParser.FormulaDefinitionContext = FormulaDefinitionContext;

class StmtContext extends antlr4.ParserRuleContext {

    constructor(parser, parent, invokingState) {
        if(parent===undefined) {
            parent = null;
        }
        if(invokingState===undefined || invokingState===null) {
            invokingState = -1;
        }
        super(parent, invokingState);
        this.parser = parser;
        this.ruleIndex = seaParser.RULE_stmt;
    }

	expr() {
	    return this.getTypedRuleContext(ExprContext,0);
	};

	if_stmt() {
	    return this.getTypedRuleContext(If_stmtContext,0);
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterStmt(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitStmt(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitStmt(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}



class ExprContext extends antlr4.ParserRuleContext {

    constructor(parser, parent, invokingState) {
        if(parent===undefined) {
            parent = null;
        }
        if(invokingState===undefined || invokingState===null) {
            invokingState = -1;
        }
        super(parent, invokingState);
        this.parser = parser;
        this.ruleIndex = seaParser.RULE_expr;
    }


	 
		copyFrom(ctx) {
			super.copyFrom(ctx);
		}

}


class NumberLiteralExpressionContext extends ExprContext {

    constructor(parser, ctx) {
        super(parser);
        super.copyFrom(ctx);
    }

	Number() {
	    return this.getToken(seaParser.Number, 0);
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterNumberLiteralExpression(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitNumberLiteralExpression(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitNumberLiteralExpression(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}

seaParser.NumberLiteralExpressionContext = NumberLiteralExpressionContext;

class ParenthesizedExpressionContext extends ExprContext {

    constructor(parser, ctx) {
        super(parser);
        super.copyFrom(ctx);
    }

	expr() {
	    return this.getTypedRuleContext(ExprContext,0);
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterParenthesizedExpression(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitParenthesizedExpression(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitParenthesizedExpression(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}

seaParser.ParenthesizedExpressionContext = ParenthesizedExpressionContext;

class AdditiveExpressionContext extends ExprContext {

    constructor(parser, ctx) {
        super(parser);
        super.copyFrom(ctx);
    }

	expr = function(i) {
	    if(i===undefined) {
	        i = null;
	    }
	    if(i===null) {
	        return this.getTypedRuleContexts(ExprContext);
	    } else {
	        return this.getTypedRuleContext(ExprContext,i);
	    }
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterAdditiveExpression(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitAdditiveExpression(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitAdditiveExpression(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}

seaParser.AdditiveExpressionContext = AdditiveExpressionContext;

class RelationalExpressionContext extends ExprContext {

    constructor(parser, ctx) {
        super(parser);
        super.copyFrom(ctx);
    }

	expr = function(i) {
	    if(i===undefined) {
	        i = null;
	    }
	    if(i===null) {
	        return this.getTypedRuleContexts(ExprContext);
	    } else {
	        return this.getTypedRuleContext(ExprContext,i);
	    }
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterRelationalExpression(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitRelationalExpression(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitRelationalExpression(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}

seaParser.RelationalExpressionContext = RelationalExpressionContext;

class LogicalAndExpressionContext extends ExprContext {

    constructor(parser, ctx) {
        super(parser);
        super.copyFrom(ctx);
    }

	expr = function(i) {
	    if(i===undefined) {
	        i = null;
	    }
	    if(i===null) {
	        return this.getTypedRuleContexts(ExprContext);
	    } else {
	        return this.getTypedRuleContext(ExprContext,i);
	    }
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterLogicalAndExpression(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitLogicalAndExpression(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitLogicalAndExpression(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}

seaParser.LogicalAndExpressionContext = LogicalAndExpressionContext;

class StringLiteralContext extends ExprContext {

    constructor(parser, ctx) {
        super(parser);
        super.copyFrom(ctx);
    }

	StringLiteral() {
	    return this.getToken(seaParser.StringLiteral, 0);
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterStringLiteral(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitStringLiteral(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitStringLiteral(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}

seaParser.StringLiteralContext = StringLiteralContext;

class LogicalOrExpressionContext extends ExprContext {

    constructor(parser, ctx) {
        super(parser);
        super.copyFrom(ctx);
    }

	expr = function(i) {
	    if(i===undefined) {
	        i = null;
	    }
	    if(i===null) {
	        return this.getTypedRuleContexts(ExprContext);
	    } else {
	        return this.getTypedRuleContext(ExprContext,i);
	    }
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterLogicalOrExpression(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitLogicalOrExpression(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitLogicalOrExpression(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}

seaParser.LogicalOrExpressionContext = LogicalOrExpressionContext;

class MetricExpressionContext extends ExprContext {

    constructor(parser, ctx) {
        super(parser);
        super.copyFrom(ctx);
    }

	Metric() {
	    return this.getToken(seaParser.Metric, 0);
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterMetricExpression(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitMetricExpression(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitMetricExpression(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}

seaParser.MetricExpressionContext = MetricExpressionContext;

class MutilplicativeExpressionContext extends ExprContext {

    constructor(parser, ctx) {
        super(parser);
        super.copyFrom(ctx);
    }

	expr = function(i) {
	    if(i===undefined) {
	        i = null;
	    }
	    if(i===null) {
	        return this.getTypedRuleContexts(ExprContext);
	    } else {
	        return this.getTypedRuleContext(ExprContext,i);
	    }
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterMutilplicativeExpression(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitMutilplicativeExpression(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitMutilplicativeExpression(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}

seaParser.MutilplicativeExpressionContext = MutilplicativeExpressionContext;

class FunctionCallExpressionContext extends ExprContext {

    constructor(parser, ctx) {
        super(parser);
        super.copyFrom(ctx);
    }

	function_call() {
	    return this.getTypedRuleContext(Function_callContext,0);
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterFunctionCallExpression(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitFunctionCallExpression(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitFunctionCallExpression(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}

seaParser.FunctionCallExpressionContext = FunctionCallExpressionContext;

class Expr_listContext extends antlr4.ParserRuleContext {

    constructor(parser, parent, invokingState) {
        if(parent===undefined) {
            parent = null;
        }
        if(invokingState===undefined || invokingState===null) {
            invokingState = -1;
        }
        super(parent, invokingState);
        this.parser = parser;
        this.ruleIndex = seaParser.RULE_expr_list;
    }

	expr = function(i) {
	    if(i===undefined) {
	        i = null;
	    }
	    if(i===null) {
	        return this.getTypedRuleContexts(ExprContext);
	    } else {
	        return this.getTypedRuleContext(ExprContext,i);
	    }
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterExpr_list(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitExpr_list(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitExpr_list(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}



class If_stmtContext extends antlr4.ParserRuleContext {

    constructor(parser, parent, invokingState) {
        if(parent===undefined) {
            parent = null;
        }
        if(invokingState===undefined || invokingState===null) {
            invokingState = -1;
        }
        super(parent, invokingState);
        this.parser = parser;
        this.ruleIndex = seaParser.RULE_if_stmt;
    }


	 
		copyFrom(ctx) {
			super.copyFrom(ctx);
		}

}


class IfStatementContext extends If_stmtContext {

    constructor(parser, ctx) {
        super(parser);
        super.copyFrom(ctx);
    }

	expr = function(i) {
	    if(i===undefined) {
	        i = null;
	    }
	    if(i===null) {
	        return this.getTypedRuleContexts(ExprContext);
	    } else {
	        return this.getTypedRuleContext(ExprContext,i);
	    }
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterIfStatement(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitIfStatement(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitIfStatement(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}

seaParser.IfStatementContext = IfStatementContext;

class Function_callContext extends antlr4.ParserRuleContext {

    constructor(parser, parent, invokingState) {
        if(parent===undefined) {
            parent = null;
        }
        if(invokingState===undefined || invokingState===null) {
            invokingState = -1;
        }
        super(parent, invokingState);
        this.parser = parser;
        this.ruleIndex = seaParser.RULE_function_call;
    }

	VarName() {
	    return this.getToken(seaParser.VarName, 0);
	};

	expr_list() {
	    return this.getTypedRuleContext(Expr_listContext,0);
	};

	enterRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.enterFunction_call(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof seaListener ) {
	        listener.exitFunction_call(this);
		}
	}

	accept(visitor) {
	    if ( visitor instanceof seaVisitor ) {
	        return visitor.visitFunction_call(this);
	    } else {
	        return visitor.visitChildren(this);
	    }
	}


}




seaParser.FormulaContext = FormulaContext; 
seaParser.StmtContext = StmtContext; 
seaParser.ExprContext = ExprContext; 
seaParser.Expr_listContext = Expr_listContext; 
seaParser.If_stmtContext = If_stmtContext; 
seaParser.Function_callContext = Function_callContext; 
