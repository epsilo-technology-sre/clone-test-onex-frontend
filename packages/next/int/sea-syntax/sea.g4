grammar sea;

formula: stmt NEWLINE # FormulaDefinition;

stmt: (expr | if_stmt);

expr:
	function_call								# FunctionCallExpression
	| expr ('*' | '/') expr						# MutilplicativeExpression
	| expr ('+' | '-') expr						# AdditiveExpression
	| expr ('<' | '>' | '<=' | '>=' | '=') expr	# RelationalExpression
	| expr 'OR' expr							# LogicalOrExpression
	| expr 'AND' expr							# LogicalAndExpression
	| Metric									# MetricExpression
	| Number									# NumberLiteralExpression
	| StringLiteral								# StringLiteral
	| '(' expr ')'								# ParenthesizedExpression;

expr_list: expr (',' expr)*;

if_stmt: 'IF' expr 'THEN' expr 'ELSE' expr # IfStatement;

function_call: VarName '(' ')' | VarName '(' expr_list ')';

NEWLINE: [\r\n]+;
Number: [0-9]+ | [0-9]+ '.' [0-9]+;

StringLiteral: UnterminatedStringLiteral '"';
UnterminatedStringLiteral: '"' (~["\\\r\n] | '\\' (. | EOF))*;

fragment IDENTIFIER: [a-zA-Z_]+ [0-9a-zA-Z_]*;

VarName: IDENTIFIER;

Metric: '@' IDENTIFIER ('(' Cohort ')')*;
Cohort: [0-9]+ ('h' | 'd' | 'm' | 'q' | 'H' | 'D' | 'M' | 'Q');

WS: [ \t]+ -> skip;
