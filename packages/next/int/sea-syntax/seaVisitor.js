// Generated from sea.g4 by ANTLR 4.9.2
// jshint ignore: start
import antlr4 from 'antlr4';

// This class defines a complete generic visitor for a parse tree produced by seaParser.

export default class seaVisitor extends antlr4.tree.ParseTreeVisitor {

	// Visit a parse tree produced by seaParser#FormulaDefinition.
	visitFormulaDefinition(ctx) {
	  return this.visitChildren(ctx);
	}


	// Visit a parse tree produced by seaParser#stmt.
	visitStmt(ctx) {
	  return this.visitChildren(ctx);
	}


	// Visit a parse tree produced by seaParser#NumberLiteralExpression.
	visitNumberLiteralExpression(ctx) {
		return Number(ctx.Number().getText());
	  return this.visitChildren(ctx);
	}


	// Visit a parse tree produced by seaParser#ParenthesizedExpression.
	visitParenthesizedExpression(ctx) {
	  return this.visitChildren(ctx);
	}


	// Visit a parse tree produced by seaParser#AdditiveExpression.
	visitAdditiveExpression(ctx) {
	  return this.visitChildren(ctx);
	}


	// Visit a parse tree produced by seaParser#RelationalExpression.
	visitRelationalExpression(ctx) {
	  return this.visitChildren(ctx);
	}


	// Visit a parse tree produced by seaParser#LogicalAndExpression.
	visitLogicalAndExpression(ctx) {
	  return this.visitChildren(ctx);
	}


	// Visit a parse tree produced by seaParser#StringLiteral.
	visitStringLiteral(ctx) {
	  return this.visitChildren(ctx);
	}


	// Visit a parse tree produced by seaParser#LogicalOrExpression.
	visitLogicalOrExpression(ctx) {
	  return this.visitChildren(ctx);
	}


	// Visit a parse tree produced by seaParser#MetricExpression.
	visitMetricExpression(ctx) {
	  return this.visitChildren(ctx);
	}


	// Visit a parse tree produced by seaParser#MutilplicativeExpression.
	visitMutilplicativeExpression(ctx) {
	  return this.visitChildren(ctx);
	}


	// Visit a parse tree produced by seaParser#FunctionCallExpression.
	visitFunctionCallExpression(ctx) {
	  return this.visitChildren(ctx);
	}


	// Visit a parse tree produced by seaParser#expr_list.
	visitExpr_list(ctx) {
	  return this.visitChildren(ctx);
	}


	// Visit a parse tree produced by seaParser#IfStatement.
	visitIfStatement(ctx) {
	  return this.visitChildren(ctx);
	}


	// Visit a parse tree produced by seaParser#function_call.
	visitFunction_call(ctx) {
	  return this.visitChildren(ctx);
	}



}