type CustomMetric = {
  id?: number;
  name: string;
  eid: string;
  formula: string;
  transpiledCode?: {
    php: string;
    js: string;
  };
  requiredMetrics?: [string, any][];
  requiredFunctions?: string[];
  createdAt?: any;
  updatedAt?: any;
};
