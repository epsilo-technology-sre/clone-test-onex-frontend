import { ChannelHeaderUI } from '@ep/shopee/src/components/channel-header';
import { BlockScreenUI } from '@ep/shopee/src/components/common/block-screen';
import { BreadcrumbsUI } from '@ep/shopee/src/components/common/breadcrumbs';
import {
  TabPanelUI,
  TabSelectionContainer,
} from '@ep/shopee/src/components/common/tab-selection';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { TABS } from '../constants';
import { CampaignContainer } from '../containers/campaign-container/campaign-container';
import { GlobalFiltersContainer } from '../containers/global-filters-container';
import { KeywordContainer } from '../containers/keyword-container/keyword-container';
import { ProductContainer } from '../containers/product-container/product-container';
import { CHANGE_TAB } from '../redux/actions';

const LINKS = [
  { text: 'Advertising', href: '/' },
  { text: 'Tokopedia', href: '/' },
  { text: 'Product Ads', href: '/' },
];

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      background: '#ffffff',
    },
  }),
);

export const CampaignPage = () => {
  const classes = useStyles();
  const [links, setLinks] = useState(LINKS);
  const [showWaiting, setShowWaiting] = useState(false);

  const dispatch = useDispatch();

  const linkCreateCampaign = '/advertising/tokopedia/create-campaign';

  const { tab } = useSelector((state) => ({
    tab: state.tab,
  }));

  const tabs = [
    { type: TABS.CAMPAIGN, text: 'Campaigns' },
    { type: TABS.PRODUCT, text: 'Products' },
    { type: TABS.KEYWORD, text: 'Keywords' },
  ];

  const handleChangeTab = (tab: any) => {
    dispatch(CHANGE_TAB(tab));
  };

  const handleClickLink = (link: any) => {
    console.log('Click link ', link);
  };

  return (
    <>
      <div className={classes.root}>
        <BreadcrumbsUI links={links} onClickLink={handleClickLink} />
        <ChannelHeaderUI
          title="Product Ads"
          calloutButtonText="Create Product Ads"
          calloutLink={linkCreateCampaign}
        />
        <GlobalFiltersContainer />
        <TabSelectionContainer
          tabs={tabs}
          selectedTab={tab}
          showIcon={true}
          onChangeTab={handleChangeTab}
        />
        <TabPanelUI value={tab} index={TABS.CAMPAIGN}>
          <CampaignContainer />
        </TabPanelUI>
        <TabPanelUI value={tab} index={TABS.PRODUCT}>
          <ProductContainer />
        </TabPanelUI>
        <TabPanelUI value={tab} index={TABS.KEYWORD}>
          <KeywordContainer />
        </TabPanelUI>
      </div>
      <BlockScreenUI isShow={showWaiting} />
    </>
  );
};

export default CampaignPage;
