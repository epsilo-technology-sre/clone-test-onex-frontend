import { ThemeProvider } from '@material-ui/core/styles';
import React from 'react';
import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { store } from '../../redux/store';
import LazadaTheme from '../../theme';
import { AddProductContainer } from './add-product-container';

export default {
  title: 'Toko / Container',
};

export const AddProductDialog = () => {
  return (
    <Provider store={store}>
      <ThemeProvider theme={LazadaTheme}>
        <AddProductContainer />
        <ToastContainer closeOnClick={false} hideProgressBar={true} />
      </ThemeProvider>
    </Provider>
  );
};
