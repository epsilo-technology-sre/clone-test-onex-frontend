import { actions } from '@ep/toko/src/redux/create-campaign/actions';
import { CreateCampaignState } from '@ep/toko/src/redux/create-campaign/reducers';
import { AddProductTableAction } from '@ep/shopee/src/components/add-product-keyword/add-product-table-action';
import { ProductBucket } from '@ep/shopee/src/components/add-product-keyword/product-bucket';
import { ProductBudgetEditor } from '@ep/shopee/src/components/add-product-keyword/product-budget-editor';
import {
  ButtonTextUI,
  ButtonUI,
} from '@ep/shopee/src/components/common/button';
import { TableUI } from '@ep/shopee/src/components/common/table';
import { ShopCampaignSelector } from '@ep/shopee/src/components/create-campaign/shop-campaign-selector';
import NoProduct from '@ep/shopee/src/images/no-product.svg';
import {
  Box,
  Divider,
  Grid,
  makeStyles,
  Typography,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { differenceWith, get, isEqual } from 'lodash';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { initColumns } from './add-product-columns';

const useStyle = makeStyles({
  modifyBudgetWrapper: {
    marginBottom: 16,
    border: '2px solid #E4E7E9',
  },
});

export const AddProductContainer = (props: any) => {
  const classes = useStyle();
  const dispatch = useDispatch();
  const [selectedShop, setSelectedShop] = useState();
  const [selectedCampaign, setSelectedCampaign] = useState();
  const [sortBy, setSortBy] = useState([]);

  useEffect(() => {
    handleGetProduct({ shopEid: shopId });
    dispatch(actions.getCategoryList({ shopEid: shopId }));
  }, []);

  const state = useSelector(
    ({
      setupCampaign: { shopCurrency, shop, timeline },
      setupProduct: { shops, campaigns, productList, productBucket },
    }: CreateCampaignState) => {
      const products = get(productList, 'items', []).map((item) => {
        return {
          ...item,
          _isDisabled: item.added,
        };
      });
      return {
        shops,
        shopCurrency,
        timeline,
        shop,
        campaigns,
        categories: get(productList, 'categories', []),
        products,
        selectedProductIds: get(productList, 'selectedIds', []),
        productBucket,
        selectedCategories: get(
          productList,
          'selectedCategories',
          [],
        ),
        search: get(productList, 'searchText', ''),
        pagination: {
          page: get(productList, 'page', 1),
          itemCount: get(productList, 'itemCount', 0),
          limit: get(productList, 'limit', 10),
        },
      };
    },
  );

  const {
    shops = [],
    shop: shopId,
    timeline,
    shopCurrency = 'VND',
    campaigns = [],
    categories = [],
    products = [],
    selectedProductIds = [],
    productBucket: productsInBucket,
    selectedCategories,
    search,
    pagination = {
      page: 1,
      limit: 10,
      itemCount: 0,
    },
  } = state;

  const handleGetProduct = (params: any = {}) => {
    const categoryNames = selectedCategories
      .map((i) => i.text)
      .join(',');
    dispatch(
      actions.getShopProducts({
        shopEid: shopId,
        limit: pagination.limit,
        page: pagination.page,
        searchText: search,
        categoryNames: categoryNames,
        from: timeline.startDate,
        to: timeline.endDate,
        orderBy: sortBy[0]?.id,
        orderMode: sortBy[0]?.desc ? 'desc' : 'asc',
        ...params,
      }),
    );
  };

  const addProductsToBucket = React.useCallback(
    (productIdList: any) => {
      console.info({ productList: productIdList });
      dispatch(
        actions.addProductAddToBucket({
          shopProductIds: productIdList,
          isAdded: true,
        }),
      );
    },
    [],
  );

  const handleSelectAll = React.useCallback((checked) => {
    dispatch(actions.addProductSelectAll({ isAdd: checked }));
  }, []);

  const handleSelectItem = React.useMemo(() => {
    return (item: any, checked: boolean) => {
      dispatch(
        actions.addProductSelectProduct({
          shopProductIds: Number(getRowId(item)),
          selected: checked,
        }),
      );
    };
  }, []);

  const getRowId = React.useCallback((row) => {
    return String(row.productId);
  }, []);

  const handleRemoveProducts = (productList: any) => {
    dispatch(
      actions.addProductAddToBucket({
        shopProductIds: productList.map((i) => i.productId),
        isAdded: false,
      }),
    );
  };

  const headers = React.useMemo(
    () =>
      initColumns({
        onAddToBucket: (value: any) => {
          addProductsToBucket(value.productId);
        },
      }),
    [],
  );

  const handleSearch = (value: any) => {
    dispatch(
      actions.updateAddProductSearch({
        searchText: value,
      }),
    );

    handleGetProduct({
      searchText: value,
    });
  };

  const handleChangeCategories = (value: any) => {
    dispatch(
      actions.updateAddProductSelectedCategory({ categories: value }),
    );

    handleGetProduct({
      page: 1,
      categoryNames: value.map((i) => i.text).join(','),
    });
  };

  const handleSorting = (sort: any) => {
    const isDifference =
      differenceWith(sort, sortBy, isEqual).length > 0;
    if (isDifference) {
      console.log('handle sorting', sort);
      setSortBy(sort);
      handleGetProduct({
        orderBy: sort[0]?.id,
        orderMode: sort[0]?.desc ? 'desc' : 'asc',
      });
    }
  };

  const handleChangePage = (page: number) => {
    handleGetProduct({
      page,
    });
  };

  const handleChangePageSize = (value: number) => {
    handleGetProduct({
      page: 1,
      limit: value,
    });
  };

  const handleChangeBudget = (value: any) => {
    let totalBudget = 0;
    let dailyBudget = 0;
    if (!value.isNoLimit) {
      if (value.budgetType === 'daily_budget') {
        dailyBudget = value.budget;
      } else {
        totalBudget = value.budget;
      }
    }

    dispatch(
      actions.addProductAddToBucket({
        shopProductIds: selectedProductIds,
        isAdded: true,
        modification: {
          totalBudget,
          dailyBudget,
        },
      }),
    );
  };

  const handleSave = () => {
    dispatch(actions.addBucketToCampaignProduct());
    dispatch(actions.resetAddProduct());
  };

  const handleCancel = () => {
    dispatch(actions.openAddProductModal({ visible: false }));
    dispatch(actions.resetAddProduct());
  };

  return (
    <Box p={2} style={{ background: '#ffffff' }}>
      <Box mb={1}>
        <Grid container justify="space-between">
          <Grid item>
            <Typography variant="h6">Add Product</Typography>
          </Grid>
          <Grid item>
            <CloseIcon
              style={{ cursor: 'pointer' }}
              onClick={handleCancel}
            />
          </Grid>
        </Grid>
      </Box>
      <Grid container spacing={1}>
        <Grid item xs={9}>
          {!shopId && (
            <Box mb={2}>
              <Box mb={2}>
                <ShopCampaignSelector
                  shops={shops}
                  campaigns={campaigns}
                  onChangeShop={(value: any) =>
                    setSelectedShop(value)
                  }
                  onChangeCampaign={(value: any) =>
                    setSelectedCampaign(value)
                  }
                />
              </Box>
              <Divider />
            </Box>
          )}
          {/* <Box className={classes.modifyBudgetWrapper}>
            <ProductBudgetEditor
              title="Modify budget"
              hideDaily={true}
              disabled={selectedProductIds.length === 0}
              currency={shopCurrency}
              onSubmit={handleChangeBudget}
            />
          </Box> */}
          <Box>
            <AddProductTableAction
              searchText={search}
              categories={categories}
              selectedCategories={selectedCategories}
              disabled={selectedProductIds.length === 0}
              onSearch={handleSearch}
              onChangeCategories={handleChangeCategories}
              onClickAddAll={() =>
                addProductsToBucket(selectedProductIds)
              }
            ></AddProductTableAction>
          </Box>
          <Box mt={2}>
            <Grid container>
              <Grid item xs={12}>
                <TableUI
                  className="setHeightModal"
                  columns={headers}
                  rows={products}
                  onSelectItem={handleSelectItem}
                  onSelectAll={handleSelectAll}
                  selectedIds={selectedProductIds}
                  getRowId={getRowId}
                  onSort={handleSorting}
                  resultTotal={pagination.itemCount}
                  page={pagination.page}
                  pageSize={pagination.limit}
                  onChangePage={handleChangePage}
                  onChangePageSize={handleChangePageSize}
                  noData={
                    <Box py={6}>
                      <img src={NoProduct} width="200" height="200" />
                      <Typography
                        variant="h5"
                        style={{ width: '60%', margin: 'auto' }}
                      >
                        Before you add the keywords, let choose the
                        products you want to add them in.
                      </Typography>
                    </Box>
                  }
                />
              </Grid>
            </Grid>
          </Box>
        </Grid>
        <Grid item xs={3}>
          <ProductBucket
            products={productsInBucket}
            onRemove={handleRemoveProducts}
          ></ProductBucket>
        </Grid>
      </Grid>
      <Box>
        <Grid
          container
          alignItems="center"
          justify="flex-end"
          spacing={1}
        >
          <Grid item>
            <ButtonTextUI
              size="small"
              colortext="#000"
              label="Cancel"
              onClick={handleCancel}
            ></ButtonTextUI>
          </Grid>
          <Grid item>
            <ButtonUI
              disabled={productsInBucket.length === 0}
              size="small"
              label="Add products"
              onClick={handleSave}
            ></ButtonUI>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};
