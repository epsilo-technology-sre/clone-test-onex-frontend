import {
  ButtonCell,
  InStockCell,
  LogCell,
  PercentCell,
  ProductBudgetCell,
  ProductCell,
  StatusCell,
  SubjectCell,
  SwitchCell,
} from '@ep/shopee/src/components/common/table-cell';
import { makeLazyCell } from '@ep/shopee/src/components/common/table-cell/lazy-cell';
import { PRODUCT_STATUS } from '../../constants';

export const PRODUCT_COLUMNS = [
  {
    Header: ' ',
    id: 'switch',
    accessor: (row: any) => {
      return {
        isOn: row.child_status.is_promoted_product_status,
        isDisabled: row.status === 'ended' || row.status === 'closed',
        // isDisabled: 1,
        id: row.campaign_product_id,
        row,
      };
    },
    sticky: 'left',
    width: 70,
    disableSortBy: true,
    Cell: SwitchCell,
  },
  {
    Header: 'Product',
    id: 'product_name',
    accessor: (row: any) => ({
      name: row.product_name,
      code: row.product_sid,
    }),
    sticky: 'left',
    width: 300,
    disableSortBy: true,
    Cell: ProductCell,
  },
  {
    Header: 'Status',
    id: 'status',
    sticky: 'left',
    width: 70,
    disableSortBy: true,
    accessor: (row: any) => {
      const type = row.status;
      const children = [];
      return { type, children };
    },
    Cell: StatusCell,
  },
  {
    Header: 'In stock',
    id: 'product_stock',
    accessor: (row: any) => ({
      inStock: row.product_stock,
      updatedDate: row.last_pull_product_at,
    }),
    Cell: InStockCell,
    disableSortBy: true,
  },
  {
    Header: 'Discount',
    id: 'discount',
    accessor: (row: any) => ({ number: row.discount, prefix: '%' }),
    Cell: PercentCell,
    disableSortBy: true,
  },
  {
    Header: 'Campaign',
    id: 'campaign_name',
    accessor: (row: any) => ({
      name: row.campaign_name,
      code: row.shop_name,
    }),
    width: 200,
    Cell: SubjectCell,
  },
  {
    Header: 'Impression',
    id: 'sum_total_views',
    accessor: (row: any) => ({ number: row.sum_total_views }),
    Cell: PercentCell,
  },
  {
    Header: 'Click',
    id: 'sum_click',
    accessor: (row: any) => ({ number: row.sum_click }),
    Cell: PercentCell,
  },
  // {
  //   Header: 'Keyword',
  //   id: 'totalKeyword',
  //   accessor: (row: any) => ({ number: row.totalKeyword }),
  //   Cell: PercentCell,
  //   disableSortBy: true,
  // },
  {
    Header: 'Direct Ads item sold',
    id: 'sum_direct_item_sold',
    accessor: (row: any) => ({ number: row.sum_direct_item_sold }),
    Cell: PercentCell,
    width: 175,
  },
  {
    Header: 'Ads item sold',
    id: 'sum_item_sold',
    accessor: (row: any) => ({ number: row.sum_item_sold }),
    Cell: PercentCell,
    width: 130,
  },
  {
    Header: 'Ads GMV',
    id: 'sum_gmv',
    accessor: (row: any) => ({
      number: row.sum_gmv,
      currency: row.currency,
    }),
    Cell: PercentCell,
    alwaysEnable: true,
    width: 100,
  },
  // {
  //   Header: 'Direct Ads GMV',
  //   id: 'sum_direct_gmv',
  //   accessor: (row: any) => ({
  //     number: row.sum_direct_gmv,
  //     currency: row.currency,
  //   }),
  //   Cell: PercentCell,
  //   alwaysEnable: true,
  //   width: 150,
  // },
  {
    Header: 'CPC',
    id: 'cpc',
    accessor: (row: any) => ({
      number: row.cpc,
      currency: row.currency,
    }),
    Cell: makeLazyCell(PercentCell),
    alwaysEnable: true,
    disableSortBy: true,
  },
  {
    Header: 'Bid Price',
    id: 'bid_price',
    accessor: (row: any) => ({
      number: row.max_cpc,
      currency: row.currency,
    }),
    Cell: makeLazyCell(PercentCell),
    alwaysEnable: true,
    disableSortBy: true,
  },
  {
    Header: 'Budget',
    id: 'budget',
    accessor: (row: any) => {
      return {
        dailyBudget: row.budget_config.value_daily,
        totalBudget: row.budget_config.value_total,
        currency: row.currency,
        // editor: row._isDisabled ? null : (
        //   <ProductBudgetEditorPopover
        //     product={row}
        //     currency={row.currency}
        //     total={row.budget_config.value_total}
        //     daily={row.budget_config.value_daily}
        //     hideDaily={true}
        //     triggerElem={<EditButton />}
        //   />
        // ),
      };
    },
    Cell: makeLazyCell(ProductBudgetCell),
    disableSortBy: true,
  },
  // {
  //   Header: 'Cost',
  //   id: 'sum_cost',
  //   accessor: (row: any) => ({
  //     number: row.sum_cost,
  //     currency: row.currency,
  //   }),
  //   Cell: PercentCell,
  //   alwaysEnable: true,
  // },
  // {
  //   Header: 'ROAS',
  //   id: 'roas',
  //   accessor: (row: any) => ({ number: row.roas }),
  //   Cell: PercentCell,
  //   disableSortBy: true,
  // },
  // {
  //   Header: 'CIR',
  //   id: 'cir',
  //   accessor: (row: any) => ({ number: row.cir }),
  //   Cell: PercentCell,
  //   disableSortBy: true,
  // },
  {
    Header: 'Rule',
    id: 'rule',
    accessor: (row: any) => {
      let label = '';
      if (row.ruleCount) {
        label = row.ruleCount + ' rules';
      } else {
        const allowStatus = [
          PRODUCT_STATUS.RUNNING,
          PRODUCT_STATUS.PAUSED,
          PRODUCT_STATUS.SCHEDULED,
        ];
        if (allowStatus.includes(row.status)) {
          label = 'Add rule';
        }
      }

      return {
        ...row,
        label,
      };
    },
    Cell: makeLazyCell(ButtonCell),
    disableSortBy: true,
    width: 95,
    alwaysEnable: true,
  },
  {
    Header: '',
    id: 'ruleLog',
    accessor: (row: any) => row,
    Cell: makeLazyCell(LogCell),
    disableSortBy: true,
    width: 70,
    alwaysEnable: true,
  },
];
