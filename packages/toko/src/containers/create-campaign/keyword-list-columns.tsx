import { EditButton } from '@ep/shopee/src/components/common/edit-button';
import {
  AddToBucketCell,
  CellText,
  EllipsisCell,
  PercentCell,
  ProductListActionCell,
} from '@ep/shopee/src/components/common/table-cell';
import { KeywordTypeEditorPopover } from '@ep/shopee/src/components/edit-form/keyword-type-editor-popover';
import React from 'react';
import { KeywordBudgetEditorPopover } from '../../components/custom/edit-form/keyword-budget-editor-popover';
import { MATCH_TYPE, MATCH_TYPE_LABEL } from '../../constants';
import { KeywordEditContext } from './keyword-edit-context';

export const initColumns = (props: any) => {
  const { onDelete } = props;
  return [
    {
      Header: 'Keyword Bidding',
      accessor: (row: any) => ({ value: row.keywordName }),
      sticky: 'left',
      width: 200,
      disableSortBy: true,
      Cell: EllipsisCell,
    },
    {
      Header: 'Search volume',
      id: 'searchVolume',
      accessor: (row: any) => ({
        number: row.searchVolume,
      }),
      disableSortBy: true,
      Cell: PercentCell,
    },
    {
      Header: 'Bid price',
      id: 'bidding_price',
      accessor: (row: any) => ({
        number: row.biddingPrice,
        currency: row.currency,
        editor: row._isDisabled ? null : (
          <KeywordBudgetEditorPopover
            context={KeywordEditContext}
            item={{ ...row, id: row.entityId }}
            currency={row.currency}
            bidding_price={Number(row.biddingPrice)}
            triggerElem={<EditButton />}
          />
        ),
      }),
      Cell: PercentCell,
      alwaysEnable: true,
      disableSortBy: true,
    },
    {
      Header: 'Match type',
      disableSortBy: true,
      accessor: (row: any) => ({
        text: MATCH_TYPE_LABEL[row.matchType],
        currency: row.currency,
        editor: row._isDisabled ? null : (
          <KeywordTypeEditorPopover
            context={KeywordEditContext}
            keyword={{
              ...row,
              match_type:
                row.matchType === MATCH_TYPE.BROAD_MATCH
                  ? MATCH_TYPE_LABEL[row.matchType]
                  : MATCH_TYPE.EXACT_MATCH,
            }}
            triggerElem={<EditButton />}
          />
        ),
      }),
      Cell: CellText,
    },
    {
      Header: '',
      id: 'action',
      accessor: (row: any) => ({
        product: row,
        onAddKeyword: undefined,
        onDelete,
      }),
      width: 200,
      Cell: ProductListActionCell,
      disableSortBy: true,
    },
  ];
};
