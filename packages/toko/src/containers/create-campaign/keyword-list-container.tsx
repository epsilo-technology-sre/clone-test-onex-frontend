import { COLORS } from '@ep/toko/src/constants';
import { BulkActionsUI } from '@ep/shopee/src/components/common/bulk-actions';
import { ButtonUI } from '@ep/shopee/src/components/common/button';
import { MultiSelectUI } from '@ep/shopee/src/components/common/multi-select';
import { TableUI } from '@ep/shopee/src/components/common/table';
import { InputUI } from '@ep/shopee/src/components/create-campaign/common';
import NoProduct from '@ep/shopee/src/images/no-product.svg';
import {
  Box,
  Divider,
  Grid,
  InputAdornment,
  Snackbar,
  Typography,
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import SearchIcon from '@material-ui/icons/Search';
import Alert from '@material-ui/lab/Alert';
import { orderBy } from 'lodash';
import React, { useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { actions } from '../../redux/create-campaign/actions';
import { CreateCampaignState } from '../../redux/create-campaign/reducers';
import { initColumns } from './keyword-list-columns';
import { KeywordEditContext } from './keyword-edit-context';
import { ModalBulkActionKeyword } from '../../components/custom/modal-bulk-actions/modal-bulk-action-keywords';
import { KeywordRuleLog } from '../rules/keyword-rule-log';

export const KeywordListContainer = (props: {
  backToPreviousScreen: Function;
  handleNextStep: Function;
}) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { setupCampaign, setupKeyword, loading } = useSelector(
    (state: CreateCampaignState) => {
      return {
        setupCampaign: state.setupCampaign,
        setupKeyword: state.setupKeyword,
        loading: state.loading,
      };
    },
  );

  const [searchText, setSearchText] = useState('');

  const [sortBy, setSortBy] = useState([]);

  const [submitting, setSubmitting] = useState(false);

  const [notification, setNotification] = useState<any>({
    open: false,
    status: false,
  });
  const [
    openModalBulkAction,
    setOpenModalBulkAction,
  ] = React.useState({
    open: false,
    items: [],
  });

  const [disableSubmit, setDisableSubmit] = useState(false);

  useEffect(() => {
    dispatch(
      actions.addKeyword.getEntityCategoryList({
        shopEid: setupCampaign.shop,
      }),
    );
  }, []);

  useEffect(() => {
    if (loading.createCampaign) {
      setSubmitting(false);
      setNotification({
        ...notification,
        ...loading.createCampaign,
        open: true,
      });
      if (loading.createCampaign.status) {
        setTimeout(function () {
          history.push('/advertising/tokopedia/campaign');
        }, 3000);
      }
    }
  }, [loading.createCampaign]);

  useEffect(() => {
    if (
      setupKeyword.campaignEntityList.items &&
      setupKeyword.campaignEntityList.items.length > 0
    ) {
      setDisableSubmit(false);
    } else {
      setDisableSubmit(true);
    }
  }, [setupKeyword.campaignEntityList]);

  const headers = React.useMemo(
    () =>
      initColumns({
        onDelete: (value: any) => {
          handleRemoveKeywords([value.entityId]);
        },
      }),
    [],
  );

  const rows = useMemo(() => {
    const {
      items,
      page,
      limit,
      searchText: search,
      selectedCategories,
    } = setupKeyword.campaignEntityList;
    let filteredItems = items;

    if (search) {
      const lowerCaseSearch = search.toLowerCase();
      filteredItems = items.filter((item) =>
        item.keywordName.toLowerCase().includes(lowerCaseSearch),
      );
    }

    if (selectedCategories && selectedCategories.length > 0) {
      const categoryNames = selectedCategories.map((i) => i.id);
      filteredItems = filteredItems.filter((item) =>
        categoryNames.includes(item.categoryName),
      );
    }

    if (sortBy.length > 0) {
      console.log('Sort by', sortBy);
      const field = sortBy[0].id;
      const direction = sortBy[0].desc ? 'desc' : 'asc';
      filteredItems = orderBy(filteredItems, [field], [direction]);
    }

    return filteredItems.slice((page - 1) * limit, page * limit);
  }, [setupKeyword.campaignEntityList, sortBy]);

  const handleChangeSearchText = (event: any) => {
    setSearchText(event.target.value);
  };

  const handleSearchKeyUp = (event: any) => {
    if (event.key === 'Enter') {
      dispatch(
        actions.addKeyword.updateEntityListingSearch({
          searchText: event.target.value,
        }),
      );
    }
  };

  const handleSorting = (sortBy: any) => {
    setSortBy(sortBy);
  };

  const handleChangePage = (page: number) => {
    dispatch(
      actions.addKeyword.updateCampaignEntityListPagination({
        page,
        limit: setupKeyword.campaignEntityList.limit,
      }),
    );
  };

  const handleChangePageSize = (value: number) => {
    dispatch(
      actions.addKeyword.updateCampaignEntityListPagination({
        page: 1,
        limit: value,
      }),
    );
  };

  const handleSelectAll = React.useCallback((checked) => {
    dispatch(
      actions.addKeyword.campaignEntityListSelectAll({
        isAdd: checked,
      }),
    );
  }, []);

  const handleSelectItem = React.useMemo(() => {
    return (item: any, checked: boolean) => {
      dispatch(
        actions.addKeyword.campaignEntityListSelectItem({
          entityIds: [].concat(Number(getRowId(item))),
          selected: checked,
        }),
      );
    };
  }, []);

  const getRowId = React.useCallback((row) => {
    return String(row.entityId);
  }, []);

  const openAddKeywordDialog = () => {
    dispatch(
      actions.addKeyword.openAddEntityModal({ visible: true }),
    );
  };

  const handleRemoveKeywords = (keywordIds: any) => {
    dispatch(
      actions.addKeyword.removeCampaignEntity({
        entityIds: keywordIds,
      }),
    );
  };

  const handleChangeSelectedCategories = (categories: any) => {
    console.log('Change selected categories', categories);
    dispatch(
      actions.addKeyword.updateEntityListingSelectedCategory({
        categories,
      }),
    );
  };

  const handleUpdateMatchType = (value) => {
    dispatch(
      actions.addKeyword.updateEntityMatchType({
        ...value,
        entityIds: [].concat(value.keywordId),
      }),
    );
  };

  const handleUpdateBiddingPrice = (value) => {
    dispatch(
      actions.addKeyword.updateEntityBiddingPrice({
        ...value,
        entityIds: value.itemIds,
      }),
    );
  };

  const handleCloseNotification = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setNotification({ ...notification, open: false });
  };

  const handleSubmitModifyKeywords = async (data: any) => {
    const keywordIds = setupKeyword.campaignEntityList.selectedIds;
    dispatch(
      actions.addKeyword.updateEntityBiddingPrice({
        biddingPrice: Number(data.bidding_price),
        entityIds: keywordIds,
      }),
    );
    dispatch(
      actions.addKeyword.updateEntityMatchType({
        matchType: data.match_type,
        entityIds: keywordIds,
      }),
    );
    setOpenModalBulkAction({
      items: [],
      open: false,
    });
  };

  const handleOpenModifyKeyword = (selectedKeywords) => {
    const allowItems = setupKeyword.campaignEntityList.items.filter(
      (i) => selectedKeywords.indexOf(i.entityId) > -1,
    );
    setOpenModalBulkAction({
      open: true,
      items: allowItems,
    });
  };

  const buttons = [
    <ButtonUI
      colortext="#253746"
      onClick={() =>
        handleOpenModifyKeyword(
          setupKeyword.campaignEntityList.selectedIds,
        )
      }
      label="Modify keyword"
      size="small"
      colorButton="#F6F7F8"
    />,
    <ButtonUI
      colortext="#D4290D"
      onClick={() =>
        handleRemoveKeywords(
          setupKeyword.campaignEntityList.selectedIds,
        )
      }
      label="Delete"
      size="small"
      colorButton="#F6F7F8"
    />,
  ];

  return (
    <Box p={2} style={{ background: '#ffffff' }}>
      <Typography variant="h6">Setup Keywords</Typography>
      <Box pt={3}>
        <Grid container justify="space-between" alignItems="center">
          <Grid item>
            <Grid container>
              <Grid item>
                <InputUI
                  id="campaign-searchbox"
                  placeholder="Search"
                  value={searchText}
                  onChange={handleChangeSearchText}
                  onKeyUp={handleSearchKeyUp}
                  startAdornment={
                    <InputAdornment position="start">
                      <SearchIcon fontSize="small" />
                    </InputAdornment>
                  }
                  style={{ width: 320 }}
                  labelWidth={0}
                  autoComplete="off"
                ></InputUI>
              </Grid>
              <Grid item>
                <MultiSelectUI
                  prefix="Category"
                  suffix="categories"
                  items={
                    setupKeyword.campaignEntityList.categories || []
                  }
                  selectedItems={
                    setupKeyword.campaignEntityList
                      .selectedCategories || []
                  }
                  onSaveChange={handleChangeSelectedCategories}
                />
              </Grid>
            </Grid>
          </Grid>
          <Grid item>
            {setupKeyword.campaignEntityList.items.length > 0 && (
              <ButtonUI
                size="small"
                label="Add Keywords"
                colorButton={COLORS.COMMON.GRAY}
                iconLeft={<AddIcon />}
                onClick={openAddKeywordDialog}
              ></ButtonUI>
            )}
          </Grid>
        </Grid>
      </Box>
      <Box py={1}>
        {setupKeyword.campaignEntityList.selectedIds.length > 0 && (
          <BulkActionsUI
            textSelected={`${setupKeyword.campaignEntityList.selectedIds.length} keywords selected`}
            buttons={buttons}
          />
        )}
      </Box>
      <Box py={1}>
        <Grid container>
          <KeywordEditContext.Provider
            value={{
              updateBiddingPrice: handleUpdateBiddingPrice,
              updateKeywordType: handleUpdateMatchType,
            }}
          >
            <Grid item xs={12}>
              <TableUI
                columns={headers}
                rows={rows}
                onSelectItem={handleSelectItem}
                onSelectAll={handleSelectAll}
                selectedIds={
                  setupKeyword.campaignEntityList.selectedIds
                }
                getRowId={getRowId}
                onSort={handleSorting}
                resultTotal={
                  setupKeyword.campaignEntityList.items.length
                }
                page={setupKeyword.campaignEntityList.page}
                pageSize={setupKeyword.campaignEntityList.limit}
                onChangePage={handleChangePage}
                onChangePageSize={handleChangePageSize}
                noData={
                  <Box py={6}>
                    <img src={NoProduct} width="200" height="200" />
                    <Typography
                      variant="h6"
                      style={{ margin: 'auto' }}
                    >
                      Add your greatest keywords
                    </Typography>
                    <Typography
                      variant="subtitle2"
                      style={{ margin: '16px auto' }}
                    >
                      Here’s where you would add keyword to your
                      campaign. This step is an optional, you can skip
                      it and create new campaign
                    </Typography>
                    <ButtonUI
                      size="small"
                      label="Add Keywords"
                      colorButton={COLORS.COMMON.GRAY}
                      onClick={openAddKeywordDialog}
                    ></ButtonUI>
                  </Box>
                }
              />
            </Grid>
          </KeywordEditContext.Provider>
        </Grid>
      </Box>
      <Divider />
      <Box py={2}>
        <Grid
          container
          alignItems="center"
          justify="space-between"
          spacing={1}
        >
          <Grid item>
            <ButtonUI
              size="small"
              label="Back to previous step"
              colorButton={COLORS.COMMON.GRAY}
              onClick={() => props.backToPreviousScreen()}
            ></ButtonUI>
          </Grid>
          <Grid item>
            <ButtonUI
              disabled={disableSubmit}
              size="small"
              label="Create campaign"
              onClick={() => props.handleNextStep()}
            ></ButtonUI>
          </Grid>
        </Grid>
      </Box>
      {openModalBulkAction.open && (
        <ModalBulkActionKeyword
          open={openModalBulkAction.open}
          setOpenModalBulkAction={(e) =>
            handleSubmitModifyKeywords(e)
          }
          currency={setupCampaign.shopCurrency}
          onClose={() =>
            setOpenModalBulkAction((state) => ({
              ...state,
              open: false,
            }))
          }
          keywords={openModalBulkAction.items}
          updateSelected={(row) => {
            handleSelectItem(row, false);
          }}
        />
      )}
      {notification.open && (
        <Snackbar
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          open={notification.open}
          onClose={handleCloseNotification}
          autoHideDuration={3000}
        >
          <Alert
            onClose={handleCloseNotification}
            elevation={6}
            variant="filled"
            severity={notification.status ? 'success' : 'error'}
          >
            {notification.error}
          </Alert>
        </Snackbar>
      )}
    </Box>
  );
};
