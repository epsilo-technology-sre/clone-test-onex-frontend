import React from 'react';

export const KeywordEditContext = React.createContext({
  updateBiddingPrice: (value) => {
    console.info({ updateBiddingPrice: value });
  },
  updateKeywordType: (value) => {
    console.info({ updateMatchType: value });
  },
});
