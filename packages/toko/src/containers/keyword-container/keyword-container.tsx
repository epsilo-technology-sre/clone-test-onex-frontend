import { LoadingUI } from '@ep/one/src/components/common/Loading';
import { ModalAddRuleName } from '@ep/shopee/src/components/common/modal-add-rule-name';
import { ModalBulkActionKeyword } from '@ep/shopee/src/components/common/modal-bulk-actions/modal-bulk-action-keywords';
import { ModalConfirmUI } from '@ep/shopee/src/components/common/modal-confirm';
import { KeywordView } from '@ep/shopee/src/components/keywords';
import {
  activatorKeywords,
  updateKeywordBiddingPrice,
} from '@ep/toko/src/api/api';
import { actions } from '@ep/toko/src/redux/actions';
import { Snackbar } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import { differenceWith, get, isEqual, union } from 'lodash';
import React, { useEffect, useMemo } from 'react';
import { Provider, useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import { KEYWORD_STATUS, PRODUCT_STATUS } from '../../constants';
import {
  GET_KEYWORDS,
  SET_FILTER_KEYWORD_STATUS,
  SET_PAGINATION,
  SET_SEARCH_KEYWORD,
  UPDATE_SELECTED_KEYWORDS,
} from '../../redux/actions';
import { rootSaga } from '../../redux/create-campaign/actions';
import { reducer } from '../../redux/create-campaign/reducers';
import { AddKeywordContainer } from '../add-keyword-container/add-keyword-exist-campaign-container';
import { AddRuleKeywordContainer } from '../rules/add-rule-keyword-container';
import { CreateRuleKeyword } from '../rules/keyword-create';
import { ListRuleKeyword } from '../rules/keyword-list';
import { KeywordRuleLog } from '../rules/keyword-rule-log';
import { KEYWORD_COLUMNS } from './keyword-columns';

export const KeywordContainer = () => {
  const addKeywordStore = makeStore();

  const ref = React.useRef({
    selectedKeywords: null,
    selectedProducts: [],
    keywords: null,
    pagination: null,
    sortBy: [],
  });
  const {
    keywords,
    pagination,
    globalFilters,
    selectedKeywords,
    selectedProducts,
    selectedCampaigns,
    loading,
    searchKeyword,
    filterKeywordStatus,
  } = useSelector(
    ({
      pagination,
      globalFilters,
      keywords,
      selectedKeywords,
      selectedProducts,
      selectedCampaigns,
      loading,
      searchKeyword,
      filterKeywordStatus,
    }: any) => {
      ref.current = {
        ...ref.current,
        selectedProducts,
        selectedKeywords,
        keywords,
        pagination,
      };
      return {
        pagination,
        globalFilters,
        keywords,
        selectedKeywords,
        selectedProducts,
        loading,
        searchKeyword,
        filterKeywordStatus,
        selectedCampaigns,
      };
    },
  );
  const [isBulkSubmitting, setBulkSubmitting] = React.useState(false);
  const [openModalRule, setOpenModalRule] = React.useState(false);
  const [newRule, setNewRules] = React.useState<any>({
    open: false,
  });
  const [addKeywordModal, setAddKeywordModal] = React.useState({
    isOpen: false,
    products: [],
    shopId: null,
  });

  const [ruleLog, setRuleLog] = React.useState({
    isOpen: false,
    shopEid: 0,
    parentName: '',
    itemId: 0,
    itemName: '',
    currency: '',
  });

  const [deleteRulesModal, setDeleteRulesModal] = React.useState({
    open: false,
    items: [],
  });

  const [notification, setNotification] = React.useState({
    open: false,
  });
  const [showActivator, setShowActivator] = React.useState({
    show: false,
    focusKeyword: null,
  });
  const [
    openModalBulkAction,
    setOpenModalBulkAction,
  ] = React.useState({
    open: false,
    items: [],
    currency: '',
  });

  const [createRuleKeyword, setCreateRuleKeyword] = React.useState({
    isOpen: false,
    keywords: [],
    products: [],
    shopId: null,
    currency: 'VND',
  });

  const [listKeywordRules, setListKeywordRules] = React.useState({
    isOpen: false,
    allowUpdate: true,
    shopEid: 0,
    keywordId: 0,
    keywordName: '',
    productId: 0,
    productName: '',
    currency: '',
    matchType: '',
  });

  const statusList = useMemo(() => {
    const items = ['All'];
    for (const property in KEYWORD_STATUS) {
      items.push(KEYWORD_STATUS[property]);
    }
    return items;
  }, []);

  useEffect(() => {
    if (globalFilters && globalFilters.shops?.length > 0) {
      getKeywordData();
    }
  }, [globalFilters.timeRange, globalFilters.updateCount]);

  const dispatch = useDispatch();

  const getKeywords = (payload: any) =>
    dispatch(GET_KEYWORDS.START(payload));
  const updateSelectedKeywords = (payload: any) =>
    dispatch(UPDATE_SELECTED_KEYWORDS(payload));

  const updatePagination = (paging: any) =>
    dispatch(SET_PAGINATION({ pagination: paging }));

  const updateSearchKeyword = (query: any) =>
    dispatch(SET_SEARCH_KEYWORD({ searchKeyword: query }));

  const updateFilterKeywordStatus = (status: any) =>
    dispatch(
      SET_FILTER_KEYWORD_STATUS({ filterKeywordStatus: status }),
    );

  const handleSelectAll = React.useCallback((checked) => {
    if (
      checked &&
      get(ref.current, 'selectedKeywords.length', 0) === 0
    ) {
      updateSelectedKeywords(
        (ref.current.selectedKeywords || []).concat(
          ref.current.keywords,
        ),
      );
    } else {
      updateSelectedKeywords([]);
    }
  }, []);
  const getRowId = React.useCallback((row) => {
    return String(row.keyword_id);
  }, []);

  const handleSelectItem = React.useMemo(() => {
    return (item: any, checked: boolean) => {
      if (checked) {
        updateSelectedKeywords(
          (ref.current.selectedKeywords || []).concat(item),
        );
      } else {
        updateSelectedKeywords(
          (ref.current.selectedKeywords || []).filter(
            (c: any) => getRowId(c) !== getRowId(item),
          ),
        );
      }
    };
  }, [selectedKeywords]);

  const selectedIds = React.useMemo(() => {
    return (selectedKeywords || []).map(getRowId);
  }, [selectedKeywords]);

  const getKeywordData = (argParams?: any) => {
    if (globalFilters) {
      let productIds = '';
      if (ref.current.selectedProducts) {
        productIds = ref.current.selectedProducts
          .map((item: any) => item.campaign_product_id)
          .join(',');
      }

      const urlParams = new URLSearchParams(location.search);
      let shopIds = urlParams.get('shopId');
      if (globalFilters.shops) {
        shopIds = globalFilters.shops
          .map((item: any) => item.id)
          .join(',');
      }

      const params: any = {
        shop_eids: shopIds || 4110,
        from: globalFilters.timeRange.start,
        to: globalFilters.timeRange.end,
        campaign_eids:
          selectedCampaigns.length > 0
            ? selectedCampaigns.map((i) => i.campaign_eid)
            : undefined,
        limit: ref.current.pagination.limit,
        page: ref.current.pagination.page,
        value_filter: searchKeyword,
        status: filterKeywordStatus,
        ...argParams,
      };

      if (ref.current.sortBy.length > 0) {
        params.order_by = ref.current.sortBy[0].id;
        params.order_mode = ref.current.sortBy[0].desc
          ? 'desc'
          : 'asc';
      }
      if (params.status === 'All') {
        params.status = '';
      }

      getKeywords(params);
    }
  };

  const handleViewRuleLog = (row) => {
    console.log('View keyword rule', row);
    setRuleLog({
      isOpen: true,
      shopEid: row.shop_eid,
      parentName: row.product_name,
      itemId: row.keyword_id,
      itemName: row.keyword_name,
      currency: row.currency,
    });
  };

  const handleChangeSearchText = (value: any) => {
    updateSearchKeyword(value);
    updatePagination({
      page: 1,
      limit: pagination.limit,
      item_count: pagination.item_count,
      page_count: pagination.page_count,
    });
    getKeywordData({ value_filter: value });
  };

  const handleChangeStatus = (value: any) => {
    updatePagination({
      page: 1,
      limit: pagination.limit,
      item_count: pagination.item_count,
      page_count: pagination.page_count,
    });
    updateFilterKeywordStatus(value);
    getKeywordData({ status: value });
  };

  const handleSorting = (sortBy: any) => {
    const isDifference =
      differenceWith(sortBy, ref.current.sortBy, isEqual).length > 0;
    if (isDifference) {
      ref.current.sortBy = sortBy;
      getKeywordData();
    }
  };

  const handleChangePage = (page: number) => {
    updatePagination({
      page: page,
      limit: pagination.limit,
      item_count: pagination.item_count,
      page_count: pagination.page_count,
    });
    getKeywordData();
  };

  const handleChangePageSize = (value: number) => {
    updatePagination({
      page: 1,
      limit: value,
      item_count: pagination.item_count,
      page_count: pagination.page_count,
    });
    getKeywordData();
  };

  const handleOpenAddKeyword = (products: any) => {
    console.log('Save add keywords', products);
    setAddKeywordModal({
      isOpen: true,
      shopId: null,
      products: [],
    });
  };

  const handleCloseDialog = () => {
    setAddKeywordModal((state) => ({
      ...state,
      isOpen: false,
    }));
  };

  const handleAddingSuccess = () => {
    updatePagination({
      page: 1,
      limit: pagination.limit,
      item_count: pagination.item_count,
      page_count: pagination.page_count,
    });
    getKeywordData();
    handleCloseDialog();
  };

  const handleActivator = (type: string, keywords: any) => {
    const isAllowed = keywords.every((i) => !i._isDisabled);
    if (isAllowed) {
      const activator: any = {
        active: {
          labelSubmit: 'Activate',
          title: 'Activate Keyword(s)?',
          content: 'Activating Keyword will resume its bidding ads.',
          status: 1,
        },
        deactive: {
          labelSubmit: 'Deactivate',
          title: 'Deactivate Keywords?',
          content: 'Deactivating Keyword will pause its bidding ads.',
          status: 0,
        },
      };

      setShowActivator({
        ...showActivator,
        ...activator[type],
        focusKeyword: keywords,
        show: true,
      });
    } else {
      toast.error('Can not modify ended keywords');
    }
  };

  const handleCloseNotification = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setNotification({ ...notification, open: false });
  };

  const handleSubmit = async (keywords: any[]) => {
    const shopEids = keywords.map((item) => item.shop_eid);
    const keywordIds = keywords.map((item) => item.keyword_id);
    setShowActivator({ ...showActivator, show: false });
    try {
      setBulkSubmitting(true);
      const res = await activatorKeywords({
        shop_eids: shopEids,
        campaign_keyword_ids: keywordIds,
        status: Number(showActivator.status),
      });
      setNotification({ ...notification, ...res, open: true });
      setBulkSubmitting(false);
      if (res.success) {
        await getKeywordData();
      }
    } catch (error) {
      setBulkSubmitting(false);
      setNotification({
        ...notification,
        message: error.message,
        open: true,
        success: false,
      });
    }
  };

  const handleSubmitModifyKeywords = async (data: any) => {
    const keywordIds = selectedKeywords.map(
      (item: any) => item.keyword_id,
    );
    const shopEids = selectedKeywords.map(
      (item: any) => item.shop_eid,
    );
    return handleUpdateKeyword({
      shopIds: [...new Set(shopEids)],
      keywordIds: keywordIds,
      biddingPrice: data.bidding_price,
      matchType: data.match_type,
    });
  };

  const handleUpdateKeyword = async (params: any) => {
    try {
      setBulkSubmitting(true);
      let res = await updateKeywordBiddingPrice({
        shop_eids: params.shopIds,
        campaign_keyword_ids: params.keywordIds,
        max_cpc: Number(params.biddingPrice),
      });

      // res = await updateKeywordMatchType({
      //   shop_eids: params.shopIds,
      //   campaign_keyword_ids: params.keywordIds,
      //   match_type:
      //     params.matchType === MATCH_TYPE.EXACT_MATCH
      //       ? MATCH_TYPE_POST_VALUE.EXACT_MATCH
      //       : MATCH_TYPE_POST_VALUE.BROAD_MATCH,
      // });

      setBulkSubmitting(false);
      setNotification({ ...notification, ...res, open: true });
      if (res.success) {
        getKeywordData();
        setOpenModalBulkAction({
          ...openModalBulkAction,
          open: false,
        });
      }
    } catch (error) {
      setBulkSubmitting(false);
      notification.message = [].concat(error.message).join('\n');
      setNotification({
        ...notification,
        open: true,
        success: false,
      });
    }
  };

  const handleOpenModifyBudget = () => {
    const allowItems = selectedKeywords.filter((i) => !i._isDisabled);
    if (allowItems.length > 0) {
      const isDiffCurrency = allowItems.some(
        (i) => i.currency !== allowItems[0].currency,
      );
      if (isDiffCurrency) {
        toast.error('Selected keywords have the difference currency');
      } else {
        setOpenModalBulkAction({
          open: true,
          items: allowItems,
          currency: allowItems[0].currency,
        });
      }
    } else {
      toast.error('Can not modify ended keywords');
    }
  };

  const handleGetSuggestBiddingPrice = async (params: any) => {
    // if (params.shopId && params.keywordId) {
    //   let result = await getSuggestBiddingPrice({
    //     shop_eids: [params.shopId],
    //     keyword_id: params.keywordId,
    //   });
    //   if (result.success) {
    //     return result.data.suggest_bidding_price;
    //   } else {
    //     return 0;
    //   }
    // }
    return 0;
  };

  const showStatusPopup = (keyword: any, status: boolean) => {
    handleActivator(
      status ? 'deactive' : 'active',
      [].concat(keyword),
    );
  };

  const onSubmitRule = (rule: any) => {
    const conditionJson = JSON.parse(rule.conditionJson);
    const changeDate =
      rule.fromDate === rule.timelineFrom &&
      rule.toDate === rule.timelineTo;
    const condition = rule.conditions.map(
      ({ value, period, metric_code, operator_code }) => ({
        value,
        period,
        metric_code,
        operator_code,
      }),
    );
    const changeCondition =
      JSON.stringify(conditionJson) === JSON.stringify(condition);
    if (changeCondition && changeDate) {
      dispatch(
        actions.addExistingRule({
          shopEid: rule.shopEid,
          featureCode: 'M_LZD_SS',
          objectType: rule.objectType,
          objectEids: rule.objectEids,
          ruleId: rule.ruleId,
          callback: (rs) => {
            setNotification({
              open: true,
              message: rs.data,
              success: rs.success,
            });
            if (rs.success) {
              getKeywordData();
              getRuleList();
            }
          },
        }),
      );
    } else {
      setNewRules({
        open: true,
        rule: {
          ...rule,
          ruleName: rule.ruleName,
          listRules: rule.conditions,
        },
      });
    }
    setOpenModalRule(false);
  };

  const createNewRule = (rule: any) => {
    const postRule = {
      ...newRule.rule,
      ruleName: rule.ruleName,
    };

    dispatch(
      actions.createKeywordRule({
        rule: postRule,
        keywords: createRuleKeyword.keywords,
        shopId: postRule.shopEid,
        callback: (rs) => {
          setNotification({
            open: true,
            message: rs.data,
            success: rs.success,
          });
          if (rs.success) {
            getKeywordData();
          }
        },
      }),
    );

    setNewRules({
      ...newRule,
      open: false,
    });
  };

  const handleOpenModalAddRule = () => {
    const allowItems = selectedKeywords.filter((i) => !i._isDisabled);
    if (allowItems.length > 0) {
      let shopEid = allowItems.map((p) => p.shop_eid);
      shopEid = [...new Set(shopEid)];

      if (shopEid.length > 1) {
        setNotification({
          open: true,
          success: false,
          message: 'Please choose keywords in one shop',
        });
      } else {
        const keywords = allowItems.map((row) => ({
          keyword_id: row.keyword_id,
          keyword_name: row.keyword_name,
          currency: row.currency,
          matchType: row.match_type,
        }));
        const products = allowItems.map((row) => ({
          product_id: row.campaign_product_id,
          product_name: row.product_name,
        }));
        const matchType = allowItems.filter(
          (item) => item.match_type === 'broad_match',
        );

        setCreateRuleKeyword({
          ...createRuleKeyword,
          shopId: shopEid[0],
          keywords,
          products,
          currency: allowItems[0].currency,
          matchType:
            matchType.length > 0 ? 'BROAD_MATCH' : 'EXACT_MATCH',
        });
        setOpenModalRule(true);
      }
    } else {
      toast.error('Can not add rule with ended keywords');
    }
  };

  const handleRemoveSelectedKeywords = (items: any) => {
    const keywords = items.map((row) => ({
      keyword_id: row.keyword_id,
      keyword_name: row.keyword_name,
      currency: row.currency,
      matchType: row.match_type,
    }));
    const products = items.map((row) => ({
      product_id: row.campaign_product_id,
      product_name: row.product_name,
    }));
    const matchType = items.filter(
      (item) => item.match_type === 'broad_match',
    );

    setCreateRuleKeyword({
      ...createRuleKeyword,
      keywords,
      products,
      matchType: matchType.length > 0 ? 'BROAD_MATCH' : 'EXACT_MATCH',
    });

    updateSelectedKeywords(items);
  };

  const handleAddNewRule = (row) => {
    setCreateRuleKeyword({
      ...createRuleKeyword,
      shopId: row.shop_eid,
      keywords: [
        {
          keyword_id: row.keyword_id,
          keyword_name: row.keyword_name,
          currency: row.currency,
          matchType: row.match_type,
        },
      ],
      products: [
        {
          product_id: row.campaign_product_id,
          product_name: row.product_name,
        },
      ],
      currency: row.currency,
      matchType: row.match_type.toUpperCase(),
    });

    setOpenModalRule(true);
  };

  const handleAddRuleSuccess = () => {
    getKeywordData();
    getRuleList();
    setCreateRuleKeyword((state) => ({
      ...state,
      isOpen: false,
    }));
  };

  const handleCloseAddRule = () => {
    setCreateRuleKeyword((state) => ({
      ...state,
      isOpen: false,
    }));
  };

  const getRuleList = () => {
    if (listKeywordRules.isOpen) {
      dispatch(
        actions.getKeywordRules({
          keywordId: listKeywordRules.keywordId,
          shopEid: listKeywordRules.shopEid,
        }),
      );
    }
  };

  const handleViewRule = (row) => {
    const allowStatus = [
      PRODUCT_STATUS.RUNNING,
      PRODUCT_STATUS.PAUSED,
      PRODUCT_STATUS.SCHEDULED,
    ];
    setListKeywordRules({
      isOpen: true,
      allowUpdate: allowStatus.includes(row.status),
      shopEid: row.shop_eid,
      keywordId: row.keyword_id,
      keywordName: row.keyword_name,
      productId: row.campaign_product_id,
      productName: row.product_name,
      currency: row.currency,
      matchType: row.match_type,
    });
  };

  const handleDeleteRules = () => {
    setDeleteRulesModal({
      open: true,
      items: selectedKeywords,
    });
  };

  const onSubmitDeleteRules = () => {
    setDeleteRulesModal({ ...deleteRulesModal, open: false });
    dispatch(
      actions.deleteAllKeywordRules({
        featureCode: 'M_TOK_PA',
        shopEids: union(
          deleteRulesModal.items.map((i) => i.shop_eid),
        ),
        itemIds: deleteRulesModal.items.map((i) => i.keyword_id),
        callback: (rs) => {
          setNotification({
            open: true,
            message: rs.data,
            success: rs.success,
          });
          if (rs.success) {
            getKeywordData();
          }
        },
      }),
    );
  };

  const headers = React.useMemo(() => {
    const columns = [...KEYWORD_COLUMNS];
    columns[0].onCellClick = showStatusPopup;

    const ruleCol = columns.find((i) => i.id === 'rule');
    ruleCol.onCellClick = (row: any) => {
      if (row.ruleCount) {
        handleViewRule(row);
      } else {
        handleAddNewRule(row);
      }
    };

    const logCol = columns.find((i) => i.id === 'ruleLog');
    logCol.onCellClick = (row: any) => {
      handleViewRuleLog(row);
    };

    return columns;
  }, []);

  const handleAddNewKeywords = () => {
    setAddKeywordModal({
      isOpen: false,
      shopId: null,
      products: [],
    });
    getKeywordData();
  };

  return (
    <React.Fragment>
      <KeywordView
        openModalAddrule={handleOpenModalAddRule}
        tableHeaders={headers}
        keywords={keywords}
        pagination={pagination}
        searchText={searchKeyword}
        statusList={statusList}
        selectedStatus={filterKeywordStatus}
        selectedIds={selectedIds}
        selectedKeywords={selectedKeywords}
        handleSelectItem={handleSelectItem}
        handleSelectAll={handleSelectAll}
        getRowId={getRowId}
        onSearch={handleChangeSearchText}
        onChangeStatus={handleChangeStatus}
        onSorting={handleSorting}
        onChangePage={handleChangePage}
        onChangePageSize={handleChangePageSize}
        onAddKeyword={handleOpenAddKeyword}
        handleActivator={handleActivator}
        handleUpdateKeyword={handleUpdateKeyword}
        handleOpenModifyBudget={handleOpenModifyBudget}
        handleGetSuggestBiddingPrice={handleGetSuggestBiddingPrice}
        handleDeleteRules={handleDeleteRules}
      />
      {showActivator.show && (
        <ModalConfirmUI
          open={showActivator.show}
          onSubmit={() =>
            handleSubmit([].concat(showActivator.focusKeyword))
          }
          onClose={() =>
            setShowActivator({ ...showActivator, show: false })
          }
          labelSubmit={showActivator.labelSubmit}
          title={showActivator.title}
          content={showActivator.content}
        />
      )}
      {deleteRulesModal.open && (
        <ModalConfirmUI
          open={deleteRulesModal.open}
          onSubmit={onSubmitDeleteRules}
          onClose={() =>
            setDeleteRulesModal({ ...deleteRulesModal, open: false })
          }
          labelSubmit="OK"
          title="Delete all rules?"
          content="All rules that you added on these keywords will be deleted."
        />
      )}
      {openModalBulkAction.open && (
        <ModalBulkActionKeyword
          open={openModalBulkAction.open}
          hideMatchType={true}
          setOpenModalBulkAction={(e) =>
            handleSubmitModifyKeywords(e)
          }
          currency={openModalBulkAction.currency}
          onClose={() =>
            setOpenModalBulkAction({ ...showActivator, open: false })
          }
          keywords={openModalBulkAction.items}
          updateSelected={(rows: any) => {
            setOpenModalBulkAction({
              ...openModalBulkAction,
              items: rows,
            });
            updateSelectedKeywords(rows);
          }}
        />
      )}
      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={notification.open}
        onClose={handleCloseNotification}
        autoHideDuration={3000}
      >
        <Alert
          onClose={handleCloseNotification}
          elevation={6}
          variant="filled"
          severity={notification.success ? 'success' : 'error'}
        >
          {notification.message}
        </Alert>
      </Snackbar>
      <LoadingUI loading={loading || isBulkSubmitting} />
      <Provider store={addKeywordStore}>
        <AddKeywordContainer
          addType="AddKeyword"
          isOpen={addKeywordModal.isOpen}
          onClose={handleCloseDialog}
          onAddKeyword={handleAddNewKeywords}
        ></AddKeywordContainer>
      </Provider>
      <AddRuleKeywordContainer
        open={openModalRule}
        onSubmit={onSubmitRule}
        handleCreate={() => {
          setOpenModalRule(false);
          setCreateRuleKeyword({
            ...createRuleKeyword,
            isOpen: true,
          });
        }}
        onClose={(e: any) => setOpenModalRule(e)}
        keywords={createRuleKeyword.keywords}
        products={[]}
        setKeywords={handleRemoveSelectedKeywords}
        selectedKeywords={selectedKeywords}
        currency={createRuleKeyword.currency}
        shopEid={createRuleKeyword.shopId}
        matchType={createRuleKeyword.matchType}
        // matchType={matchType.length > 0 ? 'BROAD_MATCH' : 'EXACT_MATCH'}
      />
      <ModalAddRuleName
        open={newRule.open}
        onSubmit={createNewRule}
        onClose={(e: any) => setNewRules({ ...newRule, open: e })}
      />
      <CreateRuleKeyword
        isOpen={createRuleKeyword.isOpen}
        keywords={createRuleKeyword.keywords}
        shopId={createRuleKeyword.shopId}
        products={[]}
        onSubmitSuccess={handleAddRuleSuccess}
        onClose={handleCloseAddRule}
        onOpenExistingRule={() => {
          setOpenModalRule(true);
          setCreateRuleKeyword({
            ...createRuleKeyword,
            isOpen: false,
          });
        }}
      />
      <ListRuleKeyword
        allowUpdate={listKeywordRules.allowUpdate}
        isOpen={listKeywordRules.isOpen}
        shopEid={listKeywordRules.shopEid}
        keywordId={listKeywordRules.keywordId}
        keywordName={listKeywordRules.keywordName}
        productId={listKeywordRules.productId}
        productName={listKeywordRules.productName}
        currency={listKeywordRules.currency}
        matchType={listKeywordRules.matchType}
        onClose={() => {
          setListKeywordRules((state) => ({
            ...state,
            isOpen: false,
          }));
        }}
        onUpdateSuccess={getKeywordData}
        onAddRule={(keyword) => handleAddNewRule(keyword)}
      />
      <KeywordRuleLog
        open={ruleLog.isOpen}
        shopEid={ruleLog.shopEid}
        parentName={ruleLog.parentName}
        itemId={ruleLog.itemId}
        itemName={ruleLog.itemName}
        onClose={() => {
          setRuleLog((state) => ({
            ...state,
            isOpen: false,
          }));
        }}
      ></KeywordRuleLog>
    </React.Fragment>
  );
};

function makeStore(): any {
  if (makeStore._store) return makeStore._store;

  const sagaMiddleware = createSagaMiddleware();
  let composeEnhancers = composeWithDevTools({
    name: 'toko/add-keyword',
  });

  const store = createStore(
    reducer,
    composeEnhancers(applyMiddleware(sagaMiddleware)),
  );

  sagaMiddleware.run(rootSaga);

  makeStore._store = store;

  return store;
}
