import { EditButton } from '@ep/shopee/src/components/common/edit-button';
import {
  ButtonCell,
  CellText,
  EllipsisCell,
  LogCell,
  PercentCell,
  StatusCell,
  SubjectCell,
  SwitchCell,
} from '@ep/shopee/src/components/common/table-cell';
import { makeLazyCell } from '@ep/shopee/src/components/common/table-cell/lazy-cell';
import { KeywordBudgetEditorPopover } from '@ep/shopee/src/components/edit-form/keyword-budget-editor-popover';
import React from 'react';
import { MATCH_TYPE_LABEL, PRODUCT_STATUS } from '../../constants';

export const KEYWORD_COLUMNS = [
  {
    Header: ' ',
    id: 'switch',
    accessor: (row: any) => ({
      isOn: row.child_status.campaign_product_keyword_state,
      isDisabled:
        row.campaign_product_status === 'ended' ||
        row.campaign_product_status === 'closed',
      id: row.campaign_product_keyword_id,
      row,
    }),
    sticky: 'left',
    width: 70,
    disableSortBy: true,
    Cell: SwitchCell,
  },
  {
    Header: 'Keyword',
    accessor: (row: any) => ({ value: row.keyword_name }),
    sticky: 'left',
    width: 200,
    disableSortBy: true,
    Cell: EllipsisCell,
  },
  {
    Header: 'Status',
    id: 'status',
    sticky: 'left',
    width: 70,
    disableSortBy: true,
    accessor: (row: any) => {
      return { type: row.status };
    },
    Cell: StatusCell,
  },
  // {
  //   Header: 'Product',
  //   id: 'product_name',
  //   accessor: (row: any) => ({ value: row.product_name }),
  //   width: 250,
  //   Cell: EllipsisCell,
  // },
  {
    Header: 'Campaign',
    id: 'campaign_name',
    accessor: (row: any) => ({
      name: row.campaign_name,
      code: row.shop_name,
    }),
    width: 200,
    Cell: SubjectCell,
  },
  {
    Header: 'Impression',
    id: 'sum_total_views',
    accessor: (row: any) => ({ number: row.sum_total_views }),
    Cell: PercentCell,
  },
  {
    Header: 'Click',
    id: 'sum_click',
    accessor: (row: any) => ({ number: row.sum_click }),
    Cell: makeLazyCell(PercentCell),
  },
  {
    Header: 'Direct Ads item sold',
    id: 'sum_direct_item_sold',
    accessor: (row: any) => ({ number: row.sum_direct_item_sold }),
    Cell: PercentCell,
    width: 175,
  },
  {
    Header: 'Ads item sold',
    id: 'sum_item_sold',
    accessor: (row: any) => ({ number: row.sum_item_sold }),
    Cell: PercentCell,
    width: 130,
  },
  // {
  //   Header: 'Ads GMV',
  //   id: 'sum_gmv',
  //   accessor: (row: any) => ({
  //     number: row.sum_gmv,
  //     currency: row.currency,
  //   }),
  //   Cell: PercentCell,
  //   alwaysEnable: true,
  //   width: 100,
  // },
  // {
  //   Header: 'Direct Ads GMV',
  //   id: 'sum_direct_gmv',
  //   accessor: (row: any) => ({
  //     number: row.sum_direct_gmv,
  //     currency: row.currency,
  //   }),
  //   Cell: PercentCell,
  //   alwaysEnable: true,
  //   width: 150,
  // },
  {
    Header: 'Cost',
    id: 'sum_cost',
    accessor: (row: any) => ({
      number: row.sum_cost,
      currency: row.currency,
    }),
    Cell: PercentCell,
    alwaysEnable: true,
  },
  {
    Header: 'CPC',
    id: 'cpc',
    accessor: (row: any) => ({
      number: row.cpc,
      currency: row.currency,
    }),
    Cell: makeLazyCell(PercentCell),
    alwaysEnable: true,
    disableSortBy: true,
  },
  {
    Header: 'Bid price',
    id: 'max_cpc',
    accessor: (row: any) => ({
      number: row.max_cpc,
      currency: row.currency,
      editor: row._isDisabled ? null : (
        <KeywordBudgetEditorPopover
          keyword={row}
          currency={row.currency}
          bidding_price={row.max_cpc}
          triggerElem={<EditButton />}
        />
      ),
    }),
    Cell: PercentCell,
    alwaysEnable: true,
    disableSortBy: true,
  },
  {
    Header: 'Match type',
    disableSortBy: true,
    accessor: (row: any) => ({
      text: MATCH_TYPE_LABEL[row.match_type],
      currency: row.currency,
      // editor: row._isDisabled ? null : (
      //   <KeywordTypeEditorPopover
      //     keyword={row}
      //     triggerElem={<EditButton />}
      //   />
      // ),
    }),
    Cell: makeLazyCell(CellText),
  },
  {
    Header: 'Rule',
    id: 'rule',
    accessor: (row: any) => {
      let label = '';
      if (row.ruleCount) {
        label = row.ruleCount + ' rules';
      } else {
        const allowStatus = [
          PRODUCT_STATUS.RUNNING,
          PRODUCT_STATUS.PAUSED,
          PRODUCT_STATUS.SCHEDULED,
        ];
        if (allowStatus.includes(row.status)) {
          label = 'Add rule';
        }
      }

      return {
        ...row,
        label,
      };
    },
    Cell: makeLazyCell(ButtonCell),
    disableSortBy: true,
    width: 95,
    alwaysEnable: true,
  },
  {
    Header: '',
    id: 'ruleLog',
    accessor: (row: any) => row,
    Cell: makeLazyCell(LogCell),
    disableSortBy: true,
    width: 70,
    alwaysEnable: true,
  },
];
