import { AddCustomKeyword } from '@ep/shopee/src/components/add-product-keyword/add-custom-keyword';
import { KeywordTableAction } from '@ep/shopee/src/components/add-product-keyword/keyword-table-action';
import {
  ButtonTextUI,
  ButtonUI,
} from '@ep/shopee/src/components/common/button';
import { ModifyKeyWords } from '@ep/shopee/src/components/common/modify-keywords';
import { TableUI } from '@ep/shopee/src/components/common/table';
import { ShopCampaignSelector } from '@ep/shopee/src/components/create-campaign/shop-campaign-selector';
import NoProduct from '@ep/shopee/src/images/no-product.svg';
import { actions } from '@ep/toko/src/redux/create-campaign/actions';
import { CreateCampaignState } from '@ep/toko/src/redux/create-campaign/reducers';
import {
  Box,
  Divider,
  Grid,
  makeStyles,
  Typography,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { differenceWith, get, isEqual } from 'lodash';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { KeywordBucket } from '../../components/keyword-bucket';
import { CURRENCY_LIMITATION } from '../../constants';
import { initColumns } from './add-keyword-columns';

const useStyle = makeStyles({
  modifyBudgetWrapper: {
    marginBottom: 16,
    padding: 5,
    border: '2px solid #E4E7E9',
    fontSize: '11px',

    '&.disabled': {
      background: '#F6F7F8',
    },
  },
});

export const AddKeywordContainer = (props: any) => {
  const classes = useStyle();
  const dispatch = useDispatch();
  const [selectedShop, setSelectedShop] = useState();
  const [selectedCampaign, setSelectedCampaign] = useState();
  const [sortBy, setSortBy] = useState([]);

  const state = useSelector(
    ({
      setupCampaign: { shopCurrency, shop, timeline },
      setupKeyword: { shops, campaigns, entityList, entityBucket },
      setupProduct: { campaignProductList },
    }: CreateCampaignState) => {
      const keywords = get(entityList, 'items', []).map((item) => {
        return {
          ...item,
          _isDisabled: item.added,
        };
      });
      return {
        shops,
        shopCurrency,
        timeline,
        shop,
        campaigns,
        categories: get(entityList, 'categories', []),
        keywords,
        selectedEntityIds: get(entityList, 'selectedIds', []),
        entityBucket,
        selectedCategories: get(entityList, 'selectedCategories', []),
        search: get(entityList, 'searchText', ''),
        pagination: {
          page: get(entityList, 'page', 1),
          itemCount: get(entityList, 'itemCount', 0),
          limit: get(entityList, 'limit', 10),
        },
        campaignProductList,
      };
    },
  );

  const {
    shops = [],
    shop: shopId,
    timeline,
    shopCurrency = 'VND',
    campaigns = [],
    categories = [],
    keywords = [],
    selectedEntityIds = [],
    entityBucket: entityBucket,
    selectedCategories,
    search,
    pagination = {
      page: 1,
      limit: 10,
      itemCount: 0,
    },
  } = state;

  useEffect(() => {
    handleGetKeyword({ shopEid: shopId });

    dispatch(
      actions.addKeyword.getShopSuggestEntities({
        shopEid: shopId,
        productSids: state.campaignProductList.items.map(
          (i) => i.productSId,
        ),
      }),
    );
    dispatch(
      actions.addKeyword.getEntityCategoryList({ shopEid: shopId }),
    );
  }, []);

  const [isShowCustomKeyword, setIsShowCustomKeyword] = useState(
    false,
  );

  const handleGetKeyword = (params: any = {}) => {
    const categoryNames = selectedCategories
      .map((i) => i.text)
      .join(',');
  };

  const addKeywordsToBucket = React.useCallback(
    (entityIdList: any) => {
      console.info({ keywordList: entityIdList });
      dispatch(
        actions.addKeyword.addEntityAddToBucket({
          entityIds: entityIdList,
          isAdded: true,
        }),
      );
    },
    [],
  );

  const handleSelectAll = React.useCallback((checked) => {
    dispatch(
      actions.addKeyword.addEntitySelectAll({ isAdd: checked }),
    );
  }, []);

  const handleSelectItem = React.useMemo(() => {
    return (item: any, checked: boolean) => {
      dispatch(
        actions.addKeyword.addEntitySelectEntity({
          entityIds: [Number(getRowId(item))],
          selected: checked,
        }),
      );
    };
  }, []);

  const getRowId = React.useCallback((row) => {
    return String(row.entityId);
  }, []);

  const handleRemoveKeywords = (keywordList: any) => {
    dispatch(
      actions.addKeyword.addEntityAddToBucket({
        entityIds: keywordList.map((i) => i.entityId),
        isAdded: false,
      }),
    );
  };

  const headers = React.useMemo(
    () =>
      initColumns({
        onAddToBucket: (value: any) => {
          addKeywordsToBucket(value.entityId);
        },
      }),
    [],
  );

  const handleSearch = (value: any) => {
    dispatch(
      actions.addKeyword.updateAddEntitySearch({
        searchText: value,
        currency: shopCurrency,
      }),
    );

    handleGetKeyword({
      searchText: value,
    });
  };

  const handleChangeCategories = (value: any) => {
    dispatch(
      actions.addKeyword.updateAddEntitySelectedCategory({
        categories: value,
      }),
    );

    handleGetKeyword({
      page: 1,
      categoryNames: value.map((i) => i.text).join(','),
    });
  };

  const handleSorting = (sort: any) => {
    const isDifference =
      differenceWith(sort, sortBy, isEqual).length > 0;
    if (isDifference) {
      console.log('handle sorting', sort);
      setSortBy(sort);
      handleGetKeyword({
        orderBy: sort[0]?.id,
        orderMode: sort[0]?.desc ? 'desc' : 'asc',
      });
    }
  };

  const handleChangePage = (page: number) => {
    dispatch(
      actions.addKeyword.addEntityUpdateEntityPagination({
        pagination: { currentPage: page, pageSize: pagination.limit },
      }),
    );
  };

  const handleChangePageSize = (value: number) => {
    dispatch(
      actions.addKeyword.addEntityUpdateEntityPagination({
        pagination: { pageSize: value, currentPage: 1 },
      }),
    );
  };

  const handleSave = () => {
    dispatch(actions.addKeyword.addBucketToCampaignEntity());
    dispatch(actions.addKeyword.resetAddEntity());
  };

  const handleCancel = () => {
    dispatch(
      actions.addKeyword.openAddEntityModal({ visible: false }),
    );
    dispatch(actions.addKeyword.resetAddEntity());
  };

  const handleAddCustomKeywords = (keywords: string[]) => {
    dispatch(
      actions.addKeyword.addCustomEntities({
        keywordNames: keywords,
        currency: shopCurrency,
      }),
    );
  };

  const handleModifySelectedKeywords = (payload: {
    biddingPrice: number;
    matchType: string;
  }) => {
    dispatch(
      actions.addKeyword.addEntityUpdateEntityBidPrice(payload),
    );
  };

  return (
    <Box p={2} style={{ background: '#ffffff' }}>
      <Box mb={1}>
        <Grid container justify="space-between">
          <Grid item>
            <Typography variant="h6">Add Keywords</Typography>
          </Grid>
          <Grid item>
            <CloseIcon
              style={{ cursor: 'pointer' }}
              onClick={handleCancel}
            />
          </Grid>
        </Grid>
      </Box>
      <Grid container spacing={1}>
        <Grid item xs={9}>
          {!shopId && (
            <Box mb={2}>
              <Box mb={2}>
                <ShopCampaignSelector
                  shops={shops}
                  campaigns={campaigns}
                  onChangeShop={(value: any) =>
                    setSelectedShop(value)
                  }
                  onChangeCampaign={(value: any) =>
                    setSelectedCampaign(value)
                  }
                />
              </Box>
              <Divider />
            </Box>
          )}
          <Box py={2}>
            <ModifyKeyWords
              currency={shopCurrency}
              currencyLimit={CURRENCY_LIMITATION}
              disabled={selectedEntityIds.length === 0}
              onSubmit={handleModifySelectedKeywords}
            />
          </Box>
          <Box py={2}>
            <KeywordTableAction
              searchText={search}
              disableAddAll={selectedEntityIds.length === 0}
              onSearch={handleSearch}
              onClickAddCustomKeyword={() =>
                setIsShowCustomKeyword(!isShowCustomKeyword)
              }
              onClickAddAll={() =>
                addKeywordsToBucket(selectedEntityIds)
              }
            />
          </Box>
          {isShowCustomKeyword && (
            <Box py={1}>
              <AddCustomKeyword onApply={handleAddCustomKeywords} />
            </Box>
          )}
          <Box mt={2}>
            <Grid container>
              <Grid item xs={12}>
                <TableUI
                  className="setHeightModal"
                  columns={headers}
                  rows={keywords}
                  onSelectItem={handleSelectItem}
                  onSelectAll={handleSelectAll}
                  selectedIds={selectedEntityIds}
                  getRowId={getRowId}
                  onSort={handleSorting}
                  resultTotal={pagination.itemCount}
                  page={pagination.page}
                  pageSize={pagination.limit}
                  onChangePage={handleChangePage}
                  onChangePageSize={handleChangePageSize}
                  noData={
                    <Box py={6}>
                      <img src={NoProduct} width="200" height="200" />
                      <Typography
                        variant="h5"
                        style={{ width: '60%', margin: 'auto' }}
                      >
                        Here’s where you would add keyword to your
                        campaign. This step is an optional, you can
                        skip it and create new campaign
                      </Typography>
                    </Box>
                  }
                />
              </Grid>
            </Grid>
          </Box>
        </Grid>
        <Grid item xs={3}>
          <KeywordBucket
            keywords={entityBucket}
            onRemove={handleRemoveKeywords}
          ></KeywordBucket>
        </Grid>
      </Grid>
      <Box>
        <Grid
          container
          alignItems="center"
          justify="flex-end"
          spacing={1}
        >
          <Grid item>
            <ButtonTextUI
              size="small"
              colortext="#000"
              label="Cancel"
              onClick={handleCancel}
            ></ButtonTextUI>
          </Grid>
          <Grid item>
            <ButtonUI
              disabled={entityBucket.length === 0}
              size="small"
              label="Add Keywords"
              onClick={handleSave}
            ></ButtonUI>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};
