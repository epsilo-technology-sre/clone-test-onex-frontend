import { ThemeProvider } from '@material-ui/core/styles';
import React from 'react';
import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import LazadaTheme from '../../theme';
import { AddKeywordContainer } from './add-keyword-exist-campaign-container';
import { rootSaga } from '../../redux/create-campaign/actions';
import { reducer } from '../../redux/create-campaign/reducers';

export default {
  title: 'Toko / Add Keyword',
};

export const Primary= () => {
  const store = makeStore();
  return (
    <Provider store={store}>
      <ThemeProvider theme={LazadaTheme}>
        <AddKeywordContainer />
        <ToastContainer closeOnClick={false} hideProgressBar={true} />
      </ThemeProvider>
    </Provider>
  );
};

function makeStore(): any {
  if (makeStore._store) return makeStore.store;

  const sagaMiddleware = createSagaMiddleware();
  let composeEnhancers = composeWithDevTools({
    name: 'toko/add-keyword',
  });

  const store = createStore(
    reducer,
    composeEnhancers(applyMiddleware(sagaMiddleware)),
  );

  sagaMiddleware.run(rootSaga);

  makeStore._store = store;

  return store;
}
