import {
  AddToBucketCell,
  CellText,
  EllipsisCell,
  PercentCell,
} from '@ep/shopee/src/components/common/table-cell';
import { MATCH_TYPE_LABEL } from '../../constants';

export const initColumns = (props: any) => {
  const { onAddToBucket } = props;
  return [
    {
      Header: 'Keyword Bidding',
      accessor: (row: any) => ({ value: row.keywordName }),
      sticky: 'left',
      width: 200,
      disableSortBy: true,
      Cell: EllipsisCell,
    },
    {
      Header: 'Search volume',
      id: 'searchVolume',
      accessor: (row: any) => ({
        number: row.searchVolume,
      }),
      disableSortBy: true,
      Cell: PercentCell,
    },
    {
      Header: 'Bidding price',
      id: 'biddingPrice',
      accessor: (row: any) => ({
        number: row.biddingPrice,
        currency: row.currency,
      }),
      Cell: PercentCell,
      disableSortBy: true,
    },
    {
      Header: 'Match type',
      id: 'matchType',
      accessor: (row: any) => ({
        number: row.matchType,
        text: MATCH_TYPE_LABEL[row.matchType],
      }),
      Cell: CellText,
      disableSortBy: true,
    },
    {
      Header: '',
      id: 'action',
      accessor: (row: any) => ({
        item: row,
        onAddToBucket,
      }),
      width: 200,
      Cell: AddToBucketCell,
      disableSortBy: true,
    },
  ];
};
