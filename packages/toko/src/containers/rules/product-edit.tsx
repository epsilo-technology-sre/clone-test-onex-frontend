import { ModalEditRule } from '@ep/shopee/src/components/common/modal-create-rule';
import { Snackbar } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { actions } from '../../redux/actions';
import { MarketingAdvertisingState } from '../../redux/reducers';

const availActionKeys = ['resume_product', 'pause_product'];

export function RuleProductEdit(props: {
  shopId: number;
  products: { product_id: number; product_name: string }[];
  ruleList: any[];
  ruleInfo: any;
  isOpen: boolean;
  onClose: Function;
  onSubmitSuccess: Function;
}) {
  const {
    actionList,
    metricList,
    isLoading = { status: false, error: null },
  } = useSelector((state: MarketingAdvertisingState) => {
    return {
      actionList: (state.rulesActionList || []).filter(
        (i) => availActionKeys.indexOf(i.value) > -1,
      ),
      metricList: state.rulesMetricList,
      isLoading: state.nextLoading[actions.editProductRule.type()],
    };
  });

  const dispatch = useDispatch();
  const [rules, setRules] = React.useState([]);
  const [submitStatus, setSubmitStatus] = React.useState<0 | 1 | 2>(
    0,
  );

  React.useEffect(() => {
    if (props.ruleList) {
      setRules(props.ruleList);
    }
  }, [props.ruleList]);

  React.useEffect(() => {
    dispatch(actions.getRuleActionMetrics());
  }, []);

  React.useEffect(() => {
    if (isLoading.status) {
      setSubmitStatus(1);
    }
    if (
      submitStatus === 1 &&
      isLoading.status === false &&
      !isLoading.error
    ) {
      setSubmitStatus(2);
    }

    if (submitStatus === 2) {
      props.onSubmitSuccess();
    }
  }, [isLoading, submitStatus]);

  const onSubmitRule = React.useCallback(
    (rule: any) => {
      rule.listRules = rule.listRules.map((r) => ({
        ...r,
        conditionPeriod: r.period,
        typeOfPerformance: r.metric_code,
        operator: r.operator_code,
      }));
      dispatch(
        actions.editProductRule({
          rule,
          products: props.products,
          shopId: props.shopId,
        }),
      );
    },
    [props.products],
  );

  if (!actionList || !metricList) return null;

  return (
    <React.Fragment>
      <ModalEditRule
        open={props.isOpen}
        rules={rules}
        ruleInfo={props.ruleInfo}
        onAddRule={(r: any) => {
          setRules(r);
        }}
        onSubmit={onSubmitRule}
        onClose={props.onClose}
        actionList={actionList}
        actionMetricList={metricList}
        isSubmitting={isLoading.status}
      />
      {isLoading.error && (
        <Snackbar
          anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
          open={!!isLoading.error}
          autoHideDuration={60000}
        >
          <Alert hidden={!isLoading.error} severity="error">
            {isLoading.error.message}
          </Alert>
        </Snackbar>
      )}
    </React.Fragment>
  );
}
