import { LogsRule } from '@ep/shopee/src/components/common/logs-rule';
import { differenceWith, isEqual } from 'lodash';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RULE_PERIODS } from '../../constants';
import { actions } from '../../redux/actions';
import { MarketingAdvertisingState } from '../../redux/reducers';
import { initColumns } from './logs-keyword-rule-columns';

const actionList = [
  {
    id: 'activate_product_keyword',
    text: 'Activate Product-Keyword',
  },
  {
    id: 'deactivate_product_keyword',
    text: 'Deactivate Product-Keyword',
  },
  { id: 'increase_bidding_price', text: 'Increase Bidding Price' },
  { id: 'decrease_bidding_price', text: 'Decrease Bidding Price' },
  { id: 'modify_rule', text: 'Modify rule' },
  { id: 'delete_rule', text: 'Delete rule' },
];

export const KeywordRuleLog = (props: {
  open: boolean;
  shopEid: number;
  parentName: string;
  itemId: number;
  itemName: string;
  onClose: Function;
  currency?: string;
}) => {
  const {
    open,
    shopEid,
    parentName,
    itemId,
    itemName,
    onClose,
    currency = 'VND',
  } = props;

  const {
    searchText,
    timeRange,
    selectedActions,
    logList,
    sort,
    pagination,
  } = useSelector((state: MarketingAdvertisingState) => {
    return {
      searchText: state.ruleLogFilters.searchText,
      timeRange: {
        startDate: state.ruleLogFilters.timeRange.start,
        endDate: state.ruleLogFilters.timeRange.end,
      },
      selectedActions: state.ruleLogFilters.actions,
      logList: state.ruleLogs.items,
      sort: state.ruleLogs.sort,
      pagination: state.ruleLogs.pagination,
    };
  });
  const dispatch = useDispatch();

  React.useEffect(() => {
    if (open) {
      dispatch(actions.resetRuleLog());
      dispatch(
        actions.updateRuleLogFilter({
          actions: actionList,
        }),
      );
      getLogsData({ actions: actionList.map((i) => i.id) });
    }
  }, [open]);

  const getLogsData = (argParams: any = {}) => {
    dispatch(
      actions.getRuleLogs({
        itemId,
        shopEid,
        itemType: 'product_keyword',
        search: searchText,
        from: timeRange.startDate,
        to: timeRange.endDate,
        actions: selectedActions.map((i) => i.id),
        // orderBy: sort.sortBy,
        // orderMode: sort.sortMode,
        page: pagination.page,
        limit: pagination.limit,
        ...argParams,
      }),
    );
  };

  const ruleLogs = React.useMemo(() => {
    return (logList || []).map((l) => {
      return {
        ...l,
        currency,
        condition: l.condition.map((c) => ({
          ...c,
          typeOfPerformance: c.metricName,
          period: RULE_PERIODS.find((p) => p.value === c.period)
            ?.name,
        })),
      };
    });
  }, [logList]);

  const handleSearchKeyUp = (event: any) => {
    if (event.key === 'Enter') {
      dispatch(
        actions.updateRuleLogFilter({
          searchText: event.target.value,
        }),
      );
      getLogsData({ search: event.target.value });
    }
  };

  const handleChangeDateRange = ({ startDate, endDate }: any) => {
    const newTimeRange = {
      start: startDate.format('YYYY-MM-DD'),
      end: endDate.format('YYYY-MM-DD'),
    };
    dispatch(
      actions.updateRuleLogFilter({ timeRange: newTimeRange }),
    );
    getLogsData({ from: newTimeRange.start, to: newTimeRange.end });
  };

  const handleChangeActions = (selected: any) => {
    dispatch(
      actions.updateRuleLogFilter({
        actions: selected,
      }),
    );
    getLogsData({ actions: selected.map((i) => i.id) });
  };

  const handleSorting = (sortBy: any) => {
    const isDifference =
      differenceWith(sortBy, sort?.sortBy, isEqual).length > 0;
    if (isDifference) {
      getLogsData();
    }
  };

  const handleChangePage = (page: number) => {
    getLogsData({ page });
  };

  const handleChangePageSize = (value: number) => {
    getLogsData({ limit: value });
  };

  return (
    <div>
      <LogsRule
        open={open}
        handleClose={onClose}
        handleChangeDateRange={handleChangeDateRange}
        handleChangeActions={handleChangeActions}
        actions={actionList}
        selectedTimeRange={timeRange}
        selectedActions={selectedActions}
        tableHeaders={initColumns()}
        logRule={ruleLogs}
        pagination={pagination}
        onSorting={handleSorting}
        onChangePage={handleChangePage}
        onChangePageSize={handleChangePageSize}
        handleSearchKeyUp={handleSearchKeyUp}
        ruleInfo={{
          parentItem: {
            name: 'Product',
            value: parentName,
          },
          item: {
            id: itemId,
            name: 'Keyword',
            value: itemName,
          },
        }}
      />
    </div>
  );
};
