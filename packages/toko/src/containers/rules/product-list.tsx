import { DrawerRuleProduct } from '@ep/shopee/src/components/common/drawer-rule-product';
import { ModalConfirmUI } from '@ep/shopee/src/components/common/modal-confirm';
import { get } from 'lodash';
import moment from 'moment';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RULE_PERIODS } from '../../constants';
import { actions } from '../../redux/actions';
import { MarketingAdvertisingState } from '../../redux/reducers';
import { RuleProductEdit } from './product-edit';

export function ListRuleProduct(props: {
  shopEid: number;
  productId: number;
  productName: string;
  isOpen: boolean;
  allowUpdate?: boolean;
  onClose: Function;
  onUpdateSuccess: Function;
  onAddRule: Function;
}) {
  const {
    ruleList,
    actionList,
    metricList,
    isLoading = { status: false, error: null },
    isDeleting = { status: false, error: null },
  } = useSelector((state: MarketingAdvertisingState) => {
    return {
      actionList: state.rulesActionList,
      metricList: state.rulesMetricList,
      ruleList: state.productRuleList,
      isLoading: state.nextLoading[actions.getProductRules.type()],
      isDeleting: state.nextLoading[actions.deleteProductRule.type()],
    };
  });

  const [ruleProductEdit, setRuleProductEdit] = React.useState({
    isOpen: false,
    products: [],
    ruleList: [],
    ruleInfo: {},
    shopId: null,
  });

  const [showConfirm, setShowConfirm] = React.useState({
    show: false,
    rule: {},
  });

  const [startDelete, setStartDelete] = React.useState(false);

  const dispatch = useDispatch();
  React.useEffect(() => {
    if (props.isOpen && (!actionList || actionList.length === 0)) {
      dispatch(actions.getRuleActionMetrics());
    }
  }, [metricList, actionList, props.isOpen]);

  React.useEffect(() => {
    if (actionList && props.productId && props.shopEid) {
      getRuleList();
    }
  }, [actionList, props.productId, props.shopEid]);

  React.useEffect(() => {
    if (startDelete) {
      if (!isDeleting.status) {
        getRuleList();
        setStartDelete(false);
      }
    }
  }, [isDeleting]);

  const getRuleList = () => {
    if (actionList) {
      dispatch(
        actions.getProductRules({
          productId: props.productId,
          shopEid: props.shopEid,
        }),
      );
    }
  };

  let cRuleList = React.useMemo(() => {
    if (metricList) {
      return (ruleList || []).map((i) => {
        const mlist = metricList[i.action_code];
        let biddingInfo = get(i, 'data.bidding_price_data', {});

        return {
          ruleName: i.rule_name,
          ruleId: i.rule_id,
          timeline: [
            moment(i.timeline_from, 'YYYY-MM-DD').format(
              'DD/MM/YYYY',
            ),
            '-',
            moment(i.timeline_to, 'YYYY-MM-DD').format('DD/MM/YYYY'),
          ].join(' '),
          actionCode: i.action_code,
          status: get(
            actionList.find((a) => a.value === i.action_code),
            'name',
            '-missing-',
          ),
          conditionStr: i.condition.map((c) => {
            let pName = get(
              mlist.find((m) => m.value === c.metric_code),
              'name',
              '-missing-',
            );
            let period = RULE_PERIODS.find(
              (p) => p.value === c.period,
            )?.name;
            let op = c.operator_code;
            let val = c.value;

            return [pName, period, op, val].join(' ');
          }),
          statusInfo: {
            ...biddingInfo,
            min: biddingInfo.minimum,
            max: biddingInfo.maximum,
          },
        };
      });
    }
  }, [ruleList, actionList, metricList]);

  const handleOpenAddNewRule = () => {
    props.onAddRule({
      shop_eid: props.shopEid,
      productId: props.productId,
      productName: props.productName,
    });
  };

  const handleOpenEditRule = (rule: any) => {
    const existRule = ruleList.find((r) => r.rule_id === rule.ruleId);
    if (existRule) {
      const ruleInfo = {
        ruleId: existRule.rule_id,
        ruleName: existRule.rule_name,
        action: existRule.action_code,
        conditions: existRule.condition,
        fromDate: moment(
          existRule.timeline_from,
          'YYYY-MM-DD',
        ).format('DD/MM/YYYY'),
        toDate: moment(existRule.timeline_to, 'YYYY-MM-DD').format(
          'DD/MM/YYYY',
        ),
      };
      const ruleList = existRule.condition.map((item, index) => ({
        id: `condition${index}`,
        conditionPeriod: item.period,
        operator: item.operator_code,
        typeOfPerformance: item.metric_code,
        value: item.value,
      }));
      setRuleProductEdit((state) => ({
        ...state,
        isOpen: true,
        shopId: props.shopEid,
        products: [
          {
            product_id: props.productId,
            product_name: props.productName,
          },
        ],
        ruleInfo,
        ruleList,
      }));
    }
  };

  const handleEditRuleSuccess = () => {
    getRuleList();
    setRuleProductEdit((state) => ({
      ...state,
      isOpen: false,
    }));
  };

  const handleOpenDeleteRule = (rule) => {
    setShowConfirm({
      ...showConfirm,
      show: true,
      rule,
    });
  };

  const handleDeleteRule = () => {
    setStartDelete(true);
    dispatch(
      actions.deleteProductRule({
        ruleId: showConfirm.rule.ruleId,
        shopEid: props.shopEid,
        productId: props.productId,
      }),
    );
    setShowConfirm({
      ...showConfirm,
      show: false,
    });
    props.onUpdateSuccess();
  };

  return (
    <React.Fragment>
      <DrawerRuleProduct
        open={props.isOpen}
        rules={cRuleList}
        title={props.productName}
        code={String(props.productId)}
        allowUpdate={props.allowUpdate}
        onClose={props.onClose}
        isLoading={isLoading.status}
        onOpenAddRule={handleOpenAddNewRule}
        onOpenEditRule={handleOpenEditRule}
        onDeleteRule={handleOpenDeleteRule}
      />

      <RuleProductEdit
        isOpen={ruleProductEdit.isOpen}
        shopId={ruleProductEdit.shopId}
        products={ruleProductEdit.products}
        ruleInfo={ruleProductEdit.ruleInfo}
        ruleList={ruleProductEdit.ruleList}
        onSubmitSuccess={handleEditRuleSuccess}
        onClose={() =>
          setRuleProductEdit((state) => ({
            ...state,
            isOpen: false,
          }))
        }
      />

      {showConfirm.show && (
        <ModalConfirmUI
          open={showConfirm.show}
          labelSubmit="OK"
          title="Are you sure remove this rule?"
          onClose={() =>
            setShowConfirm({ ...showConfirm, show: false })
          }
          onSubmit={handleDeleteRule}
        />
      )}
    </React.Fragment>
  );
}
