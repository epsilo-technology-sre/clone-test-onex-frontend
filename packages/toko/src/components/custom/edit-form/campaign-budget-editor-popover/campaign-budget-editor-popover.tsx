import { COLORS } from '@ep/shopee/src/constants';
import { makeStyles } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Popover from '@material-ui/core/Popover';
import { Form, Formik } from 'formik';
import PopupState, {
  bindPopover,
  bindTrigger,
} from 'material-ui-popup-state';
import React, { useContext } from 'react';
import * as Yup from 'yup';
import { CampaignContext } from '@ep/shopee/src/components/campaigns/campaign-context';
import {
  ButtonTextUI,
  ButtonUI,
} from '@ep/shopee/src/components/common/button';
import { CampaignBudget } from '../../create-campaign/campaign-budget';

const validationSchema = Yup.object().shape({
  budget: Yup.object().shape({
    daily: Yup.number().when('isNoLimit', {
      is: false,
      then: Yup.number()
        .required('Please enter Budget')
        .min(100, 'Daily budget is too low'),
    }),
  }),
});

export const CampaignBudgetEditorPopover = (props: any) => {
  const context = useContext(CampaignContext);
  const {
    triggerElem,
    campaign,
    total,
    daily,
    currency,
    hideTotal,
  } = props;
  const classes = useStyles();
  const [isLoading, setLoading] = React.useState(false);

  const applyChange = async (popupState: any, value: any) => {
    if (context && context.updateCampaignBudget) {
      let budget = value.budget;
      setLoading(true);
      const result = await context.updateCampaignBudget({
        shop_eids: [campaign.shop_eid],
        campaign_eids: [campaign.campaign_eid],
        // total_budget: budget.isNoLimit ? 0 : budget.total,
        // daily_budget: budget.isOnDaily ? budget.daily : 0,
        budget: budget.isNoLimit ? 0 : budget.daily,
      });
      if (result) {
        popupState.close();
      }
      setLoading(false);
    } else {
      popupState.close();
    }
  };

  return (
    <PopupState variant="popover" popupId="demo-popup-popover">
      {(popupState) => {
        return (
          <Box ml={1}>
            <Box {...bindTrigger(popupState)}>{triggerElem}</Box>
            <Popover
              {...bindPopover(popupState)}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
            >
              <Formik
                validationSchema={validationSchema}
                initialValues={{
                  budget: {
                    daily: daily,
                    isNoLimit: daily === 0,
                  },
                }}
                onSubmit={(values) => {
                  return applyChange(popupState, values);
                }}
                validateOnBlur={true}
              >
                {({ handleSubmit, errors }) => {
                  return (
                    <Form onSubmit={handleSubmit}>
                      <Box padding={1}>
                        <CampaignBudget currency={currency} />
                        <Box className={classes.footer}>
                          <ButtonTextUI
                            label="Cancel"
                            size="small"
                            colortext="#000"
                            onClick={() => popupState.close()}
                          />
                          <ButtonUI
                            loading={isLoading}
                            colorButton={COLORS.COMMON.ORANGE}
                            type="submit"
                            label="Apply"
                          />
                        </Box>
                      </Box>
                    </Form>
                  );
                }}
              </Formik>
            </Popover>
          </Box>
        );
      }}
    </PopupState>
  );
};

const useStyles = makeStyles({
  body: {
    padding: 8,
  },
  title: {
    color: '#596772',
    fontSize: 11,
    fontWeight: 'bold',
    padding: '5px 0',
  },
  switcher: {
    display: 'flex',
    alignItems: 'center',
    padding: '8px 0',
    '& img': {
      cursor: 'pointer',
      marginRight: 8,
    },
  },
  error: {
    color: COLORS.COMMON.RED,
    fontSize: 12,
    marginTop: 8,
  },
  footer: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: 8,
    borderTop: '2px solid #E4E7E9',
    '& button': {
      marginLeft: 5,
    },
  },
});
