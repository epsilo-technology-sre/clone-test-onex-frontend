import Box from '@material-ui/core/Box';
import Popover from '@material-ui/core/Popover';
import PopupState, {
  bindPopover,
  bindTrigger,
} from 'material-ui-popup-state';
import React, { useContext } from 'react';
import { KeywordBudgetEditor } from '@ep/shopee/src/components/edit-form/keyword-budget-editor';

export const KeywordBudgetEditorPopover = (props: any) => {
  const ContainerContext = props.context;
  const context = useContext(ContainerContext);
  const { triggerElem, item, bidding_price, currency } = props;

  const applyChange = (popupState: any, price: any) => {
    popupState.close();
    if (context && context.updateBiddingPrice) {
      context.updateBiddingPrice({
        itemIds: [item.id],
        biddingPrice: price,
        shopId: item.shop_eid,
      });
    }
  };

  return (
    <PopupState variant="popover" popupId="demo-popup-popover">
      {(popupState) => {
        return (
          <Box ml={1}>
            <Box {...bindTrigger(popupState)}>{triggerElem}</Box>
            <Popover
              {...bindPopover(popupState)}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
            >
              <KeywordBudgetEditor
                currency={currency}
                total={bidding_price}
                onSave={(value: any) =>
                  applyChange(popupState, value)
                }
                onCancel={() => popupState.close()}
              />
            </Popover>
          </Box>
        );
      }}
    </PopupState>
  );
};
