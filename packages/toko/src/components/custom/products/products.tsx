import { BulkActionsUI } from '@ep/shopee/src/components/common/bulk-actions';
import { ButtonUI } from '@ep/shopee/src/components/common/button';
import { LocalFilter } from '@ep/shopee/src/components/common/local-filter';
import { TableUISticky as TableUI } from '@ep/shopee/src/components/common/table';
import { ProductContext } from '@ep/shopee/src/components/products/product-context';
import React from 'react';

export const ProductView = (props: any) => {
  const {
    tableHeaders,
    products,
    pagination,
    statusList,
    selectedStatus,
    searchText,
    selectedProducts = [],
    handleSelectItem,
    handleSelectAll,
    getRowId,
    selectedIds,
    onSearch,
    onChangeStatus,
    onSorting,
    onChangePage,
    onChangePageSize,
    onAddProduct,
    handleActivator,
    updateProduct,
    handleOpenAddKeyword,
    handleOpenModifyBudget,
    openModalAddrule,
    handleDeleteRules,
  } = props;

  const context = {
    updateProductBudget: (value: any) => {
      return updateProduct('budget', value);
    },
  };

  const buttons = [
    <ButtonUI
      onClick={openModalAddrule}
      label="Add rule"
      size="small"
      colorButton="#F6F7F8"
    />,
    <ButtonUI
      onClick={() => handleActivator('active', selectedProducts)}
      label="Activate"
      size="small"
      colorButton="#F6F7F8"
    />,
    <ButtonUI
      colortext="#D4290D"
      onClick={() => handleActivator('deactive', selectedProducts)}
      label="Deactivate"
      size="small"
      colorButton="#F6F7F8"
    />,
    <ButtonUI
      onClick={handleDeleteRules}
      label="Delete rules"
      size="small"
      colorButton="#F6F7F8"
    />,
  ];

  return (
    <ProductContext.Provider value={context}>
      <LocalFilter
        search={searchText}
        statusList={statusList}
        status={selectedStatus}
        onChangeStatus={onChangeStatus}
        onChangeSearchText={onSearch}
        onAddProduct={onAddProduct}
      />
      {selectedProducts.length > 0 && (
        <BulkActionsUI
          textSelected={`${selectedProducts.length} products selected`}
          buttons={buttons}
        />
      )}
      <TableUI
        columns={tableHeaders}
        rows={products}
        resultTotal={pagination.item_count}
        page={pagination.page}
        pageSize={pagination.limit}
        selectedIds={selectedIds}
        getRowId={getRowId}
        noData="No data"
        onSelectItem={handleSelectItem}
        onSelectAll={handleSelectAll}
        onSort={onSorting}
        onChangePage={onChangePage}
        onChangePageSize={onChangePageSize}
      />
    </ProductContext.Provider>
  );
};

ProductView.defaultProps = {
  products: [],
  pagination: {
    page: 1,
    limit: 10,
    item_count: 0,
    page_count: 1,
  },
};
