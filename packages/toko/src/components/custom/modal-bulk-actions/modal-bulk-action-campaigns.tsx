import { COLORS } from '@ep/shopee/src/constants';
import SwitchOffIcon from '@ep/shopee/src/images/switch-off.svg';
import SwitchOnIcon from '@ep/shopee/src/images/switch-on.svg';
import { Box, InputBase, Typography } from '@material-ui/core';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import React, { useState } from 'react';
import styled from 'styled-components';
import { ActionIcon } from '@ep/shopee/src/components/common/action-icon';
import { CurrencyTextbox } from '@ep/shopee/src/components/common/textfield';
import { TooltipUI } from '@ep/shopee/src/components/common/tooltip';
import { ModalBulkActionUI } from './ModalBulkAction';
import { CampaignBudget } from '../create-campaign/campaign-budget';
import { KeywordBudgetEditor } from '@ep/shopee/src/components/edit-form/keyword-budget-editor';
import { Field, Form, Formik } from 'formik';
import * as Yup from 'yup';
import { CURRENCY_LIMITATION } from '@ep/toko/src/constants';

const Wrapper = styled.div`
  margin: 8px 0;
`;

const Wrap = styled.div`
  border: 2px solid #e4e7e9;
  border-radius: 4px;
  height: 120px;
  padding: 8px;
  width: 448px;
  overflow: auto;
`;

const Keyword = styled.span`
  background: #f6f7f8;
  border-radius: 4px;
  padding: 2px 6px;
  font-size: 14px;
  line-height: 20px;
  color: #253746;
  margin-right: 8px;
  margin-bottom: 8px;
  display: inline-block;
`;

const Delete = styled.span`
  margin-left: 9px;
`;

const TypographyUI = styled(Typography)`
  margin-bottom: 4px;
  margin-top: 8px;
`;

export interface ModalBulkActionCampaignProps {
  campaigns: any;
  open: boolean;
  setOpenModalBulkAction: any;
  updateSelected: any;
  isNoLimit?: boolean;
  daily?: number;
  currency?: string;
  biddingPrice?: number;
  onClose: () => void;
}
const useStyles = makeStyles({
  title: {
    color: '#596772',
    fontSize: 11,
    fontWeight: 'bold',
    padding: '5px 0',
  },
  switcher: {
    display: 'flex',
    alignItems: 'center',
    padding: '8px 0',
    '& img': {
      cursor: 'pointer',
      marginRight: 8,
    },
  },
  label: {
    color: '#596772',
    fontSize: 11,
    fontWeight: 'bold',
    padding: '5px 0',
  },
  error: {
    color: COLORS.COMMON.RED,
    fontSize: 12,
    marginTop: 8,
  },
  footer: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: 8,
    borderTop: '2px solid #E4E7E9',
    '& button': {
      marginLeft: 5,
    },
  },
});


export const ModalBulkActionCampaign = (
  props: ModalBulkActionCampaignProps,
) => {
  const {
    open,
    setOpenModalBulkAction,
    updateSelected,
    onClose,
    currency = 'VND',
    biddingPrice = 0,
    daily = 0,
  } = props;
  const classes = useStyles();

  const [isLoading, setLoading] = React.useState(false);

  const formRef = React.useRef<any>(null);

  const schema = React.useMemo(() => {
    console.info({ Yup });
    return Yup.object().shape({
      budget: Yup.object().shape({
        daily: Yup.number().when('isNoLimit', {
          is: false,
          then: Yup.number()
            .required('Please enter Budget')
            .min(
              CURRENCY_LIMITATION[currency].DAILY_BUDGET,
              'Daily budget is too low',
            ),
        }),
      }),
      price: Yup.number().required('Please enter Bidding price'),
    });
  }, [currency]);

  return (
    <>
      <ModalBulkActionUI
        open={open}
        loading={isLoading}
        onSubmit={() => {
          if (formRef.current) {
            setLoading(true);
            formRef.current.submitForm();
          }
        }}
        onClose={onClose}
        labelSubmit="Apply"
        title="Modify Campaign"
      >
        <Formik
          innerRef={formRef}
          validationSchema={schema}
          initialValues={{
            budget: {
              daily: daily,
              isNoLimit: props.isNoLimit,
            },
            price: biddingPrice,
          }}
          onSubmit={(values) => {
            console.info('submit', values);
            setOpenModalBulkAction({
              open: !open,
              budget: values.budget,
              biddingPrice: values.price,
            });
          }}
        >
          {({ errors }) => {
            return (
              <React.Fragment>
                <span>
                  Modify budget for{' '}
                  <strong>
                    {props.campaigns.length} selected campaigns
                  </strong>
                </span>
                <Wrapper>
                  <Wrap>
                    {props.campaigns.map(
                      (
                        { campaign_name, campaign_eid }: any,
                        index: number,
                      ) => {
                        const keyword = campaign_name;
                        const title =
                          keyword.length > 21
                            ? `${keyword.slice(0, 21)}...`
                            : keyword;
                        return (
                          <TooltipUI
                            title={keyword}
                            key={campaign_eid}
                          >
                            <Keyword>
                              {title}
                              {props.campaigns.length > 1 && (
                                <Delete
                                  onClick={() => {
                                    const newKeyword = props.campaigns.filter(
                                      (item) =>
                                        item.campaign_eid !==
                                        campaign_eid,
                                    );
                                    updateSelected(newKeyword);
                                  }}
                                >
                                  <ActionIcon status="remove" />
                                </Delete>
                              )}
                            </Keyword>
                          </TooltipUI>
                        );
                      },
                    )}
                  </Wrap>
                </Wrapper>
                <Wrapper>
                  <TypographyUI
                    variant="body2"
                    className={classes.label}
                  >
                    Bid Price
                  </TypographyUI>
                  <Field name="price">
                    {({ field, form }: any) => (
                      <CurrencyTextbox
                        currency={currency}
                        min={1}
                        step={1}
                        error={errors.price}
                        value={field.value}
                        onChange={(value: any) => {
                          form.setFieldValue(field.name, value);
                        }}
                      />
                    )}
                  </Field>
                </Wrapper>
                <Wrapper>
                  <CampaignBudget currency={currency} />
                </Wrapper>
              </React.Fragment>
            );
          }}
        </Formik>
      </ModalBulkActionUI>
    </>
  );
};
