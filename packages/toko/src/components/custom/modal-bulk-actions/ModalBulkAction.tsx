import React from 'react';
import {
  DialogTitle,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
} from '@material-ui/core';
import { ButtonUI } from '@ep/shopee/src/components/common/button';

export interface ModalConfirmUIProps {
  open: boolean;
  onSubmit: () => any;
  onClose: () => any;
  labelSubmit: string;
  disabled?: boolean;
  title: string;
  children: any;
  loading: boolean;
}

export function ModalBulkActionUI(props: ModalConfirmUIProps) {
  const {
    open,
    onSubmit,
    onClose,
    labelSubmit,
    disabled,
    title,
    loading,
  } = props;

  return (
    <Dialog aria-labelledby="simple-dialog-title" open={open}>
      <DialogTitle id="simple-dialog-title">{title}</DialogTitle>
      <DialogContent>{props.children}</DialogContent>
      <DialogActions>
        <ButtonUI
          onClick={onClose}
          label="Cancel"
          size="small"
          colorButton="#F6F7F8"
        />
        <ButtonUI
          loading={loading}
          onClick={onSubmit}
          autoFocus
          disabled={disabled}
          label={labelSubmit}
          size="small"
          variant="contained"
        />
      </DialogActions>
    </Dialog>
  );
}
