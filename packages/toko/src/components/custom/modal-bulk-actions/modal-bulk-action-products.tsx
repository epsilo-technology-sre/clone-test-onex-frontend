import { COLORS } from '@ep/shopee/src/constants';
import SwitchOffIcon from '@ep/shopee/src/images/switch-off.svg';
import SwitchOnIcon from '@ep/shopee/src/images/switch-on.svg';
import {
  Box,
  Grid,
  InputBase,
  MenuItem,
  Select,
  Typography,
} from '@material-ui/core';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import React, { useState } from 'react';
import styled from 'styled-components';
import { ActionIcon } from '../action-icon';
import { CurrencyTextbox } from '../textfield';
import { TooltipUI } from '../tooltip';
import { ModalBulkActionUI } from './ModalBulkAction';

const Wrapper = styled.div`
  margin: 8px 0;
`;

const Wrap = styled.div`
  border: 2px solid #e4e7e9;
  border-radius: 4px;
  height: 120px;
  padding: 8px;
  width: 448px;
  overflow: auto;
`;

const Keyword = styled.span`
  background: #f6f7f8;
  border-radius: 4px;
  padding: 2px 6px;
  font-size: 14px;
  line-height: 20px;
  color: #253746;
  margin-right: 8px;
  margin-bottom: 8px;
  display: inline-block;
`;

const Delete = styled.span`
  margin-left: 9px;
`;

const TypographyUI = styled(Typography)`
  margin-bottom: 4px;
  margin-top: 8px;
`;

export interface ModalBulkActionProductProps {
  products: any;
  open: boolean;
  setOpenModalBulkAction: any;
  updateSelected: any;
  currency?: string;
  error?: string;
}
const useStyles = makeStyles({
  body: {
    padding: 8,
  },
  title: {
    color: '#596772',
    fontSize: 11,
    fontWeight: 'bold',
    padding: '5px 0',
  },
  switcher: {
    display: 'flex',
    alignItems: 'center',
    padding: '8px 0',
    '& img': {
      cursor: 'pointer',
      marginRight: 8,
    },
  },
  error: {
    color: COLORS.COMMON.RED,
    fontSize: 12,
    marginTop: 8,
  },
  footer: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: 8,
    borderTop: '2px solid #E4E7E9',
    '& button': {
      marginLeft: 5,
    },
  },
});

const BootstrapInput = withStyles({
  root: {
    width: '100%',
  },
  disabled: {
    color: '#9FA7AE !important',
    background: '#F6F7F8 !important',
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: '#ffffff',
    border: '2px solid #D3D7DA',
    fontSize: 14,
    color: '#253746',
    padding: '5px 26px 5px 8px',
  },
})(InputBase);

const BUDGET = {
  DAILY: 'daily_budget',
  TOTAL: 'total_budget',
};

export const ModalBulkActionProduct = (
  props: ModalBulkActionProductProps,
) => {
  const {
    open,
    setOpenModalBulkAction,
    updateSelected,
    currency = 'VND',
    error,
    onClose = () => {},
  } = props;
  const classes = useStyles();
  const [isOnNoLimit, setIsOnNoLimit] = useState(true);
  const [budgetType, setBudgetType] = useState(BUDGET.TOTAL);
  const [budget, setBudget] = useState(0);

  const handleTotalSwitch = () => setIsOnNoLimit(!isOnNoLimit);

  const handleChangeBudgetType = (event: any) =>
    setBudgetType(event.target.value);

  return (
    <>
      <ModalBulkActionUI
        open={open}
        onSubmit={() => {
          setOpenModalBulkAction({
            open: !open,
            total_budget: 0,
            daily_budget: 0,
            [budgetType]: isOnNoLimit ? 0 : budget,
          });
        }}
        onClose={onClose}
        labelSubmit="Apply"
        title="Modify budget"
      >
        <span>
          Modify budget for{' '}
          <strong>{props.products.length} selected products</strong>
        </span>
        <Wrapper>
          <Wrap>
            {props.products.map((item: any, index: number) => {
              const keyword = item.product_name;
              const itemId = item.campaign_product_id;
              const title =
                keyword.length > 21
                  ? `${keyword.slice(0, 21)}...`
                  : keyword;

              console.info(props.products, item);

              return (
                <TooltipUI
                  title={keyword}
                  key={item.campaign_product_id}
                >
                  <Keyword>
                    {title}
                    {props.products.length > 1 && (
                      <Delete
                        onClick={() => {
                          const newKeyword = props.products.filter(
                            (item) =>
                              itemId !== item.campaign_product_id,
                          );
                          updateSelected(newKeyword);
                        }}
                      >
                        <ActionIcon status="remove" />
                      </Delete>
                    )}
                  </Keyword>
                </TooltipUI>
              );
            })}
          </Wrap>
        </Wrapper>
        <Wrapper>
          <TypographyUI variant="body2">Budget</TypographyUI>
          <Box className={classes.body}>
            <Box className={classes.switcher}>
              <img
                src={isOnNoLimit ? SwitchOnIcon : SwitchOffIcon}
                height={16}
                width={30}
                onClick={handleTotalSwitch}
              />
              <Typography variant="body2" component="span">
                No limit
              </Typography>
            </Box>
            <Box>
              <TypographyUI variant="body2">
                Help or instruction message goes here.
              </TypographyUI>
            </Box>
            <Box>
              <Grid container spacing={1}>
                <Grid item xs={5}>
                  <Select
                    id="demo-customized-select-native"
                    disabled={isOnNoLimit}
                    value={budgetType}
                    onChange={handleChangeBudgetType}
                    input={<BootstrapInput />}
                    MenuProps={{
                      anchorOrigin: {
                        vertical: "bottom",
                        horizontal: "left"
                      },
                      transformOrigin: {
                        vertical: "top",
                        horizontal: "left"
                      },
                      getContentAnchorEl: null
                    }}
                  >
                    <MenuItem value={BUDGET.DAILY}>
                      Daily budget
                    </MenuItem>
                    <MenuItem value={BUDGET.TOTAL}>
                      Total budget
                    </MenuItem>
                  </Select>
                </Grid>
                <Grid item xs={7}>
                  <CurrencyTextbox
                    currency={currency}
                    disabled={isOnNoLimit}
                    min={1}
                    step={1}
                    value={budget}
                    onChange={(value: any) => setBudget(value)}
                  ></CurrencyTextbox>
                </Grid>
              </Grid>
            </Box>
          </Box>
          <Box>
            {error && <Box className={classes.error}>{error}</Box>}
          </Box>
        </Wrapper>
      </ModalBulkActionUI>
    </>
  );
};
