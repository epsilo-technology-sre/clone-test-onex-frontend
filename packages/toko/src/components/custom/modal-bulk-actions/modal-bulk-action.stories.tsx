import React, { useState } from 'react';
import { Typography } from '@material-ui/core';
import { ModalBulkActionUI } from './ModalBulkAction';
import { ButtonUI } from '@ep/shopee/src/components/common/button';
import { ModalBulkActionCampaign } from './modal-bulk-action-campaigns';
import { action, actions } from '@storybook/addon-actions';

export default {
  title: 'Toko/Modal Bulk Actions',
};

export const CampaginModify = () => {
  const [open, setOpen] = useState(true);
  const arrKey = [];
  for (let i = 0; i < 50; i++) {
    arrKey.push(`keyword ${i}`);
  }
  const [keywords, setKeywords] = useState(arrKey);
  const [USDAmount, setUSDAmount] = React.useState(5);
  const daily = 2021;

  const handleSubmit = (value: any) => {
    console.info(value);
  };

  return (
    <>
      <ButtonUI
        onClick={() => setOpen(!open)}
        label="Show modal"
        size="small"
        colorButton="#F6F7F8"
      />
      <ModalBulkActionCampaign
        campaigns={[
          { campaign_eid: 1, campaign_name: 'Campaign 1' },
          { campaign_eid: 2, campaign_name: 'Campaign 2' },
        ]}
        open={open}
        setOpenModalBulkAction={action('modalbulkAction')}
        updateSelected={action('updateSelected')}
        isNoLimit={daily <= 0}
        daily={daily}
        currency={'IDR'}
        onClose={() => setOpen(false)}
      />
    </>
  );
};
