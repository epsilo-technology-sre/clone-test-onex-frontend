import { CurrencyTextbox } from '@ep/shopee/src/components/common/textfield';
import { CampaignName } from '@ep/shopee/src/components/create-campaign/campaign-name';
import {
  AutoCompleteUI,
  ButtonLogin,
  useStyle,
} from '@ep/shopee/src/components/create-campaign/common';
import { CURRENCY_LIMITATION } from '@ep/toko/src/constants';
import {
  Box,
  FormHelperText,
  Grid,
  TextField,
  Typography,
} from '@material-ui/core';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import React from 'react';
import * as Yup from 'yup';
import { CampaignBudget } from '../campaign-budget';

let inChecking: any = null;

type SetupCampaignProps = {
  shops: {
    shopId: number;
    shopName: string;
    shopCurrency: string;
  }[];
  campaignName: string;
  shop: number;
  budget: any;
  price: number;
  onSubmit: (form: any) => void;
  checkCampaignNameExists: (
    name: string,
    shopId: number,
  ) => Promise<boolean>;
};

const SettingCampaignSchema = (
  checkExistCampaignName: SetupCampaignProps['checkCampaignNameExists'],
) => {
  return Yup.object().shape({
    shop: Yup.number().required('Please select shop'),
    shopName: Yup.string(),
    shopCurrency: Yup.string(),
    campaignName: Yup.string()
      .required('Please enter Campaign name')
      .test(
        'contain-default',
        'Campaign Name cannot contain “Default” or “default”',
        async (value: any = '') => {
          if (value.toLowerCase().includes('default')) {
            return false;
          }
          return true;
        },
      )
      .when(['shop'], (shopId: number) => {
        if (shopId > 0) {
          return Yup.string().test(
            'Unique Campaign name',
            'Campaign Name cannot be duplicated',
            async (value: any = '') => {
              clearTimeout(inChecking);
              let campaignName = value.trim().toLowerCase();

              if (!campaignName) {
                return false;
              }

              return await new Promise((resolve, reject) => {
                inChecking = setTimeout(async () => {
                  try {
                    let result = await checkExistCampaignName(
                      value.trim(),
                      shopId,
                    );
                    resolve(result);
                  } catch (e) {
                    resolve(false);
                  }
                }, 500);
              });
            },
          );
        }
      }),
    price: Yup.number()
      .required('Please enter Bidding price')
      .when(['shopCurrency'], (shopCurrency, schema) => {
        let minValue =
          CURRENCY_LIMITATION[shopCurrency]?.MAX_CPC?.MIN || 1;
        return schema.min(
          minValue,
          `Max CPC must be higher ${minValue}`,
        );
      }),
    budget: Yup.object().shape({
      daily: Yup.number().when(
        ['isNoLimit', 'minDailyBudget'],
        (isNoLimit, minDailyBudget, schema) => {
          if (isNoLimit) {
            return schema;
          } else {
            return schema
              .required('Please enter Budget')
              .min(
                minDailyBudget,
                `Daily budget must be higher ${minDailyBudget} `,
              );
          }
        },
      ),
      minDailyBudget: Yup.number().optional(),
    }),
  });
};

export const SetupCampaign = (props: SetupCampaignProps) => {
  const classes = useStyle();

  const initialValues = {
    shop: props.shop,
    campaignName: props.campaignName,
    budget: props.budget,
    price: props.price,
    shopName: props.shopName,
    shopCurrency: props.shopCurrency,
  };

  const validationSchema = React.useMemo(
    () => SettingCampaignSchema(props.checkCampaignNameExists),
    [],
  );

  return (
    <Formik
      validationSchema={validationSchema}
      initialValues={initialValues}
      onSubmit={(values, ...rest) => {
        props.onSubmit(values);
      }}
      validateOnBlur={true}
    >
      {(formikProps) => {
        const {
          values,
          setFieldValue,
          handleSubmit,
          errors,
        } = formikProps;

        console.error({ formErrors: errors });

        return (
          <Form onSubmit={handleSubmit}>
            <Grid
              container
              direction="column"
              spacing={2}
              className={classes.root}
            >
              <Grid item>
                <Typography variant="subtitle1">
                  Campaign information
                </Typography>
                <Box>
                  <Typography
                    variant="subtitle2"
                    className={classes.label}
                  >
                    Shop
                  </Typography>
                  <AutoCompleteUI
                    id="shop"
                    fullWidth
                    options={props.shops}
                    defaultValue={(props.shops || []).find(
                      (s) => s.shopId === values.shop,
                    )}
                    getOptionLabel={(option) => option.shopName}
                    renderInput={(params) => (
                      <TextField {...params} variant="outlined" />
                    )}
                    onChange={(
                      e,
                      values: SetupCampaignProps['shops'][0],
                    ) => {
                      const val = values;
                      setFieldValue('shop', val ? val.shopId : '');
                      setFieldValue(
                        'shopName',
                        val ? val.shopName : '',
                      );
                      setFieldValue(
                        'shopCurrency',
                        val ? val.shopCurrency : '',
                      );
                      setFieldValue(
                        'budget.minDailyBudget',
                        CURRENCY_LIMITATION[val.shopCurrency]
                          ?.DAILY_BUDGET?.MIN || 1,
                      );
                    }}
                  />
                  <ErrorMessage
                    component={FormHelperText}
                    name="shop"
                  />
                </Box>
                <CampaignName></CampaignName>
              </Grid>
              <Grid item>
                <Typography
                  variant="subtitle1"
                  style={{
                    color: '#596772',
                    fontSize: 11,
                    fontWeight: 'bold',
                    padding: '5px 0',
                  }}
                >
                  Max CPC
                </Typography>
                <Field name="price">
                  {({ field, form }: any) => (
                    <CurrencyTextbox
                      currency={values.shopCurrency}
                      min={1}
                      step={1}
                      error={errors.price}
                      value={field.value}
                      onChange={(value: any) => {
                        form.setFieldValue(field.name, value);
                        form.setFieldValue(
                          'budget.minDailyBudget',
                          value * 40,
                        );
                      }}
                    />
                  )}
                </Field>
              </Grid>
              <Grid item>
                <CampaignBudget
                  currency={values.shopCurrency}
                ></CampaignBudget>
              </Grid>
              <Grid item></Grid>
              <Grid item>
                <ButtonLogin
                  variant={'contained'}
                  size="medium"
                  fullWidth
                  type={'submit'}
                >
                  Next step
                </ButtonLogin>
              </Grid>
            </Grid>
          </Form>
        );
      }}
    </Formik>
  );
};
