import { CurrencyCode } from '@ep/shopee/src/components/common/currency-code';
import {
  MATCH_TYPE,
  MATCH_TYPE_LABEL,
} from '@ep/shopee/src/constants';
import { Box, Grid, makeStyles, Typography } from '@material-ui/core';
import CancelIcon from '@material-ui/icons/Cancel';
import React from 'react';

const useStyle = makeStyles({
  root: {
    background: '#F6F7F8',
    color: '#253746',
    padding: '16px 16px 40px',
    position: 'relative',
  },
  productWrapper: {
    height: 495,
    overflow: 'auto',
  },
  product: {
    position: 'relative',
    background: '#ffffff',
    fontSize: 12,
    padding: 8,
    marginBottom: 4,
    '& .line2': {
      color: '#596772',
      fontSize: 10,
    },
    '& .remove': {
      display: 'none',
      position: 'absolute',
      top: 2,
      right: 2,
      color: '#000000',
      fontSize: 20,
      cursor: 'pointer',
    },
    '&:hover .remove': {
      display: 'block',
    },
  },
  productName: {
    width: '100%',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
  clearAll: {
    position: 'absolute',
    left: 16,
    bottom: 16,
    cursor: 'pointer',
  },
  matchType: {
    color: '#596772',
  },
});

export const KeywordBucket = (props: any) => {
  const classes = useStyle();
  const { keywords, onRemove } = props;

  const getKeywordBiddingPrice = (keyword: any) => {
    return (
      <Box>
        <CurrencyCode currency={keyword.currency} />{' '}
        {Number(keyword.biddingPrice)}
      </Box>
    );
  };

  const getKeywordMatchType = (keyword) => {
    return (
      <Box className={classes.matchType}>
        {MATCH_TYPE_LABEL[keyword.matchType]}
      </Box>
    );
  };

  return (
    <Box className={classes.root}>
      <Box pb={1.5}>
        <Typography variant="h6">Keyword bucket</Typography>
      </Box>
      <Box className={classes.productWrapper}>
        {keywords.map((item: any, index: number) => {
          return (
            <Box className={classes.product} key={index}>
              <Grid container justify="space-between">
                <Grid item xs={6}>
                  <Typography
                    variant="body2"
                    className={classes.productName}
                    title={item.keywordName}
                  >
                    {item.keywordName}
                  </Typography>
                  <Typography variant="subtitle2" className="line2">
                    {item.keywordId}
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <Box style={{ textAlign: 'right' }}>
                    {getKeywordBiddingPrice(item)}
                    {getKeywordMatchType(item)}
                  </Box>
                </Grid>
              </Grid>
              <CancelIcon
                className="remove"
                onClick={() => onRemove([item])}
              />
            </Box>
          );
        })}
      </Box>
      <Typography
        variant="body2"
        className={classes.clearAll}
        onClick={() => onRemove(keywords)}
      >
        Clear All
      </Typography>
    </Box>
  );
};
