import React from 'react';
import { Provider } from 'react-redux';
import { App } from './App';
import { store } from './redux/store';

export default {
  title: 'Toko/Main',
};

export const Primary: React.FC<{}> = () => {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
};
