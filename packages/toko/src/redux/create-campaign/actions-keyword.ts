import { actionAsync } from '@ep/one/src/utils';
import { MATCH_TYPE } from '@ep/toko/src/constants';
import moment from 'moment';
import * as eff from 'redux-saga/effects';
import { EP } from '../../api/api';
import * as fetch from '../../api/fetch';
import { actionWithRequest, makeAction } from '../util';

const actions = (prefix = 'ADD_KEYWORD') => {
  return {
    updateEntityListingSearch: makeAction({
      actname: `${prefix}/UPDATE_ENTITY_LIST_SEARCH`,
      fun: function ({ searchText }) {
        return { searchText };
      },
    }),
    updateCampaignEntityListPagination: makeAction({
      actname: `${prefix}/UPDATE_CAMPAIGN_ENTITY_LIST_PAGINATION`,
      fun: function ({ page, limit }) {
        return { page, limit };
      },
    }),
    campaignEntityListSelectItem: makeAction({
      actname: `${prefix}/CAMPAIGN_SELECT_ENTITY`,
      fun: function (payload: {
        entityIds: number | number[];
        selected: boolean;
      }) {
        return payload;
      },
    }),
    campaignEntityListSelectAll: makeAction({
      actname: `${prefix}/CAMPAIGN_SELECT_ENTITY_ALL`,
      fun: function ({ isAdd }) {
        return { isAdd };
      },
    }),
    removeCampaignEntity: makeAction({
      actname: `${prefix}/REMOVE_CAMPAIGN_ENTITY`,
      fun: (payload: { entityIds: number | number[] }) => {
        return payload;
      },
    }),
    updateEntityListingSelectedCategory: makeAction({
      actname: `${prefix}/UPDATE_ENTITY_LIST_SELECTED_CATEGORY`,
      fun: function ({ categories }) {
        return { categories };
      },
    }),
    updateAddEntitySelectedCategory: makeAction({
      actname: 'ADD_PRODUCT/UPDATE_ADD_PRODUCT_SELECTED_CATEGORY',
      fun: function ({ categories }) {
        return { categories };
      },
    }),
    getEntityCategoryList: actionWithRequest({
      actname: `${prefix}/GET_CATEGORY_LIST`,
      fun: function* (payload: { shopEid: number }) {
        const rs = yield eff.call(() =>
          fetch.get(EP.GET_CATEGORY_LIST, {
            shop_eids: payload.shopEid,
          }),
        );
        let categoryList = rs.data.map((item: any, index: number) => {
          return {
            id: index,
            text: item.category_name,
          };
        });

        return { categoryList };
      },
    }),
    addEntityAddToBucket: makeAction({
      actname: `${prefix}/ADD_TO_BUCKET`,
      fun: function (payload: {
        entityIds: number | number[];
        isAdded: boolean;
        modification?: {
          dailyBudget?: number;
          matchType?: 'broad_match' | 'exact_match';
        };
      }) {
        return payload;
      },
    }),
    getShopSuggestEntities: actionWithRequest({
      actname: `${prefix}/GET_SHOP_SUGGEST_ENTITY`,
      fun: function* getShopEntities(payload: {
        shopEid: any;
        campaignId?: number;
        productSids: number[];
        searchText?: string;
        isAddKeyword?: boolean;
      }) {
        const rs = yield eff.call(() => {
          return fetch.get(EP.GET_SUGGEST_KEYWORDS, {
            shop_eids: [].concat(payload.shopEid),
            product_sids: payload.productSids,
            value_filter: payload.searchText
              ? payload.searchText
              : undefined,
            limit: 200,
          });
        });

        const entityList = rs.data.map((item: any) => {
          return {
            ...item,
            entityId: item.keyword_id || item.id,
            keywordId: item.keyword_id || item.id,
            keywordName: item.keyword_name,
            searchVolume: item.total_search,
            suggestBid: item.suggest_bidding_price,
            biddingPrice: item.suggest_bidding_price,
            // biddingPrice: item.max_cpc,
            matchType: MATCH_TYPE.BROAD_MATCH,
            currency: 'IDR', // FIXME: hardcode @Hai
          };
        });

        let pagination = rs.pagination;

        return {
          items: entityList,
          page: pagination.page,
          limit: pagination.limit,
          itemCount: pagination.item_count,
          pageTotal: pagination.page_count,
        };
      },
    }),
    addEntitySelectAll: makeAction({
      actname: `${prefix}/MODAL_SELECT_ENTITY_ALL`,
      fun: function ({ isAdd }) {
        return { isAdd };
      },
    }),

    addEntitySelectEntity: makeAction({
      actname: `${prefix}/MODAL_SELECT_ENTITY`,
      fun: function (payload: {
        entityIds: number | number[];
        selected: boolean;
      }) {
        return payload;
      },
    }),
    updateAddEntitySearch: makeAction({
      actname: `${prefix}/UPDATE_ADD_ENTITY_SEARCH`,
      fun: function ({ searchText, currency }) {
        return { searchText, currency };
      },
    }),
    addBucketToCampaignEntity: makeAction({
      actname: `${prefix}/ADD_BUCKET_TO_CAMPAIGN_ENTITY`,
      fun: () => {},
    }),
    resetAddEntity: makeAction({
      actname: `${prefix}/RESET_ADD_ENTITY`,
      fun: function () {
        return {};
      },
    }),
    openAddEntityModal: makeAction({
      actname: `${prefix}/OPEN_ADD_ENTITY_MODAL`,
      fun: ({ visible }: { visible: boolean }) => ({ visible }),
    }),
    updateEntityBiddingPrice: makeAction({
      actname: `${prefix}/UPDATE_ENTITY_BIDDING_PRICE`,
      fun: (payload) => payload,
    }),
    updateEntityMatchType: makeAction({
      actname: `${prefix}/UPDATE_ENTITY_MATCH_TYPE`,
      fun: (payload) => payload,
    }),
    addCustomEntities: makeAction({
      actname: `${prefix}/MODAL_ADD_CUSTOM_ENTITIES`,
      fun: (payload: { keywordNames: string[]; currency: string }) =>
        payload,
    }),
    addEntityUpdateEntityBidPrice: makeAction({
      actname: `${prefix}/MODAL_UPDATE_ENTITY_BID_PRICE`,
      fun: (payload: { biddingPrice: number; matchType: string }) =>
        payload,
    }),
    addEntityUpdateEntitySearch: makeAction({
      actname: `${prefix}/MODAL_UPDATE_SEARCH`,
      fun: function ({ searchText }) {
        return { searchText };
      },
    }),
    addEntityUpdateEntityPagination: makeAction({
      actname: `${prefix}/MODAL_UPDATE_PAGINATION`,
      fun: function ({ pagination }) {
        return { pagination };
      },
    }),
    getCampaignProducts: actionAsync({
      actname: `${prefix}/GET_PRODUCT_OF_CAMPAIGN`,
      fun: function* getCampaignProducts(payload: {
        shopEid: any;
        campaignId: number;
      }) {
        const rs = yield eff.call(() => {
          return fetch.get(EP.GET_PRODUCTS, {
            shop_eids: payload.shopEid,
            campaign_eids: payload.campaignId,
            limit: 200,
            from: moment().format('YYYY-MM-DD'),
            to: moment().format('YYYY-MM-DD'),
          });
        });
        console.info(rs.data);
      },
    }),
  };
};

export { actions };
