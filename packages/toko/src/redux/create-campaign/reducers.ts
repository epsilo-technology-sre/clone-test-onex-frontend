import { produce } from 'immer';
import { orderBy, union } from 'lodash';
import moment from 'moment';
import {
  CURRENCY_LIMITATION,
  MATCH_TYPE,
  MAX_KEYWORDS_OF_PRODUCT,
} from '../../constants';
import { actions } from './actions';
import { reducerAddKeyword } from './reducer-add-keyword';
import { reducerAddProduct as reducerSetupProduct } from './reducer-add-product';

export type CreateCampaignState = {
  currentScreen: 'setupCampaign' | 'setupProduct' | 'setupKeyword';
  flow: 'createCampaign' | 'addProduct' | 'addKeyword';
  setupCampaign: Partial<{
    shopList: {
      shopId: number;
      shopName: string;
      shopCurrency: string;
    }[];
    shopCurrency: string;
    shop: number;
    campaignList: any[];
    campaignId: number;
    campaignName: string;
    budget: {
      daily: number;
      isNoLimit: boolean;
    };
    price: number;
  }>;
  setupProduct: {
    modalAddProductVisible: boolean;
    productList: {
      searchText: string;
      categories: any[];
      selectedCategories: any[];
      items: any[];
      page: number;
      limit: number;
      pageTotal: number;
      itemCount: number;
      selectedIds: number[];
    };
    productBucket: any[];
    campaignProductList: {
      searchText: string;
      categories: any[];
      selectedCategories: any[];
      items: any[];
      page: number;
      limit: number;
      itemCount: number;
      selectedIds: number[];
    };
    productKeywords: any[];
  };
  setupKeyword: {
    modalAddEntityVisible: boolean;
    shops: any[];
    campaigns: any[];
    entityList: {
      searchText: string;
      categories: any[];
      selectedCategories: any[];
      items: any[];
      page: number;
      limit: number;
      pageTotal: number;
      itemCount: number;
      selectedIds: number[];
      suggestedItems: any[];
      customItems: any[];
    };
    entityBucket: any[];
    campaignEntityList: {
      searchText: string;
      categories: any[];
      selectedCategories: any[];
      items: any[];
      page: number;
      limit: number;
      itemCount: number;
      selectedIds: number[];
    };
  };
  addKeyword?: {
    modalAddKeywordVisible: boolean;
    products: any[];
    suggestKeywords: any[];
    keywords: any[];
    selectedProducts: any[];
    selectedKeywords: any[];
    customKeywords: any[];
    keywordBucket: any[];
    searchText: string;
    pagination: any;
  };
  loading: {
    [key: string]: {
      status: boolean;
      error: any;
    };
  };
};

export const initState: CreateCampaignState = {
  currentScreen: 'setupCampaign',
  flow: 'createCampaign',
  setupCampaign: {
    shopList: [],
    shop: 0,
    campaignList: [],
    campaignId: 0,
    campaignName: '',
    shopCurrency: 'VND',
    budget: {
      total: 201,
      daily: 101,
      isOnDaily: false,
      isNoLimit: true,
    },
    timeline: {
      startDate: moment().format('DD/MM/YYYY'),
      endDate: moment().format('DD/MM/YYYY'),
      isNoLimit: false,
    },
  },
  setupProduct: {
    modalAddProductVisible: false,
    productList: {
      searchText: '',
      categories: [],
      selectedCategories: [],
      items: [],
      page: 1,
      limit: 10,
      pageTotal: 1,
      itemCount: 1,
      selectedIds: [],
    },
    productBucket: [],
    campaignProductList: {
      categories: [],
      searchText: '',
      selectedCategories: [],
      items: [],
      page: 1,
      limit: 10,
      itemCount: 1,
      selectedIds: [],
    },
    productKeywords: [],
  },
  setupKeyword: {
    modalAddEntityVisible: false,
    entityList: {
      searchText: '',
      categories: [],
      selectedCategories: [],
      items: [],
      page: 1,
      limit: 10,
      pageTotal: 1,
      itemCount: 1,
      selectedIds: [],
      suggestedItems: [],
      customItems: [],
    },
    entityBucket: [],
    campaignEntityList: {
      categories: [],
      searchText: '',
      selectedCategories: [],
      items: [],
      page: 1,
      limit: 10,
      itemCount: 1,
      selectedIds: [],
    },
  },
  addKeyword: {
    modalAddKeywordVisible: false,
    products: [],
    suggestKeywords: [],
    keywords: [],
    customKeywords: [],
    keywordBucket: [],
    selectedProducts: [],
    selectedKeywords: [],
    searchText: '',
    pagination: {
      pageSize: 10,
      currentPage: 1,
      total: 0,
    },
  },
  loading: {},
};

export function reducer(
  state: CreateCampaignState = initState,
  action: { type: string; payload?: any },
) {
  switch (action.type) {
    case 'REQUEST_ACTION_LOADING': {
      state = produce(state, (draft) => {
        draft.loading[action.payload.loading.section] = {
          status: action.payload.loading.status,
          error: action.payload.loading.error,
        };
      });
      break;
    }
    case actions.switchScreen.type(): {
      state = produce(state, (draft) => {
        draft.currentScreen = action.payload.screen;
      });
      break;
    }
    case actions.getShopList.fetchType(): {
      state = produce(state, (draft) => {
        draft.setupCampaign.shopList = action.payload.shopList;
      });
      break;
    }
    case actions.getCampaignList.fetchType(): {
      state = produce(state, (draft) => {
        draft.setupCampaign.campaignList =
          action.payload.campaignList;
        draft.setupCampaign.campaignId = null;
      });
      break;
    }
    case actions.addProductSelectShop.type(): {
      state = produce(state, (draft) => {
        const shop = state.setupCampaign.shopList.find(
          (i) => i.shopId === action.payload.shopId,
        );
        draft.setupCampaign.shop = action.payload.shopId;
        draft.setupCampaign.shopCurrency = shop
          ? shop.shopCurrency
          : '';
      });
      break;
    }
    case actions.addProductSelectCampaign.type(): {
      state = produce(state, (draft) => {
        draft.setupCampaign.campaignId = action.payload.campaignId;
      });
      break;
    }
    case actions.getCategoryList.fetchType(): {
      state = produce(state, (draft) => {
        draft.setupProduct.productList.categories =
          action.payload.categoryList;
        draft.setupProduct.productList.selectedCategories =
          action.payload.categoryList;
        draft.setupKeyword.entityList.categories =
          action.payload.categoryList;
        draft.setupKeyword.entityList.selectedCategories =
          action.payload.categoryList;
      });
      break;
    }
    case actions.setShopCurrency.type(): {
      state = produce(state, (draft) => {
        draft.setupCampaign.shopCurrency = action.payload.currency;
      });
      break;
    }
    case actions.setCampaignInfo.type(): {
      state = produce(state, (draft) => {
        draft.setupCampaign = {
          ...state.setupCampaign,
          ...action.payload.info,
        };
        draft.currentScreen = 'setupProduct';
      });
      break;
    }
    case actions.openAddKeywordModal.type(): {
      state = produce(state, (draft) => {
        draft.addKeyword.modalAddKeywordVisible =
          action.payload.visible;
      });
      break;
    }
    case actions.resetAddKeyword.type(): {
      state = produce(state, (draft) => {
        draft.addKeyword = {
          products: [],
          suggestKeywords: [],
          keywords: [],
          customKeywords: [],
          keywordBucket: [],
          selectedProducts: [],
          selectedKeywords: [],
          searchText: '',
          pagination: {
            pageSize: 10,
            currentPage: 1,
            total: 0,
          },
        };
      });
      break;
    }
    case actions.setProductBucket.type(): {
      state = produce(state, (draft) => {
        draft.setupProduct.productBucket =
          action.payload.productBucket;
      });
      break;
    }
    case actions.setProductsForAddKeywordById.type(): {
      state = produce(state, (draft) => {
        const productIds = [].concat(action.payload.productIds);
        const items = state.setupProduct.campaignProductList.items.filter(
          (item) => productIds.includes(item.productId),
        );
        draft.addKeyword.products = items;
        draft.addKeyword.selectedProducts = items;
      });
      break;
    }
    case actions.setProductsForAddKeyword.type(): {
      state = produce(state, (draft) => {
        const items = [].concat(action.payload.products);
        draft.addKeyword.products = items;
        draft.addKeyword.selectedProducts = items;
      });
      break;
    }
    case actions.updateCampaignProductListPagination.type(): {
      state = produce(state, (draft) => {
        draft.setupProduct.campaignProductList.page =
          action.payload.page;
        draft.setupProduct.campaignProductList.limit =
          action.payload.limit;
      });
      break;
    }
    case actions.updateProductKeywords.type(): {
      state = produce(state, (draft) => {
        const { productKeywordList } = action.payload;
        const newList = JSON.parse(
          JSON.stringify(state.setupProduct.productKeywords || []),
        );

        productKeywordList.forEach((kwItem: any) => {
          const existItem = newList.find(
            (item: any) =>
              item.product.productId === kwItem.product.productId,
          );
          if (existItem) {
            // Add list keyword to exist item (make sure keyword limitation of a product is 200)
            if (
              existItem.product.totalKeyword +
                existItem.keywords.length <
              MAX_KEYWORDS_OF_PRODUCT
            ) {
              const existKeywordIds = new Set(
                existItem.keywords.map((kw: any) => kw.keywordId),
              );
              const newKeywords = [
                ...existItem.keywords,
                ...kwItem.keywords.filter(
                  (kw: any) => !existKeywordIds.has(kw.keywordId),
                ),
              ];
              existItem.keywords = orderBy(
                newKeywords,
                ['searchVolume'],
                ['desc'],
              ).slice(0, MAX_KEYWORDS_OF_PRODUCT);
            }
          } else {
            // Add item with list keywords
            const newKeywords = orderBy(
              [...kwItem.keywords],
              ['searchVolume'],
              ['desc'],
            ).slice(
              0,
              MAX_KEYWORDS_OF_PRODUCT - kwItem.product.totalKeyword,
            );
            newList.push({
              product: kwItem.product,
              keywords: newKeywords,
            });
          }
        });
        draft.setupProduct.productKeywords = newList;

        // Update total
        const newProducts = JSON.parse(
          JSON.stringify(
            state.setupProduct.campaignProductList.items,
          ),
        );
        newProducts.forEach((product: any) => {
          const exist = newList.find(
            (item: any) =>
              item.product.productId === product.productId,
          );
          if (exist) {
            product.totalKeyword += exist.keywords.length;
          }
        });

        draft.setupProduct.campaignProductList.items = newProducts;
      });
      break;
    }
    case actions.getSuggestKeywords.fetchType(): {
      state = produce(state, (draft) => {
        const { suggestKeywords } = action.payload;
        draft.addKeyword.suggestKeywords = suggestKeywords;
        draft.addKeyword.keywords = suggestKeywords;
        draft.addKeyword.pagination.total = suggestKeywords.length;
      });
      break;
    }
    case actions.addCustomKeywords.type(): {
      state = produce(state, (draft) => {
        const { keywordNames, currency } = action.payload;
        const {
          suggestKeywords,
          customKeywords,
          searchText,
          pagination,
        } = state.addKeyword;

        let minValue = 0;
        if (CURRENCY_LIMITATION[currency]) {
          minValue = CURRENCY_LIMITATION[currency].BROAD_MATCH.MIN;
        }

        const uniqNames = union(keywordNames);

        if (uniqNames.length > 0) {
          const existNames = customKeywords
            .concat(suggestKeywords)
            .map((i) => i.keywordName);
          const startIdIndex =
            customKeywords.length + MAX_KEYWORDS_OF_PRODUCT + 1;

          const newKeywords: any[] = uniqNames
            .filter((i) => !existNames.includes(i))
            .map((item: string, index: number) => ({
              keywordId: startIdIndex + index,
              keywordName: item,
              searchVolume: 0,
              currency: currency,
              suggestBid: minValue,
              biddingPrice: minValue,
              matchType: MATCH_TYPE.BROAD_MATCH,
            }))
            .concat(customKeywords);
          const allKeywords = [...newKeywords, ...suggestKeywords];
          const filteredKeywords = allKeywords.filter((item) =>
            item.keywordName.includes(searchText),
          );
          const rowsOfCurrentPage = filteredKeywords.slice(
            0,
            pagination.pageSize,
          );
          draft.addKeyword.keywords = rowsOfCurrentPage;
          draft.addKeyword.customKeywords = newKeywords;
          draft.addKeyword.pagination.total = filteredKeywords.length;
          draft.addKeyword.pagination.currentPage = 1;
        }
      });
      break;
    }
    case actions.selectKeyword.type(): {
      state = produce(state, (draft) => {
        const { keyword, isAdd } = action.payload;
        let newList = [];
        if (isAdd) {
          newList = state.addKeyword.selectedKeywords.concat(keyword);
        } else {
          newList = state.addKeyword.selectedKeywords.filter(
            (item: any) => item.keywordId !== keyword.keywordId,
          );
        }

        draft.addKeyword.selectedKeywords = newList;
      });
      break;
    }
    case actions.selectAllKeywords.type(): {
      state = produce(state, (draft) => {
        let newList: any[] = [];
        if (action.payload.isAdd) {
          newList = [
            ...state.addKeyword.customKeywords,
            ...state.addKeyword.suggestKeywords,
          ];
        }
        draft.addKeyword.selectedKeywords = newList;
      });
      break;
    }
    case actions.updateSelectedKeywordPrice.type(): {
      state = produce(state, (draft) => {
        const { biddingPrice, matchType } = action.payload;
        const keywordIds = new Set(
          state.addKeyword.selectedKeywords.map(
            (item) => item.keywordId,
          ),
        );
        draft.addKeyword.suggestKeywords = state.addKeyword.suggestKeywords.map(
          (item) => {
            if (keywordIds.has(item.keywordId)) {
              return {
                ...item,
                biddingPrice,
                matchType,
              };
            }
            return item;
          },
        );
        draft.addKeyword.keywordBucket = state.addKeyword.keywordBucket.map(
          ({ keywords, product }) => {
            const newKeywords = keywords.map((k) => {
              if (keywordIds.has(k.keywordId)) {
                return {
                  ...k,
                  biddingPrice,
                  matchType,
                };
              }
              return k;
            });
            return {
              product,
              keywords: newKeywords,
            };
          },
        );
        draft.addKeyword.customKeywords = state.addKeyword.customKeywords.map(
          (item) => {
            if (keywordIds.has(item.keywordId)) {
              return {
                ...item,
                biddingPrice,
                matchType,
              };
            }
            return item;
          },
        );
        draft.addKeyword.keywords = state.addKeyword.keywords.map(
          (item) => {
            if (keywordIds.has(item.keywordId)) {
              return {
                ...item,
                biddingPrice,
                matchType,
              };
            }
            return item;
          },
        );
        draft.addKeyword.selectedKeywords = state.addKeyword.selectedKeywords.map(
          (item) => {
            return {
              ...item,
              biddingPrice,
              matchType,
            };
          },
        );
      });
      break;
    }
    case actions.updateSelectedProducts.type(): {
      state = produce(state, (draft) => {
        const { product } = action.payload;
        const { selectedProducts } = state.addKeyword;
        const isExist = selectedProducts.some(
          (item) => item.productId === product.productId,
        );
        if (isExist) {
          if (selectedProducts.length > 1) {
            draft.addKeyword.selectedProducts = selectedProducts.filter(
              (item) => item.productId !== product.productId,
            );
          }
        } else {
          draft.addKeyword.selectedProducts = selectedProducts.concat(
            product,
          );
        }
      });
      break;
    }
    case actions.addKeywordToBucket.type(): {
      state = produce(state, (draft) => {
        const { keywords } = action.payload;
        const { selectedProducts, keywordBucket } = state.addKeyword;

        if (selectedProducts.length > 0) {
          let newBucket = JSON.parse(JSON.stringify(keywordBucket));

          selectedProducts.forEach((product) => {
            const existItem = newBucket.find(
              (item: any) =>
                item.product.productId === product.productId,
            );
            if (existItem) {
              // Add list keyword to exist item (make sure keyword limitation of a product is 200)
              if (
                existItem.product.totalKeyword +
                  existItem.keywords.length <
                MAX_KEYWORDS_OF_PRODUCT
              ) {
                const existKeywordIds = new Set(
                  existItem.keywords.map((kw: any) => kw.keywordId),
                );
                const newKeywords = [
                  ...existItem.keywords,
                  ...keywords.filter(
                    (kw: any) => !existKeywordIds.has(kw.keywordId),
                  ),
                ];
                existItem.keywords = orderBy(
                  newKeywords,
                  ['searchVolume'],
                  ['desc'],
                ).slice(0, MAX_KEYWORDS_OF_PRODUCT);
              }
            } else {
              // Add item with list keywords
              const newKeywords = orderBy(
                [...keywords],
                ['searchVolume'],
                ['desc'],
              ).slice(
                0,
                MAX_KEYWORDS_OF_PRODUCT - product.totalKeyword,
              );
              newBucket.push({
                product,
                keywords: newKeywords,
              });
            }
          });

          draft.addKeyword.keywordBucket = newBucket;
        }
      });
      break;
    }
    case actions.removeKeyword.type(): {
      state = produce(state, (draft) => {
        const { keyword, product } = action.payload;
        let newBucket = JSON.parse(
          JSON.stringify(state.addKeyword.keywordBucket),
        );
        const existItem = newBucket.find(
          (item: any) => item.product.productId === product.productId,
        );
        if (existItem) {
          existItem.keywords = existItem.keywords.filter(
            (item: any) => item.keywordId !== keyword.keywordId,
          );

          if (existItem.keywords.length === 0) {
            newBucket = newBucket.filter(
              (item: any) =>
                item.product.productId !==
                existItem.product.productId,
            );
          }
          draft.addKeyword.keywordBucket = newBucket;
        }
      });
      break;
    }
    case actions.removeAllKeywords.type(): {
      state = produce(state, (draft) => {
        draft.addKeyword.keywordBucket = [];
      });
      break;
    }
    case actions.updateKeywordSearch.type(): {
      state = produce(state, (draft) => {
        const { currency } = action.payload;
        const searchText = action.payload.searchText.toLowerCase();
        const {
          customKeywords,
          suggestKeywords,
          pagination,
        } = state.addKeyword;
        const allKeywords = [...customKeywords, ...suggestKeywords];
        let filteredKeywords = allKeywords.filter((item) =>
          item.keywordName.includes(searchText),
        );

        let minValue = 0;
        if (CURRENCY_LIMITATION[currency]) {
          minValue = CURRENCY_LIMITATION[currency].BROAD_MATCH.MIN;
        }

        if (
          filteredKeywords.length === 0 &&
          searchText.length > 0 &&
          !/[~!@#$%^&*()/\-+={}|[/\]\:;"'<>./?`_/\\]/gi.test(
            searchText,
          )
        ) {
          const existNames = customKeywords.map((i) => i.keywordName);
          const startIdIndex =
            customKeywords.length + MAX_KEYWORDS_OF_PRODUCT + 1;

          const newKeywords: any[] = [searchText]
            .filter((i) => !existNames.includes(i))
            .map((item: string, index: number) => ({
              keywordId: startIdIndex + index,
              keywordName: item,
              searchVolume: 0,
              currency: currency,
              suggestBid: minValue,
              biddingPrice: minValue,
              matchType: 'Broad Match',
            }))
            .concat(customKeywords);
          draft.addKeyword.customKeywords = newKeywords.concat(
            filteredKeywords,
          );
          filteredKeywords = filteredKeywords.concat(newKeywords);
        }

        const rowsOfCurrentPage = filteredKeywords.slice(
          0,
          pagination.pageSize,
        );
        draft.addKeyword.searchText = action.payload.searchText;
        draft.addKeyword.keywords = rowsOfCurrentPage;
        draft.addKeyword.pagination.total = filteredKeywords.length;
        draft.addKeyword.pagination.currentPage = 1;
      });
      break;
    }
    case actions.updateKeywordPagination.type(): {
      state = produce(state, (draft) => {
        const { pagination } = action.payload;
        const {
          customKeywords,
          suggestKeywords,
          searchText,
        } = state.addKeyword;
        const allKeywords = [...customKeywords, ...suggestKeywords];
        const filteredKeywords = allKeywords.filter((item) =>
          item.keywordName.includes(searchText),
        );
        const start =
          (pagination.currentPage - 1) * pagination.pageSize;
        const end = start + pagination.pageSize;
        const rowsOfCurrentPage = filteredKeywords.slice(start, end);
        draft.addKeyword.keywords = rowsOfCurrentPage;
        draft.addKeyword.pagination = pagination;
      });
      break;
    }
    case actions.setFlow.type(): {
      state = produce(state, (draft) => {
        draft.flow = action.payload.flow;
      });
      break;
    }
    case actions.createCampaign.fetchType(): {
      state = produce(state, (draft) => {
        const { result } = action.payload;
        draft.loading.createCampaign = {
          status: result.success,
          error: result.message,
        };
      });
      break;
    }
    case actions.saveAddKeywords.fetchType(): {
      state = produce(state, (draft) => {
        const { result } = action.payload;
        draft.loading.createCampaign = {
          status: result.success,
          error: result.message,
        };
      });
      break;
    }
    case actions.getCampaignProducts.fetchType(): {
      const { payload } = action;
      state = produce(state, (draft) => {
        draft.setupProduct.campaignProductList.items = payload.data;
      });
      break;
    }
  }
  state = {
    ...state,
    setupProduct: reducerSetupProduct(state.setupProduct, action),
    setupKeyword: reducerAddKeyword(state.setupKeyword, action),
  };

  return state;
}
