import { actionAsync } from '@ep/one/src/utils';
import { MATCH_TYPE } from '@ep/shopee/src/constants';
import { CHANNEL } from '@ep/toko/src/constants';
import { sortList } from '@ep/toko/src/utils/utils';
import moment from 'moment';
import * as eff from 'redux-saga/effects';
import {
  addKeywords,
  createCampaign,
  EP,
  getSuggestKeywords,
} from '../../api/api';
import * as fetch from '../../api/fetch';
import { actionWithRequest, makeAction } from '../util';
import { actions as makeKeywordActions } from './actions-keyword';

const actions = {
  switchScreen: makeAction({
    actname: 'SWITCH_SCREEN',
    fun: (payload: { screen: string }) => payload,
  }),
  getShopList: actionWithRequest({
    actname: 'GET_SHOP_LIST',
    fun: function* () {
      const rs = yield eff.call(() =>
        fetch.get(EP.GET_SHOPS, { channel_code: CHANNEL }),
      );
      let shopList = rs.data.map((i: any) => {
        return {
          shopId: i.shop_eid,
          shopName: `${i.country_code} / ${i.shop_name}`,
          shopCurrency: i.country_exchange,
          channelCode: i.channel_code,
          channelId: i.channel_id,
        };
      });
      return { shopList: sortList(shopList, 'shopName') };
    },
  }),
  setShopCurrency: makeAction({
    actname: 'SET_SHOP_CURRENCY',
    fun: function ({ currency }) {
      return { currency };
    },
  }),
  setCampaignInfo: makeAction({
    actname: 'SET_CAMPAING_INFO',
    fun: function ({ info }) {
      return { info };
    },
  }),
  openAddProductModal: makeAction({
    actname: 'ADD_PRODUCT/OPEN_ADD_PRODUCT_MODAL',
    fun: ({ visible }: { visible: boolean }) => ({ visible }),
  }),
  getCampaignProducts: actionAsync({
    actname: 'ADD_PRODUCT/GET_PRODUCT_OF_CAMPAIGN',
    fun: function* getCampaignProducts(payload: {
      shopEid: any;
      campaignId: number;
    }) {
      const rs = yield eff.call(() => {
        return fetch.get(EP.GET_PRODUCTS, {
          shop_eids: payload.shopEid,
          campaign_eids: payload.campaignId,
          limit: 200,
          from: moment().format('YYYY-MM-DD'),
          to: moment().format('YYYY-MM-DD'),
        });
      });
      console.info(rs.data);

      return {
        data: rs.data.map((i) => ({
          productName: i.product_name,
          categoryName: i.category_name,
          currency: i.currency,
          priceRRP: i.price_rrp,
          pricePostsub: i.price_postsub,
          productStock: i.product_stock,
          updatedAt: i.updated_at,
          discount: i.discount,
          totalKeyword: i.totalKeyword,
          productId: Number(i.product_eid),
          productSId: Number(i.product_sid),
          itemSold: i.item_sold,
          totalBudget: 0,
          dailyBudget: 0,
        })),
      };
    },
  }),
  getShopProducts: actionWithRequest({
    actname: 'ADD_PRODUCT/GET_SHOP_PRODUCTS',
    fun: function* getShopProducts(payload: {
      shopEid: any;
      campaignId?: number;
      limit: number;
      page: number;
      categoryNames: string;
      searchText: string;
      from: string;
      to: string;
      orderBy: string;
      orderMode: string;
      isAddKeyword?: boolean;
    }) {
      const rs = yield eff.call(() => {
        return fetch.get(EP.GET_SHOP_PRODUCTS, {
          shop_eids: [].concat(payload.shopEid),
          campaign_eid: payload.campaignId,
          limit: payload.limit,
          page: payload.page,
          category_names: payload.categoryNames,
          value_filter: payload.searchText,
          // time_line_from: payload.from,
          // time_line_to:
          //   payload.to === 'No limit' ? 'no_limit' : payload.to,
          order_by: payload.orderBy,
          order_mode: payload.orderMode,
          is_add_keyword: payload.isAddKeyword ? 1 : 0,
        });
      });

      const today = moment().format('DD/MM/YYYY');

      const productList = rs.data.map((i: any) => ({
        productName: i.product_name,
        categoryName: i.category_name,
        currency: i.currency,
        priceRRP: i.price_rrp,
        pricePostsub: i.price_postsub,
        productStock: i.product_stock,
        updatedAt: i.updated_at || today,
        discount: i.discount,
        totalKeyword: i.totalKeyword,
        productId: Number(i.product_eid),
        productSId: Number(i.product_sid),
        itemSold: i.item_sold,
        totalBudget: 0,
        dailyBudget: 0,
      }));

      let pagination = rs.pagination;

      return {
        items: productList,
        page: pagination.page,
        limit: pagination.limit,
        itemCount: pagination.item_count,
        pageTotal: pagination.page_count,
      };
    },
  }),
  addProductSelectProduct: makeAction({
    actname: 'ADD_PRODUCT/SELECT_PRODUCT',
    fun: function (payload: {
      shopProductIds: number | number[];
      selected: boolean;
    }) {
      return payload;
    },
  }),
  updateAddProductSearch: makeAction({
    actname: 'ADD_PRODUCT/UPDATE_ADD_PRODUCT_SEARCH',
    fun: function ({ searchText }) {
      return { searchText };
    },
  }),
  updateAddProductSelectedCategory: makeAction({
    actname: 'ADD_PRODUCT/UPDATE_ADD_PRODUCT_SELECTED_CATEGORY',
    fun: function ({ categories }) {
      return { categories };
    },
  }),
  updateProductListingSearch: makeAction({
    actname: 'ADD_PRODUCT/UPDATE_PRODUCT_LIST_SEARCH',
    fun: function ({ searchText }) {
      return { searchText };
    },
  }),
  updateProductListingSelectedCategory: makeAction({
    actname: 'ADD_PRODUCT/UPDATE_PRODUCT_LIST_SELECTED_CATEGORY',
    fun: function ({ categories }) {
      return { categories };
    },
  }),
  getCampaignList: actionWithRequest({
    actname: 'ADD_PRODUCT/GET_CAMPAIGNS',
    fun: function* (payload: any) {
      const { shopEid, limit, page, from, to } = payload;
      const rs = yield eff.call(() =>
        fetch.get(EP.GET_CAMPAIGNS, {
          shop_eids: shopEid,
          is_from_create_campaign: 1,
          limit,
          page,
          from,
          to,
        }),
      );
      let campaignList = rs.data.map((i: any) => {
        return {
          campaignId: i.campaign_eid,
          campaignName: i.campaign_name,
          timelineFrom: i.timeline_from,
          timelineTo: i.timeline_to,
        };
      });
      return { campaignList };
    },
  }),
  addProductSelectShop: makeAction({
    actname: 'ADD_PRODUCT/SELECT_SHOP',
    fun: function ({ shopId }) {
      return { shopId };
    },
  }),
  addProductSelectCampaign: makeAction({
    actname: 'ADD_PRODUCT/SELECT_CAMPAIGN',
    fun: function ({ campaignId }) {
      return { campaignId };
    },
  }),
  addProductSelectAll: makeAction({
    actname: 'ADD_PRODUCT/SELECT_PRODUCT_ALL',
    fun: function ({ isAdd }) {
      return { isAdd };
    },
  }),
  campaignProductListSelectItem: makeAction({
    actname: 'PRODUCT_LISTING/SELECT_PRODUCT',
    fun: function (payload: {
      productIds: number | number[];
      selected: boolean;
    }) {
      return payload;
    },
  }),
  campaignProductListSelectAll: makeAction({
    actname: 'PRODUCT_LISTING/SELECT_PRODUCT_ALL',
    fun: function ({ isAdd }) {
      return { isAdd };
    },
  }),
  addProductAddToBucket: makeAction({
    actname: 'ADD_PRODUCT/ADD_TO_BUCKET',
    fun: function (payload: {
      shopProductIds: number | number[];
      isAdded: boolean;
      modification?: {
        totalBudget?: number;
        dailyBudget?: number;
      };
    }) {
      return payload;
    },
  }),
  addBucketToCampaignProduct: makeAction({
    actname: 'ADD_PRODUCT/ADD_BUCKET_TO_CAMPAIGN_PRODUCT',
    fun: () => {},
  }),
  removeCampaignProduct: makeAction({
    actname: 'ADD_PRODUCT/REMOVE_CAMPAING_PRODUCT',
    fun: (payload: { productIds: number | number[] }) => {
      return payload;
    },
  }),
  openAddKeywordModal: makeAction({
    actname: 'OPEN_ADD_KEYWORD_MODAL',
    fun: ({ visible }: { visible: boolean }) => ({ visible }),
  }),
  resetAddKeyword: makeAction({
    actname: 'RESET_ADD_KEYWORD',
    fun: function () {
      return {};
    },
  }),
  getCategoryList: actionWithRequest({
    actname: 'GET_CATEGORY_LIST',
    fun: function* (payload: { shopEid: number }) {
      const rs = yield eff.call(() =>
        fetch.get(EP.GET_CATEGORY_LIST, {
          shop_eids: payload.shopEid,
        }),
      );
      let categoryList = rs.data.map((item: any, index: number) => {
        return {
          id: index,
          text: item.category_name,
        };
      });

      return { categoryList };
    },
  }),
  resetAddProduct: makeAction({
    actname: 'RESET_ADD_PRODUCT',
    fun: function () {
      return {};
    },
  }),
  setProductsForAddKeywordById: makeAction({
    actname: 'SET_PRODUCTS_FOR_ADD_KEYWORD_BY_ID',
    fun: function ({ productIds }) {
      return { productIds };
    },
  }),
  setProductsForAddKeyword: makeAction({
    actname: 'SET_PRODUCTS_FOR_ADD_KEYWORD',
    fun: function ({ products }) {
      return { products };
    },
  }),
  updateCampaignProductListPagination: makeAction({
    actname: 'UPDATE_CAMPAIGN_PRODUCT_LIST_PAGINATION',
    fun: function ({ page, limit }) {
      return { page, limit };
    },
  }),
  setProductBucket: makeAction({
    actname: 'SET_PRODUCT_BUCKET',
    fun: function ({ productBucket }) {
      return {
        productBucket,
      };
    },
  }),
  updateProductKeywords: makeAction({
    actname: 'UPDATE_PRODUCT_KEYWORDS',
    fun: function ({ productKeywordList }) {
      return { productKeywordList };
    },
  }),
  getSuggestKeywords: actionWithRequest({
    actname: 'GET_SUGGEST_KEYWORDS',
    fun: function* (payload: {
      shopId: number;
      productSIds: string;
    }) {
      const response = yield eff.call(() =>
        getSuggestKeywords({
          shop_eids: payload.shopId,
          product_sids: payload.productSIds,
        }),
      );
      const suggestKeywords = response.data.map((item: any) => {
        return {
          keywordId: item.id,
          keywordName: item.keyword_name,
          searchVolume: item.search_volume,
          currency: 'VND',
          suggestBid: item.suggest_bidding_price,
          biddingPrice: item.suggest_bidding_price,
          matchType: 'Broad Match',
          ...item,
        };
      });
      return { suggestKeywords };
    },
  }),
  selectKeyword: makeAction({
    actname: 'SELECT_KEYWORD',
    fun: function ({ keyword, isAdd }) {
      return {
        keyword,
        isAdd,
      };
    },
  }),
  selectAllKeywords: makeAction({
    actname: 'SELECT_ALL_KEYWORDS',
    fun: function ({ isAdd }) {
      return {
        isAdd,
      };
    },
  }),
  addCustomKeywords: makeAction({
    actname: 'ADD_CUSTOM_KEYWORDS',
    fun: function ({ keywordNames, currency }) {
      return {
        keywordNames,
        currency,
      };
    },
  }),
  updateSelectedProducts: makeAction({
    actname: 'UPDATE_SELECTED_PRODUCTS',
    fun: function ({ product }) {
      return {
        product,
      };
    },
  }),
  addKeywordToBucket: makeAction({
    actname: 'ADD_KEYWORD_TO_BUCKET',
    fun: function ({ keywords }) {
      return { keywords };
    },
  }),
  removeKeyword: makeAction({
    actname: 'REMOVE_KEYWORD',
    fun: function ({ keyword, product }) {
      return { keyword, product };
    },
  }),
  removeAllKeywords: makeAction({
    actname: 'REMOVE_ALL_KEYWORDS',
    fun: function () {
      return true;
    },
  }),
  updateKeywordSearch: makeAction({
    actname: 'UPDATE_KEYWOR D_SEARCH',
    fun: function ({ searchText, currency }) {
      return { searchText, currency };
    },
  }),
  updateKeywordPagination: makeAction({
    actname: 'UPDATE_KEYWORD_PAGINATION',
    fun: function ({ pagination }) {
      return { pagination };
    },
  }),
  updateSelectedKeywordPrice: makeAction({
    actname: 'UPDATE_SELECTED_KEYWORD_PRICE',
    fun: function ({ biddingPrice, matchType }) {
      return { biddingPrice, matchType };
    },
  }),
  setFlow: makeAction({
    actname: 'SET_FLOW',
    fun: function ({ flow }) {
      return { flow };
    },
  }),
  createCampaign: actionWithRequest({
    actname: 'CREATE_CAMPAIGN',
    fun: function* (params: {
      setupCampaign: any;
      setupProduct: any;
      setupKeyword: any;
      promise: { resolve: any; reject: any };
    }) {
      try {
        let {
          setupCampaign,
          setupKeyword,
          setupProduct,
          promise = {
            resolve: Function.prototype,
            reject: Function.prototype,
          },
        } = params;

        const postData = {
          shop_eids: [].concat(setupCampaign.shop),
          campaign_name: setupCampaign.campaignName,
          campaign_budget: setupCampaign.budget.isNoLimit
            ? 0
            : setupCampaign.budget.daily,
          campaign_max_cpc: Number(setupCampaign.price),
          product_list: setupProduct.campaignProductList.items.map(
            (i: any) => ({ product_id: i.productSId }),
          ),
          keywords: setupKeyword.campaignEntityList.items.map(
            (i: any) => ({
              keyword_name: i.keywordName,
              max_cpc: Number(i.biddingPrice),
              status: 1,
            }),
          ),
        };

        const result = yield eff.call(() => createCampaign(postData));
        params.promise.resolve(result);
        return { result };
      } catch (error) {
        params.promise.reject(error);
        return { result: error };
      }
    },
  }),
  updateCampaign: actionWithRequest({
    actname: 'UPDATE_CAMPAIGN',
    fun: function* (params: {
      setupCampaign: any;
      productList?: any;
      keywordList?: any;
      promise: { resolve: any; reject: any };
    }) {
      try {
        let {
          setupCampaign,
          promise = {
            resolve: Function.prototype,
            reject: Function.prototype,
          },
        } = params;

        const postData = {
          shop_eids: [].concat(setupCampaign.shop),
          campaign_eid: setupCampaign.campaignId,
          product_list: params.productList
            ? params.productList.map((i: any) => ({
                product_id: i.productSId,
              }))
            : undefined,
          keywords: params.keywordList
            ? params.keywordList.map((i: any) => ({
                keyword_name: i.keywordName,
                max_cpc: Number(i.biddingPrice),
                status:
                  i.matchType === MATCH_TYPE.BROAD_MATCH ? 1 : 0,
                match_type: i.matchType,
              }))
            : undefined,
        };

        const result = yield eff.call(() => createCampaign(postData));
        params.promise.resolve(result);
        return { result };
      } catch (error) {
        params.promise.reject(error);
        return { result: error };
      }
    },
  }),
  saveAddKeywords: actionWithRequest({
    actname: 'SAVE_ADD_KEYWORDS',
    fun: function* ({ params }) {
      try {
        const result = yield eff.call(() => addKeywords(params));
        return { result };
      } catch (error) {
        return { result: error };
      }
    },
  }),
};

let combinedActions = {
  ...actions,
  addKeyword: makeKeywordActions(),
};

export { combinedActions as actions };

export function* rootSaga() {
  yield eff.all([
    actions.getShopList.saga(),
    actions.getCampaignList.saga(),
    actions.getCategoryList.saga(),
    actions.getSuggestKeywords.saga(),
    actions.getShopProducts.saga(),
    actions.createCampaign.saga(),
    actions.saveAddKeywords.saga(),
    actions.getCampaignProducts.saga(),
    actions.updateCampaign.saga(),
    combinedActions.addKeyword.getEntityCategoryList.saga(),
    combinedActions.addKeyword.getShopSuggestEntities.saga(),
  ]);
}
