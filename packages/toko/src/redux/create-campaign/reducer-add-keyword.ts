import { produce } from 'immer';
import { CURRENCY_LIMITATION, MATCH_TYPE, MAX_KEYWORDS_OF_PRODUCT } from '../../constants';
import { actions } from './actions';
import { CreateCampaignState } from './reducers';

export function reducerAddKeyword(
  state: CreateCampaignState['setupKeyword'],
  action: { type: string; payload?: any },
) {
  switch (action.type) {
    case actions.addKeyword.openAddEntityModal.type(): {
      state = produce(state, (draft) => {
        draft.modalAddEntityVisible = action.payload.visible;
      });
      break;
    }
    case actions.addKeyword.getShopSuggestEntities.fetchType(): {
      let { items } = action.payload;
      let customItems = state.entityList.customItems || [];
      state = produce(state, (draft) => {
        draft.entityList = {
          ...state.entityList,
          suggestedItems: items,
          items: customItems.concat(items),
        };
      });
      state = markBucketAdded(state.entityBucket, state);
      break;
    }
    case actions.addKeyword.resetAddEntity.type(): {
      state = produce(state, (draft) => {
        draft.entityBucket = [];
        draft.entityList.customItems = [];
        draft.entityList.items = state.entityList.suggestedItems;
      });
      break;
    }
    case actions.addKeyword.addEntitySelectEntity.type(): {
      state = produce(state, (draft) => {
        const selectedItems = [].concat(action.payload.entityIds);
        const selected = action.payload.selected;
        const stateSelected = new Set(
          state.entityList.selectedIds || [],
        );
        if (selected) {
          selectedItems.forEach((i) => stateSelected.add(i));
        } else {
          selectedItems.forEach((i) => stateSelected.delete(i));
        }
        draft.entityList.selectedIds = Array.from(stateSelected);
      });
      break;
    }
    case actions.addKeyword.addEntitySelectAll.type(): {
      state = produce(state, (draft) => {
        if (action.payload.isAdd) {
          draft.entityList.selectedIds = state.entityList.items.map(
            (i) => i.entityId,
          );
        } else {
          draft.entityList.selectedIds = [];
        }
      });
      break;
    }
    case actions.updateAddProductSearch.type(): {
      state = produce(state, (draft) => {
        draft.entityList.searchText = action.payload.searchText || '';
      });
      break;
    }
    case actions.updateAddProductSelectedCategory.type(): {
      state = produce(state, (draft) => {
        draft.entityList.selectedCategories =
          action.payload.categories;
      });
      break;
    }
    case actions.addKeyword.updateEntityListingSearch.type(): {
      state = produce(state, (draft) => {
        draft.campaignEntityList.searchText =
          action.payload.searchText || '';
      });
      break;
    }
    case actions.addKeyword.updateEntityListingSelectedCategory.type(): {
      state = produce(state, (draft) => {
        draft.campaignEntityList.selectedCategories =
          action.payload.categories;
      });
      break;
    }
    case actions.addKeyword.campaignEntityListSelectItem.type(): {
      state = produce(state, (draft) => {
        const selectedItems = [].concat(action.payload.entityIds);
        const selected = action.payload.selected;
        const stateSelected = new Set(
          state.campaignEntityList.selectedIds || [],
        );
        if (selected) {
          selectedItems.forEach((i) => stateSelected.add(i));
        } else {
          selectedItems.forEach((i) => stateSelected.delete(i));
        }
        draft.campaignEntityList.selectedIds = Array.from(
          stateSelected,
        );
      });
      break;
    }
    case actions.addKeyword.campaignEntityListSelectAll.type(): {
      state = produce(state, (draft) => {
        if (action.payload.isAdd) {
          const { page, limit } = state.campaignEntityList;
          const start = limit * (page - 1);
          const end = page * limit;
          const campaignList = state.campaignEntityList.items.slice(
            start,
            end,
          );
          draft.campaignEntityList.selectedIds = campaignList.map(
            (i) => i.entityId,
          );
        } else {
          draft.campaignEntityList.selectedIds = [];
        }
      });
      break;
    }
    case actions.addKeyword.addEntityAddToBucket.type(): {
      state = produce(state, (draft) => {
        const { isAdded, modification, entityIds } = action.payload;
        const selectedItems = [].concat(entityIds);
        let entityBucket = state.entityBucket;
        let updatedEntityList;

        if (modification) {
          updatedEntityList = state.entityList.items.map((item) => {
            if (selectedItems.includes(item.entityId)) {
              return {
                ...item,
                totalBudget: modification.totalBudget,
                dailyBudget: modification.dailyBudget,
              };
            }
            return item;
          });
        } else {
          updatedEntityList = state.entityList.items;
        }

        if (isAdded) {
          const oldItems = state.entityBucket.filter(
            (i) => !selectedItems.includes(i.entityId),
          );
          const newItems = updatedEntityList.filter((item) =>
            selectedItems.includes(item.entityId),
          );
          entityBucket = [...oldItems, ...newItems];
        } else {
          entityBucket = state.entityBucket.filter(
            (i) => selectedItems.indexOf(i.entityId) === -1,
          );
        }

        draft.entityList.selectedIds = [];
        draft.entityList.items = updatedEntityList;
        draft.entityBucket = entityBucket;
      });
      state = markBucketAdded(state.entityBucket, state);
      break;
    }
    case actions.addKeyword.updateAddEntitySearch.type(): {
      state = produce(state, (draft) => {
        console.log('Search kw:', action.payload)
        debugger;
        const { searchText, currency } = action.payload;
        const {
          entityList: {
            suggestedItems: suggestKeywords,
            customItems: customKeywords,
            limit: pageSize,
          },
        } = state;

        const allKeywords = [...customKeywords, ...suggestKeywords];
        let filteredKeywords = allKeywords.filter((item) =>
          item.keywordName.includes(searchText),
        );

        let minValue = 0;
        if (CURRENCY_LIMITATION[currency]) {
          minValue = CURRENCY_LIMITATION[currency].BROAD_MATCH.MIN;
        }

        if (filteredKeywords.length === 0 && searchText.length > 0) {
          const existNames = customKeywords.map((i) => i.keywordName);
          const startIdIndex =
            customKeywords.length + MAX_KEYWORDS_OF_PRODUCT + 1;

          const newKeywords: any[] = [searchText]
            .filter((i) => !existNames.includes(i))
            .map((item: string, index: number) => ({
              keywordId: startIdIndex + index,
              keywordName: item,
              searchVolume: 0,
              currency: currency,
              suggestBid: minValue,
              biddingPrice: minValue,
              matchType: MATCH_TYPE.BROAD_MATCH,
            }))
            .concat(customKeywords);
            draft.entityList.customItems  = newKeywords.concat(
            filteredKeywords,
          );
          filteredKeywords = filteredKeywords.concat(newKeywords);
        }

        const rowsOfCurrentPage = filteredKeywords.slice(
          0,
          pageSize,
        );
        draft.entityList.searchText = searchText;
        draft.entityList.items = rowsOfCurrentPage;
        draft.entityList.itemCount = filteredKeywords.length;
        draft.entityList.page = 1;
      });
      break;
    }
    case actions.addKeyword.addBucketToCampaignEntity.type(): {
      state = produce(state, (draft) => {
        const entityIds = new Set(
          state.entityBucket.map((i) => i.entityId),
        );
        draft.campaignEntityList.items = [
          ...state.campaignEntityList.items.filter(
            (i) => !entityIds.has(i.entityId),
          ),
          ...state.entityBucket,
        ];
        draft.entityBucket = [];
        draft.entityList.selectedIds = [];
        draft.modalAddEntityVisible = false;
      });
      state = updateCampaignProductListCategories(
        state.campaignEntityList.items,
        state,
      );
      break;
    }
    case actions.addKeyword.removeCampaignEntity.type(): {
      state = produce(state, (draft) => {
        let removedIds = [].concat(action.payload.entityIds);
        draft.campaignEntityList.items = state.campaignEntityList.items.filter(
          (i) => removedIds.indexOf(i.entityId) === -1,
        );
        draft.campaignEntityList.selectedIds = state.campaignEntityList.selectedIds.filter(
          (i) => removedIds.indexOf(i) === -1,
        );
      });
      state = updateCampaignProductListCategories(
        state.campaignEntityList.items,
        state,
      );
      break;
    }
    case actions.addKeyword.updateEntityBiddingPrice.type(): {
      const { payload } = action;
      state = produce(state, (draft) => {
        let items = state.campaignEntityList.items;
        draft.campaignEntityList.items = items.map((i) => {
          if (payload.entityIds.indexOf(i.entityId) > -1) {
            return { ...i, biddingPrice: payload.biddingPrice };
          }
          return i;
        });
      });
      break;
    }
    case actions.addKeyword.updateEntityMatchType.type(): {
      const { payload } = action;
      state = produce(state, (draft) => {
        let items = state.campaignEntityList.items;
        draft.campaignEntityList.items = items.map((i) => {
          if (payload.entityIds.indexOf(i.entityId) > -1) {
            return { ...i, matchType: payload.matchType };
          }
          return i;
        });
      });
      break;
    }
    case actions.addKeyword.addCustomEntities.type(): {
      state = produce(state, (draft) => {
        const { keywordNames, currency } = action.payload;
        const {
          entityList: {
            items,
            suggestedItems,
            customItems = [],
            searchText,
            limit: pageSize,
          },
        } = state;

        let minValue = 0;
        if (CURRENCY_LIMITATION[currency]) {
          minValue = CURRENCY_LIMITATION[currency].BROAD_MATCH.MIN;
        }

        if (keywordNames.length > 0) {
          const existNames = customItems.map((i) => i.keywordName);
          let startIdIndex = 1;
          if (items.length > 0) {
            startIdIndex = Math.max(...items.map((i) => Number(i.entityId))) + 1;
          }

          const newKeywords: any[] = keywordNames
            .filter((i) => i && !existNames.includes(i))
            .map((item: string, index: number) => ({
              entityId: startIdIndex + index,
              keywordId: startIdIndex + index,
              keywordName: item,
              searchVolume: 0,
              currency: currency,
              suggestBid: minValue,
              biddingPrice: minValue,
              matchType: MATCH_TYPE.BROAD_MATCH,
            }))
            .concat(customItems);
          const allKeywords = [...newKeywords, ...suggestedItems];
          const filteredKeywords = allKeywords.filter((item) =>
            item.keywordName.includes(searchText),
          );
          const rowsOfCurrentPage = filteredKeywords.slice(
            0,
            pageSize,
          );
          draft.entityList.items = rowsOfCurrentPage;
          draft.entityList.customItems = newKeywords;
          draft.entityList.itemCount = filteredKeywords.length;
          draft.entityList.page = 1;
        }
      });
      break;
    }
    case actions.addKeyword.addEntityUpdateEntityBidPrice.type(): {
      state = produce(state, (draft) => {
        const { biddingPrice, matchType } = action.payload;
        const keywordIds = new Set(state.entityList.selectedIds);
        draft.entityList.suggestedItems = state.entityList.suggestedItems.map(
          (item) => {
            if (keywordIds.has(item.entityId)) {
              return {
                ...item,
                biddingPrice,
                matchType,
              };
            }
            return item;
          },
        );
        draft.entityBucket = state.entityBucket.map(
          ({ keywords, product }) => {
            const newKeywords = keywords.map((k) => {
              if (keywordIds.has(k.entityId)) {
                return {
                  ...k,
                  biddingPrice,
                  matchType,
                };
              }
              return k;
            });
            return {
              product,
              keywords: newKeywords,
            };
          },
        );
        let customItems = state.entityList.customItems || [];
        draft.entityList.customItems = customItems.map((item) => {
          if (keywordIds.has(item.entityId)) {
            return {
              ...item,
              biddingPrice,
              matchType,
            };
          }
          return item;
        });
        draft.entityList.items = state.entityList.items.map(
          (item) => {
            if (keywordIds.has(item.keywordId)) {
              return {
                ...item,
                biddingPrice,
                matchType,
              };
            }
            return item;
          },
        );
      });
      break;
    }
    case actions.addKeyword.updateCampaignEntityListPagination.type(): {
      state = produce(state, (draft) => {
        draft.campaignEntityList.page = action.payload.page;
        draft.campaignEntityList.limit = action.payload.limit;
      });
      break;
    }
    case actions.addKeyword.addEntityUpdateEntitySearch.type(): {
      state = produce(state, (draft) => {
        const { searchText } = action.payload;
        const {
          customItems,
          suggestedItems,
          limit,
        } = state.entityList;
        let pagination = {
          pageSize: limit,
        }
        const allKeywords = [...customItems, ...suggestedItems];
        const filteredKeywords = allKeywords.filter((item) =>
          item.keywordName.includes(searchText),
        );
        const rowsOfCurrentPage = filteredKeywords.slice(
          0,
          pagination.pageSize,
        );
        draft.entityList.searchText = searchText;
        draft.entityList.items = rowsOfCurrentPage;
        draft.entityList.itemCount = filteredKeywords.length;
        draft.entityList.page = 1;
      });
      break;
    }
    case actions.addKeyword.addEntityUpdateEntityPagination.type(): {
      state = produce(state, (draft) => {
        const { pagination } = action.payload;
        const {
          customItems,
          suggestedItems,
          searchText,
        } = state.entityList;
        const allItems = [...customItems, ...suggestedItems];
        const filteredItems = allItems.filter((item) =>
          item.keywordName.includes(searchText ||''),
        );
        const start =
          (pagination.currentPage - 1) * pagination.pageSize;
        const end = start + pagination.pageSize;
        const rowsOfCurrentPage = filteredItems.slice(start, end);
        draft.entityList.items = rowsOfCurrentPage;
        draft.entityList.page = pagination.currentPage;
        draft.entityList.limit = pagination.pageSize;
      });
      break;
    }
  }

  return state;
}

function markBucketAdded(
  entityBucket: any[],
  state: CreateCampaignState['setupKeyword'],
) {
  return produce(state, (draft) => {
    let bucketIds = entityBucket.map((i) => i.entityId);
    draft.entityList.items = state.entityList.items.map((i) => {
      if (bucketIds.indexOf(i.entityId) > -1) {
        return { ...i, added: true };
      }
      return { ...i, added: false };
    });
  });
}

function updateCampaignProductListCategories(
  entities: any[],
  state: CreateCampaignState['setupKeyword'],
) {
  return produce(state, (draft) => {
    const categories = entities.reduce((acc, elem) => {
      if (acc.find((i) => i.id === elem.categoryName)) {
        return acc;
      } else {
        return acc.concat({
          id: elem.categoryName,
          text: elem.categoryName,
        });
      }
    }, []);

    draft.campaignEntityList.categories = categories;
    draft.campaignEntityList.selectedCategories = categories;
  });
}
