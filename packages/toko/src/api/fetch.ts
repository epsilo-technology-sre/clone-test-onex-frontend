import { deleteFetch, get, post, put, download } from '@ep/one/src/utils/fetch';

export { get, post, put, deleteFetch, download };
