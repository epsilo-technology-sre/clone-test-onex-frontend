import { API_URL } from '@ep/one/global';
import { oneGetShops } from '@ep/one/src/utils';
import { download, get, post, put } from './fetch';

const URL = {
  GET_CAMPAIGNS: `${API_URL}/api/v1/toko/product-ads/campaigns`,
  GET_PRODUCTS: `${API_URL}/api/v1/toko/product-ads/skus`,
  GET_KEYWORDS: `${API_URL}/api/v1/toko/product-ads/keywords`,
  GET_SHOPS: `${API_URL}/api/v1/user/shops`,
  // GET_SHOPS: `/mock/tokopedia/shops`, // FIXME

  GET_CATEGORIES: `${API_URL}/api/v1/common/categories`,
  DOWNLOAD_FILE: `${API_URL}/api/v1/toko/product-ads/export`,

  UPDATE_CAMPAIGN_NAME: `${API_URL}/api/v1/lazada/sponsored-search/edit-campaign-name`, // DELETED
  UPDATE_CAMPAIGN_BUDGET: `${API_URL}/api/v1/toko/product-ads/edit-campaign-budget`,
  UPDATE_CAMPAIGN_STATUS: `${API_URL}/api/v1/toko/product-ads/edit-campaign-status`,
  UPDATE_CAMPAIGN_TIMELINE: `${API_URL}/api/v1/lazada/sponsored-search/edit-campaign-time-line`, // DELETED
  UPDATE_PRODUCT_BUDGET: `${API_URL}/api/v1/lazada/sponsored-search/edit-sku-budget`,
  ACTIVATOR_CAMPAIGNS: `${API_URL}/api/v1/toko/product-ads/edit-campaign-status`,
  ACTIVATOR_PRODUCTS: `${API_URL}/api/v1/toko/product-ads/edit-sku-status`,
  ACTIVATOR_KEYWORDS: `${API_URL}/api/v1/toko/product-ads/edit-keyword-status`,
  GET_SUGGEST_BIDDING_PRICE: `${API_URL}/api/v1/lazada/sponsored-search/suggest-bidding-price`,

  GET_SUGGEST_KEYWORDS: `${API_URL}/api/v1/toko/product-ads/suggest-kw`,
  // GET_SUGGEST_KEYWORDS: `/mock/api/v1/toko/product-ads/suggest-kw`,
  GET_CATEGORY_LIST: `${API_URL}/api/v1/toko/product-ads/category`,
  GET_SHOP_PRODUCTS: `${API_URL}/api/v1/toko/product-ads/product-of-shop`,
  // GET_SHOP_PRODUCTS: `/mock/api/v1/toko/product-ads/product-of-shop`, // FIXME
  CREATE_CAMPAIGN: `${API_URL}/api/v1/toko/product-ads/create-or-add`,
  ADD_KEYWORDS: `${API_URL}/api/v1/lazada/sponsored-search/add-keywords`,

  UPDATE_KEYWORDS_PRICE: `${API_URL}/api/v1/lazada/sponsored-search/edit-keyword-bidding-price`,
  UPDATE_PRODUCTS_PRICE: `${API_URL}/api/v1/shopee/kwbidding/edit-sku-budget`,
  UPDATE_CAMPAIGNS_PRICE: `${API_URL}/api/v1/toko/product-ads/edit-campaign-budget`,

  // rules
  RULE_GET_ACTION_METRIC: `${API_URL}/api/v1/programmatic/rule/list-metric`,
  RULE_COUNTING: `${API_URL}/api/v1/programmatic/rule/counting`,
  RULE_GET_LIST_RULE: `${API_URL}/api/v1/programmatic/rule/list`,
  RULE_GET_LIST_RULE_LOG: `${API_URL}/api/v1/programmatic/rule/list-log`,

  RULE_EDIT: `${API_URL}/api/v1/toko/product-ads/rule/modify`,
  RULE_CREATE: `${API_URL}/api/v1/toko/product-ads/rule/create`,
  ADD_EXISTING_RULE: `${API_URL}/api/v1/toko/product-ads/rule/add-exist-rule-to-object`,
  RULE_DELETE: `${API_URL}/api/v1/toko/product-ads/rule/delete`,
  RULE_DELETE_ALL: `${API_URL}/api/v1/toko/product-ads/rule/delete-all`,

  // new
  UPDATE_CAMPAIGN_BIDDING_PRICE: `${API_URL}/api/v1/toko/product-ads/edit-campaign-max-cpc`,
  UPDATE_KEYWORDS_BIDDING_PRICE: `${API_URL}/api/v1/toko/product-ads/edit-keyword-max-cpc`,
  UPDATE_KEYWORDS_MATCH_TYPE: `${API_URL}/api/v1/toko/product-ads/edit-keyword-match-type`,
};
export { URL as EP };

// getShops, getCategories // mock data
export const getShops = async (params: any = null) =>
  await oneGetShops({ params });

export const getCategories = async (params: any = null) =>
  await get(URL.GET_CATEGORIES, params);

export const createCampaign = async (params: any = null) =>
  await post(URL.CREATE_CAMPAIGN, params);

export const addKeywords = async (params: any = null) =>
  await post(URL.ADD_KEYWORDS, params);

export const getSuggestKeywords = async (params: any = null) =>
  await get(URL.GET_SUGGEST_KEYWORDS, params);

export const getCampaigns = async (params: any = null) =>
  await get(URL.GET_CAMPAIGNS, params);

export const getProducts = async (params: any = null) =>
  await get(URL.GET_PRODUCTS, params);

export const getKeywords = async (params: any = null) =>
  await get(URL.GET_KEYWORDS, params);

export const getRuleCounting = async (params: any = null) =>
  await get(URL.RULE_COUNTING, params);

export const checkExistCampaignName = async (params: any = null) =>
  await new Promise((resolve, reject) => {
    resolve({ success: true });
  });

export const checkExistSubcriptionCode = async (params: any = null) =>
  await get(URL.CHECK_EXIST_SUBCRIPTION_CODE, params);

export const updateCampaignName = async (params: any = null) =>
  await put(URL.UPDATE_CAMPAIGN_NAME, params);

export const updateCampaignBudget = async (params: any = null) =>
  await put(URL.UPDATE_CAMPAIGN_BUDGET, params);

export const updateCampaignTimeline = async (params: any = null) =>
  await put(URL.UPDATE_CAMPAIGN_TIMELINE, params);

export const updateCampaignBiddingPrice = async (
  params: any = null,
) => await put(URL.UPDATE_CAMPAIGN_BIDDING_PRICE, params);

export const updateProductBudget = async (params: any = null) =>
  await put(URL.UPDATE_PRODUCT_BUDGET, params);

export const activatorKeywords = async (params: any = null) =>
  await put(URL.ACTIVATOR_KEYWORDS, params);

export const activatorProducts = async (params: any = null) =>
  await put(URL.ACTIVATOR_PRODUCTS, params);

export const activatorCampaigns = async (params: any = null) =>
  await put(URL.ACTIVATOR_CAMPAIGNS, params);

export const getSuggestBiddingPrice = async (params: any = null) =>
  await get(URL.GET_SUGGEST_BIDDING_PRICE, params);

export const updateKeywordsPrice = async (params: any = null) =>
  await put(URL.UPDATE_KEYWORDS_PRICE, params);

export const updateProductsPrice = async (params: any = null) =>
  await put(URL.UPDATE_PRODUCTS_PRICE, params);

export const updateCampaignPrice = async (params: any = null) =>
  await put(URL.UPDATE_CAMPAIGNS_PRICE, params);

export const downloadFile = async (params: any) => {
  return download(URL.DOWNLOAD_FILE, params);
};

export const updateKeywordBiddingPrice = async (
  params: any = null,
) => {
  return await put(URL.UPDATE_KEYWORDS_BIDDING_PRICE, params);
};

export const updateKeywordMatchType = async (params: any = null) => {
  return await put(URL.UPDATE_KEYWORDS_MATCH_TYPE, params);
};
