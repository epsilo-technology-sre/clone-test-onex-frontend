import { ThemeProvider } from '@material-ui/core/styles';
import React from 'react';
import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import Theme from '@ep/shopee/src/shopee-theme';
import { CampaignPage } from './pages';
import { store } from './redux/store';
import { startMock } from '@ep/one/src/mock';

startMock();

export const App = () => {
  return (
    <Provider store={store}>
      <ThemeProvider theme={Theme}>
        <CampaignPage></CampaignPage>
        <ToastContainer closeOnClick={false} hideProgressBar={true} />
      </ThemeProvider>
    </Provider>
  );
};

export default App;
