import {
  all,
  call,
  put,
  select,
  takeLatest,
} from 'redux-saga/effects';
import {
  getCampaigns,
  getCategories,
  getKeywords,
  getProducts,
  getRuleCounting,
  getShops,
} from '../api/api';
import { CHANNEL } from '../constants';
import {
  actions,
  GET_CAMPAIGNS,
  GET_CAMPAIGNS_TYPE,
  GET_CATEGORIES,
  GET_CATEGORIES_TYPE,
  GET_KEYWORDS,
  GET_KEYWORDS_TYPE,
  GET_PRODUCTS,
  GET_PRODUCTS_TYPE,
  GET_SHOPS,
  GET_SHOPS_TYPE,
} from './actions';

function* getCampaignsSaga(action: any) {
  try {
    const response = yield call(() => getCampaigns(action.payload));
    const { data: campaigns, pagination } = response;
    yield put(GET_CAMPAIGNS.SUCCESS({ campaigns, pagination }));
  } catch (e) {
    yield put(GET_CAMPAIGNS.ERROR(e));
  }
}

function* getProductsSaga(action: any) {
  try {
    const response = yield call(() => getProducts(action.payload));
    const { data, pagination } = response;
    let countRes: any = [];
    if (data.length > 0) {
      countRes = yield call(() =>
        getRuleCounting({
          shop_eids: [].concat(action.payload.shop_eids),
          object_type: 'campaign_product',
          object_eids: data
            .map((i) => i.campaign_product_id)
            .join(','),
        }),
      );
    }

    const products = data.map((item: any) => {
      const count = countRes.data.find(
        (c) =>
          c.object_eid === item.campaign_product_id &&
          c.shop_eid === item.shop_eid,
      );
      return {
        ...item,
        product_id: item.campaign_product_id,
        productName: item.product_name,
        productId: item.campaign_product_id,
        productSId: item.product_sid,
        dailyBudget: item.budget_config.value_daily,
        totalBudget: item.budget_config.value_total,
        ruleCount: count ? count.quantity_rule : 0,
      };
    });

    yield put(GET_PRODUCTS.SUCCESS({ products, pagination }));
  } catch (e) {
    yield put(GET_PRODUCTS.ERROR(e));
  }
}

function* getKeywordsSaga(action: any) {
  try {
    const { selectedCampaigns } = yield select((state) => {
      return { selectedCampaigns: state.selectedCampaigns || [] };
    });

    let payload = action.payload;
    if (selectedCampaigns.length > 0) {
      payload.campaign_eids = selectedCampaigns.map(
        (c) => c.campaign_eid,
      );
    }

    const response = yield call(() => getKeywords(payload));
    const { data, pagination } = response;
    let countRes: any = [];
    if (data.length > 0) {
      countRes = yield call(() =>
        getRuleCounting({
          shop_eids: action.payload.shop_eids,
          object_type: 'product_keyword',
          object_eids: data.map((i) => i.keyword_id).join(','),
        }),
      );
    }

    const keywords = data.map((item: any) => {
      const count = countRes.data.find(
        (c) =>
          c.object_eid === item.keyword_id &&
          c.shop_eid === item.shop_eid,
      );
      return {
        ...item,
        ruleCount: count ? count.quantity_rule : 0,
      };
    });

    yield put(GET_KEYWORDS.SUCCESS({ keywords, pagination }));
  } catch (e) {
    yield put(GET_KEYWORDS.ERROR(e));
  }
}

function* getShopsSaga(action: any) {
  try {
    const response = yield call(() =>
      getShops({
        ...action.payload,
        channel_code: CHANNEL,
      }),
    );
    const { data: shops } = response;
    yield put(GET_SHOPS.SUCCESS({ shops }));
  } catch (e) {
    yield put(GET_SHOPS.ERROR(e));
  }
}

function* getCategoriesSaga(action: any) {
  try {
    const response = yield call(() => getCategories(action.payload));
    const { data: categories } = response;
    yield put(GET_CATEGORIES.SUCCESS({ categories }));
  } catch (e) {
    yield put(GET_CATEGORIES.ERROR(e));
  }
}

function* actionWatcher() {
  yield takeLatest(GET_SHOPS_TYPE.START, getShopsSaga);
  yield takeLatest(GET_CATEGORIES_TYPE.START, getCategoriesSaga);
  yield takeLatest(GET_CAMPAIGNS_TYPE.START, getCampaignsSaga);
  yield takeLatest(GET_PRODUCTS_TYPE.START, getProductsSaga);
  yield takeLatest(GET_KEYWORDS_TYPE.START, getKeywordsSaga);
}

export default function* rootSaga() {
  yield all([
    actionWatcher(),
    actions.getRuleActionMetrics.saga(),
    actions.createProductRule.saga(),
    actions.addExistingRule.saga(),
    actions.editProductRule.saga(),
    actions.deleteProductRule.saga(),
    actions.deleteAllProductRules.saga(),
    actions.createKeywordRule.saga(),
    actions.editKeywordRule.saga(),
    actions.deleteKeywordRule.saga(),
    actions.deleteAllKeywordRules.saga(),
    actions.getKeywordRules.saga(),
    actions.getProductRules.saga(),
    actions.getExistKeywordRules.saga(),
    actions.getExistProductRules.saga(),
    actions.getRuleLogs.saga(),
  ]);
}
