import { produce } from 'immer';
import { uniqBy, uniqueId } from 'lodash';
import {
  CURRENCY_LIMITATION,
  MATCH_TYPE,
  MAX_KEYWORDS_OF_PRODUCT,
} from '../../constants';
import { actions } from './actions';

export type CreateShopAdsState = {
  currentStep: 'AdsInfo' | 'AdsCreative' | 'SetupKeywords';
  shopList: any[];
  setupAdsInfo?: {
    shop: number;
    adsName: string;
    currency: string;
    budget: {
      total: number;
      daily: number;
      isOnDaily: boolean;
      isNoLimit: boolean;
    };
    timeline: {
      fromDate: string;
      toDate: string;
      isOnNoLimit: boolean;
    };
  };
  adsCreative?: {
    optionCategories: any[];
    isMainShop: boolean;
    shopCategory: string;
    tagline: string;
  };
  keywordList: any[];
  addKeyword?: {
    isShowAddKeywordModal: boolean;
    suggestKeywords: any[];
    keywords: any[];
    selectedProducts: any[];
    selectedKeywords: any[];
    customKeywords: any[];
    keywordBucket: any[];
    searchText: string;
    pagination: any;
  };
  loading: {
    [key: string]: {
      status: boolean;
      error: any;
    };
  };
};

export type EditShopAdsState = CreateShopAdsState & {
  shopAds?: any[];
  selectedShopId?: number;
  selectedShopAdsId?: number;
};

export const initState: EditShopAdsState = {
  currentStep: 'AdsInfo',
  shopList: [],
  loading: {},
  keywordList: [],
  addKeyword: {
    isShowAddKeywordModal: false,
    suggestKeywords: [],
    keywords: [],
    customKeywords: [],
    keywordBucket: [],
    selectedProducts: [],
    selectedKeywords: [],
    searchText: '',
    pagination: {
      pageSize: 10,
      currentPage: 1,
      total: 0,
    },
  },
};

export function reducer(
  state: EditShopAdsState = initState,
  action: { type: string; payload: any },
) {
  let { payload } = action;
  switch (action.type) {
    case 'REQUEST_ACTION_LOADING': {
      state = produce(state, (draft) => {
        draft.loading[action.payload.loading.section] = {
          status: action.payload.loading.status,
          error: action.payload.loading.error,
        };
      });
      break;
    }
    case actions.getShopList.fetchType(): {
      state = produce(state, (draft) => {
        draft.shopList = payload.shopList;
      });
      break;
    }
    case actions.setStep.type(): {
      state = produce(state, (draft) => {
        draft.currentStep = payload.step;
      });
      break;
    }
    case actions.setAdsInfo.type(): {
      state = produce(state, (draft) => {
        draft.setupAdsInfo = payload.adsInfo;
      });
      break;
    }
    case actions.getShopCategory.fetchType(): {
      state = produce(state, (draft) => {
        draft.adsCreative = {
          ...state.adsCreative,
          optionCategories: payload.categoryList,
        };
      });
      break;
    }
    case actions.setAdsCreative.type(): {
      state = produce(state, (draft) => {
        draft.adsCreative = {
          ...state.adsCreative,
          ...payload.adsCreative,
        };
      });
      break;
    }
    case actions.updateShopAdsKeywords.type(): {
      state = produce(state, (draft) => {
        let keywordList = (state.keywordList || []).concat(
          payload.keywordList,
        );
        keywordList = uniqBy(keywordList, (i) => i.keywordId);

        draft.keywordList = keywordList;
      });
      break;
    }

    case actions.removeAddedKeyword.type(): {
      state = produce(state, (draft) => {
        draft.keywordList = state.keywordList.filter(
          (i) => payload.removedIds.indexOf(i.keywordId) === -1,
        );
      });
      break;
    }

    case actions.getShopAds.fetchType(): {
      state = produce(state, (draft) => {
        draft.shopAds = payload.shopAds;
      });
      break;
    }

    case actions.selectShop.type(): {
      state = produce(state, (draft) => {
        draft.selectedShopId = payload.shopId;
      });
      break;
    }
    case actions.selectShopAds.type(): {
      state = produce(state, (draft) => {
        draft.selectedShopAdsId = payload.shopAdsId;
      });
      break;
    }

    case actions.resetAddKeyword.type(): {
      state = produce(state, (draft) => {
        draft.addKeyword.keywordBucket = [];
      });
      break;
    }

    // add keywords setup
    case actions.getSuggestKeywords.fetchType(): {
      state = produce(state, (draft) => {
        const { suggestKeywords } = payload;
        draft.addKeyword = {
          ...state.addKeyword,
          suggestKeywords: suggestKeywords,
          keywords: suggestKeywords.slice(
            0,
            state.addKeyword.pagination.pageSize,
          ),
        };
        draft.addKeyword.pagination.total = suggestKeywords.length;
      });
      break;
    }
    case actions.addCustomKeywords.fetchType(): {
      state = produce(state, (draft) => {
        const { validatedKeywords, currency } = action.payload;

        const {
          suggestKeywords,
          customKeywords,
          searchText,
          pagination,
        } = state.addKeyword;

        let minValue = 0;
        if (CURRENCY_LIMITATION[currency]) {
          minValue = CURRENCY_LIMITATION[currency].BROAD_MATCH.MIN;
        }

        if (validatedKeywords.length > 0) {
          const existNames = customKeywords
            .concat(suggestKeywords)
            .map((i) => i.keywordName);
          const startIdIndex = MAX_KEYWORDS_OF_PRODUCT + 1;

          const newKeywords = validatedKeywords
            .filter((i) => !existNames.includes(i.keywordName))
            .map((i, index) => ({
              keywordId: startIdIndex + Number(uniqueId()) + index,
              ...i,
            }))
            .concat(customKeywords);

          const allKeywords = [...newKeywords, ...suggestKeywords];
          const filteredKeywords = allKeywords.filter((item) =>
            item.keywordName.includes(searchText),
          );
          const rowsOfCurrentPage = filteredKeywords.slice(
            0,
            pagination.pageSize,
          );
          draft.addKeyword.keywords = rowsOfCurrentPage;
          draft.addKeyword.customKeywords = newKeywords;
          draft.addKeyword.pagination.total = filteredKeywords.length;
          draft.addKeyword.pagination.currentPage = 1;
        }
      });
      break;
    }
    case actions.removeCustomKeywords.type(): {
      state = produce(state, (draft) => {
        const { keywordIds } = action.payload;
        draft.addKeyword.keywords = state.addKeyword.keywords.filter(
          (i) => keywordIds.indexOf(i.keywordId) === -1,
        );
        draft.addKeyword.customKeywords = state.addKeyword.customKeywords.filter(
          (i) => keywordIds.indexOf(i.keywordId) === -1,
        );
        draft.addKeyword.pagination.total =
          state.addKeyword.pagination.total - keywordIds.length;
      });
      break;
    }
    case actions.selectKeyword.type(): {
      state = produce(state, (draft) => {
        const { keyword, isAdd } = action.payload;
        let newList = [];
        if (isAdd) {
          newList = state.addKeyword.selectedKeywords.concat(keyword);
        } else {
          newList = state.addKeyword.selectedKeywords.filter(
            (item: any) => item.keywordId !== keyword.keywordId,
          );
        }

        draft.addKeyword.selectedKeywords = newList;
      });
      break;
    }
    case actions.selectAllKeywords.type(): {
      state = produce(state, (draft) => {
        const {
          customKeywords,
          suggestKeywords,
          searchText,
          pagination,
        } = state.addKeyword;
        let newList: any[] = [];
        if (action.payload.isAdd) {
          const start =
            (pagination.currentPage - 1) * pagination.pageSize;
          const end = start + pagination.pageSize;
          newList = [...customKeywords, ...suggestKeywords]
            .filter((item) => item.keywordName.includes(searchText))
            .slice(start, end);
        }
        draft.addKeyword.selectedKeywords = newList;
      });
      break;
    }
    case actions.updateSelectedKeywordPrice.type(): {
      state = produce(state, (draft) => {
        const { biddingPrice, matchType } = action.payload;
        const keywordIds = new Set(
          state.addKeyword.selectedKeywords.map(
            (item) => item.keywordId,
          ),
        );
        draft.addKeyword.suggestKeywords = state.addKeyword.suggestKeywords.map(
          (item) => {
            if (keywordIds.has(item.keywordId)) {
              return {
                ...item,
                biddingPrice,
                matchType,
              };
            }
            return item;
          },
        );
        draft.addKeyword.keywordBucket = state.addKeyword.keywordBucket.map(
          (k) => {
            if (keywordIds.has(k.keywordId)) {
              return {
                ...k,
                biddingPrice,
                matchType,
              };
            }
            return k;
          },
        );
        draft.addKeyword.customKeywords = state.addKeyword.customKeywords.map(
          (item) => {
            if (keywordIds.has(item.keywordId)) {
              return {
                ...item,
                biddingPrice,
                matchType,
              };
            }
            return item;
          },
        );
        draft.addKeyword.keywords = state.addKeyword.keywords.map(
          (item) => {
            if (keywordIds.has(item.keywordId)) {
              return {
                ...item,
                biddingPrice,
                matchType,
              };
            }
            return item;
          },
        );
        draft.addKeyword.selectedKeywords = state.addKeyword.selectedKeywords.map(
          (item) => {
            return {
              ...item,
              biddingPrice,
              matchType,
            };
          },
        );
      });
      break;
    }
    case actions.addKeywordToBucket.type(): {
      state = produce(state, (draft) => {
        const { keywords } = action.payload;
        const { keywordBucket } = state.addKeyword;

        let newKeywords = keywords.filter(
          (k) =>
            !keywordBucket.find((k1) => k1.keywordId === k.keywordId),
        );

        draft.addKeyword.keywordBucket = [
          ...keywordBucket,
          ...newKeywords,
        ];
        draft.addKeyword.keywords = state.addKeyword.keywords.map(
          (k) => {
            if (
              newKeywords.find((k1) => k1.keywordId === k.keywordId)
            ) {
              return { ...k, added: true };
            }
            return k;
          },
        );
      });
      break;
    }
    case actions.removeKeyword.type(): {
      state = produce(state, (draft) => {
        const { keyword } = action.payload;
        let newBucket = state.addKeyword.keywordBucket.filter(
          (k1) => k1.keywordId !== keyword.keywordId,
        );
        draft.addKeyword.keywordBucket = newBucket;
        draft.addKeyword.keywords = state.addKeyword.keywords.map(
          (k) => {
            if (
              newBucket.find((k1) => k1.keywordId === k.keywordId)
            ) {
              return { ...k, added: true };
            } else {
              return { ...k, added: false };
            }
          },
        );
      });
      break;
    }
    case actions.removeAllKeywords.type(): {
      state = produce(state, (draft) => {
        draft.addKeyword.keywordBucket = [];
      });
      break;
    }
    case actions.updateKeywordSearch.type(): {
      state = produce(state, (draft) => {
        const { currency } = action.payload;
        const searchText = action.payload.searchText.toLowerCase();
        const {
          customKeywords,
          suggestKeywords,
          pagination,
        } = state.addKeyword;
        const allKeywords = [...customKeywords, ...suggestKeywords];
        let filteredKeywords = allKeywords.filter((item) =>
          item.keywordName.includes(searchText),
        );

        let minValue = 0;
        if (CURRENCY_LIMITATION[currency]) {
          minValue = CURRENCY_LIMITATION[currency].BROAD_MATCH.MIN;
        }

        if (
          filteredKeywords.length === 0 &&
          searchText.length > 0 &&
          !/[~!@#$%^&*()/\-+={}|[/\]\:;"'<>./?`_/\\]/gi.test(
            searchText,
          )
        ) {
          const existNames = customKeywords.map((i) => i.keywordName);
          const startIdIndex =
            customKeywords.length + MAX_KEYWORDS_OF_PRODUCT + 1;

          const newKeywords: any[] = [searchText]
            .filter((i) => !existNames.includes(i))
            .map((item: string, index: number) => ({
              keywordId: startIdIndex + index,
              keywordName: item,
              searchVolume: 0,
              currency: currency,
              suggestBid: minValue,
              biddingPrice: minValue,
              matchType: MATCH_TYPE.BROAD_MATCH,
            }))
            .concat(customKeywords);
          draft.addKeyword.customKeywords = newKeywords.concat(
            filteredKeywords,
          );
          filteredKeywords = filteredKeywords.concat(newKeywords);
        }

        const rowsOfCurrentPage = filteredKeywords.slice(
          0,
          pagination.pageSize,
        );
        draft.addKeyword.searchText = action.payload.searchText;
        draft.addKeyword.keywords = rowsOfCurrentPage.map((k) => {
          if (
            state.addKeyword.keywordBucket.find(
              (k1) => k1.keywordId === k.keywordId,
            )
          ) {
            return { ...k, added: true };
          } else {
            return { ...k, added: false };
          }
        });
        draft.addKeyword.pagination.total = filteredKeywords.length;
        draft.addKeyword.pagination.currentPage = 1;
      });
      break;
    }
    case actions.updateKeywordPagination.type(): {
      state = produce(state, (draft) => {
        const { pagination } = action.payload;
        const {
          customKeywords,
          suggestKeywords,
          searchText,
        } = state.addKeyword;
        const allKeywords = [...customKeywords, ...suggestKeywords];
        const filteredKeywords = allKeywords.filter((item) =>
          item.keywordName.includes(searchText),
        );
        const start =
          (pagination.currentPage - 1) * pagination.pageSize;
        const end = start + pagination.pageSize;
        const rowsOfCurrentPage = filteredKeywords.slice(start, end);
        draft.addKeyword.keywords = rowsOfCurrentPage;
        draft.addKeyword.pagination = pagination;
      });
      break;
    }
    case actions.openAddKeywordModal.type(): {
      state = produce(state, (draft) => {
        draft.addKeyword.isShowAddKeywordModal =
          action.payload.visible;
      });
      break;
    }
    case actions.resetAddKeyword.type(): {
      state = produce(state, (draft) => {
        draft.addKeyword = {
          ...state.addKeyword,
          suggestKeywords: [],
          keywords: [],
          customKeywords: [],
          keywordBucket: [],
          selectedKeywords: [],
          searchText: '',
          pagination: {
            pageSize: 10,
            currentPage: 1,
            total: 0,
          },
        };
      });
      break;
    }
  }
  return state;
}
