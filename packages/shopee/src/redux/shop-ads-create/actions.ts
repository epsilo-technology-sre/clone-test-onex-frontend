import { actionAsync, actionNative } from '@ep/one/src/utils';
import { sortList } from '@ep/shopee/src/utils/utils';
import { get, union } from 'lodash';
import moment from 'moment';
import * as eff from 'redux-saga/effects';
import { EP } from '../../api/api';
import * as fetch from '../../api/fetch';
import { CHANNEL, SHOP_ADS_STATUS } from '../../constants';
import { actionWithRequest, makeAction } from '../util';
import { CreateShopAdsState } from './reducers';

const actions = {
  getShopList: actionWithRequest({
    actname: 'SHOPADS/GET_SHOP_LIST',
    fun: function* () {
      const rs = yield eff.call(() =>
        fetch.get(EP.GET_SHOPS, { channel_code: CHANNEL }),
      );
      let shopList = rs.data.map((i: any) => {
        return {
          shopId: i.shop_eid,
          shopName: `${i.country_code} / ${i.shop_name}`,
          shopCurrency: i.country_exchange,
          channelCode: i.channel_code,
          channelId: i.channel_id,
        };
      });
      return { shopList: sortList(shopList, 'shopName') };
    },
  }),
  setStep: makeAction({
    actname: 'SHOPADS/SET_STEP',
    fun: function ({ step }) {
      return { step };
    },
  }),

  getShopCategory: actionWithRequest({
    actname: 'SHOPADS/GET_SHOP_CATEGORY',
    fun: function* ({ shopIds }) {
      const rs = yield eff.call(() =>
        fetch.get(EP.SHOPADS_GET_SHOP_CATEGORY, {
          shop_eids: shopIds,
        }),
      );

      let categoryList = rs.data.map((i: any) => {
        return {
          shopCatId: i.shop_category_eid,
          shopCatName: i.shop_category_name,
          productCount: i.product_count,
          shopId: i.shop_eid,
          channelCode: i.channel_code,
          countryCode: i.country_code,
        };
      });
      return { categoryList };
    },
  }),
  setAdsInfo: makeAction({
    actname: 'SHOPADS/SET_ADS_INFO',
    fun: function ({ adsInfo }) {
      return { adsInfo };
    },
  }),
  setAdsCreative: makeAction({
    actname: 'SHOPADS/SET_ADS_CREATIVE',
    fun: function ({ adsCreative }) {
      return { adsCreative };
    },
  }),
  updateShopAdsKeywords: makeAction({
    actname: 'SHOPADS/UPDATE_SHOPADS_KEYWORDS',
    fun: function ({ keywordList }) {
      return {
        keywordList: keywordList.filter((i) => i.isValid !== false),
      };
    },
  }),
  removeAddedKeyword: makeAction({
    actname: 'SHOPADS/REMOVE_ADDED_KEYWORDS',
    fun: function ({ removedIds }) {
      return { removedIds };
    },
  }),
  saveNewShopAds: actionWithRequest({
    actname: 'SHOPADS/SUBMIT_NEW_SHOPADS',
    fun: function* saveNewShopAds({ promise }) {
      const postShopAds = yield eff.select(
        (state: CreateShopAdsState) => {
          let {
            setupAdsInfo: { shop, adsName, budget, timeline },
            adsCreative: { shopCategory, tagline },
            keywordList,
          } = state;

          return {
            shop_eid: shop,
            shop_eids: [shop],
            shop_ads_name: adsName,
            total_budget: budget.isNoLimit ? 0 : budget.total,
            daily_budget: budget.isOnDaily ? budget.daily : 0,
            start_date: moment(
              timeline.fromDate,
              'DD/MM/YYYY',
            ).format('YYYY-MM-DD'),
            end_date: timeline.isOnNoLimit
              ? null
              : moment(timeline.toDate, 'DD/MM/YYYY').format(
                  'YYYY-MM-DD',
                ),
            tagline,
            shop_category_eid: shopCategory,
            keywords: keywordList.map((i) => ({
              name: i.keywordName,
              match_type: i.matchType,
              bidding_price: i.biddingPrice,
            })),
          };
        },
      );

      try {
        const rs = yield eff.call(() => {
          return fetch.post(EP.SHOPADS_CREATE_NEW, postShopAds);
        });
        promise.resolve(rs);
      } catch (rs) {
        console.error('error request', rs);
        promise.reject(rs);
      }
    },
  }),
};

const addKeywordActions = (prefix = 'SHOPADS/ADD_KEYWORDS/') => ({
  getSuggestKeywords: actionWithRequest({
    actname: prefix + 'GET_SUGGEST_KEYWORDS',
    fun: function* ({ shopId }) {
      const rs = yield eff.call(() => {
        return fetch.get(EP.SHOPADS_GET_SUGGESTED_KEYWORDS, {
          shop_eids: [].concat(shopId),
          limit: 200,
        });
      });

      const suggestKeywords = rs.data.map((item: any, index) => {
        return {
          keywordId: index,
          keywordName: item.keyword_name,
          currency: item.currency,
          suggestBid: item.suggest_bidding_price,
          biddingPrice: item.suggest_bidding_price,
          matchType: item.match_type,
          searchVolume: item.search_volume,
        };
      });

      return {
        suggestKeywords,
      };
    },
  }),
  selectKeyword: makeAction({
    actname: prefix + 'SELECT_KEYWORD',
    fun: function ({ keyword, isAdd }) {
      return {
        keyword,
        isAdd,
      };
    },
  }),
  selectAllKeywords: makeAction({
    actname: prefix + 'SELECT_ALL_KEYWORDS',
    fun: function ({ isAdd }) {
      return {
        isAdd,
      };
    },
  }),
  addCustomKeywords: actionAsync({
    actname: prefix + 'ADD_CUSTOM_KEYWORDS',
    fun: function* ({ keywordNames, currency }) {
      let uniqKeywords = union(keywordNames);
      let shopEid = yield eff.select((state: CreateShopAdsState) => {
        return get(
          state,
          'setupAdsInfo.shop',
          get(state, 'selectedShopId'),
        );
      });

      const data: any[] = yield eff.call(async () => {
        const res = await fetch.post(
          EP.SHOPADS_ADS_CHECK_KEYWORDS_VALID,
          {
            shop_eids: [].concat(shopEid),
            keyword_names: uniqKeywords,
          },
        );
        return res.data;
      });

      return {
        validatedKeywords: data.map((i) => ({
          keywordName: i.keyword_name,
          currency: currency,
          suggestBid: i.suggest_bidding_price,
          biddingPrice: i.suggest_bidding_price,
          matchType: i.match_type,
          searchVolume: i.search_volume,
          isValid: i.is_valid,
          isCustom: true,
        })),
        currency,
      };
    },
  }),
  removeCustomKeywords: makeAction({
    actname: prefix + 'REMOVE_CUSTOM_KEYWORDS',
    fun: function ({ keywordIds }) {
      return {
        keywordIds,
      };
    },
  }),
  updateSelectedProducts: makeAction({
    actname: prefix + 'UPDATE_SELECTED_PRODUCTS',
    fun: function ({ product }) {
      return {
        product,
      };
    },
  }),
  addKeywordToBucket: actionNative({
    actname: prefix + 'ADD_KEYWORD_TO_BUCKET',
    fun: function ({ keywords }) {
      return { keywords };
    },
  }),
  removeKeyword: makeAction({
    actname: prefix + 'REMOVE_KEYWORD',
    fun: function ({ keyword }) {
      return { keyword };
    },
  }),
  removeAllKeywords: makeAction({
    actname: prefix + 'REMOVE_ALL_KEYWORDS',
    fun: function () {
      return true;
    },
  }),
  updateKeywordSearch: makeAction({
    actname: prefix + 'UPDATE_KEYWORD_SEARCH',
    fun: function ({ searchText, currency }) {
      return { searchText, currency };
    },
  }),
  updateKeywordPagination: makeAction({
    actname: prefix + 'UPDATE_KEYWORD_PAGINATION',
    fun: function ({ pagination }) {
      return { pagination };
    },
  }),
  updateSelectedKeywordPrice: makeAction({
    actname: prefix + 'UPDATE_SELECTED_KEYWORD_PRICE',
    fun: function ({ biddingPrice, matchType }) {
      return { biddingPrice, matchType };
    },
  }),
  openAddKeywordModal: makeAction({
    actname: prefix + 'OPEN_ADD_KEYWORD_MODAL',
    fun: ({ visible }: { visible: boolean }) => ({ visible }),
  }),
  resetAddKeyword: makeAction({
    actname: prefix + 'RESET_ADD_KEYWORD',
    fun: function () {
      return {};
    },
  }),
});

const actionsExitingShopAds = (prefix = '') => ({
  getShopAds: actionWithRequest({
    actname: `${prefix}GET_SHOP_ADS`,
    fun: function* getShopAds({ shopId, globalFilters }) {
      const rs = yield eff.call(() => {
        return fetch.get(EP.SHOPADS_GET_SHOPADS, {
          shop_eids: [shopId],
          from: globalFilters.timeRange.start,
          to: globalFilters.timeRange.end,
          limit: 100,
          status: [
            SHOP_ADS_STATUS.RUNNING,
            SHOP_ADS_STATUS.PAUSED,
            SHOP_ADS_STATUS.SCHEDULED,
            SHOP_ADS_STATUS.SYNCING,
          ],
        });
      });
      let shopAds = rs.data.map((i) => ({
        shopAdsName: i.shop_ads_name,
        shopAdsId: i.shop_ads_eid,
        currency: i.currency,
      }));
      return { shopAds };
    },
  }),

  selectShop: makeAction({
    actname: `${prefix}/SELECT_SHOP`,
    fun: function ({ shopId }) {
      return { shopId };
    },
  }),
  selectShopAds: makeAction({
    actname: `${prefix}/SELECT_SHOP_ADS`,
    fun: function ({ shopAdsId }) {
      return { shopAdsId };
    },
  }),
});

const derivativeActions = {
  ...actions,
  ...addKeywordActions(),
  ...actionsExitingShopAds('SHOPADS/ADD_KEYWORD/'),
};

export { derivativeActions as actions };

export function* rootSaga() {
  yield eff.all([
    actions.getShopList.saga(),
    actions.getShopCategory.saga(),
    derivativeActions.getSuggestKeywords.saga(),
    actions.saveNewShopAds.saga(),
    derivativeActions.getShopAds.saga(),
    derivativeActions.addCustomKeywords.saga(),
  ]);
}
