import moment from 'moment';
import * as eff from 'redux-saga/effects';
import { EP, getRuleCounting, getShops } from '../../api/api';
import * as fetch from '../../api/fetch';
import {
  CHANNEL,
  DATE_FORMAT,
  SHOP_ADS_STATUS,
} from '../../constants';
import { actionWithRequest, makeAction } from '../util';
import { ShopAdState } from './reducers';

const actions = {
  changeTab: makeAction({
    actname: 'SHOPAD/CHANGE_TAB',
    fun: function changeTab({ tab }) {
      return { tab };
    },
  }),
  getShops: actionWithRequest({
    actname: 'SHOPAD/GET_SHOP',
    fun: function* () {
      const response = yield eff.call(() =>
        getShops({
          channel_code: CHANNEL,
        }),
      );
      const { data: shops } = response;

      return { shops };
    },
  }),
  filterDateRange: makeAction({
    actname: 'SHOPAD/FILTER_DATE_RANGE',
    fun: function ({
      timeRange,
    }: {
      timeRange: { start: string; end: string };
    }) {
      return { timeRange };
    },
  }),
  filterShop: makeAction({
    actname: 'SHOPAD/FILTER_SHOP',
    fun: function ({ shops }: { shops: any[] }) {
      return { shops };
    },
  }),
  filterCountry: makeAction({
    actname: 'SHOPAD/FILTER_COUNTRY',
    fun: function ({ countries }: { countries: any[] }) {
      return { countries };
    },
  }),
  updatePagination: makeAction({
    actname: 'SHOPAD/UPDATE_PAGINATION',
    fun: function ({ pagination }) {
      return { pagination };
    },
  }),
  updateLocalFilter: makeAction({
    actname: 'SHOPAD/UPDATE_LOCAL_FILTER',
    fun: function ({
      status,
      searchText,
    }: {
      status?: string;
      searchText?: string;
    }) {
      return {
        localFilter: {
          status: status,
          searchText: searchText,
        },
      };
    },
  }),
  getShopAds: actionWithRequest({
    actname: 'SHOPAD/GET_SHOP_ADS',
    fun: function* ({
      globalFilters,
      pagination = {},
      localFilter = {},
      sort,
    }) {
      const {
        currentPagination,
        currentLocalFilter,
        currentSort,
      } = yield eff.select((state: ShopAdState) => ({
        currentPosition: state.pagination,
        currentLocalFilter: state.localFilter,
        currentSort: state.sort,
      }));

      let queryOrderBy: string;
      let queryOrderMode: string;
      let qSort = sort || currentSort;
      if (qSort) {
        queryOrderBy = qSort.id;
        queryOrderMode = qSort.desc ? 'desc' : 'asc';
      }

      let queryPagination = { ...currentPagination, ...pagination };
      let queryLocalFilter = {
        ...currentLocalFilter,
        ...localFilter,
      };
      const shopEids = globalFilters.shops.map((i) => i.id).join(',');

      const rs = yield eff.call(() =>
        fetch.get(EP.SHOPADS_GET_SHOPADS, {
          shop_eids: shopEids,
          from: globalFilters.timeRange.start,
          to: globalFilters.timeRange.end,
          limit: queryPagination.limit,
          page: queryPagination.page,
          order_by: queryOrderBy,
          order_mode: queryOrderMode,
          status:
            queryLocalFilter.status == 'All'
              ? undefined
              : queryLocalFilter.status,
          value_filter:
            queryLocalFilter.searchText !== null
              ? queryLocalFilter.searchText
              : undefined,
        }),
      );

      let countRes: any = {};
      if (rs.data.length > 0) {
        countRes = yield eff.call(() =>
          getRuleCounting({
            shop_eids: shopEids,
            object_type: 'shop_ads',
            object_eids: rs.data.map((i) => i.shop_ads_eid).join(','),
          }),
        );
      }

      let items = rs.data.map((i) => {
        const count = countRes.data.find(
          (c) =>
            c.object_eid === i.shop_ads_eid &&
            c.shop_eid === i.shop_eid,
        );
        return {
          ...i,
          shopAdsId: i.shop_ads_eid,
          shopEid: i.shop_eid,
          shopStatus: i.child_status
            ? i.child_status.shop_ads_state
            : true,
          childStatus: i.child_status,
          shopAdsStatus: i.shop_ads_status,
          adsCreativeTagline: i.tagline,
          shopCategoryName: i.shop_category_name,
          shopCategoryEid: i.shop_category_eid,
          shopName: i.shop_name,
          shopAdsName: i.shop_ads_name,
          shopAdsCode: i.shop_ads_code,
          sumItemSold: i.sum_item_sold,
          sumDirectItemSold: i.sum_direct_item_sold,
          sumCost: i.sum_cost,
          sumGmv: i.sum_gmv,
          sumDirectGmv: i.sum_direct_gmv,
          currency: i.currency,
          sumImpression: i.sum_impression,
          sumClick: i.sum_click,
          budgetConfig: {
            valueDaily: i.budget_config.value_daily,
            valueTotal: i.budget_config.value_total,
          },
          timelineFrom: i.timeline_from,
          timelineTo: i.timeline_to,
          ruleCount: count ? count.quantity_rule : 0,
          _isDisabled: i.shop_ads_status === SHOP_ADS_STATUS.ENDED,
        };
      });

      let resultPagination = rs.pagination;

      return {
        items,
        pagination: {
          itemCount: resultPagination.item_count,
          limit: resultPagination.limit,
          page: resultPagination.page,
          pageCount: resultPagination.page_count,
        },
        sort: qSort,
      };
    },
  }),

  getShopAdsKeywords: actionWithRequest({
    actname: 'SHOPAD/GET_SHOP_ADS_KEYWORDS',
    fun: function* (payload) {
      const rs = yield eff.call(() =>
        fetch.get(EP.SHOPADS_GET_SHOPADS_KEYWORDS, payload),
      );

      let countRes: any = {};
      if (rs.data.length > 0) {
        countRes = yield eff.call(() =>
          getRuleCounting({
            shop_eids: payload.shop_eids,
            object_type: 'shop_ads_keyword',
            object_eids: rs.data
              .map((i) => i.shop_ads_keyword_id)
              .join(','),
          }),
        );
      }

      const items = rs.data.map((i) => {
        const count = countRes.data.find(
          (c) =>
            c.object_eid === i.shop_ads_keyword_id &&
            c.shop_eid === i.shop_eid,
        );
        return {
          ...i,
          shopId: i.shop_eid,
          shopAdsId: i.shop_ads_eid,
          keywordId: i.shop_ads_keyword_id,
          keyword_id: i.shop_ads_keyword_id,
          shopAdsName: i.shop_ads_name,
          shopAdsCode: i.shop_ads_code,
          keywordName: i.keyword_name,
          keyword_name: i.keyword_name,
          status: i.status,
          isAdsPosition: i.is_ads_position,
          adsPositionLastUpdatedAt: i.ads_position_last_updated_at,
          biddingPrice: i.bidding_price,
          matchType: i.match_type,
          createdAt: i.created_at,
          sumItemSold: i.sum_item_sold,
          sumGmv: i.sum_gmv,
          sumCost: i.sum_cost,
          sumImpression: i.sum_impression,
          sumClick: i.sum_click,
          shopName: i.shop_name,
          currency: i.currency,
          countryCode: i.country_code,
          childStatus: i.child_status,
          ruleCount: count ? count.quantity_rule : 0,
        };
      });

      let resultPagination = rs.pagination;

      return {
        items,
        pagination: {
          itemCount: resultPagination.item_count,
          limit: resultPagination.limit,
          page: resultPagination.page,
          pageCount: resultPagination.page_count,
        },
      };
    },
  }),

  updateSelectedShopAds: makeAction({
    actname: 'SHOP_ADS/UPDATE_SELECTED_SHOP_ADS',
    fun: function ({ items }) {
      return { items };
    },
  }),
  updateSelectedShopAdKeywords: makeAction({
    actname: 'SHOP_ADS/UPDATE_SELECTED_KEYWORDS',
    fun: function ({ items }) {
      return { items };
    },
  }),
  onSelectShopAds: makeAction({
    actname: 'SHOP_ADS/ON_SELECT_SHOPADS',
    fun: function ({ items, checked }) {
      return { items, checked };
    },
  }),
  onSelectShopAdsKeyword: makeAction({
    actname: 'SHOP_ADS/ON_SELECT_SHOPADS_KEYWORDS',
    fun: function ({ items, checked }) {
      return { items, checked };
    },
  }),

  updateShopTagline: actionWithRequest({
    actname: 'SHOPADS/UPDATE_SHOP_TAG_LINE',
    fun: function* updateShopTagline({ params, promise }) {
      let shopEids = [].concat(params.tagline.shopEids);
      let shopAdsEids = [].concat(params.tagline.shopAdsId);
      let title = params.tagline.value.tagline;
      let isUseDefault = params.tagline.value.isUseDefault;
      let shopAdCategory = params.tagline.value.adCategory;

      console.info(params.tagline);
      try {
        let rs = yield eff.call(() => {
          console.info('request');
          return fetch.put(EP.SHOPADS_UPDATE_TAGLINE, {
            shop_eids: shopEids,
            shop_ads_eids: shopAdsEids,
            shop_category_eid: isUseDefault ? null : shopAdCategory,
            tagline: title,
          });
        });
        promise.resolve(rs);
      } catch (error) {
        promise.reject(error);
      }
    },
  }),

  updateShopAdBudget: actionWithRequest({
    actname: 'SHOPADS/UPDATE_SHOPAD_BUDGET',
    fun: function* updateShopBudget({
      params: { row, value },
      promise,
    }) {
      try {
        let rs = yield eff.call(() => {
          return fetch.put(EP.SHOPADS_UPDATE_BUDGET, {
            shop_eids: [
              ...new Set<number>(
                [].concat(row).map((r) => r.shopEid),
              ),
            ],
            shop_ads_eids: [].concat(row).map((r) => r.shopAdsId),
            total_budget: value.budget.isNoLimit
              ? 0
              : value.budget.total,
            daily_budget: value.budget.isOnDaily
              ? value.budget.daily
              : 0,
          });
        });
        promise.resolve(rs);
      } catch (error) {
        promise.reject(error);
      }
    },
  }),
  updateShopAdTimeline: actionWithRequest({
    actname: 'SHOPADS/UPDATE_SHOPAD_TIMELINE',
    fun: function* updateShopBudget({
      params: { row, value },
      promise,
    }) {
      try {
        let rs = yield eff.call(() => {
          return fetch.put(EP.SHOPADS_UPDATE_TIMELINE, {
            shop_eids: [].concat(row.shopEid),
            shop_ads_eids: [].concat(row.shopAdsId),
            start_date: moment(
              value.timeline.startDate,
              DATE_FORMAT,
            ).format('YYYY-MM-DD'),
            end_date: value.timeline.isOnNoLimit
              ? null
              : moment(value.timeline.endDate, DATE_FORMAT).format(
                  'YYYY-MM-DD',
                ),
          });
        });
        promise.resolve(rs);
      } catch (error) {
        promise.reject(error);
      }
    },
  }),

  updateAdsActivation: actionWithRequest({
    actname: 'SHOPADS/UPDATE_ADS_ACTIVATION',
    fun: function* updateAdsActivation({
      params: { rows, value },
      promise,
    }) {
      try {
        let rs = yield eff.call(() => {
          return fetch.put(EP.SHOPADS_UPDATE_AD_STATUS, {
            shop_eids: [
              ...new Set<number>(
                [].concat(rows).map((r) => r.shopEid),
              ),
            ],
            shop_ads_eids: [].concat(rows).map((r) => r.shopAdsId),
            status: value.activation.status ? 1 : 0,
          });
        });
        promise.resolve(rs);
      } catch (error) {
        promise.reject(error);
      }
    },
  }),

  getShopCategories: actionWithRequest({
    actname: 'SHOPADS/GET_SHOP_CATEGORIES',
    fun: function* updateAdsActivation({ shopEid, promise }) {
      const rs = yield eff.call(() => {
        return fetch.get(EP.SHOPADS_GET_SHOP_CATEGORY, {
          shop_eids: [].concat(shopEid),
          shop_eid: shopEid,
        });
      });
      try {
        const categories = rs.data.map((i) => ({
          categoryId: i.shop_category_eid,
          categoryName: i.shop_category_name,
        }));
        promise.resolve(categories);
      } catch (rs) {
        promise.resolve([]);
      }
    },
  }),

  // ==== rules related
  getRuleActionMetrics: actionWithRequest({
    actname: 'RULE/GET_ACTION_METRICS',
    fun: function* () {
      const rs = yield eff.call(() =>
        fetch.get(EP.RULE_GET_ACTION_METRIC),
      );
      return { actionMetrics: rs.data };
    },
  }),
  createProductRule: actionWithRequest({
    actname: 'RULES/CREATE_RULE_PRODUCT',
    fun: function* ({ rule, products, shopId, callback }) {
      const postRule = {
        rule_name: rule.ruleName,
        timeline_from: moment(rule.fromDate, DATE_FORMAT).format(
          'YYYY-MM-DD',
        ),
        timeline_to: moment(rule.toDate, DATE_FORMAT).format(
          'YYYY-MM-DD',
        ),
        action_code: rule.action,
        metric_condition: JSON.stringify(
          rule.listRules.map((r) => ({
            value: r.value,
            period: !r.conditionPeriod
              ? undefined
              : Number(r.conditionPeriod),
            metric_code: r.typeOfPerformance,
            operator_code: r.operator,
          })),
        ), // [{"value": 0, "period": 1000000, "metric_code": "current_stock", "operator_code": ">"}]
        feature_code: 'M_SHP_SA', // change on Lazada to M_LZD_SS
        object_type: 'shop_ads',
        object_eids: products.map((p) => p.product_id).join(','),
        shop_eids: [shopId],
        shop_eid: shopId, // makesure we only create rule for 1 product of 1 shop
      };
      console.info(postRule);
      const rs = yield eff.call(() =>
        fetch.post(EP.SHOPADS_RULE_CREATE, postRule),
      );
      if (callback) {
        callback(rs);
      }
      console.info({ rs });
      return { actionMetrics: rs.data };
    },
  }),
  addExistingRule: actionWithRequest({
    actname: 'RULES/ADD_EXISTING_RULE_PRODUCT',
    fun: function* ({
      shopEid,
      featureCode,
      objectType,
      objectEids,
      ruleId,
      callback,
    }) {
      const postRule = {
        shop_eids: [shopEid],
        shop_eid: shopEid, // makesure we only create rule for 1 product of 1 shop
        feature_code: featureCode,
        object_type: objectType,
        object_eids: objectEids,
        rule_ids: ruleId,
      };

      let rs: any = {};
      try {
        rs = yield eff.call(() =>
          fetch.post(EP.SHOPADS_ADD_EXISTING_RULE, postRule),
        );
      } catch (error) {
        rs = {
          ...rs,
          data: error.message,
        };
      }

      console.info({ rs });
      if (callback) {
        callback(rs);
      }
      return { actionMetrics: rs.data };
    },
  }),
  editProductRule: actionWithRequest({
    actname: 'RULES/EDIT_RULE_PRODUCT',
    fun: function* ({ rule, products, shopId }) {
      const postRule = {
        rule_id: rule.ruleId,
        rule_name: rule.ruleName,
        timeline_from: moment(rule.fromDate, DATE_FORMAT).format(
          'YYYY-MM-DD',
        ),
        timeline_to: moment(rule.toDate, DATE_FORMAT).format(
          'YYYY-MM-DD',
        ),
        action_code: rule.action,
        metric_condition: JSON.stringify(
          rule.listRules.map((r) => ({
            value: r.value,
            period: !r.conditionPeriod
              ? undefined
              : Number(r.conditionPeriod),
            metric_code: r.typeOfPerformance,
            operator_code: r.operator,
          })),
        ),
        feature_code: 'M_SHP_SA', // change on Lazada to M_LZD_SS
        object_type: 'shop_ads',
        object_eid: products.map((p) => p.product_id).join(','),
        shop_eids: [shopId],
        shop_eid: shopId, // makesure we only create rule for 1 product of 1 shop
      };
      const rs = yield eff.call(() =>
        fetch.put(EP.SHOPADS_RULE_EDIT, postRule),
      );
      return { actionMetrics: rs.data };
    },
  }),
  deleteProductRule: actionWithRequest({
    actname: 'RULES/DELETE_PRODUCT_RULE',
    fun: function* ({ productId, shopEid, ruleId }) {
      const rs = yield eff.call(() =>
        fetch.deleteFetch(EP.SHOPADS_RULE_DELETE, {
          shop_eids: [shopEid],
          shop_eid: shopEid, // makesure we only create rule for 1 product of 1 shop
          object_type: 'shop_ads',
          object_eid: productId,
          rule_id: ruleId,
        }),
      );

      return { message: rs.data.message };
    },
  }),
  deleteAllProductRules: actionWithRequest({
    actname: 'RULES/DELETE_PRODUCT_RULE_ALL',
    fun: function* ({ itemIds, shopEids, featureCode, callback }) {
      let rs: any = {};
      try {
        rs = yield eff.call(() =>
          fetch.deleteFetch(EP.SHOPADS_RULE_DELETE_ALL, {
            shop_eids: shopEids,
            feature_code: featureCode,
            object_type: 'shop_ads',
            object_eids: itemIds,
          }),
        );
      } catch (error) {
        rs = {
          ...rs,
          data: error.message,
        };
      }

      if (callback) {
        callback(rs);
      }

      return { message: rs.data.message };
    },
  }),
  createKeywordRule: actionWithRequest({
    actname: 'RULES/CREATE_RULE_KEYWORD',
    fun: function* ({ rule, keywords, shopId }) {
      const postRule = {
        rule_name: rule.ruleName,
        timeline_from: moment(rule.fromDate, DATE_FORMAT).format(
          'YYYY-MM-DD',
        ),
        timeline_to: moment(rule.toDate, DATE_FORMAT).format(
          'YYYY-MM-DD',
        ),
        action_code: rule.action,
        metric_condition: JSON.stringify(
          rule.listRules.map((r) => ({
            value: r.value,
            period: !r.conditionPeriod
              ? undefined
              : Number(r.conditionPeriod),
            metric_code: r.typeOfPerformance,
            operator_code: r.operator,
          })),
        ), // [{"value": 0, "period": 1000000, "metric_code": "current_stock", "operator_code": ">"}]
        bidding_price_data:
          [
            'increase_bidding_price_sakw',
            'decrease_bidding_price_sakw',
          ].indexOf(rule.action) > -1
            ? JSON.stringify({
                step: rule.ruleValue.value,
                type: rule.ruleValue.type,
                maximum: rule.biddingPriceMax,
                minimum: rule.biddingPriceMin,
              })
            : undefined,
        feature_code: 'M_SHP_SA', // change on Lazada to M_LZD_SS
        object_type: 'shop_ads_keyword',
        object_eids: keywords.map((p) => p.keyword_id).join(','),
        shop_eids: [shopId],
        shop_eid: shopId, // makesure we only create rule for 1 product of 1 shop
      };
      console.info(postRule);
      const rs = yield eff.call(() =>
        fetch.post(EP.SHOPADS_RULE_CREATE, postRule),
      );
      console.info({ rs });
      return { actionMetrics: rs.data };
    },
  }),
  editKeywordRule: actionWithRequest({
    actname: 'RULES/EDIT_RULE_KEYWORD',
    fun: function* ({ rule, keywords, shopId }) {
      const postRule = {
        rule_id: rule.ruleId,
        rule_name: rule.ruleName,
        timeline_from: moment(rule.fromDate, DATE_FORMAT).format(
          'YYYY-MM-DD',
        ),
        timeline_to: moment(rule.toDate, DATE_FORMAT).format(
          'YYYY-MM-DD',
        ),
        action_code: rule.action,
        metric_condition: JSON.stringify(
          rule.listRules.map((r) => ({
            value: r.value,
            period: !r.conditionPeriod
              ? undefined
              : Number(r.conditionPeriod),
            metric_code: r.typeOfPerformance,
            operator_code: r.operator,
          })),
        ),
        bidding_price_data:
          [
            'increase_bidding_price_sakw',
            'decrease_bidding_price_sakw',
          ].indexOf(rule.action) > -1
            ? JSON.stringify({
                step: rule.ruleValue.value,
                type: rule.ruleValue.type,
                maximum: rule.biddingPriceMax,
                minimum: rule.biddingPriceMin,
              })
            : undefined,
        feature_code: 'M_SHP_SA', // change on Lazada to M_LZD_SS
        object_type: 'shop_ads_keyword',
        object_eid: keywords.map((p) => p.keyword_id).join(','),
        shop_eids: [shopId],
        shop_eid: shopId, // makesure we only create rule for 1 product of 1 shop
      };
      console.info(postRule);
      const rs = yield eff.call(() =>
        fetch.put(EP.SHOPADS_RULE_EDIT, postRule),
      );
      console.info({ rs });
      return { actionMetrics: rs.data };
    },
  }),
  deleteKeywordRule: actionWithRequest({
    actname: 'RULES/DELETE_KEYWORD_RULE',
    fun: function* ({ keywordId, shopEid, ruleId }) {
      const rs = yield eff.call(() =>
        fetch.deleteFetch(EP.SHOPADS_RULE_DELETE, {
          shop_eids: [shopEid],
          shop_eid: shopEid, // makesure we only create rule for 1 product of 1 shop
          object_type: 'shop_ads_keyword',
          object_eid: keywordId,
          rule_id: ruleId,
        }),
      );

      return { message: rs.data.message };
    },
  }),
  deleteAllKeywordRules: actionWithRequest({
    actname: 'RULES/DELETE_KEYWORD_RULE_ALL',
    fun: function* ({ itemIds, shopEids, featureCode, callback }) {
      let rs: any = {};
      try {
        rs = yield eff.call(() =>
          fetch.deleteFetch(EP.SHOPADS_RULE_DELETE_ALL, {
            shop_eids: shopEids,
            feature_code: featureCode,
            object_type: 'shop_ads_keyword',
            object_eids: itemIds,
          }),
        );
      } catch (error) {
        rs = {
          ...rs,
          data: error.message,
        };
      }

      if (callback) {
        callback(rs);
      }

      return { message: rs.data.message };
    },
  }),
  getExistProductRules: actionWithRequest({
    actname: 'RULES/GET_EXIST_RULES_PRODUCT',
    fun: function* ({ productId, shopEid }) {
      const rs = yield eff.call(() =>
        fetch.get(EP.RULE_GET_LIST_RULE, {
          shop_eids: shopEid,
          object_type: 'shop_ads',
          object_eid: productId,
        }),
      );

      let ruleList = rs.data.map((r) => ({
        ...r,
        condition: JSON.parse(r.condition_json),
        data: JSON.parse(r.data_json),
      }));
      return { ruleList: ruleList };
    },
  }),
  getExistKeywordRules: actionWithRequest({
    actname: 'RULES/GET_EXIST_RULES_KEYWORD',
    fun: function* ({ keywordId, shopEid }) {
      const rs = yield eff.call(() =>
        fetch.get(EP.RULE_GET_LIST_RULE, {
          shop_eids: shopEid,
          object_type: 'shop_ads_keyword',
          object_eid: keywordId,
        }),
      );

      let ruleList = rs.data.map((r) => ({
        ...r,
        condition: JSON.parse(r.condition_json),
        data: JSON.parse(r.data_json),
      }));
      return { ruleList: ruleList };
    },
  }),
  getProductRules: actionWithRequest({
    actname: 'RULES/GET_RULES_PRODUCT',
    fun: function* ({ productId, shopEid }) {
      const rs = yield eff.call(() =>
        fetch.get(EP.RULE_GET_LIST_RULE, {
          shop_eids: shopEid,
          object_type: 'shop_ads',
          object_eid: productId,
        }),
      );

      let ruleList = rs.data.map((r) => ({
        ...r,
        condition: JSON.parse(r.condition_json),
        data: JSON.parse(r.data_json),
      }));
      return { ruleList: ruleList };
    },
  }),

  getKeywordRules: actionWithRequest({
    actname: 'RULES/GET_RULES_KEYWORD',
    fun: function* ({ keywordId, shopEid }) {
      const rs = yield eff.call(() =>
        fetch.get(EP.RULE_GET_LIST_RULE, {
          shop_eids: shopEid,
          object_type: 'shop_ads_keyword',
          object_eid: keywordId,
        }),
      );

      let ruleList = rs.data.map((r) => ({
        ...r,
        condition: JSON.parse(r.condition_json),
        data: JSON.parse(r.data_json),
      }));
      return { ruleList: ruleList };
    },
  }),
  updateRuleLogSorting: makeAction({
    actname: 'SHOPAD/UPDATE_RULE_LOG_SORTING',
    fun: function ({ sort }) {
      return { sort };
    },
  }),
  updateRuleLogFilter: makeAction({
    actname: 'SHOPAD/UPDATE_RULE_LOG_FILTER',
    fun: function (payload) {
      return payload;
    },
  }),
  resetRuleLog: makeAction({
    actname: 'SHOPAD/RESET_RULE_LOG',
    fun: function () {
      return {};
    },
  }),
  getRuleLogs: actionWithRequest({
    actname: 'SHOPAD/GET_RULE_LOGS',
    fun: function* ({
      itemId,
      shopEid,
      itemType,
      search,
      from,
      to,
      actions,
      page,
      limit,
      orderBy,
      orderMode,
    }) {
      const rs = yield eff.call(() =>
        fetch.get(EP.RULE_GET_LIST_RULE_LOG, {
          shop_eids: shopEid,
          object_type: itemType,
          object_eid: itemId,
          search,
          from,
          to,
          action_code: actions,
          page,
          limit,
          orderBy,
          orderMode,
        }),
      );

      const logList = rs.data.map((r) => ({
        ...r,
        ruleName: r.rule_name,
        actionCode: r.action_code,
        action: r.action_name,
        ruleValue: r.rule_data.bidding_price_data?.step,
        type: r.rule_data.bidding_price_data?.type,
        minBid: r.rule_data.bidding_price_data?.minimum,
        maxBid: r.rule_data.bidding_price_data?.maximum,
        create: moment(r.created_at).format('DD/MM/YYYY'),
        condition: r.condition_checkpoint.map((c) => ({
          metricName: c.metric_name,
          metricCode: c.metric_code,
          period: c.period,
          operator: c.operator_code,
          value: c.value,
          checkpoint: c.value_checkpoint,
        })),
      }));

      const pagination = {
        limit: rs.pagination.limit,
        page: rs.pagination.page,
        itemCount: rs.pagination.item_count,
        pageCount: rs.pagination.page_count,
      };

      return { logList, pagination };
    },
  }),
};

const derivedActions = {
  ...actions,
  updateGlobalFilter: makeAction({
    actname: 'UPDATE_GLOBAL_FILTER',
    fun: function () {
      return {};
    },
  }),
  refreshShopAds: actionWithRequest({
    actname: 'SHOPADS/REFRESH_SHOPADS_LIST',
    fun: function* refreshShopAds() {
      const { globalFilters } = yield eff.select(
        (state: ShopAdState) => {
          return { globalFilters: state.globalFilters };
        },
      );

      yield eff.put(actions.getShopAds({ globalFilters }));
    },
  }),
  sortShopAds: actionWithRequest({
    actname: 'SHOPAD/SORT',
    fun: function* ({ sortBy }) {
      const singleSort = sortBy[0];

      const { globalFilters, sort } = yield eff.select(
        (state: ShopAdState) => {
          return {
            globalFilters: state.globalFilters,
            sort: state.sort,
          };
        },
      );

      if (
        !sort ||
        sort.id !== singleSort.id ||
        sort.desc !== singleSort.desc
      ) {
        yield eff.put(
          actions.getShopAds({
            globalFilters,
            sort: singleSort,
          }),
        );
      }
    },
  }),

  shopAdsAddKeywords: actionWithRequest({
    actname: 'SHOPADS/KEYWORD/ADD_NEW_KEYWORDS',
    fun: function* shopAdsAddNewKeywords(payload: {
      shopAdsId: number;
      shopId: number;
      keywords: {
        keywordName: string;
        keywordId: number;
        matchType: string;
        biddingPrice: number;
      }[];
      promise: {
        resolve: Function;
        reject: Function;
      };
    }) {
      try {
        const rs = yield eff.call(() => {
          const postData = {
            shop_eids: [payload.shopId],
            shop_ads_eids: [payload.shopAdsId],
            keywords: payload.keywords.map((i) => ({
              name: i.keywordName,
              bidding_price: i.biddingPrice,
              match_type: i.matchType,
            })),
          };
          return fetch.put(EP.SHOPADS_ADS_ADD_KEYWORDS, postData);
        });
        payload.promise.resolve(rs);
      } catch (rs) {
        payload.promise.reject(rs);
      }
    },
  }),
};

export { derivedActions as actions };

export function* rootSaga() {
  yield eff.all([
    actions.getShops.saga(),
    actions.getShopAds.saga(),
    actions.getShopAdsKeywords.saga(),
    actions.updateShopTagline.saga(),
    actions.updateShopAdBudget.saga(),
    actions.updateShopAdTimeline.saga(),
    actions.updateAdsActivation.saga(),
    actions.getRuleActionMetrics.saga(),
    actions.createProductRule.saga(),
    actions.addExistingRule.saga(),
    actions.editProductRule.saga(),
    actions.deleteProductRule.saga(),
    actions.deleteAllProductRules.saga(),
    actions.createKeywordRule.saga(),
    actions.editKeywordRule.saga(),
    actions.deleteKeywordRule.saga(),
    actions.deleteAllKeywordRules.saga(),
    actions.getKeywordRules.saga(),
    actions.getProductRules.saga(),
    actions.getExistKeywordRules.saga(),
    actions.getExistProductRules.saga(),
    actions.getRuleLogs.saga(),
    actions.getShopCategories.saga(),
    derivedActions.refreshShopAds.saga(),
    derivedActions.sortShopAds.saga(),
    derivedActions.shopAdsAddKeywords.saga(),
  ]);
}
