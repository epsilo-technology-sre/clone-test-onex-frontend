import { produce } from 'immer';
import { uniqBy } from 'lodash';
import moment from 'moment';
import { CHANNEL } from '../../constants';
import { actions } from './actions';

export type ShopAdState = {
  tab: 'ShopAds' | 'ShopAdsKeywords';
  previousTab?: 'ShopAds' | 'ShopAdsKeywords';
  // FIXME: [FE-6] Separating the pagination between different table
  pagination: {
    page?: number;
    limit?: number;
    itemCount?: number;
    pageCount?: number;
  };
  shopAds?: {
    items: any[];
    selectedIds: any[];
  };
  keywords?: {
    items: any[];
    selectedIds: any[];
  };
  searchShopAds?: string;
  searchKeywords?: string;
  filterCampaignStatus?: string;
  filterKeywordStatus?: string;
  loading: {
    [key: string]: {
      status: boolean;
      error: any;
    };
  };
  existShopAdRuleList?: any[];
  existKeywordRuleList?: any[];
  shopAdRuleList?: any[];
  keywordRuleList?: any[];
  rulesActionList?: any[];
  rulesMetricList?: {
    [key: string]: any[];
  };
  ruleLogFilters: {
    searchText: string;
    timeRange: {
      start: string;
      end: string;
    };
    actions: any[];
  };
  ruleLogs: {
    items: any[];
    sort: any;
    pagination: {
      page?: number;
      limit?: number;
      itemCount?: number;
      pageCount?: number;
    };
  };
  globalFilters: {
    timeRange: {
      start: string;
      end: string;
    };
    countries: any[];
    shops: any[];
    updateCount: number;
  };
  localFilter?: {
    status: string;
    searchText: string;
  };
  sort?: {
    id: string;
    desc: boolean;
  };
  [key: string]: any;
};

const reducer = (
  state: ShopAdState = {
    // tab: 'ShopAdsKeywords',
    // previousTab: 'ShopAdsKeywords',
    tab: 'ShopAds',
    previousTab: 'ShopAds',
    pagination: {
      limit: 10,
      page: 1,
      itemCount: 0,
    },
    searchCampaign: '',
    filterCampaignStatus: 'All',
    filterKeywordStatus: 'All',
    availableFilter: {
      countries: [],
      shops: [],
    },
    optionGlobalFilters: {
      countries: [],
      shops: [],
    },
    globalFilters: {
      timeRange: {
        start: moment().format('YYYY-MM-DD'),
        end: moment().format('YYYY-MM-DD'),
      },
      countries: [],
      shops: [],
      updateCount: 0,
    },
    loading: {},
    shopAds: {
      items: [],
      selectedIds: [],
    },
    keywords: {
      items: [],
      selectedIds: [],
    },
    localFilter: {
      searchText: '',
      status: 'All',
    },
    ruleLogFilters: {
      searchText: '',
      timeRange: {
        start: moment().format('YYYY-MM-DD'),
        end: moment().format('YYYY-MM-DD'),
      },
      actions: [],
    },
    ruleLogs: {
      items: [],
      sort: null,
      pagination: {
        limit: 10,
        page: 1,
        itemCount: 0,
        pageCount: 1,
      },
    },
  },
  action: any,
) => {
  switch (action.type) {
    case actions.changeTab.type(): {
      const { payload } = action;
      console.info(payload);
      if (payload.tab === state.tab) {
        return state;
      }

      return produce(state, (draft) => {
        switch (payload.tab) {
          case 'ShopAds': {
            draft.shopAds.items = [];
            draft.localFilter.searchText = '';
            break;
          }
          case 'ShopAdsKeywords': {
            draft.keywords.items = [];
            draft.localFilter.searchText = '';
            break;
          }
        }
        draft.tab = payload.tab;
        draft.previousTab = state.tab;
      });
    }
    case actions.getShops.fetchType(): {
      const { payload } = action;

      return produce(state, (draft) => {
        const shops = payload.shops.filter(
          (i: any) => i.channel_code === CHANNEL,
        );

        const countries = shops.reduce((acc, s) => {
          if (acc.find((s1) => s1.countryCode === s.country_code)) {
            return acc;
          } else {
            return acc.concat({
              countryCode: s.country_code,
              countryName: s.country_name,
            });
          }
        }, []);

        const countryOptions = countries.map((c) => ({
          id: c.countryCode,
          text: c.countryName,
        }));

        const shopOptions = shops.map((i) => ({
          id: i.shop_eid,
          text: `${i.country_code} / ${i.shop_name}`,
          countryCode: i.country_code,
          currency: i.country_exchange,
        }));

        draft.shops = shops;
        draft.availableFilter = {
          countries: countryOptions,
          shops: shopOptions,
        };

        draft.optionGlobalFilters = {
          countries: countryOptions,
          shops: shopOptions,
        };

        draft.globalFilters = {
          ...state.globalFilters,
          countries: countryOptions,
          shops: shopOptions,
          updateCount: state.globalFilters.updateCount + 1,
        };
      });
    }
    case actions.filterCountry.type(): {
      const { payload } = action;
      return produce(state, (draft) => {
        const shopOptions = state.availableFilter.shops.reduce(
          (acc, elem) => {
            if (
              payload.countries.some(
                (item: any) => item.id === elem.countryCode,
              )
            ) {
              return [...acc, elem];
            } else {
              return acc;
            }
          },
          [],
        );

        draft.globalFilters.countries = payload.countries;
        draft.optionGlobalFilters.shops = shopOptions;
        draft.globalFilters.shops = shopOptions;
      });
    }
    case actions.filterShop.type(): {
      const { payload } = action;
      return produce(state, (draft) => {
        draft.globalFilters.shops = payload.shops;
      });
    }
    case actions.filterDateRange.type(): {
      const { payload } = action;
      return produce(state, (draft) => {
        draft.globalFilters.timeRange = payload.timeRange;
      });
    }
    case actions.updateGlobalFilter.type(): {
      return produce(state, (draft) => {
        draft.globalFilters.updateCount =
          state.globalFilters.updateCount + 1;
        draft.pagination = {
          page: 1,
        };
      });
    }
    case actions.updateLocalFilter.type(): {
      const { payload } = action;
      return produce(state, (draft) => {
        draft.localFilter.searchText = payload.localFilter.searchText;
        draft.localFilter.status =
          payload.localFilter.status || state.localFilter.status;
      });
    }
    case actions.updatePagination.type(): {
      const { payload } = action;
      return produce(state, (draft) => {
        draft.pagination = payload.pagination;
      });
    }
    case actions.getShopAds.fetchType(): {
      const { payload } = action;
      return produce(state, (draft) => {
        draft.shopAds = {
          items: payload.items,
          selectedIds: state.shopAds.selectedIds,
        };
        draft.pagination = {
          itemCount: payload.pagination.itemCount,
          limit: payload.pagination.limit,
          pageCount: payload.pagination.pageCount,
          page: payload.pagination.page,
        };
        draft.sort = payload.sort;
      });
    }
    case actions.getShopAdsKeywords.fetchType(): {
      const { payload } = action;
      return produce(state, (draft) => {
        draft.keywords = {
          items: payload.items,
          selectedIds: state.keywords.selectedIds,
        };
        draft.pagination = {
          itemCount: payload.pagination.itemCount,
          limit: payload.pagination.limit,
          pageCount: payload.pagination.pageCount,
          page: payload.pagination.page,
        };
      });
    }
    case actions.updateSelectedShopAds.type(): {
      return produce(state, (draft) => {
        draft.shopAds.selectedIds = action.payload.items.map(
          (i) => i.shopAdsId,
        );
      });
    }
    case actions.updateSelectedShopAdKeywords.type(): {
      return produce(state, (draft) => {
        draft.keywords.selectedIds = action.payload.items.map(
          (i) => i.keywordId,
        );
      });
    }
    case actions.onSelectShopAds.type(): {
      const { payload } = action;

      return produce(state, (draft) => {
        if (payload.items === null) {
          draft.shopAds.selectedIds = payload.checked
            ? state.shopAds.items.map((i) => i.shopAdsId)
            : [];
        } else {
          let selectedIds = new Set(state.shopAds.selectedIds);
          let checkedIds = payload.items.map((i) => i.shopAdsId);

          for (let id of checkedIds) {
            if (payload.checked) selectedIds.add(id);
            else selectedIds.delete(id);
          }

          draft.shopAds.selectedIds = [...selectedIds];
        }
      });
    }
    case actions.onSelectShopAdsKeyword.type(): {
      const { payload } = action;

      return produce(state, (draft) => {
        if (payload.items === null) {
          draft.keywords.selectedIds = payload.checked
            ? state.keywords.items.map((i) => i.keywordId)
            : [];
        } else {
          let selectedIds = new Set(state.keywords.selectedIds);
          let checkedIds = payload.items.map((i) => i.keywordId);

          for (let id of checkedIds) {
            if (payload.checked) selectedIds.add(id);
            else selectedIds.delete(id);
          }

          draft.keywords.selectedIds = [...selectedIds];
        }
      });
    }

    case actions.getRuleActionMetrics.fetchType(): {
      const { payload } = action;
      return produce(state, (draft) => {
        let actionList = uniqBy(
          payload.actionMetrics,
          'action_code',
        ).map((i) => ({ name: i.action_name, value: i.action_code }));

        let metricList = actionList.reduce((acc, a) => {
          return {
            ...acc,
            [a.value]: payload.actionMetrics
              .filter((i) => i.action_code === a.value)
              .map((i) => ({
                name: i.metric_name,
                value: i.metric_code,
              })),
          };
        }, {});

        draft.rulesActionList = actionList;
        draft.rulesMetricList = metricList;
      });
    }
    case actions.getExistProductRules.fetchType(): {
      return produce(state, (draft) => {
        draft.existShopAdRuleList = action.payload.ruleList;
      });
    }
    case actions.getExistKeywordRules.fetchType(): {
      return produce(state, (draft) => {
        draft.existKeywordRuleList = action.payload.ruleList;
      });
    }
    case actions.getProductRules.fetchType(): {
      return produce(state, (draft) => {
        draft.shopAdRuleList = action.payload.ruleList;
      });
      break;
    }
    case actions.getKeywordRules.fetchType(): {
      return produce(state, (draft) => {
        draft.keywordRuleList = action.payload.ruleList;
      });
      break;
    }
    case actions.resetRuleLog.type(): {
      return produce(state, (draft) => {
        draft.ruleLogFilters = {
          searchText: '',
          timeRange: {
            start: moment().format('YYYY-MM-DD'),
            end: moment().format('YYYY-MM-DD'),
          },
          actions: [],
        };
        draft.ruleLogs = {
          items: [],
          sort: null,
          pagination: {
            limit: 10,
            page: 1,
            itemCount: 0,
            pageCount: 1,
          },
        };
      });
    }
    case actions.getRuleLogs.fetchType(): {
      return produce(state, (draft) => {
        const { logList, pagination } = action.payload;
        draft.ruleLogs = {
          ...state.ruleLogs,
          items: logList,
          pagination,
        };
      });
      break;
    }
    case actions.updateRuleLogSorting.type(): {
      return produce(state, (draft) => {
        draft.ruleLogs = {
          ...state.ruleLogs,
          sort: action.payload.sort,
        };
      });
    }
    case actions.updateRuleLogFilter.type(): {
      return produce(state, (draft) => {
        draft.ruleLogFilters = {
          ...state.ruleLogFilters,
          ...action.payload,
        };
      });
    }
    case 'REQUEST_ACTION_LOADING': {
      state = produce(state, (draft) => {
        draft.loading[action.payload.loading.section] = {
          status: action.payload.loading.status,
          error: action.payload.loading.error,
        };
      });
      return state;
      break;
    }
    default:
      return state;
  }
};

export default reducer;
