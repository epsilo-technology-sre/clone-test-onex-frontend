import { produce } from 'immer';
import { actions } from './actions';
import { CreateCampaignState } from './reducers';

export function reducerAddProduct(
  state: CreateCampaignState['setupProduct'],
  action: { type: string; payload?: any },
) {
  switch (action.type) {
    case actions.openAddProductModal.type(): {
      state = produce(state, (draft) => {
        draft.modalAddProductVisible = action.payload.visible;
      });
      break;
    }
    case actions.getShopProducts.fetchType(): {
      state = produce(state, (draft) => {
        draft.productList = {
          ...state.productList,
          ...action.payload,
        };
      });
      state = markBucketAdded(state.productBucket, state);
      break;
    }
    case actions.resetAddProduct.type(): {
      state = produce(state, (draft) => {
        draft.productList = {
          searchText: '',
          selectedCategories: [],
          items: [],
          page: 1,
          limit: 10,
          pageTotal: 1,
          itemCount: 1,
          selectedIds: [],
        };
        draft.productBucket = []
      });
      break;
    }
    case actions.addProductSelectProduct.type(): {
      state = produce(state, (draft) => {
        const selectedItems = [].concat(
          action.payload.shopProductIds,
        );
        const selected = action.payload.selected;
        const stateSelected = new Set(
          state.productList.selectedIds || [],
        );
        if (selected) {
          selectedItems.forEach((i) => stateSelected.add(i));
        } else {
          selectedItems.forEach((i) => stateSelected.delete(i));
        }
        draft.productList.selectedIds = Array.from(stateSelected);
      });
      break;
    }
    case actions.addProductSelectAll.type(): {
      state = produce(state, (draft) => {
        if (action.payload.isAdd) {
          draft.productList.selectedIds = state.productList.items.map(
            (i) => i.productId,
          );
        } else {
          draft.productList.selectedIds = [];
        }
      });
      break;
    }
    case actions.updateAddProductSearch.type(): {
      state = produce(state, (draft) => {
        draft.productList.searchText = action.payload.searchText || '';
      });
      break;
    }
    case actions.updateAddProductSelectedCategory.type(): {
      state = produce(state, (draft) => {
        draft.productList.selectedCategories = action.payload.categories;
      });
      break;
    }
    case actions.updateProductListingSearch.type(): {
      state = produce(state, (draft) => {
        draft.campaignProductList.searchText = action.payload.searchText || '';
      });
      break;
    }
    case actions.updateProductListingSelectedCategory.type(): {
      state = produce(state, (draft) => {
        draft.campaignProductList.selectedCategories = action.payload.categories;
      });
      break;
    }
    case actions.campaignProductListSelectItem.type(): {
      state = produce(state, (draft) => {
        const selectedItems = [].concat(
          action.payload.productIds,
        );
        const selected = action.payload.selected;
        const stateSelected = new Set(
          state.campaignProductList.selectedIds || [],
        );
        if (selected) {
          selectedItems.forEach((i) => stateSelected.add(i));
        } else {
          selectedItems.forEach((i) => stateSelected.delete(i));
        }
        draft.campaignProductList.selectedIds = Array.from(stateSelected);
      });
      break;
    }
    case actions.campaignProductListSelectAll.type(): {
      state = produce(state, (draft) => {
        if (action.payload.isAdd) {
          const { page, limit } = state.campaignProductList;
          const start = limit * (page - 1);
          const end = page * limit;
          const campaignList = state.campaignProductList.items.slice(start, end)
          draft.campaignProductList.selectedIds = campaignList.map(
            (i) => i.productId,
          );
        } else {
          draft.campaignProductList.selectedIds = [];
        }
      });
      break;
    }
    case actions.addProductAddToBucket.type(): {
      state = produce(state, (draft) => {
        const { isAdded, modification, shopProductIds} = action.payload;
        const selectedItems = [].concat(shopProductIds);
        let productBucket = state.productBucket;
        let updatedProductList;

        if (modification) {
          updatedProductList = state.productList.items.map((item) => {
            if (selectedItems.includes(item.productId)) {
              return {
                ...item,
                totalBudget: modification.totalBudget,
                dailyBudget: modification.dailyBudget,
              }
            }
            return item;
          })
        } else {
          updatedProductList = state.productList.items;
        }

        if (isAdded) {
          const oldItems = state.productBucket.filter((i) => !selectedItems.includes(i.productId));
          const newItems = updatedProductList.filter(
            (item) => selectedItems.includes(item.productId),
          );
          productBucket = [...oldItems, ...newItems];
        } else {
          productBucket = state.productBucket.filter(
            (i) => selectedItems.indexOf(i.productId) === -1,
          );
        }
        
        draft.productList.selectedIds = [];
        draft.productList.items = updatedProductList;
        draft.productBucket = productBucket;
      });
      state = markBucketAdded(state.productBucket, state);
      break;
    }
    case actions.addBucketToCampaignProduct.type(): {
      state = produce(state, (draft) => {
        const newItems = JSON.parse(JSON.stringify(state.campaignProductList.items));
        state.productBucket.forEach((p) => {
          const exist = newItems.some((i) => i.productId === p.productId);
          if (!exist) {
            newItems.push(p);
          }
        })
        draft.campaignProductList.items = newItems;

        draft.productBucket = [];
        draft.productList.selectedIds = [];
        draft.modalAddProductVisible = false;
      });
      state = updateCampaignProductListCategories(state.campaignProductList.items, state);
      break;
    }
    case actions.removeCampaignProduct.type(): {
      state = produce(state, (draft) => {
        let removedIds = [].concat(action.payload.productIds);
        draft.campaignProductList.items = state.campaignProductList.items.filter(
          (i) => removedIds.indexOf(i.productId) === -1,
        );
        draft.campaignProductList.selectedIds = state.campaignProductList.selectedIds.filter( (i) => removedIds.indexOf(i) === -1)
        draft.productKeywords = state.productKeywords.filter((i) => !removedIds.includes(i.product.productId));
      });
      state = updateCampaignProductListCategories(state.campaignProductList.items, state);
      break;
    }
  }

  return state;
}

function markBucketAdded(
  productBucket: any[],
  state: CreateCampaignState['setupProduct'],
) {
  return produce(state, (draft) => {
    let bucketIds = productBucket.map((i) => i.productId);
    draft.productList.items = state.productList.items.map((i) => {
      if (bucketIds.indexOf(i.productId) > -1) {
        return { ...i, added: true };
      }
      return { ...i, added: false };
    });
  });
}

function updateCampaignProductListCategories(products: any[], state: CreateCampaignState['setupProduct'],) {
  
  return produce(state, (draft) => {
    const categories = products.reduce((acc, elem) => {
      if (acc.find((i) => i.id === elem.categoryName)) {
        return acc;
      } else {
        return acc.concat({
          id: elem.categoryName,
          text: elem.categoryName,
        });
      }
    }, []);

    draft.campaignProductList.categories = categories;
    draft.campaignProductList.selectedCategories = categories;
  });
}
