import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ModalAddRuleKeywords } from '../../components/common/modal-add-rule-keywords';
import { actions } from '../../redux/shop-ads/actions';
import { ShopAdState } from '../../redux/shop-ads/reducers';

const availActionKeys = [
  'activate_shop_ads_keyword',
  'deactivate_shop_ads_keyword',
  'increase_bidding_price_sakw',
  'decrease_bidding_price_sakw',
];

export const AddRuleShopAdKeywordContainer = (props: any) => {
  const {
    open,
    shopEid,
    currency,
    products,
    keywords,
    selectedKeywords,
    onClose,
    onSubmit,
    setKeywords,
    handleCreate,
  } = props;

  const { ruleList, actionList, metricList } = useSelector(
    (state: ShopAdState) => {
      return {
        actionList: (state.rulesActionList || []).filter(
          (i) => availActionKeys.indexOf(i.value) > -1,
        ),
        metricList: state.rulesMetricList,
        ruleList: state.existKeywordRuleList,
      };
    },
  );

  const dispatch = useDispatch();

  React.useEffect(() => {
    if (open) {
      dispatch(
        actions.getExistKeywordRules({
          productId: [],
          shopEid,
        }),
      );
    }
  }, [open]);

  return (
    <div>
      <ModalAddRuleKeywords
        suffix={products.length > 1 ? 'shop ads' : 'shop ad'}
        open={open}
        onSubmit={onSubmit}
        handleCreate={handleCreate}
        onClose={onClose}
        keywords={keywords}
        products={products}
        setKeywords={setKeywords}
        selectedKeywords={selectedKeywords}
        currency={currency}
        shopEid={shopEid}
        matchType={'BROAD_MATCH'}
        ruleList={ruleList}
        actionList={actionList}
        metricList={metricList}
        // matchType={matchType.length > 0 ? 'BROAD_MATCH' : 'EXACT_MATCH'}
      />
    </div>
  );
};
