import React from 'react';
import { RuleProductCreate } from './product-create';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';
import { startMock } from '@ep/one/src/mock';
import { CreateRuleKeyword } from './keyword-create';
import { ListRuleKeyword } from './keyword-list';

export default {
  title: 'Shopee/Container Rules',
};

export function Primary() {
  startMock();
  return (
    <Provider store={store}>
      <RuleProductCreate
        isOpen={true}
        shopId={3921}
        products={[{ product_id: 1234, product_name: 'AAAA' }]}
      />
    </Provider>
  );
}

export function Keyword() {
  startMock();
  const type = ['BROAD_MATCH', 'EXACT_MATCH'];

  const dataKeywords = demoKeywords().selectedKeywords.map((i) => ({
    keyword_id: i.keyword_id,
    keyword_name: i.keyword_name,
    matchType:
      i.match_type === 'Broad Match' ? 'BROAD_MATCH' : 'EXACT_MATCH',
  }));

  return (
    <Provider store={store}>
      <CreateRuleKeyword
        isOpen={true}
        shopId={400}
        products={[{ product_id: 1234, product_name: 'AAAA' }]}
        keywords={dataKeywords}
        currency={'VND'}
        onClose={() => {}}
      />
    </Provider>
  );
}

export function KeywordList() {
  startMock();

  return (
    <Provider store={store}>
      <ListRuleKeyword
        isOpen={true}
        shopEid={400}
        keywordId={6964}
        keywordName={'whisper'}
        onClose={() => {}}
      />
    </Provider>
  );
}

function demoKeywords() {
  return {
    selectedKeywords: [
      {
        keyword_id: 6964,
        campaign_eid: 84,
        campaign_product_id: 1372,
        keyword_name: 'whisper',
        bidding_price: 180,
        keyword_status: 'running',
        keyword_type: 0,
        shop_eid: 400,
        product_name:
          'Whisper Sanitary Pads Regular Flow Wings Isi 10',
        campaign_name: 'CAMPSP5FD1D2953532F',
        current_position: null,
        child_status: {
          campaign_product_keyword_state: 1,
        },
        current_position_update_at: null,
        match_type: 'Broad Match',
        campaign_product_status: 'paused',
        sum_impression: 0,
        sum_click: 0,
        sum_cost: 0,
        sum_item_sold: 0,
        sum_gmv: 0,
        shop_name: 'P&G Official Store',
        currency: 'IDR',
        roas: 0,
        cir: 0,
        cpc: 0,
        _isDisabled: false,
      },
      {
        keyword_id: 6965,
        campaign_eid: 84,
        campaign_product_id: 1372,
        keyword_name: 'pembalut whisper',
        bidding_price: 180,
        keyword_status: 'running',
        keyword_type: 0,
        shop_eid: 400,
        product_name:
          'Whisper Sanitary Pads Regular Flow Wings Isi 10',
        campaign_name: 'CAMPSP5FD1D2953532F',
        current_position: null,
        child_status: {
          campaign_product_keyword_state: 1,
        },
        current_position_update_at: null,
        match_type: 'Broad Match',
        campaign_product_status: 'paused',
        sum_impression: 0,
        sum_click: 0,
        sum_cost: 0,
        sum_item_sold: 0,
        sum_gmv: 0,
        shop_name: 'P&G Official Store',
        currency: 'IDR',
        roas: 0,
        cir: 0,
        cpc: 0,
        _isDisabled: false,
      },
      {
        keyword_id: 6966,
        campaign_eid: 84,
        campaign_product_id: 1372,
        keyword_name: 'pembalut wanita',
        bidding_price: 639,
        keyword_status: 'running',
        keyword_type: 0,
        shop_eid: 400,
        product_name:
          'Whisper Sanitary Pads Regular Flow Wings Isi 10',
        campaign_name: 'CAMPSP5FD1D2953532F',
        current_position: null,
        child_status: {
          campaign_product_keyword_state: 1,
        },
        current_position_update_at: null,
        match_type: 'Broad Match',
        campaign_product_status: 'paused',
        sum_impression: 0,
        sum_click: 0,
        sum_cost: 0,
        sum_item_sold: 0,
        sum_gmv: 0,
        shop_name: 'P&G Official Store',
        currency: 'IDR',
        roas: 0,
        cir: 0,
        cpc: 0,
        _isDisabled: false,
      },
      {
        keyword_id: 6967,
        campaign_eid: 84,
        campaign_product_id: 1372,
        keyword_name: 'pembalut',
        bidding_price: 2734.2,
        keyword_status: 'running',
        keyword_type: 0,
        shop_eid: 400,
        product_name:
          'Whisper Sanitary Pads Regular Flow Wings Isi 10',
        campaign_name: 'CAMPSP5FD1D2953532F',
        current_position: null,
        child_status: {
          campaign_product_keyword_state: 1,
        },
        current_position_update_at: null,
        match_type: 'Broad Match',
        campaign_product_status: 'paused',
        sum_impression: 0,
        sum_click: 0,
        sum_cost: 0,
        sum_item_sold: 0,
        sum_gmv: 0,
        shop_name: 'P&G Official Store',
        currency: 'IDR',
        roas: 0,
        cir: 0,
        cpc: 0,
        _isDisabled: false,
      },
    ],
  };
}
