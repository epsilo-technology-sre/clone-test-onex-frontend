import {
  EllipsisCell,
  ListRuleCell,
  PercentCell,
} from '@ep/shopee/src/components/common/table-cell/table-cell';

export const initColumns = () => {
  return [
    {
      Header: 'Rule',
      accessor: (row: any) => ({ value: row.ruleName }),
      width: 200,
      disableSortBy: true,
      Cell: EllipsisCell,
    },
    {
      Header: 'Action',
      id: 'action',
      accessor: 'action',
      disableSortBy: true,
    },
    {
      Header: 'Rule value',
      id: 'ruleValue',
      accessor: (row: any) => ({
        number: row.ruleValue,
        currency: row.type === 'absolute' ? row.currency : '',
        prefix: row.type === 'absolute' ? '' : '%',
      }),
      disableSortBy: true,
      Cell: PercentCell,
    },
    {
      Header: 'Min bid',
      id: 'minBid',
      accessor: (row: any) => ({
        number: row.minBid,
        currency: row.currency,
      }),
      Cell: PercentCell,
      disableSortBy: true,
    },
    {
      Header: 'Max bid',
      id: 'maxBid',
      accessor: (row: any) => ({
        number: row.maxBid,
        currency: row.currency,
      }),
      Cell: PercentCell,
      disableSortBy: true,
    },
    {
      Header: 'Create',
      id: 'create',
      accessor: 'create',
      disableSortBy: true,
    },
    {
      Header: 'Condition',
      id: 'condition',
      accessor: (row: any) => row,
      width: 200,
      Cell: ListRuleCell,
      disableSortBy: true,
    },
  ];
};
