import { Snackbar } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ModalEditRuleKeywords } from '../../components/common/modal-create-rule-keywords';
import { actions } from '../../redux/shop-ads/actions';
import { ShopAdState } from '../../redux/shop-ads/reducers';

const availActionKeys = [
  'activate_shop_ads_keyword',
  'deactivate_shop_ads_keyword',
  'increase_bidding_price_sakw',
  'decrease_bidding_price_sakw',
];

export function RuleShopAdKeywordEdit(props: {
  shopId: number;
  products: { product_id: number; product_name: string }[];
  ruleList: any[];
  ruleInfo: any;
  keywords: {
    keyword_id: number;
    keyword_name: string;
    matchType: string;
    currency: string;
  }[];
  isOpen: boolean;
  onClose: Function;
  onSubmitSuccess: Function;
}) {
  const {
    actionList,
    metricList,
    isLoading = { status: false, error: null },
  } = useSelector((state: ShopAdState) => {
    return {
      actionList: (state.rulesActionList || []).filter(
        (i) => availActionKeys.indexOf(i.value) > -1,
      ),
      metricList: state.rulesMetricList,
      isLoading: state.loading[actions.editKeywordRule.type()],
    };
  });

  const dispatch = useDispatch();
  const [rules, setRules] = React.useState([]);
  const [keywords, setKeywords] = React.useState([]);
  const [submitStatus, setSubmitStatus] = React.useState<0 | 1 | 2>(
    0,
  );

  React.useEffect(() => {
    if (props.ruleList) {
      setRules(props.ruleList);
    }
  }, [props.ruleList]);

  React.useEffect(() => {
    dispatch(actions.getRuleActionMetrics());
  }, []);

  React.useEffect(() => {
    setKeywords([].concat(props.keywords));
  }, [props.keywords]);

  React.useEffect(() => {
    if (isLoading.status) {
      setSubmitStatus(1);
    }
    if (
      submitStatus === 1 &&
      isLoading.status === false &&
      !isLoading.error
    ) {
      setSubmitStatus(2);
    }

    if (submitStatus === 2) {
      props.onSubmitSuccess();
    }
  }, [isLoading, submitStatus]);

  const onSubmitRule = (rule: any) => {
    rule.listRules = rule.listRules.map((r) => ({
      ...r,
      conditionPeriod: r.period,
      typeOfPerformance: r.metric_code,
      operator: r.operator_code,
    }));
    dispatch(
      actions.editKeywordRule({
        rule,
        keywords: props.keywords,
        shopId: props.shopId,
      }),
    );
  };

  const { matchType, currency } = React.useMemo(() => {
    return {
      matchType: keywords.some((i) => i.matchType === 'Broad Match')
        ? 'BROAD_MATCH'
        : 'EXACT_MATCH',
      currency:
        keywords && keywords.length > 0
          ? keywords[0].currency
          : undefined,
    };
  }, [keywords]);

  if (!keywords || keywords.length === 0) return null;
  if (!actionList || !metricList) return null;

  return (
    <React.Fragment>
      <ModalEditRuleKeywords
        open={props.isOpen}
        rules={rules}
        ruleInfo={props.ruleInfo}
        products={props.products}
        onAddRule={(r: any) => {
          setRules(r);
        }}
        onSubmit={onSubmitRule}
        onClose={props.onClose}
        setKeywords={(keywords: any[]) => {
          setKeywords(keywords);
        }}
        keywords={keywords}
        actionList={actionList}
        metricList={metricList}
        currency={currency}
        matchType={matchType}
        isSubmitting={isLoading.status}
      />
      {isLoading.error && (
        <Snackbar
          anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
          open={!!isLoading.error}
          autoHideDuration={60000}
        >
          <Alert hidden={!isLoading.error} severity="error">
            {isLoading.error.message}
          </Alert>
        </Snackbar>
      )}
    </React.Fragment>
  );
}
