import { get } from 'lodash';
import moment from 'moment';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { DrawerRuleKeyword } from '../../components/common/drawer-rule-keyword';
import { ModalConfirmUI } from '../../components/common/modal-confirm';
import { RULE_PERIODS } from '../../constants';
import { actions } from '../../redux/actions';
import { ShopAdState } from '../../redux/shop-ads/reducers';
import { RuleShopAdKeywordEdit } from './shop-ads-keyword-edit';

export function ListRuleShopAdKeyword(props: {
  shopEid: number;
  keywordId: number;
  keywordName: string;
  productId: number;
  productName: string;
  currency: string;
  matchType: string;
  allowUpdate: boolean;
  isOpen: boolean;
  onClose: Function;
  onUpdateSuccess: Function;
  onAddRule: Function;
}) {
  const {
    ruleList,
    actionList,
    metricList,
    isLoading = { status: false, error: null },
    isDeleting = { status: false, error: null },
  } = useSelector((state: ShopAdState) => {
    return {
      actionList: state.rulesActionList,
      metricList: state.rulesMetricList,
      ruleList: state.keywordRuleList,
      isLoading: state.loading[actions.getKeywordRules.type()],
      isDeleting: state.loading[actions.deleteKeywordRule.type()],
    };
  });

  const [ruleKeywordEdit, setRuleKeywordEdit] = React.useState({
    isOpen: false,
    keywords: [],
    products: [],
    ruleList: [],
    ruleInfo: {},
    shopId: null,
  });

  const [showConfirm, setShowConfirm] = React.useState({
    show: false,
    rule: {},
  });

  const [startDelete, setStartDelete] = React.useState(false);

  const dispatch = useDispatch();
  React.useEffect(() => {
    if (props.isOpen && (!actionList || actionList.length === 0)) {
      dispatch(actions.getRuleActionMetrics());
    }
  }, [metricList, actionList, props.isOpen]);

  React.useEffect(() => {
    if (actionList && props.keywordId && props.shopEid) {
      getRuleList();
    }
  }, [actionList, props.keywordId, props.shopEid]);

  React.useEffect(() => {
    if (startDelete) {
      if (!isDeleting.status) {
        getRuleList();
        setStartDelete(false);
      }
    }
  }, [isDeleting]);

  const getRuleList = () => {
    if (actionList) {
      dispatch(
        actions.getKeywordRules({
          keywordId: props.keywordId,
          shopEid: props.shopEid,
        }),
      );
    }
  };

  let cRuleList = React.useMemo(() => {
    if (metricList) {
      return (ruleList || []).map((i) => {
        const mlist = metricList[i.action_code];
        let biddingInfo = get(i, 'data.bidding_price_data', {});

        return {
          ruleName: i.rule_name,
          ruleId: i.rule_id,
          timeline: [
            moment(i.timeline_from, 'YYYY-MM-DD').format(
              'MM/DD/YYYY',
            ),
            '-',
            moment(i.timeline_to, 'YYYY-MM-DD').format('MM/DD/YYYY'),
          ].join(' '),
          actionCode: i.action_code,
          status: get(
            actionList.find((a) => a.value === i.action_code),
            'name',
            '-missing-',
          ),
          conditionStr: i.condition.map((c) => {
            let pName = get(
              mlist.find((m) => m.value === c.metric_code),
              'name',
              '-missing-',
            );
            let period = RULE_PERIODS.find(
              (p) => p.value === c.period,
            )?.name;
            let op = c.operator_code;
            let val = c.value;

            return [pName, period, op, val].join(' ');
          }),
          statusInfo: {
            ...biddingInfo,
            min: biddingInfo.minimum,
            max: biddingInfo.maximum,
          },
        };
      });
    }
  }, [ruleList, actionList, metricList]);

  const handleOpenAddNewRule = () => {
    props.onAddRule({
      shop_eid: props.shopEid,
      shop_ads_keyword_id: props.keywordId,
      keyword_name: props.keywordName,
      currency: props.currency,
      match_type: props.matchType,
      shop_ads_eid: props.productId,
      shop_ads_name: props.productName,
    });
  };

  const handleOpenEditRule = (rule: any) => {
    const existRule = ruleList.find((r) => r.rule_id === rule.ruleId);
    if (existRule) {
      const ruleInfo = {
        ruleId: existRule.rule_id,
        ruleName: existRule.rule_name,
        action: existRule.action_code,
        conditions: existRule.condition,
        fromDate: moment(
          existRule.timeline_from,
          'YYYY-MM-DD',
        ).format('DD/MM/YYYY'),
        toDate: moment(existRule.timeline_to, 'YYYY-MM-DD').format(
          'DD/MM/YYYY',
        ),
      };

      if (existRule.data.bidding_price_data) {
        ruleInfo.biddingPrice = {
          min: existRule.data.bidding_price_data.minimum,
          max: existRule.data.bidding_price_data.maximum,
          step: existRule.data.bidding_price_data.step,
          type: existRule.data.bidding_price_data.type,
        };
      }

      const ruleList = existRule.condition.map((item, index) => ({
        id: `condition${index}`,
        conditionPeriod: item.period,
        operator: item.operator_code,
        typeOfPerformance: item.metric_code,
        value: item.value,
      }));
      setRuleKeywordEdit((state) => ({
        ...state,
        isOpen: true,
        shopId: props.shopEid,
        keywords: [
          {
            keyword_id: props.keywordId,
            keyword_name: props.keywordName,
            currency: props.currency,
            matchType: props.matchType,
          },
        ],
        products: [
          // {
          //   product_id: props.productId,
          //   product_name: props.productName,
          // },
        ],
        ruleInfo,
        ruleList,
      }));
    }
  };

  const handleAddRuleSuccess = () => {
    props.onUpdateSuccess();
    getRuleList();
    setCreateRuleKeyword((state) => ({
      ...state,
      isOpen: false,
    }));
  };

  const handleEditRuleSuccess = () => {
    getRuleList();
    setRuleKeywordEdit((state) => ({
      ...state,
      isOpen: false,
    }));
  };

  const handleOpenDeleteRule = (rule) => {
    setShowConfirm({
      ...showConfirm,
      show: true,
      rule,
    });
  };

  const handleDeleteRule = () => {
    setStartDelete(true);
    dispatch(
      actions.deleteKeywordRule({
        ruleId: showConfirm.rule.ruleId,
        keywordId: props.keywordId,
        shopEid: props.shopEid,
      }),
    );
    setShowConfirm({
      ...showConfirm,
      show: false,
    });
    props.onUpdateSuccess();
  };

  return (
    <React.Fragment>
      <DrawerRuleKeyword
        open={props.isOpen}
        rules={cRuleList}
        title={props.keywordName}
        code={String(props.keywordId)}
        allowUpdate={props.allowUpdate}
        onClose={props.onClose}
        isLoading={isLoading.status}
        onOpenAddRule={handleOpenAddNewRule}
        onOpenEditRule={handleOpenEditRule}
        onDeleteRule={handleOpenDeleteRule}
      />

      <RuleShopAdKeywordEdit
        isOpen={ruleKeywordEdit.isOpen}
        keywords={ruleKeywordEdit.keywords}
        shopId={ruleKeywordEdit.shopId}
        products={ruleKeywordEdit.products}
        ruleInfo={ruleKeywordEdit.ruleInfo}
        ruleList={ruleKeywordEdit.ruleList}
        onSubmitSuccess={handleEditRuleSuccess}
        onClose={() => {
          setRuleKeywordEdit((state) => ({
            ...state,
            isOpen: false,
          }));
        }}
      />

      {showConfirm.show && (
        <ModalConfirmUI
          open={showConfirm.show}
          labelSubmit="OK"
          title="Are you sure remove this rule?"
          onClose={() =>
            setShowConfirm({ ...showConfirm, show: false })
          }
          onSubmit={handleDeleteRule}
        />
      )}
    </React.Fragment>
  );
}
