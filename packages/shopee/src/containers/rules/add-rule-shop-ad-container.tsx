import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ModalAddRule } from '../../components/common/modal-add-rule';
import { actions } from '../../redux/shop-ads/actions';
import { ShopAdState } from '../../redux/shop-ads/reducers';

const availActionKeys = ['resume_shop_ads', 'pause_shop_ads'];

export const AddRuleShopAdContainer = (props: any) => {
  const {
    open,
    products,
    shopEid,
    onClose,
    onSubmit,
    setProduct,
    handleCreate,
  } = props;

  const { ruleList, actionList, metricList } = useSelector(
    (state: ShopAdState) => {
      return {
        actionList: (state.rulesActionList || []).filter(
          (i) => availActionKeys.indexOf(i.value) > -1,
        ),
        metricList: state.rulesMetricList,
        ruleList: state.existShopAdRuleList,
      };
    },
  );
  const dispatch = useDispatch();

  React.useEffect(() => {
    if (open) {
      dispatch(
        actions.getExistProductRules({
          productId: [],
          shopEid,
        }),
      );
    }
  }, [open]);

  return (
    <div>
      <ModalAddRule
        suffix={products.length > 1 ? 'shop ads' : 'shop ad'}
        open={open}
        products={products}
        ruleList={ruleList}
        actionList={actionList}
        metricList={metricList}
        shopEid={shopEid}
        onSubmit={onSubmit}
        onClose={onClose}
        handleCreate={handleCreate}
        setProduct={setProduct}
      ></ModalAddRule>
    </div>
  );
};
