import { ThemeProvider } from '@material-ui/core/styles';
import React from 'react';
import { Provider } from 'react-redux';
import 'react-toastify/dist/ReactToastify.css';
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import { rootSaga } from '../../redux/create-campaign/actions';
import { reducer } from '../../redux/create-campaign/reducers';
import ShopeeTheme from '../../shopee-theme';
import { AddKeywordContainer } from './add-keyword-container';

export default {
  title: 'Shopee / Container',
};

export const AddKeywordDialog = () => {
  const store = makeStore();
  return (
    <Provider store={store}>
      <ThemeProvider theme={ShopeeTheme}>
        <AddKeywordContainer />
      </ThemeProvider>
    </Provider>
  );
};

function makeStore() {
  const sagaMiddleware = createSagaMiddleware();
  let composeEnhancers = composeWithDevTools({
    name: 'shopee/create-campaign',
  });

  const store = createStore(
    reducer,
    composeEnhancers(applyMiddleware(sagaMiddleware)),
  );

  sagaMiddleware.run(rootSaga);

  return store;
}
