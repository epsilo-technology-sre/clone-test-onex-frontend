import { AddKeyword } from '@ep/shopee/src/components/add-product-keyword/add-keyword';
import { actions } from '@ep/shopee/src/redux/create-campaign/actions';
import { CreateCampaignState } from '@ep/shopee/src/redux/create-campaign/reducers';
import moment from 'moment';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import { initColumns } from './add-keyword-columns';

export const AddKeywordExistCampaignContainer = (props: any) => {
  const dispatch = useDispatch();
  const [isSubmitting, setSubmitting] = React.useState(false);
  const state = useSelector(
    ({
      flow,
      setupCampaign: { shop, shopCurrency, campaignId },
      addKeyword: {
        products,
        keywordBucket,
        selectedProducts,
        selectedKeywords,
        keywords,
        searchText,
        pagination,
      },
      loading,
    }: CreateCampaignState) => {
      return {
        flow,
        shop,
        shopCurrency,
        campaignId,
        products,
        keywordBucket,
        selectedProducts,
        selectedKeywords,
        keywords,
        searchText,
        pagination,
        loading,
      };
    },
  );

  const {
    flow,
    shop: shopId,
    shopCurrency,
    campaignId,
    products,
    keywordBucket,
    selectedProducts,
    selectedKeywords,
    keywords,
    searchText,
    pagination,
    loading,
  } = state;

  useEffect(() => {
    const productSIds = products.map((i) => i.productSId).join(',');
    dispatch(
      actions.getSuggestKeywords({
        shopId,
        productSIds,
      }),
    );
  }, []);

  const handleUpdateSelectedKeywordPrice = (value: any) => {
    dispatch(
      actions.updateSelectedKeywordPrice({
        biddingPrice: value.biddingPrice,
        matchType: value.matchType,
      }),
    );
  };
  const handleSearch = (searchText: any, currency: string) => {
    dispatch(actions.updateKeywordSearch({ searchText, currency }));
  };

  const handleAddKeywordsToBucket = (keywords: any[]) => {
    dispatch(actions.addKeywordToBucket({ keywords }));
  };

  const handleRemoveKeywordFromBucket = (
    keyword: any,
    product: any,
  ) => {
    dispatch(
      actions.removeKeyword({
        keyword,
        product,
      }),
    );
  };

  const handleResetKeywordBucket = () => {
    dispatch(actions.removeAllKeywords());
  };

  const handleAddCustomKeywords = (
    keywordNames: any,
    currency: any,
  ) => {
    dispatch(actions.addCustomKeywords({ keywordNames, currency }));
  };

  const handleSelectProductBucketItem = (product: any) => {
    dispatch(actions.updateSelectedProducts({ product }));
  };

  const handleSelectKeywordItem = (keyword: any, isAdd: boolean) => {
    dispatch(actions.selectKeyword({ keyword, isAdd }));
  };

  const handleSelectAllKeywordItems = (checked: boolean) => {
    dispatch(
      actions.selectAllKeywords({
        isAdd: checked,
      }),
    );
  };

  const handleChangePagination = (pagination: any) => {
    dispatch(actions.updateKeywordPagination({ pagination }));
  };

  const handleBackToPrevious = () => {
    dispatch(actions.openAddKeywordModal({ visible: false }));
    dispatch(actions.openAddProductModal({ visible: true }));
    dispatch(actions.resetAddKeyword());
  };

  const handleCancel = () => {
    dispatch(actions.openAddKeywordModal({ visible: false }));
    dispatch(actions.resetAddKeyword());
  };

  const addProductKeywordToCampaign = (productsInBucket: any) => {
    const productList = productsInBucket.map((item: any) => {
      return {
        product_eid: item.product.productId,
        value_total: item.product.totalBudget,
        value_daily: item.product.dailyBudget,
      };
    });
    let keywords = [];
    productsInBucket.forEach((productKw: any) => {
      keywords = keywords.concat(
        productKw.keywords.map((item: any) => {
          return {
            name: item.keywordName,
            bidding_price: item.biddingPrice,
            match_type:
              item.matchType === 'Broad Match' ||
              item.matchType === 'broad_match'
                ? 'broad_match'
                : 'exact_match',
            product_eid: productKw.product.productId,
          };
        }),
      );
    });
    const productIds = productList.map(
      (item: any) => item.product_eid,
    );
    const today = moment().format('DD/MM/YYYY');
    const params = {
      campaign_eid: campaignId,
      campaign_time_line_from: today,
      campaign_time_line_to: today,
      campaign_daily_budget: 0,
      campaign_total_budget: 0,
      shop_eids: [shopId],
      product_eids: productIds,
      products: productList,
      keywords,
    };
    setSubmitting(true);
    dispatch(actions.createCampaign({ params }));
  };

  const addKeywordToProduct = (productsInBucket: any) => {
    let campaignProductId = productsInBucket.map(
      (i) =>
        i.product.campaignProductId || i.product.campaign_product_id,
    );
    campaignProductId = [...new Set(campaignProductId)];

    let keywords = [];
    productsInBucket.forEach((productKw: any) => {
      keywords = keywords.concat(
        productKw.keywords.map((item: any) => {
          return {
            name: item.keywordName,
            bidding_price: item.biddingPrice,
            campaign_product_id:
              productKw.product.campaignProductId ||
              productKw.product.campaign_product_id,
            match_type:
              item.matchType === 'Broad Match' ||
              item.matchType === 'broad_match'
                ? 'broad_match'
                : 'exact_match',
          };
        }),
      );
    });
    const params = {
      shop_eids: [shopId],
      campaign_product_ids: campaignProductId,
      keywords,
    };
    setSubmitting(true);
    dispatch(actions.saveAddKeywords({ params }));
  };

  React.useEffect(() => {
    if (loading.createCampaign) {
      setSubmitting(false);
      if (loading.createCampaign.status) {
        toast.success(loading.createCampaign.error);
        props.onAddingSuccess();
      } else {
        toast.error(loading.createCampaign.error);
      }
    }
  }, [loading.createCampaign]);

  const handleSaveKeyword = () => {
    if (keywordBucket.length !== products.length) {
      toast.error(
        'At least one product has no keyword. Please add keyword',
      );
    } else {
      if (flow === 'addProduct') {
        addProductKeywordToCampaign(keywordBucket);
      } else {
        addKeywordToProduct(keywordBucket);
      }
    }
  };

  const headers = React.useMemo(
    () =>
      initColumns({
        onAddToBucket: (value: any) =>
          handleAddKeywordsToBucket([value]),
      }),
    [],
  );

  return (
    <>
      <AddKeyword
        tableHeaders={headers}
        currency={shopCurrency}
        productBucket={products}
        selectedProducts={selectedProducts}
        keywords={keywords}
        selectedKeywords={selectedKeywords}
        searchText={searchText}
        pagination={pagination}
        keywordBucket={keywordBucket}
        hasPreviousStep={props.hasPreviousStep}
        onUpdateSelectedKeywordPrice={
          handleUpdateSelectedKeywordPrice
        }
        onSearch={handleSearch}
        onAddKeywordsToBucket={handleAddKeywordsToBucket}
        onRemoveKeywordFromBucket={handleRemoveKeywordFromBucket}
        onResetKeywordBucket={handleResetKeywordBucket}
        onAddCustomKeywords={handleAddCustomKeywords}
        onSelectProductBucketItem={handleSelectProductBucketItem}
        onSelectKeywordItem={handleSelectKeywordItem}
        onSelectAllKeywordItems={handleSelectAllKeywordItems}
        onChangePagination={handleChangePagination}
        onBackToPrevious={handleBackToPrevious}
        onSave={handleSaveKeyword}
        onCancel={handleCancel}
        isSubmitting={isSubmitting}
      ></AddKeyword>
    </>
  );
};
