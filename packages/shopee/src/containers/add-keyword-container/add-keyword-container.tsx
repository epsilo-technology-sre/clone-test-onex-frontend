import { AddKeyword } from '@ep/shopee/src/components/add-product-keyword/add-keyword';
import { actions } from '@ep/shopee/src/redux/create-campaign/actions';
import { CreateCampaignState } from '@ep/shopee/src/redux/create-campaign/reducers';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { initColumns } from './add-keyword-columns';

export const AddKeywordContainer = () => {
  const dispatch = useDispatch();
  const state = useSelector(
    ({
      setupCampaign: { shop, shopCurrency },
      addKeyword: {
        products,
        keywordBucket,
        selectedProducts,
        selectedKeywords,
        keywords,
        searchText,
        pagination,
      },
    }: CreateCampaignState) => {
      return {
        shop,
        shopCurrency,
        products,
        keywordBucket,
        selectedProducts,
        selectedKeywords,
        keywords,
        searchText,
        pagination,
      };
    },
  );

  const {
    shop: shopId,
    shopCurrency,
    products,
    keywordBucket,
    selectedProducts,
    selectedKeywords,
    keywords,
    searchText,
    pagination,
  } = state;

  useEffect(() => {
    const productSIds = products.map((i) => i.productSId).join(',');
    dispatch(
      actions.getSuggestKeywords({
        shopId,
        productSIds,
      }),
    );
  }, []);

  const handleUpdateSelectedKeywordPrice = (value: any) => {
    dispatch(
      actions.updateSelectedKeywordPrice({
        biddingPrice: value.biddingPrice,
        matchType: value.matchType,
      }),
    );
  };
  const handleSearch = (searchText: any, currency: string) => {
    dispatch(actions.updateKeywordSearch({ searchText, currency }));
  };

  const handleAddKeywordsToBucket = (keywords: any[]) => {
    dispatch(actions.addKeywordToBucket({ keywords }));
  };

  const handleRemoveKeywordFromBucket = (
    keyword: any,
    product: any,
  ) => {
    dispatch(
      actions.removeKeyword({
        keyword,
        product,
      }),
    );
  };

  const handleResetKeywordBucket = () => {
    dispatch(actions.removeAllKeywords());
  };

  const handleAddCustomKeywords = (
    keywordNames: any,
    currency: any,
  ) => {
    dispatch(actions.addCustomKeywords({ keywordNames, currency }));
  };

  const handleSelectProductBucketItem = (product: any) => {
    dispatch(actions.updateSelectedProducts({ product }));
  };

  const handleSelectKeywordItem = (keyword: any, isAdd: boolean) => {
    dispatch(actions.selectKeyword({ keyword, isAdd }));
  };

  const handleSelectAllKeywordItems = (checked: boolean) => {
    dispatch(
      actions.selectAllKeywords({
        isAdd: checked,
      }),
    );
  };

  const handleChangePagination = (pagination: any) => {
    dispatch(actions.updateKeywordPagination({ pagination }));
  };

  const handleBackToPrevious = () => {
    dispatch(actions.openAddKeywordModal({ visible: false }));
    dispatch(actions.openAddProductModal({ visible: true }));
    dispatch(actions.resetAddKeyword());
  };

  const handleCancel = () => {
    dispatch(actions.openAddKeywordModal({ visible: false }));
    dispatch(actions.resetAddKeyword());
  };

  const handleSaveKeyword = () => {
    dispatch(
      actions.updateProductKeywords({
        productKeywordList: keywordBucket,
      }),
    );
    dispatch(actions.openAddKeywordModal({ visible: false }));
    dispatch(actions.resetAddKeyword());
  };

  const headers = React.useMemo(
    () =>
      initColumns({
        onAddToBucket: (value: any) =>
          handleAddKeywordsToBucket([value]),
      }),
    [],
  );

  return (
    <>
      <AddKeyword
        tableHeaders={headers}
        currency={shopCurrency}
        productBucket={products}
        selectedProducts={selectedProducts}
        keywords={keywords}
        selectedKeywords={selectedKeywords}
        searchText={searchText}
        pagination={pagination}
        keywordBucket={keywordBucket}
        onUpdateSelectedKeywordPrice={
          handleUpdateSelectedKeywordPrice
        }
        onSearch={handleSearch}
        onAddKeywordsToBucket={handleAddKeywordsToBucket}
        onRemoveKeywordFromBucket={handleRemoveKeywordFromBucket}
        onResetKeywordBucket={handleResetKeywordBucket}
        onAddCustomKeywords={handleAddCustomKeywords}
        onSelectProductBucketItem={handleSelectProductBucketItem}
        onSelectKeywordItem={handleSelectKeywordItem}
        onSelectAllKeywordItems={handleSelectAllKeywordItems}
        onChangePagination={handleChangePagination}
        onBackToPrevious={handleBackToPrevious}
        onSave={handleSaveKeyword}
        onCancel={handleCancel}
      ></AddKeyword>
    </>
  );
};
