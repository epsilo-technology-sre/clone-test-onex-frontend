import { EditButton } from '@ep/shopee/src/components/common/edit-button';
import {
  BudgetCell,
  PercentCell,
  StatusCell,
  SubjectLinkCell,
  SwitchCell,
  TimelineCell,
} from '@ep/shopee/src/components/common/table-cell';
import { CampaignBudgetEditorPopover } from '@ep/shopee/src/components/edit-form/campaign-budget-editor-popover';
import { CampaignNameEditorPopover } from '@ep/shopee/src/components/edit-form/campaign-name-editor-popover';
import { CampaignTimelineEditorPopover } from '@ep/shopee/src/components/edit-form/campaign-timeline-editor-popover';
import React from 'react';
import { useLocation } from 'react-router';
import { makeLazyCell } from '../../components/common/table-cell/lazy-cell';
import { CAMPAIGN_STATUS, TABS } from '../../constants';

export const CAMPAIGN_COLUMNS = [
  {
    Header: ' ',
    id: 'switch',
    accessor: (row: any) => ({
      id: row.campaign_eid,
      row,
      isOn: row.child_status?.campaign_state,
      isDisabled: row.campaign_status === 'ended',
    }),
    sticky: 'left',
    width: 70,
    disableSortBy: true,
    Cell: SwitchCell,
  },
  {
    Header: 'Campaign',
    id: 'campaign_name',
    accessor: (row: any) => {
      const url = `${useLocation().pathname}?tab=${
        TABS.PRODUCT
      }&campaignIds=${row.campaign_eid}`;
      return {
        url: url,
        name: row.campaign_name,
        code: row.campaign_code,
        editor: row.campaign_status !== CAMPAIGN_STATUS.ENDED && (
          <CampaignNameEditorPopover
            campaign={row}
            name={row.campaign_name}
            triggerElem={<EditButton />}
            key={Math.random().toString().slice(2)}
          />
        ),
      };
    },
    sticky: 'left',
    width: 200,
    disableSortBy: true,
    Cell: SubjectLinkCell,
  },
  {
    Header: 'Status',
    id: 'campaign_status',
    sticky: 'left',
    width: 70,
    disableSortBy: true,
    accessor: (row: any) => {
      const type = row.campaign_status;
      let children = [];
      if (
        type === CAMPAIGN_STATUS.RUNNING ||
        type === CAMPAIGN_STATUS.PAUSED
      ) {
        children = [
          {
            enable: !!row.child_status?.campaign_budget_state,
            text: 'Budget',
          },
          {
            enable: !!row.child_status?.campaign_state,
            text: 'Campaign state',
          },
          {
            enable: !!row.child_status?.account_balance_state,
            text: 'Account balance',
          },
          {
            enable: !!row.child_status?.promoted_product_state,
            text: 'Promoted product state',
          },
        ];
      }
      return { type, children };
    },
    Cell: StatusCell,
  },
  {
    Header: 'Shop',
    accessor: 'shop_name',
    disableSortBy: true,
  },
  {
    Header: 'Ads item sold',
    id: 'sum_item_sold',
    accessor: (row: any) => ({ number: row.sum_item_sold }),
    Cell: PercentCell,
    width: 130,
  },
  {
    Header: 'Direct Ads item sold',
    id: 'sum_direct_item_sold',
    accessor: (row: any) => ({ number: row.sum_direct_item_sold }),
    Cell: PercentCell,
    alwaysEnable: true,
    width: 175,
  },
  {
    Header: 'Ads GMV',
    id: 'sum_gmv',
    accessor: (row: any) => ({
      number: row.sum_gmv,
      currency: row.currency,
    }),
    Cell: PercentCell,
    alwaysEnable: true,
    width: 100,
  },
  {
    Header: 'Direct Ads GMV',
    id: 'sum_direct_gmv',
    accessor: (row: any) => ({
      number: row.sum_direct_gmv,
      currency: row.currency,
    }),
    Cell: PercentCell,
    alwaysEnable: true,
    width: 150,
  },
  {
    Header: 'Cost',
    id: 'sum_cost',
    accessor: (row: any) => ({
      number: row.sum_cost,
      currency: row.currency,
    }),
    Cell: PercentCell,
    alwaysEnable: true,
  },
  {
    Header: 'ROAS',
    id: 'roas',
    accessor: (row: any) => ({ number: row.roas }),
    Cell: PercentCell,
    disableSortBy: true,
  },
  {
    Header: 'Impression',
    id: 'sum_impression',
    accessor: (row: any) => ({ number: row.sum_impression }),
    Cell: PercentCell,
  },
  {
    Header: 'Click',
    id: 'sum_click',
    accessor: (row: any) => ({ number: row.sum_click }),
    Cell: makeLazyCell(PercentCell),
  },
  {
    Header: 'CIR',
    id: 'cir',
    accessor: (row: any) => ({ number: row.cir }),
    Cell: makeLazyCell(PercentCell),
    disableSortBy: true,
  },
  {
    Header: 'CPC',
    id: 'cpc',
    accessor: (row: any) => ({
      number: row.cpc,
      currency: row.currency,
    }),
    Cell: makeLazyCell(PercentCell),
    disableSortBy: true,
    alwaysEnable: true,
  },
  {
    Header: 'Budget',
    id: 'budget',
    accessor: (row: any) => ({
      dailyBudget: row.budget_config.value_daily,
      totalBudget: row.budget_config.value_total,
      currency: row.currency,
      editor: row.campaign_status !== CAMPAIGN_STATUS.ENDED && (
        <CampaignBudgetEditorPopover
          campaign={row}
          currency={row.currency}
          total={row.budget_config.value_total}
          daily={row.budget_config.value_daily}
          triggerElem={<EditButton />}
        />
      ),
    }),
    Cell: makeLazyCell(BudgetCell),
    disableSortBy: true,
  },
  {
    Header: 'Timeline',
    id: 'timeline',
    accessor: (row: any) => ({
      fromDate: row.timeline_from,
      toDate: row.timeline_to,
      editor: row.campaign_status !== CAMPAIGN_STATUS.ENDED && (
        <CampaignTimelineEditorPopover
          campaign={row}
          start={row.timeline_from}
          end={row.timeline_to}
          triggerElem={<EditButton />}
        />
      ),
    }),
    width: 250,
    disableSortBy: true,
    Cell: makeLazyCell(TimelineCell),
  },
];
