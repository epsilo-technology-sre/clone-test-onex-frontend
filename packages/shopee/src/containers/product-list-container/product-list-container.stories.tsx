import { Box } from '@material-ui/core';
import React from 'react';
import { ProductListContainer } from './product-list-container';
import { ProductListContainer as ProductListContainerNext } from '../create-campaign/product-list-container';
import { ThemeProvider } from '@material-ui/core/styles';
import { Provider } from 'react-redux';
import ShopeeTheme from '../../shopee-theme';
import { ToastContainer } from 'react-toastify';
import { store } from '../../redux/store';
import 'react-toastify/dist/ReactToastify.css';

export default {
  title: 'Shopee / Container',
};

export const ProductList = () => {
  return (
    <Provider store={store}>
      <ThemeProvider theme={ShopeeTheme}>
        <ProductListContainer />
        <ToastContainer closeOnClick={false} hideProgressBar={true} />
      </ThemeProvider>
    </Provider>
  );
};

export const ProductListNext = () => {
  return (
    <Provider store={store}>
      <ThemeProvider theme={ShopeeTheme}>
        <ProductListContainerNext />
        <ToastContainer closeOnClick={false} hideProgressBar={true} />
      </ThemeProvider>
    </Provider>
  );
};