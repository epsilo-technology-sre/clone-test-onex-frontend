import React from 'react';
import {
  SubjectCell,
  InStockCell,
  PercentCell,
  ProductListActionCell,
  ProductBudgetCell,
} from '@ep/shopee/src/components/common/table-cell';

export const initColumns = (props: any) => {
  const { onAddKeyword, onDelete } = props;

  return [
    {
      Header: 'Product',
      id: 'productName',
      accessor: (row: any) => ({
        name: row.product_name,
        code: row.product_eid,
      }),
      sticky: 'left',
      width: 300,
      disableSortBy: true,
      Cell: SubjectCell,
    },
    {
      Header: 'In stock',
      id: 'inStock',
      accessor: (row: any) => ({
        inStock: row.product_stock,
        updatedDate: '11/11/2020 - 10:45 am',
      }),
      width: 200,
      Cell: InStockCell,
    },
    {
      Header: 'Keyword',
      id: 'keywords',
      accessor: (row: any) => ({ number: row.totalKeyword }),
      Cell: PercentCell,
    },
    {
      Header: 'Discount',
      id: 'discount',
      accessor: (row: any) => ({ number: row.discount, prefix: '%' }),
      Cell: PercentCell,
    },
    {
      Header: 'Ads Item Sold',
      id: 'itemSold',
      accessor: (row: any) => ({ number: row.sum_item_sold }),
      Cell: PercentCell,
    },
    {
      Header: 'Price',
      id: 'salePrice',
      accessor: (row: any) => ({
        number: 40000,
        currency: row.currency,
      }),
      Cell: PercentCell,
    },
    {
      Header: 'Budget',
      id: 'budget',
      accessor: (row: any) => ({
        dailyBudget: row.dailyBudget,
        totalBudget: row.totalBudget,
        currency: row.currency,
      }),
      Cell: ProductBudgetCell,
      disableSortBy: true,
    },
    {
      Header: '',
      id: 'action',
      accessor: (row: any) => ({
        product: row,
        onAddKeyword,
        onDelete,
      }),
      width: 200,
      Cell: ProductListActionCell,
      disableSortBy: true,
    },
  ];
};
