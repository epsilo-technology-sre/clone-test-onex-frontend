import { useOneFullPageLoading } from '@ep/one/src/hooks/use-loading';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AddNewShop } from '../../components/create-shop-ads';
import { CreateShopAdsState } from '../../redux/shop-ads-create/reducers';
import { actions } from '../../redux/shop-ads-create/actions';
import { AdCreative } from '../../components/create-shop-ads/ad-creative';
import { get } from 'lodash';
import { SetupKeyword } from '../../components/create-shop-ads/setup-keyword';
import {
  ContainerShopAdsAddKeyword,
  ContainerShopAdsSetupKeyword,
} from './setup-keyword';

export function ContainerShopAdsCreate() {
  const loading = useOneFullPageLoading();
  const dispatch = useDispatch();

  let content = null;

  const { currentStep, shopList, setupAdsInfo } = useSelector(
    (state: CreateShopAdsState) => {
      return {
        currentStep: state.currentStep,
        shopList: state.shopList,
        setupAdsInfo: state.setupAdsInfo,
      };
    },
  );
  useEffect(() => {
    dispatch(actions.getShopList());
  }, [currentStep]);

  if (currentStep === 'AdsInfo' && shopList) {
    content = (
      <AddNewShop
        defaultAdInfo={setupAdsInfo}
        shops={shopList}
        onSubmit={(values) => {
          dispatch(actions.setAdsInfo({ adsInfo: values }));
          dispatch(actions.setStep({ step: 'AdsCreative' }));
        }}
      />
    );
  } else if (currentStep === 'AdsCreative') {
    content = <ContainerAdCreative />;
  } else if (currentStep === 'SetupKeywords') {
    content = (
      <React.Fragment>
        <ContainerShopAdsSetupKeyword />
        <ContainerShopAdsAddKeyword />
      </React.Fragment>
    );
  }

  return (
    <React.Fragment>
      {content}
      {loading}
    </React.Fragment>
  );
}

function ContainerAdCreative() {
  const { categoryList, shopId, adsCreative } = useSelector(
    (state: CreateShopAdsState) => {
      return {
        categoryList: get(state, 'adsCreative.optionCategories', []),
        shopId: state.setupAdsInfo.shop,
        adsCreative: get(state, 'adsCreative', {
          shopCategory: '',
          isMainShop: true,
          tagline: '',
        }),
      };
    },
  );
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(actions.getShopCategory({ shopIds: [shopId] }));
  }, [shopId]);

  const onSubmit = (values) => {
    dispatch(actions.setAdsCreative({ adsCreative: values }));
    dispatch(actions.setStep({ step: 'SetupKeywords' }));
  };

  return (
    <AdCreative
      categoryList={categoryList}
      adsCreative={adsCreative}
      onSubmit={onSubmit}
    />
  );
}
