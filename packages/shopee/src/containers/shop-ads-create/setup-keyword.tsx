import { useOneAlert } from '@ep/one/src/hooks/use-alert';
import { actions } from '@ep/shopee/src/redux/shop-ads-create/actions';
import { orderBy } from 'lodash';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { ModalConfirmUI } from '../../components/common/modal-confirm';
import { CreateShopAdsAddKeywords } from '../../components/create-shop-ads/add-keyword-ads';
import { initColumns } from '../../components/create-shop-ads/add-keyword-columns';
import { SetupKeyword } from '../../components/create-shop-ads/setup-keyword';
import { CreateShopAdsState } from '../../redux/shop-ads-create/reducers';

export const ContainerShopAdsAddKeyword = () => {
  const dispatch = useDispatch();
  const state = useSelector(
    ({
      setupAdsInfo: { shop, currency },
      keywordList,
      addKeyword: {
        keywordBucket,
        selectedProducts,
        selectedKeywords,
        keywords,
        searchText,
        pagination,
        isShowAddKeywordModal,
      },
    }: CreateShopAdsState) => {
      return {
        shop,
        shopCurrency: currency,
        products: [],
        keywordBucket,
        selectedProducts,
        selectedKeywords,
        keywords,
        searchText,
        pagination,
        isShowAddKeywordModal,
        addedKeywordList: keywordList,
      };
    },
  );

  const {
    shop: shopId,
    shopCurrency,
    products,
    keywordBucket,
    selectedProducts,
    selectedKeywords,
    keywords,
    searchText,
    pagination,
    isShowAddKeywordModal,
    addedKeywordList,
  } = state;

  useEffect(() => {
    dispatch(
      actions.getSuggestKeywords({
        shopId,
      }),
    );
  }, []);

  const [
    confirmInvalidKeywords,
    showConfirmInvalidKeywords,
  ] = React.useState({ numInvalidKeywords: 0, display: false });

  let presentKeywords = React.useMemo(() => {
    const alladded = addedKeywordList.concat(keywordBucket);
    return keywords.map((i) => {
      if (alladded.find((i1) => i1.keywordId === i.keywordId)) {
        return { ...i, added: true };
      } else {
        return { ...i, added: false };
      }
    });
  }, [keywordBucket, addedKeywordList, keywords]);

  const handleUpdateSelectedKeywordPrice = (value: any) => {
    dispatch(
      actions.updateSelectedKeywordPrice({
        biddingPrice: value.biddingPrice,
        matchType: value.matchType,
      }),
    );
  };
  const handleSearch = (searchText: any, currency: string) => {
    dispatch(actions.updateKeywordSearch({ searchText, currency }));
  };

  const handleAddKeywordsToBucket = (keywords: any[]) => {
    console.info({ keywords });
    dispatch(actions.addKeywordToBucket({ keywords }));
  };

  const handleRemoveKeywordFromBucket = (keyword: any) => {
    dispatch(
      actions.removeKeyword({
        keyword,
      }),
    );
  };

  const handleResetKeywordBucket = () => {
    dispatch(actions.removeAllKeywords());
  };

  const handleAddCustomKeywords = (
    keywordNames: any,
    currency: any,
  ) => {
    dispatch(actions.addCustomKeywords({ keywordNames, currency }));
  };

  const handleSelectProductBucketItem = (product: any) => {
    dispatch(actions.updateSelectedProducts({ product }));
  };

  const handleSelectKeywordItem = (keyword: any, isAdd: boolean) => {
    dispatch(actions.selectKeyword({ keyword, isAdd }));
  };

  const handleSelectAllKeywordItems = (checked: boolean) => {
    dispatch(
      actions.selectAllKeywords({
        isAdd: checked,
      }),
    );
  };

  const handleChangePagination = (pagination: any) => {
    dispatch(actions.updateKeywordPagination({ pagination }));
  };

  const handleBackToPrevious = () => {
    dispatch(actions.openAddKeywordModal({ visible: false }));
    dispatch(actions.resetAddKeyword());
  };

  const handleCancel = () => {
    dispatch(actions.openAddKeywordModal({ visible: false }));
    dispatch(actions.resetAddKeyword());
  };

  const handleConfirmationForContainsInvalidKeywords = () => {
    const numKeywords = keywordBucket.filter(
      (i) => i.isValid === false,
    ).length;
    if (numKeywords) {
      showConfirmInvalidKeywords({
        numInvalidKeywords: numKeywords,
        display: true,
      });
    } else {
      handleSaveKeyword();
    }
  };

  const handleSaveKeyword = () => {
    dispatch(
      actions.updateShopAdsKeywords({
        keywordList: keywordBucket,
      }),
    );
    dispatch(actions.openAddKeywordModal({ visible: false }));
    dispatch(actions.resetAddKeyword());
  };

  const handleRemoveCustomKeyword = (keyword) => {
    dispatch(
      actions.removeCustomKeywords({
        keywordIds: [keyword.keywordId],
      }),
    );
  };

  const headers = React.useMemo(
    () =>
      initColumns({
        onAddToBucket: (value: any) =>
          handleAddKeywordsToBucket([value]),
        onRemoveKeyword: (keyword: any) => {
          handleRemoveCustomKeyword(keyword);
        },
      }),
    [],
  );

  return (
    <>
      <CreateShopAdsAddKeywords
        tableHeaders={headers}
        openModalAddKeyword={isShowAddKeywordModal}
        currency={shopCurrency}
        productBucket={products}
        selectedProducts={selectedProducts}
        keywords={presentKeywords}
        selectedKeywords={selectedKeywords}
        searchText={searchText}
        pagination={pagination}
        keywordBucket={keywordBucket}
        onUpdateSelectedKeywordPrice={
          handleUpdateSelectedKeywordPrice
        }
        onSearch={handleSearch}
        onAddKeywordsToBucket={handleAddKeywordsToBucket}
        onRemoveKeywordFromBucket={handleRemoveKeywordFromBucket}
        onResetKeywordBucket={handleResetKeywordBucket}
        onAddCustomKeywords={handleAddCustomKeywords}
        onSelectProductBucketItem={handleSelectProductBucketItem}
        onSelectKeywordItem={handleSelectKeywordItem}
        onSelectAllKeywordItems={handleSelectAllKeywordItems}
        onChangePagination={handleChangePagination}
        onBackToPrevious={handleBackToPrevious}
        onSave={handleConfirmationForContainsInvalidKeywords}
        onCancel={handleCancel}
      ></CreateShopAdsAddKeywords>
      <ModalConfirmUI
        title={`Continue without invalid keywords?`}
        open={confirmInvalidKeywords.display}
        content={`
        ${confirmInvalidKeywords.numInvalidKeywords} keywords are reserved & can't be used.
        Do you want to continue without them?`}
        labelSubmit={'Continue'}
        onClose={() => {
          showConfirmInvalidKeywords({
            numInvalidKeywords: 0,
            display: false,
          });
        }}
        onSubmit={() => {
          showConfirmInvalidKeywords({
            numInvalidKeywords: 0,
            display: false,
          });
          handleSaveKeyword();
        }}
      />
    </>
  );
};

export function ContainerShopAdsSetupKeyword() {
  const dispatch = useDispatch();
  const history = useHistory();
  const { keywordList } = useSelector((state: CreateShopAdsState) => {
    return {
      keywordList: state.keywordList,
    };
  });

  const [searchText, setSearchText] = React.useState('');
  const [sortBy, setSortBy] = React.useState([]);
  const [page, setPage] = React.useState(1);
  const [limit, setLimit] = React.useState(10);
  const [selectedIds, setSelectedIds] = React.useState([]);

  const { alert, showRequestAlert } = useOneAlert();

  const rows = React.useMemo(() => {
    let filteredItems = keywordList;

    if (searchText) {
      const lowerCaseSearch = searchText.toLowerCase();
      filteredItems = keywordList.filter((item) =>
        item.keywordName.toLowerCase().includes(lowerCaseSearch),
      );
    }

    if (sortBy.length > 0) {
      console.log('Sort by', sortBy);
      const field = sortBy[0].id;
      const direction = sortBy[0].desc ? 'desc' : 'asc';
      filteredItems = orderBy(filteredItems, [field], [direction]);
    }

    return filteredItems.slice((page - 1) * limit, page * limit);
  }, [keywordList, sortBy, searchText]);

  const handleSearchText = (event: any) => {
    if (event.key === 'Enter') {
      setSearchText(event.target.value);
    }
  };

  const openModalAddKeywords = () => {
    dispatch(actions.openAddKeywordModal({ visible: true }));
  };

  const handleSave = () => {
    new Promise((resolve, reject) => {
      dispatch(
        actions.saveNewShopAds({ promise: { resolve, reject } }),
      );
    })
      .then((rs) => {
        console.info(rs);
        showRequestAlert(rs);
        setTimeout(function () {
          history.push('/advertising/shopee/shop-ads');
        }, 3000);
      })
      .catch((rs) => {
        console.error(rs);
        showRequestAlert(rs);
      });
  };

  const handleRemoveItem = (keywordId: number) => {
    dispatch(actions.removeAddedKeyword({ removedIds: [keywordId] }));
  };

  const handleSelectItem = React.useMemo(() => {
    return (item: any, checked: boolean) => {
      console.log('handleSelectItem item', { item, checked });
      let nuSel = selectedIds;
      if (checked) {
        nuSel = selectedIds.concat(item.id);
      } else {
        nuSel = selectedIds.filter((i) => i !== item.id);
      }
    };
  }, [selectedIds]);

  const getRowId = React.useCallback((row) => {
    return String(row.keywordId);
  }, []);

  const handleSelectAll = React.useCallback(
    (checked) => {
      if (checked) {
        setSelectedIds(rows.map(getRowId));
      } else {
        setSelectedIds([]);
      }
    },
    [rows],
  );

  const handleSorting = (sortBy: any) => {
    setSortBy(sortBy);
  };

  const handleChangePage = (page: number) => {
    setPage(page);
  };

  const handleChangePageSize = (value: number) => {
    setLimit(value);
  };

  const handleBackToPrevious = () => {
    dispatch(actions.setStep({ step: 'AdsInfo' }));
  };

  console.info({ rows, selectedIds, keywordList, page, limit });

  return (
    <React.Fragment>
      <SetupKeyword
        keywords={rows}
        selectedIds={selectedIds}
        resultTotal={keywordList.length}
        page={page}
        pageSize={limit}
        getRowId={getRowId}
        onBackToPreviousStep={handleBackToPrevious}
        onChangePage={handleChangePage}
        onChangePageSize={handleChangePageSize}
        onRemoveKeyword={handleRemoveItem}
        onSearch={handleSearchText}
        onSelectRowItem={handleSelectItem}
        onSelectAllItem={handleSelectAll}
        openModalAddKeywords={openModalAddKeywords}
        onSave={handleSave}
      />
      {alert}
    </React.Fragment>
  );
}
