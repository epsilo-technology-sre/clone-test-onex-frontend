import React from 'react';
import { useLocation } from 'react-router';
import { EditButton } from '../../components/common/edit-button';
import {
  ButtonCell,
  InStockCell,
  LogCell,
  PercentCell,
  ProductBudgetCell,
  ProductCell,
  StatusCell,
  SubjectCell,
  SwitchCell,
} from '../../components/common/table-cell';
import { makeLazyCell } from '../../components/common/table-cell/lazy-cell';
import { ProductBudgetEditorPopover } from '../../components/edit-form/product-budget-editor-popover';
import { PRODUCT_STATUS, TABS } from '../../constants';

export const PRODUCT_COLUMNS = [
  {
    Header: ' ',
    id: 'switch',
    accessor: (row: any) => {
      return {
        isOn: row.child_status.is_promoted_product_status,
        isDisabled: row.status === 'ended' || row.status === 'closed',
        // isDisabled: 1,
        id: row.campaign_product_id,
        row,
      };
    },
    sticky: 'left',
    width: 70,
    disableSortBy: true,
    Cell: SwitchCell,
  },
  {
    Header: 'Product',
    id: 'product_name',
    accessor: (row: any) => {
      const location = useLocation();
      const query = new URLSearchParams(location.search);
      let url = `${location.pathname}?tab=${TABS.KEYWORD}&productIds=${row.campaign_product_id}`;
      if (query.has('campaignIds')) {
        url += `&campaignIds=${query.get('campaignIds')}`;
      }
      return {
        url: url,
        name: row.product_name,
        code: row.product_sid,
      };
    },
    sticky: 'left',
    width: 300,
    disableSortBy: true,
    Cell: ProductCell,
  },
  {
    Header: 'Status',
    id: 'status',
    sticky: 'left',
    width: 70,
    disableSortBy: true,
    accessor: (row: any) => {
      const type = row.status;
      let children = [];
      if (
        type === PRODUCT_STATUS.RUNNING ||
        type === PRODUCT_STATUS.PAUSED
      ) {
        children = [
          {
            enable: !!row.child_status.campaign_budget_state,
            text: 'Campaign Budget',
          },
          {
            enable: !!row.child_status.is_promoted_product_status,
            text: 'Promoted product state',
          },
          {
            enable: !!row.child_status.is_campaign_running,
            text: 'Campaign state',
          },
          {
            enable: !!row.child_status.is_valid_stock,
            text: 'Quantity',
          },
          {
            enable: !!row.child_status.is_cost_lower_budget,
            text: 'Budget',
          },
          {
            enable: !!row.child_status
              .is_account_balance_greater_zero,
            text: 'Account balance',
          },
        ];
      }
      return { type, children };
    },
    Cell: StatusCell,
  },
  {
    Header: 'In stock',
    id: 'product_stock',
    accessor: (row: any) => ({
      inStock: row.product_stock,
      updatedDate: row.last_pull_product_at,
    }),
    Cell: InStockCell,
    disableSortBy: true,
  },
  {
    Header: 'Discount',
    id: 'discount',
    accessor: (row: any) => ({ number: row.discount, prefix: '%' }),
    Cell: PercentCell,
    disableSortBy: true,
  },
  {
    Header: 'Campaign',
    id: 'campaign_name',
    accessor: (row: any) => ({
      name: row.campaign_name,
      code: row.shop_name,
    }),
    width: 200,
    Cell: SubjectCell,
  },
  {
    Header: 'Keyword',
    id: 'totalKeyword',
    accessor: (row: any) => ({ number: row.totalKeyword }),
    Cell: PercentCell,
    disableSortBy: true,
  },
  {
    Header: 'Ads item sold',
    id: 'sum_item_sold',
    accessor: (row: any) => ({ number: row.sum_item_sold }),
    Cell: PercentCell,
    width: 130,
  },
  {
    Header: 'Direct Ads item sold',
    id: 'sum_direct_item_sold',
    accessor: (row: any) => ({ number: row.sum_direct_item_sold }),
    Cell: PercentCell,
    width: 175,
  },
  {
    Header: 'Ads GMV',
    id: 'sum_gmv',
    accessor: (row: any) => ({
      number: row.sum_gmv,
      currency: row.currency,
    }),
    Cell: PercentCell,
    alwaysEnable: true,
    width: 100,
  },
  {
    Header: 'Direct Ads GMV',
    id: 'sum_direct_gmv',
    accessor: (row: any) => ({
      number: row.sum_direct_gmv,
      currency: row.currency,
    }),
    Cell: PercentCell,
    alwaysEnable: true,
    width: 150,
  },
  {
    Header: 'Cost',
    id: 'sum_cost',
    accessor: (row: any) => ({
      number: row.sum_cost,
      currency: row.currency,
    }),
    Cell: PercentCell,
    alwaysEnable: true,
    width: 150,
  },
  {
    Header: 'ROAS',
    id: 'roas',
    accessor: (row: any) => ({ number: row.roas }),
    Cell: PercentCell,
    disableSortBy: true,
  },
  {
    Header: 'Impression',
    id: 'sum_impression',
    accessor: (row: any) => ({ number: row.sum_impression }),
    Cell: PercentCell,
  },
  {
    Header: 'Click',
    id: 'sum_click',
    accessor: (row: any) => ({ number: row.sum_click }),
    Cell: PercentCell,
  },
  {
    Header: 'CIR',
    id: 'cir',
    accessor: (row: any) => ({ number: row.cir }),
    Cell: makeLazyCell(PercentCell),
    disableSortBy: true,
  },
  {
    Header: 'CPC',
    id: 'cpc',
    accessor: (row: any) => ({
      number: row.cpc,
      currency: row.currency,
    }),
    Cell: makeLazyCell(PercentCell),
    alwaysEnable: true,
    disableSortBy: true,
  },
  {
    Header: 'Budget',
    id: 'budget',
    accessor: (row: any) => {
      return {
        dailyBudget: row.budget_config.value_daily,
        totalBudget: row.budget_config.value_total,
        currency: row.currency,
        editor: row._isDisabled ? null : (
          <ProductBudgetEditorPopover
            product={row}
            currency={row.currency}
            total={row.budget_config.value_total}
            daily={row.budget_config.value_daily}
            triggerElem={<EditButton />}
          />
        ),
      };
    },
    Cell: makeLazyCell(ProductBudgetCell),
    disableSortBy: true,
  },
  {
    Header: 'Rule',
    id: 'rule',
    accessor: (row: any) => {
      let label = '';
      if (row.ruleCount) {
        label = row.ruleCount + ' rules';
      } else {
        const allowStatus = [
          PRODUCT_STATUS.RUNNING,
          PRODUCT_STATUS.PAUSED,
          PRODUCT_STATUS.SCHEDULED,
        ];
        if (allowStatus.includes(row.status)) {
          label = 'Add rule';
        }
      }

      return {
        ...row,
        label,
      };
    },
    Cell: makeLazyCell(ButtonCell),
    disableSortBy: true,
    width: 95,
    alwaysEnable: true,
  },
  {
    Header: '',
    id: 'ruleLog',
    accessor: (row: any) => row,
    Cell: makeLazyCell(LogCell),
    disableSortBy: true,
    width: 70,
    alwaysEnable: true,
  },
];
