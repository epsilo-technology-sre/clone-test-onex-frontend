import { Box } from '@material-ui/core';
import React from 'react';
import { AddProductContainer } from './add-product-container';
import { ThemeProvider } from '@material-ui/core/styles';
import { Provider } from 'react-redux';
import ShopeeTheme from '../../shopee-theme';
import { ToastContainer } from 'react-toastify';
import { store } from '../../redux/store';
import 'react-toastify/dist/ReactToastify.css';

export default {
  title: 'Shopee / Container',
};

export const AddProductDialog = () => {
  return (
    <Provider store={store}>
      <ThemeProvider theme={ShopeeTheme}>
        <AddProductContainer />
        <ToastContainer closeOnClick={false} hideProgressBar={true} />
      </ThemeProvider>
    </Provider>
  );
};
