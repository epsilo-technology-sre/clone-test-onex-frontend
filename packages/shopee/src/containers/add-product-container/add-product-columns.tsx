import {
  AddToBucketCell,
  InStockCell,
  PercentCell,
  ProductBudgetCell,
  SubjectCell,
} from '@ep/shopee/src/components/common/table-cell';

export const initColumns = (props: any) => {
  const { onAddToBucket } = props;

  return [
    {
      Header: 'Product',
      id: 'product_name',
      accessor: (row: any) => ({
        name: row.productName,
        code: row.productId,
      }),
      sticky: 'left',
      width: 300,
      Cell: SubjectCell,
    },
    {
      Header: 'In stock',
      id: 'product_stock',
      accessor: (row: any) => ({
        inStock: row.productStock,
        updatedDate: row.updatedAt,
      }),
      width: 200,
      Cell: InStockCell,
    },
    {
      Header: 'Keyword',
      id: 'totalKeyword',
      accessor: (row: any) => ({ number: row.totalKeyword }),
      Cell: PercentCell,
    },
    {
      Header: 'Discount',
      id: 'discount',
      accessor: (row: any) => ({ number: row.discount, prefix: '%' }),
      Cell: PercentCell,
    },
    {
      Header: 'Ads Item Sold',
      id: 'item_sold',
      accessor: (row: any) => ({ number: row.itemSold }),
      Cell: PercentCell,
    },
    {
      Header: 'Price',
      id: 'price_postsub',
      accessor: (row: any) => ({
        number: row.pricePostsub,
        currency: row.currency,
      }),
      Cell: PercentCell,
    },
    {
      Header: 'Budget',
      id: 'budget',
      accessor: (row: any) => ({
        dailyBudget: row.dailyBudget,
        totalBudget: row.totalBudget,
        currency: row.currency,
      }),
      Cell: ProductBudgetCell,
      disableSortBy: true,
    },
    {
      Header: '',
      id: 'action',
      accessor: (row: any) => ({
        item: row,
        onAddToBucket,
      }),
      width: 200,
      Cell: AddToBucketCell,
      disableSortBy: true,
    },
  ];
};
