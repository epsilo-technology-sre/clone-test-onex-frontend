import { startMock } from '@ep/one/src/mock';
import ShopeeTheme from '@ep/shopee/src/shopee-theme';
import { ThemeProvider } from '@material-ui/core/styles';
import React, { useState } from 'react';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import { rootSaga } from '../../redux/create-campaign/actions';
import {
  CreateCampaignState,
  reducer,
} from '../../redux/create-campaign/reducers';
import { CreateCampaignContainer } from './campaign-creating';

export default {
  title: 'Shopee / Create Campaign Full Flow',
};

export function Primary() {
  let [mocked, setMocked] = useState(0);
  startMock().then(() => {
    setMocked(1);
  });
  const store = makeStore();
  if (mocked) {
    return (
      <Provider store={store}>
        <ThemeProvider theme={ShopeeTheme}>
          <CreateCampaignContainer />
        </ThemeProvider>
      </Provider>
    );
  } else {
    return 'loading...';
  }
}

function makeStore() {
  const sagaMiddleware = createSagaMiddleware();
  let composeEnhancers = composeWithDevTools({
    name: 'shopee/create-campaign',
  });

  const store = createStore(
    reducer,
    demoState(),
    composeEnhancers(applyMiddleware(sagaMiddleware)),
  );

  sagaMiddleware.run(rootSaga);

  return store;
}

function demoState(): CreateCampaignState {
  return {
    currentScreen: 'setupProduct',
    setupCampaign: {
      shopList: [
        {
          shopId: 400,
          shopName: 'pgofficialstore',
          shopCurrency: 'IDR',
          channelCode: 'SHOPEE',
          channelId: '162',
        },
        {
          shopId: 405,
          shopName: 'Enfa Official Shop',
          shopCurrency: 'THB',
          channelCode: 'SHOPEE',
          channelId: '167',
        },
        {
          shopId: 503,
          shopName: 'upspring.my',
          shopCurrency: 'MYR',
          channelCode: 'SHOPEE',
          channelId: '103',
        },
        {
          shopId: 504,
          shopName: 'upspring.sg',
          shopCurrency: 'SGD',
          channelCode: 'SHOPEE',
          channelId: '165',
        },
        {
          shopId: 3820,
          shopName: 'P&G MY',
          shopCurrency: 'MYR',
          channelCode: 'SHOPEE',
          channelId: '103',
        },
        {
          shopId: 3920,
          shopName: 'lysol',
          shopCurrency: 'PHP',
          channelCode: 'SHOPEE',
          channelId: '53',
        },
      ],
      shop: 400,
      campaignName: 'hellow',
      budget: {
        total: 201,
        daily: 110,
        isOnDaily: false,
        isNoLimit: true,
      },
      timeline: {
        startDate: '05/12/2020',
        endDate: '31/12/2020',
        isNoLimit: true,
      },
      shopName: 'pgofficialstore',
      shopCurrency: 'IDR',
    },
    setupProduct: {
      modalAddProductVisible: false,
      productBucket: [],
      productList: {
        items: [],
        // items: [
        //   {
        //     productName: 'Xí muội nho/ trần bì/ mơ Đài Loan - 240g',
        //     priceRRP: 48000,
        //     pricePostsub: 40000,
        //     productStock: 1,
        //     discount: 16.67,
        //     totalKeyword: 0,
        //     productId: 3221291369,
        //     itemSold: 0,
        //     added: false,
        //   },
        //   {
        //     productName:
        //       'Cao quy linh Đài Loan (đậu đỏ/ đậu xanh/ đậu đỏ mix bắp)',
        //     priceRRP: 24000,
        //     pricePostsub: 19000,
        //     productStock: 35,
        //     discount: 20.83,
        //     totalKeyword: 0,
        //     productId: 5134644820,
        //     itemSold: 0,
        //     added: false,
        //   },
        //   {
        //     productName: 'Cổ vịt dài Tứ Xuyên siêu ngon - 50g',
        //     priceRRP: 32000,
        //     pricePostsub: 26000,
        //     productStock: 10,
        //     discount: 18.75,
        //     totalKeyword: 0,
        //     productId: 3555187182,
        //     itemSold: 0,
        //     added: false,
        //   },
        //   {
        //     productName: 'Mochi vị trà sữa trân châu Đài Loan 120g',
        //     priceRRP: 56000,
        //     pricePostsub: 48000,
        //     productStock: 19,
        //     discount: 14.29,
        //     totalKeyword: 0,
        //     productId: 6943416147,
        //     itemSold: 0,
        //     added: false,
        //   },
        //   {
        //     productName: 'Kẹo trà sữa trân châu 120g',
        //     priceRRP: 35000,
        //     pricePostsub: 26000,
        //     productStock: 17,
        //     discount: 25.71,
        //     totalKeyword: 0,
        //     productId: 6135874466,
        //     itemSold: 0,
        //     added: false,
        //   },
        //   {
        //     productName: 'Bánh quế trứng muối chảy - Con Vịt',
        //     priceRRP: 49000,
        //     pricePostsub: 11000,
        //     productStock: 15,
        //     discount: 77.55,
        //     totalKeyword: 0,
        //     productId: 5321468184,
        //     itemSold: 0,
        //     added: false,
        //   },
        //   {
        //     productName:
        //       'Chân vịt cay/ cánh vịt cay Da Cheng - Đặc sản Phúc Kiến',
        //     priceRRP: 13000,
        //     pricePostsub: 8200,
        //     productStock: 30,
        //     discount: 36.92,
        //     totalKeyword: 0,
        //     productId: 3453057661,
        //     itemSold: 0,
        //     added: false,
        //   },
        //   {
        //     productName: 'Bánh trứng chảy Liu Xin Su (hộp 6 bánh)',
        //     priceRRP: 0,
        //     pricePostsub: 80000,
        //     productStock: 3,
        //     discount: 0,
        //     totalKeyword: 0,
        //     productId: 6553325791,
        //     itemSold: 0,
        //     added: false,
        //   },
        //   {
        //     productName:
        //       'Xúc xích cay/ xúc xích ngô (bắp) - món Trung Hot',
        //     priceRRP: 0,
        //     pricePostsub: 6000,
        //     productStock: 0,
        //     discount: 0,
        //     totalKeyword: 0,
        //     productId: 6131242961,
        //     itemSold: 0,
        //     added: false,
        //   },
        //   {
        //     productName:
        //       'Bánh Phomai nướng Morinaga BAKE Creamy Cheese (45gr-10 viên)',
        //     priceRRP: 67000,
        //     pricePostsub: 56000,
        //     productStock: 0,
        //     discount: 16.42,
        //     totalKeyword: 0,
        //     productId: 5221316108,
        //     itemSold: 0,
        //     added: false,
        //   },
        //   {
        //     productName:
        //       'Loai 1 - Xúc xích cay/ xúc xích ngô (bắp) - món Trung Hot',
        //     priceRRP: 0,
        //     pricePostsub: 6000,
        //     productStock: 0,
        //     discount: 0,
        //     totalKeyword: 0,
        //     productId: 6131242998,
        //     itemSold: 0,
        //     added: false,
        //   },
        //   {
        //     productName:
        //       'Loai 1 - Bánh Phomai nướng Morinaga BAKE Creamy Cheese (45gr-10 viên)',
        //     priceRRP: 67000,
        //     pricePostsub: 56000,
        //     productStock: 0,
        //     discount: 16.42,
        //     totalKeyword: 0,
        //     productId: 5221316199,
        //     itemSold: 0,
        //     added: false,
        //   },
        // ],
        page: 1,
        limit: 10,
        itemCount: 39,
        pageTotal: 4,
        selectedIds: [
          // 3221291369,
          // 5134644820,
          // 3555187182,
          // 6943416147,
          // 6135874466,
          // 5321468184,
        ],
      },
      campaignProductList: {
        items: [],
        page: 1,
        limit: 10,
        itemCount: 1,
        selectedIds: [],
      },
      // campaignProductList: [
      //   {
      //     productName: 'Xí muội nho/ trần bì/ mơ Đài Loan - 240g',
      //     priceRRP: 48000,
      //     pricePostsub: 40000,
      //     productStock: 1,
      //     discount: 16.67,
      //     totalKeyword: 0,
      //     productId: 3221291369,
      //     itemSold: 0,
      //     added: false,
      //   },
      //   {
      //     productName:
      //       'Cao quy linh Đài Loan (đậu đỏ/ đậu xanh/ đậu đỏ mix bắp)',
      //     priceRRP: 24000,
      //     pricePostsub: 19000,
      //     productStock: 35,
      //     discount: 20.83,
      //     totalKeyword: 0,
      //     productId: 5134644820,
      //     itemSold: 0,
      //     added: false,
      //   },
      //   {
      //     productName: 'Cổ vịt dài Tứ Xuyên siêu ngon - 50g',
      //     priceRRP: 32000,
      //     pricePostsub: 26000,
      //     productStock: 10,
      //     discount: 18.75,
      //     totalKeyword: 0,
      //     productId: 3555187182,
      //     itemSold: 0,
      //     added: false,
      //   },
      //   {
      //     productName: 'Mochi vị trà sữa trân châu Đài Loan 120g',
      //     priceRRP: 56000,
      //     pricePostsub: 48000,
      //     productStock: 19,
      //     discount: 14.29,
      //     totalKeyword: 0,
      //     productId: 6943416147,
      //     itemSold: 0,
      //     added: false,
      //   },
      //   {
      //     productName: 'Kẹo trà sữa trân châu 120g',
      //     priceRRP: 35000,
      //     pricePostsub: 26000,
      //     productStock: 17,
      //     discount: 25.71,
      //     totalKeyword: 0,
      //     productId: 6135874466,
      //     itemSold: 0,
      //     added: false,
      //   },
      //   {
      //     productName: 'Bánh quế trứng muối chảy - Con Vịt',
      //     priceRRP: 49000,
      //     pricePostsub: 11000,
      //     productStock: 15,
      //     discount: 77.55,
      //     totalKeyword: 0,
      //     productId: 5321468184,
      //     itemSold: 0,
      //     added: false,
      //   },
      // ],
    },
    addKeyword: {
      modalAddKeywordVisible: false,
      products: [],
      suggestKeywords: [],
      keywords: [],
      customKeywords: [],
      keywordBucket: [],
      selectedProducts: [],
      selectedKeywords: [],
      searchText: '',
      pagination: {
        pageSize: 10,
        currentPage: 1,
        total: 0,
      },
    },
    addProductKeyword: { flow: 'newCampaign' },
    loading: {
      GET_SHOP_LIST: {
        status: false,
      },
      'ADD_PRODUCT/GET_SHOP_PRODUCTS': {
        status: false,
      },
    },
  };
}
