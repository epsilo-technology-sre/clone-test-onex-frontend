import { checkExistCampaignName } from '@ep/shopee/src/api/api';
import { Dialog, makeStyles, Typography } from '@material-ui/core';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { SetupCampaign } from '../../components/create-campaign/setup-campaign';
import { actions } from '../../redux/create-campaign/actions';
import { CreateCampaignState } from '../../redux/create-campaign/reducers';
import { AddKeywordContainer } from '../add-keyword-container';
import { AddProductContainer } from '../add-product-container';
import { ProductListContainer } from './product-list-container';

const useStyle = makeStyles({
  dialogPaper: {
    maxHeight: 'calc(100% - 32px)',
  },
});

export function CreateCampaignContainer() {
  let content = null;
  const classes = useStyle();
  let dispatch = useDispatch();
  let {
    currentScreen,
    setupCampaign,
    setupProduct,
    addKeyword,
  } = useSelector((state: CreateCampaignState) => {
    return {
      currentScreen: state.currentScreen,
      setupCampaign: state.setupCampaign,
      setupProduct: state.setupProduct,
      addKeyword: state.addKeyword,
    };
  });

  useEffect(() => {
    if (currentScreen === 'setupCampaign') {
      dispatch(actions.getShopList());
    }
  }, [currentScreen]);

  function onCheckCampaignNameExists(name: string, shopId: number) {
    return checkExistCampaignName({
      name: name,
      shop_eids: shopId,
    }).then((rs) => {
      return rs.success as boolean;
    });
  }

  if (currentScreen === 'setupCampaign') {
    content = (
      <React.Fragment>
        <Typography variant={'h6'}>Setup campaign info</Typography>
        <SetupCampaign
          shops={setupCampaign.shopList}
          campaignName={setupCampaign.campaignName}
          shop={setupCampaign.shop}
          budget={setupCampaign.budget}
          timeline={setupCampaign.timeline}
          shopCurrency={setupCampaign.shopCurrency}
          onSubmit={(info) =>
            dispatch(actions.setCampaignInfo({ info }))
          }
          checkCampaignNameExists={onCheckCampaignNameExists}
        />
      </React.Fragment>
    );
  } else if (currentScreen === 'setupProduct') {
    content = (
      <React.Fragment>
        <ProductListContainer
          backToPreviousScreen={() => {
            dispatch(
              actions.switchScreen({ screen: 'setupCampaign' }),
            );
          }}
        />
        <Dialog
          open={setupProduct.modalAddProductVisible}
          fullWidth
          maxWidth={'xl'}
          classes={{ paper: classes.dialogPaper }}
        >
          <AddProductContainer></AddProductContainer>
        </Dialog>
        <Dialog
          open={addKeyword.modalAddKeywordVisible}
          fullWidth
          maxWidth={'xl'}
          classes={{ paper: classes.dialogPaper }}
        >
          <AddKeywordContainer></AddKeywordContainer>
        </Dialog>
      </React.Fragment>
    );
  }

  return content;
}
