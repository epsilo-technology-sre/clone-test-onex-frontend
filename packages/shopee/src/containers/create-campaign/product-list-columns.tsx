import {
  InStockCell,
  PercentCell,
  ProductBudgetCell,
  ProductListActionCell,
  SubjectCell,
} from '@ep/shopee/src/components/common/table-cell';

export const initColumns = (props: any) => {
  const { onAddKeyword, onDelete } = props;

  return [
    {
      Header: 'Product',
      id: 'productName',
      accessor: (row: any) => ({
        name: row.productName,
        code: row.productId,
      }),
      sticky: 'left',
      width: 300,
      Cell: SubjectCell,
    },
    {
      Header: 'In stock',
      id: 'productStock',
      accessor: (row: any) => ({
        inStock: row.productStock,
        updatedDate: row.updatedAt,
      }),
      width: 200,
      Cell: InStockCell,
    },
    {
      Header: 'Keyword',
      id: 'totalKeyword',
      accessor: (row: any) => ({ number: row.totalKeyword }),
      Cell: PercentCell,
      disableSortBy: true,
    },
    {
      Header: 'Discount',
      id: 'discount',
      accessor: (row: any) => ({ number: row.discount, prefix: '%' }),
      Cell: PercentCell,
    },
    {
      Header: 'Ads Item Sold',
      id: 'itemSold',
      accessor: (row: any) => ({ number: row.itemSold }),
      Cell: PercentCell,
    },
    {
      Header: 'Price',
      id: 'pricePostsub',
      accessor: (row: any) => ({
        number: row.pricePostsub,
        currency: row.currency || 'VND',
      }),
      Cell: PercentCell,
    },
    {
      Header: 'Budget',
      id: 'budget',
      accessor: (row: any) => ({
        dailyBudget: row.dailyBudget,
        totalBudget: row.totalBudget,
        currency: row.currency,
      }),
      Cell: ProductBudgetCell,
      disableSortBy: true,
    },
    {
      Header: '',
      id: 'action',
      accessor: (row: any) => ({
        product: row,
        onAddKeyword,
        onDelete,
      }),
      width: 200,
      Cell: ProductListActionCell,
      disableSortBy: true,
    },
  ];
};
