import { COLORS } from '@ep/shopee/src/constants';
import NoProduct from '@ep/shopee/src/images/no-product.svg';
import {
  Box,
  Divider,
  Grid,
  InputAdornment,
  Snackbar,
  Typography,
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import SearchIcon from '@material-ui/icons/Search';
import Alert from '@material-ui/lab/Alert';
import { get, orderBy } from 'lodash';
import React, { useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { BulkActionsUI } from '../../components/common/bulk-actions';
import { ButtonUI } from '../../components/common/button';
import { MultiSelectUI } from '../../components/common/multi-select';
import { TableUI } from '../../components/common/table';
import { InputUI } from '../../components/create-campaign/common';
import { actions } from '../../redux/create-campaign/actions';
import { CreateCampaignState } from '../../redux/create-campaign/reducers';
import { initColumns } from './product-list-columns';

export const ProductListContainer = (props: {
  backToPreviousScreen: Function;
}) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { setupCampaign, setupProduct, loading } = useSelector(
    (state: CreateCampaignState) => {
      return {
        setupCampaign: state.setupCampaign,
        setupProduct: state.setupProduct,
        loading: state.loading,
      };
    },
  );

  const [searchText, setSearchText] = useState('');
  const [sortBy, setSortBy] = useState([]);
  const [submitting, setSubmitting] = useState(false);
  const [notification, setNotification] = useState<any>({
    open: false,
    status: false,
  });

  const [disableSubmit, setDisableSubmit] = useState(false);

  useEffect(() => {
    dispatch(
      actions.getCategoryList({ shopEid: setupCampaign.shop }),
    );
  }, []);

  useEffect(() => {
    if (loading.createCampaign) {
      setSubmitting(false);
      setNotification({
        ...notification,
        ...loading.createCampaign,
        open: true,
      });
      if (loading.createCampaign.status) {
        setTimeout(function () {
          history.push('/advertising/shopee/campaign');
        }, 3000);
      }
    }
  }, [loading.createCampaign]);

  useEffect(() => {
    if (
      setupProduct.campaignProductList.items &&
      setupProduct.campaignProductList.items.length > 0
    ) {
      setDisableSubmit(
        setupProduct.campaignProductList.items.some(
          (item: any) => item.totalKeyword === 0,
        ),
      );
    } else {
      setDisableSubmit(true);
    }
  }, [setupProduct.campaignProductList]);

  useEffect(() => {
    if (
      !setupProduct.modalAddProductVisible &&
      get(setupProduct, 'campaignProductList.items.length', 0) > 0
    ) {
      setNotification({
        ...notification,
        status: true,
        error: 'Products have been added successfully.',
        open: true,
      });
    }
  }, [setupProduct.campaignProductList.items]);

  const headers = React.useMemo(
    () =>
      initColumns({
        onAddKeyword: (value: any) => {
          openModalAddKeywords([value.productId]);
        },
        onDelete: (value: any) => {
          handleRemoveProducts([value.productId]);
        },
      }),
    [],
  );

  const rows = useMemo(() => {
    const {
      items,
      page,
      limit,
      searchText: search,
      selectedCategories,
    } = setupProduct.campaignProductList;
    let filteredItems = items;

    if (search) {
      const lowerCaseSearch = search.toLowerCase();
      filteredItems = items.filter((item) =>
        item.productName.toLowerCase().includes(lowerCaseSearch),
      );
    }

    if (selectedCategories && selectedCategories.length > 0) {
      const categoryNames = selectedCategories.map((i) => i.id);
      filteredItems = filteredItems.filter((item) =>
        categoryNames.includes(item.categoryName),
      );
    }

    if (sortBy.length > 0) {
      console.log('Sort by', sortBy);
      const field = sortBy[0].id;
      const direction = sortBy[0].desc ? 'desc' : 'asc';
      filteredItems = orderBy(filteredItems, [field], [direction]);
    }

    return filteredItems.slice((page - 1) * limit, page * limit);
  }, [setupProduct.campaignProductList, sortBy]);

  const handleChangeSearchText = (event: any) => {
    setSearchText(event.target.value);
  };

  const handleSearchKeyUp = (event: any) => {
    if (event.key === 'Enter') {
      dispatch(
        actions.updateProductListingSearch({
          searchText: event.target.value,
        }),
      );
    }
  };

  const handleSorting = (sortBy: any) => {
    setSortBy(sortBy);
  };

  const handleChangePage = (page: number) => {
    dispatch(
      actions.updateCampaignProductListPagination({
        page,
        limit: setupProduct.campaignProductList.limit,
      }),
    );
  };

  const handleChangePageSize = (value: number) => {
    dispatch(
      actions.updateCampaignProductListPagination({
        page: 1,
        limit: value,
      }),
    );
  };

  const handleSelectAll = React.useCallback((checked) => {
    dispatch(
      actions.campaignProductListSelectAll({ isAdd: checked }),
    );
  }, []);

  const handleSelectItem = React.useMemo(() => {
    return (item: any, checked: boolean) => {
      dispatch(
        actions.campaignProductListSelectItem({
          productIds: Number(getRowId(item)),
          selected: checked,
        }),
      );
    };
  }, []);

  const getRowId = React.useCallback((row) => {
    return String(row.productId);
  }, []);

  const openAddProductDialog = () => {
    dispatch(actions.openAddProductModal({ visible: true }));
  };

  const handleCreateCampaign = () => {
    const products = setupProduct.campaignProductList.items.map(
      (item: any) => {
        return {
          product_eid: item.productId,
          value_total: item.totalBudget,
          value_daily: item.dailyBudget,
        };
      },
    );
    let keywords = [];
    setupProduct.productKeywords.forEach((productKw: any) => {
      keywords = keywords.concat(
        productKw.keywords.map((item: any) => {
          return {
            name: item.keywordName,
            bidding_price: item.biddingPrice,
            match_type:
              item.matchType === 'Broad Match' ||
              item.matchType === 'broad_match'
                ? 'broad_match'
                : 'exact_match',
            product_eid: productKw.product.productId,
          };
        }),
      );
    });
    const productIds = products.map((item: any) => item.product_eid);
    const totalBudget = setupCampaign.budget.isNoLimit
      ? 0
      : setupCampaign.budget.total;
    const dailyBudget = setupCampaign.budget.isOnDaily
      ? setupCampaign.budget.daily
      : 0;
    const endDate = setupCampaign.timeline.isNoLimit
      ? 'no_limit'
      : setupCampaign.timeline.endDate;
    const params = {
      campaign_name: setupCampaign.campaignName,
      campaign_time_line_from: setupCampaign.timeline.startDate,
      campaign_time_line_to: endDate,
      campaign_daily_budget: dailyBudget,
      campaign_total_budget: totalBudget,
      shop_eids: [setupCampaign.shop],
      product_eids: productIds,
      products,
      keywords,
    };
    dispatch(actions.createCampaign({ params }));
    setSubmitting(true);
  };

  const openModalAddKeywords = (productIds: any) => {
    dispatch(actions.setProductsForAddKeywordById({ productIds }));
    dispatch(actions.openAddKeywordModal({ visible: true }));
  };

  const handleRemoveProducts = (productIds: any) => {
    dispatch(
      actions.removeCampaignProduct({
        productIds: productIds,
      }),
    );
  };

  const handleChangeSelectedCategories = (categories: any) => {
    dispatch(
      actions.updateProductListingSelectedCategory({
        categories,
      }),
    );
  };

  const buttons = [
    <ButtonUI
      onClick={() =>
        openModalAddKeywords(
          setupProduct.campaignProductList.selectedIds,
        )
      }
      label="Add keyword"
      size="small"
      colorButton="#F6F7F8"
    />,
    <ButtonUI
      colortext="#D4290D"
      onClick={() =>
        handleRemoveProducts(
          setupProduct.campaignProductList.selectedIds,
        )
      }
      label="Delete"
      size="small"
      colorButton="#F6F7F8"
    />,
  ];

  const handleCloseNotification = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setNotification({ ...notification, open: false });
  };

  return (
    <Box p={2} style={{ background: '#ffffff' }}>
      <Typography variant="h6">
        Setup products and keywords
      </Typography>
      <Box pt={3}>
        <Grid container justify="space-between" alignItems="center">
          <Grid item>
            <Grid container>
              <Grid item>
                <InputUI
                  id="campaign-searchbox"
                  placeholder="Search"
                  value={searchText}
                  onChange={handleChangeSearchText}
                  onKeyUp={handleSearchKeyUp}
                  startAdornment={
                    <InputAdornment position="start">
                      <SearchIcon fontSize="small" />
                    </InputAdornment>
                  }
                  style={{ width: 320 }}
                  labelWidth={0}
                  autoComplete="off"
                ></InputUI>
              </Grid>
              <Grid item>
                <MultiSelectUI
                  prefix="Category"
                  suffix="categories"
                  items={
                    setupProduct.campaignProductList.categories || []
                  }
                  selectedItems={
                    setupProduct.campaignProductList
                      .selectedCategories || []
                  }
                  onSaveChange={handleChangeSelectedCategories}
                />
              </Grid>
            </Grid>
          </Grid>
          <Grid item>
            {setupProduct.campaignProductList.items.length > 0 && (
              <ButtonUI
                size="small"
                label="Add products"
                colorButton={COLORS.COMMON.GRAY}
                iconLeft={<AddIcon />}
                onClick={openAddProductDialog}
              ></ButtonUI>
            )}
          </Grid>
        </Grid>
      </Box>
      <Box py={1}>
        {setupProduct.campaignProductList.selectedIds.length > 0 && (
          <BulkActionsUI
            textSelected={`${setupProduct.campaignProductList.selectedIds.length} products selected`}
            buttons={buttons}
          />
        )}
      </Box>
      <Box py={1}>
        <Grid container>
          <Grid item xs={12}>
            <TableUI
              columns={headers}
              rows={rows}
              onSelectItem={handleSelectItem}
              onSelectAll={handleSelectAll}
              selectedIds={
                setupProduct.campaignProductList.selectedIds
              }
              getRowId={getRowId}
              onSort={handleSorting}
              resultTotal={
                setupProduct.campaignProductList.items.length
              }
              page={setupProduct.campaignProductList.page}
              pageSize={setupProduct.campaignProductList.limit}
              onChangePage={handleChangePage}
              onChangePageSize={handleChangePageSize}
              noData={
                <Box py={6}>
                  <img src={NoProduct} width="200" height="200" />
                  <Typography variant="h6" style={{ margin: 'auto' }}>
                    Add your amazing products
                  </Typography>
                  <Typography
                    variant="subtitle2"
                    style={{ margin: '16px auto' }}
                  >
                    Here’s where you would add product to your
                    campaign.
                  </Typography>
                  <ButtonUI
                    size="small"
                    label="Add products"
                    colorButton={COLORS.COMMON.GRAY}
                    onClick={openAddProductDialog}
                  ></ButtonUI>
                </Box>
              }
            />
          </Grid>
        </Grid>
      </Box>
      <Divider />
      <Box py={2}>
        <Grid
          container
          alignItems="center"
          justify="space-between"
          spacing={1}
        >
          <Grid item>
            <ButtonUI
              size="small"
              label="Back to previous step"
              colorButton={COLORS.COMMON.GRAY}
              onClick={() => props.backToPreviousScreen()}
            ></ButtonUI>
          </Grid>
          <Grid item>
            <ButtonUI
              loading={submitting}
              disabled={disableSubmit}
              size="small"
              label="Create campaign"
              onClick={handleCreateCampaign}
            ></ButtonUI>
          </Grid>
        </Grid>
      </Box>
      {notification.open && (
        <Snackbar
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          open={notification.open}
          onClose={handleCloseNotification}
          autoHideDuration={3000}
        >
          <Alert
            onClose={handleCloseNotification}
            elevation={6}
            variant="filled"
            severity={notification.status ? 'success' : 'error'}
          >
            {notification.error}
          </Alert>
        </Snackbar>
      )}
    </Box>
  );
};
