import { Box, Snackbar } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import { get, union } from 'lodash';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useState,
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import { downloadShopAdsFile } from '../../api/api';
import { ModalAddRuleName } from '../../components/common/modal-add-rule-name';
import { ModalBulkActionShop } from '../../components/common/modal-bulk-actions/modal-bulk-action-shops';
import { ModalConfirmUI } from '../../components/common/modal-confirm';
import { ShopAds } from '../../components/shop-ads';
import { ShopAdsContext } from '../../components/shop-ads/shop-ads-context';
import { SHOP_ADS_STATUS, TABS } from '../../constants';
import { actions } from '../../redux/shop-ads/actions';
import { ShopAdState } from '../../redux/shop-ads/reducers';
import { AddRuleShopAdContainer } from '../rules/add-rule-shop-ad-container';
import { RuleShopAdCreate } from '../rules/shop-ads-create';
import { ListRuleShopAd } from '../rules/shop-ads-list';
import { ShopAdsRuleLog } from '../rules/shop-ads-rule-log';
import { SHOP_ADS_COLUMNS } from './shop-ads-columns';
import { useTableItemSelect } from './use-table-item-select';

export function ShopAdsContainer(props: any) {
  const { globalFilters, selectedShops = [] } = props;

  const [showActivator, setShowActivator] = useState<any>({
    show: false,
    focusShop: null,
  });

  const [openModalBulkAction, setOpenModalBulkAction] = useState<any>(
    {
      open: false,
      items: [],
    },
  );

  const [notification, setNotification] = useState({
    open: false,
  });

  const [addModalType, setAddModalType] = React.useState('');

  const [ruleProductCreate, setRuleProductCreate] = React.useState({
    isOpen: false,
    products: [],
    shopId: null,
  });

  const [listProductRules, setListProductRules] = React.useState({
    isOpen: false,
    allowUpdate: true,
    shopEid: 0,
    productId: 0,
    productName: '',
  });

  const [openModalRule, setOpenModalRule] = useState(false);
  const [newRule, setNewRules] = useState<any>({
    open: false,
  });

  const [ruleLog, setRuleLog] = React.useState({
    isOpen: false,
    shopEid: 0,
    parentName: '',
    itemId: 0,
    itemName: '',
    currency: '',
  });

  const [deleteRulesModal, setDeleteRulesModal] = React.useState({
    open: false,
    items: [],
  });

  const {
    selectedIds,
    onSelectAll,
    onSelectItem,
  } = useTableItemSelect({
    stateSelector: (state: ShopAdState) => {
      return state.shopAds;
    },
    selectAction: (items, checked) =>
      actions.onSelectShopAds({ items, checked }),
  });

  const statusList = useMemo(() => {
    const items = ['All'];
    for (const property in SHOP_ADS_STATUS) {
      items.push(SHOP_ADS_STATUS[property]);
    }
    return items;
  }, []);

  const dispatch = useDispatch();

  useEffect(() => {
    if (globalFilters && globalFilters.shops?.length > 0) {
      dispatch(actions.getShopAds({ globalFilters }));
    }
  }, [globalFilters.timeRange, globalFilters.updateCount]);

  const pagination = useSelector((state: ShopAdState) => {
    return state.pagination;
  });

  let shopAds = useSelector((state: ShopAdState) => {
    return get(state, 'shopAds.items', []);
  });

  let shopAdsStatus = useSelector((state: ShopAdState) => {
    return state.localFilter.status;
  });

  const handleUpdateSelectedShopsForAddRule = (items: any) => {
    const itemIds = items.map((i) => i.shopAdsId);
    handleUpdateSelectedShops(items);
    setRuleProductCreate({
      ...ruleProductCreate,
      products: itemIds.map((i) => {
        const shopAd = shopAds.find((i1) => i1.shopAdsId === i);
        return {
          ...shopAd,
          productId: shopAd.shopAdsId,
          product_id: shopAd.shopAdsId,
          product_name: shopAd.shopAdsName,
        };
      }),
    });
  };

  const handleUpdateSelectedShops = (items: any) => {
    console.log(
      'handleUpdateSelectedShops',
      items,
      shopAds,
      selectedIds,
    );
    dispatch(actions.updateSelectedShopAds({ items }));
  };

  const updatePagination = useCallback(
    (paging: any) => {
      dispatch(
        actions.getShopAds({ globalFilters, pagination: paging }),
      );
    },
    [globalFilters],
  );

  const getRowId = useCallback((row) => {
    return String(row.shopAdsId);
  }, []);

  const onClickSubjectLabel = (shop: any) => {
    dispatch(
      actions.onSelectShopAds({ items: [shop], checked: true }),
    );
    dispatch(actions.changeTab({ tab: TABS.SHOPADS_KEYWORD }));
  };

  const getShopData = React.useCallback(() => {
    dispatch(actions.refreshShopAds());
  }, []);

  const handleViewRuleLog = (row) => {
    setRuleLog({
      isOpen: true,
      shopEid: row.shop_eid,
      parentName: row.shopName,
      itemId: row.shopAdsId,
      itemName: row.shopAdsName,
      currency: row.currency,
    });
  };

  const handleChangeSearchText = (value: any) => {
    dispatch(actions.updateLocalFilter({ searchText: value }));
    updatePagination({
      page: 1,
    });
  };

  const handleChangeStatus = (value: any) => {
    dispatch(
      actions.updateLocalFilter({
        status: value,
      }),
    );
    updatePagination({
      page: 1,
    });
  };

  const handleSorting = (sortBy: any) => {
    if (sortBy.length > 0) {
      dispatch(actions.sortShopAds({ sortBy }));
    }
  };

  const handleChangePage = (page: number) => {
    updatePagination({
      page: page,
    });
  };

  const handleChangePageSize = (value: number) => {
    updatePagination({
      page: 1,
      limit: value,
    });
  };

  const handleActivator = (type: string, shops: any) => {
    const selectedItems = shopAds.filter((i) =>
      shops.includes(i.shopAdsId),
    );
    const isAllowed = selectedItems.every((i) => !i._isDisabled);
    if (isAllowed) {
      const activator: any = {
        active: {
          labelSubmit: 'Activate',
          title: 'Activate Shop Ads',
          content:
            'Your Shop Ads will be reactivated in this channel.',
          status: 1,
        },
        deactive: {
          labelSubmit: 'Deactivate',
          title: 'Deactivate Shop Ads?',
          content:
            'We will deactivate your Shop Ads on this channel. You can still re-enable your Shop Ads in the future.',
          status: 0,
        },
      };
      setShowActivator({
        ...showActivator,
        ...activator[type],
        focusShop: shops,
        type,
        show: true,
      });
    } else {
      toast.error('Can not modify ended shop ads');
    }
  };

  const showStatusPopup = (shop: any, status: boolean) => {
    handleActivator(
      status ? 'deactive' : 'active',
      [].concat(shop.shopAdsId),
    );
  };

  const updateShop = React.useCallback(
    async (field: string, value: any) => {
      const updateFn: any = {
        name: (tagline: any) => {
          return new Promise((resolve, reject) => {
            dispatch(
              actions.updateShopTagline({
                params: { tagline },
                promise: { resolve, reject },
              }),
            );
          });
        },
        budget: ({ row, value }) => {
          return new Promise((resolve, reject) => {
            dispatch(
              actions.updateShopAdBudget({
                params: { value, row },
                promise: { resolve, reject },
              }),
            );
          });
        },
        timeline: ({ row, value }) => {
          return new Promise((resolve, reject) => {
            dispatch(
              actions.updateShopAdTimeline({
                params: { value, row },
                promise: { resolve, reject },
              }),
            );
          });
        },
      };

      try {
        const response = await updateFn[field](value);
        const result = response.success;
        if (response.success) {
          toast.success(response.message);
          getShopData();
        } else {
          toast.error(response.message);
          return false;
        }
        return result;
      } catch (e) {
        if (
          !e.message ||
          e.message.length === 0 ||
          !String([].concat(e.message).join('')).trim()
        ) {
          toast.error(
            'An unexpected error occurred. Please try again in a few moments',
          );
        } else {
          toast.error([].concat(e.message).join('\n'));
        }
        return false;
      }
    },
    [],
  );

  const handleSubmit = async (selectedIds: any[]) => {
    const selectedShopAds = selectedIds.map((i) =>
      shopAds.find((i1) => i1.shopAdsId === i),
    );

    setShowActivator({ ...showActivator, show: false });
    try {
      const res = await new Promise((resolve, reject) => {
        dispatch(
          actions.updateAdsActivation({
            params: {
              value: {
                activation: {
                  status: showActivator.status,
                },
              },
              rows: selectedShopAds,
            },
            promise: { resolve, reject },
          }),
        );
      });

      setNotification({ ...notification, ...res, open: true });
    } catch (error) {
      notification.message = error.message;
      setNotification({
        ...notification,
        open: true,
        success: false,
      });
    }

    getShopData();
  };

  const handleSubmitModifyShopAdss = async ({
    total_budget,
    daily_budget,
  }: any) => {
    const selectedShopAds = selectedIds.map((i) =>
      shopAds.find((i1) => i1.shopAdsId === i),
    );

    try {
      const res = await new Promise((resolve, reject) => {
        dispatch(
          actions.updateShopAdBudget({
            params: {
              value: {
                budget: {
                  total: total_budget,
                  daily: daily_budget,
                  isNoLimit: !total_budget,
                  isOnDaily: !!daily_budget,
                },
              },
              row: selectedShopAds,
            },
            promise: { resolve, reject },
          }),
        );
      });

      setNotification({ ...notification, ...res, open: true });
    } catch (error) {
      notification.message = error.message;
      setNotification({
        ...notification,
        open: true,
        success: false,
      });
    }
    setOpenModalBulkAction({ ...openModalBulkAction, open: false });
    getShopData();
  };

  const handleOpenModifyBudget = (shopAdsIds: any) => {
    const allowItems = shopAds.filter(
      (i) => !i._isDisabled && selectedIds.includes(i.shopAdsId),
    );
    if (allowItems.length > 0) {
      const isDiffCurrency = allowItems.some(
        (i) => i.currency !== allowItems[0].currency,
      );
      if (isDiffCurrency) {
        toast.error('Selected shops have the difference currency');
      } else {
        setOpenModalBulkAction({ open: true, items: allowItems });
      }
    } else {
      toast.error('Can not modify ended shop ads');
    }
  };

  const handleCloseNotification = (event: any, reason: any) => {
    if (reason === 'clickaway') {
      return;
    }
    setNotification({ ...notification, open: false });
  };

  const handleViewRule = (row) => {
    const allowStatus = [
      SHOP_ADS_STATUS.RUNNING,
      SHOP_ADS_STATUS.PAUSED,
      SHOP_ADS_STATUS.SCHEDULED,
    ];
    setListProductRules({
      isOpen: true,
      allowUpdate: allowStatus.includes(row.shopAdsStatus),
      shopEid: row.shopEid,
      productId: row.shopAdsId,
      productName: row.shopAdsName,
    });
  };

  const handleAddNewRule = (shopAds: any) => {
    setRuleProductCreate({
      ...ruleProductCreate,
      shopId: shopAds.shopEid,
      products: [
        {
          ...shopAds,
          productId: shopAds.shopAdsId,
          product_id: shopAds.shopAdsId,
          product_eid: shopAds.shopAdsId,
          product_name: shopAds.shopAdsName,
        },
      ],
    });
    setOpenModalRule(true);
  };

  const handleAddRuleSuccess = () => {
    setRuleProductCreate((state) => ({
      ...state,
      isOpen: false,
    }));
    getShopData();
    getRuleList();
  };

  const handleCloseAddRule = () => {
    setRuleProductCreate((state) => ({
      ...state,
      isOpen: false,
    }));
  };

  const handleOpenModalAddRule = () => {
    const allowItems = shopAds.filter(
      (i) => !i._isDisabled && selectedIds.includes(i.shopAdsId),
    );
    if (allowItems.length > 0) {
      let shopEid = allowItems.map((p) => p.shopEid);
      shopEid = [...new Set(shopEid)];

      if (shopEid.length > 1) {
        setNotification({
          open: true,
          success: false,
          message: 'Please choose the shop ads in one shop',
        });
      } else {
        setRuleProductCreate({
          ...ruleProductCreate,
          products: allowItems.map((i) => ({
            ...i,
            productId: i.shopAdsId,
            product_id: i.shopAdsId,
            product_name: i.shopAdsName,
          })),
          shopId: shopEid[0],
        });
        setOpenModalRule(true);
      }
    } else {
      toast.error('Can not add rule for ended shop ads');
    }
  };

  const getRuleList = () => {
    if (listProductRules.isOpen) {
      dispatch(
        actions.getProductRules({
          productId: listProductRules.productId,
          shopEid: listProductRules.shopEid,
        }),
      );
    }
  };

  const onSubmitRule = (rule: any) => {
    const conditionJson = JSON.parse(rule.conditionJson);
    const changeDate =
      rule.fromDate === rule.timelineFrom &&
      rule.toDate === rule.timelineTo;
    const condition = rule.conditions.map(
      ({ value, period, metric_code, operator_code }) => ({
        value,
        period,
        metric_code,
        operator_code,
      }),
    );
    const changeCondition =
      JSON.stringify(conditionJson) === JSON.stringify(condition);
    if (changeCondition && changeDate) {
      dispatch(
        actions.addExistingRule({
          shopEid: rule.shopEid,
          featureCode: 'M_SHP_SA',
          objectType: 'shop_ads',
          objectEids: rule.objectEids,
          ruleId: rule.ruleId,
          callback: (rs) => {
            setNotification({
              open: true,
              message: rs.data,
              success: rs.success,
            });
            if (rs.success) {
              getShopData();
              getRuleList();
            }
          },
        }),
      );
    } else {
      setNewRules({
        open: true,
        rule: {
          ...rule,
          ruleName: rule.ruleName,
          listRules: rule.conditions,
        },
      });
    }
    setOpenModalRule(false);
  };

  const createNewRule = (rule: any) => {
    const postRule = {
      ...newRule.rule,
      ruleName: rule.ruleName,
    };

    const products = ruleProductCreate.products.map((p) => ({
      ...p,
      product_id: p.productId || p.product_id,
    }));

    dispatch(
      actions.createProductRule({
        rule: postRule,
        products,
        shopId: postRule.shopEid,
        callback: (rs) => {
          setNotification({
            open: true,
            message: rs.data,
            success: rs.success,
          });
          if (rs.success) {
            getShopData();
          }
        },
      }),
    );

    setNewRules({
      ...newRule,
      open: false,
    });
  };

  const handleDeleteRules = () => {
    const selectedShopAds = shopAds.filter((i) =>
      selectedIds.includes(i.shopAdsId),
    );
    setDeleteRulesModal({
      open: true,
      items: selectedShopAds,
    });
  };

  const onSubmitDeleteRules = () => {
    setDeleteRulesModal({ ...deleteRulesModal, open: false });
    dispatch(
      actions.deleteAllProductRules({
        featureCode: 'M_SHP_SA',
        shopEids: union(
          deleteRulesModal.items.map((i) => i.shop_eid),
        ),
        itemIds: deleteRulesModal.items.map((i) => i.shopAdsId),
        callback: (rs) => {
          setNotification({
            open: true,
            message: rs.data,
            success: rs.success,
          });
          if (rs.success) {
            getShopData();
          }
        },
      }),
    );
  };

  const handleDownloadFile = (selectedShopAdIds) => {
    const shopIds = shopAds
      .filter((i) => selectedShopAdIds.includes(i.shopAdsId))
      .map((i) => i.shopEid);
    return downloadShopAdsFile({
      shop_eids: shopIds,
      shop_ads_eids: selectedShopAdIds,
      from: globalFilters.timeRange.start,
      to: globalFilters.timeRange.end,
      limit: 10000,
    });
  };

  const handleGetExportFileName = (selectedShopAdIds) => {
    return 'epsilo_shopads.xlsx';
  };

  const headers = useMemo(() => {
    const columns: any = [
      ...SHOP_ADS_COLUMNS({
        onInlineSubmitBudget: (row, value) => {
          console.info('on inline submit budget', { row, value });
          return updateShop('budget', { row, value });
        },
        onInlineSubmitTagline: () => {},
        onInlineSubmitTimeline: (row, value) => {
          return updateShop('timeline', { row, value });
        },
      }),
    ];
    columns[0].onCellClick = showStatusPopup;
    columns[1].onClickLabel = (row: any) => {
      onClickSubjectLabel(row);
    };

    const ruleCol = columns.find((i) => i.id === 'rule');
    ruleCol.onCellClick = (row: any) => {
      if (row.ruleCount) {
        handleViewRule(row);
      } else {
        handleAddNewRule(row);
      }
    };

    const logCol = columns.find((i) => i.id === 'ruleLog');
    logCol.onCellClick = (row: any) => {
      handleViewRuleLog(row);
    };

    return columns;
  }, []);

  const context = {
    updateAdCreative: async (value: any) => {
      return updateShop('name', value);
    },
    getCategoryList: (shop) => {
      return new Promise((resolve, reject) => {
        dispatch(
          actions.getShopCategories({
            shopEid: shop.shopEid,
            promise: { resolve, reject },
          }),
        );
      });
    },
  };

  return (
    <Box>
      <ShopAdsContext.Provider value={context}>
        <ShopAds
          tableHeaders={headers}
          shops={shopAds}
          pagination={pagination}
          statusList={statusList}
          selectedStatus={shopAdsStatus}
          selectedShops={selectedShops}
          updateSelectedShops={handleUpdateSelectedShops}
          handleSelectItem={onSelectItem}
          handleSelectAll={onSelectAll}
          getRowId={getRowId}
          selectedIds={selectedIds}
          onSearch={handleChangeSearchText}
          onChangeStatus={handleChangeStatus}
          onSorting={handleSorting}
          onChangePage={handleChangePage}
          onChangePageSize={handleChangePageSize}
          handleActivator={handleActivator}
          handleOpenModifyBudget={handleOpenModifyBudget}
          openModalAddRule={handleOpenModalAddRule}
          handleDeleteRules={handleDeleteRules}
          handleDownloadFile={handleDownloadFile}
          handleGetExportFileName={handleGetExportFileName}
        />
      </ShopAdsContext.Provider>
      {showActivator.show && (
        <ModalConfirmUI
          open={showActivator.show}
          onSubmit={() =>
            handleSubmit([].concat(showActivator.focusShop))
          }
          onClose={() =>
            setShowActivator({ ...showActivator, show: false })
          }
          labelSubmit={showActivator.labelSubmit}
          title={showActivator.title}
          content={showActivator.content}
        />
      )}
      {deleteRulesModal.open && (
        <ModalConfirmUI
          open={deleteRulesModal.open}
          onSubmit={onSubmitDeleteRules}
          onClose={() =>
            setDeleteRulesModal({ ...deleteRulesModal, open: false })
          }
          labelSubmit="OK"
          title="Delete all rules?"
          content="All rules that you added on these shop ads will be deleted."
        />
      )}
      {openModalBulkAction.open && (
        <ModalBulkActionShop
          open={openModalBulkAction.open}
          setOpenModalBulkAction={(e: any) =>
            handleSubmitModifyShopAdss(e)
          }
          onClose={() =>
            setOpenModalBulkAction({
              ...openModalBulkAction,
              open: false,
            })
          }
          shopAds={openModalBulkAction.items}
          updateSelected={(items) => {
            handleUpdateSelectedShops(items);
            setOpenModalBulkAction({
              ...openModalBulkAction,
              items,
            });
          }}
        />
      )}
      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={notification.open}
        onClose={handleCloseNotification}
        autoHideDuration={3000}
      >
        <Alert
          onClose={handleCloseNotification}
          elevation={6}
          variant="filled"
          severity={notification.success ? 'success' : 'error'}
        >
          {notification.message}
        </Alert>
      </Snackbar>
      <RuleShopAdCreate
        isOpen={ruleProductCreate.isOpen}
        shopId={ruleProductCreate.shopId}
        products={ruleProductCreate.products}
        onSubmitSuccess={handleAddRuleSuccess}
        onClose={handleCloseAddRule}
        onOpenExistingRule={() => {
          setOpenModalRule(true);
          setRuleProductCreate({
            ...ruleProductCreate,
            isOpen: false,
          });
        }}
      />
      <AddRuleShopAdContainer
        onSubmit={onSubmitRule}
        open={openModalRule}
        onClose={(e: any) => setOpenModalRule(e)}
        handleCreate={() => {
          setOpenModalRule(false);
          setRuleProductCreate({
            ...ruleProductCreate,
            isOpen: true,
          });
        }}
        products={ruleProductCreate.products}
        setProduct={handleUpdateSelectedShopsForAddRule}
        shopEid={ruleProductCreate.shopId}
      />
      <ModalAddRuleName
        open={newRule.open}
        onSubmit={createNewRule}
        onClose={(e: any) => setNewRules({ ...newRule, open: e })}
      />
      <ListRuleShopAd
        onAddRule={(keyword) => handleAddNewRule(keyword)}
        allowUpdate={listProductRules.allowUpdate}
        isOpen={listProductRules.isOpen}
        shopEid={listProductRules.shopEid}
        productId={listProductRules.productId}
        productName={listProductRules.productName}
        onUpdateSuccess={getShopData}
        onClose={() => {
          setListProductRules((state) => ({
            ...state,
            isOpen: false,
          }));
        }}
      />
      <ShopAdsRuleLog
        open={ruleLog.isOpen}
        shopEid={ruleLog.shopEid}
        parentName={ruleLog.parentName}
        itemId={ruleLog.itemId}
        itemName={ruleLog.itemName}
        onClose={() => {
          setRuleLog((state) => ({
            ...state,
            isOpen: false,
          }));
        }}
      ></ShopAdsRuleLog>
    </Box>
  );
}
