import { useSelector, useDispatch } from 'react-redux';
import { get } from 'lodash';
import React from 'react';

export function useTableItemSelect({
  stateSelector,
  selectAction,
}: {
  stateSelector: (arg: any) => { items: any[]; selectedIds: any[] };
  selectAction: (
    items: any[],
    status: boolean,
  ) => { type: string; payload?: any };
}) {
  const dispatch = useDispatch();
  const state = useSelector((state) => {
    const { items, selectedIds } = stateSelector(state);
    return { items, selectedIds };
  });

  let onSelectItem = React.useCallback((item, checked) => {
    dispatch(selectAction([item], checked));
  }, []);

  let onSelectAll = React.useCallback((checked: boolean) => {
    dispatch(selectAction(null, checked));
  }, []);

  return {
    selectedIds: state.selectedIds,
    onSelectItem: onSelectItem,
    onSelectAll: onSelectAll,
  };
}
