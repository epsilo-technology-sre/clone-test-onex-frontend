import {
  getShopAdsSuggestBiddingPrice,
  updateShopAdsKeywordPrice,
  updateShopAdsKeywordStatus,
} from '@ep/shopee/src/api/api';
import { Snackbar } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import { differenceWith, get, isEqual, union } from 'lodash';
import React, { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import { ModalAddRuleName } from '../../components/common/modal-add-rule-name';
import { ModalBulkActionKeyword } from '../../components/common/modal-bulk-actions/modal-bulk-action-keywords';
import { ModalConfirmUI } from '../../components/common/modal-confirm';
import { KeywordView } from '../../components/keywords';
import { SHOP_ADS_KEYWORDS_STATUS } from '../../constants';
import { actions } from '../../redux/shop-ads/actions';
import { ShopAdState } from '../../redux/shop-ads/reducers';
import { AddRuleShopAdKeywordContainer } from '../rules/add-rule-shop-ad-keyword-container';
import { CreateRuleShopAdKeyword } from '../rules/shop-ads-keyword-create';
import { ListRuleShopAdKeyword } from '../rules/shop-ads-keyword-list';
import { ShopAdsKeywordRuleLog } from '../rules/shop-ads-keyword-rule-log';
import { SHOP_ADS_KEYWORD_COLUMNS } from './keyword-shop-ads-columns';
import { ContainerShopAdsAddKeyword } from './shop-ads-add-keywords';
import { useTableItemSelect } from './use-table-item-select';

export const KeywordShopAdsContainer = () => {
  const ref = React.useRef({
    selectedKeywords: null,
    selectedShopAdsIds: [],
    keywords: null,
    pagination: null,
    sortBy: [],
  });
  const {
    keywords,
    pagination,
    globalFilters,
    selectedKeywords,
    loading,
    searchKeyword,
    filterKeywordStatus,
  } = useSelector((state: ShopAdState) => {
    const keywordList = get(state, 'keywords.items', []);
    const selectedIds = get(state, 'keywords.selectedIds', []);
    const selectedShopAdsIds = get(state, 'shopAds.selectedIds', []);
    const selectedKeywords = keywordList.filter((i) =>
      selectedIds.includes(i.keywordId),
    );
    const pagination = {
      page_count: state.pagination.pageCount,
      limit: state.pagination.limit,
      page: state.pagination.page,
      item_count: state.pagination.itemCount,
    };
    ref.current = {
      ...ref.current,
      selectedKeywords,
      selectedShopAdsIds: selectedShopAdsIds,
      keywords: keywordList,
      pagination,
    };
    return {
      pagination,
      globalFilters: state.globalFilters,
      keywords: keywordList,
      selectedKeywords,
      selectedShopAdsIds,
      loading: state.loading,
      searchKeyword: state.localFilter?.searchText,
      filterKeywordStatus: state.localFilter?.status,
    };
  });

  const [addKeywordModal, setAddKeywordModal] = React.useState({
    isOpen: false,
    shopId: null,
  });

  const [deleteRulesModal, setDeleteRulesModal] = React.useState({
    open: false,
    items: [],
  });

  const [notification, setNotification] = React.useState({
    open: false,
  });
  const [showActivator, setShowActivator] = React.useState({
    show: false,
    focusKeyword: null,
  });
  const [
    openModalBulkAction,
    setOpenModalBulkAction,
  ] = React.useState({
    open: false,
    items: [],
    currency: '',
  });

  const [createRuleKeyword, setCreateRuleKeyword] = React.useState({
    isOpen: false,
    keywords: [],
    products: [],
    shopId: null,
    currency: 'VND',
  });

  const [listKeywordRules, setListKeywordRules] = React.useState({
    isOpen: false,
    allowUpdate: true,
    shopEid: 0,
    keywordId: 0,
    keywordName: '',
    productId: 0,
    productName: '',
    currency: '',
    matchType: '',
  });

  const statusList = useMemo(() => {
    const items = ['All'];
    for (const property in SHOP_ADS_KEYWORDS_STATUS) {
      items.push(SHOP_ADS_KEYWORDS_STATUS[property]);
    }
    return items;
  }, []);

  useEffect(() => {
    if (globalFilters && globalFilters.shops?.length > 0) {
      getKeywordData({ page: 1 });
    }
  }, [globalFilters.timeRange, globalFilters.updateCount]);

  const dispatch = useDispatch();

  const updateSearchKeyword = (query: any) =>
    dispatch(actions.updateLocalFilter({ searchText: query }));

  const updateFilterKeywordStatus = (status: any) =>
    dispatch(actions.updateLocalFilter({ status }));

  const [openModalRule, setOpenModalRule] = React.useState(false);

  const [newRule, setNewRules] = React.useState<any>({
    open: false,
  });

  const [ruleLog, setRuleLog] = React.useState({
    isOpen: false,
    shopEid: 0,
    parentName: '',
    itemId: 0,
    itemName: '',
    currency: '',
  });

  const getRowId = React.useCallback((row) => {
    return String(row.keywordId);
  }, []);

  const {
    selectedIds,
    onSelectAll,
    onSelectItem,
  } = useTableItemSelect({
    stateSelector: (state: ShopAdState) => {
      return state.keywords;
    },
    selectAction: (items, checked) =>
      actions.onSelectShopAdsKeyword({ items, checked }),
  });

  const getKeywordData = (argParams?: any) => {
    if (globalFilters) {
      let productIds = '';
      if (ref.current.selectedProducts) {
        productIds = ref.current.selectedProducts
          .map((item: any) => item.shop_ads_eid)
          .join(',');
      }

      const urlParams = new URLSearchParams(location.search);
      let shopIds = urlParams.get('shopId');
      if (globalFilters.shops) {
        shopIds = globalFilters.shops
          .map((item: any) => item.id)
          .join(',');
      }

      const params: any = {
        shop_eids: shopIds || 4110,
        from: globalFilters.timeRange.start,
        to: globalFilters.timeRange.end,
        shop_ads_eids: ref.current.selectedShopAdsIds,
        limit: pagination.limit,
        page: pagination.page,
        order_by: '',
        order_mode: '',
        status: filterKeywordStatus,
        value_filter: searchKeyword,
        ...argParams,
      };

      if (ref.current.sortBy.length > 0) {
        params.order_by = ref.current.sortBy[0].id;
        params.order_mode = ref.current.sortBy[0].desc
          ? 'desc'
          : 'asc';
      }
      if (params.status === 'All') {
        params.status = '';
      }

      dispatch(actions.getShopAdsKeywords(params));
    }
  };

  const handleViewRuleLog = (row) => {
    console.log('View rule log', row);
    setRuleLog({
      isOpen: true,
      shopEid: row.shop_eid,
      parentName: row.shopAdsName,
      itemId: row.keywordId,
      itemName: row.keywordName,
      currency: row.currency,
    });
  };

  const handleChangeSearchText = (value: any) => {
    updateSearchKeyword(value);
    getKeywordData({ value_filter: value, page: 1 });
  };

  const handleChangeStatus = (value: any) => {
    updateFilterKeywordStatus(value);
    getKeywordData({ status: value, page: 1 });
  };

  const handleSorting = (sortBy: any) => {
    const isDifference =
      differenceWith(sortBy, ref.current.sortBy, isEqual).length > 0;
    if (isDifference) {
      ref.current.sortBy = sortBy;
      getKeywordData();
    }
  };

  const handleChangePage = (page: number) => {
    getKeywordData({ page });
  };

  const handleChangePageSize = (value: number) => {
    getKeywordData({ page: 1, limit: value });
  };

  const handleOpenAddKeyword = () => {
    setAddKeywordModal((state) => ({
      ...state,
      isOpen: true,
      shopId: null,
    }));
  };

  const handleCloseDialog = () => {
    setAddKeywordModal((state) => ({
      ...state,
      isOpen: false,
    }));
  };

  const handleAddingSuccess = () => {
    getKeywordData({ page: 1 });
    handleCloseDialog();
  };

  const handleActivator = (type: string, keywordList: any) => {
    const isAllowed = keywordList.every((i) => !i._isDisabled);
    if (isAllowed) {
      const activator: any = {
        active: {
          labelSubmit: 'Activate',
          title: 'Activate Keyword(s)?',
          content: 'Activating Keyword will resume its bidding ads.',
          status: 'running',
        },
        deactive: {
          labelSubmit: 'Deactivate',
          title: 'Deactivate Keywords?',
          content: 'Deactivating Keyword will pause its bidding ads.',
          status: 'paused',
        },
      };

      setShowActivator({
        ...showActivator,
        ...activator[type],
        focusKeyword: keywordList,
        show: true,
      });
    } else {
      toast.error('Can not modify ended campaigns');
    }
  };

  const handleCloseNotification = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setNotification({ ...notification, open: false });
  };

  const handleSubmit = async (keywords: any[]) => {
    const shopEids = keywords.map((item) => item.shopId);
    const keywordIds = keywords.map((item) => item.keywordId);
    setShowActivator({ ...showActivator, show: false });

    try {
      const res = await updateShopAdsKeywordStatus({
        shop_eids: [...new Set(shopEids)],
        shop_ads_keyword_ids: keywordIds,
        status: showActivator.status === 'running' ? 1 : 0,
      });
      setNotification({ ...notification, ...res, open: true });
      if (res.success) {
        await getKeywordData();
      }
    } catch (error) {
      setNotification({
        ...notification,
        message: error.message,
        open: true,
        success: false,
      });
    }
  };

  const handleSubmitModifyKeywords = async (data: any) => {
    const keywordIds = selectedKeywords.map(
      (item: any) => item.keywordId,
    );
    const shopEids = selectedKeywords.map((item: any) => item.shopId);
    handleUpdateKeyword({
      shopIds: [...new Set(shopEids)],
      keywordIds: keywordIds,
      biddingPrice: Number(data.bidding_price),
      matchType: data.match_type,
    });
  };

  const handleUpdateKeyword = async (params: any) => {
    try {
      const res = await updateShopAdsKeywordPrice({
        shop_eids: params.shopIds,
        shop_ads_keyword_ids: params.keywordIds,
        bidding_price: parseFloat(params.biddingPrice),
        match_type: params.matchType,
      });
      setNotification({ ...notification, ...res, open: true });
      if (res.success) {
        getKeywordData();
        setOpenModalBulkAction({
          ...openModalBulkAction,
          open: false,
        });
      }
    } catch (error) {
      notification.message = [].concat(error.message).join('\n');
      setNotification({
        ...notification,
        open: true,
        success: false,
      });
    }
  };

  const handleAddNewKeyword = (payload: {
    shopAdsId: number;
    shopId: number;
    keywords: {
      keywordName: string;
      keywordId: number;
      matchType: string;
      biddingPrice: number;
      isValid?: boolean;
    }[];
  }) => {
    console.info('handleAddNewKeyword', payload);
    let keywords = get(payload, 'keywords', []).filter(
      (i) => i.isValid !== false,
    );
    if (keywords.length === 0) {
      setNotification({
        ...notification,
        message: 'There are no valid keywords to add.',
        open: true,
      });
      return;
    }

    new Promise((resolve, reject) => {
      dispatch(
        actions.shopAdsAddKeywords({
          ...payload,
          keywords,
          promise: { resolve, reject },
        }),
      );
    })
      .then((res) => {
        setNotification({ ...notification, ...res, open: true });
      })
      .catch((res) => {
        setNotification({ ...notification, ...res, open: true });
      });
  };

  const handleOpenModifyBudget = () => {
    const allowItems = selectedKeywords.filter((i) => !i._isDisabled);
    if (allowItems.length > 0) {
      const isDiffCurrency = allowItems.some(
        (i) => i.currency !== allowItems[0].currency,
      );
      if (isDiffCurrency) {
        toast.error('Selected keywords have the difference currency');
      } else {
        setOpenModalBulkAction({
          open: true,
          items: allowItems,
          currency: allowItems[0].currency,
        });
      }
    } else {
      toast.error('Can not modify ended keywords');
    }
  };

  const handleGetSuggestBiddingPrice = async (params: any) => {
    if (params.shopId && params.keywordId) {
      let result = await getShopAdsSuggestBiddingPrice({
        shop_eids: [params.shopId],
        keyword_id: params.keywordId,
      });
      if (result.success) {
        return result.data.suggest_bidding_price;
      } else {
        return 0;
      }
    }
    return 0;
  };

  const showStatusPopup = (keyword: any, status: boolean) => {
    handleActivator(
      status ? 'deactive' : 'active',
      [].concat(keyword),
    );
  };

  const handleAddNewRule = (row) => {
    setCreateRuleKeyword({
      ...createRuleKeyword,
      shopId: row.shop_eid,
      keywords: [
        {
          keyword_id: row.shop_ads_keyword_id,
          keyword_name: row.keyword_name,
          currency: row.currency,
          matchType: row.match_type,
        },
      ],
      products: [
        {
          product_id: row.shop_ads_eid,
          product_name: row.shop_ads_name,
        },
      ],
      currency: row.currency,
    });
    setOpenModalRule(true);
  };

  const handleAddRuleSuccess = () => {
    getKeywordData();
    getRuleList();
    setCreateRuleKeyword((state) => ({
      ...state,
      isOpen: false,
    }));
  };

  const handleCloseAddRule = () => {
    setCreateRuleKeyword((state) => ({
      ...state,
      isOpen: false,
    }));
  };

  const getRuleList = () => {
    if (listKeywordRules.isOpen) {
      dispatch(
        actions.getKeywordRules({
          keywordId: listKeywordRules.keywordId,
          shopEid: listKeywordRules.shopEid,
        }),
      );
    }
  };

  const onSubmitRule = (rule: any) => {
    const conditionJson = JSON.parse(rule.conditionJson);
    const changeDate =
      rule.fromDate === rule.timelineFrom &&
      rule.toDate === rule.timelineTo;
    const changeCondition =
      JSON.stringify(conditionJson) ===
      JSON.stringify(rule.conditions);
    if (!changeCondition && changeDate) {
      dispatch(
        actions.addExistingRule({
          shopEid: rule.shopEid,
          featureCode: 'M_SHP_SA',
          objectType: 'shop_ads_keyword',
          objectEids: rule.objectEids,
          ruleId: rule.ruleId,
          callback: (rs) => {
            setNotification({
              open: true,
              message: rs.data,
              success: rs.success,
            });
            if (rs.success) {
              getKeywordData();
              getRuleList();
            }
          },
        }),
      );
    } else {
      setNewRules({
        open: true,
        rule: {
          ...rule,
          ruleName: rule.ruleName,
          listRules: rule.conditions,
        },
      });
    }
    setOpenModalRule(false);
  };

  const createNewRule = (rule: any) => {
    const postRule = {
      ...newRule.rule,
      ruleName: rule.ruleName,
    };

    dispatch(
      actions.createKeywordRule({
        rule: postRule,
        keywords: createRuleKeyword.keywords,
        shopId: postRule.shopEid,
        callback: (rs) => {
          setNotification({
            open: true,
            message: rs.data,
            success: rs.success,
          });
          if (rs.success) {
            getKeywordData();
          }
        },
      }),
    );

    setNewRules({
      ...newRule,
      open: false,
    });
  };

  const updateSelectedKeywords = (items: any) => {
    console.log('updateSelectedShops', items, keywords, selectedIds);
    dispatch(actions.updateSelectedShopAdKeywords({ items }));
  };

  const handleOpenModalAddRule = () => {
    const allowItems = selectedKeywords.filter((i) => !i._isDisabled);
    if (allowItems.length > 0) {
      let shopEid = allowItems.map((p) => p.shop_eid);
      shopEid = [...new Set(shopEid)];

      if (shopEid.length > 1) {
        setNotification({
          open: true,
          success: false,
          message: 'Please choose keywords in one shop',
        });
      } else {
        const keywordList = allowItems.map((row) => ({
          keyword_id: row.keywordId,
          keyword_name: row.keywordName,
          currency: row.currency,
          matchType: row.matchType,
        }));

        const productList = allowItems.reduce((acc, item) => {
          if (acc.find((i) => i.product_id === item.shopAdsId)) {
            return acc;
          } else {
            return acc.concat({
              product_id: item.shopAdsId,
              product_name: item.shopAdsName,
            });
          }
        }, []);

        const matchType = allowItems.filter(
          (item) => item.match_type === 'broad_match',
        );

        setCreateRuleKeyword({
          ...createRuleKeyword,
          shopId: shopEid[0],
          keywords: keywordList,
          products: productList,
          currency: allowItems[0].currency,
          matchType:
            matchType.length > 0 ? 'BROAD_MATCH' : 'EXACT_MATCH',
        });
        setOpenModalRule(true);
      }
    } else {
      toast.error('Can not add rule with ended keywords');
    }
  };

  const handleViewRule = (row) => {
    const allowStatus = [
      SHOP_ADS_KEYWORDS_STATUS.RUNNING,
      SHOP_ADS_KEYWORDS_STATUS.PAUSED,
      SHOP_ADS_KEYWORDS_STATUS.SCHEDULED,
    ];
    setListKeywordRules({
      isOpen: true,
      allowUpdate: allowStatus.includes(row.status),
      shopEid: row.shop_eid,
      keywordId: row.shop_ads_keyword_id,
      keywordName: row.keyword_name,
      productId: row.shop_ads_eid,
      productName: row.shop_ads_name,
      currency: row.currency,
      matchType: row.match_type,
    });
  };

  const handleDeleteRules = () => {
    setDeleteRulesModal({
      open: true,
      items: selectedKeywords,
    });
  };

  const onSubmitDeleteRules = () => {
    console.log('Delete keyword rules', deleteRulesModal);
    setDeleteRulesModal({ ...deleteRulesModal, open: false });
    dispatch(
      actions.deleteAllKeywordRules({
        featureCode: 'M_SHP_SA',
        shopEids: union(
          deleteRulesModal.items.map((i) => i.shop_eid),
        ),
        itemIds: deleteRulesModal.items.map((i) => i.keywordId),
        callback: (rs) => {
          setNotification({
            open: true,
            message: rs.data,
            success: rs.success,
          });
          if (rs.success) {
            getKeywordData();
          }
        },
      }),
    );
  };

  const headers = React.useMemo(() => {
    const columns = [...SHOP_ADS_KEYWORD_COLUMNS];
    columns[0].onCellClick = showStatusPopup;

    const ruleCol = columns.find((i) => i.id === 'rule');
    ruleCol.onCellClick = (row: any) => {
      if (row.ruleCount) {
        handleViewRule(row);
      } else {
        handleAddNewRule(row);
      }
    };

    const logCol = columns.find((i) => i.id === 'ruleLog');
    logCol.onCellClick = (row: any) => {
      handleViewRuleLog(row);
    };

    return columns;
  }, [SHOP_ADS_KEYWORD_COLUMNS]);

  const handleUpdateSelectedKeywordsForAddRule = (items: any) => {
    const keywordList = items.map((i) => ({
      keyword_id: i.keywordId,
      keyword_name: i.keywordName,
      currency: i.currency,
      matchType: i.matchType,
    }));

    const productList = items.reduce((acc, item) => {
      if (acc.find((i) => i.product_id === item.shopAdsId)) {
        return acc;
      } else {
        return acc.concat({
          product_id: item.shopAdsId,
          product_name: item.shopAdsName,
        });
      }
    }, []);

    setCreateRuleKeyword({
      ...createRuleKeyword,
      keywords: keywordList,
      products: productList,
    });

    updateSelectedKeywords(items);
  };

  return (
    <React.Fragment>
      <KeywordView
        tableHeaders={headers}
        keywords={keywords}
        pagination={pagination}
        searchText={searchKeyword}
        statusList={statusList}
        selectedStatus={filterKeywordStatus}
        selectedIds={selectedIds}
        selectedKeywords={selectedKeywords}
        handleSelectItem={onSelectItem}
        handleSelectAll={onSelectAll}
        getRowId={getRowId}
        onSearch={handleChangeSearchText}
        onChangeStatus={handleChangeStatus}
        onSorting={handleSorting}
        onChangePage={handleChangePage}
        onChangePageSize={handleChangePageSize}
        onAddKeyword={handleOpenAddKeyword}
        handleActivator={handleActivator}
        handleUpdateKeyword={handleUpdateKeyword}
        handleOpenModifyBudget={handleOpenModifyBudget}
        handleGetSuggestBiddingPrice={handleGetSuggestBiddingPrice}
        openModalAddrule={handleOpenModalAddRule}
        handleDeleteRules={handleDeleteRules}
      />
      {showActivator.show && (
        <ModalConfirmUI
          open={showActivator.show}
          onSubmit={() =>
            handleSubmit([].concat(showActivator.focusKeyword))
          }
          onClose={() =>
            setShowActivator({ ...showActivator, show: false })
          }
          labelSubmit={showActivator.labelSubmit}
          title={showActivator.title}
          content={showActivator.content}
        />
      )}
      {deleteRulesModal.open && (
        <ModalConfirmUI
          open={deleteRulesModal.open}
          onSubmit={onSubmitDeleteRules}
          onClose={() =>
            setDeleteRulesModal({ ...deleteRulesModal, open: false })
          }
          labelSubmit="OK"
          title="Delete all rules?"
          content="All rules that you added on these keywords will be deleted."
        />
      )}
      {openModalBulkAction.open && (
        <ModalBulkActionKeyword
          open={openModalBulkAction.open}
          setOpenModalBulkAction={(e) =>
            handleSubmitModifyKeywords(e)
          }
          currency={openModalBulkAction.currency}
          onClose={() =>
            setOpenModalBulkAction({
              ...openModalBulkAction,
              open: false,
            })
          }
          keywords={openModalBulkAction.items}
          updateSelected={(items) => {
            updateSelectedKeywords(items);
            setOpenModalBulkAction({
              ...openModalBulkAction,
              items,
            });
          }}
        />
      )}
      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={notification.open}
        onClose={handleCloseNotification}
        autoHideDuration={3000}
      >
        <Alert
          onClose={handleCloseNotification}
          elevation={6}
          variant="filled"
          severity={notification.success ? 'success' : 'error'}
        >
          {notification.message}
        </Alert>
      </Snackbar>

      <ContainerShopAdsAddKeyword
        shops={globalFilters.shops}
        isShown={addKeywordModal.isOpen}
        globalFilters={globalFilters}
        onCloseModal={() => {
          handleCloseDialog();
        }}
        onAddNewKeywords={(payload) => {
          handleAddNewKeyword(payload);
        }}
      />

      <CreateRuleShopAdKeyword
        isOpen={createRuleKeyword.isOpen}
        keywords={createRuleKeyword.keywords}
        shopId={createRuleKeyword.shopId}
        products={createRuleKeyword.products}
        onSubmitSuccess={handleAddRuleSuccess}
        onClose={handleCloseAddRule}
        onOpenExistingRule={() => {
          setOpenModalRule(true);
          setCreateRuleKeyword({
            ...createRuleKeyword,
            isOpen: false,
          });
        }}
      />
      <AddRuleShopAdKeywordContainer
        open={openModalRule}
        onSubmit={onSubmitRule}
        handleCreate={() => {
          setOpenModalRule(false);
          setCreateRuleKeyword({
            ...createRuleKeyword,
            isOpen: true,
          });
        }}
        onClose={(e: any) => setOpenModalRule(e)}
        keywords={createRuleKeyword.keywords}
        products={createRuleKeyword.products}
        setKeywords={handleUpdateSelectedKeywordsForAddRule}
        selectedKeywords={selectedKeywords}
        currency={createRuleKeyword.currency}
        shopEid={createRuleKeyword.shopId}
        matchType={'BROAD_MATCH'}
        // matchType={matchType.length > 0 ? 'BROAD_MATCH' : 'EXACT_MATCH'}
      />
      <ModalAddRuleName
        open={newRule.open}
        onSubmit={createNewRule}
        onClose={(e: any) => setNewRules({ ...newRule, open: e })}
      />
      <ListRuleShopAdKeyword
        allowUpdate={listKeywordRules.allowUpdate}
        isOpen={listKeywordRules.isOpen}
        shopEid={listKeywordRules.shopEid}
        keywordId={listKeywordRules.keywordId}
        keywordName={listKeywordRules.keywordName}
        productId={listKeywordRules.productId}
        productName={listKeywordRules.productName}
        currency={listKeywordRules.currency}
        matchType={listKeywordRules.matchType}
        onClose={() => {
          setListKeywordRules((state) => ({
            ...state,
            isOpen: false,
          }));
        }}
        onUpdateSuccess={getKeywordData}
        onAddRule={(keyword) => handleAddNewRule(keyword)}
      />
      <ShopAdsKeywordRuleLog
        open={ruleLog.isOpen}
        shopEid={ruleLog.shopEid}
        parentName={ruleLog.parentName}
        itemId={ruleLog.itemId}
        itemName={ruleLog.itemName}
        onClose={() => {
          setRuleLog((state) => ({
            ...state,
            isOpen: false,
          }));
        }}
      ></ShopAdsKeywordRuleLog>
    </React.Fragment>
  );
};
