import { EditButton } from '@ep/shopee/src/components/common/edit-button';
import {
  AdsStateCell,
  ButtonCell,
  CellText,
  LogCell,
  PercentCell,
  StatusCell,
  SubjectCell,
  SwitchCell,
} from '@ep/shopee/src/components/common/table-cell';
import moment from 'moment';
import React from 'react';
import { makeLazyCell } from '../../components/common/table-cell/lazy-cell';
import { KeywordBudgetEditorPopover } from '../../components/edit-form/keyword-budget-editor-popover';
import { KeywordTypeEditorPopover } from '../../components/edit-form/keyword-type-editor-popover';
import {
  MATCH_TYPE,
  SHOP_ADS_KEYWORDS_STATUS,
} from '../../constants';

export const SHOP_ADS_KEYWORD_COLUMNS = [
  {
    Header: ' ',
    id: 'switch',
    accessor: (row: any) => ({
      id: row.shopAdsId,
      row,
      isOn: !row.childStatus
        ? true
        : row.childStatus?.shop_ads_keyword_state,
      isDisabled: row.shopStatus === 'ended',
    }),
    sticky: 'left',
    width: 70,
    disableSortBy: true,
    Cell: SwitchCell,
  },
  {
    Header: 'Keyword bidding',
    accessor: 'keyword_name',
    sticky: 'left',
  },
  {
    Header: 'Status',
    id: 'status',
    sticky: 'left',
    width: 70,
    disableSortBy: true,
    accessor: (row: any) => {
      return { type: row.status };
    },
    Cell: StatusCell,
  },
  {
    Header: 'Shop Ads',
    id: 'shop_ads_name',
    accessor: (row: any) => ({
      name: row.shopAdsName,
      code: row.shopAdsCode,
    }),
    width: 200,
    disableSortBy: true,
    Cell: SubjectCell,
  },
  {
    Header: 'Ads current state',
    id: 'is_ads_position',
    accessor: (row: any) => ({
      isWin: !!row.isAdsPosition,
      updatedDate:
        row.adsPositionLastUpdatedAt &&
        !isNaN(row.adsPositionLastUpdatedAt)
          ? moment
              .unix(row.adsPositionLastUpdatedAt)
              .format('DD/MM/YYYY')
          : '',
    }),
    width: 200,
    disableSortBy: true,
    Cell: AdsStateCell,
  },
  {
    Header: 'Bidding price',
    id: 'bidding_price',
    accessor: (row: any) => ({
      number: row.biddingPrice,
      currency: row.currency,
      editor: row._isDisabled ? null : (
        <KeywordBudgetEditorPopover
          keyword={row}
          currency={row.currency}
          bidding_price={row.biddingPrice}
          triggerElem={<EditButton />}
        />
      ),
    }),
    Cell: PercentCell,
    disableSortBy: true,
  },
  {
    Header: 'Ads item sold',
    id: 'sum_item_sold',
    accessor: (row: any) => ({ number: row.sum_item_sold }),
    Cell: PercentCell,
    width: 130,
  },
  {
    Header: 'Direct Ads item sold',
    id: 'sum_direct_item_sold',
    accessor: (row: any) => ({ number: row.sum_direct_item_sold }),
    Cell: PercentCell,
    width: 175,
  },
  {
    Header: 'Ads GMV',
    id: 'sum_gmv',
    accessor: (row: any) => ({
      number: row.sum_gmv,
      currency: row.currency,
    }),
    Cell: PercentCell,
    width: 100,
  },
  {
    Header: 'Direct Ads GMV',
    id: 'sum_direct_gmv',
    accessor: (row: any) => ({
      number: row.sum_direct_gmv,
      currency: row.currency,
    }),
    Cell: PercentCell,
    width: 150,
  },
  {
    Header: 'Cost',
    id: 'sum_cost',
    accessor: (row: any) => ({
      number: row.sumCost,
      currency: row.currency,
    }),
    Cell: PercentCell,
  },
  {
    Header: 'ROAS',
    id: 'roas',
    accessor: (row: any) => ({ number: row.roas }),
    Cell: PercentCell,
    disableSortBy: true,
  },
  {
    Header: 'Impression',
    id: 'sum_impression',
    accessor: (row: any) => ({ number: row.sumImpression }),
    Cell: PercentCell,
  },
  {
    Header: 'Click',
    id: 'sum_click',
    accessor: (row: any) => ({ number: row.sumClick }),
    Cell: makeLazyCell(PercentCell),
  },
  {
    Header: 'CIR',
    id: 'cir',
    accessor: (row: any) => ({ number: row.cir }),
    Cell: makeLazyCell(PercentCell),
    disableSortBy: true,
  },
  {
    Header: 'CPC',
    id: 'cpc',
    accessor: (row: any) => ({
      number: row.cpc,
      currency: row.currency,
    }),
    Cell: makeLazyCell(PercentCell),
    disableSortBy: true,
  },
  {
    Header: 'Match type',
    disableSortBy: true,
    accessor: (row: any) => ({
      text:
        row.matchType === MATCH_TYPE.BROAD_MATCH
          ? 'Broad match'
          : 'Exact match',
      currency: row.currency,
      editor: row._isDisabled ? null : (
        <KeywordTypeEditorPopover
          keyword={row}
          triggerElem={<EditButton />}
        />
      ),
    }),
    Cell: makeLazyCell(CellText),
  },
  {
    Header: 'Rule',
    id: 'rule',
    accessor: (row: any) => {
      let label = '';
      if (row.ruleCount) {
        label = row.ruleCount + ' rules';
      } else {
        const allowStatus = [
          SHOP_ADS_KEYWORDS_STATUS.RUNNING,
          SHOP_ADS_KEYWORDS_STATUS.PAUSED,
          SHOP_ADS_KEYWORDS_STATUS.SCHEDULED,
        ];
        if (allowStatus.includes(row.status)) {
          label = 'Add rule';
        }
      }

      return {
        ...row,
        label,
      };
    },
    Cell: makeLazyCell(ButtonCell),
    width: 95,
    disableSortBy: true,
    alwaysEnable: true,
  },
  {
    Header: '',
    id: 'ruleLog',
    accessor: (row: any) => row,
    Cell: makeLazyCell(LogCell),
    disableSortBy: true,
    width: 70,
    alwaysEnable: true,
  },
];
