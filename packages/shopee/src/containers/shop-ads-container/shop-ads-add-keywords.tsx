import { useOneFullPageLoading } from '@ep/one/src/hooks/use-loading';
import { makeOneStore } from '@ep/one/src/utils/store';
import React from 'react';
import { Provider, useDispatch, useSelector } from 'react-redux';
import { ModalConfirmUI } from '../../components/common/modal-confirm';
import { initColumns } from '../../components/create-shop-ads/add-keyword-columns';
import { ExistingShopAdsAddKeywords } from '../../components/create-shop-ads/add-keyword-existing-shopads';
import {
  actions,
  rootSaga,
} from '../../redux/shop-ads-create/actions';
import {
  EditShopAdsState,
  initState,
  reducer,
} from '../../redux/shop-ads-create/reducers';

export function ContainerShopAdsAddKeyword({
  shops,
  isShown,
  globalFilters,
  onCloseModal,
  onAddNewKeywords,
}: {
  shops: any;
  isShown: boolean;
  globalFilters: any;
  onAddNewKeywords: (payload: any) => void;
  onCloseModal: () => void;
}) {
  let store = React.useMemo(
    () =>
      makeOneStore({
        storeName: 'shopee/shopads/add-keyword',
        rootSaga: rootSaga,
        reducer: reducer,
        initState: { ...initState, shopList: shops },
      }),
    [shops],
  );

  return (
    <Provider store={store}>
      <ContainerExistingShopAdsAddKeywords
        shops={shops}
        isShown={isShown}
        globalFilters={globalFilters}
        onAddNewKeywords={onAddNewKeywords}
        onCloseModal={onCloseModal}
      />
    </Provider>
  );
}

function ContainerExistingShopAdsAddKeywords({
  shops,
  isShown,
  globalFilters,
  onAddNewKeywords,
  onCloseModal,
}: {
  shops: any;
  isShown: boolean;
  globalFilters: any;
  onAddNewKeywords: (payload: any) => void;
  onCloseModal: () => void;
}) {
  const dispatch = useDispatch();
  const state = useSelector(
    ({
      // setupAdsInfo: { shop, currency },
      shopAds,
      selectedShopId,
      selectedShopAdsId,
      keywordList,
      addKeyword: {
        keywordBucket,
        selectedKeywords,
        keywords,
        searchText,
        pagination,
        isShowAddKeywordModal,
      },
    }: EditShopAdsState) => {
      return {
        keywordBucket,
        selectedKeywords,
        keywords,
        searchText,
        pagination,
        isShowAddKeywordModal,
        addedKeywordList: keywordList,
        shopAds,
        selectedShopAdsId,
        selectedShopId,
      };
    },
  );

  const {
    keywordBucket,
    selectedKeywords,
    keywords,
    searchText,
    pagination,
    addedKeywordList,
    shopAds,
    selectedShopAdsId,
    selectedShopId,
  } = state;

  const [shopCurrency, setShopCurrency] = React.useState('VND');
  const [
    confirmInvalidKeywords,
    showConfirmInvalidKeywords,
  ] = React.useState({ numInvalidKeywords: 0, display: false });

  const loading = useOneFullPageLoading();

  let presentKeywords = React.useMemo(() => {
    const alladded = addedKeywordList.concat(keywordBucket);
    return keywords.map((i) => {
      if (alladded.find((i1) => i1.keywordId === i.keywordId)) {
        return { ...i, added: true };
      } else {
        return { ...i, added: false };
      }
    });
  }, [keywordBucket, addedKeywordList, keywords]);

  const handleUpdateSelectedKeywordPrice = (value: any) => {
    dispatch(
      actions.updateSelectedKeywordPrice({
        biddingPrice: value.biddingPrice,
        matchType: value.matchType,
      }),
    );
  };
  const handleSearch = (searchText: any, currency: string) => {
    dispatch(actions.updateKeywordSearch({ searchText, currency }));
  };

  const handleAddKeywordsToBucket = (keywords: any[]) => {
    console.info({ keywords });
    dispatch(actions.addKeywordToBucket({ keywords }));
  };

  const handleRemoveKeywordFromBucket = (keyword: any) => {
    dispatch(
      actions.removeKeyword({
        keyword,
      }),
    );
  };

  const handleResetKeywordBucket = () => {
    dispatch(actions.removeAllKeywords());
  };

  const handleAddCustomKeywords = (
    keywordNames: any,
    currency: any,
  ) => {
    dispatch(actions.addCustomKeywords({ keywordNames, currency }));
  };

  const handleSelectKeywordItem = (keyword: any, isAdd: boolean) => {
    dispatch(actions.selectKeyword({ keyword, isAdd }));
  };

  const handleSelectAllKeywordItems = (checked: boolean) => {
    dispatch(
      actions.selectAllKeywords({
        isAdd: checked,
      }),
    );
  };

  const handleChangePagination = (pagination: any) => {
    dispatch(actions.updateKeywordPagination({ pagination }));
  };

  const handleBackToPrevious = () => {
    dispatch(actions.openAddKeywordModal({ visible: false }));
    dispatch(actions.resetAddKeyword());
  };

  const handleCancel = () => {
    dispatch(actions.resetAddKeyword());
    onCloseModal();
  };

  const handleConfirmationForContainsInvalidKeywords = () => {
    const numKeywords = keywordBucket.filter(
      (i) => i.isValid === false,
    ).length;

    if (numKeywords) {
      showConfirmInvalidKeywords({
        numInvalidKeywords: numKeywords,
        display: true,
      });
    } else {
      handleSaveKeyword();
    }
  };

  const handleSaveKeyword = () => {
    onAddNewKeywords({
      shopId: selectedShopId,
      shopAdsId: selectedShopAdsId,
      keywords: keywordBucket,
    });
    dispatch(actions.resetAddKeyword());
    onCloseModal();
  };

  const handleChangeShop = (shop) => {
    setShopCurrency(shop.currency);
    dispatch(actions.getSuggestKeywords({ shopId: shop.id }));
    dispatch(actions.selectShop({ shopId: shop.id }));
    dispatch(actions.getShopAds({ shopId: shop.id, globalFilters }));
  };
  const handleChangeShopAds = (shopAds) => {
    console.info('handlechangeshopads', shopAds);
    dispatch(actions.selectShopAds({ shopAdsId: shopAds.shopAdsId }));
  };

  const handleRemoveCustomKeyword = (keyword) => {
    dispatch(
      actions.removeCustomKeywords({
        keywordIds: [keyword.keywordId],
      }),
    );
  };

  const headers = React.useMemo(
    () =>
      initColumns({
        onAddToBucket: (value: any) =>
          handleAddKeywordsToBucket([value]),
        onRemoveKeyword: (keyword) =>
          handleRemoveCustomKeyword(keyword),
      }),
    [],
  );

  return (
    <React.Fragment>
      <ExistingShopAdsAddKeywords
        shopList={shops}
        shopAdsList={shopAds}
        tableHeaders={headers}
        openModalAddKeyword={isShown}
        currency={shopCurrency}
        keywords={presentKeywords}
        selectedKeywords={selectedKeywords}
        searchText={searchText}
        pagination={pagination}
        keywordBucket={keywordBucket}
        onUpdateSelectedKeywordPrice={
          handleUpdateSelectedKeywordPrice
        }
        onSearch={handleSearch}
        onAddKeywordsToBucket={handleAddKeywordsToBucket}
        onRemoveKeywordFromBucket={handleRemoveKeywordFromBucket}
        onResetKeywordBucket={handleResetKeywordBucket}
        onAddCustomKeywords={handleAddCustomKeywords}
        onSelectKeywordItem={handleSelectKeywordItem}
        onSelectAllKeywordItems={handleSelectAllKeywordItems}
        onChangePagination={handleChangePagination}
        onBackToPrevious={handleBackToPrevious}
        onSave={handleConfirmationForContainsInvalidKeywords}
        onCancel={handleCancel}
        selectedShopId={selectedShopId}
        selectedShopAdsId={selectedShopAdsId}
        onChangeShop={handleChangeShop}
        onChangeShopAds={handleChangeShopAds}
      />
      {loading}
      <ModalConfirmUI
        title={`Continue without invalid keywords?`}
        open={confirmInvalidKeywords.display}
        content={`
        ${confirmInvalidKeywords.numInvalidKeywords} keywords are reserved & can't be used.
        Do you want to continue without them?`}
        labelSubmit={'Continue'}
        onClose={() => {
          showConfirmInvalidKeywords({
            numInvalidKeywords: 0,
            display: false,
          });
        }}
        onSubmit={() => {
          showConfirmInvalidKeywords({
            numInvalidKeywords: 0,
            display: false,
          });
          handleSaveKeyword();
        }}
      />
    </React.Fragment>
  );
}
