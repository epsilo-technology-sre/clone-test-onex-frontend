import { useOneFullPageLoading } from '@ep/one/src/hooks/use-loading';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ChannelHeaderUI } from '../../components/channel-header';
import { BreadcrumbsUI } from '../../components/common/breadcrumbs';
import {
  TabPanelUI,
  TabSelectionUI,
} from '../../components/common/tab-selection-shop-ads';
import { TABS } from '../../constants';
import { ShopAdsContainer } from '../../containers/shop-ads-container';
import { actions } from '../../redux/shop-ads/actions';
import { ShopAdState } from '../../redux/shop-ads/reducers';
import { GlobalFiltersContainer } from './global-filters-container';
import { KeywordShopAdsContainer } from './keyword-shop-ads-container';

const LINKS = [
  { text: 'Advertising', href: '/' },
  { text: 'Shopee', href: '/' },
  { text: 'Shop Ads', href: '/' },
];

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      background: '#ffffff',
      padding: theme.spacing(2),
    },
  }),
);

export const ShopAds = () => {
  const classes = useStyles();
  const [links, setLinks] = useState(LINKS);

  const dispatch = useDispatch();

  const linkCreateShopAds = '/advertising/shopee/create-shop-ads';

  const { tab, shopAdsSelectedIds } = useSelector(
    (state: ShopAdState) => ({
      tab: state.tab,
      shopAdsSelectedIds: state.shopAds.selectedIds,
    }),
  );

  const resetSelectedShopAds = () => {
    dispatch(
      actions.onSelectShopAds({ items: null, checked: false }),
    );
  };

  const loading = useOneFullPageLoading();

  const globalFilters = useSelector((state: ShopAdState) => {
    return state.globalFilters;
  });

  const tabs = [
    { type: TABS.SHOPADS, text: 'Shop Ads' },
    { type: TABS.SHOPADS_KEYWORD, text: 'Keywords' },
  ];

  const handleChangeTab = (tab: any) => {
    dispatch(actions.changeTab({ tab }));
  };

  const handleClickLink = (link: any) => {
    console.log('Click link ', link);
  };

  return (
    <>
      <div className={classes.root}>
        <BreadcrumbsUI links={links} onClickLink={handleClickLink} />
        <ChannelHeaderUI
          title="Shop Ads"
          calloutButtonText="Create Shop Ads"
          calloutLink={linkCreateShopAds}
        />
        <GlobalFiltersContainer />
        <TabSelectionUI
          tabs={tabs}
          selectedTab={tab}
          selectedShopAds={shopAdsSelectedIds}
          selectedKeywords={[]}
          showIcon={true}
          onChangeTab={handleChangeTab}
          handleResetSelectedShopAds={resetSelectedShopAds}
        />
        <TabPanelUI value={tab} index={TABS.SHOPADS}>
          <ShopAdsContainer globalFilters={globalFilters} />
        </TabPanelUI>
        <TabPanelUI value={tab} index={TABS.SHOPADS_KEYWORD}>
          <KeywordShopAdsContainer></KeywordShopAdsContainer>
        </TabPanelUI>
      </div>
      {loading}
    </>
  );
};

export default ShopAds;
