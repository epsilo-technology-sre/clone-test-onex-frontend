import { get } from 'lodash';
import moment from 'moment';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { GlobalFilters } from '../../components/global-filters';
import { actions } from '../../redux/shop-ads/actions';

export function GlobalFiltersContainer() {
  const dispatch = useDispatch();

  const {
    countries,
    shops,
    selectedTimeRange,
    selectedCountries,
    selectedShops,
  } = useSelector((state: any) => {
    return {
      countries: get(state, 'optionGlobalFilters.countries', []),
      shops: get(state, 'optionGlobalFilters.shops', []),
      selectedCountries: get(state, 'globalFilters.countries', []),
      selectedShops: get(state, 'globalFilters.shops', []),
      selectedTimeRange: get(state, 'globalFilters.timeRange', {}),
    };
  });

  useEffect(() => {
    dispatch(actions.getShops());
  }, []);

  const handleChangeDateRange = ({ startDate, endDate }: any) => {
    dispatch(
      actions.filterDateRange({
        timeRange: {
          start: moment(startDate).format('YYYY-MM-DD'),
          end: moment(endDate).format('YYYY-MM-DD'),
        },
      }),
    );
  };

  const handleChangeCountries = (selected: any) => {
    dispatch(actions.filterCountry({ countries: selected }));
  };

  const handleChangeShops = (selected: any) => {
    dispatch(actions.filterShop({ shops: selected }));
  };

  const handleSubmitChange = () => {
    dispatch(actions.updateGlobalFilter());
  };

  return (
    <GlobalFilters
      countries={countries}
      shops={shops}
      selectedTimeRange={selectedTimeRange}
      selectedCountries={selectedCountries}
      selectedShops={selectedShops}
      onChangeDateRange={handleChangeDateRange}
      onChangeCountries={handleChangeCountries}
      onChangeShops={handleChangeShops}
      onSubmitChange={handleSubmitChange}
    />
  );
}
