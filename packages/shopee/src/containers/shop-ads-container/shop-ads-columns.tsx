import { EditButton } from '@ep/shopee/src/components/common/edit-button';
import {
  BudgetCell,
  ButtonCell,
  LogCell,
  PercentCell,
  ShopCreativeCell,
  StatusCell,
  SubjectCell,
  SwitchCell,
  TimelineCell,
} from '@ep/shopee/src/components/common/table-cell';
import { AdCreativeEditorPopover } from '@ep/shopee/src/components/edit-form/ad-creative-editor-popover';
import React from 'react';
import { makeLazyCell } from '../../components/common/table-cell/lazy-cell';
import { BudgetForm } from '../../components/edit-form/inline-edit-popover/budget-editor';
import { EditorPopover as EditorContextPopover } from '../../components/edit-form/inline-edit-popover/popover-wrapper';
import { TimelineForm } from '../../components/edit-form/inline-edit-popover/timeline-editor';
import { SHOP_ADS_STATUS } from '../../constants';

export const SHOP_ADS_COLUMNS = ({
  onInlineSubmitTagline,
  onInlineSubmitBudget,
  onInlineSubmitTimeline,
}) => [
  {
    Header: ' ',
    id: 'switch',
    accessor: (row: any) => ({
      id: row.shopAdsId,
      row,
      isOn: row.shopStatus,
      isDisabled: row.shopAdsStatus === SHOP_ADS_STATUS.ENDED,
    }),
    sticky: 'left',
    width: 70,
    disableSortBy: true,
    Cell: SwitchCell,
  },
  {
    Header: 'Shop Ads',
    id: 'shop_ads_name',
    accessor: (row: any) => ({
      name: row.shopAdsName,
      code: row.shopAdsCode,
    }),
    sticky: 'left',
    width: 200,
    disableSortBy: true,
    Cell: SubjectCell,
  },
  {
    Header: 'Ad Creative',
    id: 'shop_category_name',
    accessor: (row: any) => ({
      name: row.shopCategoryEid
        ? row.shopCategoryName
        : 'Main Shop Page',
      code: row.adsCreativeTagline || '-',
      editorEnabled: row.shopAdsStatus !== SHOP_ADS_STATUS.ENDED,
    }),
    sticky: 'left',
    width: 200,
    disableSortBy: true,
    Cell: ShopCreativeCell,
    Editor: TaglineEditor,
  },
  {
    Header: 'Status',
    id: 'status',
    sticky: 'left',
    width: 70,
    disableSortBy: true,
    accessor: (row: any) => {
      const type = row.shopAdsStatus;
      let children = [];
      if (
        type === SHOP_ADS_STATUS.RUNNING ||
        type === SHOP_ADS_STATUS.PAUSED
      ) {
        children = [
          {
            enable: !!row.childStatus?.shop_ads_budget_state,
            text: 'Budget',
          },
          {
            enable: !!row.childStatus?.account_balance_state,
            text: 'Account balance',
          },
          {
            enable: !!row.childStatus?.shop_ads_keyword_active_state,
            text: 'Keyword State',
          },
          {
            enable: !!row.childStatus?.shop_ads_state,
            text: 'Shop ads State',
          },
        ];
      }
      return { type, children };
    },
    Cell: StatusCell,
  },
  {
    Header: 'Shop',
    id: 'shop_name',
    accessor: 'shopName',
  },
  {
    Header: 'Ads item sold',
    id: 'sum_item_sold',
    accessor: (row: any) => ({ number: row.sumItemSold }),
    Cell: PercentCell,
    width: 130,
  },
  {
    Header: 'Direct Ads item sold',
    id: 'sum_direct_item_sold',
    accessor: (row: any) => ({ number: row.sumDirectItemSold }),
    Cell: PercentCell,
    width: 175,
  },
  {
    Header: 'Ads GMV',
    id: 'sum_gmv',
    accessor: (row: any) => ({
      number: row.sumGmv,
      currency: row.currency,
    }),
    Cell: PercentCell,
    width: 100,
  },
  {
    Header: 'Direct Ads GMV',
    id: 'sum_direct_gmv',
    accessor: (row: any) => ({
      number: row.sumDirectGmv,
      currency: row.currency,
    }),
    Cell: PercentCell,
    width: 150,
  },
  {
    Header: 'Cost',
    id: 'sum_cost',
    accessor: (row: any) => ({
      number: row.sumCost,
      currency: row.currency,
    }),
    Cell: PercentCell,
  },
  {
    Header: 'ROAS',
    id: 'roas',
    accessor: (row: any) => ({ number: row.roas }),
    Cell: PercentCell,
    disableSortBy: true,
  },
  {
    Header: 'Impression',
    id: 'sum_impression',
    accessor: (row: any) => ({ number: row.sumImpression }),
    Cell: PercentCell,
  },
  {
    Header: 'Click',
    id: 'sum_click',
    accessor: (row: any) => ({ number: row.sumClick }),
    Cell: makeLazyCell(PercentCell),
  },
  {
    Header: 'CIR',
    id: 'cir',
    accessor: (row: any) => ({ number: row.cir }),
    Cell: makeLazyCell(PercentCell),
    disableSortBy: true,
  },
  {
    Header: 'CPC',
    id: 'cpc',
    accessor: (row: any) => ({
      number: row.cpc,
      currency: row.currency,
    }),
    Cell: makeLazyCell(PercentCell),
    disableSortBy: true,
  },
  {
    Header: 'Budget',
    id: 'budget',
    accessor: (row: any) => ({
      dailyBudget: row.budgetConfig.valueDaily,
      totalBudget: row.budgetConfig.valueTotal,
      currency: row.currency,
      editor: row.shopAdsStatus !== SHOP_ADS_STATUS.ENDED && (
        <EditorContextPopover
          triggerElem={<EditButton />}
          onFormSubmit={(value) => onInlineSubmitBudget(row, value)}
        >
          <BudgetForm
            currency={row.currency}
            budget={{
              total: row.budgetConfig.valueTotal,
              daily: row.budgetConfig.valueDaily,
              isNoLimit:
                row.budgetConfig.valueDaily &&
                row.budgetConfig.valueDaily,
              isOnDaily: row.budgetConfig.valueDaily,
            }}
          />
        </EditorContextPopover>
      ),
    }),
    Cell: makeLazyCell(BudgetCell),
    disableSortBy: true,
  },
  {
    Header: 'Timeline',
    id: 'timeline',
    accessor: (row: any) => ({
      fromDate: row.timelineFrom,
      toDate: row.timelineTo,
      editor: row.shopAdsStatus !== SHOP_ADS_STATUS.ENDED && (
        <EditorContextPopover
          triggerElem={<EditButton />}
          onFormSubmit={(value) => onInlineSubmitTimeline(row, value)}
        >
          <TimelineForm
            currency={row.currency}
            timeline={{
              startDate: row.timelineFrom,
              endDate: row.timelineTo,
            }}
          />
        </EditorContextPopover>
      ),
    }),
    width: 250,
    disableSortBy: true,
    Cell: makeLazyCell(TimelineCell),
  },
  {
    Header: 'Rule',
    id: 'rule',
    accessor: (row: any) => {
      let label = '';
      if (row.ruleCount) {
        label = row.ruleCount + ' rules';
      } else {
        const allowStatus = [
          SHOP_ADS_STATUS.RUNNING,
          SHOP_ADS_STATUS.PAUSED,
          SHOP_ADS_STATUS.SCHEDULED,
        ];
        if (allowStatus.includes(row.shopAdsStatus)) {
          label = 'Add rule';
        }
      }

      return {
        ...row,
        label,
      };
    },
    Cell: makeLazyCell(ButtonCell),
    disableSortBy: true,
    width: 95,
    alwaysEnable: true,
  },
  {
    Header: '',
    id: 'ruleLog',
    accessor: (row: any) => row,
    Cell: makeLazyCell(LogCell),
    disableSortBy: true,
    width: 70,
    alwaysEnable: true,
  },
];

function TaglineEditor({ row }) {
  return (
    <AdCreativeEditorPopover
      shop={row}
      triggerElem={<EditButton />}
    />
  );
}
