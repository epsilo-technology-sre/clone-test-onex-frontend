import { get } from 'lodash';
import moment from 'moment';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { GlobalFilters } from '../components/global-filters';
import {
  actions,
  GET_SHOPS,
  UPDATE_FILTER_COUNTRY,
  UPDATE_FILTER_DATE_RANGE,
  UPDATE_FILTER_SHOP,
} from '../redux/actions';

export function GlobalFiltersContainer() {
  const dispatch = useDispatch();

  const {
    countries,
    shops,
    selectedTimeRange,
    selectedCountries,
    selectedShops,
  } = useSelector((state: any) => {
    return {
      countries: get(state, 'optionGlobalFilters.countries', []),
      shops: get(state, 'optionGlobalFilters.shops', []),
      selectedCountries: get(state, 'globalFilters.countries', []),
      selectedShops: get(state, 'globalFilters.shops', []),
      selectedTimeRange: get(state, 'globalFilters.timeRange', {}),
    };
  });

  const loadShop = () => dispatch(GET_SHOPS.START({}));

  useEffect(() => {
    loadShop();
  }, []);

  const handleChangeDateRange = ({ startDate, endDate }: any) => {
    dispatch(
      UPDATE_FILTER_DATE_RANGE({
        timeRange: {
          start: moment(startDate).format('YYYY-MM-DD'),
          end: moment(endDate).format('YYYY-MM-DD'),
        },
      }),
    );
  };

  const handleChangeCountries = (selected: any) => {
    dispatch(UPDATE_FILTER_COUNTRY({ countries: selected }));
  };

  const handleChangeShops = (selected: any) => {
    dispatch(UPDATE_FILTER_SHOP({ shops: selected }));
  };

  const handleSubmitChange = () => {
    dispatch(actions.updateGlobalFilter());
  };

  return (
    <GlobalFilters
      countries={countries}
      shops={shops}
      selectedTimeRange={selectedTimeRange}
      selectedCountries={selectedCountries}
      selectedShops={selectedShops}
      onChangeDateRange={handleChangeDateRange}
      onChangeCountries={handleChangeCountries}
      onChangeShops={handleChangeShops}
      onSubmitChange={handleSubmitChange}
    />
  );
}
