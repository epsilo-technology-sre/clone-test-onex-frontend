import { Box } from '@material-ui/core';
import React from 'react';
import { Provider } from 'react-redux';
import { HashRouter as Router } from 'react-router-dom';
import { App } from './App';
import { store } from './redux/store';

export default {
  title: 'Shopee/Main',
};

export const Primary: React.FC<{}> = () => {
  return (
    <Provider store={store}>
      <Box p={2}>
        <Router>
          <App />
        </Router>
      </Box>
    </Provider>
  );
};
