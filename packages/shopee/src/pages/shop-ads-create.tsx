import React from 'react';
import { ThemeProvider } from '@material-ui/core/styles';
import { Provider } from 'react-redux';
import { makeOneStore } from '@ep/one/src/utils/store';
import ShopeeTheme from '../shopee-theme';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { rootSaga } from '../redux/shop-ads-create/actions';
import { reducer } from '../redux/shop-ads-create/reducers';
import { ContainerShopAdsCreate } from '../containers/shop-ads-create/shop-ads-create';

export default function PageShopAds() {
  let store = makeOneStore({
    storeName: 'shopee/shopads/create',
    rootSaga: rootSaga,
    reducer: reducer,
    // initState: demoState(),
  });

  return (
    <Provider store={store}>
      <ThemeProvider theme={ShopeeTheme}>
        <ContainerShopAdsCreate />
        <ToastContainer closeOnClick={false} hideProgressBar={true} />
      </ThemeProvider>
    </Provider>
  );
}

const demoState = () => ({
  currentStep: 'SetupKeywords',
  shopList: [
    {
      shopId: 4135,
      shopName: 'Enfa Official Store',
      shopCurrency: 'VND',
      channelCode: 'SHOPEE',
    },
    {
      shopId: 4134,
      shopName: "L'Oreal Paris",
      shopCurrency: 'PHP',
      channelCode: 'SHOPEE',
    },
    {
      shopId: 3921,
      shopName: 'XYZ Shop',
      shopCurrency: 'VND',
      channelCode: 'SHOPEE',
    },
    {
      shopId: 3920,
      shopName: 'Lysol',
      shopCurrency: 'PHP',
      channelCode: 'SHOPEE',
    },
    {
      shopId: 3820,
      shopName: 'P&G Official Store',
      shopCurrency: 'MYR',
      channelCode: 'SHOPEE',
    },
    {
      shopId: 504,
      shopName: 'Upspring Official Store',
      shopCurrency: 'SGD',
      channelCode: 'SHOPEE',
    },
    {
      shopId: 503,
      shopName: 'Upspring',
      shopCurrency: 'MYR',
      channelCode: 'SHOPEE',
    },
    {
      shopId: 405,
      shopName: 'Enfa Official Shop',
      shopCurrency: 'THB',
      channelCode: 'SHOPEE',
    },
    {
      shopId: 400,
      shopName: 'P&G Official Store',
      shopCurrency: 'IDR',
      channelCode: 'SHOPEE',
    },
  ],
  loading: {
    'SHOPADS/ADD_KEYWORDS/GET_SUGGEST_KEYWORDS': {
      status: false,
    },
    'SHOPADS/GET_SHOP_LIST': {
      status: false,
    },
  },
  setupAdsInfo: {
    shop: 3820,
    adsName: 'Hello 1234',
    currency: 'MYR',
    budget: {
      total: 1000000,
      daily: 1000,
      isNoLimit: true,
      isOnDaily: true,
    },
    timeline: {
      fromDate: '28/12/2020',
      toDate: '31/12/2020',
      isOnNoLimit: false,
    },
  },
  adsCreative: {
    optionCategories: [],
    shopCategory: '',
    tagline: 'Hellow',
    isMainShop: true,
  },
  addKeyword: {
    isShowAddKeywordModal: false,
    products: [],
    suggestKeywords: [],
    keywords: [],
    customKeywords: [],
    keywordBucket: [],
    selectedProducts: [],
    selectedKeywords: [],
    searchText: '',
    pagination: {
      pageSize: 10,
      currentPage: 1,
      total: 0,
    },
  },
  keywordList: [
    {
      keywordId: 2,
      keywordName: 'hello',
      currency: 'MYR',
      suggestBid: 0.2,
      biddingPrice: 0.2,
      matchType: 'broad_match',
      added: false,
    },
    {
      keywordId: 3,
      keywordName: 'xin chao bon110',
      currency: 'MYR',
      suggestBid: 9,
      biddingPrice: 9,
      matchType: 'exact_match',
      added: false,
    },
    {
      keywordId: 4,
      keywordName: 'hello 4110',
      currency: 'MYR',
      suggestBid: 9,
      biddingPrice: 9,
      matchType: 'exact_match',
      added: false,
    },
  ],
});
