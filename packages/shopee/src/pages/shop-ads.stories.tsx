import React from 'react';
import PageShopAds from './shop-ads';

export default {
  title: 'Shopee / Shop Ads page',
};

export const ShopAdsView = () => {
  return <PageShopAds></PageShopAds>;
};
