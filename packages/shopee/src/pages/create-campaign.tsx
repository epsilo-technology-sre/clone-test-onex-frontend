import React, { useEffect, useState } from 'react';
import { Provider, useSelector } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import { rootSaga } from '../redux/create-campaign/actions';
import {
  CreateCampaignState,
  reducer,
} from '../redux/create-campaign/reducers';
import { CreateCampaignContainer } from '../containers/create-campaign/campaign-creating';
import { useOneMenu } from '@ep/one/src/hooks/use-menu';
import {
  Box,
  Divider,
  makeStyles,
  Typography,
} from '@material-ui/core';
import CircleLeftIcon from './circle-left.svg';
import clsx from 'clsx';
import { useHistory, useLocation } from 'react-router';

export default function PageCreateCampaign() {
  const store = makeStore();
  return (
    <Provider store={store}>
      <CreateCampaignContainer />
      <CustomLeftPanel />
    </Provider>
  );
}

function makeStore() {
  const sagaMiddleware = createSagaMiddleware();
  let composeEnhancers = composeWithDevTools({
    name: 'shopee/create-campaign',
  });

  const store = createStore(
    reducer,
    composeEnhancers(applyMiddleware(sagaMiddleware)),
  );

  sagaMiddleware.run(rootSaga);

  return store;
}

function CustomLeftPanel(): JSX.Element {
  const { setCustomLeftPanel } = useOneMenu();
  const { currentScreen } = useSelector(
    ({ currentScreen }: CreateCampaignState) => {
      return { currentScreen };
    },
  );

  useEffect(() => {
    setCustomLeftPanel(
      <LeftPanelContent currentScreen={currentScreen} />,
    );
  }, [currentScreen]);

  return null;
}

function LeftPanelContent(props: { currentScreen: string }) {
  const classes = useStyle();
  const { resetCustomLeftPanel } = useOneMenu();
  const history = useHistory();

  return (
    <div className={classes.root}>
      <Box
        className={classes.backLink}
        marginLeft="10px"
        display="flex"
        alignItems="center"
        onClick={() => {
          history.push('/advertising/shopee/campaign');
          resetCustomLeftPanel();
        }}
      >
        <img src={CircleLeftIcon} style={{ marginRight: '10px' }} />{' '}
        Back to campaign
      </Box>
      <Divider
        style={{ marginTop: '20px', marginBottom: '20px' }}
      ></Divider>
      <p className={classes.smallTitle}>Create Campaign</p>
      <Box
        className={clsx(
          classes.link,
          props.currentScreen === 'setupCampaign' && 'active',
        )}
      >
        <span>Setup campaign info</span>
        <span style={{ color: '#3F4F5C' }}>...</span>
      </Box>
      <Box
        className={clsx(
          classes.link,
          props.currentScreen === 'setupProduct' && 'active',
        )}
      >
        <span>Setup product and keyword</span>
      </Box>
    </div>
  );
}

const useStyle = makeStyles(() => ({
  root: {
    marginTop: '12px',
  },
  backLink: {
    marginLeft: '10px',
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer',
  },
  smallTitle: {
    marginLeft: '10px',
    color: '#253746',
    fontSize: '14px',
    fontWeight: 600,
  },
  link: {
    borderRadius: '4px',
    fontSize: '12px',
    display: 'flex',
    padding: '12px 16px',
    justifyContent: 'space-between',
    color: '#C2C7CB',
    '&.active': {
      background: 'rgba(37, 55, 70, 0.08);',
      '& span': {
        color: '#ED5C10',
      },
    },
  },
}));
