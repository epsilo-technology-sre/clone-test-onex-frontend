import React from 'react';
import { ThemeProvider } from '@material-ui/core/styles';
import { Provider } from 'react-redux';
import { makeOneStore } from '@ep/one/src/utils/store';
import ShopeeTheme from '../shopee-theme';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { rootSaga } from '../redux/shop-ads/actions';
import reducer from '../redux/shop-ads/reducers';
import { ShopAds } from '../containers/shop-ads-container/shop-ads';

export default function PageShopAds() {
  let store = React.useMemo(
    () =>
      makeOneStore({
        storeName: 'shopee/shopads',
        rootSaga: rootSaga,
        reducer: reducer,
      }),
    [],
  );

  return (
    <Provider store={store}>
      <ShopAds></ShopAds>
      <ToastContainer closeOnClick={false} hideProgressBar={true} />
    </Provider>
  );
}
