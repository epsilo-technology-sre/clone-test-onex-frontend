import { ThemeProvider } from '@material-ui/core/styles';
import React from 'react';
import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { CampaignPage } from './pages';
import { store } from './redux/store';
import ShopeeTheme from './shopee-theme';

export const App = () => {
  return (
    <Provider store={store}>
      <ThemeProvider theme={ShopeeTheme}>
        <CampaignPage></CampaignPage>
        <ToastContainer closeOnClick={false} hideProgressBar={true} />
      </ThemeProvider>
    </Provider>
  );
};

export default App;
