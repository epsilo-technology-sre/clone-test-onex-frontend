export const CHANNEL = 'SHOPEE';

export const TABS = {
  CAMPAIGN: 'Campaign',
  PRODUCT: 'Product',
  KEYWORD: 'Keyword',
  SHOPADS: 'ShopAds',
  SHOPADS_KEYWORD: 'ShopAdsKeywords',
};

export const SHOP_ADS_TABS = {
  CAMPAIGN: 'Shop ads',
  KEYWORD: 'Keywords',
};

export const CAMPAIGN_STATUS = {
  RUNNING: 'running',
  PAUSED: 'paused',
  SCHEDULED: 'scheduled',
  SYNCING: 'syncing',
  ENDED: 'ended',
};

export const SHOP_ADS_STATUS = {
  RUNNING: 'running',
  PAUSED: 'paused',
  SCHEDULED: 'scheduled',
  SYNCING: 'syncing',
  ENDED: 'ended',
};

export const SHOP_ADS_KEYWORDS_STATUS = {
  RUNNING: 'running',
  PAUSED: 'paused',
  SCHEDULED: 'scheduled',
  SYNCING: 'syncing',
  ENDED: 'ended',
  DISAPPROVED: 'disapproved',
};

export const PRODUCT_STATUS = {
  RUNNING: 'running',
  PAUSED: 'paused',
  SCHEDULED: 'scheduled',
  SYNCING: 'syncing',
  ENDED: 'ended',
  CLOSED: 'closed',
};

export const SHOP_STATUS = {
  INIT: 'Init',
  GOOD: 'Good',
  WARNING: 'Warning',
  SYNCING: 'Syncing',
  ERROR: 'Error',
  PULLED: 'Pulled',
};

export const KEYWORD_STATUS = {
  RUNNING: 'running',
  PAUSED: 'paused',
  SCHEDULED: 'scheduled',
  SYNCING: 'syncing',
};

export const SKU_STATUS = {
  RUNNING: 'running',
  PAUSED: 'paused',
  SCHEDULED: 'scheduled',
  SYNCING: 'syncing',
  ENDED: 'ended',
};

export const SKU_KEYWORD_STATUS = {
  ACTIVE: 'active',
  SCHEDULED: 'scheduled',
  SYNCING: 'syncing',
  REMOVED: 'removed',
};

export const COLORS = {
  CHART: {
    ORANGE: `#dd7604`,
    BLUE: `#2e8cb8`,
  },
  COMMON: {
    MAIN_RGBA: '51, 63, 80',
    MAIN: '#333f50', //Note: design name is Ink1
    MAIN_OPA_70: 'rgba(51, 63, 80, 0.7)',
    MAIN_OPA_40: 'rgba(51, 63, 80, 0.4)',
    MAIN_OPA_20: 'rgba(51, 63, 80, 0.2)',
    MAIN_OPA_8: 'rgba(51, 63, 80, 0.08)',
    ORANGE_RGBA: '241, 97, 69',
    ORANGE: '#f16145',
    ORANGE_OPA_70: 'rgba(241, 97, 69, 0.7)',
    ORANGE_OPA_40: 'rgba(241, 97, 69, 0.4)',
    ORANGE_OPA_20: 'rgba(241, 97, 69, 0.2)',
    ORANGE_OPA_8: 'rgba(241, 97, 69, 0.08)',
    BLUE_RGBA: '32, 104, 237',
    BLUE: '#2068ED',
    BLUE_OPA_70: 'rgba(32, 104, 237, 0.7)',
    BLUE_OPA_40: 'rgba(32, 104, 237, 0.4)',
    BLUE_OPA_30: 'rgba(32, 104, 237, 0.3)',
    BLUE_OPA_20: 'rgba(32, 104, 237, 0.2)',
    BLUE_OPA_8: 'rgba(32, 104, 237, 0.08)',
    GREEN_RGBA: '58, 167, 109',
    GREEN: '#3AA76D',
    GREEN_OPA_70: 'rgba(58, 167, 109, 0.7)',
    GREEN_OPA_40: 'rgba(58, 167, 109,0.4)',
    GREEN_OPA_20: 'rgba(58, 167, 109, 0.2)',
    GREEN_OPA_8: 'rgba(58, 167, 109, 0.08)',
    RED_RGBA: '205, 50, 73',
    RED: '#CD3249',
    RED_OPA_70: 'rgba(205, 50, 73, 0.7)',
    RED_OPA_40: 'rgba(205, 50, 73, 0.4)',
    RED_OPA_20: 'rgba(205, 50, 73, 0.2)',
    RED_OPA_8: 'rgba(205, 50, 73, 0.08)',
    GRAY_RGBA: '246, 247, 248',
    GRAY: '#F6F7F8',
    GRAY_OPA_70: 'rgba(246, 247, 248, 0.7)',
    GRAY_OPA_40: 'rgba(246, 247, 248, 0.4)',
    GRAY_OPA_20: 'rgba(246, 247, 248, 0.2)',
    GRAY_OPA_8: 'rgba(246, 247, 248, 0.08)',
    WHITE_RGBA: '255, 255, 255',
    WHITE: '#FFFFFF',
    WHITE_OPA_70: 'rgba(255, 255, 255, 0.7)',
    WHITE_OPA_40: 'rgba(255, 255, 255, 0.4)',
    WHITE_OPA_20: 'rgba(255, 255, 255, 0.2)',
    WHITE_OPA_8: 'rgba(255, 255, 255, 0.08)',
    BG_LIGHT: '#F8FAFD',
    BG_DARK: '#F2F3F4',
  },
};

export const DATE_FORMAT = 'DD/MM/YYYY';

export const PRODUCT_DAILY_BUDGET = {
  VND: { MIN: 5000 },
  SGD: { MIN: 2 },
  PHP: { MIN: 20 },
  IDR: { MIN: 2500 },
  MYR: { MIN: 2 },
  TWD: { MIN: 1 },
  USD: { MIN: 1 },
  THB: { MIN: 20 },
};

export const PRODUCT_TOTAL_BUDGET = {
  VND: { MIN: 50000 },
  SGD: { MIN: 20 },
  PHP: { MIN: 200 },
  IDR: { MIN: 38700.86 },
  MYR: { MIN: 222.87 },
  TWD: { MIN: 1 },
  USD: { MIN: 1 },
  THB: { MIN: 244.1 },
};

export const SHOP_ADS_DAILY_BUDGET = {
  VND: { MIN: 20000 },
  SGD: { MIN: 2.5 },
  PHP: { MIN: 25 },
  IDR: { MIN: 5000 },
  MYR: { MIN: 4 },
  TWD: { MIN: 0 },
  USD: { MIN: 0 },
  THB: { MIN: 20 },
};

export const SHOP_ADS_TOTAL_BUDGET = {
  VND: { MIN: 200000 },
  SGD: { MIN: 25 },
  PHP: { MIN: 250 },
  IDR: { MIN: 50000 },
  MYR: { MIN: 40 },
  TWD: { MIN: 0 },
  USD: { MIN: 0 },
  THB: { MIN: 100 },
};

export const BUDGET_TYPE = {
  DAILY: 'daily_budget',
  TOTAL: 'total_budget',
};

export const MATCH_TYPE = {
  EXACT_MATCH: 'exact_match',
  BROAD_MATCH: 'broad_match',
};

export const MATCH_TYPE_LABEL = {
  exact_match: 'Exact Match',
  broad_match: 'Broad Match',
};

export const MAX_KEYWORDS_OF_PRODUCT = 200;

export const NO_MATCH_TYPE_LIMITATION = {
  VND: {
    MIN: 400,
    MAX: 330000,
  },
  MYR: {
    MIN: 0.06,
    MAX: 60,
  },
  THB: {
    MIN: 1,
    MAX: 450,
  },
  IDR: {
    MIN: 150,
    MAX: 200000,
  },
  SGD: {
    MIN: 0.05,
    MAX: 20,
  },
  PHP: {
    MIN: 1,
    MAX: 750,
  },
};

export const CURRENCY_LIMITATION = {
  VND: {
    BROAD_MATCH: {
      MIN: 480,
      MAX: 30000,
    },
    EXACT_MATCH: {
      MIN: 400,
      MAX: 30000,
    },
    DAILY_BUDGET: {
      MIN: 5000,
      MAX: 0,
    },
    TOTAL_BUDGET: {
      MIN: 50000,
      MAX: 0,
    },
  },
  MYR: {
    BROAD_MATCH: {
      MIN: 0.07,
      MAX: 10,
    },
    EXACT_MATCH: {
      MIN: 0.06,
      MAX: 10,
    },
    DAILY_BUDGET: {
      MIN: 2,
      MAX: 0,
    },
    TOTAL_BUDGET: {
      MIN: 222.87,
      MAX: 0,
    },
  },
  THB: {
    BROAD_MATCH: {
      MIN: 1.2,
      MAX: 100,
    },
    EXACT_MATCH: {
      MIN: 1,
      MAX: 100,
    },
    DAILY_BUDGET: {
      MIN: 20,
      MAX: 0,
    },
    TOTAL_BUDGET: {
      MIN: 2444.1,
      MAX: 0,
    },
  },
  IDR: {
    BROAD_MATCH: {
      MIN: 180,
      MAX: 12000,
    },
    EXACT_MATCH: {
      MIN: 150,
      MAX: 12000,
    },
    DAILY_BUDGET: {
      MIN: 2500,
      MAX: 0,
    },
    TOTAL_BUDGET: {
      MIN: 38700.86,
      MAX: 0,
    },
  },
  SGD: {
    BROAD_MATCH: {
      MIN: 0.05,
      MAX: 10,
    },
    EXACT_MATCH: {
      MIN: 0.04,
      MAX: 10,
    },
    DAILY_BUDGET: {
      MIN: 2,
      MAX: 0,
    },
    TOTAL_BUDGET: {
      MIN: 20,
      MAX: 0,
    },
  },
  PHP: {
    BROAD_MATCH: {
      MIN: 0.48,
      MAX: 80,
    },
    EXACT_MATCH: {
      MIN: 0.43,
      MAX: 80,
    },
    DAILY_BUDGET: {
      MIN: 20,
      MAX: 0,
    },
    TOTAL_BUDGET: {
      MIN: 200,
      MAX: 0,
    },
  },
};

export const SHOP_ADS_CURRENCY_LIMIT = {
  VND: {
    BROAD_MATCH: {
      MIN: 600,
      MAX: 1200000000000000,
    },
    EXACT_MATCH: {
      MIN: 500,
      MAX: 1000000000,
    },
  },
  MYR: {
    BROAD_MATCH: {
      MIN: 0.1,
      MAX: 1200000000000000,
    },
    EXACT_MATCH: {
      MIN: 0.08,
      MAX: 1000000000,
    },
  },
  THB: {
    BROAD_MATCH: {
      MIN: 1.6,
      MAX: 1200000000000000,
    },
    EXACT_MATCH: {
      MIN: 1.3,
      MAX: 1000000000,
    },
  },
  IDR: {
    BROAD_MATCH: {
      MIN: 240,
      MAX: 6000,
    },
    EXACT_MATCH: {
      MIN: 200,
      MAX: 5000,
    },
  },
  SGD: {
    BROAD_MATCH: {
      MIN: 0.1,
      MAX: 1200000000000000,
    },
    EXACT_MATCH: {
      MIN: 0.1,
      MAX: 1000000000,
    },
  },
  PHP: {
    BROAD_MATCH: {
      MIN: 0.6,
      MAX: 1200000000000000,
    },
    EXACT_MATCH: {
      MIN: 0.5,
      MAX: 1000000000,
    },
  },
};

export const RULE_PERIODS = [
  { name: 'Current day', value: 1 },
  { name: 'Last 2 Days + current day', value: 3 },
  { name: 'Last 6 Days + current day', value: 7 },
  { name: 'Last 13 Days + current day', value: 14 },
  { name: 'Last 29 Days + current day', value: 30 },
];

export const RULE_LOG_AFTER_DAYS = 31;

export const LOCAL_STORAGE_KEY = {
  ASSESSMENT_SHOPS: 'assessment_shops',
  ASSESSMENT_KEYWORDS: 'assessment_keywords',
};
