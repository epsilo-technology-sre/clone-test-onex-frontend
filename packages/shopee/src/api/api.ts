import { API_URL } from '@ep/one/global';
import { download } from '@ep/one/src/utils/fetch';
import { get, post, put } from './fetch';

const URL = {
  GET_CAMPAIGNS: `${API_URL}/api/v1/shopee/kwbidding/campaigns`,
  GET_PRODUCTS: `${API_URL}/api/v1/shopee/kwbidding/skus`,
  GET_KEYWORDS: `${API_URL}/api/v1/shopee/kwbidding/keywords`,
  GET_SHOPS: `${API_URL}/api/v1/user/shops`,
  GET_CATEGORIES: `${API_URL}/api/v1/common/categories`,
  DOWNLOAD_FILE: `${API_URL}/api/v1/shopee/kwbidding/export`,

  CHECK_EXIST_CAMPAIGN_NAME: `${API_URL}/api/v1/shopee/kwbidding/check-campaign-name-is-exist`,
  UPDATE_CAMPAIGN_NAME: `${API_URL}/api/v1/shopee/kwbidding/edit-campaign-name`,
  UPDATE_CAMPAIGN_BUDGET: `${API_URL}/api/v1/shopee/kwbidding/edit-campaign-budget`,
  UPDATE_CAMPAIGN_STATUS: `${API_URL}/api/v1/shopee/kwbidding/edit-campaign-status`,
  UPDATE_CAMPAIGN_TIMELINE: `${API_URL}/api/v1/shopee/kwbidding/edit-campaign-time-line`,
  UPDATE_PRODUCT_BUDGET: `${API_URL}/api/v1/shopee/kwbidding/edit-sku-budget`,
  ACTIVATOR_CAMPAIGNS: `${API_URL}/api/v1/shopee/kwbidding/edit-campaign-status`,
  ACTIVATOR_PRODUCTS: `${API_URL}/api/v1/shopee/kwbidding/edit-sku-status`,
  ACTIVATOR_KEYWORDS: `${API_URL}/api/v1/shopee/kwbidding/edit-keyword-status`,
  GET_SUGGEST_BIDDING_PRICE: `${API_URL}/api/v1/shopee/kwbidding/suggest-bidding-price`,

  // create campaign

  GET_SUGGEST_KEYWORDS: `${API_URL}/api/v1/shopee/kwbidding/suggest-keywords`,
  GET_CATEGORY_LIST: `${API_URL}/api/v1/shopee/common/categories`,
  GET_SHOP_PRODUCTS: `${API_URL}/api/v1/shopee/common/products-of-shop`,
  CREATE_CAMPAIGN: `${API_URL}/api/v1/shopee/kwbidding/create-campaign`,
  ADD_KEYWORDS: `${API_URL}/api/v1/shopee/kwbidding/add-keywords`,

  UPDATE_KEYWORDS_PRICE: `${API_URL}/api/v1/shopee/kwbidding/edit-keyword-bidding-price`,
  UPDATE_PRODUCTS_PRICE: `${API_URL}/api/v1/shopee/kwbidding/edit-sku-budget`,
  UPDATE_CAMPAIGNS_PRICE: `${API_URL}/api/v1/shopee/kwbidding/edit-campaign-budget`,

  // rules
  RULE_GET_ACTION_METRIC: `${API_URL}/api/v1/programmatic/rule/list-metric`,
  RULE_COUNTING: `${API_URL}/api/v1/programmatic/rule/counting`,
  RULE_GET_LIST_RULE: `${API_URL}/api/v1/programmatic/rule/list`,
  RULE_GET_LIST_RULE_LOG: `${API_URL}/api/v1/programmatic/rule/list-log`,

  RULE_EDIT: `${API_URL}/api/v1/shopee/kwbidding/rule/modify`,
  RULE_CREATE: `${API_URL}/api/v1/shopee/kwbidding/rule/create`,
  ADD_EXISTING_RULE: `${API_URL}/api/v1/shopee/kwbidding/rule/add-exist-rule-to-object`,
  RULE_DELETE: `${API_URL}/api/v1/shopee/kwbidding/rule/delete`,
  RULE_DELETE_ALL: `${API_URL}/api/v1/shopee/kwbidding/rule/delete-all`,

  SHOPADS_RULE_EDIT: `${API_URL}/api/v1/shopee/shopads/rule/modify`,
  SHOPADS_RULE_CREATE: `${API_URL}/api/v1/shopee/shopads/rule/create`,
  SHOPADS_ADD_EXISTING_RULE: `${API_URL}/api/v1/shopee/shopads/rule/add-exist-rule-to-object`,
  SHOPADS_RULE_DELETE: `${API_URL}/api/v1/shopee/shopads/rule/delete`,
  SHOPADS_RULE_DELETE_ALL: `${API_URL}/api/v1/shopee/shopads/rule/delete-all`,

  ACTIVATOR_SHOPS: `${API_URL}/api/v1/programmatic/rule/list-metric`,
  UPDATE_SHOP_BUDGET: `${API_URL}/api/v1/programmatic/rule/list-metric`,
  UPDATE_SHOP_NAME: `${API_URL}/api/v1/programmatic/rule/list-metric`,
  UPDATE_SHOP_PRICE: `${API_URL}/api/v1/programmatic/rule/list-metric`,
  UPDATE_SHOP_TIMELINE: `${API_URL}/api/v1/programmatic/rule/list-metric`,

  // SHOP ADS
  SHOPADS_GET_SHOPADS: `${API_URL}/api/v1/shopee/shopads/list-shopads`,
  SHOPADS_EDIT_SHOPADS_STATUS: `${API_URL}/api/v1/shopee/shopads/edit-shopads-status`,
  SHOPADS_DOWNLOAD_FILE: `${API_URL}/api/v1/shopee/shopads/export`,

  SHOPADS_GET_SHOPADS_KEYWORDS: `${API_URL}/api/v1/shopee/shopads/list-shopads-keyword`,
  SHOPADS_EDIT_KEYWORD_STATUS: `${API_URL}/api/v1/shopee/shopads/edit-shopads-keyword-status`,
  SHOPADS_EDIT_KEYWORD_BIDDING_PRICE: `${API_URL}/api/v1/shopee/shopads/edit-shopads-keyword-bidding-price`,
  SHOPADS_UPDATE_TAGLINE: `${API_URL}/api/v1/shopee/shopads/edit-shopads-ad-creative`,
  SHOPADS_UPDATE_BUDGET: `${API_URL}/api/v1/shopee/shopads/edit-shopads-budget`,
  SHOPADS_UPDATE_TIMELINE: `${API_URL}/api/v1/shopee/shopads/edit-shopads-timeline`,
  SHOPADS_UPDATE_AD_STATUS: `${API_URL}/api/v1/shopee/shopads/edit-shopads-status`,
  SHOPADS_GET_SHOP_CATEGORY: `${API_URL}/api/v1/shopee/shopads/list-shop-category`,
  SHOPADS_CREATE_NEW: `${API_URL}/api/v1/shopee/shopads/create-shopads`,
  SHOPADS_ADS_ADD_KEYWORDS: `${API_URL}/api/v1/shopee/shopads/add-keyword-to-shopads`,
  SHOPADS_GET_SUGGESTED_KEYWORDS: `${API_URL}/api/v1/shopee/shopads/list-shopads-suggest-keyword`,
  SHOPADS_GET_SUGGEST_BIDDING_PRICE: `${API_URL}/api/v1/shopee/shopads/suggest-bidding-price`,
  SHOPADS_ADS_CHECK_KEYWORDS_VALID: `${API_URL}/api/v1/shopee/shopads/list-shopads-suggest-keyword-valid`,
};
export { URL as EP };

// getShops, getCategories // mock data
export const getShops = async (params: any = null) =>
  await get(URL.GET_SHOPS, params);

export const getCategories = async (params: any = null) =>
  await get(URL.GET_CATEGORIES, params);

export const createCampaign = async (params: any = null) =>
  await post(URL.CREATE_CAMPAIGN, params);

export const addKeywords = async (params: any = null) =>
  await post(URL.ADD_KEYWORDS, params);

export const getSuggestKeywords = async (params: any = null) =>
  await get(URL.GET_SUGGEST_KEYWORDS, params);

export const getCampaigns = async (params: any = null) =>
  await get(URL.GET_CAMPAIGNS, params);

export const getProducts = async (params: any = null) =>
  await get(URL.GET_PRODUCTS, params);

export const getKeywords = async (params: any = null) =>
  await post(URL.GET_KEYWORDS, params);

export const getRuleCounting = async (params: any = null) =>
  await get(URL.RULE_COUNTING, params);

export const checkExistCampaignName = async (params: any = null) =>
  await get(URL.CHECK_EXIST_CAMPAIGN_NAME, params);

export const updateCampaignName = async (params: any = null) =>
  await put(URL.UPDATE_CAMPAIGN_NAME, params);

export const updateCampaignBudget = async (params: any = null) =>
  await put(URL.UPDATE_CAMPAIGN_BUDGET, params);

export const updateCampaignTimeline = async (params: any = null) =>
  await put(URL.UPDATE_CAMPAIGN_TIMELINE, params);

export const updateProductBudget = async (params: any = null) =>
  await put(URL.UPDATE_PRODUCT_BUDGET, params);

export const activatorKeywords = async (params: any = null) =>
  await put(URL.ACTIVATOR_KEYWORDS, params);

export const activatorProducts = async (params: any = null) =>
  await put(URL.ACTIVATOR_PRODUCTS, params);

export const activatorCampaigns = async (params: any = null) =>
  await put(URL.ACTIVATOR_CAMPAIGNS, params);

export const getSuggestBiddingPrice = async (params: any = null) =>
  await get(URL.GET_SUGGEST_BIDDING_PRICE, params);

export const getShopAdsSuggestBiddingPrice = async (
  params: any = null,
) => await get(URL.SHOPADS_GET_SUGGEST_BIDDING_PRICE, params);

export const updateKeywordsPrice = async (params: any = null) =>
  await put(URL.UPDATE_KEYWORDS_PRICE, params);

export const updateProductsPrice = async (params: any = null) =>
  await put(URL.UPDATE_PRODUCTS_PRICE, params);

export const updateCampaignPrice = async (params: any = null) =>
  await put(URL.UPDATE_CAMPAIGNS_PRICE, params);

export const activatorShops = async (params: any = null) =>
  await put(URL.ACTIVATOR_SHOPS, params);
export const updateShopBudget = async (params: any = null) =>
  await put(URL.UPDATE_SHOP_BUDGET, params);
export const updateShopName = async (params: any = null) =>
  await put(URL.UPDATE_SHOP_NAME, params);
export const updateShopPrice = async (params: any = null) =>
  await put(URL.UPDATE_SHOP_PRICE, params);
export const updateShopTimeline = async (params: any = null) =>
  await put(URL.UPDATE_SHOP_TIMELINE, params);

export const updateShopAdsKeywordStatus = async (
  params: any = null,
) => await put(URL.SHOPADS_EDIT_KEYWORD_STATUS, params);
export const updateShopAdsKeywordPrice = async (params: any = null) =>
  await put(URL.SHOPADS_EDIT_KEYWORD_BIDDING_PRICE, params);

export const downloadFile = async (params: any) => {
  return download(URL.DOWNLOAD_FILE, params);
};

export const downloadShopAdsFile = async (params: any) => {
  return download(URL.SHOPADS_DOWNLOAD_FILE, params);
};
