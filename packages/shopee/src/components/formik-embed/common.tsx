import React from 'react';
import {
  Button,
  Select,
  makeStyles,
  OutlinedInput,
  withStyles,
} from '@material-ui/core';
import { COLORS } from '@ep/shopee/src/constants';

export const InputUI = withStyles({
  root: {
    fontSize: 14,
    '& fieldset': {
      borderColor: '#C2C7CB',
      borderWidth: 2,
    },
    '&.Mui-focused fieldset': {
      borderColor: '#485764!important',
    },
  },
  input: {
    paddingTop: 8,
    paddingBottom: 8,
  },
})(OutlinedInput);

export const useStyle = makeStyles(() => ({
  root: {
    width: 480,
    margin: '0 auto',
  },
  logo: {
    height: 40,
  },
  title: {
    fontSize: 35,
  },
  row: {
    marginBottom: 24,
    width: '100%',
  },
  form: {},
  link: {
    color: '#ED5C10',
    '&:hover': {
      color: '#ED5C10',
    },
  },
  linkGroup: {
    textAlign: 'center',
    color: '#ED5C10',
  },
  label: {
    color: '#596772',
    fontSize: 11,
    fontWeight: 'bold',
    padding: '5px 0',
  },
  error: {
    color: COLORS.COMMON.RED,
    fontSize: 12,
  },
  switcher: {
    display: 'flex',
    alignItems: 'center',
    padding: '8px 0',
    '& img': {
      cursor: 'pointer',
      marginRight: 8,
    },
  },
  selectWrapper: {
    background: '#F6F7F8',
    borderRadius: 4,
  },
  body: {
    padding: 8,
  },
  footer: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: 8,
    borderTop: '2px solid #E4E7E9',
    '& button': {
      marginLeft: 5,
    },
  },
}));
