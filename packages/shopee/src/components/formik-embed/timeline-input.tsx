import { DATE_FORMAT } from '@ep/shopee/src/constants';
import SwitchOffIcon from '@ep/shopee/src/images/switch-off.svg';
import SwitchOnIcon from '@ep/shopee/src/images/switch-on.svg';
import { Box, Typography } from '@material-ui/core';
import { Field, useFormikContext } from 'formik';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { SingleDatePickerUI } from '../common/single-date-picker';
import { useStyle } from './common';

export const FormikTimelineInput = (props: any) => {
  const classes = useStyle();
  const formik = useFormikContext();

  const [minEndDate, setMinEndDate] = useState(null);

  useEffect(() => {
    const today = moment();
    const minDate =
      today.diff(
        moment(formik.values.timeline.startDate, DATE_FORMAT),
      ) > 0
        ? today.format(DATE_FORMAT)
        : formik.values.timeline.startDate;
    setMinEndDate(minDate);
  }, [formik.values.timeline.startDate]);

  const handleChangeStartDate = (date: any) => {
    formik.setFieldValue('timeline.startDate', date);

    if (
      moment(date, DATE_FORMAT).diff(
        moment(formik.values.timeline.endDate, DATE_FORMAT),
      ) > 0
    ) {
      formik.setFieldValue('timeline.endDate', date);
    }
  };

  const handleChangeEndDate = (date: any) => {
    formik.setFieldValue('timeline.endDate', date);
  };

  const handleChangeNoLimit = () => {
    formik.setFieldValue('timeline.endDate', minEndDate);
    formik.setFieldValue(
      'timeline.isOnNoLimit',
      !formik.values.timeline?.isOnNoLimit,
    );
  };

  return (
    <>
      <Box>
        <Box pb={1}>
          <Typography variant="body2" className={classes.label}>
            From date
          </Typography>
          <Field name="timeline.startDate">
            {({ field }: any) => {
              return (
                <SingleDatePickerUI
                  date={field.value}
                  minDate={moment().format(DATE_FORMAT)}
                  onChange={handleChangeStartDate}
                />
              );
            }}
          </Field>
          {formik.errors.timeline?.startDate ? (
            <Box className={classes.error}>
              {formik.errors.timeline?.startDate}
            </Box>
          ) : null}
        </Box>
        <Box>
          <Typography variant="body2" className={classes.label}>
            To date
          </Typography>
          <Field name="timeline.endDate">
            {({ field }: any) => {
              return (
                <SingleDatePickerUI
                  date={field.value || formik.values.timeline?.startDate}
                  disabled={formik.values.timeline?.isOnNoLimit}
                  minDate={minEndDate}
                  onChange={handleChangeEndDate}
                />
              );
            }}
          </Field>
          {formik.errors.timeline?.endDate ? (
            <Box className={classes.error}>
              {formik.errors.timeline?.endDate}
            </Box>
          ) : null}
        </Box>
        <Box className={classes.switcher}>
          <img
            src={
              formik.values.timeline?.isOnNoLimit
                ? SwitchOnIcon
                : SwitchOffIcon
            }
            height={16}
            width={30}
            onClick={handleChangeNoLimit}
          />
          <Typography variant="body2" component="span">
            No limit
          </Typography>
        </Box>
      </Box>
    </>
  );
};
