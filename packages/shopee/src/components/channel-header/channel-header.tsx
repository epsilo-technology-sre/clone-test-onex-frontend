import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import { useHistory } from 'react-router';
import { ButtonUI } from '../common/button';

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(4),
  },
  title: {
    fontWeight: 500,
    color: '#253746',
  },
}));

export const ChannelHeaderUI = (props: {
  title: string;
  calloutButtonText: string;
  calloutLink: string;
}) => {
  const classes = useStyles();
  const history = useHistory();

  const {
    title = 'Keyword Bidding',
    calloutButtonText = 'Create Campaigns',
    calloutLink = '/advertising/shopee/create-campaign',
  } = props;

  return (
    <Grid
      container
      justify="space-between"
      alignItems="center"
      className={classes.root}
    >
      <Grid item>
        <Typography variant="h5" className={classes.title}>
          {title}
        </Typography>
      </Grid>
      <Grid item>
        <ButtonUI
          label={calloutButtonText}
          variant="contained"
          onClick={() => {
            history.push(calloutLink);
          }}
        />
      </Grid>
    </Grid>
  );
};

ChannelHeaderUI.defaultProps = {};
