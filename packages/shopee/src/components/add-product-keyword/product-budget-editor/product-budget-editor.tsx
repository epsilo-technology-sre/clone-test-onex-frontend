import {
  BUDGET_TYPE,
  COLORS,
  CURRENCY_LIMITATION,
} from '@ep/shopee/src/constants';
import SwitchOffIcon from '@ep/shopee/src/images/switch-off.svg';
import SwitchOnIcon from '@ep/shopee/src/images/switch-on.svg';
import {
  FormHelperText,
  InputAdornment,
  MenuItem,
  OutlinedInput,
} from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import InputBase from '@material-ui/core/InputBase';
import Select from '@material-ui/core/Select';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import React from 'react';
import * as Yup from 'yup';
import { ButtonTextUI, ButtonUI } from '../../common/button';
import { CurrencyCode } from '../../common/currency-code';

const useStyles = makeStyles({
  body: {
    padding: 8,
  },
  title: {
    color: '#596772',
    fontSize: 11,
    fontWeight: 'bold',
    padding: '5px 0',
  },
  switcher: {
    display: 'flex',
    alignItems: 'center',
    padding: '8px 0',
    '& img': {
      cursor: 'pointer',
      marginRight: 8,
    },
  },
  error: {
    color: COLORS.COMMON.RED,
    fontSize: 12,
  },
  buttons: {
    display: 'flex',
    // justifyContent: 'flex-end',
    paddingTop: 8,
  },
});

export const InputUI = withStyles({
  root: {
    fontSize: 14,
    '& fieldset': {
      borderColor: '#C2C7CB',
      borderWidth: 2,
    },
    '&.Mui-focused fieldset': {
      borderColor: '#485764!important',
    },
  },
  input: {
    paddingTop: 8,
    paddingBottom: 8,
  },
})(OutlinedInput);

const BootstrapInput = withStyles({
  root: {
    width: '100%',
  },
  disabled: {
    color: '#9FA7AE !important',
    background: '#F6F7F8 !important',
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: '#ffffff',
    border: '2px solid #D3D7DA',
    fontSize: 14,
    color: '#253746',
    padding: '5px 26px 5px 8px',
  },
})(InputBase);

export const ProductBudgetEditor = (props: any) => {
  const classes = useStyles();
  const {
    currency = 'VND',
    title,
    hideTotal = false,
    hideDaily = false,
    disabled,
    onCancel,
    onSubmit,
  } = props;

  const validationSchema = React.useMemo(() => {
    let dailyBudget = 0;
    let totalBudget = 0;
    if (CURRENCY_LIMITATION[currency]) {
      dailyBudget = CURRENCY_LIMITATION[currency].DAILY_BUDGET;
      totalBudget = CURRENCY_LIMITATION[currency].TOTAL_BUDGET;
    }

    return Yup.object().shape({
      budget: Yup.number()
        .when('isNoLimit', {
          is: false,
          then: Yup.number().required('Please enter Budget'),
        })
        .when(
          ['isNoLimit', 'budgetType'],
          (isNoLimit, budgetType, schema) => {
            if (isNoLimit) {
              return schema;
            } else {
              const minValue =
                budgetType === BUDGET_TYPE.DAILY
                  ? dailyBudget.MIN
                  : totalBudget.MIN;
              return Yup.number()
                .required('Please enter Budget')
                .min(
                  minValue,
                  `Budget cannot be lower than ${minValue}`,
                );
            }
          },
        ),
    });
  }, [currency]);

  let defaultBudget = 200;
  const currencyLitmit = CURRENCY_LIMITATION[currency];
  if (currencyLitmit) {
    defaultBudget = hideDaily
      ? currencyLitmit.TOTAL_BUDGET.MIN
      : currencyLitmit.DAILY_BUDGET.MIN;
  }

  const initialValues = {
    isNoLimit: true,
    budgetType: hideDaily ? BUDGET_TYPE.TOTAL : BUDGET_TYPE.DAILY,
    budget: defaultBudget,
  };

  return (
    <Formik
      validationSchema={validationSchema}
      initialValues={initialValues}
      onSubmit={onSubmit}
      validateOnBlur={true}
      enableReinitialize={true}
    >
      {({ errors, values, setFieldValue, handleSubmit }: any) => {
        return (
          <Form onSubmit={handleSubmit}>
            <Box className={classes.body}>
              {title && (
                <Typography variant="subtitle1">{title}</Typography>
              )}
              <Box className={classes.switcher}>
                {disabled ? (
                  <img src={SwitchOffIcon} height={16} width={30} />
                ) : (
                  <img
                    src={
                      values.isNoLimit ? SwitchOnIcon : SwitchOffIcon
                    }
                    height={16}
                    width={30}
                    onClick={() =>
                      setFieldValue('isNoLimit', !values.isNoLimit)
                    }
                  />
                )}

                <Typography variant="body2" component="span">
                  No limit
                </Typography>
              </Box>
              <Box>
                <Grid container spacing={1}>
                  <Grid item xs={5}>
                    <Field name="budgetType">
                      {({ field }: any) => {
                        return (
                          <Select
                            id="demo-customized-select-native"
                            disabled={values.isNoLimit}
                            {...field}
                            input={<BootstrapInput />}
                          >
                            {!hideDaily && (
                              <MenuItem value={BUDGET_TYPE.DAILY}>
                                Daily budget
                              </MenuItem>
                            )}
                            {!hideTotal && (
                              <MenuItem value={BUDGET_TYPE.TOTAL}>
                                Total budget
                              </MenuItem>
                            )}
                          </Select>
                        );
                      }}
                    </Field>
                  </Grid>
                  <Grid item xs={7}>
                    <Field name="budget">
                      {({ field }: any) => {
                        return (
                          <InputUI
                            id="daily-budget"
                            type="number"
                            fullWidth
                            disabled={values.isNoLimit}
                            {...field}
                            startAdornment={
                              <InputAdornment position="start">
                                <CurrencyCode currency={currency} />
                              </InputAdornment>
                            }
                            error={!!errors.budget}
                            autoComplete="off"
                          ></InputUI>
                        );
                      }}
                    </Field>
                    <ErrorMessage
                      component={FormHelperText}
                      name="budget"
                      className={classes.error}
                    />
                  </Grid>
                </Grid>
              </Box>
              <Box className={classes.buttons}>
                {onCancel && (
                  <ButtonTextUI
                    label="Cancel"
                    size="small"
                    colortext="#000"
                    onClick={onCancel}
                  />
                )}
                <ButtonUI
                  disabled={disabled}
                  label="Apply"
                  size="small"
                  colorButton={COLORS.COMMON.GRAY}
                  variant="contained"
                  type={'submit'}
                />
              </Box>
            </Box>
          </Form>
        );
      }}
    </Formik>
  );
};
