import React from 'react';
import { ProductBudgetEditor } from './product-budget-editor';

export default {
  title: 'Shopee / Add-Product-Keyword',
};

export const ProductBudgetEditorExample = () => {
  const handleSubmit = (value: any) => {
    console.log('Submit valid data', value);
  };

  const handleCancel = () => {
    console.log('Cancel modify product budget');
  };

  return (
    <ProductBudgetEditor
      title="Modify budget"
      onCancel={handleCancel}
      onSubmit={handleSubmit}
    ></ProductBudgetEditor>
  );
};
