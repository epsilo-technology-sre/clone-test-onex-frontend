import { Box } from '@material-ui/core';
import React, { useState } from 'react';
import { KeywordTable } from './keyword-table';

export default {
  title: 'Shopee / Add-Product-Keyword',
};

const keywords = [
  {
    keywordId: 1,
    keywordName: 'Keyword 1',
    searchVolume: 2000,
    currency: 'VND',
    suggestBid: 10000,
    biddingPrice: 10000,
    matchType: 'Exact Match',
  },
  {
    keywordId: 2,
    keywordName: 'Keyword 2',
    searchVolume: 2000,
    currency: 'VND',
    suggestBid: 10000,
    biddingPrice: 10000,
    matchType: 'Exact Match',
  },
  {
    keywordId: 3,
    keywordName: 'Keyword 3',
    searchVolume: 2000,
    currency: 'VND',
    suggestBid: 10000,
    biddingPrice: 10000,
    matchType: 'Exact Match',
  },
  {
    keywordId: 4,
    keywordName: 'Keyword 4',
    searchVolume: 2000,
    currency: 'VND',
    suggestBid: 10000,
    biddingPrice: 10000,
    matchType: 'Exact Match',
  },
  {
    keywordId: 5,
    keywordName: 'Keyword 5',
    searchVolume: 2000,
    currency: 'VND',
    suggestBid: 10000,
    biddingPrice: 10000,
    matchType: 'Exact Match',
  },
  {
    keywordId: 6,
    keywordName: 'Keyword 6',
    searchVolume: 2000,
    currency: 'VND',
    suggestBid: 10000,
    biddingPrice: 10000,
    matchType: 'Exact Match',
  },
  {
    keywordId: 7,
    keywordName: 'Keyword 7',
    searchVolume: 2000,
    currency: 'VND',
    suggestBid: 10000,
    biddingPrice: 10000,
    matchType: 'Exact Match',
  },
  {
    keywordId: 8,
    keywordName: 'Keyword 8',
    searchVolume: 2000,
    currency: 'VND',
    suggestBid: 10000,
    biddingPrice: 10000,
    matchType: 'Exact Match',
  },
  {
    keywordId: 9,
    keywordName: 'Keyword 9',
    searchVolume: 2000,
    currency: 'VND',
    suggestBid: 10000,
    biddingPrice: 10000,
    matchType: 'Exact Match',
  },
  {
    keywordId: 10,
    keywordName: 'Keyword 10',
    searchVolume: 2000,
    currency: 'VND',
    suggestBid: 10000,
    biddingPrice: 10000,
    matchType: 'Exact Match',
  },
  {
    keywordId: 11,
    keywordName: 'Keyword 11',
    searchVolume: 2000,
    currency: 'VND',
    suggestBid: 10000,
    biddingPrice: 10000,
    matchType: 'Exact Match',
  },
  {
    keywordId: 12,
    keywordName: 'Keyword 12',
    searchVolume: 2000,
    currency: 'VND',
    suggestBid: 10000,
    biddingPrice: 10000,
    matchType: 'Exact Match',
  },
  {
    keywordId: 13,
    keywordName: 'Keyword 13',
    searchVolume: 2000,
    currency: 'VND',
    suggestBid: 10000,
    biddingPrice: 10000,
    matchType: 'Exact Match',
  },
  {
    keywordId: 14,
    keywordName: 'Keyword 14',
    searchVolume: 2000,
    currency: 'VND',
    suggestBid: 10000,
    biddingPrice: 10000,
    matchType: 'Exact Match',
  },
  {
    keywordId: 15,
    keywordName: 'Keyword 15',
    searchVolume: 2000,
    currency: 'VND',
    suggestBid: 10000,
    biddingPrice: 10000,
    matchType: 'Exact Match',
  },
];

export const KeywordTableDemo = (props: any) => {
  const [rows, setRows] = useState(keywords);
  const [selectedIds, setSelectedIds] = useState([]);
  const [pageSize, setPageSize] = useState(10);
  const [currentPage, setCurrentPage] = useState(1);
  const [resultTotal, setResultTotal] = useState(keywords.length);
  const [sortBy, setSortBy] = useState([]);

  const handleSelectItem = (row, checked: boolean) => {
    return Promise.resolve(checked);
  };

  const handleSelectAll = (checked: boolean) => {
    console.info({ checked });
    return Promise.resolve(checked);
  };

  const handleSorting = (sortBy: any) => {
    setSortBy(sortBy);
  };

  const handleChangePage = (page: number) => {
    setCurrentPage(page);
  };

  const handleChangePageSize = (value: number) => {
    setCurrentPage(1);
    setPageSize(value);
  };

  const handleAddToBucket = (value: any) => {
    console.log('Add to bucket', value);
  };

  const getRowId = (row: any) => row.keywordId;

  return (
    <Box>
      <KeywordTable
        rows={rows}
        selectedIds={selectedIds}
        resultTotal={resultTotal}
        page={currentPage}
        pageSize={pageSize}
        getRowId={getRowId}
        onSelectItem={handleSelectItem}
        onSelectAll={handleSelectAll}
        onSort={handleSorting}
        onChangePage={handleChangePage}
        onChangePageSize={handleChangePageSize}
        onAddToBucket={handleAddToBucket}
      ></KeywordTable>
    </Box>
  );
};
