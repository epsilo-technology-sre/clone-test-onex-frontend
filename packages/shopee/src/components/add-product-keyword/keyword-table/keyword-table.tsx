import { TableUI } from '@ep/shopee/src/components/common/table';
import { Box, Typography } from '@material-ui/core';
import React from 'react';

export const KeywordTable = (props: any) => {
  return (
    <TableUI
      className="setHeightModalAddkeyword"
      noData={
        <Box py={6}>
          <Typography
            variant="subtitle2"
            style={{ margin: '16px auto' }}
          >
            No data
          </Typography>
        </Box>
      }
      {...props}
    />
  );
};
