import { AddCustomKeyword } from '@ep/shopee/src/components/add-product-keyword/add-custom-keyword';
import { KeywordProductBucket } from '@ep/shopee/src/components/add-product-keyword/keyword-product-bucket';
import { KeywordTable } from '@ep/shopee/src/components/add-product-keyword/keyword-table';
import { KeywordTableAction } from '@ep/shopee/src/components/add-product-keyword/keyword-table-action';
import {
  ButtonTextUI,
  ButtonUI,
} from '@ep/shopee/src/components/common/button';
import { ModifyKeyWords } from '@ep/shopee/src/components/common/modify-keywords';
import { SelectedKeyword } from '@ep/shopee/src/components/common/selected-keyword';
import { Box, Grid, makeStyles, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import React, { useEffect, useState } from 'react';

const useStyle = makeStyles({
  root: {
    background: '#ffffff',
    padding: 16,
  },
});

export const AddKeyword = (props: any) => {
  const classes = useStyle();

  const {
    tableHeaders,
    hasPreviousStep,
    currency,
    noMatchType,
    productBucket,
    selectedProducts,
    keywords,
    selectedKeywords,
    searchText,
    pagination,
    keywordBucket,
    onUpdateSelectedKeywordPrice,
    onSearch,
    onAddKeywordsToBucket,
    onRemoveKeywordFromBucket,
    onResetKeywordBucket,
    onAddCustomKeywords,
    onSelectProductBucketItem,
    onSelectKeywordItem,
    onSelectAllKeywordItems,
    onChangePagination,
    onBackToPrevious,
    onSave,
    onCancel,
    isSubmitting,
  } = props;

  const [selectedProductIds, setSelectedProductIds] = React.useState(
    [],
  );
  const [products, setProducts] = useState([]);
  const [selectedIds, setSelectedIds] = useState([]);

  const [isShowCustomKeyword, setIsShowCustomKeyword] = useState(
    false,
  );

  const [sortBy, setSortBy] = useState([]);

  useEffect(() => {
    setSelectedIds(
      selectedKeywords.map((item: any) => item.keywordId),
    );
  }, [selectedKeywords]);

  useEffect(() => {
    setSelectedProductIds(
      selectedProducts.map((item: any) => item.productId),
    );
  }, [selectedProducts]);

  React.useEffect(() => {
    const productList = productBucket.map((product: any) => {
      const existItem = keywordBucket.find(
        (item: any) => item.product.productId === product.productId,
      );
      if (existItem) {
        return {
          ...product,
          addedKeyword: existItem.keywords.length,
        };
      }
      return product;
    });
    setProducts(productList);
  }, [productBucket, keywordBucket]);

  const handleAddCustomKeyword = (keywordNames: string[]) => {
    // setIsShowCustomKeyword(!isShowCustomKeyword);
    onAddCustomKeywords(keywordNames, currency);
  };

  const handleSorting = (sortBy: any) => {
    setSortBy(sortBy);
  };

  const handleSearch = (value: any) => {
    if (value !== searchText) {
      onSearch(value, currency);
    }
  };

  const handleChangePage = (page: number) => {
    onChangePagination({
      ...pagination,
      currentPage: page,
    });
  };

  const handleChangePageSize = (value: number) => {
    onChangePagination({
      ...pagination,
      pageSize: value,
      currentPage: 1,
    });
  };

  const getRowId = (row: any) => row.keywordId;

  const columns = React.useMemo(() => {
    return tableHeaders;
  }, [tableHeaders]);

  return (
    <Box className={classes.root}>
      <Grid container justify="space-between">
        <Grid item>
          <Typography variant="h6">Add Keywords</Typography>
        </Grid>
        <Grid item>
          <CloseIcon
            style={{ cursor: 'pointer' }}
            onClick={onCancel}
          />
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={2}>
          <Box py={2}>
            <KeywordProductBucket
              items={products}
              selectedIds={selectedProductIds}
              onSelect={onSelectProductBucketItem}
            />
          </Box>
        </Grid>
        <Grid item xs={8}>
          <Box px={2}>
            <Box py={2}>
              <ModifyKeyWords
                currency={currency}
                noMatchType={noMatchType}
                disabled={selectedIds.length === 0}
                onSubmit={onUpdateSelectedKeywordPrice}
              />
            </Box>
            <Box py={2}>
              <KeywordTableAction
                searchText={searchText}
                disableAddAll={selectedIds.length === 0}
                onSearch={handleSearch}
                onClickAddCustomKeyword={() =>
                  setIsShowCustomKeyword(!isShowCustomKeyword)
                }
                onClickAddAll={() =>
                  onAddKeywordsToBucket(selectedKeywords)
                }
              />
            </Box>
            {isShowCustomKeyword && (
              <Box py={1}>
                <AddCustomKeyword onApply={handleAddCustomKeyword} />
              </Box>
            )}
            <Box py={2}>
              <KeywordTable
                columns={columns}
                rows={keywords}
                selectedIds={selectedIds}
                resultTotal={pagination.total}
                page={pagination.currentPage}
                pageSize={pagination.pageSize}
                getRowId={getRowId}
                onSelectItem={onSelectKeywordItem}
                onSelectAll={onSelectAllKeywordItems}
                onSort={handleSorting}
                onChangePage={handleChangePage}
                onChangePageSize={handleChangePageSize}
              />
            </Box>
          </Box>
        </Grid>
        <Grid item xs={2}>
          <Box py={2}>
            <SelectedKeyword
              noMatchType={noMatchType}
              listItem={keywordBucket}
              onRemoveItem={onRemoveKeywordFromBucket}
              onRemoveAll={onResetKeywordBucket}
            />
          </Box>
        </Grid>
      </Grid>
      <Box>
        <Grid
          container
          alignItems="center"
          justify="space-between"
          spacing={1}
        >
          <Grid item>
            {hasPreviousStep && (
              <ButtonTextUI
                size="small"
                colortext="#000"
                label="Back to previous step"
                onClick={onBackToPrevious}
              />
            )}
          </Grid>
          <Grid item>
            <ButtonTextUI
              size="small"
              colortext="#000"
              label="Cancel"
              style={{ marginRight: 8 }}
              onClick={onCancel}
            />
            <ButtonUI
              loading={isSubmitting}
              size="small"
              label="Save keyword"
              disabled={keywordBucket.length === 0}
              onClick={onSave}
            />
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};
