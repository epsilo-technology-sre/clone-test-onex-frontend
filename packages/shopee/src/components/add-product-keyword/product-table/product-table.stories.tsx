import { Box } from '@material-ui/core';
import React, { useState } from 'react';
import { ProductTable } from './product-table';

export default {
  title: 'Shopee / Add-Product-Keyword',
};

const products = [];

export const ProductTableDemo = (props: any) => {
  const [rows, setRows] = useState(products);
  const [selectedIds, setSelectedIds] = useState([]);
  const [pageSize, setPageSize] = useState(10);
  const [currentPage, setCurrentPage] = useState(1);
  const [resultTotal, setResultTotal] = useState(products.length);
  const [sortBy, setSortBy] = useState([]);

  const handleSelectItem = (row, checked: boolean) => {
    return Promise.resolve(checked);
  };

  const handleSelectAll = (checked: boolean) => {
    console.info({ checked });
    return Promise.resolve(checked);
  };

  const handleSorting = (sortBy: any) => {
    setSortBy(sortBy);
  };

  const handleChangePage = (page: number) => {
    setCurrentPage(page);
  };

  const handleChangePageSize = (value: number) => {
    setCurrentPage(1);
    setPageSize(value);
  };

  const handleAddToBucket = (value: any) => {
    console.log('Add to bucket', value);
  };

  const getRowId = (row: any) => row.keywordId;

  return (
    <Box>
      <ProductTable
        rows={rows}
        selectedIds={selectedIds}
        resultTotal={resultTotal}
        page={currentPage}
        pageSize={pageSize}
        getRowId={getRowId}
        onSelectItem={handleSelectItem}
        onSelectAll={handleSelectAll}
        onSort={handleSorting}
        onChangePage={handleChangePage}
        onChangePageSize={handleChangePageSize}
        onAddToBucket={handleAddToBucket}
      ></ProductTable>
    </Box>
  );
};
