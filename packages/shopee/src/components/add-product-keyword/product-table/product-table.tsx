import { TableUI } from '@ep/shopee/src/components/common/table';
import { Box, Typography } from '@material-ui/core';
import React from 'react';
import { initColumns } from './columns';

export const ProductTable = (props: any) => {
  const headers = initColumns({
    onAddToBucket: (value: any) => {
      props.onAddToBucket(value);
    },
  });
  return (
    <TableUI
      columns={headers}
      noData={
        <Box py={6}>
          <Typography
            variant="subtitle2"
            style={{ margin: '16px auto' }}
          >
            No data
          </Typography>
        </Box>
      }
      {...props}
    />
  );
};
