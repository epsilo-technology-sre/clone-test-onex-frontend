import { MAX_KEYWORDS_OF_PRODUCT } from '@ep/shopee/src/constants';
import { Box, Grid, makeStyles, Typography } from '@material-ui/core';
import clsx from 'clsx';
import React from 'react';
import { CurrencyCode } from '../../common/currency-code';

const useStyle = makeStyles({
  root: {
    background: '#F6F7F8',
    color: '#253746',
    padding: '16px 16px 40px',
    position: 'relative',
  },
  productWrapper: {
    height: 495,
    overflow: 'auto',
  },
  product: {
    position: 'relative',
    background: '#ffffff',
    fontSize: 12,
    padding: 8,
    marginBottom: 4,
    cursor: 'pointer',
    border: '2px solid #fff',
    borderRadius: 4,
    '& .line2': {
      color: '#596772',
      fontSize: 10,
      padding: '2px 0',
    },
    '& .remove': {
      display: 'none',
      position: 'absolute',
      top: 2,
      right: 2,
      color: '#000000',
      fontSize: 20,
      cursor: 'pointer',
    },
    '&:hover .remove': {
      display: 'block',
    },
  },
  active: {
    borderColor: '#485764',
  },
  productName: {
    fontSize: 12,
    fontWeight: 600,
    width: '100%',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
  keyword: {
    fontSize: 10,
    lineHeight: '12px',
    color: '#ED5C10',
  },
});

export interface KeywordProductBucketProps {
  items: any;
  selectedIds?: any[];
  onSelect?: any;
}

export const KeywordProductBucket = (
  props: KeywordProductBucketProps,
) => {
  const classes = useStyle();
  const { items = [], selectedIds, onSelect } = props;

  const getProductBudget = (product: any) => {
    if (product.totalBudget) {
      return (
        <>
          <Box>
            <CurrencyCode currency={product.currency} />
            {product.totalBudget}
          </Box>
          <Typography variant="body2" align="right" className="line2">
            Total budget
          </Typography>
        </>
      );
    }

    if (product.dailyBudget) {
      return (
        <>
          <Box>
            <CurrencyCode currency={product.currency} />
            {product.dailyBudget}
          </Box>
          <Typography variant="body2" align="right" className="line2">
            Daily budget
          </Typography>
        </>
      );
    }

    return (
      <Typography
        variant="body2"
        align="right"
        style={{ fontSize: 10 }}
      >
        No limit
      </Typography>
    );
  };

  return (
    <Box className={classes.root}>
      <Box pb={1.5}>
        <Typography variant="h6">Product bucket</Typography>
      </Box>
      <Box className={classes.productWrapper}>
        {items.map((item: any) => {
          const keywordCount = item.addedKeyword
            ? item.totalKeyword + item.addedKeyword
            : item.totalKeyword;
          return (
            <Box
              key={item.productId}
              className={clsx(classes.product, {
                [classes.active]: selectedIds.includes(
                  item.productId,
                ),
              })}
              onClick={() => onSelect(item)}
            >
              <Grid container justify="space-between">
                <Grid item xs={6}>
                  <Typography
                    variant="body2"
                    className={classes.productName}
                    title={item.productName}
                  >
                    {item.productName}
                  </Typography>
                  <Typography variant="subtitle2" className="line2">
                    {item.productId}
                  </Typography>
                  <Typography
                    variant="subtitle2"
                    className={classes.keyword}
                  >
                    {keywordCount}/
                    {MAX_KEYWORDS_OF_PRODUCT - keywordCount} Keyword
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <Box style={{ textAlign: 'right' }}>
                    {getProductBudget(item)}
                  </Box>
                </Grid>
              </Grid>
            </Box>
          );
        })}
      </Box>
    </Box>
  );
};
