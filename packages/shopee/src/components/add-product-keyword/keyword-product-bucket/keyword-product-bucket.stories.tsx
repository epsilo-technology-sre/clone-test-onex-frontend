import React from 'react';
import { KeywordProductBucket } from './keyword-product-bucket';

export default {
  title: 'Shopee / Add-Product-Keyword',
};

const products = [
  {
    productId: 6467757,
    productName: '1 Bánh mì bơ tỏi sấy Hàn Quốc - 120gr',
    dailyBudget: 0,
    totalBudget: 12000,
    currency: 'VND',
    totalKeyword: 100,
  },
  {
    productId: 6467758,
    productName: '2 Bánh mì bơ tỏi sấy Hàn Quốc - 120gr',
    dailyBudget: 10000,
    totalBudget: 0,
    currency: 'VND',
    totalKeyword: 20,
  },
  {
    productId: 6467759,
    productName: '3 Bánh mì bơ tỏi sấy Hàn Quốc - 120gr',
    dailyBudget: 0,
    totalBudget: 0,
    currency: 'VND',
    totalKeyword: 10,
  },
];
export const Main = () => {
  const [selectedIds, setSelectedIds] = React.useState([]);
  const [selectedProducts, setSelectedProducts] = React.useState([]);

  React.useEffect(() => {
    setSelectedIds(
      selectedProducts.map((item: any) => item.productId),
    );
  }, [selectedProducts]);

  const handleSelectItem = (value: any) => {
    const isExist = selectedProducts.some(
      (item) => item.productId === value.productId,
    );
    if (isExist) {
      setSelectedProducts(
        selectedProducts.filter(
          (item) => item.productId !== value.productId,
        ),
      );
    } else {
      setSelectedProducts(selectedProducts.concat(value));
    }
  };

  return (
    <KeywordProductBucket
      items={products}
      selectedIds={selectedIds}
      onSelect={handleSelectItem}
    ></KeywordProductBucket>
  );
};
