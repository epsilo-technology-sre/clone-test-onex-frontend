import { ProductBucket } from '@ep/shopee/src/components/add-product-keyword/product-bucket';
import { ProductBudgetEditor } from '@ep/shopee/src/components/add-product-keyword/product-budget-editor';
import { ProductTable } from '@ep/shopee/src/components/add-product-keyword/product-table';
import { ProductTableAction } from '@ep/shopee/src/components/add-product-keyword/product-table-action';
import {
  ButtonTextUI,
  ButtonUI,
} from '@ep/shopee/src/components/common/button';
import { ShopCampaignSelector } from '@ep/shopee/src/components/create-campaign/shop-campaign-selector';
import {
  GET_CAMPAIGNS,
  GET_CATEGORIES,
  GET_PRODUCTS,
  GET_SHOPS,
  UPDATE_SELECTED_PRODUCTS,
} from '@ep/shopee/src/redux/actions';
import {
  Box,
  Divider,
  Grid,
  makeStyles,
  Typography,
} from '@material-ui/core';
import debounce from 'lodash/debounce';
import get from 'lodash/get';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

const useStyle = makeStyles({
  modifyBudgetWrapper: {
    marginTop: 8,
    marginBottom: 16,
    border: '2px solid #E4E7E9',
  },
});

export const AddProduct = (props: any) => {
  const classes = useStyle();
  const dispatch = useDispatch();
  const [selectedShop, setSelectedShop] = useState();
  const [selectedCampaign, setSelectedCampaign] = useState();
  const [searchText, setSearchText] = useState('');

  const [tableRows, setTableRows] = useState([]);
  const [pageSize, setPageSize] = useState(10);
  const [currentPage, setCurrentPage] = useState(1);
  const [sortBy, setSortBy] = useState([]);
  const [productsInBucket, setProductsInBucket] = useState([]);
  const [selectingRows, setSelectingRows] = useState([]);

  const currency = 'VND';

  const getCampaigns = (payload: any) =>
    dispatch(GET_CAMPAIGNS.START(payload));
  const getProducts = (payload: any) =>
    dispatch(GET_PRODUCTS.START(payload));
  const getCategories = (payload: any) =>
    dispatch(GET_CATEGORIES.START(payload));
  const getShops = (payload?: any) =>
    dispatch(GET_SHOPS.START(payload));
  const updateSelectedProducts = (payload: any) =>
    dispatch(UPDATE_SELECTED_PRODUCTS(payload));

  useEffect(() => {
    getShops();
    getCampaigns({
      shop_eids: 4110,
      from: '2020-10-01',
      to: '2020-10-31',
      limit: 10,
      page: 1,
      orderBy: 'sum_gmv',
      order_mode: 'desc',
    });
    getProducts({
      shop_eids: 4110,
      from: '2020-10-01',
      to: '2020-10-31',
      limit: 10,
      page: 1,
      orderBy: 'sum_gmv',
      order_mode: 'desc',
    });
    handleGetProduct();
    getCategories({ shop_eid: 4110 });
  }, []);

  const handleGetProduct = (params?: any) => {
    getProducts({
      shop_eids: 4110,
      from: '2020-10-01',
      to: '2020-10-31',
      limit: 10,
      page: currentPage,
      orderBy: 'sum_gmv',
      order_mode: 'desc',
      ...params,
    });
  };

  const ref = useRef({
    selectedProducts: null,
    products: null,
  });

  const state = useSelector(
    ({
      shops,
      campaigns,
      categories,
      products,
      selectedProducts,
      pagination,
    }: any) => {
      ref.current = {
        selectedProducts,
        products,
      };
      return {
        shops,
        campaigns,
        categories,
        products,
        selectedProducts,
        pagination,
      };
    },
  );
  const {
    shops = [],
    campaigns = [],
    categories = [],
    products = [],
    selectedProducts = [],
    pagination = {},
  } = state;

  const [selectedCategories, setSelectedCategories] = useState(
    categories,
  );
  const refTable = React.useRef({ products: products, buckets: [] });

  useEffect(() => {
    debounce(
      () => handleGetProduct({ filter_value: searchText }),
      3000,
    );
  }, [searchText, selectedCategories, pageSize, currentPage, sortBy]);

  const addProductsToBucket = React.useCallback(
    (productList: any) => {
      const newBucketProducts = [...refTable.current.buckets];
      productList.forEach((product: any) => {
        const exist = newBucketProducts.find(
          (item) => item.productId === product.productId,
        );
        if (!exist) {
          newBucketProducts.push(product);
        }
      });

      const bucketProductIds = newBucketProducts.map(
        (item: any) => item.productId,
      );
      const newRows = refTable.current.products.map((item) => {
        const isAdded = bucketProductIds.includes(item.productId);
        return { ...item, added: isAdded };
      });

      setProductsInBucket(newBucketProducts);
      setTableRows(newRows);
      refTable.current.products = newRows;
      refTable.current.buckets = newBucketProducts;
    },
    [],
  );

  const handleSelectAll = React.useCallback((checked) => {
    if (
      checked &&
      get(ref.current, 'selectedProducts.length', 0) === 0
    ) {
      updateSelectedProducts(
        (ref.current.selectedProducts || []).concat(
          ref.current.products,
        ),
      );
    } else {
      updateSelectedProducts([]);
    }
  }, []);

  const handleSelectItem = React.useMemo(() => {
    return (item: any, checked: boolean) => {
      if (checked) {
        updateSelectedProducts(
          (ref.current.selectedProducts || []).concat(item),
        );
      } else {
        updateSelectedProducts(
          (ref.current.selectedProducts || []).filter(
            (c: any) =>
              c.campaign_product_id !== item.campaign_product_id,
          ),
        );
      }
    };
  }, [selectedProducts]);

  const getRowId = React.useCallback((row) => {
    return String(row.campaign_product_id);
  }, []);

  const selectedIds = React.useMemo(() => {
    return (selectedProducts || []).map((i) =>
      String(i.campaign_product_id),
    );
  }, [selectedProducts]);

  const handleRemoveProducts = (productList: any) => {
    const newBucketProducts = productsInBucket.filter(
      (product: any) => {
        const exist = productList.find(
          (item: any) => item.productId === product.productId,
        );
        return !exist;
      },
    );

    const bucketProductIds = newBucketProducts.map(
      (item: any) => item.productId,
    );
    const newRows = refTable.current.products.map((item) => {
      const isAdded = bucketProductIds.includes(item.productId);
      return { ...item, added: isAdded };
    });

    setProductsInBucket(newBucketProducts);
    setTableRows(newRows);
    refTable.current.products = newRows;
    refTable.current.buckets = newBucketProducts;
  };

  const handleChangeSearchText = (event: any) => {
    setSearchText(event.target.value);
  };

  const handleSorting = (sortBy: any) => {
    setSortBy(sortBy);
  };

  const handleChangePage = (page: number) => {
    setCurrentPage(page);
  };

  const handleChangePageSize = (value: number) => {
    setCurrentPage(1);
    setPageSize(value);
  };

  const handleSelect = (value: any) => {
    const selectedRows = value.map((item: any) => item.original);
    setSelectingRows(selectedRows);
  };

  const handleChangeBudget = (value: any) => {
    let totalBudget = 0;
    let dailyBudget = 0;
    if (!value.isNoLimit) {
      if (value.budgetType === 'daily_budget') {
        dailyBudget = value.budget;
      } else {
        totalBudget = value.budget;
      }
    }

    const updatedProducts = refTable.current.products.map(
      (product: any) => {
        const isSelecting = selectingRows.find(
          (item: any) => item.productId === product.productId,
        );
        if (isSelecting) {
          return { ...product, totalBudget, dailyBudget };
        }
        return product;
      },
    );

    refTable.current.products = updatedProducts;
    setTableRows(updatedProducts);
  };

  console.log(products, 'selectedProducts');

  return (
    <Box p={2} style={{ background: '#ffffff' }}>
      <Typography variant="h6">Add Product</Typography>
      <Grid container spacing={1}>
        <Grid item xs={9}>
          <Box py={2}>
            <ShopCampaignSelector
              shops={shops}
              campaigns={campaigns}
              onChangeShop={(value: any) => setSelectedShop(value)}
              onChangeCampaign={(value: any) =>
                setSelectedCampaign(value)
              }
            />
          </Box>
          <Divider />
          <Box className={classes.modifyBudgetWrapper}>
            <ProductBudgetEditor
              title="Modify budget"
              disabled={selectingRows.length === 0}
              currency={currency}
              onSubmit={handleChangeBudget}
            />
          </Box>
          <Box>
            <ProductTableAction
              searchText={searchText}
              disableAddAll={false}
              categories={categories}
              selectedCategories={selectedCategories}
              onSearch={handleChangeSearchText}
              onChangeCategories={(value: any) =>
                setSelectedCategories(value)
              }
              onClickAddAll={() =>
                addProductsToBucket(selectedProducts)
              }
            ></ProductTableAction>
          </Box>
          <Box py={2}>
            <Grid container>
              <Grid item xs={12}>
                <ProductTable
                  rows={products}
                  onSelect={handleSelect}
                  onSelectItem={handleSelectItem}
                  onSelectAll={handleSelectAll}
                  selectedIds={selectedIds}
                  getRowId={getRowId}
                  onSort={handleSorting}
                  resultTotal={pagination.item_count}
                  page={currentPage}
                  pageSize={pageSize}
                  onChangePage={handleChangePage}
                  onChangePageSize={handleChangePageSize}
                  onAddToBucket={(value: any) => {
                    addProductsToBucket([value]);
                  }}
                ></ProductTable>
              </Grid>
            </Grid>
          </Box>
        </Grid>
        <Grid item xs={3}>
          <ProductBucket
            products={productsInBucket}
            onRemove={handleRemoveProducts}
          ></ProductBucket>
        </Grid>
      </Grid>
      <Box>
        <Grid
          container
          alignItems="center"
          justify="flex-end"
          spacing={1}
        >
          <Grid item>
            <ButtonTextUI
              size="small"
              colortext="#000"
              label="Cancel"
            ></ButtonTextUI>
          </Grid>
          <Grid item>
            <ButtonUI size="small" label="Add products"></ButtonUI>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};
