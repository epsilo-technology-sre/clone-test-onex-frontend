import { Box } from '@material-ui/core';
import React, { useState } from 'react';
import { ProductTableAction } from './product-table-action';

export default {
  title: 'shopee / Add-Product-Keyword',
};

export const KeywordTableActionDemo = () => {
  const [searchText, setSearchText] = useState('');

  const handleSearch = (value: any) => {
    console.log(`Search "${value}"`);
    setSearchText(value);
  };

  const handleChangeCategories = (value: any) => {
    console.log('Change categories', value);
  };

  const handleClickAddAll = () => {
    console.log('Click add all');
  };

  return (
    <Box p={3} style={{ background: '#ffffff' }}>
      <ProductTableAction
        searchText={searchText}
        disableAddAll={false}
        categories={[]}
        selectedCategories={[]}
        onSearch={handleSearch}
        onChangeCategories={handleChangeCategories}
        onClickAddAll={handleClickAddAll}
      ></ProductTableAction>
    </Box>
  );
};
