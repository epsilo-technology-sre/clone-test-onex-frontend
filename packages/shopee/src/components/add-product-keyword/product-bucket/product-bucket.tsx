import { Box, Grid, makeStyles, Typography } from '@material-ui/core';
import CancelIcon from '@material-ui/icons/Cancel';
import React from 'react';
import { CurrencyCode } from '../../common/currency-code';

const useStyle = makeStyles({
  root: {
    background: '#F6F7F8',
    color: '#253746',
    padding: '16px 16px 40px',
    position: 'relative',
  },
  productWrapper: {
    height: 495,
    overflow: 'auto',
  },
  product: {
    position: 'relative',
    background: '#ffffff',
    fontSize: 12,
    padding: 8,
    marginBottom: 4,
    '& .line2': {
      color: '#596772',
      fontSize: 10,
    },
    '& .remove': {
      display: 'none',
      position: 'absolute',
      top: 2,
      right: 2,
      color: '#000000',
      fontSize: 20,
      cursor: 'pointer',
    },
    '&:hover .remove': {
      display: 'block',
    },
  },
  productName: {
    width: '100%',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
  clearAll: {
    position: 'absolute',
    left: 16,
    bottom: 16,
    cursor: 'pointer',
  },
});

export const ProductBucket = (props: any) => {
  const classes = useStyle();
  const { products, onRemove } = props;

  const getProductBudget = (product: any) => {
    if (product.totalBudget) {
      return (
        <>
          <Box>
            <CurrencyCode currency={product.currency} />
            {product.totalBudget}
          </Box>
          <Typography variant="body2" align="right" className="line2">
            Total budget
          </Typography>
        </>
      );
    }

    if (product.dailyBudget) {
      return (
        <>
          <Box>
            <CurrencyCode currency={product.currency} />
            {product.dailyBudget}
          </Box>
          <Typography variant="body2" align="right" className="line2">
            Daily budget
          </Typography>
        </>
      );
    }

    return (
      <Typography
        variant="body2"
        align="right"
        style={{ fontSize: 10 }}
      >
        No limit
      </Typography>
    );
  };

  return (
    <Box className={classes.root}>
      <Box pb={1.5}>
        <Typography variant="h6">Product bucket</Typography>
      </Box>
      <Box className={classes.productWrapper}>
        {products.map((item: any, index: number) => {
          return (
            <Box className={classes.product} key={index}>
              <Grid container justify="space-between">
                <Grid item xs={6}>
                  <Typography
                    variant="body2"
                    className={classes.productName}
                    title={item.productName}
                  >
                    {item.productName}
                  </Typography>
                  <Typography variant="subtitle2" className="line2">
                    {item.productId}
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <Box style={{ textAlign: 'right' }}>
                    {getProductBudget(item)}
                  </Box>
                </Grid>
              </Grid>
              <CancelIcon
                className="remove"
                onClick={() => onRemove([item])}
              />
            </Box>
          );
        })}
      </Box>
      <Typography
        variant="body2"
        className={classes.clearAll}
        onClick={() => onRemove(products)}
      >
        Clear All
      </Typography>
    </Box>
  );
};
