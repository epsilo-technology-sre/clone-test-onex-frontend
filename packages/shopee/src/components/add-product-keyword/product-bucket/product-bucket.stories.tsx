import React from 'react';
import { ProductBucket } from './product-bucket';

export default {
  title: 'Shopee / Add-Product-Keyword',
};

const products = [
  {
    productName: 'Xí muội nho/ trần bì/ mơ Đài Loan - 240g',
    productId: '3221291369',
    dailyBudget: 0,
    totalBudget: 0,
    currency: 'VND',
  },
  {
    productName:
      'Cao quy linh Đài Loan (đậu đỏ/ đậu xanh/ đậu đỏ mix bắp)',
    productId: '3221291369',
    dailyBudget: 2000,
    totalBudget: 0,
    currency: 'VND',
  },
  {
    productName:
      'Cao quy linh Đài Loan (đậu đỏ/ đậu xanh/ đậu đỏ mix bắp)',
    productId: '3221291369',
    dailyBudget: 0,
    totalBudget: 15000,
    currency: 'VND',
  },
];

export const ProductBucketExample = () => {
  const handleRemoveProducts = (products: any) => {
    console.log('Remove product', products);
  };

  return (
    <ProductBucket
      products={products}
      onRemove={handleRemoveProducts}
    ></ProductBucket>
  );
};
