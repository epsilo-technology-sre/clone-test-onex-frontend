import { Box } from '@material-ui/core';
import React, { useState } from 'react';
import { KeywordTableAction } from './keyword-table-action';

export default {
  title: 'shopee / Add-Product-Keyword',
};

export const KeywordTableActionDemo = () => {
  const [searchText, setSearchText] = useState('');

  const handleSearch = (value: any) => {
    console.log(`Search "${value}"`);
    setSearchText(value);
  };

  const handleClickAddCustomKeyword = () => {
    console.log('Click add custom keyword');
  };

  const handleClickAddAll = () => {
    console.log('Click add all');
  };

  return (
    <Box p={3} style={{ background: '#ffffff' }}>
      <KeywordTableAction
        searchText={searchText}
        disableAddAll={false}
        onSearch={handleSearch}
        onClickAddCustomKeyword={handleClickAddCustomKeyword}
        onClickAddAll={handleClickAddAll}
      ></KeywordTableAction>
    </Box>
  );
};
