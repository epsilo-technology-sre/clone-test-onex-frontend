import { COLORS } from '@ep/shopee/src/constants';
import {
  Grid,
  InputAdornment,
  OutlinedInput,
  withStyles,
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import SearchIcon from '@material-ui/icons/Search';
import React, { useEffect, useState } from 'react';
import { ButtonUI } from '../../common/button';

export const InputUI = withStyles({
  root: {
    fontSize: 14,
    '& fieldset': {
      borderColor: '#C2C7CB',
      borderWidth: 2,
    },
    '&.Mui-focused fieldset': {
      borderColor: '#485764!important',
    },
  },
  input: {
    paddingTop: 8,
    paddingBottom: 8,
  },
})(OutlinedInput);

export const KeywordTableAction = (props: any) => {
  const {
    searchText,
    disableAddAll = false,
    onSearch,
    onClickAddCustomKeyword,
    onClickAddAll,
  } = props;
  const [text, setText] = useState('');

  useEffect(() => {
    setText(searchText);
  }, [searchText]);

  const handleChangeText = (event: any) => {
    setText(event.target.value);
  };

  const handleKeyUp = (event: any) => {
    if (event.key === 'Enter') {
      onSearch(event.target.value.trim());
    }
  };

  return (
    <Grid container justify="space-between" alignItems="center">
      <Grid item>
        <InputUI
          id="keyword-table-searchbox"
          placeholder="Search"
          value={text}
          onChange={handleChangeText}
          onKeyUp={handleKeyUp}
          startAdornment={
            <InputAdornment position="start">
              <SearchIcon fontSize="small" />
            </InputAdornment>
          }
          style={{ width: 320 }}
          labelWidth={0}
          autoComplete="off"
        ></InputUI>
      </Grid>
      <Grid item>
        <Grid container spacing={1}>
          <Grid item>
            <ButtonUI
              size="small"
              label="Add custom keyword"
              colorButton={COLORS.COMMON.GRAY}
              onClick={onClickAddCustomKeyword}
            ></ButtonUI>
          </Grid>
          <Grid item>
            <ButtonUI
              size="small"
              label="Add all"
              disabled={disableAddAll}
              colorButton={COLORS.COMMON.GRAY}
              iconLeft={<AddIcon />}
              onClick={onClickAddAll}
            ></ButtonUI>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};
