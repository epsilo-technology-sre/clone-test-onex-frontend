import { MultiSelectUI } from '@ep/shopee/src/components/common/multi-select';
import { COLORS } from '@ep/shopee/src/constants';
import {
  Grid,
  InputAdornment,
  OutlinedInput,
  withStyles,
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import SearchIcon from '@material-ui/icons/Search';
import React, { useEffect, useState } from 'react';
import { ButtonUI } from '../../common/button';

export const InputUI = withStyles({
  root: {
    fontSize: 14,
    '& fieldset': {
      borderColor: '#C2C7CB',
      borderWidth: 2,
    },
    '&.Mui-focused fieldset': {
      borderColor: '#485764!important',
    },
  },
  input: {
    paddingTop: 8,
    paddingBottom: 8,
  },
})(OutlinedInput);

export const AddProductTableAction = (props: any) => {
  const {
    searchText,
    categories,
    selectedCategories,
    disabled = false,
    onSearch,
    onChangeCategories,
    onClickAddAll,
  } = props;
  const [text, setText] = useState('');

  useEffect(() => {
    setText(searchText);
  }, [searchText]);

  const handleChangeText = (event: any) => {
    setText(event.target.value);
  };

  const handleKeyUp = (event: any) => {
    if (event.key === 'Enter') {
      onSearch(event.target.value.trim());
    }
  };

  return (
    <Grid container justify="space-between" alignItems="center">
      <Grid item>
        <Grid container>
          <Grid item>
            <InputUI
              id="add-product-table-searchbox"
              placeholder="Search"
              value={text}
              onChange={handleChangeText}
              onKeyUp={handleKeyUp}
              startAdornment={
                <InputAdornment position="start">
                  <SearchIcon fontSize="small" />
                </InputAdornment>
              }
              style={{ width: 320 }}
              labelWidth={0}
              autoComplete="off"
            />
          </Grid>
          <Grid item>
            <MultiSelectUI
              prefix="Category"
              suffix="categories"
              items={categories}
              selectedItems={selectedCategories}
              onSaveChange={onChangeCategories}
            />
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <ButtonUI
          size="small"
          label="Add all"
          disabled={disabled}
          colorButton={COLORS.COMMON.GRAY}
          iconLeft={<AddIcon />}
          onClick={onClickAddAll}
        />
      </Grid>
    </Grid>
  );
};
