import { COLORS } from '@ep/shopee/src/constants';
import {
  Box,
  makeStyles,
  OutlinedInput,
  Typography,
  withStyles,
} from '@material-ui/core';
import React from 'react';
import { ButtonUI } from '../../common/button';

const useStyle = makeStyles({
  root: {
    width: '100%',
  },
  guideline: {
    color: '#515F6B',
  },
});

export const TextArea = withStyles({
  root: {
    fontSize: 14,
    padding: 8,
    '& fieldset': {
      borderColor: '#C2C7CB',
      borderWidth: 2,
    },
    '&.Mui-focused fieldset': {
      borderColor: '#485764!important',
    },
  },
})(OutlinedInput);

export const AddCustomKeyword = (props: any) => {
  const classes = useStyle();
  const [text, setText] = React.useState('');

  const handleChange = (
    event: React.ChangeEvent<HTMLInputElement>,
  ) => {
    const editedText = event.target.value.replace(
      /[~!@#$%^&*()/\-+={}|[/\]\:;"'<>./?`_/\\]/gi,
      '',
    );
    if(editedText.split('\n').length <= 20) {
      setText(editedText);
    }
  };

  const handleApply = () => {
    let keywords: string[] = [];
    if (text) {
      keywords = text
        .split('\n')
        .map((item) => item.trim().toLowerCase())
        .filter((item) => !!item);
    }

    props.onApply(keywords);
    setText('');
  };

  return (
    <>
      <TextArea
        id="custom-keyword-box"
        className={classes.root}
        multiline
        rows={4}
        value={text}
        onChange={handleChange}
      />
      <Typography
        variant="caption"
        component="div"
        className={classes.guideline}
      >
        Input one keyword per line. The maximum number of keywords is 20.
      </Typography>
      <Box pt={2}>
        <ButtonUI
          colorButton={COLORS.COMMON.GRAY}
          size="small"
          label="Fill down table"
          onClick={handleApply}
        ></ButtonUI>
      </Box>
    </>
  );
};
