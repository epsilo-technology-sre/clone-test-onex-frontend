import { Box, Divider, Typography } from '@material-ui/core';
import { action } from '@storybook/addon-actions';
import React from 'react';
import { AddCustomKeyword } from './add-custom-keyword';

export default {
  title: 'Shopee / Add-Product-Keyword',
};

export const AddCustomKeywordDemo = () => {
  const [keywords, setKeywords] = React.useState([]);

  return (
    <Box p={2}>
      <Typography variant="h6">Add Custom Keywords</Typography>
      <AddCustomKeyword
        onApply={action('on add custom keyword')}
      ></AddCustomKeyword>
      <Divider />
      {keywords.length > 0 && (
        <Box p={1}>
          <Typography variant="subtitle1">
            {keywords.length} Custom Keywords
          </Typography>
          {keywords.map((item, index) => (
            <Typography variant="body2" key={index}>
              {item}
            </Typography>
          ))}
        </Box>
      )}
    </Box>
  );
};
