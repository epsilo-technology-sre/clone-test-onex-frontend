import React, {useState} from 'react'
import { ProductBudgetEditorPopover } from './product-budget-editor-popover'
import EditIcon from '@material-ui/icons/Edit';
import { Box } from '@material-ui/core';

export default {
  title: 'Shopee / Product budget editor popover'
}

export const Primary = () => {
  const [totalBudget, setTotalBudget] = useState(1);
  const [dailyBudget, setDailyBudget] = useState(1);

  const handleSave = (value: any) => {
    // setBudget(value)
    console.log('On save ', value)
  }

  return (
    <Box>
      <Box>
        <ProductBudgetEditorPopover
          currency="VND"
          total={0}
          daily={0}
          triggerElem={<EditIcon />}
          onSaveChange={handleSave}
        />
      </Box>
      <Box>
        <Box>
          <b>Total budget:</b> 
          {` ${totalBudget}`}
        </Box>
        <Box>
          <b>Daily budget:</b> 
          {` ${dailyBudget}`}
        </Box>
      </Box>
    </Box>
    
  )
}