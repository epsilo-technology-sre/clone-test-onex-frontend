import Box from '@material-ui/core/Box';
import Popover from '@material-ui/core/Popover';
import PopupState, {
  bindPopover,
  bindTrigger,
} from 'material-ui-popup-state';
import React, { useContext } from 'react';
import { ProductContext } from '../../products/product-context';
import { ProductBudgetEditor } from '../product-budget-editor';

export const ProductBudgetEditorPopover = (props: any) => {
  const context = useContext(ProductContext);
  const {
    triggerElem,
    product,
    total,
    daily,
    currency,
    hideTotal,
    hideDaily,
  } = props;

  const applyChange = (popupState: any, value: any) => {
    popupState.close();
    if (context && context.updateProductBudget) {
      context.updateProductBudget({
        shop_eids: [product.shop_eid],
        campaign_product_ids: value.productIds,
        total_budget: value.total,
        daily_budget: value.daily,
      });
    }
  };

  return (
    <PopupState variant="popover" popupId="demo-popup-popover">
      {(popupState) => {
        return (
          <Box ml={1}>
            <Box {...bindTrigger(popupState)}>{triggerElem}</Box>
            <Popover
              {...bindPopover(popupState)}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
            >
              <ProductBudgetEditor
                products={[product]}
                currency={currency}
                total={total}
                daily={daily}
                hideTotal={hideTotal}
                hideDaily={hideDaily}
                onSave={(value: any) =>
                  applyChange(popupState, value)
                }
                onCancel={() => popupState.close()}
              />
            </Popover>
          </Box>
        );
      }}
    </PopupState>
  );
};
