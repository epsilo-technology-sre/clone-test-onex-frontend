import Box from '@material-ui/core/Box';
import Popover from '@material-ui/core/Popover';
import PopupState, {
  bindPopover,
  bindTrigger,
} from 'material-ui-popup-state';
import React, { useContext, useEffect } from 'react';
import { ShopAdsContext } from '../../shop-ads/shop-ads-context';
import { AdCreativeEditor } from '../ad-creative-editor';

export const AdCreativeEditorPopover = (props: any) => {
  const context = useContext(ShopAdsContext);
  const { triggerElem, shop } = props;

  const applyChange = (popupState: any, value: any) => {
    popupState.close();
    if (context && context.updateAdCreative) {
      context.updateAdCreative({
        shopEids: [shop.shopEid],
        shopAdsId: shop.shopAdsId,
        value: value,
      });
    }
  };

  return (
    <PopupState variant="popover" popupId="demo-popup-popover">
      {(popupState) => {
        return (
          <Box ml={1}>
            <Box {...bindTrigger(popupState)}>{triggerElem}</Box>
            <Popover
              {...bindPopover(popupState)}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
            >
              <PopupAwarenessEditor
                popupState={popupState}
                loadCategoryList={context.getCategoryList}
                shop={shop}
                applyChange={applyChange}
              ></PopupAwarenessEditor>
            </Popover>
          </Box>
        );
      }}
    </PopupState>
  );
};

function PopupAwarenessEditor({
  popupState,
  loadCategoryList,
  shop,
  applyChange,
}) {
  const [catList, setCatList] = React.useState([]);
  useEffect(() => {
    if (popupState.isOpen) {
      loadCategoryList(shop).then(
        (
          catList: {
            categoryId: number | string;
            categoryName: string;
          }[],
        ) => {
          setCatList(catList);
        },
      );
    }
  }, [popupState.isOpen]);

  return (
    <AdCreativeEditor
      categoryList={catList}
      tagline={shop.adsCreativeTagline}
      isUseDefault={shop.shopCategoryEid === null}
      adCategory={shop.shopCategoryEid}
      onSave={(value: any) => applyChange(popupState, value)}
      onCancel={() => popupState.close()}
    />
  );
}
