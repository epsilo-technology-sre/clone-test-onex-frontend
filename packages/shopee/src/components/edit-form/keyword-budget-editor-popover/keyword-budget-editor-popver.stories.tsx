import React from 'react'
import { KeywordBudgetEditorPopover } from './keyword-budget-editor-popover'
import EditIcon from '@material-ui/icons/Edit';
import { Box } from '@material-ui/core';

export default {
  title: 'Shopee / Keyword budget editor popover'
}

export const Primary = () => {
  const handleSave = (value: any) => {
    console.log('On save ', value)
  }

  return (
    <Box>
      <Box>
        <KeywordBudgetEditorPopover
          currency="VND"
          total={0}
          daily={0}
          triggerElem={<EditIcon />}
          onSaveChange={handleSave}
        />
      </Box>
    </Box>
    
  )
}