import Box from '@material-ui/core/Box';
import Popover from '@material-ui/core/Popover';
import { bindPopover, bindTrigger } from 'material-ui-popup-state';
import { usePopupState } from 'material-ui-popup-state/hooks';
import React, { useContext } from 'react';
import { KeywordContext } from '../../keywords/keyword-context';
import { KeywordBudgetEditor } from '../keyword-budget-editor';

export const KeywordBudgetEditorPopover = (props: any) => {
  const context = useContext(KeywordContext);
  const popupState = usePopupState({
    variant: 'popover',
    popupId: 'demo-popup-popover',
  });
  const { triggerElem, keyword, bidding_price, currency } = props;
  const [suggestPrice, setSuggestPrice] = React.useState(0);
  const [isLoading, setLoading] = React.useState(false);

  React.useEffect(() => {
    if (popupState.isOpen) {
      getSuggestBiddingPrice();
    }
  }, [popupState]);

  const getSuggestBiddingPrice = async () => {
    if (context && context.getSuggestBiddingPrice) {
      const price = await context.getSuggestBiddingPrice({
        shopId: keyword.shop_eid,
        keywordId: keyword.keyword_id,
      });
      setSuggestPrice(price);
    }
  };

  const applyChange = (price: any) => {
    if (context && context.updateKeywordBudget) {
      setLoading(true);
      let rs = context.updateKeywordBudget({
        keywordId: [keyword.keyword_id],
        matchType: keyword.match_type
          .toLowerCase()
          .split(' ')
          .join('_'),
        biddingPrice: price,
        shopId: keyword.shop_eid,
      });
      if (rs.then) {
        rs.then(() => {
          setLoading(false);
          popupState.close();
        }).catch(() => {
          setLoading(false);
        });
      } else {
        popupState.close();
      }
    }
  };

  return (
    <Box ml={1}>
      <Box {...bindTrigger(popupState)}>{triggerElem}</Box>
      <Popover
        {...bindPopover(popupState)}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
      >
        <KeywordBudgetEditor
          currency={currency}
          suggestPrice={suggestPrice}
          total={bidding_price}
          onSave={(value: any) => applyChange(value)}
          onCancel={() => popupState.close()}
          loading={isLoading}
        />
      </Popover>
    </Box>
  );
};
