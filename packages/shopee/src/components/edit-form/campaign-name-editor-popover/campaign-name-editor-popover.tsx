import { COLORS } from '@ep/shopee/src/constants';
import Box from '@material-ui/core/Box';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Popover from '@material-ui/core/Popover';
import {
  createStyles,
  makeStyles,
  Theme,
  withStyles,
} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { bindPopover, bindTrigger } from 'material-ui-popup-state';
import { usePopupState } from 'material-ui-popup-state/hooks';
import React, { useContext, useEffect, useState } from 'react';
import { CampaignContext } from '../../campaigns/campaign-context';
import { ButtonTextUI, ButtonUI } from '../../common/button';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    body: {
      padding: 8,
    },
    title: {
      color: '#596772',
      fontSize: 11,
      fontWeight: 'bold',
      padding: '5px 0',
    },
    errorMessage: {
      color: COLORS.COMMON.RED,
      fontSize: 12,
      marginTop: 8,
    },
    footer: {
      display: 'flex',
      justifyContent: 'flex-end',
      padding: theme.spacing(1),
      borderTop: '2px solid #E4E7E9',
      '& button': {
        marginLeft: 5,
      },
    },
  }),
);

const Textbox = withStyles({
  root: {
    '&:hover .MuiOutlinedInput-notchedOutline': {
      borderColor: '#C2C7CB',
    },
  },
  notchedOutline: {
    border: '2px solid #C2C7CB',
  },
  input: {
    width: 215,
    padding: 8,
    fontSize: 14,
  },
})(OutlinedInput);

// FIXME: [FE-5] refactor to use Yup validation
export const CampaignNameEditorPopover = (props: any) => {
  const classes = useStyles();
  const { triggerElem, campaign, name } = props;
  const context = useContext(CampaignContext);

  const [text, setText] = useState(name);
  const [error, setError] = useState('');
  const [isOpen, setIsOpen] = useState(false);

  const applyChange = async (popupState: any) => {
    if (context && context.updateCampaignName) {
      const result = await context.updateCampaignName({
        shop_eids: [campaign.shop_eid],
        campaign_eid: campaign.campaign_eid,
        name: text,
      });
      if (result) {
        popupState.close();
      }
    } else {
      popupState.close();
    }
  };

  const popupState = usePopupState({
    variant: 'popover',
    popupId: 'demoPopover',
  });

  useEffect(() => {
    setIsOpen(popupState.isOpen);
  }, [popupState.isOpen]);

  const handleBlur = () => {
    let message = '';
    if (!text) {
      message = 'Please enter Campaign name';
    } else if (text.toLowerCase().includes('default')) {
      message = 'Campaign name is not allowed to contain "default"';
    }
    setError(message);
  };

  const handleChange = (e: any) => {
    setText(e.target.value);
  };

  return (
    <div>
      <Box ml={1}>
        <Box {...bindTrigger(popupState)}>{triggerElem}</Box>
        <Popover
          {...bindPopover(popupState)}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          onClose={() => {
            popupState.close();
            setText(name);
          }}
        >
          <Box className={classes.body} key={isOpen}>
            <Typography variant="body2" className={classes.title}>
              Campaign name
            </Typography>
            <Box>
              <Textbox
                id="name-edit-searchbox"
                placeholder="Search"
                value={text}
                onChange={handleChange}
                onBlur={handleBlur}
                labelWidth={0}
                autoComplete="off"
              ></Textbox>
              <Typography
                variant="body2"
                className={classes.errorMessage}
              >
                {error}
              </Typography>
            </Box>
          </Box>
          <Box className={classes.footer}>
            <ButtonTextUI
              label="Cancel"
              size="small"
              colortext="#000"
              onClick={() => {
                popupState.close();
                setText(name);
              }}
            />
            <ButtonUI
              label="Apply"
              size="small"
              variant="contained"
              onClick={() => applyChange(popupState)}
            />
          </Box>
        </Popover>
      </Box>
    </div>
    // <PopupState variant="popover" popupId="demo-popup-popover">
    //   <div>

    //   </div>
    //   {(popupState) => {
    //     return (

    //   )}}
    // </PopupState>
  );
};
