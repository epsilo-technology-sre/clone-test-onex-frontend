import React, { useState } from 'react';
import { CampaignNameEditorPopover } from './campaign-name-editor-popover'
import EditIcon from '@material-ui/icons/Edit';

export default {
  title: 'Shopee / Campaign Name Editor Popover',
};

export const CampaignNameEditor = () => {
  const [campaignName, setCampaignName] = useState('');

  const handleSave = (value: any) => {
    setCampaignName(value);
  }

  return (
    <div>
      <CampaignNameEditorPopover 
        name={campaignName} 
        triggerElem={<EditIcon />}
        onSaveChange={handleSave}
      ></CampaignNameEditorPopover>
    </div>
  );
};
