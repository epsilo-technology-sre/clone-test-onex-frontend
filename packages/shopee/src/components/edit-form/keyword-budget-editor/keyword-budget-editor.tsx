import { formatCurrency } from '@ep/shopee/src/utils/utils';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Field, Form, Formik } from 'formik';
import React from 'react';
import * as Yup from 'yup';
import { ButtonTextUI, ButtonUI } from '../../common/button';
import { CurrencyCode } from '../../common/currency-code';
import { CurrencyTextbox } from '../../common/textfield';

const useStyles = makeStyles({
  body: {
    padding: 8,
  },
  title: {
    color: '#596772',
    fontSize: 11,
    fontWeight: 'bold',
    padding: '5px 0',
  },
  switcher: {
    display: 'flex',
    alignItems: 'center',
    padding: '8px 0',
    '& img': {
      cursor: 'pointer',
      marginRight: 8,
    },
  },
  suggestPrice: {
    paddingTop: 4,
    fontSize: 12,
    color: '#0BA373',
  },
  footer: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: 8,
    borderTop: '2px solid #E4E7E9',
    '& button': {
      marginLeft: 5,
    },
  },
});

const validationSchema = Yup.object().shape({
  price: Yup.string().required('Please enter Bidding price'),
});

export const KeywordBudgetEditor = (props: any) => {
  const classes = useStyles();
  const {
    suggestPrice,
    currency,
    total,
    onSave,
    onCancel,
    loading = false,
  } = props;

  return (
    <Formik
      validationSchema={validationSchema}
      initialValues={{ price: total, matchType: 'exact_match' }}
      onSubmit={(values) => {
        onSave(values.price);
      }}
      validateOnBlur={true}
    >
      {({ errors }) => {
        return (
          <Form>
            <Box className={classes.body}>
              <Box className={classes.switcher}>
                <Typography variant="body2" component="span">
                  Bidding Price
                </Typography>
              </Box>
              <Box>
                <Field name="price">
                  {({ field, form }: any) => (
                    <CurrencyTextbox
                      currency={currency}
                      min={1}
                      step={1}
                      error={errors.price}
                      value={field.value}
                      onChange={(value: any) => {
                        form.setFieldValue(field.name, value);
                      }}
                    />
                  )}
                </Field>
              </Box>
              {suggestPrice > 0 && (
                <Box className={classes.suggestPrice}>
                  Suggested <CurrencyCode currency={currency} />
                  {formatCurrency(suggestPrice, currency)}
                </Box>
              )}
            </Box>
            <Box className={classes.footer}>
              <ButtonTextUI
                label="Cancel"
                size="small"
                colortext="#000"
                onClick={onCancel}
              />
              <ButtonUI
                loading={loading}
                type="submit"
                label="Apply"
                size="small"
                variant="contained"
              />
            </Box>
          </Form>
        );
      }}
    </Formik>
  );
};
