import { COLORS } from '@ep/shopee/src/constants';
import SwitchOffIcon from '@ep/one/src/images/switch-off.svg';
import SwitchOnIcon from '@ep/one/src/images/switch-on.svg';
import {
  Typography,
  MenuItem,
  Box,
  InputBase,
  Select,
  OutlinedInput,
} from '@material-ui/core';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { Field, Formik } from 'formik';
import React from 'react';
import * as Yup from 'yup';
import { ButtonTextUI, ButtonUI } from '../../common/button';


let validationSchema = Yup.object().shape({
  isUseDefault: Yup.bool().optional(),
  tagline: Yup.string()
    .required('Tagline is required.')
    .max(50, 'Tagline must have maximum 50 characters'),
  adCategory: Yup.string()
    .when('isUseDefault', {
      is: false,
      then: Yup.string().required(),
    })
    .nullable(),
});

export const AdCreativeEditor = (props: any) => {
  const classes = useStyles();
  const {
    onSave,
    onCancel,
    categoryList,
    tagline,
    isUseDefault,
    adCategory,
  } = props;

  console.info({ tagline, adCategory });

  return (
    <Formik
      validationSchema={validationSchema}
      initialValues={{ tagline: tagline, isUseDefault, adCategory }}
      onSubmit={(values) => {
        onSave({
          adCategory: values.adCategory,
          isUseDefault: values.isUseDefault,
          tagline: values.tagline,
        });
      }}
      validateOnBlur={true}
    >
      {({ setFieldValue, errors, values, submitForm }) => {
        console.info('formerror', errors);
        return (
          <>
            <Box className={classes.body}>
              <Box className={classes.switcher}>
                <img
                  src={
                    values.isUseDefault ? SwitchOnIcon : SwitchOffIcon
                  }
                  height={16}
                  width={30}
                  onClick={() => {
                    setFieldValue(
                      'isUseDefault',
                      !values.isUseDefault,
                    );
                  }}
                />
                <Typography variant="body2" component="span">
                  Main Shop Page (Default)
                </Typography>
              </Box>
              <Box mb={2}>
                <Select
                  id="demo-customized-select-native"
                  disabled={values.isUseDefault}
                  value={values.adCategory}
                  onChange={(evt) => {
                    setFieldValue('adCategory', evt.target.value);
                  }}
                  input={
                    <BootstrapInput placeholder="Choose Shop Category" />
                  }
                  MenuProps={{
                    anchorOrigin: {
                      vertical: 'bottom',
                      horizontal: 'left',
                    },
                    transformOrigin: {
                      vertical: 'top',
                      horizontal: 'left',
                    },
                    getContentAnchorEl: null,
                  }}
                >
                  {categoryList.map((i) => (
                    <MenuItem value={i.categoryId}>
                      {i.categoryName}
                    </MenuItem>
                  ))}
                </Select>
                {errors.adCategory && (
                  <Box className={classes.error}>
                    {errors.adCategory}
                  </Box>
                )}
              </Box>
              <Box>
                <Field name="tagline">
                  {({ field, form }) => (
                    <Textbox
                      placeholder="Tagline"
                      labelWidth={0}
                      autoComplete="off"
                      {...field}
                    />
                  )}
                </Field>
                <Box>
                  {errors.tagline && (
                    <Box className={classes.error}>
                      {errors.tagline}
                    </Box>
                  )}
                </Box>
              </Box>
            </Box>
            <Box className={classes.footer}>
              <ButtonTextUI
                label="Cancel"
                size="small"
                colortext="#000"
                onClick={onCancel}
              />
              <ButtonUI
                label="Apply"
                size="small"
                variant="contained"
                onClick={() => submitForm()}
              />
            </Box>
          </>
        );
      }}
    </Formik>
  );
};

const Textbox = withStyles({
  root: {
    '&:hover .MuiOutlinedInput-notchedOutline': {
      borderColor: '#C2C7CB',
    },
  },
  notchedOutline: {
    border: '2px solid #C2C7CB',
  },
  input: {
    width: 215,
    padding: 8,
    fontSize: 14,
  },
})(OutlinedInput);

const useStyles = makeStyles({
  body: {
    padding: 8,
  },
  title: {
    color: '#596772',
    fontSize: 11,
    fontWeight: 'bold',
    padding: '5px 0',
  },
  switcher: {
    display: 'flex',
    alignItems: 'center',
    padding: '8px 0',
    '& img': {
      cursor: 'pointer',
      marginRight: 8,
    },
  },
  error: {
    color: COLORS.COMMON.RED,
    fontSize: 12,
    marginTop: 8,
  },
  footer: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: 8,
    borderTop: '2px solid #E4E7E9',
    '& button': {
      marginLeft: 5,
    },
  },
});

const BootstrapInput = withStyles({
  root: {
    width: '100%',
  },
  disabled: {
    color: '#9FA7AE !important',
    background: '#F6F7F8 !important',
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: '#ffffff',
    border: '2px solid #D3D7DA',
    fontSize: 14,
    color: '#253746',
    padding: '5px 26px 5px 8px',
  },
})(InputBase);
