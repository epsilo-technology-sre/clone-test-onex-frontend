import {
  CAMPAIGN_STATUS,
  DATE_FORMAT,
} from '@ep/shopee/src/constants';
import SwitchOffIcon from '@ep/shopee/src/images/switch-off.svg';
import SwitchOnIcon from '@ep/shopee/src/images/switch-on.svg';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Popover from '@material-ui/core/Popover';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import {
  bindPopper,
  bindToggle,
  usePopupState,
} from 'material-ui-popup-state/hooks';
import moment from 'moment';
import React, { useContext, useEffect, useState } from 'react';
import { CampaignContext } from '../../campaigns/campaign-context';
import { ButtonTextUI, ButtonUI } from '../../common/button';
import { SingleDatePickerUI } from '../../common/single-date-picker';

const useStyles = makeStyles({
  root: {
    '& .MuiPaper-root': {
      overflow: 'inherit',
    },
  },
  popoverPaper: {
    overflowX: 'visible',
    overflowY: 'visible',
  },
  body: {
    padding: 8,
  },
  popover: {
    border: '1px solid #c3c3c3',
  },
  title: {
    color: '#596772',
    fontSize: 11,
    fontWeight: 'bold',
    padding: '5px 0',
  },
  switcher: {
    display: 'flex',
    alignItems: 'center',
    padding: '8px 0',
    '& img': {
      cursor: 'pointer',
      marginRight: 8,
    },
  },
  footer: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: 8,
    borderTop: '2px solid #E4E7E9',
    '& button': {
      marginLeft: 5,
    },
  },
});

export const CampaignTimelineEditorPopover = (props: any) => {
  const classes = useStyles();
  const context = useContext(CampaignContext);
  const { campaign, triggerElem, start, end } = props;

  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [minEndDate, setMinEndDate] = useState(null);
  const [isOnNoLimit, setIsOnNoLimit] = useState(false);
  const [isDisabledStart, setIsDisabledStart] = useState(false);

  useEffect(() => {
    const status = [
      CAMPAIGN_STATUS.RUNNING,
      CAMPAIGN_STATUS.PAUSED,
      CAMPAIGN_STATUS.SYNCING,
      CAMPAIGN_STATUS.ENDED,
    ];
    const isDisabled = campaign
      ? status.includes(campaign.campaign_status)
      : false;
    setIsDisabledStart(isDisabled);
  }, [campaign]);

  useEffect(() => {
    setStartDate(start);
  }, [start]);

  useEffect(() => {
    const today = moment();
    const minDate =
      today.diff(moment(startDate, DATE_FORMAT)) > 0
        ? today.format(DATE_FORMAT)
        : startDate;
    setMinEndDate(minDate);
  }, [startDate]);

  useEffect(() => {
    let dateString = end;
    let checkedNoLimit = false;
    if (end.toLowerCase() === 'no limit') {
      dateString = start ? start : moment().format(DATE_FORMAT);
      checkedNoLimit = true;
    }

    setEndDate(dateString);
    setIsOnNoLimit(checkedNoLimit);
  }, [end]);

  const handleSave = async (popupState: any) => {
    if (context && context.updateCampaignTimeline) {
      const result = await context.updateCampaignTimeline({
        shop_eids: [campaign.shop_eid],
        campaign_eid: campaign.campaign_eid,
        time_line_from: startDate,
        time_line_to: isOnNoLimit ? 'no_limit' : endDate,
      });
      if (result) {
        popupState.close();
      }
    } else {
      popupState.close();
    }
  };

  const handleChangeStartDate = (date: any) => {
    setStartDate(date);
    if (
      moment(date, DATE_FORMAT).diff(moment(endDate, DATE_FORMAT)) > 0
    ) {
      setEndDate(date);
    }
  };

  const handleChangeEndDate = (date: any) => {
    setEndDate(date);
  };

  const popupState = usePopupState({
    variant: 'popper',
    popupId: 'demoPopper',
  });
  return (
    <div>
      <Box {...bindToggle(popupState)}>{triggerElem}</Box>
      <Popover
        {...bindPopper(popupState)}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        onClose={() => popupState.close()}
        className={classes.root}
        classes={{
          paper: classes.popoverPaper,
        }}
      >
        <Paper className={classes.popover}>
          <Box className={classes.body}>
            <Box pb={1}>
              <Typography variant="body2" className={classes.title}>
                From date
              </Typography>
              <SingleDatePickerUI
                disabled={isDisabledStart}
                date={startDate}
                minDate={moment().format(DATE_FORMAT)}
                onChange={handleChangeStartDate}
              />
            </Box>
            <Box>
              <Typography variant="body2" className={classes.title}>
                To date
              </Typography>
              <SingleDatePickerUI
                placement={'top'}
                disabled={isOnNoLimit}
                date={endDate}
                minDate={minEndDate}
                onChange={handleChangeEndDate}
              />
            </Box>
            <Box className={classes.switcher}>
              <img
                src={isOnNoLimit ? SwitchOnIcon : SwitchOffIcon}
                height={16}
                width={30}
                onClick={() => setIsOnNoLimit(!isOnNoLimit)}
              />
              <Typography variant="body2" component="span">
                No limit
              </Typography>
            </Box>
          </Box>
          <Box className={classes.footer}>
            <ButtonTextUI
              label="Cancel"
              size="small"
              colortext="#000"
              onClick={() => popupState.close()}
            />
            <ButtonUI
              label="Apply"
              size="small"
              variant="contained"
              onClick={() => handleSave(popupState)}
            />
          </Box>
        </Paper>
      </Popover>
    </div>
  );
};
