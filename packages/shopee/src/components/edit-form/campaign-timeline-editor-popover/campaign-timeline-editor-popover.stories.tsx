import React, {useState} from 'react'
import { CampaignTimelineEditorPopover } from './campaign-timeline-editor-popover'
import EditIcon from '@material-ui/icons/Edit';
import { Box } from '@material-ui/core';

export default {
  title: 'Shopee / Campaign timeline editor popover'
}

export const Primary = () => {
  const [start, setStart] = useState('2/9/2020');
  const [end, setEnd] = useState('5/10/2020');
 
  return (
    <Box>
      <Box>
        <CampaignTimelineEditorPopover
          start={start}
          end={end}
          triggerElem={<EditIcon />}
          onSaveChange={(value: any) => console.log('Save timeline', value)}
        />
      </Box>
    </Box>
  )
}