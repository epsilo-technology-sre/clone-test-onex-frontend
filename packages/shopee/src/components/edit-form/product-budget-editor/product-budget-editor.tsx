import { COLORS } from '@ep/shopee/src/constants';
import SwitchOffIcon from '@ep/shopee/src/images/switch-off.svg';
import SwitchOnIcon from '@ep/shopee/src/images/switch-on.svg';
import { MenuItem } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import InputBase from '@material-ui/core/InputBase';
import Select from '@material-ui/core/Select';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import React, { useEffect, useState } from 'react';
import { ButtonTextUI, ButtonUI } from '../../common/button';
import { CurrencyTextbox } from '../../common/textfield';

const BUDGET = {
  DAILY: 'daily_budget',
  TOTAL: 'total_budget',
};

const useStyles = makeStyles({
  body: {
    padding: 8,
  },
  title: {
    color: '#596772',
    fontSize: 11,
    fontWeight: 'bold',
    padding: '5px 0',
  },
  switcher: {
    display: 'flex',
    alignItems: 'center',
    padding: '8px 0',
    '& img': {
      cursor: 'pointer',
      marginRight: 8,
    },
  },
  error: {
    color: COLORS.COMMON.RED,
    fontSize: 12,
    marginTop: 8,
  },
  footer: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: 8,
    borderTop: '2px solid #E4E7E9',
    '& button': {
      marginLeft: 5,
    },
  },
});

const BootstrapInput = withStyles({
  root: {
    width: '100%',
  },
  disabled: {
    color: '#9FA7AE !important',
    background: '#F6F7F8 !important',
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: '#ffffff',
    border: '2px solid #D3D7DA',
    fontSize: 14,
    color: '#253746',
    padding: '5px 26px 5px 8px',
  },
})(InputBase);

export const ProductBudgetEditor = (props: any) => {
  const classes = useStyles();
  const {
    products,
    currency,
    daily = 0,
    total = 0,
    hideDaily = false,
    hideTotal = false,
    onSave,
    onCancel,
  } = props;

  const [isOnNoLimit, setIsOnNoLimit] = useState(false);
  const [budgetType, setBudgetType] = useState(BUDGET.TOTAL);
  const [budget, setBudget] = useState(1);
  const [error, setError] = useState('');

  useEffect(() => {
    let noLimit = true;
    let budgetValue = 0;
    let selectedBudgetType = '';

    if (daily === 0 && total === 0) {
      noLimit = true;
      budgetValue = 0;
      selectedBudgetType = BUDGET.TOTAL;
    } else {
      noLimit = false;
      budgetValue = total > 0 ? total : daily;
      selectedBudgetType = total > 0 ? BUDGET.TOTAL : BUDGET.DAILY;
    }

    setIsOnNoLimit(noLimit);
    setBudgetType(selectedBudgetType);
    setBudget(budgetValue);
  }, [daily, total]);

  const handleTotalSwitch = () => {
    setIsOnNoLimit(!isOnNoLimit);
  };

  const handleChangeBudgetType = (event: any) => {
    setBudgetType(event.target.value);
  };

  const validate = () => {
    return true;
  };

  const handleSave = () => {
    if (validate()) {
      let totalBudget = 0;
      let dailyBudget = 0;

      if (!isOnNoLimit) {
        if (budgetType === BUDGET.TOTAL) {
          totalBudget = parseFloat(budget);
        } else {
          dailyBudget = parseFloat(budget);
        }
      }

      onSave({
        productIds: products.map(
          (item: any) => item.campaign_product_id,
        ),
        total: totalBudget,
        daily: dailyBudget,
      });
    }
  };

  return (
    <>
      <Box className={classes.body}>
        <Box className={classes.switcher}>
          <img
            src={isOnNoLimit ? SwitchOnIcon : SwitchOffIcon}
            height={16}
            width={30}
            onClick={handleTotalSwitch}
          />
          <Typography variant="body2" component="span">
            No limit
          </Typography>
        </Box>
        <Box>
          <Grid container spacing={1}>
            <Grid item xs={5}>
              <Select
                id="demo-customized-select-native"
                disabled={isOnNoLimit}
                value={budgetType}
                onChange={handleChangeBudgetType}
                input={<BootstrapInput />}
                MenuProps={{
                  anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'left',
                  },
                  transformOrigin: {
                    vertical: 'top',
                    horizontal: 'left',
                  },
                  getContentAnchorEl: null,
                }}
              >
                {!hideDaily && (
                  <MenuItem value={BUDGET.DAILY}>
                    Daily budget
                  </MenuItem>
                )}
                {!hideTotal && (
                  <MenuItem value={BUDGET.TOTAL}>
                    Total budget
                  </MenuItem>
                )}
              </Select>
            </Grid>
            <Grid item xs={7}>
              <CurrencyTextbox
                currency={currency}
                disabled={isOnNoLimit}
                min={1}
                step={1}
                value={budget}
                onChange={(value: any) => setBudget(value)}
              ></CurrencyTextbox>
            </Grid>
          </Grid>
        </Box>
      </Box>
      <Box>
        {error && <Box className={classes.error}>{error}</Box>}
      </Box>
      <Box className={classes.footer}>
        <ButtonTextUI
          label="Cancel"
          size="small"
          colortext="#000"
          onClick={onCancel}
        />
        <ButtonUI
          label="Apply"
          size="small"
          variant="contained"
          onClick={handleSave}
        />
      </Box>
    </>
  );
};
