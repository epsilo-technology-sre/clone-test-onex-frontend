import React, {useContext, useState, useEffect} from 'react'

import { makeStyles } from '@material-ui/core/styles'
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'
import SwitchOnIcon from '@ep/shopee/src/images/switch-on.svg'
import SwitchOffIcon from '@ep/shopee/src/images/switch-off.svg'

import { CurrencyTextbox } from '../../common/textfield'
import { ButtonUI, ButtonTextUI } from '../../common/button';
import { COLORS } from '@ep/shopee/src/constants';

const useStyles = makeStyles({
    body: {
      padding: 8,
    },
    title: {
      color: '#596772',
      fontSize: 11,
      fontWeight: 'bold',
      padding: '5px 0',
    },
    switcher: {
      display: 'flex',
      alignItems: 'center',
      padding: '8px 0',
      '& img': {
        cursor: 'pointer',
        marginRight: 8,
      }
    },
    error: {
      color: COLORS.COMMON.RED,
      fontSize: 12,
      marginTop: 8,
    },
    footer: {
      display: 'flex',
      justifyContent: 'flex-end',
      padding: 8,
      borderTop: '2px solid #E4E7E9',
      '& button': {
        marginLeft: 5,
      }
    },
});

export const CampaignBudgetEditor = (props: any) => {
  const classes = useStyles();
  const { campaigns, currency, total, daily, onSave, onCancel } = props;
  
  const [totalBudget, setTotalBudget] = useState(1);
  const [dailyBudget, setDailyBudget] = useState(1);

  const [error, setError] = useState('');

  const [isOnNoLimit, setIsOnNoLimit] = useState(false);
  const [isOnDaily, setIsOnDaily] = useState(true);

  useEffect(() => {
    setTotalBudget(total)
    setIsOnNoLimit(!total)
  }, [total])

  useEffect(() => {
    setDailyBudget(daily)
    setIsOnDaily(!!daily)
  }, [daily])


  const handleChangeTotalBudget = (value: any) => {
    setTotalBudget(value)
  }

  const handleChangeDailyBudget = (value: any) => {
    setDailyBudget(value)
  }

  const handleTotalSwitch = () => {
    setIsOnNoLimit(!isOnNoLimit);
  }

  const handleDailySwitch = () => {
    setIsOnDaily(!isOnDaily);
  }

  const validate = () => {
    if (!isOnNoLimit && parseFloat(totalBudget) < parseFloat(dailyBudget)) {
      setError('Daily Budget must be lower than Total Budget')
      return false;
    }

    return true;
  }

  const handleSave = () => {
    if (validate()) {
      onSave({
        campaignIds: campaigns.map(item => item.campaign_eid),
        total: parseFloat(totalBudget),
        daily: parseFloat(dailyBudget),
      });
    }
  }

  return (
    <>
      <Box className={classes.body}>
        <Box pb={1}>
          <Typography variant="body2" className={classes.title}>Total budget</Typography>
          <CurrencyTextbox 
            currency={currency} 
            disabled={isOnNoLimit}
            min={100} 
            step={1}
            value={totalBudget}
            onChange={handleChangeTotalBudget}
          ></CurrencyTextbox>
        </Box>
        <Box className={classes.switcher}>
          <img 
            src={isOnNoLimit ? SwitchOnIcon : SwitchOffIcon} 
            height={16} 
            width={30}
            onClick={handleTotalSwitch}
          />
          <Typography variant="body2" component="span">No limit</Typography>
        </Box>
        <Box className={classes.switcher}>
          <img 
            src={isOnDaily ? SwitchOnIcon : SwitchOffIcon} 
            height={16} 
            width={30}
            onClick={handleDailySwitch}
          />
          <Typography variant="body2" component="span">Daily budget</Typography>
        </Box>
        <Box>
          <Typography variant="body2" className={classes.title}>Daily budget</Typography>
          <CurrencyTextbox 
            currency={currency} 
            disabled={!isOnDaily}
            min={1} 
            step={1}
            value={dailyBudget}
            onChange={handleChangeDailyBudget}
          ></CurrencyTextbox>
        </Box>
      </Box>
      <Box>
        {error && <Box className={classes.error}>
            {error}
          </Box>}
      </Box>
      <Box className={classes.footer}>
        <ButtonTextUI
          label="Cancel"
          size="small"
          colortext="#000"
          onClick={onCancel}
        />
        <ButtonUI
          label="Apply"
          size="small"
          variant="contained"
          onClick={handleSave}
        />
      </Box>
    </>
  )
}
