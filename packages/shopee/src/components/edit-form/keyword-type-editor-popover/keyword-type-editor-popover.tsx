import Box from '@material-ui/core/Box';
import Popover from '@material-ui/core/Popover';
import PopupState, {
  bindPopover,
  bindTrigger,
} from 'material-ui-popup-state';
import React, { useContext } from 'react';
import { KeywordContext } from '../../keywords/keyword-context';
import { KeywordTypeEditor } from '../keyword-type-editor';

export const KeywordTypeEditorPopover = (props: any) => {
  const context = useContext(props.context || KeywordContext);
  const { triggerElem, keyword } = props;

  const [isLoading, setLoading] = React.useState(false);

  const applyChange = (popupState: any, value: any) => {
    if (context && context.updateKeywordType) {
      setLoading(true);
      let rs = context.updateKeywordType({
        ...value,
        shopId: keyword.shop_eid,
      });
      if (rs.then) {
        rs.then(() => {
          setLoading(false);
          popupState.close();
        }).catch(() => {
          setLoading(false);
        });
      } else {
        popupState.close();
      }
    }
  };

  return (
    <PopupState variant="popover" popupId="demo-popup-popover">
      {(popupState) => {
        return (
          <Box ml={1}>
            <Box {...bindTrigger(popupState)}>{triggerElem}</Box>
            <Popover
              {...bindPopover(popupState)}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
            >
              <KeywordTypeEditor
                loading={isLoading}
                keyword={[keyword]}
                onSave={(value: any) =>
                  applyChange(popupState, value)
                }
                onCancel={() => popupState.close()}
              />
            </Popover>
          </Box>
        );
      }}
    </PopupState>
  );
};
