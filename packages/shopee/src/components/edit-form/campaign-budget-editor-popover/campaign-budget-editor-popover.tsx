import {
  COLORS,
  CURRENCY_LIMITATION,
} from '@ep/shopee/src/constants';
import { makeStyles } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Popover from '@material-ui/core/Popover';
import { Form, Formik } from 'formik';
import PopupState, {
  bindPopover,
  bindTrigger,
} from 'material-ui-popup-state';
import React, { useContext } from 'react';
import * as Yup from 'yup';
import { CampaignContext } from '../../campaigns/campaign-context';
import { ButtonTextUI, ButtonUI } from '../../common/button';
import { CampaignBudget } from '../../create-campaign/campaign-budget';

export const CampaignBudgetEditorPopover = (props: any) => {
  const context = useContext(CampaignContext);
  const {
    triggerElem,
    campaign,
    total,
    daily,
    currency,
    currencyLimitation,
  } = props;
  const classes = useStyles();

  const limitation = currencyLimitation || CURRENCY_LIMITATION;

  const validationSchema = React.useMemo(() => {
    let dailyBudget = 0;
    let totalBudget = 0;
    if (limitation[currency]) {
      dailyBudget = limitation[currency].DAILY_BUDGET.MIN;
      totalBudget = limitation[currency].TOTAL_BUDGET.MIN;
    }
    return Yup.object().shape({
      budget: Yup.object().shape({
        total: Yup.number()
          .test(
            'should-not-contains-e',
            'Invalid number',
            (v) => !String(v).includes('e'),
          )
          .when('isNoLimit', {
            is: true,
            then: Yup.number().optional(),
          })
          .when('isNoLimit', {
            is: false,
            then: Yup.number()
              .required()
              .min(
                totalBudget,
                `Budget cannot be lower than ${totalBudget}`,
              ),
          }),
        daily: Yup.number()
          .when('isOnDaily', {
            is: true,
            then: Yup.number()
              .required('Please enter Budget')
              .min(
                dailyBudget,
                `Budget cannot be lower than ${dailyBudget}`,
              ),
          })
          .when('isOnDaily', {
            is: false,
            then: Yup.number().optional(),
          })
          .when(['isNoLimit', 'isOnDaily'], {
            is: (isNoLimit, isOnDaily) => {
              return !isNoLimit && isOnDaily;
            },
            then: Yup.number().lessThan(
              Yup.ref('total'),
              'Daily Budget must be lower than Total Budget.',
            ),
          }),
      }),
    });
  }, [currency]);

  const applyChange = async (popupState: any, value: any) => {
    if (context && context.updateCampaignBudget) {
      let budget = value.budget;
      const result = await context.updateCampaignBudget({
        shop_eids: [campaign.shop_eid],
        campaign_eids: [campaign.campaign_eid],
        total_budget: budget.isNoLimit ? 0 : budget.total,
        daily_budget: budget.isOnDaily ? budget.daily : 0,
      });
      if (result) {
        popupState.close();
      }
    } else {
      popupState.close();
    }
  };

  return (
    <PopupState variant="popover" popupId="demo-popup-popover">
      {(popupState) => {
        return (
          <Box ml={1}>
            <Box {...bindTrigger(popupState)}>{triggerElem}</Box>
            <Popover
              {...bindPopover(popupState)}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
            >
              <Formik
                validationSchema={validationSchema}
                initialValues={{
                  budget: {
                    total: total,
                    daily: daily,
                    isNoLimit: !total,
                    isOnDaily: !!daily,
                  },
                }}
                onSubmit={(values) => {
                  return applyChange(popupState, values);
                }}
                validateOnBlur={true}
              >
                {({ handleSubmit, errors }) => {
                  return (
                    <Form onSubmit={handleSubmit}>
                      <Box padding={1}>
                        <CampaignBudget currency={currency} />
                        <Box className={classes.footer}>
                          <ButtonTextUI
                            label="Cancel"
                            size="small"
                            colortext="#000"
                            onClick={() => popupState.close()}
                          />
                          <ButtonUI
                            colorButton={COLORS.COMMON.ORANGE}
                            type="submit"
                            label="Apply"
                          />
                        </Box>
                      </Box>
                    </Form>
                  );
                }}
              </Formik>
            </Popover>
          </Box>
        );
      }}
    </PopupState>
  );
};

const useStyles = makeStyles({
  body: {
    padding: 8,
  },
  title: {
    color: '#596772',
    fontSize: 11,
    fontWeight: 'bold',
    padding: '5px 0',
  },
  switcher: {
    display: 'flex',
    alignItems: 'center',
    padding: '8px 0',
    '& img': {
      cursor: 'pointer',
      marginRight: 8,
    },
  },
  error: {
    color: COLORS.COMMON.RED,
    fontSize: 12,
    marginTop: 8,
  },
  footer: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: 8,
    borderTop: '2px solid #E4E7E9',
    '& button': {
      marginLeft: 5,
    },
  },
});
