import React, {useState} from 'react'
import { CampaignBudgetEditorPopover } from './campaign-budget-editor-popover'
import EditIcon from '@material-ui/icons/Edit';
import { Box } from '@material-ui/core';

export default {
  title: 'Shopee / Budget editor popover'
}

export const Primary = () => {
  const [totalBudget, setTotalBudget] = useState(1);
  const [dailyBudget, setDailyBudget] = useState(1);

  const handleSave = (value: any) => {
    console.log('Save budget', value)
    setTotalBudget(value.totalBudget);
    setDailyBudget(value.dailyBudget)
  }

  return (
    <Box>
      <Box>
        <CampaignBudgetEditorPopover
          currency="VND"
          totalBudget={totalBudget}
          dailyBudget={dailyBudget}
          triggerElem={<EditIcon />}
          onSaveChange={handleSave}
        />
      </Box>
      <Box>
        <Box>
          <b>Total budget:</b> 
          {` ${totalBudget}`}
        </Box>
        <Box>
          <b>Daily budget:</b> 
          {` ${dailyBudget}`}
        </Box>
      </Box>
    </Box>
    
  )
}