import {
  PRODUCT_DAILY_BUDGET,
  PRODUCT_TOTAL_BUDGET,
} from '@ep/shopee/src/constants';
import { ButtonTextUI, ButtonUI } from '../../common/button';
import { Box } from '@material-ui/core';
import { Formik } from 'formik';
import React from 'react';
import * as Yup from 'yup';
import { FormikBudgetInput } from '../../formik-embed/budget-input';
import { useStyle } from './common';

const validationSchema = (formCurrency: string) => {
  let currency = String(formCurrency).toUpperCase();

  Yup.object().shape({
    budget: Yup.object().shape({
      total: Yup.number()
        .test(
          'should-not-contains-e',
          'Invalid number',
          (v) => !String(v).includes('e'),
        )
        .when('isNoLimit', {
          is: true,
          then: Yup.number().optional(),
        })
        .when('isNoLimit', {
          is: false,
          then: Yup.number()
            .required()
            .min(
              PRODUCT_TOTAL_BUDGET[currency].MIN,
              'Total budget is too low',
            ),
        }),
      daily: Yup.number()
        .when('isOnDaily', {
          is: true,
          then: Yup.number()
            .required('Please enter Budget')
            .min(
              PRODUCT_DAILY_BUDGET[currency].MIN,
              'Daily budget is too low',
            ),
        })
        .when('isOnDaily', {
          is: false,
          then: Yup.number().optional(),
        })
        .when(['isNoLimit', 'isOnDaily'], {
          is: (isNoLimit, isOnDaily) => {
            return !isNoLimit && isOnDaily;
          },
          then: Yup.number().lessThan(
            Yup.ref('total'),
            'Daily Budget must be lower than Total Budget.',
          ),
        }),
    }),
  });
};

export const BudgetForm = (props: {
  currency: string;
  budget: {
    total: number;
    daily: number;
    isNoLimit: boolean;
    isOnDaily: boolean;
  };
  onSubmit?: Function;
  onCancel: Function;
}) => {
  const classes = useStyle();
  const { currency, budget, onSubmit: onSave, onCancel } = props;

  const budgetValidation = React.useMemo(() => {
    return validationSchema(currency);
  }, [currency]);

  return (
    <Formik
      validationSchema={budgetValidation}
      initialValues={{
        budget: {
          total: budget.total,
          daily: budget.daily,
          isNoLimit: !budget.isNoLimit,
          isOnDaily: !!budget.isNoLimit,
        },
      }}
      onSubmit={(values) => {
        onSave && onSave(values);
      }}
      validateOnBlur={true}
    >
      {({ handleSubmit }) => {
        return (
          <Box className={classes.body}>
            <FormikBudgetInput currency={currency} />

            <Box className={classes.footer}>
              <ButtonTextUI
                label="Cancel"
                size="small"
                colortext="#000"
                onClick={onCancel}
              />
              <ButtonUI
                type="submit"
                label="Apply"
                size="small"
                variant="contained"
                onClick={handleSubmit}
              />
            </Box>
          </Box>
        );
      }}
    </Formik>
  );
};
