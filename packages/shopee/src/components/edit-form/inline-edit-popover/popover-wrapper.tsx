import React from 'react';
import Box from '@material-ui/core/Box';
import Popover from '@material-ui/core/Popover';
import PopupState, {
  bindPopover,
  bindTrigger,
} from 'material-ui-popup-state';
import { useStyle } from './common';

export const EditorPopover = (props: any) => {
  const { triggerElem, children: formElement, onFormSubmit } = props;

  let classes = useStyle();

  const applyChange = (popupState: any, value: any) => {
    const result = onFormSubmit(value);
    if (result && result.then) {
      result.then((r) => {
        if (r) {
          popupState.close();
        }
      });
    }
  };

  return (
    <PopupState variant="popover" popupId="demo-popup-popover">
      {(popupState) => {
        return (
          <Box ml={1}>
            <Box {...bindTrigger(popupState)}>{triggerElem}</Box>
            <Popover
              {...bindPopover(popupState)}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              className={classes.root}
              classes={{
                paper: classes.popoverPaper,
              }}
            >
              {React.cloneElement(formElement, {
                onSubmit: (value: any) =>
                  applyChange(popupState, value),
                onCancel: () => {
                  popupState.close();
                },
              })}
            </Popover>
          </Box>
        );
      }}
    </PopupState>
  );
};
