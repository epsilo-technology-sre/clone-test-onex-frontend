import { Box } from '@material-ui/core';
import { Formik } from 'formik';
import React from 'react';
import * as Yup from 'yup';
import { ButtonTextUI, ButtonUI } from '../../common/button';
import { FormikTimelineInput } from '../../formik-embed/timeline-input';
import { useStyle } from './common';

const validationSchema = Yup.object().shape({
  timeline: Yup.object().shape({
    startDate: Yup.string().required('Start date is empty'),
    endDate: Yup.string().nullable(),
    isOnNoLimit: Yup.boolean(),
  }),
});

export const TimelineForm = (props: {
  currency: string;
  timeline: {
    startDate: string;
    endDate: string;
  };
  onSubmit?: Function;
  onCancel: Function;
}) => {
  const classes = useStyle();
  const { timeline, onSubmit: onSave, onCancel } = props;

  return (
    <Formik
      validationSchema={validationSchema}
      initialValues={{
        timeline: {
          startDate: timeline.startDate,
          endDate:
            timeline.endDate === 'No limit' ? null : timeline.endDate,
          isOnNoLimit: timeline.endDate === 'No limit',
        },
      }}
      onSubmit={(values) => {
        onSave && onSave(values);
      }}
      validateOnBlur={true}
    >
      {({ handleSubmit }) => {
        return (
          <Box className={classes.body}>
            <FormikTimelineInput />

            <Box className={classes.footer}>
              <ButtonTextUI
                label="Cancel"
                size="small"
                colortext="#000"
                onClick={onCancel}
              />
              <ButtonUI
                type="submit"
                label="Apply"
                size="small"
                variant="contained"
                onClick={handleSubmit}
              />
            </Box>
          </Box>
        );
      }}
    </Formik>
  );
};
