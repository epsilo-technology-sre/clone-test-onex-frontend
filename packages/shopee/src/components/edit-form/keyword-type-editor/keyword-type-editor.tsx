import { COLORS } from '@ep/shopee/src/constants';
import { MenuItem } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import InputBase from '@material-ui/core/InputBase';
import Select from '@material-ui/core/Select';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import React, { useState } from 'react';
import { ButtonTextUI, ButtonUI } from '../../common/button';

const TYPE = {
  EXACT: 'exact_match',
  BOARD: 'broad_match',
};

const useStyles = makeStyles({
  body: {
    padding: 8,
  },
  title: {
    color: '#596772',
    fontSize: 11,
    fontWeight: 'bold',
    padding: '5px 0',
  },
  switcher: {
    display: 'flex',
    alignItems: 'center',
    padding: '8px 0',
    '& img': {
      cursor: 'pointer',
      marginRight: 8,
    },
  },
  error: {
    color: COLORS.COMMON.RED,
    fontSize: 12,
    marginTop: 8,
  },
  footer: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: 8,
    borderTop: '2px solid #E4E7E9',
    '& button': {
      marginLeft: 5,
    },
  },
});

const BootstrapInput = withStyles({
  root: {
    width: '100%',
  },
  disabled: {
    color: '#9FA7AE !important',
    background: '#F6F7F8 !important',
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: '#ffffff',
    border: '2px solid #D3D7DA',
    fontSize: 14,
    color: '#253746',
    padding: '5px 26px 5px 8px',
  },
})(InputBase);

export const KeywordTypeEditor = (props: any) => {
  const classes = useStyles();
  const { keyword, onSave, onCancel, error, loading = false } = props;

  const [type, setType] = useState(TYPE.EXACT);

  React.useEffect(() => {
    setType(
      keyword[0].match_type === 'Broad Match'
        ? TYPE.BOARD
        : TYPE.EXACT,
    );
  }, [keyword]);

  const handleChangeBudgetType = (event: any) => {
    setType(event.target.value);
  };

  const validate = () => {
    return true;
  };

  const handleSave = () => {
    if (validate()) {
      onSave({
        keywordId: [keyword[0].keyword_id],
        biddingPrice: keyword[0].bidding_price,
        matchType: type,
      });
    }
  };

  return (
    <>
      <Box className={classes.body}>
        <Box className={classes.switcher}>
          <Typography variant="body2" component="span">
            Match Type
          </Typography>
        </Box>
        <Box>
          <Select
            id="demo-customized-select-native"
            value={type}
            onChange={handleChangeBudgetType}
            input={<BootstrapInput />}
            MenuProps={{
              anchorOrigin: {
                vertical: 'bottom',
                horizontal: 'left',
              },
              transformOrigin: {
                vertical: 'top',
                horizontal: 'left',
              },
              getContentAnchorEl: null,
            }}
          >
            <MenuItem value={TYPE.EXACT}>Exact Match</MenuItem>
            <MenuItem value={TYPE.BOARD}>Broad Match</MenuItem>
          </Select>
        </Box>
      </Box>
      <Box>
        {error && <Box className={classes.error}>{error}</Box>}
      </Box>
      <Box className={classes.footer}>
        <ButtonTextUI
          label="Cancel"
          size="small"
          colortext="#000"
          onClick={onCancel}
        />
        <ButtonUI
          loading={loading}
          label="Apply"
          size="small"
          variant="contained"
          onClick={handleSave}
        />
      </Box>
    </>
  );
};
