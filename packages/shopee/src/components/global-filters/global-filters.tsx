import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import React from 'react';
import { ButtonUI } from '../common/button';
import { DateRangePickerUI } from '../common/date-range-picker';
import { MultiSelectUI } from '../common/multi-select';

export const GlobalFilters = (props: any) => {
  const {
    countries,
    shops,
    selectedTimeRange,
    selectedCountries,
    selectedShops,
    onChangeDateRange,
    onChangeCountries,
    onChangeShops,
    onSubmitChange,
  } = props;

  return (
    <Box my={2}>
      <Grid container justify="flex-start" alignItems="center">
        <Grid item>
          <DateRangePickerUI
            startDate={selectedTimeRange.startDate}
            endDate={selectedTimeRange.endDate}
            onApply={onChangeDateRange}
            isOutsideRange="future"
          />
        </Grid>
        <Grid item>
          <MultiSelectUI
            prefix="Country"
            suffix="countries"
            items={countries}
            selectedItems={selectedCountries}
            onSaveChange={onChangeCountries}
          />
        </Grid>
        <Grid item>
          <MultiSelectUI
            prefix="Shop"
            suffix="shops"
            items={shops}
            selectedItems={selectedShops}
            onSaveChange={onChangeShops}
          />
        </Grid>
        <Grid item>
          <Box pl={1}>
            <ButtonUI
              label="Apply"
              size="small"
              variant="contained"
              onClick={onSubmitChange}
            />
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};
