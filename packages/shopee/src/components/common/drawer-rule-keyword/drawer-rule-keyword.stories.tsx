import React, { useState } from 'react'
import { Paper, Button } from '@material-ui/core'
import { DrawerRuleKeyword } from './drawer-rule-keyword';

export default {
  title: 'Shopee/Drawer rule keyword'
}

const statusType = ["deactivate", "activate", "increase", "decrease"]

const options = Array.from({ length: 2 }, (_, key) => {
  const random = Math.floor(Math.random() * Math.floor(4));
  return {
    ruleName: `Rule name ${key}`,
    "timeline": "11/9/2020 - 11/14/2020",
    status: statusType[random],
    "typeOfPerformance": "performance - period - operator - value",
    statusInfo: random < 2 ? {} : {
      type: 'absolute',
      step: 500,
      max: 3500,
      min: 500,

    }
  }
});

export const main = () => {
  const [open, setOpen] = useState(true)
  return (
    <Paper>
      <Button onClick={() => setOpen(true)}>Open</Button>
      <DrawerRuleKeyword
        rules={options}
        open={open}
        onClose={(e: boolean) => setOpen(e)}
      />
    </Paper>
  )
}
