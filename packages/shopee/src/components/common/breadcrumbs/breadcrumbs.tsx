import React from 'react';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';
import { withStyles } from '@material-ui/core/styles'

const StyledBreadcrumbs = withStyles({
  li: {
    fontSize: 14,
    '& a:hover': {
      color: '#ED5C10',
    }
  }
})(Breadcrumbs)

export const BreadcrumbsUI = (props: any) => {
  const { links, onClickLink } = props;

  const handleClick = (event: any, link) => {
    event.preventDefault();
    onClickLink(link)
  }

  return (
    <StyledBreadcrumbs aria-label="breadcrumb">
      {links.map((link: any, index: number) => (
        <Link 
          key={index}
          color={index === (links.length - 1) ? 'textPrimary' : 'inherit'} 
          href={link.href} 
          onClick={(e: any) => handleClick(e, link)}>
          {link.text}
        </Link>
      ))}
    </StyledBreadcrumbs>
  );
}

BreadcrumbsUI.defaultProps = {
  links: [],
}
