import React from 'react'
import { BreadcrumbsUI } from './breadcrumbs'

export default {
  title: 'Shopee/Breadcrumbs'
}

const links = [
  { text: 'Overview', href: '/' },
  { text: 'Shopee', href: '/' },
  { text: 'Campaign management', href: '/' },
]

export const Breadcrumbs = () => {
  const handleClickLink = (link: any) => {
    console.log('Click link ', link)
  }

  return (
    <div>
      <BreadcrumbsUI links={links} onClickLink={handleClickLink}></BreadcrumbsUI>
    </div>
  )
}