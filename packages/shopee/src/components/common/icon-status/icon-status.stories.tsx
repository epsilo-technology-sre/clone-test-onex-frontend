import React from 'react'
import Paper from '@material-ui/core/Paper'
import Box from '@material-ui/core/Box'
import { IconStatus } from './icon-status';
import { SHOP_STATUS } from '@ep/shopee/src/constants'

export default {
  title: 'Shopee/StatusIcon'
}

export const main = () => {
  const types = [];
  for (const status in SHOP_STATUS) {
    types.push(SHOP_STATUS[status])
  }

  return (
    <Paper>
      {types.map((type, index) => (
        <Box mx={2}>
          <IconStatus key={index} status={type} />
          <Box component="span" ml={1} style={{textTransform: 'capitalize'}}>{type}</Box>
        </Box>))}
    </Paper>
  )
}
