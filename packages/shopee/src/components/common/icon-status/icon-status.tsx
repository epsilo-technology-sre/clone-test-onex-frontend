import { SHOP_STATUS } from '@ep/shopee/src/constants';
import EndedIcon from '@ep/shopee/src/images/ended.svg';
import InitIcon from '@ep/shopee/src/images/init.svg';
import PausedIcon from '@ep/shopee/src/images/paused.svg';
import RunningIcon from '@ep/shopee/src/images/running.svg';
import SyncingIcon from '@ep/shopee/src/images/syncing.svg';
import { Icon } from '@material-ui/core';
import React from 'react';

export interface StatusIconProps {
  status: any;
}

export const IconStatus = ({ status }: StatusIconProps) => {
  const icons = {
    [SHOP_STATUS.INIT]: InitIcon,
    [SHOP_STATUS.GOOD]: RunningIcon,
    [SHOP_STATUS.SYNCING]: SyncingIcon,
    [SHOP_STATUS.ERROR]: EndedIcon,
    [SHOP_STATUS.WARNING]: PausedIcon,
    [SHOP_STATUS.PULLED]: SyncingIcon,
  };

  return (
    <Icon>
      <img
        src={icons[status] || ''}
        height={12}
        width={12}
        alt={status}
      />
    </Icon>
  );
};
