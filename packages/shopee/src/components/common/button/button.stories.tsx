import React from 'react';
import { text, boolean, select } from '@storybook/addon-knobs';
import {
  ButtonUI,
  ButtonUIProps,
  ButtonTextUI,
  ButtonLinkUI,
  ButtonIconUI,
} from './button';
import AttachmentIcon from '@material-ui/icons/Attachment';
import { COLORS } from '@ep/shopee/src/constants';
import { WrapStory } from '../layout/WrapStory';


export default {
  title: 'Shopee/Button',
};

export const Primary = () => {
  const props: ButtonUIProps = {
    label: text('label', 'sample'),
    colorButton: select(
      'colorButton',
      {
        RED: COLORS.COMMON.RED,
        GREEN: COLORS.COMMON.GREEN,
        ORANGE: COLORS.COMMON.ORANGE,
      },
      COLORS.COMMON.ORANGE,
    ),
    iconLeft: boolean('iconLeft', false),
    iconRight: boolean('iconRight', false),
    variant: select(
      'variant',
      {
        OUTLINED: `outlined`,
        CONTAINED: `contained`,
      },
      `contained`,
    ),
    size: select(
      'size',
      {
        SMALL: `small`,
        MEDIUM: `medium`,
        LARGE: `large`,
      },
      `medium`,
    ),
    disabled: boolean(`disabled`, false),
  };
  return (
    <div>
      <ButtonUI
          disabled={props.disabled}
          label={props.label}
          size="small"
          variant="contained"
          colorButton={props.colorButton}
          iconLeft={props.iconLeft && <AttachmentIcon />}
          iconRight={props.iconRight && <AttachmentIcon />}
        />

      <WrapStory>
        <ButtonUI
          disabled={props.disabled}
          label={props.label}
          size={props.size}
          variant={props.variant}
          colorButton={props.colorButton}
          iconLeft={props.iconLeft && <AttachmentIcon />}
          iconRight={props.iconRight && <AttachmentIcon />}
        />
      </WrapStory>
      <WrapStory>
        <ButtonTextUI
          disabled={props.disabled}
          label={props.label}
          size={props.size}
          colortext={props.colorButton}
          iconLeft={props.iconLeft && <AttachmentIcon />}
          iconRight={props.iconRight && <AttachmentIcon />}
        />
      </WrapStory>
      <WrapStory>
        <ButtonLinkUI
          disabled={props.disabled}
          label={props.label}
          size={props.size}
          iconLeft={props.iconLeft && <AttachmentIcon />}
          iconRight={props.iconRight && <AttachmentIcon />}
        />
      </WrapStory>
      <WrapStory>
        <ButtonIconUI>
          <AttachmentIcon />
        </ButtonIconUI>
      </WrapStory>
    </div>
  );
};
