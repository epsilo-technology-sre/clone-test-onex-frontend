import React from 'react';
import { toast } from 'react-toastify';
import { ButtonUI, ButtonUIProps } from './button';

type ButtonFileDownloadProps = ButtonUIProps & {
  getFileName: () => string;
  downloadFetch: () => Promise<any>;
  onAfterDownload?: () => void;
};

export function ButtonFileDownload(props: ButtonFileDownloadProps) {
  const [isLoading, setIsLoading] = React.useState(false);

  const download = () => {
    setIsLoading(true);
    props
      .downloadFetch()
      .then((blob) => {
        setIsLoading(false);
        downloadFile(blob, props.getFileName());
      })
      .catch((err) => {
        setIsLoading(false);
        toast.error(err.message);
      })
      .then(() => {
        if (props.onAfterDownload) {
          props.onAfterDownload();
        }
      });
  };

  if (isLoading) {
    return <ButtonUI label="Downloading..." disabled />;
  }

  return <ButtonUI {...props} onClick={download} />;
}

// copy from: https://github.com/kennethjiang/js-file-download/blob/master/file-download.js
function downloadFile(
  data: Blob,
  filename: string,
  mime?: string,
  bom?: string,
) {
  var blobData = typeof bom !== 'undefined' ? [bom, data] : [data];
  var blob = new Blob(blobData, {
    type: mime || 'application/octet-stream',
  });
  if (typeof window.navigator.msSaveBlob !== 'undefined') {
    // IE workaround for "HTML7007: One or more blob URLs were
    // revoked by closing the blob for which they were created.
    // These URLs will no longer resolve as the data backing
    // the URL has been freed."
    window.navigator.msSaveBlob(blob, filename);
  } else {
    var blobURL =
      window.URL && window.URL.createObjectURL
        ? window.URL.createObjectURL(blob)
        : window.webkitURL.createObjectURL(blob);
    var tempLink = document.createElement('a');
    tempLink.style.display = 'none';
    tempLink.href = blobURL;
    tempLink.setAttribute('download', filename);

    // Safari thinks _blank anchor are pop ups. We only want to set _blank
    // target if the browser does not support the HTML5 download attribute.
    // This allows you to download files in desktop safari if pop up blocking
    // is enabled.
    if (typeof tempLink.download === 'undefined') {
      tempLink.setAttribute('target', '_blank');
    }

    document.body.appendChild(tempLink);
    tempLink.click();

    // Fixes "webkit blob resource error 1"
    setTimeout(function () {
      document.body.removeChild(tempLink);
      window.URL.revokeObjectURL(blobURL);
    }, 200);
  }
}
