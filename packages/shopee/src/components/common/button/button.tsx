import * as React from 'react';
import {
  Button,
  Grid,
  withStyles,
  ButtonProps,
  IconButton,
  IconButtonProps,
  Theme,
  CircularProgress,
} from '@material-ui/core';
import { COLORS } from '@ep/shopee/src/constants';
import styled from 'styled-components';
import { WrapSVG } from '../layout/Wrap';

export const ButtonOgrangeStyle = withStyles((theme: Theme) => ({
  root: {
    textTransform: 'capitalize',
    borderRadius: theme.shape.borderRadius,
    border: (props: any) =>
      props.isoutlined && `1px solid ${COLORS.COMMON.ORANGE_OPA_40}`,
    background: (props: any) =>
      props.isoutlined ? `transparent` : COLORS.COMMON.ORANGE,
    color: (props: any) =>
      props.isoutlined ? COLORS.COMMON.ORANGE : 'white',
    paddingLeft: '1.2rem',
    paddingRight: '1.2rem',
    '&:disabled': {
      backgroundColor: (props: any) =>
        props.isoutlined
          ? `transparent`
          : COLORS.COMMON.ORANGE_OPA_40,
      color: (props: any) =>
        props.isoutlined ? COLORS.COMMON.ORANGE_OPA_20 : `white`,
    },
    '&:hover': {
      backgroundColor: (props: any) =>
        props.isoutlined ? `transparent` : 'rgba(221, 93, 69, 1)',
    },
    '&:active': {
      backgroundColor: COLORS.COMMON.ORANGE_OPA_8,
    },
    '&:focus': {
      boxShadow: `0 0 0 0.1rem ${COLORS.COMMON.ORANGE_OPA_20}`,
      backgroundColor: (props: any) =>
        props.isoutlined ? `transparent` : 'rgba(221, 93, 69, 1)',
    },
  },
}))(Button);

export const ButtonGreenStyle = withStyles((theme: Theme) => ({
  root: {
    textTransform: 'capitalize',
    borderRadius: theme.shape.borderRadius,
    border: (props: any) =>
      props.isoutlined && `1px solid ${COLORS.COMMON.GREEN_OPA_40}`,
    background: (props: any) =>
      props.isoutlined ? `transparent` : COLORS.COMMON.GREEN,
    color: (props: any) =>
      props.isoutlined ? COLORS.COMMON.GREEN : 'white',
    paddingLeft: '1.2rem',
    paddingRight: '1.2rem',
    '&:disabled': {
      backgroundColor: (props: any) =>
        props.isoutlined ? `transparent` : COLORS.COMMON.GREEN_OPA_40,
      color: (props: any) =>
        props.isoutlined ? COLORS.COMMON.GREEN_OPA_20 : `white`,
      borderColor: (props: any) =>
        props.isoutlined ? COLORS.COMMON.GREEN_OPA_20 : `inherit`,
    },
    '&:hover': {
      backgroundColor: (props: any) =>
        props.isoutlined ? `transparent` : 'rgba(38, 163, 109, 1)',
    },
    '&:active': {
      backgroundColor: COLORS.COMMON.GREEN_OPA_8,
    },
    '&:focus': {
      boxShadow: `0 0 0 0.1rem ${COLORS.COMMON.GREEN_OPA_20}`,
      backgroundColor: (props: any) =>
        props.isoutlined ? `transparent` : 'rgba(38, 163, 109, 1)',
    },
  },
}))(Button);

export const ButtonRedStyle = withStyles((theme: Theme) => ({
  root: {
    textTransform: 'capitalize',
    borderRadius: theme.shape.borderRadius,
    border: (props: any) =>
      props.isoutlined && `1px solid ${COLORS.COMMON.RED_OPA_40}`,
    background: (props: any) =>
      props.isoutlined ? `transparent` : COLORS.COMMON.RED,
    color: (props: any) =>
      props.isoutlined ? COLORS.COMMON.RED : 'white',
    paddingLeft: '1.2rem',
    paddingRight: '1.2rem',
    '&:disabled': {
      backgroundColor: (props: any) =>
        props.isoutlined ? `transparent` : COLORS.COMMON.RED_OPA_40,
      color: (props: any) =>
        props.isoutlined ? COLORS.COMMON.RED_OPA_20 : `white`,
      borderColor: (props: any) =>
        props.isoutlined ? COLORS.COMMON.RED_OPA_20 : `inherit`,
    },
    '&:hover': {
      backgroundColor: (props: any) =>
        props.isoutlined ? `transparent` : 'rgba(185, 46, 73, 1)',
    },
    '&:active': {
      backgroundColor: COLORS.COMMON.RED_OPA_8,
    },
    '&:focus': {
      boxShadow: `0 0 0 0.1rem ${COLORS.COMMON.RED_OPA_20}`,
      backgroundColor: (props: any) =>
        props.isoutlined ? `transparent` : 'rgba(185, 46, 73, 1)',
    },
  },
}))(Button);

export const ButtonGrayStyle = withStyles((theme: Theme) => ({
  root: {
    textTransform: 'capitalize',
    borderRadius: theme.shape.borderRadius,
    border: (props: any) =>
      props.isoutlined && `1px solid ${COLORS.COMMON.GRAY_OPA_40}`,
    background: (props: any) =>
      props.isoutlined ? `transparent` : COLORS.COMMON.GRAY,
    color: (props: any) => {
      if (props.colortext) {
        return props.colortext;
      }
      return props.isoutlined ? COLORS.COMMON.GRAY : 'rgb(37,55,70)';
    },
    paddingLeft: '1.2rem',
    paddingRight: '1.2rem',
    '&:disabled': {
      backgroundColor: (props: any) =>
        props.isoutlined ? `transparent` : COLORS.COMMON.GRAY_OPA_40,
      color: (props: any) =>
        props.isoutlined
          ? COLORS.COMMON.GRAY_OPA_20
          : `rgb(37,55,70)`,
      borderColor: (props: any) =>
        props.isoutlined ? COLORS.COMMON.GRAY_OPA_20 : `inherit`,
    },
    '&:hover': {
      backgroundColor: (props: any) =>
        props.isoutlined ? `transparent` : 'rgba(246, 247, 248, 1)',
    },
    '&:active': {
      backgroundColor: COLORS.COMMON.GRAY_OPA_8,
    },
    '&:focus': {
      boxShadow: `0 0 0 0.1rem ${COLORS.COMMON.GRAY_OPA_20}`,
      backgroundColor: (props: any) =>
        props.isoutlined ? `transparent` : 'rgba(246, 247, 248, 1)',
    },
  },
}))(Button);

export const ButtonMainStyle = withStyles((theme: Theme) => ({
  root: {
    textTransform: 'capitalize',
    borderRadius: theme.shape.borderRadius,
    border: (props: any) =>
      props.isoutlined && `1px solid ${COLORS.COMMON.MAIN_OPA_40}`,
    borderColor: (props: any) =>
      props.borderColor
        ? props.borderColor
        : COLORS.COMMON.MAIN_OPA_40,
    background: (props: any) =>
      props.isoutlined ? `transparent` : COLORS.COMMON.MAIN,
    color: (props: any) =>
      props.isoutlined ? COLORS.COMMON.MAIN : 'white',
    paddingLeft: '1.2rem',
    paddingRight: '1.2rem',
    '&:disabled': {
      backgroundColor: (props: any) =>
        props.isoutlined ? `transparent` : COLORS.COMMON.MAIN_OPA_40,
      color: (props: any) =>
        props.isoutlined ? COLORS.COMMON.MAIN_OPA_20 : `white`,
      borderColor: (props: any) =>
        props.isoutlined ? COLORS.COMMON.MAIN_OPA_20 : `inherit`,
    },
    '&:hover': {
      backgroundColor: (props: any) =>
        props.isoutlined ? `transparent` : 'rgba(185, 46, 73, 1)',
    },
    '&:active': {
      backgroundColor: COLORS.COMMON.MAIN_OPA_8,
    },
    '&:focus': {
      boxShadow: `0 0 0 0.1rem ${COLORS.COMMON.MAIN_OPA_20}`,
      backgroundColor: (props: any) =>
        props.isoutlined ? `transparent` : 'rgba(185, 46, 73, 1)',
    },
  },
}))(Button);

export const ButtonTextStyle = withStyles((theme: Theme) => ({
  root: {
    borderRadius: theme.shape.borderRadius,
    textTransform: 'capitalize',
    background: `transparent`,
    color: (props: any) =>
      props.colortext && `rgba(${props.colortext}, 1)`,
    '&:disabled': {
      backgroundColor: `transparent`,
      color: (props: any) =>
        props.colortext && `rgba(${props.colortext}, 0.4)`,
    },
    '&:hover': {
      backgroundColor: (props: any) =>
        props.islink
          ? `transparent`
          : props.colortext && `rgba(${props.colortext}, 0.08)`,
      textDecoration: (props: any) => props.islink && `underline`,
    },
    '&:active': {
      backgroundColor: (props: any) =>
        props.colortext && `rgba(${props.colortext}, 0.4)`,
    },
    '&:focus': {
      boxShadow: (props: any) =>
        props.colortext && `0 0 0 0.1rem rgba(${props.colortext},0.2`,
    },
  },
}))(Button);

const WrapiconLeft: any = styled(WrapSVG)`
  margin-right: ${(props: any) => (props.label ? `0.5rem` : ``)};
`;

const WrapiconRight: any = styled(WrapSVG)`
  margin-left: ${(props: any) => (props.label ? `0.5rem` : ``)};
`;

const LabelIconButton = ({ label, iconLeft, iconRight }: any) => (
  <Grid
    container
    direction="row"
    justify="center"
    alignItems="center"
    component={`span`}
  >
    {iconLeft && (
      <WrapiconLeft label={label}>{iconLeft}</WrapiconLeft>
    )}
    {label}
    {iconRight && (
      <WrapiconRight label={label}>{iconRight}</WrapiconRight>
    )}
  </Grid>
);

export interface ButtonUIProps extends ButtonProps {
  loading?: boolean;
  label?: string | any;
  colorButton?: any;
  iconLeft?: any;
  iconRight?: any;
  colortext?: string;
  borderColor?: string;
}

export const ButtonUI = (props: ButtonUIProps) => {
  const {
    loading,
    label,
    colorButton,
    iconLeft,
    iconRight,
    variant,
    ...agrs
  } = props;
  let render = null;
  const checkIsOutlined = Number(variant === `outlined`);
  const loadingCom = loading ? (
    <CircularProgress size={20} color="inherit" />
  ) : null;
  switch (colorButton) {
    case COLORS.COMMON.ORANGE:
      render = (
        <ButtonOgrangeStyle isoutlined={checkIsOutlined} {...agrs}>
          {loading ? (
            loadingCom
          ) : (
            <LabelIconButton
              label={label}
              iconLeft={iconLeft}
              iconRight={iconRight}
            />
          )}
        </ButtonOgrangeStyle>
      );
      break;
    case COLORS.COMMON.GREEN:
      render = (
        <ButtonGreenStyle isoutlined={checkIsOutlined} {...agrs}>
          {loading ? (
            loadingCom
          ) : (
            <LabelIconButton
              label={label}
              iconLeft={iconLeft}
              iconRight={iconRight}
            />
          )}
        </ButtonGreenStyle>
      );
      break;
    case COLORS.COMMON.RED:
      render = (
        <ButtonRedStyle isoutlined={checkIsOutlined} {...agrs}>
          {loading ? (
            loadingCom
          ) : (
            <LabelIconButton
              label={label}
              iconLeft={iconLeft}
              iconRight={iconRight}
            />
          )}
        </ButtonRedStyle>
      );
      break;
    case COLORS.COMMON.GRAY:
      render = (
        <ButtonGrayStyle isoutlined={checkIsOutlined} {...agrs}>
          {loading ? (
            loadingCom
          ) : (
            <LabelIconButton
              label={label}
              iconLeft={iconLeft}
              iconRight={iconRight}
            />
          )}
        </ButtonGrayStyle>
      );
      break;
    case COLORS.COMMON.MAIN:
      render = (
        <ButtonMainStyle isoutlined={checkIsOutlined} {...agrs}>
          {loading ? (
            loadingCom
          ) : (
            <LabelIconButton
              label={label}
              iconLeft={iconLeft}
              iconRight={iconRight}
            />
          )}
        </ButtonMainStyle>
      );
      break;
    default:
      render = null;
      break;
  }
  return render;
};

ButtonUI.defaultProps = {
  label: null,
  colorButton: COLORS.COMMON.ORANGE,
  iconLeft: null,
  iconRight: null,
};

export const ButtonTextUI = (props: ButtonUIProps) => {
  const {
    loading,
    label,
    colortext,
    iconLeft,
    iconRight,
    ...agrs
  } = props;
  let colortextRGA = ``;
  switch (colortext) {
    case COLORS.COMMON.MAIN:
      colortextRGA = COLORS.COMMON.MAIN_RGBA;
      break;
    case COLORS.COMMON.ORANGE:
      colortextRGA = COLORS.COMMON.ORANGE_RGBA;
      break;
    case COLORS.COMMON.GREEN:
      colortextRGA = COLORS.COMMON.GREEN_RGBA;
      break;
    case COLORS.COMMON.RED:
      colortextRGA = COLORS.COMMON.RED_RGBA;
      break;
    default:
      break;
  }
  return (
    <ButtonTextStyle colortext={colortextRGA} {...agrs}>
      <LabelIconButton
        label={label}
        iconLeft={iconLeft}
        iconRight={iconRight}
      />
    </ButtonTextStyle>
  );
};

ButtonTextUI.defaultProps = {
  label: null,
  colortext: COLORS.COMMON.ORANGE,
  iconLeft: null,
  iconRight: null,
};

export const ButtonLinkUI = (props: ButtonUIProps) => {
  const { label, colortext, iconLeft, iconRight, ...agrs } = props;
  return (
    <ButtonTextStyle islink={1} colortext={colortext} {...agrs}>
      <LabelIconButton
        label={label}
        iconLeft={iconLeft}
        iconRight={iconRight}
      />
    </ButtonTextStyle>
  );
};

ButtonLinkUI.defaultProps = {
  label: null,
  colortext: COLORS.COMMON.MAIN_RGBA,
};

export interface ButtonIconUIProps extends IconButtonProps {
  label?: string;
  colorButton?: string;
}

export const IconButtonStyle = withStyles({
  root: {
    border: `1px solid ${COLORS.COMMON.WHITE_OPA_8}`,
    boxShadow: `0px 8px 16px 0px rgba(51,63,80,0.08)`,
    color: (props: any) =>
      props.colorButton === COLORS.COMMON.ORANGE
        ? `#fff`
        : COLORS.COMMON.MAIN,
    backgroundColor: (props: any) => props.colorButton,
    '&:hover': {
      backgroundColor: (props: any) => props.colorButton,
    },
  },
})(IconButton);

export const ButtonIconUI = (props: ButtonIconUIProps) => {
  const { label, children, colorButton, ...agrs } = props;
  return (
    <IconButtonStyle
      aria-label={label}
      colorButton={colorButton}
      {...agrs}
    >
      {children}
    </IconButtonStyle>
  );
};

ButtonIconUI.defaultProps = {
  children: null,
  colorButton: COLORS.COMMON.WHITE,
};
