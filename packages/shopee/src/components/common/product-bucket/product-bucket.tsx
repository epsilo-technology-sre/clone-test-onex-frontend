import React, { useEffect, useState } from 'react';
import { Box, Typography, makeStyles } from '@material-ui/core';
import { ItemSelected } from '../item-selected';

const useStyle = makeStyles({
  root: {
    background: '#F6F7F8',
    color: '#253746',
    padding: '16px 16px 40px',
    position: 'relative',
  },
  productWrapper: {
    height: 790,
    overflow: 'auto',
  },
});

export interface ItemSlectedProps {
  listItem: any;
  onChange?: any;
}

export const ProductBucket = (props: ItemSlectedProps) => {
  const { listItem, onChange } = props;
  const [selected, setSelected] = useState([])
  const classes = useStyle();

  useEffect(() => {
    if(onChange){
      onChange(selected)
    }
  }, [selected])

  return (
    <Box className={classes.root}>
      <Box pb={1.5}>
        <Typography variant="h6">Product bucket</Typography>
      </Box>
      <Box>
        {listItem.map((item: any) => {
          const itemSelected = selected.some(itemSelect => itemSelect === item.code);
          return <ItemSelected
            key={item.code}
            name={item.name}
            type={item.type}
            currency={item.currency}
            budget={3000}
            code={item.code}
            keyword={item.totalKeyword}
            selected={itemSelected}
            onClick={(e: any) => {
              let itemSelect = [...new Set([...selected, e])];
              if (selected.includes(e)) {
                itemSelect = selected.filter(item => e !== item)
              }
              setSelected(itemSelect)
            }}
          />
        })}
      </Box>
    </Box>
  );
};
