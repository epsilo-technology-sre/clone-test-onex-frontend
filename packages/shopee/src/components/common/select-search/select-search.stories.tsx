import React from 'react'
import Paper from '@material-ui/core/Paper'
import { SelectSearch } from './select-search';

export default {
  title: 'Shopee/Select search'
}

const options = [
  {
    country: "PT",
    'official-name': "The Portuguese Republic",
    name: "Portugal",
    'citizen-names': "Portuguese",
  },
  {
    country: "PW",
    'official-name': "The Republic of Palau",
    name: "Palau",
    'start-date': "1994-10-01",
    'citizen-names': "Palauan",
  }
];

export const main = () => {
  return (
    <Paper>
      <SelectSearch options={options} />
    </Paper>
  )
}
