import { COLORS } from '@ep/shopee/src/constants';
import {
  Box,
  makeStyles,
  TextField,
  Typography,
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import React, { useState } from 'react';
import styled from 'styled-components';
import ErrorIcon from '../../../images/error-icon.svg';

export interface SelectSearchProps {
  options: any;
  value?: any;
  title?: any;
  onChange?: any;
  placeholder?: string;
  error?: string;
  disabled?: boolean;
  inputValue?: string;
}

const useStyles = makeStyles({
  errorMessage: {
    marginTop: 5,
    fontSize: 12,
    color: COLORS.COMMON.RED,
    '& img': {
      marginRight: 5,
    },
  },
  AutoComplete: {
    '& .MuiOutlinedInput-notchedOutline': {
      borderWidth: 2,
    },
  },
  labelInput: {
    marginTop: 8,
    marginBottom: 4,
    fontWeight: 'bold',
    fontSize: 11,
    lineHeight: '16px',
  },
});

const Wrapper = styled.div`
  margin: 8px 0;
`;

const TypographyUI = styled(Typography)`
  margin-bottom: 4px;
  margin-top: 8px;
`;

export const SelectSearch = (props: SelectSearchProps) => {
  const {
    options,
    title,
    placeholder,
    error,
    onChange,
    disabled,
    inputValue,
    value,
  } = props;
  const [open, setOpen] = useState(false);
  const classes = useStyles();

  return (
    <Wrapper>
      <TypographyUI variant="body2" className={classes.labelInput}>
        {title}
      </TypographyUI>
      <Autocomplete
        id="asynchronous-demo"
        open={open}
        onOpen={() => setOpen(true)}
        onClose={() => setOpen(false)}
        getOptionSelected={(option, value) =>
          option.name === value.name.toLowerCase()
        }
        getOptionLabel={(option) => option.name}
        options={options}
        onChange={(event: any, newValue: any) => {
          onChange(newValue || '');
        }}
        inputValue={inputValue}
        value={value}
        disabled={disabled}
        disableClearable={true}
        renderInput={(params) => (
          <TextField
            {...params}
            placeholder={placeholder}
            variant="outlined"
            className={classes.AutoComplete}
            size="small"
            InputProps={{
              ...params.InputProps,
              endAdornment: params.InputProps.endAdornment,
            }}
          />
        )}
      />
      {error && (
        <Box className={classes.errorMessage}>
          <img src={ErrorIcon} height={9} width={9} alt="error" />
          {error}
        </Box>
      )}
    </Wrapper>
  );
};
