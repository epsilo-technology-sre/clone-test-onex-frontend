import { TooltipUI } from '@ep/one/src/components/common/tooltip';
import { Box, Grid, makeStyles, Typography } from '@material-ui/core';
import CancelIcon from '@material-ui/icons/Cancel';
import React from 'react';
import { MATCH_TYPE_LABEL } from '../../../constants';
import { CurrencyCode } from '../currency-code';

const useStyle = makeStyles({
  root: {
    background: '#F6F7F8',
    color: '#253746',
    padding: '16px 16px 40px',
    position: 'relative',
  },
  productWrapper: {
    height: 495,
    overflow: 'auto',
  },
  groupName: {
    fontSize: 11,
    fontWeight: 'bold',
    color: '#596772',
  },
  item: {
    position: 'relative',
    background: '#ffffff',
    fontSize: 12,
    padding: 8,
    marginBottom: 4,
    '& .line2': {
      color: '#596772',
      fontSize: 10,
    },
    '& .invalid-keyword': {
      color: '#D4290D',
      fontSize: 10,
    },
    '& .remove': {
      display: 'none',
      position: 'absolute',
      top: 2,
      right: 2,
      color: '#000000',
      fontSize: 20,
      cursor: 'pointer',
    },
    '&:hover .remove': {
      display: 'block',
    },
  },
  itemName: {
    fontSize: 12,
    fontWeight: 600,
    color: '#253746',
    width: '100%',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
  clearAll: {
    position: 'absolute',
    left: 16,
    bottom: 16,
    cursor: 'pointer',
    fontWeight: 600,
  },
});

export interface ItemSlectedProps {
  keywords: any[];
  onChange?: any;
  noMatchType?: boolean;
  onRemoveItem?: (keyword: any) => void;
  onRemoveAll?: () => void;
}

export const SelectedKeyword = (props: ItemSlectedProps) => {
  const {
    keywords,
    noMatchType = false,
    onRemoveItem,
    onRemoveAll,
  } = props;
  const classes = useStyle();
  return (
    <Box className={classes.root}>
      <Box pb={1.5}>
        <Typography variant="h6">Selected keyword</Typography>
      </Box>
      <Box className={classes.productWrapper}>
        {keywords.map((keyword: any) => {
          return (
            <Box className={classes.item} key={keyword.keywordId}>
              <Grid container justify="space-between">
                <Grid item xs={6}>
                  <Typography
                    variant="body2"
                    className={classes.itemName}
                    title={keyword.keywordName}
                  >
                    {keyword.keywordName}
                  </Typography>
                  {keyword.isValid === false && (
                    <TooltipUI title="This Keyword is reserved & can't be used">
                      <Typography
                        variant="subtitle2"
                        className="invalid-keyword"
                      >
                        {'Invalid keyword'}
                      </Typography>
                    </TooltipUI>
                  )}
                </Grid>
                <Grid item xs={6}>
                  <Box style={{ textAlign: 'right' }}>
                    <Box>
                      <CurrencyCode currency={keyword.currency} />
                      {keyword.biddingPrice}
                    </Box>
                    {!noMatchType && (
                      <Typography
                        variant="body2"
                        align="right"
                        className="line2"
                      >
                        {MATCH_TYPE_LABEL[keyword.matchType]}
                      </Typography>
                    )}
                  </Box>
                </Grid>
              </Grid>
              <CancelIcon
                className="remove"
                onClick={() => onRemoveItem(keyword)}
              />
            </Box>
          );
        })}
      </Box>
      <Typography
        variant="body2"
        className={classes.clearAll}
        onClick={onRemoveAll}
      >
        Clear All
      </Typography>
    </Box>
  );
};
