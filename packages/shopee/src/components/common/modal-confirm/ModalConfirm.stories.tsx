import React, { useState } from 'react';
import { ModalConfirmUI } from './ModalConfirm';
import { ButtonUI } from '../button';

export default {
  title: 'Shopee/Modal Confirm',
};

export const Primary = () => {
  const [open, setOpen] = useState(false);
  return (
    <>
      <ButtonUI
        onClick={() => setOpen(!open)}
        label="Show modal"
        size="small"
        colorButton="#F6F7F8"
      />
      <ModalConfirmUI
        open={open}
        onSubmit={() => {
          setOpen(!open);
          console.log('submit');
        }}
        onClose={() => setOpen(false)}
        labelSubmit="Active"
        title="Activated Keywords(s)?"
        content="Activated Keywords will run ads in this chanel"
      />
    </>
  );
};
