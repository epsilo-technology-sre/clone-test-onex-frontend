import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import React from 'react';
import { ButtonUI } from '../button';

export interface ModalConfirmUIProps {
  open: boolean;
  onSubmit: () => any;
  onClose: () => any;
  labelSubmit: string;
  disabled?: boolean;
  title: string;
  content?: string;
}

const useStyle = makeStyles({
  content: {
    maxWidth: 400,
  },
  actions: {
    padding: '0 24px 24px',
  },
});

export function ModalConfirmUI(props: ModalConfirmUIProps) {
  const {
    open,
    onSubmit,
    onClose,
    labelSubmit,
    disabled,
    title,
    content,
  } = props;
  const classes = useStyle();
  return (
    <Dialog aria-labelledby="simple-dialog-title" open={open}>
      <DialogTitle id="simple-dialog-title">{title}</DialogTitle>
      {content && (
        <DialogContent className={classes.content}>
          {content}
        </DialogContent>
      )}
      <DialogActions className={classes.actions}>
        <ButtonUI
          onClick={onClose}
          label="Cancel"
          size="small"
          colorButton="#F6F7F8"
        />
        <ButtonUI
          onClick={onSubmit}
          disabled={disabled}
          label={labelSubmit}
          size="small"
          variant="contained"
        />
      </DialogActions>
    </Dialog>
  );
}
