import React from 'react';
import { ButtonCell } from '../table-cell';
import DeleteIcon from '@material-ui/icons/Delete';
import SvgIcon from '@material-ui/core/SvgIcon';


export const MODAL_RULE_COLUMNS = [
  {
    Header: 'Type of performance',
    id: 'typeOfPerformance',
    accessor: 'typeOfPerformance',
    disableSortBy: true,
    width: 152,
  },
  {
    Header: 'Period',
    id: 'conditionPeriod',
    disableSortBy: true,
    accessor: 'conditionPeriod',
    width: 104,
  },
  {
    Header: 'Operator',
    id: 'operator',
    accessor: 'operator',
    disableSortBy: true,
    width: 72,
  },
  {
    Header: 'Value',
    id: 'value',
    accessor: 'value',
    disableSortBy: true,
    width: 72,
  },
  {
    Header: '',
    id: 'delete',
    accessor: (row: any) => ({
      rule: row,
      label: <SvgIcon fontSize="small">
        <DeleteIcon />
      </SvgIcon>,
    }),
    disableSortBy: true,
    width: 48,
    Cell: ButtonCell
  },
];
