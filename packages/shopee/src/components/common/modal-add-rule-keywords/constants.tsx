export const dataSearch: any = [
  { name: 'Portugal', value: 'zortugal' },
  { name: 'Palauan', value: 'zalauan' },
];

export const dataAction: any = [
  { name: 'Deactivate Product-Keyword', value: 'deactivate' },
  { name: 'Activate Product-Keyword', value: 'activate' },
  { name: 'Increase Bidding Price', value: 'increase' },
  { name: 'Decrease Bidding Price', value: 'decrease' },
];

export function getTypeOfPerformance(type) {
  switch (type) {
    case 'activate' :
    case 'deactivate' :
      return [
        { name: 'Total GMV of Product', value: 'Total GMV of Product' },
        { name: 'Total Item Sold of Product', value: 'Total Item Sold of Product' },
        { name: 'Ads Item Sold', value: 'Ads Item Sold' },
        { name: 'Ads GMV', value: 'Ads GMV' },
        { name: 'Cost', value: 'Cost' },
        { name: 'CTR', value: 'CTR' },
        { name: 'CR', value: 'CR' },
        { name: 'Impression', value: 'Impression' },
        { name: 'Click', value: 'Click' },
        { name: '% Discount of Product', value: '% Discount of Product' },
        { name: 'CPC (Cost per click)', value: 'CPC (Cost per click)' },
      ];
    default:
      return [
        { name: 'Ranking', value: 'Ranking' },
        { name: 'CIR', value: 'CIR' },
        { name: '% Discount of Product', value: '% Discount of Product' },
        { name: 'CPC (Cost per click)', value: 'CPC (Cost per click)' },
        { name: 'Ads GMV', value: 'Ads GMV' },
        { name: 'ROAS', value: 'ROAS' },
      ];
  }
}

export const dataOperator: any = [
  { name: '=', value: '=' },
  { name: '>', value: '>' },
  { name: '<', value: '<' },
  { name: '>=', value: '>=' },
  { name: '<=', value: '<=' },
];

export const dataPeriod: any = [
  { name: 'Current day', value: 1 },
  { name: 'Last 2 Days + current day', value: 3 },
  { name: 'Last 6 Days + current day', value: 7 },
  { name: 'Last 13 Days + current day', value: 14 },
  { name: 'Last 29 Days + current day', value: 30 },
];

