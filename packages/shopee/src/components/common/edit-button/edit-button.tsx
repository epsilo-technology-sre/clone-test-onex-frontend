import React from 'react'
import Box from '@material-ui/core/Box'
import { makeStyles } from '@material-ui/core/styles'
import EditIcon from '@material-ui/icons/Edit';

const useStyles = makeStyles({
  editBtn: {
    position: 'absolute',
    top: 10,
    right: 10,
    background: '#253746',
    color: '#ffffff',
    borderRadius: '50%',
    cursor: 'pointer',
    width: 24,
    height: 24,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  }
})

export const EditButton = () => {
  const classes = useStyles();
  return (
    <Box className="edit-btn">
      <Box className={`${classes.editBtn}`}>
        <EditIcon style={{fontSize: '1rem'}}/>
      </Box>
    </Box>
  )
}