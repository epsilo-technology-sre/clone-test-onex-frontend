import React from 'react'
import { EditButton } from './edit-button'

export default {
  title: 'Shopee/EditButton'
}

export const Primary = () => {
  return (
    <EditButton />
  )
}