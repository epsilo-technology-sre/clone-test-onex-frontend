import {
  Dialog,
  Grid,
  IconButton,
  Typography,
} from '@material-ui/core';
import MuiDialogActions from '@material-ui/core/DialogActions';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import {
  createStyles,
  makeStyles,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import React from 'react';
import { COLORS } from '../../../constants';
import { ButtonLinkUI } from '../button';
import { TableUINoCheckbox } from '../table';
import { LogsRuleFilters } from './logs-rule-filters';

export interface LogsRuleProps {
  open: boolean;
  handleClose: Function;
  handleChangeDateRange: Function;
  handleChangeActions: Function;
  actions: any[];
  selectedTimeRange: any;
  selectedActions: any;
  tableHeaders: any;
  logRule: any;
  ruleInfo: {
    parentItem: any;
    item: any;
  };
  pagination: any;
  onSorting: Function;
  onChangePage: Function;
  onChangePageSize: Function;
  handleSearchKeyUp: Function;
}

export const LogsRule = (props: LogsRuleProps) => {
  const {
    open,
    handleClose,
    handleChangeDateRange,
    handleChangeActions,
    actions,
    selectedTimeRange,
    selectedActions,
    tableHeaders,
    logRule,
    pagination,
    onSorting,
    onChangePage,
    onChangePageSize,
    handleSearchKeyUp,
    ruleInfo,
  } = props;
  const classes = useStyles();

  return (
    <React.Fragment>
      <Dialog
        classes={classes}
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle
          id="customized-dialog-title"
          onClose={handleClose}
          className={classes.dialogTitle}
        >
          Logs
          <p className={classes.helperTextTitle}>
            The data only showing from the previous 30 days.
          </p>
        </DialogTitle>
        <DialogContent>
          <Grid container>
            <Grid item md={4}>
              <div className={classes.logsTitle}>
                {ruleInfo.parentItem.name}
              </div>
              <div className={classes.logsContent}>
                {ruleInfo.parentItem.value}
              </div>
            </Grid>
            <Grid item md={4}>
              <div className={classes.logsTitle}>
                {ruleInfo.item.name}
              </div>
              <div className={classes.logsContent}>
                {ruleInfo.item.value}
              </div>
            </Grid>
            <Grid item md={4}>
              <div className={classes.logsTitle}>ID</div>
              <div className={classes.logsContent}>
                {ruleInfo.item.id}
              </div>
            </Grid>
          </Grid>
          <LogsRuleFilters
            actions={actions}
            selectedTimeRange={selectedTimeRange}
            selectedActions={selectedActions}
            onChangeDateRange={handleChangeDateRange}
            onChangeActions={handleChangeActions}
            handleSearchKeyUp={handleSearchKeyUp}
          />
          <TableUINoCheckbox
            columns={tableHeaders}
            rows={logRule}
            resultTotal={pagination.itemCount}
            page={pagination.page}
            pageSize={pagination.limit}
            onSort={onSorting}
            onChangePage={onChangePage}
            onChangePageSize={onChangePageSize}
            className={'log-rule-filter'}
          />
        </DialogContent>
        <DialogActions>
          <Grid container justify="flex-end">
            <ButtonLinkUI
              label="Cancel"
              size="small"
              colortext="#f16145"
              onClick={handleClose}
            />
          </Grid>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
};

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });

const useStyles = makeStyles(() => ({
  paper: {
    maxWidth: 1224,
  },
  wrapForm: { maxHeight: 208, overflow: 'auto' },
  wrapConditions: {
    padding: 16,
    border: '2px solid #e4e7e9',
    borderRadius: 4,
    marginTop: 8,
  },
  labelInput: {
    marginTop: 8,
    marginBottom: 4,
    fontWeight: 'bold',
    fontSize: 11,
    lineHeight: '16px',
  },
  error: {
    color: COLORS.COMMON.RED,
    fontSize: 12,
  },
  input: {
    '& .MuiOutlinedInput-notchedOutline': {
      border: '2px solid #e4e7e9',
    },
  },
  dialogTitle: {
    padding: '16px 16px 0 16px',
  },
  helperTextTitle: {
    fontSize: 10,
    lineHeight: '12px',
    color: '#596772',
    margin: 0,
  },
  logsTitle: {
    fontSize: 12,
    lineHeight: '16px',
    color: '#596772',
  },
  logsContent: {
    fontWeight: 600,
    fontSize: 14,
    lineHeight: '20px',
    color: '#253746',
  },
}));

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose?: () => void;
  classes: any;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle
      disableTypography
      className={classes.root}
      {...other}
    >
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);
