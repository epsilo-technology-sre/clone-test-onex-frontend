import { Button, Paper } from '@material-ui/core';
import differenceWith from 'lodash/differenceWith';
import isEqual from 'lodash/isEqual';
import React from 'react';
import { LogsRule } from './logs-rule';
import { initColumns } from './logs-rule-columns';
export default {
  title: 'Shopee/Logs rule',
};

const ruleList = Array.from({ length: 5 }, (_, key) => {
  const random = Math.floor(Math.random() * 4 + 1);
  return {
    ruleName: `Product ${key}`,
    action: 'Action type',
    ruleValue: 10000,
    currency: 'VND',
    minBid: 10000 * (random - 1),
    maxBid: 10000 * random,
    create: '31/12/2020',
    condition: Array.from({ length: random }, (_, key) => ({
      typeOfPerformance: 'Ads Item Sold',
      period: 'Last x days',
      operator: '=',
      value: 20,
      checkpoint: 20,
    })),
  };
});

export const Main = () => {
  const ref = React.useRef({
    pagination: null,
    sortBy: [],
  });

  const [openLogsRule, setOpenLogsRule] = React.useState(true);

  const [actions, setActions] = React.useState([]);
  const [selectedTimeRange, setSelectedTimeRange] = React.useState(
    '',
  );
  const [selectedActions, setSelectedActions] = React.useState('');

  const [pagination, setPagination] = React.useState<any>({
    page: 1,
  });

  const getLogsData = (argParams: any = {}) => {
    console.log('Function: getLogsData', argParams);
  };

  const handleClose = () => {
    setOpenLogsRule(!openLogsRule);
  };

  const handleChangeDateRange = ({ startDate, endDate }: any) => {
    console.log('Function: handleChangeDateRange', {
      startDate,
      endDate,
    });
  };

  const handleChangeActions = (selected: any) => {
    console.log('Function: handleChangeActions', selected);
  };

  const handleSorting = (sortBy: any) => {
    const isDifference =
      differenceWith(sortBy, ref.current.sortBy, isEqual).length > 0;
    if (isDifference) {
      ref.current.sortBy = sortBy;
      getLogsData();
    }
  };

  const handleChangePage = (page: number) => {
    getLogsData();
  };

  const handleSearchKeyUp = (event: any) => {
    if (event.key === 'Enter') {
      console.log('Function: handleSearchKeyUp', event.target.value);
    }
  };

  const handleChangePageSize = (value: number) => {
    console.log('Function: handleChangePageSize', value);
    getLogsData();
  };

  return (
    <Paper>
      <Button onClick={() => setOpenLogsRule(!openLogsRule)}>
        open
      </Button>
      <LogsRule
        open={openLogsRule}
        handleClose={handleClose}
        handleChangeDateRange={handleChangeDateRange}
        handleChangeActions={handleChangeActions}
        actions={actions}
        selectedTimeRange={selectedTimeRange}
        selectedActions={selectedActions}
        tableHeaders={initColumns()}
        logRule={ruleList}
        pagination={pagination}
        onSorting={handleSorting}
        onChangePage={handleChangePage}
        onChangePageSize={handleChangePageSize}
        handleSearchKeyUp={handleSearchKeyUp}
        ruleInfo={{
          parentItem: {
            name: 'Product',
            value: 'Product A',
          },
          item: {
            id: 1234,
            name: 'Keyword',
            value: 'Keyword B',
          },
        }}
      />
    </Paper>
  );
};
