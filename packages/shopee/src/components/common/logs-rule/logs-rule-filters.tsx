import {
  Box,
  Grid,
  InputAdornment,
  makeStyles,
  OutlinedInput,
  withStyles,
} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import moment from 'moment';
import React from 'react';
import { RULE_LOG_AFTER_DAYS } from '../../../constants';
import { DateRangePickerUI } from '../date-range-picker';
import { MultiSelectUI } from '../multi-select';

export interface LogsRuleFiltersProps {
  actions: any;
  selectedTimeRange: any;
  selectedActions: any;
  onChangeDateRange: Function;
  onChangeActions: Function;
  handleSearchKeyUp: Function;
}

export const LogsRuleFilters = (props: LogsRuleFiltersProps) => {
  const {
    actions,
    selectedTimeRange,
    selectedActions,
    onChangeDateRange,
    onChangeActions,
    handleSearchKeyUp,
  } = props;

  const classes = useStyles();

  return (
    <Box my={2}>
      <Grid container justify="space-between" alignItems="center">
        <Grid item>
          <Grid container alignItems="center">
            <Grid item>
              <SearchBox
                id="rule-log-searchbox"
                placeholder="Search"
                onKeyUp={handleSearchKeyUp}
                startAdornment={
                  <InputAdornment position="start">
                    <SearchIcon fontSize="small" />
                  </InputAdornment>
                }
                labelWidth={0}
                autoComplete="off"
              />
            </Grid>
            <Grid item>
              <DateRangePickerUI
                startDate={selectedTimeRange.startDate}
                endDate={selectedTimeRange.endDate}
                onApply={onChangeDateRange}
                isOutsideRange={(day) => {
                  return !day.isBetween(
                    moment().add(-RULE_LOG_AFTER_DAYS, 'days'),
                    moment(),
                  );
                }}
              />
            </Grid>
            <Grid item>
              <MultiSelectUI
                prefix="Actions"
                suffix="actions"
                items={actions}
                selectedItems={selectedActions}
                onSaveChange={onChangeActions}
              />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Box>
  );
};

const SearchBox = withStyles({
  root: {
    marginRight: 5,
    width: 320,
    '&:hover .MuiOutlinedInput-notchedOutline': {
      borderColor: '#C2C7CB',
    },
  },
  input: {
    padding: '6.5px 14px',
  },
})(OutlinedInput);

const useStyles = makeStyles({
  root: {
    background: '#F6F7F8',
    borderRadius: 4,
    padding: '0.5rem',
    marginLeft: 5,
    '& svg': {
      width: 18,
      height: 18,
    },
  },
  statusWrapper: {
    background: '#F6F7F8',
    borderRadius: 4,
  },
  statusItem: {
    textTransform: 'capitalize',
    fontSize: 14,
  },
});
