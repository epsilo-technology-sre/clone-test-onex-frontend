import { TableUI, PureTableUI } from './table';
import { TableUINoCheckbox } from './table-no-checkbox';
import { TableUICheckboxRight } from './table-checkbox-placement-right';
import { TableUIPermission } from './table-permission';
import { TableUISticky } from './table-sticky';

export {
  TableUI,
  PureTableUI,
  TableUINoCheckbox,
  TableUISticky,
  TableUICheckboxRight,
  TableUIPermission,
};
