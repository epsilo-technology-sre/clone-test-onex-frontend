import ArrowDownIcon from '@ep/shopee/src/images/arrow-down.svg';
import ArrowUpIcon from '@ep/shopee/src/images/arrow-up.svg';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { sum } from 'lodash';
import React, { useEffect } from 'react';
import { ScrollSync, ScrollSyncPane } from 'react-scroll-sync';
import {
  useBlockLayout,
  useRowSelect,
  useSortBy,
  useTable,
} from 'react-table';
import { useSticky } from 'react-table-sticky';
import loading from './bar-loading.gif';
import { CheckboxUI } from './checkbox';
import { Styles } from './style-sticky';

type TableProps = {
  selectedIds: any[];
  columns: any[];
  getRowId: (
    row: any,
    relativeIndex: string,
    parentIndex?: string,
  ) => string;
  [key: string]: any;
};

const useStyle = makeStyles({
  arrowUp: {
    color: '#3F4F5C',
    position: 'absolute',
    top: 18,
    right: 10,
  },
  arrowDown: {
    color: '#3F4F5C',
    position: 'absolute',
    top: 18,
    right: 10,
  },
  stickyGroup: {
    position: 'sticky',
    display: 'inline-flex',
    boxSizing: 'border-box',
    left: 0,
    zIndex: 1,
    boxShadow: '4px 0px 5px -3px #ccc',
  },
  float: {
    borderBottom: '2px solid #ededed',
  },
});

export const PureTableStickyUI = (props: TableProps) => {
  const classes = useStyle();
  const columns = React.useMemo(() => props.columns, []);
  const {
    noData,
    onSort,
    selectedIds,
    onSelectAll,
    onSelectItem,
    getRowId,
    className,
  } = props;

  const tableInstance = useTable(
    {
      columns,
      data: props.rows,
      manualSortBy: true,
      initialState: {
        selectedRowIds: (selectedIds || []).reduce(
          (acc, i) => ({
            ...acc,
            [i]: true,
          }),
          {},
        ),
      },
      useControlledState: (state: any) => {
        return React.useMemo(() => {
          return {
            ...state,
            selectedRowIds: (selectedIds || []).reduce(
              (acc, i) => ({
                ...acc,
                [i]: true,
              }),
              {},
            ),
          };
        }, [state, selectedIds]);
      },
      getRowId: getRowId
        ? getRowId
        : React.useCallback((row, index) => index, []),
    },
    useSortBy,
    useBlockLayout,
    // useResizeColumns,
    useSticky,
    useRowSelect,
    (hooks: any) => {
      hooks.visibleColumns.push((columns: any) => {
        return [
          {
            id: 'selection',
            Header: (header: any) => {
              const {
                isAllRowsSelected,
                getToggleAllRowsSelectedProps,
              } = header;
              return (
                <CheckboxUI
                  {...getToggleAllRowsSelectedProps()}
                  onChange={() => {
                    onSelectAll(!isAllRowsSelected);
                  }}
                />
              );
            },
            Cell: ({ row }: any) => {
              const checkboxProps = row.getToggleRowSelectedProps();
              return (
                <CheckboxUI
                  {...checkboxProps}
                  onChange={() => {
                    onSelectItem(row.original, !row.isSelected);
                  }}
                />
              );
            },
            sticky: 'left',
            disableSortBy: true,
            width: 50,
            alwaysEnable: true,
          },
          ...columns,
        ];
      });
    },
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    selectedFlatRows,
    state: { sortBy },
  } = tableInstance;

  useEffect(() => {
    onSort(sortBy);
  }, [sortBy, onSort]);

  const headerRef = React.useRef();
  const tableRef = React.useRef();
  (function useLater() {
    let lastKnownPosY = 0;
    let tableTop = 0;
    let ticking = false;
    let tid = 0;

    let makeHeaderSticky: (e: Event) => void;

    if (headerRef.current && tableRef.current) {
      let headerDom = headerRef.current as HTMLElement;
      let tableDom = tableRef.current as HTMLElement;
      let trect = tableDom.getBoundingClientRect();
      tableTop = window.scrollY + trect.top;

      let observer = new window.ResizeObserver(() => {
        let trect = tableDom.getBoundingClientRect();
        tableTop = window.scrollY + trect.top;
        makeHeaderSticky(new Event('scroll-fake'));
      });

      observer.observe(document.body);

      makeHeaderSticky = function makeHeaderSticky() {
        lastKnownPosY = window.scrollY;
        if (!ticking) {
          tid = window.setTimeout(
            () =>
              window.requestAnimationFrame(() => {
                if (lastKnownPosY > tableTop) {
                  let top = 56 - (tableTop - lastKnownPosY) + 'px';
                  headerDom.style.transform = `translateY(${top})`;
                  headerDom.classList.add(classes.float);
                } else {
                  headerDom.style.transform = `translateY(0px)`;
                  headerDom.classList.remove(classes.float);
                }
                ticking = false;
              }),
            500,
          );
          ticking = true;
        }
      };

      window.addEventListener('scroll', makeHeaderSticky);
      return () => {
        window.removeEventListener('scroll', makeHeaderSticky);
        window.clearTimeout(tid);
        observer.disconnect();
      };
    }
  });

  const getDirectionIcon = (column: any) => {
    if (column.isSorted) {
      return column.isSortedDesc ? (
        <img
          src={ArrowDownIcon}
          className={classes.arrowDown}
          width={12}
          height={12}
        />
      ) : (
        <img
          src={ArrowUpIcon}
          className={classes.arrowUp}
          width={12}
          height={12}
        />
      );
    }
    return '';
  };

  let batchCount = Array(Math.ceil(rows.length / 10)).fill(0);

  return (
    <Styles>
      <ScrollSync>
        <div
          {...getTableProps()}
          className={clsx(
            'table sticky',
            { [className]: className },
            'ngsticky',
          )}
          ref={tableRef}
        >
          <div className="header" ref={headerRef}>
            <ScrollSyncPane>
              <div className="scrollable">
                {headerGroups.map((headerGroup: any) => {
                  let stickyCells = headerGroup.headers.filter(
                    (i) => i.sticky === 'left',
                  );

                  let groupWidth = sum(
                    stickyCells.map((i) => i.totalWidth),
                  );

                  return (
                    <div
                      {...headerGroup.getHeaderGroupProps()}
                      className="tr"
                    >
                      <div
                        className={classes.stickyGroup}
                        style={{ width: groupWidth }}
                      >
                        {stickyCells.map((cell: any) => (
                          <div
                            {...cell.getHeaderProps()}
                            data-sticky-td={undefined}
                            data-sticky-last-left-td={undefined}
                            className={`th`}
                          >
                            {cell.render('Header')}
                          </div>
                        ))}
                      </div>
                      {headerGroup.headers
                        .slice(stickyCells.length)
                        .map((column: any) => (
                          <div
                            {...column.getHeaderProps(
                              column.getSortByToggleProps(),
                            )}
                            className={`th`}
                          >
                            {column.render('Header')}
                            {/* <div
                    {...column.getResizerProps()}
                    className="resizer"
                  ></div> */}
                            {getDirectionIcon(column)}
                          </div>
                        ))}
                    </div>
                  );
                })}
              </div>
            </ScrollSyncPane>
          </div>
          <ScrollSyncPane>
            <div {...getTableBodyProps()} className="body">
              {batchCount.map((_, index) => {
                let brows = rows.slice(index * 10, (index + 1) * 10);
                return (
                  <IncrementalRows
                    key={brows.map((i) => i.id).join('-')}
                    rows={brows}
                    prepareRow={prepareRow}
                  />
                );
              })}
              {rows.length === 0 && (
                <div className="no-data">{noData}</div>
              )}
            </div>
          </ScrollSyncPane>
        </div>
      </ScrollSync>
    </Styles>
  );
};

function IncrementalRows({ rows, prepareRow }) {
  const classes = useStyle();
  const [rendered, setRendered] = React.useState(0);
  React.useEffect(() => {
    console.info('effect...');
    let frameid = null;
    let tid = null;
    frameid = window.requestAnimationFrame(() => {
      tid = window.setTimeout(() => {
        setRendered(1);
      }, 100);
    });

    return () => {
      window.cancelAnimationFrame(frameid);
      window.clearTimeout(tid);
    };
  }, []);

  if (!rendered)
    return (
      <div style={{ marginLeft: '3.9em' }}>
        <img src={loading} style={{ height: '20px' }} />
      </div>
    );

  const enableColumns = ['Status', 'Campaign', 'Product'];

  return (
    <React.Fragment>
      {rows.map((row: any) => {
        prepareRow(row);
        let stickyCells = row.cells.filter(
          (i) => i.column.sticky === 'left',
        );

        let groupWidth = sum(stickyCells.map((i) => i.totalWidth));

        return (
          <div {...row.getRowProps()} className="tr">
            <div
              className={classes.stickyGroup}
              style={{ width: groupWidth }}
            >
              {stickyCells.map((cell: any) => (
                <div
                  {...cell.getCellProps()}
                  style={{
                    ...cell.getCellProps().style,
                    zIndex: undefined,
                    position: undefined,
                    left: undefined,
                  }}
                  data-sticky-td={undefined}
                  data-sticky-last-left-td={undefined}
                  className={clsx('td', {
                    disabled: cell.row.original._isDisabled,
                    status:
                      enableColumns.includes(cell.column.Header) ||
                      cell.column.alwaysEnable,
                  })}
                >
                  {cell.render('Cell')}
                </div>
              ))}
            </div>
            {row.cells.slice(stickyCells.length).map((cell: any) => (
              <div
                {...cell.getCellProps()}
                className={clsx('td', {
                  disabled: cell.row.original._isDisabled,
                  status:
                    enableColumns.includes(cell.column.Header) ||
                    cell.column.alwaysEnable,
                })}
              >
                {cell.render('Cell')}
              </div>
            ))}
          </div>
        );
      })}
    </React.Fragment>
  );
}
