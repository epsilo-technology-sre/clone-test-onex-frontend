import React from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import { withStyles } from '@material-ui/core/styles';

const GreenCheckbox = withStyles({
  root: {
    color: '#D3D7DA',
    '&$checked': {
      color: '#0BA373',
    },
    '&$checked.Mui-disabled': {
      color: 'rgba(0, 0, 0, 0.26)',
    },
  },
  checked: {},
})((props) => <Checkbox color="default" {...props} />);

export const CheckboxUI = React.forwardRef(
  ({ indeterminate, ...rest }, ref) => {
    const defaultRef = React.useRef();
    const resolveRef = ref || defaultRef;

    React.useEffect(() => {
      if (resolveRef.current) {
        resolveRef.current.indeterminate = indeterminate;
      }
    }, [resolveRef, indeterminate]);

    return (
      <>
        <GreenCheckbox size="small" ref={resolveRef} {...rest} />
      </>
    );
  },
);
