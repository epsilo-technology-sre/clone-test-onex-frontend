import ArrowDownIcon from '@ep/shopee/src/images/arrow-down.svg';
import ArrowUpIcon from '@ep/shopee/src/images/arrow-up.svg';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import React, { useEffect } from 'react';
import {
  useBlockLayout,
  useRowSelect,
  useSortBy,
  useTable,
} from 'react-table';
import { useSticky } from 'react-table-sticky';
import { TableFilterUI } from '../table-filter';
import { CheckboxUI } from './checkbox';
import { Styles } from './style';
import { PureTableStickyUI } from './table-ui-sticky';

const useStyle = makeStyles({
  arrowUp: {
    color: '#3F4F5C',
    position: 'absolute',
    top: 18,
    right: 10,
  },
  arrowDown: {
    color: '#3F4F5C',
    position: 'absolute',
    top: 18,
    right: 10,
  },
});

type TableProps = {
  selectedIds: any[];
  columns: any[];
  getRowId: (
    row: any,
    relativeIndex: string,
    parentIndex?: string,
  ) => string;
  [key: string]: any;
};

export const PureTableUI = (props: TableProps) => {
  const classes = useStyle();
  const columns = React.useMemo(() => props.columns, []);
  const {
    noData,
    onSort,
    selectedIds,
    onSelectAll,
    onSelectItem,
    getRowId,
    className,
    enabledSticky = false,
  } = props;



  const tableInstance = useTable(
    {
      columns,
      data: props.rows,
      manualSortBy: true,
      initialState: {
        selectedRowIds: (selectedIds || []).reduce(
          (acc, i) => ({
            ...acc,
            [i]: true,
          }),
          {},
        ),
      },
      useControlledState: (state: any) => {
        return React.useMemo(() => {
          return {
            ...state,
            selectedRowIds: (selectedIds || []).reduce(
              (acc, i) => ({
                ...acc,
                [i]: true,
              }),
              {},
            ),
          };
        }, [state, selectedIds]);
      },
      getRowId: getRowId
        ? getRowId
        : React.useCallback((row, index) => index, []),
    },
    useSortBy,
    useBlockLayout,
    // useResizeColumns,
    useSticky,
    useRowSelect,
    (hooks: any) => {
      hooks.visibleColumns.push((columns: any) => {
        return [
          {
            id: 'selection',
            Header: (header: any) => {
              const {
                isAllRowsSelected,
                getToggleAllRowsSelectedProps,
              } = header;
              return (
                <CheckboxUI
                  {...getToggleAllRowsSelectedProps()}
                  onChange={() => {
                    onSelectAll(!isAllRowsSelected);
                  }}
                />
              );
            },
            Cell: ({ row }: any) => {
              const checkboxProps = row.getToggleRowSelectedProps();
              if (row.original._isDisabled) {
                checkboxProps.checked = false;
              }

              return (
                <CheckboxUI
                  {...checkboxProps}
                  onChange={() => {
                    if (!row.original._isDisabled) {
                      onSelectItem(row.original, !row.isSelected);
                    }
                  }}
                  disabled={row.original._isDisabled}
                />
              );
            },
            sticky: 'left',
            disableSortBy: true,
            width: 50,
          },
          ...columns,
        ];
      });
    },
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    selectedFlatRows,
    state: { sortBy },
  } = tableInstance;

  useEffect(() => {
    onSort(sortBy);
  }, [sortBy, onSort]);

  const headerRef = React.useRef();
  const tableRef = React.useRef();

  const getDirectionIcon = (column: any) => {
    if (column.isSorted) {
      return column.isSortedDesc ? (
        <img
          src={ArrowDownIcon}
          className={classes.arrowDown}
          width={12}
          height={12}
        />
      ) : (
        <img
          src={ArrowUpIcon}
          className={classes.arrowUp}
          width={12}
          height={12}
        />
      );
    }
    return '';
  };

  return (
    <Styles>
      <div
        {...getTableProps()}
        className={clsx('table sticky', { [className]: className })}
        ref={tableRef}
      >
        <div className="header" ref={headerRef}>
          {headerGroups.map((headerGroup: any) => (
            <div
              {...headerGroup.getHeaderGroupProps()}
              className="tr"
            >
              {headerGroup.headers.map((column: any) => (
                <div
                  {...column.getHeaderProps(
                    column.getSortByToggleProps(),
                  )}
                  className={`th ${classes.tableHeader}`}
                >
                  {column.render('Header')}
                  {/* <div
                    {...column.getResizerProps()}
                    className="resizer"
                  ></div> */}
                  {getDirectionIcon(column)}
                </div>
              ))}
            </div>
          ))}
        </div>
        <div {...getTableBodyProps()} className="body">
          {rows.length > 0 &&
            rows.map((row: any) => {
              prepareRow(row);
              return (
                <div {...row.getRowProps()} className="tr">
                  {row.cells.map((cell: any) => (
                    <div
                      {...cell.getCellProps()}
                      className={clsx('td', {
                        disabled: cell.row.original._isDisabled,
                        status: cell.column.Header === 'Status',
                      })}
                    >
                      {cell.render('Cell')}
                    </div>
                  ))}
                </div>
              );
            })}
          {rows.length === 0 && (
            <div className="no-data">{noData}</div>
          )}
        </div>
      </div>

      {/* <pre>
        <code>
          {JSON.stringify(
            { selectedFlatRows: selectedFlatRows.map(row => row.original) },
            null,
            2
          )}
        </code>
      </pre> */}
    </Styles>
  );
};

export const TableUI = (props: any) => {
  const {
    columns,
    rows,
    resultTotal,
    page,
    pageSize,
    onChangePage,
    onChangePageSize,
    onSort,
    noData,
    onSelectItem,
    onSelectAll,
    selectedIds,
    getRowId,
    className,
    enabledSticky,
  } = props;

  return (
    <div>
      <PureTableUI
        columns={columns}
        className={className}
        rows={rows}
        noData={noData}
        onSort={onSort}
        onSelectItem={onSelectItem}
        onSelectAll={onSelectAll}
        selectedIds={selectedIds}
        getRowId={getRowId}
        enabledSticky={enabledSticky}
      />
      <TableFilterUI
        resultTotal={resultTotal}
        page={page}
        pageSize={pageSize}
        onChangePage={onChangePage}
        onChangePageSize={onChangePageSize}
      />
    </div>
  );
};
