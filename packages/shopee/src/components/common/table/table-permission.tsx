import ArrowDownIcon from '@ep/shopee/src/images/arrow-down.svg';
import ArrowUpIcon from '@ep/shopee/src/images/arrow-up.svg';
import { makeStyles } from '@material-ui/core/styles';
import React, { useEffect } from 'react';
import {
  useBlockLayout,
  useRowSelect,
  useSortBy,
  useTable,
} from 'react-table';
import clsx from 'clsx';
import { useSticky } from 'react-table-sticky';
import { TableFilterUI } from '../table-filter';
import { Styles } from './style';

const useStyle = makeStyles({
  arrowUp: {
    color: '#3F4F5C',
    position: 'absolute',
    bottom: 10,
    right: 10,
  },
  arrowDown: {
    color: '#3F4F5C',
    position: 'absolute',
    bottom: 10,
    right: 10,
  },
});

type TableProps = {
  selectedIds: any[];
  columns: any[];
  getRowId: (
    row: any,
    relativeIndex: string,
    parentIndex?: string,
  ) => string;
  [key: string]: any;
};

export const PureTableUI = (props: TableProps) => {
  const classes = useStyle();
  const columns = React.useMemo(() => props.columns, []);
  const { noData, onSort, selectedIds, getRowId, className } = props;

  const tableInstance = useTable(
    {
      columns,
      data: props.rows,
      manualSortBy: true,
      initialState: {
        selectedRowIds: (selectedIds || []).reduce(
          (acc, i) => ({
            ...acc,
            [i]: true,
          }),
          {},
        ),
      },
      useControlledState: (state: any) => {
        return React.useMemo(() => {
          return {
            ...state,
            selectedRowIds: (selectedIds || []).reduce(
              (acc, i) => ({
                ...acc,
                [i]: true,
              }),
              {},
            ),
          };
        }, [state, selectedIds]);
      },
      getRowId: getRowId
        ? getRowId
        : React.useCallback((row, index) => index, []),
    },
    useSortBy,
    useBlockLayout,
    useSticky,
    useRowSelect,
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    state: { sortBy },
  } = tableInstance;

  useEffect(() => {
    onSort(sortBy);
  }, [sortBy, onSort]);

  const getDirectionIcon = (column: any) => {
    if (column.isSorted) {
      return column.isSortedDesc ? (
        <img
          src={ArrowDownIcon}
          className={classes.arrowDown}
          width={12}
          height={12}
        />
      ) : (
        <img
          src={ArrowUpIcon}
          className={classes.arrowUp}
          width={12}
          height={12}
        />
      );
    }
    return '';
  };

  return (
    <Styles>
      <div
        {...getTableProps()}
        className={clsx('table sticky', { [className]: className })}
      >
        <div className="header">
          {headerGroups.map((headerGroup: any) => {
            return (
              <div
                {...headerGroup.getHeaderGroupProps()}
                className="tr"
              >
                {headerGroup.headers.map((column: any) => {
                  return (
                    <div
                      {...column.getHeaderProps(
                        column.getSortByToggleProps(),
                      )}
                      className="th"
                    >
                      {column.render('Header')}
                      {getDirectionIcon(column)}
                    </div>
                  );
                })}
              </div>
            );
          })}
        </div>
        <div {...getTableBodyProps()} className="body">
          {rows.length > 0 &&
            rows.map((row: any) => {
              prepareRow(row);
              return (
                <div
                  {...row.getRowProps()}
                  className={clsx(
                    'tr',
                    row.values.permission.isHead &&
                      'headerPermission',
                  )}
                >
                  {row.cells.map((cell: any) => {
                    return (
                      <div
                        {...cell.getCellProps()}
                        className={clsx('td')}
                      >
                        {cell.render('Cell')}
                      </div>
                    );
                  })}
                </div>
              );
            })}
          {rows.length === 0 && (
            <div className="no-data">{noData}</div>
          )}
        </div>
      </div>
    </Styles>
  );
};

export const TableUIPermission = (props: any) => {
  const {
    columns,
    rows,
    resultTotal,
    page,
    pageSize,
    onChangePage,
    onChangePageSize,
    onSort,
    noData,
    onSelectItem,
    onSelectAll,
    selectedIds,
    getRowId,
    className,
  } = props;
  return (
    <div>
      <PureTableUI
        columns={columns}
        className={className}
        rows={rows}
        noData={noData}
        onSort={onSort}
        onSelectItem={onSelectItem}
        onSelectAll={onSelectAll}
        selectedIds={selectedIds}
        getRowId={getRowId}
      />
      {page && (
        <TableFilterUI
          resultTotal={resultTotal}
          page={page}
          pageSize={pageSize}
          onChangePage={onChangePage}
          onChangePageSize={onChangePageSize}
        />
      )}
    </div>
  );
};
