import React from 'react';

export function makeLazyRender(Cell) {
  function LazyRenderComponent(props) {
    let ref = React.useRef();
    let [rendered, setRendered] = React.useState(0);

    React.useEffect(() => {
      let observer = new IntersectionObserver((entries) => {
        entries.forEach((i) => {
          if (i.isIntersecting && i.intersectionRatio >= 0) {
            window.requestAnimationFrame(() => {
              if (i.isIntersecting && i.intersectionRatio >= 0) {
                setRendered(() => {
                  observer.unobserve(ref.current);
                  observer.disconnect();
                  return 1;
                });
              }
            });
          }
        });
      });

      if (ref.current) {
        observer.observe(ref.current);
      }

      return () => {
        if (ref.current) observer.unobserve(ref.current);
        observer.disconnect();
      };
    }, []);

    if (!rendered) return <div style={{minHeight: '20px'}} ref={ref}></div>;
    return <Cell {...props} />;
  }
  return LazyRenderComponent;
}
