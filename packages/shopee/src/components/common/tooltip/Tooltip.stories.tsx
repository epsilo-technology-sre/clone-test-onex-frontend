import React from 'react';
import { TooltipUI } from './Tooltip';
import styled from 'styled-components';

export default {
  title: 'Shopee/Toolip',
};

const WrapTooltip = styled.div`
  height: 1vh;
  margin-top: 200px;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`

export const Primary = () => <WrapTooltip>
  <TooltipUI title="asdasdasdas">
    <span>asdasd...</span>
  </TooltipUI>
</WrapTooltip>;
