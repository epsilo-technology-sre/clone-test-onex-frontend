import ThaiBahtIcon from '@ep/shopee/src/images/currency_thai_baht.svg';
import React from 'react';

type currencyType =
  | 'VND'
  | 'SGD'
  | 'PHP'
  | 'IDR'
  | 'MYR'
  | 'TWD'
  | 'USD'
  | 'THB';
export interface CurrencyCodeProps {
  currency: currencyType;
}

export const CurrencyCode = ({ currency }: CurrencyCodeProps) => {
  const CURRENCY = {
    VND: '₫',
    SGD: 'S$',
    PHP: '₱',
    IDR: 'Rp',
    MYR: 'RM',
    TWD: 'NT$',
    USD: '$',
    THB: <img src={ThaiBahtIcon} width={12} height={15} />,
  };

  return <>{CURRENCY[currency]}</>;
};
