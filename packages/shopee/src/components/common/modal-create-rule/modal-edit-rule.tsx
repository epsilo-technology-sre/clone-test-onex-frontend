import { COLORS, DATE_FORMAT } from '@ep/shopee/src/constants';
import {
  Box,
  Dialog,
  FormHelperText,
  Grid,
  IconButton,
  TextField,
  Typography,
} from '@material-ui/core';
import MuiDialogActions from '@material-ui/core/DialogActions';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import {
  createStyles,
  makeStyles,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import { isEmpty } from 'lodash';
import moment from 'moment';
import React, { useRef } from 'react';
import styled from 'styled-components';
import { ButtonLinkUI, ButtonUI } from '../button';
import { initColumns } from '../modal-add-rule/columns';
import { SelectSearch } from '../select-search';
import { SingleDatePickerUI } from '../single-date-picker';
import { TableUINoCheckbox } from '../table';
import {
  dataAction,
  dataOperator,
  dataPeriod,
  dataTypeOfPerformance,
  validationSchema,
} from './constants';

export interface ModalEditRuleProps {
  open: boolean;
  onClose?: any;
  onSubmit: any;
  ruleInfo: any;
  rules: any;
  onAddRule: any;
  actionList: {
    name: string;
    value: string;
  }[];
  actionMetricList: {
    [key: string]: {
      name: string;
      value: string;
    };
  };
  isSubmitting: boolean;
}

export function ModalEditRule(props: ModalEditRuleProps) {
  const {
    open,
    onClose,
    onSubmit,
    ruleInfo,
    rules,
    onAddRule,
    actionList = dataAction,
    actionMetricList = dataTypeOfPerformance,
    isSubmitting = false,
  } = props;
  const formRef = useRef();
  const classes = useStyles();

  const [errorMessage, setErrorMessage] = React.useState('');

  React.useEffect(() => {
    if (open) {
      setErrorMessage('');
    }
  }, [open]);

  const handleClose = () => {
    onClose(false);
  };

  let initialValues = {
    ruleName: '',
    action: 'pause_product',
    fromDate: moment().format(DATE_FORMAT),
    toDate: moment().format(DATE_FORMAT),
    rules: {
      typeOfPerformance: '',
      conditionPeriod: '',
      operator: '',
      value: '',
    },
    conditions: [],
  };

  if (!isEmpty(ruleInfo)) {
    initialValues = {
      ...initialValues,
      ruleName: ruleInfo.ruleName,
      action: ruleInfo.action,
      conditions: ruleInfo.conditions.map((i) => ({
        ...i,
        id: Math.random(),
      })),
      fromDate: ruleInfo.fromDate,
      toDate: ruleInfo.toDate,
    };
  }

  return (
    <div>
      <Dialog
        open={open}
        classes={{ paper: classes.paper }}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle
          id="customized-dialog-title"
          onClose={handleClose}
          className={classes.dialogTitle}
        >
          Modify rule
        </DialogTitle>
        <DialogContent>
          <Formik
            validationSchema={validationSchema}
            initialValues={initialValues}
            onSubmit={(values: any) => {
              if (values.conditions.length > 0) {
                return onSubmit({
                  ...values,
                  ruleId: ruleInfo.ruleId,
                  listRules: values.conditions,
                });
              } else {
                setErrorMessage('Please enter condition');
              }
            }}
            validateOnBlur={true}
            innerRef={formRef}
          >
            {(formik: any) => {
              const { errors } = formik;
              const handleRemoveRule = (props) => {
                const { rule } = props;
                const { conditions } = formik.values;
                const newRule = conditions.filter(
                  (condition) => condition.id !== rule.id,
                );
                formik.setFieldValue('conditions', newRule);
              };
              return (
                <Form
                // className={classes.wrapForm}
                >
                  <Typography
                    variant="body2"
                    className={classes.labelInput}
                  >
                    Rule name
                  </Typography>
                  <Field name="ruleName">
                    {({ field }: any) => {
                      return (
                        <TextField
                          className={classes.input}
                          fullWidth
                          id="outlined-size-small"
                          autoComplete="off"
                          placeholder="Enter rule name"
                          variant="outlined"
                          size="small"
                          {...field}
                        />
                      );
                    }}
                  </Field>
                  <ErrorMessage
                    component={FormHelperText}
                    name="ruleName"
                    className={classes.error}
                  />
                  <Field name="action">
                    {({ field, form }: any) => (
                      <SelectSearch
                        error={errors[field.name]}
                        title="Action"
                        placeholder="Choose Action"
                        value={(actionList || []).find(
                          (v) => v.value === field.value,
                        )}
                        disabled={true}
                        options={actionList}
                        onChange={(value: any) => {
                          form.setFieldValue(
                            field.name,
                            value.value || '',
                          );
                        }}
                      />
                    )}
                  </Field>
                  <Typography
                    variant="body2"
                    className={classes.labelInput}
                  >
                    From Date
                  </Typography>
                  <Field name="fromDate">
                    {({ field, form }: any) => (
                      <SingleDatePickerUI
                        openDirection="up"
                        date={field.value}
                        minDate={moment().format(DATE_FORMAT)}
                        onChange={(date: any) =>
                          form.setFieldValue(field.name, date)
                        }
                      />
                    )}
                  </Field>
                  <Typography
                    variant="body2"
                    className={classes.labelInput}
                  >
                    To Date
                  </Typography>
                  <Field name="toDate">
                    {({ field, form }: any) => (
                      <SingleDatePickerUI
                        openDirection="up"
                        date={field.value}
                        placeholder="No limit"
                        minDate={moment().format(DATE_FORMAT)}
                        onChange={(date: any) =>
                          form.setFieldValue(field.name, date)
                        }
                      />
                    )}
                  </Field>
                  <div className={classes.wrapConditions}>
                    <Field name="rules.typeOfPerformance">
                      {({ field, form }: any) => {
                        return (
                          <SelectSearch
                            disabled={form.values.action === ''}
                            error={errors[field.name]}
                            inputValue={(
                              actionMetricList[form.values.action] ||
                              []
                            ).find((v) => v === field.value)}
                            title="Type of performance"
                            placeholder="Choose Performance"
                            options={
                              actionMetricList[form.values.action] ||
                              []
                            }
                            onChange={(value: any) => {
                              form.setFieldValue(
                                field.name,
                                value.value,
                              );
                              const disabled =
                                [
                                  'percent_discount_product',
                                  'current_stock',
                                ].indexOf(
                                  value.value.toLowerCase(),
                                ) !== -1;
                              if (disabled) {
                                form.setFieldValue(
                                  'rules.conditionPeriod',
                                  '',
                                );
                              }
                            }}
                          />
                        );
                      }}
                    </Field>
                    <Field name="rules.conditionPeriod">
                      {({ field, form }: any) => {
                        const valuePerformance = form.values.rules.typeOfPerformance.toLowerCase();
                        const disabled =
                          [
                            'percent_discount_product',
                            'current_stock',
                          ].indexOf(valuePerformance) !== -1;
                        return (
                          <SelectSearch
                            disabled={disabled}
                            error={errors[field.name]}
                            inputValue={dataPeriod.find(
                              (i) => i.value === field.value,
                            )}
                            title="Condition period"
                            placeholder="Choose Period"
                            options={dataPeriod}
                            onChange={(value: any) =>
                              form.setFieldValue(
                                field.name,
                                value.value,
                              )
                            }
                          />
                        );
                      }}
                    </Field>
                    <Field name="rules.operator">
                      {({ field, form }: any) => (
                        <SelectSearch
                          error={errors[field.name]}
                          inputValue={field.value}
                          title="Operator"
                          placeholder="Choose Operator"
                          options={dataOperator}
                          onChange={(value: any) =>
                            form.setFieldValue(
                              field.name,
                              value.value,
                            )
                          }
                        />
                      )}
                    </Field>
                    <Typography
                      variant="body2"
                      className={classes.labelInput}
                    >
                      Value
                    </Typography>
                    <Box>
                      <Field name="rules.value">
                        {({ field }: any) => {
                          return (
                            <TextField
                              className={classes.input}
                              fullWidth
                              id="outlined-size-small"
                              autoComplete="off"
                              variant="outlined"
                              size="small"
                              {...field}
                            />
                          );
                        }}
                      </Field>
                      <ErrorMessage
                        component={FormHelperText}
                        name="rules.value"
                        className={classes.error}
                      />
                    </Box>
                    <Box style={{ marginTop: 16 }}>
                      <Field>
                        {({ field, form }: any) => {
                          const {
                            conditionPeriod,
                            operator,
                            typeOfPerformance,
                            value,
                          } = field.value.rules;
                          const valuePerformance = form.values.rules.typeOfPerformance.toLowerCase();
                          let disabled = [
                            conditionPeriod,
                            operator,
                            typeOfPerformance,
                            value,
                          ].includes('');
                          if (
                            [
                              'percent_discount_product',
                              'current_stock',
                            ].indexOf(valuePerformance) !== -1
                          ) {
                            disabled = false;
                          }

                          return (
                            <ButtonUI
                              disabled={disabled}
                              label="Add condition"
                              size="small"
                              colorButton="#F6F7F8"
                              onClick={() => {
                                const { rules } = field.value;
                                const newCondition = [
                                  ...form.values.conditions,
                                  {
                                    value: rules.value,
                                    period: rules.conditionPeriod,
                                    metric_code:
                                      rules.typeOfPerformance,
                                    operator_code: rules.operator,
                                    id: `condition${Math.floor(
                                      Math.random() * 10,
                                    )}`,
                                  },
                                ];
                                form.setFieldValue(
                                  'conditions',
                                  newCondition,
                                );
                                form.setFieldValue(
                                  'rules',
                                  form.initialValues.rules,
                                );
                                setErrorMessage('');
                              }}
                            />
                          );
                        }}
                      </Field>
                      <Field>
                        {({ form }: any) => {
                          return (
                            <ButtonUI
                              label="Reset"
                              size="small"
                              colorButton="#F6F7F8"
                              onClick={() =>
                                form.setFieldValue(
                                  'rules',
                                  form.initialValues.rules,
                                )
                              }
                            />
                          );
                        }}
                      </Field>
                    </Box>
                  </div>
                  <Field name={'conditions'}>
                    {({ field, form }: any) => {
                      const rows = field.value.map((r) => {
                        const metricCode = actionMetricList[
                          form.values.action
                        ].filter((a) => a.value === r.metric_code);
                        const period = dataPeriod.filter(
                          (p) => p.value === r.period,
                        );
                        return {
                          ...r,
                          metric_code: metricCode[0]?.name || '',
                          period: period[0]?.name || '',
                        };
                      });
                      return (
                        <TableUINoCheckbox
                          key={field.value.length}
                          className="add-rule"
                          columns={initColumns({
                            onCellClick: handleRemoveRule,
                          })}
                          rows={rows}
                          noData="No data"
                          onSort={() => {}}
                        />
                      );
                    }}
                  </Field>
                </Form>
              );
            }}
          </Formik>
          <Box className={classes.error}>{errorMessage}</Box>
        </DialogContent>
        <DialogActions>
          <Grid container justify="space-between">
            <Grid xs={6}></Grid>
            <Grid xs={6}>
              <Grid container justify="flex-end">
                <ButtonLinkUI
                  label="Cancel"
                  size="small"
                  colortext="#f16145"
                  onClick={handleClose}
                />
                <ButtonUI
                  label="Save rule"
                  size="small"
                  variant="contained"
                  loading={isSubmitting}
                  onClick={() => {
                    if (formRef.current) {
                      formRef.current.handleSubmit();
                    }
                  }}
                />
              </Grid>
            </Grid>
          </Grid>
        </DialogActions>
      </Dialog>
    </div>
  );
}

const Product = styled.span`
  background: #f6f7f8;
  border-radius: 4px;
  padding: 2px 6px;
  font-size: 14px;
  line-height: 20px;
  color: #253746;
  margin-right: 8px;
  margin-bottom: 8px;
  display: inline-block;
`;

const Wrap = styled.div`
  border: 2px solid #e4e7e9;
  border-radius: 4px;
  max-height: 120px;
  padding: 8px 8px 0 8px;
  width: 100%;
  overflow: auto;
`;

const Wrapper = styled.div`
  margin: 8px 0;
`;

const Delete = styled.span`
  margin-left: 9px;
  float: right;
  line-height: 14px;
`;

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });

const useStyles = makeStyles(() => ({
  paper: { width: 480, maxHeight: 528 },
  wrapForm: { maxHeight: 208, overflow: 'auto' },
  wrapConditions: {
    padding: 16,
    border: '2px solid #e4e7e9',
    borderRadius: 4,
    marginTop: 8,
  },
  error: {
    color: COLORS.COMMON.RED,
    fontSize: 12,
  },
  labelInput: {
    marginTop: 8,
    marginBottom: 4,
    fontWeight: 'bold',
    fontSize: 11,
    lineHeight: '16px',
  },
  input: {
    '& .MuiOutlinedInput-notchedOutline': {
      border: '2px solid #e4e7e9',
    },
  },
  dialogTitle: {
    padding: '16px 16px 0 16px',
  },
}));

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose?: () => void;
  classes: any;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle
      disableTypography
      className={classes.root}
      {...other}
    >
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);
