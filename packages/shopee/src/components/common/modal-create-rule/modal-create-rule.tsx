import { COLORS, DATE_FORMAT } from '@ep/shopee/src/constants';
import {
  Box,
  Dialog,
  FormHelperText,
  Grid,
  IconButton,
  InputBase,
  MenuItem,
  Select,
  TextField,
  Typography,
} from '@material-ui/core';
import MuiDialogActions from '@material-ui/core/DialogActions';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import {
  createStyles,
  makeStyles,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import moment from 'moment';
import React, { useRef } from 'react';
import styled from 'styled-components';
import { ActionIcon } from '../action-icon';
import { ButtonLinkUI, ButtonUI } from '../button';
import { InputUI } from '../modal-add-rule-keywords/modal-add-rule-keywords';
import { initColumns } from '../modal-add-rule/columns';
import { SingleDatePickerUI } from '../single-date-picker';
import { TableUINoCheckbox } from '../table';
import { TooltipUI } from '../tooltip';
import {
  dataAction,
  dataOperator,
  dataPeriod,
  dataTypeOfPerformance,
  validationSchema,
} from './constants';

export interface ModalAddRuleProps {
  suffix?: string;
  open: boolean;
  onClose?: any;
  products: any;
  onSubmit: any;
  rules: any;
  onAddRule: any;
  setProduct: any;
  actionList: {
    name: string;
    value: string;
  }[];
  actionMetricList: {
    [key: string]: {
      name: string;
      value: string;
    };
  };
  isSubmitting: boolean;
  onOpenExistingRule: any;
}

export function ModalCreateRule(props: ModalAddRuleProps) {
  const {
    suffix = 'products',
    open,
    onClose,
    products,
    onSubmit,
    setProduct,
    actionList = dataAction,
    actionMetricList = dataTypeOfPerformance,
    isSubmitting = false,
    onOpenExistingRule,
  } = props;
  const formRef = useRef();
  const classes = useStyles();

  const [errorMessage, setErrorMessage] = React.useState('');

  React.useEffect(() => {
    if (open) {
      setErrorMessage('');
    }
  }, [open]);

  const handleClose = () => {
    onClose(false);
  };

  return (
    <div>
      <Dialog
        open={open}
        classes={{ paper: classes.paper }}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle
          id="customized-dialog-title"
          onClose={handleClose}
          className={classes.dialogTitle}
        >
          Create rule for
        </DialogTitle>
        <DialogContent>
          <span>
            Create rule for{' '}
            <strong>
              {products.length} selected {suffix}
            </strong>
          </span>
          <Wrapper>
            <Wrap>
              {products.map((item: any, index: number) => {
                const keyword = item.product_name;
                const title =
                  keyword.length > 21
                    ? `${keyword.slice(0, 21)}...`
                    : keyword;
                return (
                  <TooltipUI title={keyword} key={index}>
                    <Product>
                      {title}
                      {products.length > 1 && (
                        <Delete
                          onClick={() => {
                            const newKeyword = products.filter(
                              (item1: any) =>
                                item1.product_id !== item.product_id,
                            );
                            setProduct(newKeyword);
                          }}
                        >
                          <ActionIcon status="remove" />
                        </Delete>
                      )}
                    </Product>
                  </TooltipUI>
                );
              })}
            </Wrap>
          </Wrapper>
          <Formik
            validationSchema={validationSchema}
            initialValues={{
              existingRule: '',
              ruleName: '',
              action: '',
              fromDate: moment().format(DATE_FORMAT),
              toDate: moment().format(DATE_FORMAT),
              rules: {
                typeOfPerformance: '',
                conditionPeriod: '',
                operator: '',
                value: '',
              },
              conditions: [],
            }}
            onSubmit={(values: any) => {
              if (values.conditions.length > 0) {
                return onSubmit({
                  ...values,
                  listRules: values.conditions,
                });
              } else {
                setErrorMessage('Please enter condition');
              }
            }}
            validateOnBlur={true}
            innerRef={formRef}
          >
            {(formik: any) => {
              const { errors } = formik;
              const handleRemoveRule = (props) => {
                const { rule } = props;
                const { conditions } = formik.values;
                const newRule = conditions.filter(
                  (condition) => condition.id !== rule.id,
                );
                formik.setFieldValue('conditions', newRule);
              };
              return (
                <Form
                // className={classes.wrapForm}
                >
                  <Typography
                    variant="body2"
                    className={classes.labelInput}
                  >
                    Rule name
                  </Typography>
                  <Field name="ruleName">
                    {({ field }: any) => {
                      return (
                        <TextField
                          className={classes.input}
                          fullWidth
                          id="outlined-size-small"
                          autoComplete="off"
                          placeholder="Enter rule name"
                          variant="outlined"
                          size="small"
                          {...field}
                        />
                      );
                    }}
                  </Field>
                  <ErrorMessage
                    component={FormHelperText}
                    name="ruleName"
                    className={classes.error}
                  />
                  <Typography
                    variant="body2"
                    className={classes.labelInput}
                  >
                    Action
                  </Typography>
                  <Field name="action">
                    {({ field }: any) => (
                      <Select
                        id="operator"
                        {...field}
                        input={
                          <BootstrapInput
                            placeholder={'Choose Action'}
                          />
                        }
                        displayEmpty
                        MenuProps={{
                          anchorOrigin: {
                            vertical: 'bottom',
                            horizontal: 'left',
                          },
                          transformOrigin: {
                            vertical: 'top',
                            horizontal: 'left',
                          },
                          getContentAnchorEl: null,
                        }}
                      >
                        <MenuItem
                          style={{ display: 'none' }}
                          value={''}
                          disabled
                        >
                          Choose Action
                        </MenuItem>
                        {actionList.map((item: any) => (
                          <MenuItem
                            value={item.value}
                            key={item.value}
                          >
                            {item.name}
                          </MenuItem>
                        ))}
                      </Select>
                    )}
                  </Field>
                  <ErrorMessage
                    component={FormHelperText}
                    name="action"
                    className={classes.error}
                  />
                  <Typography
                    variant="body2"
                    className={classes.labelInput}
                  >
                    From Date
                  </Typography>
                  <Field name="fromDate">
                    {({ field, form }: any) => (
                      <SingleDatePickerUI
                        openDirection="up"
                        date={field.value}
                        minDate={moment().format(DATE_FORMAT)}
                        onChange={(date: any) =>
                          form.setFieldValue(field.name, date)
                        }
                      />
                    )}
                  </Field>
                  <Typography
                    variant="body2"
                    className={classes.labelInput}
                  >
                    To Date
                  </Typography>
                  <Field name="toDate">
                    {({ field, form }: any) => (
                      <SingleDatePickerUI
                        openDirection="up"
                        date={field.value}
                        placeholder="No limit"
                        minDate={moment().format(DATE_FORMAT)}
                        onChange={(date: any) =>
                          form.setFieldValue(field.name, date)
                        }
                      />
                    )}
                  </Field>
                  <div className={classes.wrapConditions}>
                    <Typography
                      variant="body2"
                      className={classes.labelInput}
                    >
                      Type of performance
                    </Typography>
                    <Field name="rules.typeOfPerformance">
                      {({ field, form }: any) => {
                        return (
                          <Select
                            disabled={form.values.action === ''}
                            id="operator"
                            {...field}
                            input={
                              <BootstrapInput
                                placeholder={'Choose Performance'}
                              />
                            }
                            displayEmpty
                            MenuProps={{
                              anchorOrigin: {
                                vertical: 'bottom',
                                horizontal: 'left',
                              },
                              transformOrigin: {
                                vertical: 'top',
                                horizontal: 'left',
                              },
                              getContentAnchorEl: null,
                            }}
                          >
                            <MenuItem
                              style={{ display: 'none' }}
                              value={''}
                              disabled
                            >
                              Choose Action
                            </MenuItem>
                            {(
                              actionMetricList[form.values.action] ||
                              []
                            ).map((item: any) => (
                              <MenuItem
                                value={item.value}
                                key={item.value}
                              >
                                {item.name}
                              </MenuItem>
                            ))}
                          </Select>
                        );
                      }}
                    </Field>
                    <Typography
                      variant="body2"
                      className={classes.labelInput}
                    >
                      Condition period
                    </Typography>
                    <Field name="rules.conditionPeriod">
                      {({ field, form }: any) => {
                        const valuePerformance = form.values.rules.typeOfPerformance.toLowerCase();
                        const disabled =
                          [
                            'percent_discount_product',
                            'current_stock',
                          ].indexOf(valuePerformance) !== -1;
                        return (
                          <Select
                            disabled={
                              disabled || form.values.action === ''
                            }
                            id="operator"
                            {...field}
                            input={
                              <BootstrapInput
                                placeholder={'Choose Period'}
                              />
                            }
                            displayEmpty
                            MenuProps={{
                              anchorOrigin: {
                                vertical: 'bottom',
                                horizontal: 'left',
                              },
                              transformOrigin: {
                                vertical: 'top',
                                horizontal: 'left',
                              },
                              getContentAnchorEl: null,
                            }}
                          >
                            <MenuItem
                              style={{ display: 'none' }}
                              value={''}
                              disabled
                            >
                              Choose Action
                            </MenuItem>
                            {dataPeriod.map((item: any) => (
                              <MenuItem
                                value={item.value}
                                key={item.value}
                              >
                                {item.name}
                              </MenuItem>
                            ))}
                          </Select>
                        );
                      }}
                    </Field>
                    <Typography
                      variant="body2"
                      className={classes.labelInput}
                    >
                      Operator
                    </Typography>
                    <Field name="rules.operator">
                      {({ field, form }: any) => (
                        <Select
                          disabled={form.values.action === ''}
                          id="operator"
                          {...field}
                          input={
                            <BootstrapInput
                              placeholder={'Choose Operator'}
                            />
                          }
                          displayEmpty
                          MenuProps={{
                            anchorOrigin: {
                              vertical: 'bottom',
                              horizontal: 'left',
                            },
                            transformOrigin: {
                              vertical: 'top',
                              horizontal: 'left',
                            },
                            getContentAnchorEl: null,
                          }}
                        >
                          <MenuItem
                            style={{ display: 'none' }}
                            value={''}
                            disabled
                          >
                            Choose Action
                          </MenuItem>
                          {dataOperator.map((item: any) => (
                            <MenuItem
                              value={item.value}
                              key={item.value}
                            >
                              {item.name}
                            </MenuItem>
                          ))}
                        </Select>
                      )}
                    </Field>
                    <ErrorMessage
                      component={FormHelperText}
                      name="rules.operator"
                      className={classes.error}
                    />
                    <Typography
                      variant="body2"
                      className={classes.labelInput}
                    >
                      Value
                    </Typography>
                    <Box>
                      <Field name="rules.value">
                        {({ field, form }: any) => {
                          return (
                            <InputUI
                              disabled={form.values.action === ''}
                              id="daily-budget"
                              type="number"
                              fullWidth
                              {...field}
                              autoComplete="off"
                              className={classes.input}
                            />
                          );
                        }}
                      </Field>
                      <ErrorMessage
                        component={FormHelperText}
                        name="rules.value"
                        className={classes.error}
                      />
                    </Box>
                    <Box style={{ marginTop: 16 }}>
                      <Field>
                        {({ field, form }: any) => {
                          const {
                            conditionPeriod,
                            operator,
                            typeOfPerformance,
                            value,
                          } = field.value.rules;
                          const valuePerformance = form.values.rules.typeOfPerformance.toLowerCase();
                          let disabled = [
                            conditionPeriod,
                            operator,
                            typeOfPerformance,
                            value,
                          ].includes('');
                          if (
                            [
                              'percent_discount_product',
                              'current_stock',
                            ].indexOf(valuePerformance) !== -1
                          ) {
                            disabled = false;
                          }

                          return (
                            <ButtonUI
                              disabled={disabled}
                              label="Add condition"
                              size="small"
                              colorButton="#F6F7F8"
                              onClick={() => {
                                const { rules } = field.value;
                                const newCondition = [
                                  ...form.values.conditions,
                                  {
                                    value: rules.value,
                                    period: rules.conditionPeriod,
                                    metric_code:
                                      rules.typeOfPerformance,
                                    operator_code: rules.operator,
                                    id: Math.random(),
                                  },
                                ];
                                form.setFieldValue(
                                  'conditions',
                                  newCondition,
                                );
                                form.setFieldValue(
                                  'rules',
                                  form.initialValues.rules,
                                );
                                setErrorMessage('');
                              }}
                            />
                          );
                        }}
                      </Field>
                      <Field>
                        {({ form }: any) => {
                          return (
                            <ButtonUI
                              label="Reset"
                              size="small"
                              colorButton="#F6F7F8"
                              onClick={() =>
                                form.setFieldValue(
                                  'rules',
                                  form.initialValues.rules,
                                )
                              }
                            />
                          );
                        }}
                      </Field>
                    </Box>
                  </div>
                  <Field name={'conditions'}>
                    {({ field, form }: any) => {
                      const rows = field.value.map((r) => {
                        const metricCode = actionMetricList[
                          form.values.action
                        ].filter((a) => a.value === r.metric_code);
                        const period = dataPeriod.filter(
                          (p) => p.value === r.period,
                        );
                        return {
                          ...r,
                          metric_code: metricCode[0]?.name || '',
                          period: period[0]?.name || '',
                        };
                      });
                      return (
                        <TableUINoCheckbox
                          key={field.value.length}
                          className="add-rule"
                          columns={initColumns({
                            onCellClick: handleRemoveRule,
                          })}
                          rows={rows}
                          noData="No data"
                          onSort={() => {}}
                        />
                      );
                    }}
                  </Field>
                </Form>
              );
            }}
          </Formik>
          <Box className={classes.error}>{errorMessage}</Box>
        </DialogContent>
        <DialogActions>
          <Grid container justify="space-between">
            <Grid xs={6}>
              <ButtonUI
                label="Existing rule"
                size="small"
                colorButton="#F6F7F8"
                onClick={onOpenExistingRule}
              />
            </Grid>
            <Grid xs={6}>
              <Grid container justify="flex-end">
                <ButtonLinkUI
                  label="Cancel"
                  size="small"
                  colortext="#f16145"
                  onClick={handleClose}
                />
                <ButtonUI
                  label="Save rule"
                  size="small"
                  variant="contained"
                  loading={isSubmitting}
                  onClick={() => {
                    if (formRef.current) {
                      formRef.current.handleSubmit();
                    }
                  }}
                />
              </Grid>
            </Grid>
          </Grid>
        </DialogActions>
      </Dialog>
    </div>
  );
}

const Product = styled.span`
  background: #f6f7f8;
  border-radius: 4px;
  padding: 2px 6px;
  font-size: 14px;
  line-height: 20px;
  color: #253746;
  margin-right: 8px;
  margin-bottom: 8px;
  display: inline-block;
`;

const Wrap = styled.div`
  border: 2px solid #e4e7e9;
  border-radius: 4px;
  max-height: 120px;
  padding: 8px 8px 0 8px;
  width: 100%;
  overflow: auto;
`;

const Wrapper = styled.div`
  margin: 8px 0;
`;

const Delete = styled.span`
  margin-left: 9px;
  float: right;
  line-height: 14px;
`;

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });

const useStyles = makeStyles(() => ({
  paper: { width: 480, maxHeight: 528 },
  wrapForm: { maxHeight: 208, overflow: 'auto' },
  wrapConditions: {
    padding: 16,
    border: '2px solid #e4e7e9',
    borderRadius: 4,
    marginTop: 8,
  },
  error: {
    color: COLORS.COMMON.RED,
    fontSize: 12,
  },
  labelInput: {
    marginTop: 8,
    marginBottom: 4,
    fontWeight: 'bold',
    fontSize: 11,
    lineHeight: '16px',
  },
  input: {
    '& .MuiOutlinedInput-notchedOutline': {
      border: '2px solid #e4e7e9',
    },
  },
  dialogTitle: {
    padding: '16px 16px 0 16px',
  },
}));

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose?: () => void;
  classes: any;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle
      disableTypography
      className={classes.root}
      {...other}
    >
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

const BootstrapInput = withStyles({
  root: {
    width: '100%',
  },
  disabled: {
    color: '#9FA7AE !important',
    background: '#F6F7F8 !important',
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: '#ffffff',
    border: '2px solid #C2C7CB',
    fontSize: 14,
    color: '#253746',
    padding: '8px 26px 9px 8px',
  },
})(InputBase);
