import * as Yup from 'yup';

export const validationSchema = Yup.object().shape({
  ruleName: Yup.string().required('Please input Rule name'),
  action: Yup.string().required('Please choose action'),
  fromDate: Yup.string().required('From date is empty'),
  toDate: Yup.string().required('To date is empty'),
  rules: Yup.object().shape({
    typeOfPerformance: Yup.string(),
    conditionPeriod: Yup.string().when('typeOfPerformance', {
      is: (val) => {
        const valuePerformance = val || '';
        const disabled =
          ['percent_discount_product', 'current_stock'].indexOf(
            valuePerformance.toLowerCase(),
          ) !== -1;
        return !disabled;
      },
      then: Yup.string().optional(),
    }),
    operator: Yup.string(),
    value: Yup.string(),
  }),
});

// .when('isNoLimit', {
//   is: true,
//   then: Yup.number().optional(),
// })
export const dataSearch: any = [
  {
    name: 'Portugal',
    value: 'Portugal',
  },
  {
    name: 'Palauan',
    value: 'Palauan',
  },
];

export const dataAction: any = [
  { name: 'Pause Product', value: 'paused' },
  { name: 'Resume Product', value: 'resume' },
];

export const dataTypeOfPerformance: any = {
  paused: [
    { name: 'Ads Item Sold', value: 'Ads Item Sold' },
    { name: 'Ads GMV', value: 'Ads GMV' },
    { name: 'Cost', value: 'Cost' },
    { name: 'CPI', value: 'CPI' },
    { name: 'CTR', value: 'CTR' },
    { name: 'CR', value: 'CR' },
    { name: 'CIR', value: 'CIR' },
    { name: 'CPC', value: 'CPC' },
    { name: 'ROAS', value: 'ROAS' },
    { name: '% Discount of Product', value: '% Discount of Product' },
    { name: 'Total GMV of Product', value: 'Total GMV of Product' },
    {
      name: 'Total Item Sold of Product',
      value: 'Total Item Sold of Product',
    },
    { name: '%GMV Contribution', value: '%GMV Contribution' },
    { name: 'Current Stock', value: 'Current Stock' },
  ],
  resume: [
    { name: 'Current Stock', value: 'Current Stock' },
    { name: 'Total GMV of Product', value: 'Total GMV of Product' },
    {
      name: 'Total Item Sold of Product',
      value: 'Total Item Sold of Product',
    },
    { name: '% Discount of Product', value: '% Discount of Product' },
  ],
};

export const dataOperator: any = [
  { name: '=', value: '=' },
  { name: '>', value: '>' },
  { name: '<', value: '<' },
  { name: '>=', value: '>=' },
  { name: '<=', value: '<=' },
];

export const dataPeriod: any = [
  { name: 'Current day', value: 1 },
  { name: 'Last 2 Days + current day', value: 3 },
  { name: 'Last 6 Days + current day', value: 7 },
  { name: 'Last 13 Days + current day', value: 14 },
  { name: 'Last 29 Days + current day', value: 30 },
];
