import {
  isDiffMonthOrYear,
  isSameDay,
} from '@ep/shopee/src/utils/dateTimeUtility';
import { FormControl } from '@material-ui/core';
import InputBase from '@material-ui/core/InputBase';
import makeStyles from '@material-ui/core/styles/makeStyles';
import withStyles from '@material-ui/core/styles/withStyles';
import { isEmpty } from 'lodash';
import moment from 'moment';
import { nanoid } from 'nanoid';
import React, { useEffect, useState } from 'react';
import { DayPickerRangeController } from 'react-dates';
import Select from 'react-select';
import styled from 'styled-components';
import { ButtonTextUI, ButtonUI } from '../button';

const useStyles = makeStyles((theme) => ({
  presetRange: {
    width: '100%',
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    marginTop: theme.spacing(2),
  },
  inputDateRanges: {
    display: 'flex',
    justifyContent: 'space-between',
    margin: '16px 16px 0 16px',
    '& > *': {
      width: 280,
    },
  },
  footer: {
    display: 'flex',
    justifyContent: 'flex-end',
    margin: theme.spacing(2),
    marginTop: 0,
    '& > *': {
      margin: theme.spacing(0.5),
    },
  },
}));

const BootstrapInput = withStyles((theme) => ({
  root: {
    'label + &': {
      marginTop: theme.spacing(0.5),
    },
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.common.white,
    border: '1px solid #ced4da',
    fontSize: 16,
    width: '100%',
    padding: '10px 12px',
    transition: theme.transitions.create([
      'border-color',
      'box-shadow',
    ]),
    fontFamily: [
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
    ].join(','),
    '&:focus': {
      borderColor: 'rgb(241, 97, 69)',
    },
  },
}))(InputBase);

const WrapperContainer = styled.div`
  width: 610px;
  border: 1px solid lightgrey;
  border-radius: 4px;
  background: white;

  .DayPicker__horizontal {
    width: auto !important;
    div > div {
      width: auto !important;
    }
  }

  .DayPickerNavigation_button__default {
    border: 0;
    outline-color: rgb(241, 97, 69);
    box-shadow: none;
  }

  .DayPickerNavigation_button__default:active {
    background: rgba(241, 97, 69, 0.08);
  }

  .DayPickerNavigation_leftButton__horizontalDefault {
    left: 16px;
  }

  .DayPickerNavigation_rightButton__horizontalDefault {
    right: 16px;
  }

  .DayPicker_weekHeader {
    padding: 0 8px !important;
  }

  .CalendarMonth {
    padding: 0 8px !important;
  }

  .CalendarDay__default {
    width: 40px !important;
  }

  .CalendarDay__today {
    color: rgb(241, 97, 69);
  }

  .CalendarDay__selected {
    background: rgb(241, 97, 69);
    border: 1px solid #e4e7e7;
    color: white;
  }

  .CalendarDay__selected:active,
  .CalendarDay__selected:hover {
    background: rgb(241, 97, 69);
    border: 1px solid #e4e7e7;
  }

  .CalendarDay__selected_span {
    background: rgba(241, 97, 69, 0.08);
    color: rgba(0, 0, 0, 0.8);
    border: 1px solid #e4e7e7;
  }

  .CalendarDay__selected_span:active,
  .CalendarDay__selected_span:hover {
    background: rgba(241, 97, 69, 0.08);
    color: rgba(0, 0, 0, 0.8);
    border: 1px solid #e4e7e7;
  }

  .CalendarDay__hovered_span,
  .CalendarDay__hovered_span:active,
  .CalendarDay__hovered_span:hover {
    background: #e4e7e7;
    color: white;
    border: 1px solid #e4e7e7;
  }
`;

const InputDateRange = (props: any) => {
  const {
    startDefaultValue,
    endDefaultValue,
    startDateOnBlur,
    endDateOnBlur,
    dateFormat = 'DD/MM/YYYY',
  } = props;

  const [inputStartDate, setInputStartDate] = useState<any>();
  const [inputEndDate, setInputEndDate] = useState<any>();
  useEffect(() => {
    const formattedDate = startDefaultValue
      ? moment(startDefaultValue).format(dateFormat)
      : '';

    setInputStartDate(formattedDate);
  }, [startDefaultValue]);

  useEffect(() => {
    const formattedDate = endDefaultValue
      ? moment(endDefaultValue).format(dateFormat)
      : '';
    setInputEndDate(formattedDate);
  }, [endDefaultValue]);

  const inputStartDateChange = (event: any) => {
    const value = event.target.value;
    setInputStartDate(value);
  };

  const inputEndDateChange = (event: any) => {
    const value = event.target.value;
    setInputEndDate(value);
  };

  const handleStartDateOnBlur = (event: any) => {
    const value = event.target.value;
    const date = moment(value, props.dateFormat, true);
    if (date.isValid()) {
      startDateOnBlur(date);
    } else {
      setInputStartDate(moment(startDefaultValue).format(dateFormat));
    }
  };

  const handleEndDateOnBlur = (event: any) => {
    const value = event.target.value;
    const date = moment(value, props.dateFormat, true);
    if (date.isValid()) {
      endDateOnBlur(date);
    } else {
      setInputEndDate(moment(endDefaultValue).format(dateFormat));
    }
  };

  const classes = useStyles();
  return (
    <div className={classes.inputDateRanges}>
      <FormControl>
        <label htmlFor="bootstrap-input-start">Starting Date</label>
        <BootstrapInput
          defaultValue={startDefaultValue}
          value={inputStartDate}
          readOnly={true}
          id="bootstrap-input-start"
          onChange={inputStartDateChange}
          onBlur={handleStartDateOnBlur}
        />
      </FormControl>
      <FormControl>
        <label htmlFor="bootstrap-input-end">Ending Date</label>
        <BootstrapInput
          id="bootstrap-input-end"
          defaultValue={endDefaultValue}
          readOnly={true}
          value={inputEndDate}
          onChange={inputEndDateChange}
          onBlur={handleEndDateOnBlur}
        />
      </FormControl>
    </div>
  );
};

export const DateRangePickerWrapper: React.FC<any> = (props) => {
  const classes = useStyles();
  const [key, setKey] = useState<any>();
  const [initVisibleMonth, setInitVisibleMonth] = useState<any>();
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [selectedStartDate, setSelectedStartDate] = useState<any>(
    null,
  );

  const [selectedEndDate, setSelectedEndDate] = useState<any>(null);
  const [preSetValue, setPreSetValue] = useState(null);

  useEffect(() => {
    if (props.startDate && props.endDate) {
      setSelectedStartDate(props.startDate);
      setSelectedEndDate(props.endDate);
    } else if (props.defaultRange) {
      setSelectedStartDate(props.defaultRange.value.start);
      setSelectedEndDate(props.defaultRange.value.end);
    }
  }, []);

  useEffect(() => {
    if (isDiffMonthOrYear(initVisibleMonth, selectedStartDate)) {
      setKey(nanoid());
    }
  }, [selectedStartDate, initVisibleMonth]);

  let initFocusInput = 'startDate';
  if (props.autoFocus) {
    initFocusInput = 'startDate';
  } else if (props.autoFocusEndDate) {
    initFocusInput = 'endDate';
  }
  const [focusedInput, setFocusedInput] = useState(initFocusInput);

  const handleCancelClick = () => {
    setSelectedStartDate(startDate);
    setSelectedEndDate(endDate);
    props.onCancel && props.onCancel();
  };

  const handleApplyClick = () => {
    setStartDate(selectedStartDate);
    setEndDate(selectedEndDate);
    props.onApply({
      startDate: selectedStartDate,
      endDate: selectedEndDate,
    });
  };

  const handlePreSetRangeChange = (selectedItem: any) => {
    onDatesChange({
      label: selectedItem.label,
      startDate: selectedItem.value.start,
      endDate: selectedItem.value.end,
    });
  };

  const onDatesChange = ({ label, startDate, endDate }: any) => {
    const { onDateRangeChange } = props;
    setSelectedStartDate(startDate);
    setSelectedEndDate(endDate);
    const selectedRanged = label
      ? props.rangeOptions.find((item: any) => item.label === label)
      : props.rangeOptions.find(({ value }: any) => {
          return (
            isSameDay(value.start, startDate) &&
            isSameDay(value.end, endDate)
          );
        });
    if (selectedRanged) {
      setPreSetValue(selectedRanged);
      onDateRangeChange && onDateRangeChange(selectedRanged);
    } else {
      const custom: any = { label: 'Custom' };
      setPreSetValue(custom);
      onDateRangeChange && onDateRangeChange(custom);
    }
  };

  const inputStartDateBlur = (date: any) => {
    onDatesChange({
      startDate: date,
      endDate: selectedEndDate,
    });

    if (date.isAfter(selectedEndDate, 'day')) {
      setSelectedEndDate(date);
    }
  };

  const inputEndDateBlur = (date: any) => {
    onDatesChange({
      startDate: selectedStartDate,
      endDate: date,
    });

    if (date.isBefore(selectedStartDate, 'day')) {
      setSelectedStartDate(date);
    }
  };

  const onFocusChange = (focusedInput: any) => {
    setFocusedInput(!focusedInput ? 'startDate' : focusedInput);
  };

  const onInitialVisibleMonth = () => {
    const visibleMonthOfDay =
      !isEmpty(selectedStartDate) && selectedStartDate.isValid()
        ? selectedStartDate
        : moment();

    setInitVisibleMonth(visibleMonthOfDay);
    return visibleMonthOfDay;
  };

  return (
    <WrapperContainer className="DateRangePickerWrapper">
      {!isEmpty(props.rangeOptions) && (
        <FormControl className={classes.presetRange}>
          <label htmlFor="preSetRange">Date Range</label>
          <Select
            id="preSetRange"
            defaultValue={props.defaultRange}
            options={props.rangeOptions}
            onChange={handlePreSetRangeChange}
            value={preSetValue || props.defaultRange}
            components={{
              IndicatorSeparator: () => null,
            }}
            theme={(theme: any) => ({
              ...theme,
              borderRadius: 0,
              colors: {
                ...theme.colors,
                // customized by design
                primary25: 'rgba(241, 97, 69, 0.08)',
                primary50: 'rgba(241, 97, 69, 0.5)',
                primary75: 'rgba(241, 97, 69, 0.75)',
                primary: 'rgb(241, 97, 69)',
              },
            })}
            styles={{
              control: (styles) => ({
                ...styles,
                marginTop: 4,
                borderRadius: 4,
              }),
              menu: (provided: any, state: any) => ({
                ...provided,
                zIndex: 999,
              }),
            }}
          />
        </FormControl>
      )}

      <InputDateRange
        startDefaultValue={selectedStartDate}
        endDefaultValue={selectedEndDate}
        startDateOnBlur={inputStartDateBlur}
        endDateOnBlur={inputEndDateBlur}
        dateFormat={props.dateFormat}
      />
      <DayPickerRangeController
        key={key}
        {...props}
        onDatesChange={onDatesChange}
        onFocusChange={onFocusChange}
        focusedInput={focusedInput}
        startDate={selectedStartDate}
        endDate={selectedEndDate}
        initialVisibleMonth={onInitialVisibleMonth}
        noBorder
        hideKeyboardShortcutsPanel
        isOutsideRange={(day) => {
          if (typeof props.isOutsideRange === 'function') {
            return props.isOutsideRange(day);
          } else {
            const isOutsideRange: any = {
              pass: moment().subtract(1, 'days') > day,
              future: moment().subtract(-1, 'days') < day,
            };
            return isOutsideRange[props.isOutsideRange] || false;
          }
        }}
      />
      <div className={classes.footer}>
        <ButtonTextUI
          label="Cancel"
          size="small"
          onClick={handleCancelClick}
        />
        <ButtonUI
          label="Apply"
          size="small"
          variant="contained"
          disabled={!(selectedStartDate && selectedEndDate)}
          onClick={handleApplyClick}
        />
      </div>
    </WrapperContainer>
  );
};

DateRangePickerWrapper.defaultProps = {
  autoFocus: false,
  autoFocusEndDate: false,
  initialStartDate: null,
  initialEndDate: null,

  // input related props
  startDateId: 'startDate',
  startDatePlaceholderText: 'Start Date',
  endDateId: 'endDate',
  endDatePlaceholderText: 'End Date',
  disabled: false,
  required: false,
  screenReaderInputMessage: '',
  showClearDates: false,
  showDefaultInputIcon: false,
  customInputIcon: null,
  customArrowIcon: null,
  customCloseIcon: null,
  block: false,
  small: false,
  regular: false,

  // calendar presentation and interaction related props
  renderMonthText: null,
  orientation: 'horizontal',
  anchorDirection: 'left',
  horizontalMargin: 0,
  withPortal: false,
  withFullScreenPortal: false,
  initialVisibleMonth: null,
  numberOfMonths: 2,
  keepOpenOnDateSelect: false,
  reopenPickerOnClearDates: false,
  isRTL: false,

  // navigation related props
  navPrev: null,
  navNext: null,
  onPrevMonthClick() {},
  onNextMonthClick() {},
  onClose() {},

  // day presentation and interaction related props
  renderDayContents: null,
  minimumNights: 0,
  enableOutsideDays: false,
  isDayBlocked: () => false,
  // isOutsideRange: (day: any) => false,
  isDayHighlighted: () => false,

  // internationalization
  displayFormat: () => moment.localeData().longDateFormat('L'),
  monthFormat: 'MMMM YYYY',
  dateFormat: 'DD/MM/YYYY',
  // phrases: DateRangePickerPhrases,
};
