import React, { useEffect } from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Box from '@material-ui/core/Box';
import InputAdornment from '@material-ui/core/InputAdornment';
import { CurrencyCode } from '../currency-code';
import { COLORS } from '@ep/shopee/src/constants';
import ErrorIcon from '@ep/shopee/src/images/error-icon.svg';

const useStyles = makeStyles({
  errorMessage: {
    marginTop: 5,
    fontSize: 12,
    color: COLORS.COMMON.RED,
    '& img': {
      marginRight: 5,
    },
  },
});

const CurrencyBox = withStyles({
  root: {
    width: '100%',
    '&:hover .MuiOutlinedInput-notchedOutline': {
      borderColor: '#C2C7CB',
    },
  },
  notchedOutline: {
    border: '2px solid #D3D7DA',
  },
  disabled: {
    color: '#9FA7AE !important',
    background: '#F6F7F8 !important',
  },
  input: {
    padding: 8,
    fontSize: 14,
  },
})(OutlinedInput);

export const InputUI = withStyles({
  root: {
    '& fieldset': {
      borderColor: '#C2C7CB',
      borderWidth: 2,
    },
    '&.Mui-focused fieldset': {
      borderColor: '#485764!important',
    },
  },
  input: {
    paddingTop: 14,
    paddingBottom: 14,
  },
})(OutlinedInput);

export const CurrencyTextbox = (props: any) => {
  const classes = useStyles();
  const {
    currency,
    min,
    max,
    step,
    value,
    disabled = false,
    onChange,
    error
  } = props;

  const handleChange = (e: any) => {
    onChange(e.target.value);
  };

  return (
    <div>
      <CurrencyBox
        value={value}
        type="number"
        inputProps={{
          step,
        }}
        disabled={disabled}
        onChange={handleChange}
        startAdornment={
          <InputAdornment position="start">
            <CurrencyCode currency={currency} />
          </InputAdornment>
        }
        autoComplete="off"
      />
      {error && (
        <Box className={classes.errorMessage}>
          <img src={ErrorIcon} height={9} width={9} />
          {error}
        </Box>
      )}
    </div>
  );
};
