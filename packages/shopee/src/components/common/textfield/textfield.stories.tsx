
import React from 'react'
import { Typography, Box } from '@material-ui/core';
import { CurrencyTextbox } from './textfield'

export default {
  title: 'shopee / TextField'
}

export const CurrencyTextBoxDemo = () => {
  const [USDAmount, setUSDAmount] = React.useState(5);
  const [VNDAmount, setVNDAmount] = React.useState(12000);

  const handleUSDChange = (value: any) => {
    setUSDAmount(value)
  }

  const handleVNDChange = (value: any) => {
    setVNDAmount(value)
  }

  return (
    <Box>
      <Box p={2}>
        <Typography variant="body2">USD</Typography>
        <CurrencyTextbox 
          currency="USD" 
          min={1} 
          // max={100}
          step={1}
          value={USDAmount}
          onChange={handleUSDChange}
        ></CurrencyTextbox>
      </Box>
      <Box p={2}>
        <Typography variant="body2">VND</Typography>
        <CurrencyTextbox 
          currency="VND" 
          min={10000} 
          max={1000000} 
          step={1000}
          value={VNDAmount}
          onChange={handleVNDChange}
        ></CurrencyTextbox>
      </Box>
    </Box>
  )
}