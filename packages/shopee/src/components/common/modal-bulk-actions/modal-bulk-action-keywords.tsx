import {
  InputBase,
  MenuItem,
  Select,
  Typography,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { Field, Form, Formik } from 'formik';
import React, { useRef } from 'react';
import styled from 'styled-components';
import * as Yup from 'yup';
import { ActionIcon } from '../action-icon';
import { CurrencyTextbox } from '../textfield';
import { TooltipUI } from '../tooltip';
import { ModalBulkActionUI } from './ModalBulkAction';

const Wrapper = styled.div`
  margin: 8px 0;
`;

const Wrap = styled.div`
  border: 2px solid #e4e7e9;
  border-radius: 4px;
  height: 120px;
  padding: 8px;
  width: 448px;
  overflow: auto;
`;

const Keyword = styled.span`
  background: #f6f7f8;
  border-radius: 4px;
  padding: 2px 6px;
  font-size: 14px;
  line-height: 20px;
  color: #253746;
  margin-right: 8px;
  margin-bottom: 8px;
  display: inline-block;
`;

const Delete = styled.span`
  margin-left: 9px;
`;

const TypographyUI = styled(Typography)`
  margin-bottom: 4px;
  margin-top: 8px;
`;

export interface ModalBulkActionKeywordProps {
  keywords: any;
  open: boolean;
  hideMatchType?: boolean;
  setOpenModalBulkAction: any;
  updateSelected: any;
  onClose: any;
  currency: string;
}

const BootstrapInput = withStyles({
  root: {
    width: '100%',
  },
  disabled: {
    color: '#9FA7AE !important',
    background: '#F6F7F8 !important',
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: '#ffffff',
    border: '2px solid #D3D7DA',
    fontSize: 14,
    color: '#253746',
    padding: '5px 26px 5px 8px',
  },
})(InputBase);

const validationSchema = Yup.object().shape({
  price: Yup.string().required('Please enter Bidding price'),
});

export const ModalBulkActionKeyword = (
  props: ModalBulkActionKeywordProps,
) => {
  const {
    open,
    hideMatchType = false,
    setOpenModalBulkAction,
    updateSelected,
    onClose,
    currency = 'VND',
  } = props;

  const formRef = useRef();

  return (
    <>
      <ModalBulkActionUI
        open={open}
        onSubmit={() => {
          if (formRef.current) {
            formRef.current.handleSubmit();
          }
        }}
        onClose={onClose}
        labelSubmit="Apply"
        title="Modify Keyword"
      >
        <span>
          Modify keyword for{' '}
          <strong>{props.keywords.length} selected keywords</strong>
        </span>
        <Wrapper>
          <Wrap>
            {props.keywords.map((item: any, index: number) => {
              const keyword = item.keyword_name;
              const title =
                keyword.length > 21
                  ? `${keyword.slice(0, 21)}...`
                  : keyword;
              return (
                <TooltipUI title={keyword} key={index}>
                  <Keyword>
                    {title}
                    {props.keywords.length > 1 && (
                      <Delete
                        onClick={() => {
                          const newKeyword = props.keywords.filter(
                            (item1) =>
                              item1.keyword_id !== item.keyword_id,
                          );
                          updateSelected(newKeyword);
                        }}
                      >
                        <ActionIcon status="remove" />
                      </Delete>
                    )}
                  </Keyword>
                </TooltipUI>
              );
            })}
          </Wrap>
        </Wrapper>
        <Formik
          validationSchema={validationSchema}
          initialValues={{ price: 0, matchType: 'exact_match' }}
          onSubmit={(values) => {
            return setOpenModalBulkAction({
              open: !open,
              bidding_price: values.price,
              match_type: values.matchType,
            });
          }}
          validateOnBlur={true}
          innerRef={formRef}
        >
          {({ errors }) => {
            return (
              <Form>
                <Wrapper>
                  <TypographyUI variant="body2">
                    Bidding Price
                  </TypographyUI>
                  <Field name="price">
                    {({ field, form }: any) => (
                      <CurrencyTextbox
                        currency={currency}
                        min={1}
                        step={1}
                        error={errors.price}
                        value={field.value}
                        onChange={(value: any) => {
                          form.setFieldValue(field.name, value);
                        }}
                      />
                    )}
                  </Field>
                </Wrapper>
                {!hideMatchType && (
                  <Wrapper>
                    <TypographyUI variant="body2">
                      Match Type
                    </TypographyUI>
                    <Field name="matchType">
                      {({ field, form }: any) => (
                        <Select
                          id="demo-customized-select-native"
                          value={field.value}
                          onChange={(e: any) =>
                            form.setFieldValue(
                              field.name,
                              e.target.value,
                            )
                          }
                          input={<BootstrapInput />}
                          MenuProps={{
                            anchorOrigin: {
                              vertical: 'bottom',
                              horizontal: 'left',
                            },
                            transformOrigin: {
                              vertical: 'top',
                              horizontal: 'left',
                            },
                            getContentAnchorEl: null,
                          }}
                        >
                          <MenuItem value="exact_match">
                            Exact Match
                          </MenuItem>
                          <MenuItem value="broad_match">
                            Broad Match
                          </MenuItem>
                        </Select>
                      )}
                    </Field>
                  </Wrapper>
                )}
              </Form>
            );
          }}
        </Formik>
      </ModalBulkActionUI>
    </>
  );
};
