import { COLORS } from '@ep/shopee/src/constants';
import SwitchOffIcon from '@ep/shopee/src/images/switch-off.svg';
import SwitchOnIcon from '@ep/shopee/src/images/switch-on.svg';
import { Box, InputBase, Typography } from '@material-ui/core';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import React, { useState } from 'react';
import styled from 'styled-components';
import { ActionIcon } from '../action-icon';
import { CurrencyTextbox } from '../textfield';
import { TooltipUI } from '../tooltip';
import { ModalBulkActionUI } from './ModalBulkAction';

const Wrapper = styled.div`
  margin: 8px 0;
`;

const Wrap = styled.div`
  border: 2px solid #e4e7e9;
  border-radius: 4px;
  height: 120px;
  padding: 8px;
  width: 448px;
  overflow: auto;
`;

const Keyword = styled.span`
  background: #f6f7f8;
  border-radius: 4px;
  padding: 2px 6px;
  font-size: 14px;
  line-height: 20px;
  color: #253746;
  margin-right: 8px;
  margin-bottom: 8px;
  display: inline-block;
`;

const Delete = styled.span`
  margin-left: 9px;
`;

const TypographyUI = styled(Typography)`
  margin-bottom: 4px;
  margin-top: 8px;
`;

const useStyles = makeStyles({
  title: {
    color: '#596772',
    fontSize: 11,
    fontWeight: 'bold',
    padding: '5px 0',
  },
  switcher: {
    display: 'flex',
    alignItems: 'center',
    padding: '8px 0',
    '& img': {
      cursor: 'pointer',
      marginRight: 8,
    },
  },
  error: {
    color: COLORS.COMMON.RED,
    fontSize: 12,
    marginTop: 8,
  },
  footer: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: 8,
    borderTop: '2px solid #E4E7E9',
    '& button': {
      marginLeft: 5,
    },
  },
});

const BootstrapInput = withStyles({
  root: {
    width: '100%',
  },
  disabled: {
    color: '#9FA7AE !important',
    background: '#F6F7F8 !important',
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: '#ffffff',
    border: '2px solid #D3D7DA',
    fontSize: 14,
    color: '#253746',
    padding: '5px 26px 5px 8px',
  },
})(InputBase);

export interface ModalBulkActionShopAdsProps {
  shopAds: any;
  open: boolean;
  setOpenModalBulkAction: any;
  updateSelected: any;
  daily?: number;
  total?: number;
  currency?: string;
  onClose: () => void;
}
export const ModalBulkActionShop = (
  props: ModalBulkActionShopAdsProps,
) => {
  const {
    open,
    setOpenModalBulkAction,
    updateSelected,
    onClose,
    // currency = 'VND',
  } = props;
  const classes = useStyles();
  const [isOnNoLimit, setIsOnNoLimit] = useState(true);
  const [isOnDailyBudger, setIsOnDailyBudger] = useState(false);
  const [budget, setBudget] = useState(0);
  const [dailyBudget, setDailyBudget] = useState(0);
  const [error, setError] = useState('');
  const [currency, setCurrency] = useState(props.shopAds[0].currency);

  const handleTotalSwitch = () => setIsOnNoLimit(!isOnNoLimit);

  const handleDailySwitch = () =>
    setIsOnDailyBudger(!isOnDailyBudger);

  return (
    <>
      <ModalBulkActionUI
        open={open}
        onSubmit={() => {
          setOpenModalBulkAction({
            open: !open,
            total_budget: isOnNoLimit ? 0 : budget,
            daily_budget: isOnDailyBudger ? dailyBudget : 0,
          });
        }}
        onClose={onClose}
        labelSubmit="Apply"
        title="Modify budget"
      >
        <span>
          Modify budget for{' '}
          <strong>{props.shopAds.length} selected Shop Ads</strong>
        </span>
        <Wrapper>
          <Wrap>
            {props.shopAds.map(
              ({ shopAdsName, shopAdsId }: any, index: number) => {
                const keyword = shopAdsName;
                const title =
                  keyword.length > 21
                    ? `${keyword.slice(0, 21)}...`
                    : keyword;
                return (
                  <TooltipUI title={keyword} key={shopAdsId}>
                    <Keyword>
                      {title}
                      {props.shopAds.length > 1 && (
                        <Delete
                          onClick={() => {
                            const newKeyword = props.shopAds.filter(
                              (item) => item.shopAdsId !== shopAdsId,
                            );
                            updateSelected(newKeyword);
                          }}
                        >
                          <ActionIcon status="remove" />
                        </Delete>
                      )}
                    </Keyword>
                  </TooltipUI>
                );
              },
            )}
          </Wrap>
        </Wrapper>
        <Wrapper>
          <TypographyUI variant="body2">Budget</TypographyUI>
          <Box>
            <Box>
              <CurrencyTextbox
                currency={currency}
                disabled={isOnNoLimit}
                min={1}
                step={1}
                value={budget}
                onChange={(value: any) => setBudget(value)}
              />
            </Box>
            <Box className={classes.switcher}>
              <img
                src={isOnNoLimit ? SwitchOnIcon : SwitchOffIcon}
                height={16}
                width={30}
                onClick={handleTotalSwitch}
              />
              <Typography variant="body2" component="span">
                No limit
              </Typography>
            </Box>
            <Box className={classes.switcher}>
              <img
                src={isOnDailyBudger ? SwitchOnIcon : SwitchOffIcon}
                height={16}
                width={30}
                onClick={handleDailySwitch}
              />
              <Typography variant="body2" component="span">
                Daily budget
              </Typography>
            </Box>
            {isOnDailyBudger && (
              <Box>
                <CurrencyTextbox
                  currency={currency}
                  min={1}
                  step={1}
                  value={dailyBudget}
                  onChange={(value: any) => setDailyBudget(value)}
                />
              </Box>
            )}
          </Box>
          <Box>
            {error && <Box className={classes.error}>{error}</Box>}
          </Box>
        </Wrapper>
      </ModalBulkActionUI>
    </>
  );
};
