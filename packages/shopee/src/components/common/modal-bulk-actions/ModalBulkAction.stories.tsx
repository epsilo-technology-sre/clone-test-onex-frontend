import React, { useState } from 'react';
import { Typography } from '@material-ui/core'
import { ModalBulkActionUI } from './ModalBulkAction';
import { TooltipUI } from '../tooltip';
import { ButtonUI } from '../button';
import styled from 'styled-components';
import { ActionIcon } from '../action-icon';
import { CurrencyTextbox } from '../textfield';

export default {
  title: 'Shopee/Modal Bulk Actions',
};

const Wrapper = styled.div`
  margin: 8px 0;
`;


const Wrap = styled.div`
  border: 2px solid #E4E7E9;
  border-radius: 4px;
  height: 120px;
  padding: 8px;
  width: 448px;
  overflow: auto;
`;

const Keyword = styled.span`
  background: #F6F7F8;
  border-radius: 4px;
  padding: 2px 6px;
  font-size: 14px;
  line-height: 20px;
  color: #253746;
  margin-right: 8px;
  margin-bottom: 8px;
  display: inline-block;
`;

const Delete = styled.span`
  margin-left: 9px;
`;

export const Primary = () => {
  const [open, setOpen] = useState(true);
  const arrKey = []
  for (let i = 0; i < 50; i++){
    arrKey.push(`keyword ${i}`)
  }
  const [keywords, setKeywords] = useState(arrKey);
  const [USDAmount, setUSDAmount] = React.useState(5);

  const handleUSDChange = (value: any) => {
    setUSDAmount(value)
  }

  return (
    <>
      <ButtonUI
        onClick={() => setOpen(!open)}
        label="Show modal"
        size="small"
        colorButton="#F6F7F8"
      />
      <ModalBulkActionUI
        open={open}
        onSubmit={() => {
          setOpen(!open);
          console.log('submit');
        }}
        onClose={() => setOpen(false)}
        labelSubmit="Active"
        title="Modify Keyword"
      >
        <span>Modify keyword for <strong>03 selected keywords</strong></span>
        <Wrapper>
        <Wrap>
          {keywords.map((keyword: string, index: number) => {
            const title = keyword.length > 21 ? `${keyword.slice(0, 21)}...` : keyword
            return <TooltipUI title={keyword} key={index}>
              <Keyword>
                {title}
                {keywords.length > 1 && <Delete onClick={() => {
                  const newKeyword = keywords.filter((item, id) => index !== id)
                  setKeywords(newKeyword)
                }}>
                  <ActionIcon status="remove" />
                </Delete>}
              </Keyword>
            </TooltipUI>
          })}
        </Wrap>
        </Wrapper>
        <Wrapper>
          <Typography variant="body2">Bidding Price</Typography>
          <CurrencyTextbox
            currency="USD"
            min={1}
            step={1}
            value={USDAmount}
            onChange={handleUSDChange}
          />
        </Wrapper>
      </ModalBulkActionUI>
    </>
  );
};
