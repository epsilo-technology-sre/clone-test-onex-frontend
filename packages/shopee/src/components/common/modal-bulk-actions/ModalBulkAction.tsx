import React from 'react';
import { DialogTitle, Dialog, DialogActions, DialogContent, DialogContentText } from '@material-ui/core';
import { ButtonUI } from '../button';

export interface ModalConfirmUIProps {
  open: boolean,
  onSubmit: () => any,
  onClose: () => any,
  labelSubmit: string,
  disabled?: boolean,
  title: string,
  children: any,
}

export function ModalBulkActionUI(props: ModalConfirmUIProps) {
  const { open, onSubmit, onClose, labelSubmit, disabled, title } = props;

  return (
    <Dialog aria-labelledby="simple-dialog-title" open={open}>
      <DialogTitle id="simple-dialog-title">{title}</DialogTitle>
      <DialogContent>
        <DialogContentText>{props.children}</DialogContentText>
      </DialogContent>
      <DialogActions>
        <ButtonUI
          onClick={onClose}
          label="Cancel"
          size="small"
          colorButton="#F6F7F8"
        />
        <ButtonUI
          onClick={onSubmit}
          autoFocus
          disabled={disabled}
          label={labelSubmit}
          size="small"
          variant="contained"
        />
      </DialogActions>
    </Dialog>
  );
}
