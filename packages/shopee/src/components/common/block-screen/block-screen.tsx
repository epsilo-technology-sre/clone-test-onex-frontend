import React from 'react'
import Box from '@material-ui/core/Box'
import { makeStyles } from '@material-ui/core/styles'
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles({
  root: {
    position: 'fixed',
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    background: 'rgba(0, 0, 0, 0.3)',
    zIndex: 1000,
  },
})

interface BlockScreenUI {
  isShow: boolean
}

export const BlockScreenUI = (props: any) => {
  const classes = useStyles();
  const { isShow } = props;
  return (
    isShow && <Box className={classes.root}>
      <CircularProgress />
    </Box>
  )
}

BlockScreenUI.defaultProps = {
  isShow: false
}
