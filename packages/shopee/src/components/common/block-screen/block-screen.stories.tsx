import React from 'react'
import { BlockScreenUI } from './block-screen'

export default {
  title: 'Shopee/BlockScreen'
}

export const BlockScreen = () => {
  return (
    <BlockScreenUI isShow={true}></BlockScreenUI>
  )
}