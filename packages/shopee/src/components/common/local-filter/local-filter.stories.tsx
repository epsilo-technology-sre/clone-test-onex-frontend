import React from 'react'
import Paper from '@material-ui/core/Paper'
import Box from '@material-ui/core/Box'
import { LocalFilter } from './local-filter'

export default {
  title: 'Shopee/LocalFilter'
}

export const Main = () => {
  const [searchText, setSearchText] = React.useState('')
  const [status, setStatus] = React.useState('All')
  const statusList = ['All', 'Running', 'Scheduled', 'Syncing', 'Paused', 'Ended']
  const handleChangeStatus = (value: any) => {
    setStatus(value);
  }

  const handleChangeSearchText = (value: any) => {
    setSearchText(value);
  }

  return (
    <Paper>
      <Box p={3}>
        <Box>
          <LocalFilter 
            statusList={statusList}
            status={status} 
            onChangeStatus={handleChangeStatus}
            onChangeSearchText={handleChangeSearchText}
          ></LocalFilter>
        </Box>
        <Box>
          You selected options <b>"{status}"</b>
        </Box>
        <Box>
          You searched <b>"{searchText}"</b>
        </Box>
      </Box>
    </Paper>
  )
}