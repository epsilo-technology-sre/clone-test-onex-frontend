import { COLORS } from '@ep/shopee/src/constants';
import Box from '@material-ui/core/Box';
import FormControl from '@material-ui/core/FormControl';
import Grid from '@material-ui/core/Grid';
import InputAdornment from '@material-ui/core/InputAdornment';
import MenuItem from '@material-ui/core/MenuItem';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Select from '@material-ui/core/Select';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import SearchIcon from '@material-ui/icons/Search';
import React, { useState } from 'react';
import { ButtonUI } from '../button';

const useStyles = makeStyles({
  root: {
    background: '#F6F7F8',
    borderRadius: 4,
    padding: '0.5rem',
    marginLeft: 5,
    '& svg': {
      width: 18,
      height: 18,
    },
  },
  statusWrapper: {
    background: '#F6F7F8',
    borderRadius: 4,
  },
  statusItem: {
    textTransform: 'capitalize',
    fontSize: 14,
  },
});

const SearchBox = withStyles({
  root: {
    marginRight: 5,
    width: 320,
    '&:hover .MuiOutlinedInput-notchedOutline': {
      borderColor: '#C2C7CB',
    },
  },
  notchedOutline: {
    border: '2px solid #C2C7CB',
  },
  input: {
    padding: 8,
    fontSize: 14,
  },
})(OutlinedInput);

const StatusListbox = withStyles({
  root: {
    padding: `0.5rem 1rem`,
    fontSize: 14,
    '& .MuiOutlinedInput-notchedOutline': {
      border: 'none',
      background: 'red',
    },
    fieldset: {
      background: 'green',
    },
  },
  selectMenu: {
    paddingTop: 7,
    paddingBottom: 7,
  },
})(Select);

interface LocalFilterProps {
  search: string;
  statusList: any;
  status: string;
  onChangeSearchText: any;
  onChangeStatus: any;
  onAddProduct?: any;
  onAddKeyword?: any;
}

export const LocalFilter = (props: LocalFilterProps) => {
  const classes = useStyles();

  const {
    search,
    status,
    statusList,
    onChangeSearchText,
    onChangeStatus,
    onAddProduct,
    onAddKeyword,
  } = props;

  const [searchText, setSearchText] = useState('');

  React.useEffect(() => {
    console.log('Local filter search', search);
    setSearchText(search);
  }, [search]);

  const handleChangeSearchText = (event: any) => {
    setSearchText(event.target.value);
  };

  const handleSearchKeyUp = (event: any) => {
    if (event.key === 'Enter') {
      onChangeSearchText(event.target.value);
    }
  };

  const handleChangeStatus = (event: any) => {
    onChangeStatus(event.target.value);
  };

  const formatSelectedValue = (value: any) => {
    return (
      <Box className={classes.statusItem}>
        Status: <b>{value}</b>
      </Box>
    );
  };

  return (
    <Box my={2}>
      <Grid container justify="space-between" alignItems="center">
        <Grid item>
          <SearchBox
            id="campaign-searchbox"
            placeholder="Search"
            value={searchText}
            onChange={handleChangeSearchText}
            onKeyUp={handleSearchKeyUp}
            startAdornment={
              <InputAdornment position="start">
                <SearchIcon fontSize="small" />
              </InputAdornment>
            }
            labelWidth={0}
            autoComplete="off"
          ></SearchBox>
          <FormControl
            variant="outlined"
            size="small"
            className={classes.statusWrapper}
          >
            <StatusListbox
              labelId="label"
              id="campaign-status-select"
              value={status}
              IconComponent={ExpandMoreIcon}
              autoWidth={false}
              defaultValue="All"
              renderValue={formatSelectedValue}
              MenuProps={{
                anchorOrigin: {
                  vertical: 'bottom',
                  horizontal: 'left',
                },
                transformOrigin: {
                  vertical: 'top',
                  horizontal: 'left',
                },
                getContentAnchorEl: null,
              }}
              onChange={handleChangeStatus}
            >
              {statusList.map((item, index) => (
                <MenuItem
                  key={index}
                  value={item}
                  className={classes.statusItem}
                >
                  {item}
                </MenuItem>
              ))}
            </StatusListbox>
          </FormControl>
        </Grid>
        <Grid item>
          {onAddProduct && (
            <ButtonUI
              size="small"
              label="Add product"
              colorButton={COLORS.COMMON.GRAY}
              iconLeft={<AddIcon />}
              onClick={onAddProduct}
            />
          )}
          {onAddKeyword && (
            <ButtonUI
              size="small"
              label="Add keyword"
              colorButton={COLORS.COMMON.GRAY}
              iconLeft={<AddIcon />}
              onClick={onAddKeyword}
            />
          )}
          {/*<IconButton classes={{ root: classes.root }} size="small">*/}
          {/*  <GetAppIcon />*/}
          {/*</IconButton>*/}
          {/*<IconButton classes={{ root: classes.root }} size="small">*/}
          {/*  <SettingsIcon />*/}
          {/*</IconButton>*/}
        </Grid>
      </Grid>
    </Box>
  );
};
