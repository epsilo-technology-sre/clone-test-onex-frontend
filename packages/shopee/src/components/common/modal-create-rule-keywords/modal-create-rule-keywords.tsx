import {
  COLORS,
  CURRENCY_LIMITATION,
  DATE_FORMAT,
} from '@ep/shopee/src/constants';
import { formatCurrency } from '@ep/shopee/src/utils/utils';
import {
  Box,
  Dialog,
  FormHelperText,
  Grid,
  IconButton,
  InputAdornment,
  InputBase,
  MenuItem,
  OutlinedInput,
  Select,
  TextField,
  Typography,
} from '@material-ui/core';
import MuiDialogActions from '@material-ui/core/DialogActions';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import {
  createStyles,
  makeStyles,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import moment from 'moment';
import React, { useRef } from 'react';
import styled from 'styled-components';
import * as Yup from 'yup';
import { ActionIcon } from '../action-icon';
import { ButtonLinkUI, ButtonUI } from '../button';
import { CurrencyCode } from '../currency-code';
import { initColumns } from '../modal-add-rule/columns';
import { SingleDatePickerUI } from '../single-date-picker';
import { TableUINoCheckbox } from '../table';
import { TooltipUI } from '../tooltip';
import { dataOperator, dataPeriod } from './constants';

export interface ModalAddRuleProps {
  open: boolean;
  onClose?: any;
  keywords: any;
  products: any;
  onSubmit: any;
  onAddRule: any;
  onOpenExistingRule: any;
  setKeywords: any;
  currency: string;
  matchType: string;
  actionList: any[];
  metricList: {
    [key: string]: any[];
  };
  isSubmitting: boolean;
}

export function ModalCreateRuleKeywords(props: ModalAddRuleProps) {
  const {
    open,
    onClose,
    keywords,
    onSubmit,
    onAddRule,
    setKeywords,
    products,
    currency,
    matchType,
    actionList,
    metricList,
    onOpenExistingRule,
  } = props;
  const formRef = useRef();
  const classes = useStyles();

  const [errorMessage, setErrorMessage] = React.useState('');

  React.useEffect(() => {
    if (open) {
      setErrorMessage('');
    }
  }, [open]);

  const increaseActions = [
    'increase_bidding_price',
    'increase_bidding_price_sakw',
  ];

  const decreaseActions = [
    'decrease_bidding_price',
    'decrease_bidding_price_sakw',
  ];

  const biddingPriceActions = [
    ...increaseActions,
    ...decreaseActions,
  ];

  const handleClose = () => {
    onClose(false);
  };

  const validationSchema = React.useMemo(() => {
    return Yup.object().shape({
      ruleName: Yup.string().required('Please enter rule name'),
      action: Yup.string().required('Please choose action'),
      fromDate: Yup.string().required('From date is empty'),
      toDate: Yup.string().required('To date is empty'),
      biddingPriceMin: Yup.number().when(
        'action',
        (action: string) => {
          if (decreaseActions.includes(action)) {
            return Yup.number()
              .min(
                CURRENCY_LIMITATION[currency][matchType].MIN,
                `Minimum Bidding Price must be greater than or equal to ${CURRENCY_LIMITATION[currency][matchType].MIN} ${currency}`,
              )
              .test(
                'should-not-contains-e',
                'Invalid number',
                (v) => !String(v).includes('e'),
              );
          } else {
            return Yup.number().notRequired();
          }
        },
      ),
      biddingPriceMax: Yup.number().when(
        'action',
        (action: string) => {
          if (increaseActions.includes(action)) {
            return Yup.number()
              .min(
                CURRENCY_LIMITATION[currency][matchType].MIN,
                `Maximum Bidding Price must be greater than or equal to ${CURRENCY_LIMITATION[currency][matchType].MIN} ${currency}`,
              )
              .test(
                'should-not-contains-e',
                'Invalid number',
                (v) => !String(v).includes('e'),
              );
          } else {
            return Yup.number().notRequired();
          }
        },
      ),
      ruleValue: Yup.object().shape({
        value: Yup.number()
          .required('Please input value')
          .when('action', {
            is: (val) => {
              const valuePerformance = val || '';
              const disabled =
                biddingPriceActions.indexOf(
                  valuePerformance.toLowerCase(),
                ) !== -1;
              return !disabled;
            },
            then: Yup.number().optional(),
          }),
      }),
      rules: Yup.object().shape({
        typeOfPerformance: Yup.string(),
        conditionPeriod: Yup.string().when('typeOfPerformance', {
          is: (val) => {
            const valuePerformance = val || '';
            const disabled =
              ['percent_discount_product', 'ranking'].indexOf(
                valuePerformance.toLowerCase(),
              ) !== -1;
            return !disabled;
          },
          then: Yup.string().optional(),
        }),
        operator: Yup.string(),
        value: Yup.number(),
      }),
    });
  }, [currency, matchType]);

  return (
    <div>
      <Dialog
        open={open}
        classes={{ paper: classes.paper }}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle
          id="customized-dialog-title"
          className={classes.dialogTitle}
          onClose={handleClose}
        >
          Create rule
        </DialogTitle>
        <DialogContent>
          <span>
            Create rule for{' '}
            <strong>
              {`${keywords.length} selected ${
                keywords.length > 1 ? 'keywords' : 'keyword'
              }`}{' '}
            </strong>
          </span>
          <Wrapper>
            <Wrap>
              {keywords.map((item: any, index: number) => {
                const keyword = item.keyword_name;
                const title =
                  keyword.length > 21
                    ? `${keyword.slice(0, 21)}...`
                    : keyword;
                return (
                  <TooltipUI title={keyword} key={index}>
                    <Product>
                      {title}
                      {keywords.length > 1 && (
                        <Delete
                          onClick={() => {
                            const newKeyword = keywords.filter(
                              (item1: any) =>
                                item1.keyword_id !== item.keyword_id,
                            );
                            setKeywords(newKeyword);
                          }}
                        >
                          <ActionIcon status="remove" />
                        </Delete>
                      )}
                    </Product>
                  </TooltipUI>
                );
              })}
            </Wrap>
          </Wrapper>
          <Wrapper style={{display: products.length === 0 ? 'none': undefined}}>
            <Wrap>
              {products.map((item: any, index: number) => {
                let title = item.product_name;
                if (title.length > 21) {
                  title = `${title.slice(0, 21)}...`;
                }

                return (
                  <TooltipUI title={item.product_name} key={index}>
                    <Product>{title}</Product>
                  </TooltipUI>
                );
              })}
            </Wrap>
          </Wrapper>
          <FormHelperText id="component-text">
            Select a product to view exact keywords have been
            allocated on each of them.
          </FormHelperText>
          <Formik
            validationSchema={validationSchema}
            initialValues={{
              existingRule: '',
              ruleName: '',
              action: '',
              biddingPriceMin: 0,
              biddingPriceMax: 0,
              fromDate: moment().format(DATE_FORMAT),
              toDate: moment().format(DATE_FORMAT),
              ruleValue: {
                type: 'absolute',
                value: 0,
              },
              rules: {
                typeOfPerformance: '',
                conditionPeriod: '',
                operator: '',
                value: '',
              },
              conditions: [],
            }}
            onSubmit={(values: any) => {
              if (values.conditions.length > 0) {
                return onSubmit({
                  ...values,
                  listRules: values.conditions,
                });
              } else {
                setErrorMessage('Please enter condition');
              }
            }}
            validateOnBlur={true}
            innerRef={formRef}
          >
            {(formik: any) => {
              const { errors } = formik;
              const handleRemoveRule = (props) => {
                const { rule } = props;
                const { conditions } = formik.values;
                const newRule = conditions.filter(
                  (condition) => condition.id !== rule.id,
                );
                formik.setFieldValue('conditions', newRule);
              };
              return (
                <Form
                // className={classes.wrapForm}
                >
                  <Typography
                    variant="body2"
                    className={classes.labelInput}
                  >
                    Rule name
                  </Typography>
                  <Field name="ruleName">
                    {({ field }: any) => {
                      return (
                        <TextField
                          className={classes.input}
                          fullWidth
                          id="outlined-size-small"
                          autoComplete="off"
                          variant="outlined"
                          placeholder="Enter rule name"
                          size="small"
                          {...field}
                        />
                      );
                    }}
                  </Field>
                  <ErrorMessage
                    component={FormHelperText}
                    name="ruleName"
                    className={classes.error}
                  />
                  <Typography
                    variant="body2"
                    className={classes.labelInput}
                  >
                    Action
                  </Typography>
                  <Field name="action">
                    {({ field }: any) => (
                      <Select
                        id="action"
                        {...field}
                        input={<BootstrapInput />}
                        displayEmpty
                        MenuProps={{
                          anchorOrigin: {
                            vertical: 'bottom',
                            horizontal: 'left',
                          },
                          transformOrigin: {
                            vertical: 'top',
                            horizontal: 'left',
                          },
                          getContentAnchorEl: null,
                        }}
                      >
                        <MenuItem
                          style={{ display: 'none' }}
                          value={''}
                          disabled
                        >
                          Choose action
                        </MenuItem>
                        {actionList.map((item: any) => (
                          <MenuItem
                            value={item.value}
                            key={item.value}
                          >
                            {item.name}
                          </MenuItem>
                        ))}
                      </Select>
                    )}
                  </Field>
                  <ErrorMessage
                    component={FormHelperText}
                    name="action"
                    className={classes.error}
                  />
                  {biddingPriceActions.includes(
                    formik.values.action,
                  ) && (
                    <>
                      <Grid container spacing={2}>
                        <Grid item xs={4}>
                          <Typography
                            variant="body2"
                            className={classes.labelInput}
                          >
                            Rule value
                          </Typography>
                          <Field name="ruleValue.type">
                            {({ field }: any) => {
                              return (
                                <Select
                                  id="type"
                                  {...field}
                                  input={<BootstrapInput />}
                                  MenuProps={{
                                    anchorOrigin: {
                                      vertical: 'bottom',
                                      horizontal: 'left',
                                    },
                                    transformOrigin: {
                                      vertical: 'top',
                                      horizontal: 'left',
                                    },
                                    getContentAnchorEl: null,
                                  }}
                                >
                                  <MenuItem value={'absolute'}>
                                    Absolute
                                  </MenuItem>
                                  <MenuItem value={'%'}>%</MenuItem>
                                </Select>
                              );
                            }}
                          </Field>
                        </Grid>
                        <Grid item xs={8}>
                          <Typography
                            variant="body2"
                            style={{ color: 'transparent' }}
                            className={classes.labelInput}
                          >
                            d
                          </Typography>
                          <Field name="ruleValue.value">
                            {({ field, form }: any) => {
                              return (
                                <InputUI
                                  id="daily-budget"
                                  type="number"
                                  fullWidth
                                  size={'small'}
                                  {...field}
                                  startAdornment={
                                    form.values.ruleValue.type ===
                                      'absolute' && (
                                      <InputAdornment position="start">
                                        <CurrencyCode
                                          currency={currency}
                                        />
                                      </InputAdornment>
                                    )
                                  }
                                  error={!!errors.budget}
                                  autoComplete="off"
                                />
                              );
                            }}
                          </Field>
                          <ErrorMessage
                            component={FormHelperText}
                            name="ruleValue.value"
                            className={classes.error}
                          />
                        </Grid>
                      </Grid>
                      <Grid container spacing={2}>
                        <Grid item xs={6}>
                          <Typography
                            variant="body2"
                            className={classes.labelInput}
                          >
                            Min Bid
                          </Typography>
                          <Field name="biddingPriceMin">
                            {({ field }: any) => {
                              return (
                                <InputUI
                                  id="daily-budget"
                                  type="number"
                                  disabled={increaseActions.includes(
                                    formik.values.action,
                                  )}
                                  fullWidth
                                  {...field}
                                  startAdornment={
                                    <InputAdornment position="start">
                                      <CurrencyCode
                                        currency={currency}
                                      />
                                    </InputAdornment>
                                  }
                                  error={!!errors.biddingPriceMin}
                                  autoComplete="off"
                                />
                              );
                            }}
                          </Field>
                          <FormHelperText id="component-helper-text">
                            Min:{' '}
                            {formatCurrency(
                              CURRENCY_LIMITATION[currency][matchType]
                                .MIN,
                              currency,
                            )}
                          </FormHelperText>
                          <ErrorMessage
                            component={FormHelperText}
                            name="biddingPriceMin"
                            className={classes.error}
                          />
                        </Grid>
                        <Grid item xs={6}>
                          <Typography
                            variant="body2"
                            className={classes.labelInput}
                          >
                            Max Bid
                          </Typography>
                          <Field name="biddingPriceMax">
                            {({ field }: any) => {
                              return (
                                <InputUI
                                  id="daily-budget"
                                  type="number"
                                  fullWidth
                                  disabled={decreaseActions.includes(
                                    formik.values.action,
                                  )}
                                  {...field}
                                  startAdornment={
                                    <InputAdornment position="start">
                                      <CurrencyCode
                                        currency={currency}
                                      />
                                    </InputAdornment>
                                  }
                                  error={!!errors.max}
                                  autoComplete="off"
                                />
                              );
                            }}
                          </Field>
                          <ErrorMessage
                            component={FormHelperText}
                            name="biddingPriceMax"
                            className={classes.error}
                          />
                        </Grid>
                      </Grid>
                    </>
                  )}
                  <Typography
                    variant="body2"
                    className={classes.labelInput}
                  >
                    From Date
                  </Typography>
                  <Field name="fromDate">
                    {({ field, form }: any) => (
                      <SingleDatePickerUI
                        date={field.value}
                        minDate={moment().format(DATE_FORMAT)}
                        onChange={(date: any) =>
                          form.setFieldValue(field.name, date)
                        }
                      />
                    )}
                  </Field>
                  <Typography
                    variant="body2"
                    className={classes.labelInput}
                  >
                    To Date
                  </Typography>
                  <Field name="toDate">
                    {({ field, form }: any) => {
                      return (
                        <SingleDatePickerUI
                          date={field.value}
                          placeholder="No limit"
                          minDate={moment().format(DATE_FORMAT)}
                          onChange={(date: any) =>
                            form.setFieldValue(field.name, date)
                          }
                        />
                      );
                    }}
                  </Field>
                  <div className={classes.wrapConditions}>
                    <Typography
                      variant="body2"
                      className={classes.labelInput}
                    >
                      Type of performance
                    </Typography>
                    <Field name="rules.typeOfPerformance">
                      {({ field, form }: any) => {
                        return (
                          <Select
                            id="typeOfPerformance"
                            {...field}
                            disabled={form.values.action === ''}
                            input={<BootstrapInput />}
                            displayEmpty
                            MenuProps={{
                              anchorOrigin: {
                                vertical: 'bottom',
                                horizontal: 'left',
                              },
                              transformOrigin: {
                                vertical: 'top',
                                horizontal: 'left',
                              },
                              getContentAnchorEl: null,
                            }}
                            onChange={(event) => {
                              console.log('Change type', event);
                              form.setFieldValue(
                                event.target.name,
                                event.target.value,
                              );
                              if (
                                [
                                  'percent_discount_product',
                                  'ranking',
                                ].includes(
                                  event.target.value.toLowerCase(),
                                )
                              ) {
                                form.setFieldValue(
                                  'rules.conditionPeriod',
                                  '',
                                );
                              }
                            }}
                          >
                            <MenuItem
                              style={{ display: 'none' }}
                              value={''}
                              disabled
                            >
                              Choose Type of performance
                            </MenuItem>
                            {(
                              metricList[form.values.action] || []
                            ).map((item) => (
                              <MenuItem
                                value={item.value}
                                key={item.value}
                              >
                                {item.name}
                              </MenuItem>
                            ))}
                          </Select>
                        );
                      }}
                    </Field>
                    <Typography
                      variant="body2"
                      className={classes.labelInput}
                    >
                      Condition period
                    </Typography>
                    <Field name="rules.conditionPeriod">
                      {({ field, form }: any) => {
                        const valuePerformance = form.values.rules.typeOfPerformance.toLowerCase();
                        const disabled =
                          [
                            'percent_discount_product',
                            'ranking',
                          ].indexOf(valuePerformance) !== -1;
                        return (
                          <Select
                            disabled={
                              disabled || form.values.action === ''
                            }
                            id="conditionPeriod"
                            {...field}
                            input={<BootstrapInput />}
                            displayEmpty
                            MenuProps={{
                              anchorOrigin: {
                                vertical: 'bottom',
                                horizontal: 'left',
                              },
                              transformOrigin: {
                                vertical: 'top',
                                horizontal: 'left',
                              },
                              getContentAnchorEl: null,
                            }}
                          >
                            <MenuItem
                              style={{ display: 'none' }}
                              value={''}
                              disabled
                            >
                              Choose Condition period
                            </MenuItem>
                            {dataPeriod.map((item: any) => (
                              <MenuItem
                                value={item.value}
                                key={item.value}
                              >
                                {item.name}
                              </MenuItem>
                            ))}
                          </Select>
                        );
                      }}
                    </Field>
                    <Typography
                      variant="body2"
                      className={classes.labelInput}
                    >
                      Operator
                    </Typography>
                    <Field name="rules.operator">
                      {({ field, form }: any) => {
                        return (
                          <Select
                            id="operator"
                            {...field}
                            disabled={form.values.action === ''}
                            input={<BootstrapInput />}
                            displayEmpty
                            MenuProps={{
                              anchorOrigin: {
                                vertical: 'bottom',
                                horizontal: 'left',
                              },
                              transformOrigin: {
                                vertical: 'top',
                                horizontal: 'left',
                              },
                              getContentAnchorEl: null,
                            }}
                          >
                            <MenuItem
                              style={{ display: 'none' }}
                              value={''}
                              disabled
                            >
                              Choose Operator
                            </MenuItem>
                            {dataOperator.map((item: any) => (
                              <MenuItem
                                value={item.value}
                                key={item.value}
                              >
                                {item.name}
                              </MenuItem>
                            ))}
                          </Select>
                        );
                      }}
                    </Field>
                    <Typography
                      variant="body2"
                      className={classes.labelInput}
                    >
                      Value
                    </Typography>
                    <Box>
                      <Field name="rules.value">
                        {({ field, form }: any) => {
                          return (
                            <InputUI
                              disabled={form.values.action === ''}
                              id="daily-budget"
                              type="number"
                              fullWidth
                              {...field}
                              autoComplete="off"
                              className={classes.input}
                            />
                          );
                        }}
                      </Field>
                      <ErrorMessage
                        component={FormHelperText}
                        name="rules.value"
                        className={classes.error}
                      />
                    </Box>
                    <Box style={{ marginTop: 16 }}>
                      <Field>
                        {({ field, form }: any) => {
                          const {
                            conditionPeriod,
                            operator,
                            typeOfPerformance,
                            value,
                          } = field.value.rules;
                          const valuePerformance = form.values.rules.typeOfPerformance.toLowerCase();
                          let checkedValues = [
                            typeOfPerformance,
                            conditionPeriod,
                            operator,
                            value,
                          ];
                          if (
                            [
                              'percent_discount_product',
                              'ranking',
                            ].includes(valuePerformance)
                          ) {
                            checkedValues = [
                              typeOfPerformance,
                              operator,
                              value,
                            ];
                          }
                          let disabled = checkedValues.includes('');
                          return (
                            <ButtonUI
                              disabled={disabled}
                              label="Add condition"
                              size="small"
                              colorButton="#F6F7F8"
                              onClick={() => {
                                const { rules } = field.value;
                                const newCondition = [
                                  ...form.values.conditions,
                                  {
                                    value: rules.value,
                                    period: rules.conditionPeriod,
                                    metric_code:
                                      rules.typeOfPerformance,
                                    operator_code: rules.operator,
                                    id: `condition${Math.floor(
                                      Math.random() * 10,
                                    )}`,
                                  },
                                ];
                                form.setFieldValue(
                                  'conditions',
                                  newCondition,
                                );
                                form.setFieldValue(
                                  'rules',
                                  form.initialValues.rules,
                                );
                                setErrorMessage('');
                              }}
                            />
                          );
                        }}
                      </Field>
                      <Field>
                        {({ form }: any) => {
                          return (
                            <ButtonUI
                              label="Reset"
                              size="small"
                              variant={'outlined'}
                              borderColor={'transparent'}
                              colorButton="#333f50"
                              onClick={() =>
                                form.setFieldValue(
                                  'rules',
                                  form.initialValues.rules,
                                )
                              }
                            />
                          );
                        }}
                      </Field>
                    </Box>
                  </div>
                  <Field name={'conditions'}>
                    {({ field, form }: any) => {
                      const rows = field.value.map((r) => {
                        const metricCode = metricList[
                          form.values.action
                        ].filter((a) => a.value === r.metric_code);
                        const period = dataPeriod.filter(
                          (p) => p.value === r.period,
                        );
                        return {
                          ...r,
                          metric_code: metricCode[0]?.name || '',
                          period: period[0]?.name || '',
                        };
                      });
                      return (
                        <TableUINoCheckbox
                          key={field.value.length}
                          className="add-rule"
                          columns={initColumns({
                            onCellClick: handleRemoveRule,
                          })}
                          rows={rows}
                          noData="No data"
                          onSort={() => {}}
                        />
                      );
                    }}
                  </Field>
                </Form>
              );
            }}
          </Formik>
          <Box className={classes.error}>{errorMessage}</Box>
        </DialogContent>
        <DialogActions>
          <Grid container justify="space-between">
            <Grid xs={6}>
              <ButtonUI
                label="Existing rule"
                size="small"
                colorButton="#F6F7F8"
                onClick={onOpenExistingRule}
              />
            </Grid>
            <Grid xs={6}>
              <Grid container justify="flex-end">
                <ButtonLinkUI
                  label="Cancel"
                  size="small"
                  colortext="#f16145"
                  onClick={handleClose}
                />
                <ButtonUI
                  label="Save rule"
                  size="small"
                  variant="contained"
                  loading={props.isSubmitting}
                  onClick={() => {
                    if (formRef.current) {
                      formRef.current.handleSubmit();
                    }
                  }}
                />
              </Grid>
            </Grid>
          </Grid>
        </DialogActions>
      </Dialog>
    </div>
  );
}

const Product = styled.span`
  background: #f6f7f8;
  border-radius: 4px;
  padding: 2px 6px;
  font-size: 14px;
  line-height: 20px;
  color: #253746;
  margin-right: 8px;
  margin-bottom: 8px;
  display: inline-block;
`;

const Wrap = styled.div`
  border: 2px solid #e4e7e9;
  border-radius: 4px;
  max-height: 120px;
  padding: 8px 8px 0 8px;
  width: 100%;
  overflow: auto;
`;

const Wrapper = styled.div`
  margin: 8px 0;
`;

const Delete = styled.span`
  margin-left: 9px;
  float: right;
  line-height: 14px;
`;

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });

const useStyles = makeStyles(() => ({
  paper: { width: 480, maxHeight: 528 },
  wrapForm: { maxHeight: 208, overflow: 'auto' },
  wrapConditions: {
    padding: 16,
    border: '2px solid #e4e7e9',
    borderRadius: 4,
    marginTop: 8,
  },
  error: {
    color: COLORS.COMMON.RED,
    fontSize: 12,
  },
  labelInput: {
    marginTop: 8,
    marginBottom: 4,
    fontWeight: 'bold',
    fontSize: 11,
    lineHeight: '16px',
  },
  input: {
    '& .MuiOutlinedInput-notchedOutline': {
      border: '2px solid #e4e7e9',
    },
  },
  dialogTitle: {
    padding: '16px 16px 0 16px',
  },
}));

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose?: () => void;
  classes: any;
}

export const InputUI = withStyles({
  root: {
    fontSize: 14,
    '& fieldset': {
      borderColor: '#e4e7e9',
      borderWidth: 2,
    },
    '&.Mui-focused fieldset': {
      borderColor: '#485764!important',
    },
  },
  input: {
    height: 40,
    paddingTop: 0,
    paddingBottom: 0,
  },
})(OutlinedInput);

const BootstrapInput = withStyles({
  root: {
    width: '100%',
  },
  disabled: {
    color: '#9FA7AE !important',
    background: '#F6F7F8 !important',
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: '#ffffff',
    border: '2px solid #e4e7e9',
    fontSize: 14,
    color: '#253746',
    padding: '8px 26px 9px 8px',
  },
})(InputBase);

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle
      disableTypography
      className={classes.root}
      {...other}
    >
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);
