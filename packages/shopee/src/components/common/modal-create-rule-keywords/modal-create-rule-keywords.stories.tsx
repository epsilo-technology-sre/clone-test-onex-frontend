import { Button, Paper } from '@material-ui/core';
import React, { useState } from 'react';
import { dataAction, getTypeOfPerformance } from './constants';
import { ModalCreateRuleKeywords } from './modal-create-rule-keywords';

export default {
  title: 'Shopee/Modal create rule keywords',
};

const type = ['broad_match', 'exact_match'];

const dataProducts = Array.from({ length: 5 }, (_, key) => ({
  product_name: `product ${key}`,
  product_id: `SKYPRODUCT-${key}`,
}));

const dataKeywords = Array.from({ length: 10 }, (_, key) => ({
  keyword_name: `keyword ${key}`,
  keyword_id: `SKYPRODUCT-${key}`,
  matchType: type[Math.floor(Math.random() * Math.floor(2))],
}));

const dataTypeOfPerformance: any = {};
dataAction.forEach((action) => {
  dataTypeOfPerformance[action.value] = getTypeOfPerformance(
    action.value,
  );
});

// const dataRules = Array.from({ length: 2 }, (_, key) => ({
//   keyword_name: `keyword ${key}`,
//   keyword_id: `SKYPRODUCT-${key}`,
//   id: `ads${key}`,
//   typeOfPerformance: `Ads item sold ${key}`,
//   conditionPeriod: `Last ${key + 1} days`,
//   operator: '=',
//   value: (key + 1) * 10000,
// }));

export const main = () => {
  const [open, setOpen] = useState(true);
  const [rules, setRules] = useState([]);
  const [keywords, setKeywords] = useState(dataKeywords);

  const matchType = keywords.filter(
    (item) => item.matchType === 'broad_match',
  );

  return (
    <Paper>
      <Button onClick={() => setOpen(true)}>Show modal</Button>
      <ModalCreateRuleKeywords
        open={open}
        rules={rules}
        onAddRule={(r: any) => setRules(r)}
        onSubmit={(value: any) => console.log('valuev', value)}
        onClose={(e: any) => setOpen(e)}
        keywords={keywords}
        products={dataProducts}
        setKeywords={(k: any) => setKeywords(k)}
        currency="VND"
        matchType={
          matchType.length > 0 ? 'BROAD_MATCH' : 'EXACT_MATCH'
        }
        actionList={dataAction}
        metricList={dataTypeOfPerformance}
      />
    </Paper>
  );
};
