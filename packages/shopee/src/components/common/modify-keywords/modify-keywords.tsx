import {
  COLORS,
  CURRENCY_LIMITATION,
  MATCH_TYPE,
  NO_MATCH_TYPE_LIMITATION,
} from '@ep/shopee/src/constants';
import {
  Box,
  FormHelperText,
  Grid,
  InputAdornment,
  InputBase,
  makeStyles,
  MenuItem,
  OutlinedInput,
  Select,
  Typography,
  withStyles,
} from '@material-ui/core';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import React from 'react';
import * as Yup from 'yup';
import { ButtonUI } from '../button';
import { CurrencyCode } from '../currency-code';

const useStyle = makeStyles({
  root: {
    background: 'rgba(37, 55, 70, 0.02)',
    border: '2px solid #E4E7E9',
    borderRadius: 4,
    padding: '8px 16px',
    color: '#253746',
  },
  header: {
    fontWeight: 600,
    marginBottom: 8,
  },
  title: {
    fontSize: 11,
    fontWeight: 'bold',
    color: '#596772',
  },
  error: {
    color: COLORS.COMMON.RED,
    fontSize: 12,
  },
});

const BootstrapInput = withStyles({
  root: {
    width: '100%',
  },
  disabled: {
    color: '#9FA7AE !important',
    background: '#F6F7F8 !important',
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: '#ffffff',
    border: '2px solid #D3D7DA',
    fontSize: 14,
    color: '#253746',
    padding: '5px 26px 5px 8px',
  },
})(InputBase);

export const InputUI = withStyles({
  root: {
    fontSize: 14,
    '& fieldset': {
      borderColor: '#C2C7CB',
      borderWidth: 2,
    },
    '&.Mui-focused fieldset': {
      borderColor: '#485764!important',
    },
  },
  input: {
    paddingTop: 8,
    paddingBottom: 8,
  },
})(OutlinedInput);

interface ModifyKeyWordsProps {
  currency: string;
  disabled?: boolean;
  noMatchType?: boolean;
  currencyLimit?: any;
  onSubmit: any;
}

export const ModifyKeyWords = (props: ModifyKeyWordsProps) => {
  const {
    currency,
    disabled = false,
    noMatchType = false,
    currencyLimit = CURRENCY_LIMITATION,
    onSubmit,
  } = props;

  const classes = useStyle();

  let inititalPrice = 500;
  if (currencyLimit[currency]) {
    inititalPrice = currencyLimit[currency].EXACT_MATCH.MIN;
  }

  const initialValues = {
    biddingPrice: inititalPrice,
    matchType: MATCH_TYPE.EXACT_MATCH,
  };

  const getMatchTypeValidation = () => {
    let broadMatch = { MIN: 1, MAX: 10 };
    let exactMatch = { MIN: 1, MAX: 10 };
    if (currencyLimit[currency]) {
      broadMatch = currencyLimit[currency].BROAD_MATCH;
      exactMatch = currencyLimit[currency].EXACT_MATCH;
    }

    return Yup.object().shape({
      biddingPrice: Yup.number()
        .required('Please enter price')
        .when('matchType', {
          is: MATCH_TYPE.BROAD_MATCH,
          then: Yup.number().min(
            broadMatch.MIN,
            `Bid price cannot be lower than ${broadMatch.MIN}`,
          ),
          otherwise: Yup.number().min(
            exactMatch.MIN,
            `Bid price cannot be lower than ${exactMatch.MIN}`,
          ),
        }),
    });
  };

  const getNoMatchTypeValidation = () => {
    let min = 1;
    let max = 1000000;

    if (NO_MATCH_TYPE_LIMITATION[currency]) {
      min = NO_MATCH_TYPE_LIMITATION[currency].MIN;
      max = NO_MATCH_TYPE_LIMITATION[currency].MAX;
    }

    return Yup.object().shape({
      biddingPrice: Yup.number()
        .required('Please enter price')
        .min(min, `Bid price cannot be lower than ${min}`)
        .max(max, `Bid price cannot be higher than ${max}`),
    });
  };

  const validationSchema = React.useMemo(() => {
    if (noMatchType) {
      return getNoMatchTypeValidation();
    } else {
      return getMatchTypeValidation();
    }
  }, [currency]);

  return (
    <div className={classes.root}>
      <Formik
        validationSchema={validationSchema}
        initialValues={initialValues}
        onSubmit={onSubmit}
        validateOnBlur={true}
        enableReinitialize={true}
      >
        {({ errors, handleSubmit }: any) => {
          return (
            <Form onSubmit={handleSubmit}>
              <Box>
                <Typography
                  variant="body1"
                  className={classes.header}
                >
                  Modify keywords
                </Typography>
                <Box>
                  <Grid container spacing={1}>
                    <Grid item xs={noMatchType ? 12 : 6}>
                      <Typography
                        variant="subtitle1"
                        className={classes.title}
                      >
                        Bidding price
                      </Typography>
                      <Field name="biddingPrice">
                        {({ field }: any) => {
                          return (
                            <InputUI
                              id="match-type"
                              type="number"
                              fullWidth
                              disabled={disabled}
                              {...field}
                              startAdornment={
                                <InputAdornment position="start">
                                  <CurrencyCode currency={currency} />
                                </InputAdornment>
                              }
                              error={!!errors.biddingPrice}
                              autoComplete="off"
                            ></InputUI>
                          );
                        }}
                      </Field>
                      {!disabled && (
                        <ErrorMessage
                          component={FormHelperText}
                          name="biddingPrice"
                          className={classes.error}
                        />
                      )}
                    </Grid>
                    {!noMatchType && (
                      <Grid item xs={6}>
                        <Typography
                          variant="subtitle1"
                          className={classes.title}
                        >
                          Match type
                        </Typography>
                        <Field name="matchType">
                          {({ field }: any) => {
                            return (
                              <Select
                                id="demo-customized-select-native"
                                disabled={disabled}
                                {...field}
                                input={<BootstrapInput />}
                              >
                                <MenuItem
                                  value={MATCH_TYPE.EXACT_MATCH}
                                >
                                  Exact Match
                                </MenuItem>
                                <MenuItem
                                  value={MATCH_TYPE.BROAD_MATCH}
                                >
                                  Broad Match
                                </MenuItem>
                              </Select>
                            );
                          }}
                        </Field>
                      </Grid>
                    )}
                  </Grid>
                </Box>
                <Box pt={1}>
                  <ButtonUI
                    disabled={disabled}
                    label="Apply"
                    size="small"
                    colorButton={COLORS.COMMON.GRAY}
                    variant="contained"
                    type={'submit'}
                  />
                </Box>
              </Box>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};
