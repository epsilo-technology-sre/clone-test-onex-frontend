import React, { useState } from 'react'
import { Paper, Button } from '@material-ui/core'
import { DrawerRuleProduct } from './drawer-rule-product';

export default {
  title: 'Shopee/Drawer rule product'
}

const options = [
  {
    rule_id: 1,
    "rule_name": "Rule name 0",
    "timeline": "11/9/2020 - 11/10/2020",
    "type_of_performance": "Pause product",
    "type_of_performance_text": "performance - period - operator - value"
  },
  {
    rule_id: 2,
    "rule_name": "Rule name 1",
    "timeline": "11/9/2020 - 11/11/2020",
    "type_of_performance": "Resume product",
    "type_of_performance_text": "performance - period - operator - value"
  },
  {
    rule_id: 3,
    "rule_name": "Rule name 2",
    "timeline": "11/9/2020 - 11/12/2020",
    "type_of_performance": "Resume product",
    "type_of_performance_text": "performance - period - operator - value"
  },
  {
    rule_id: 4,
    "rule_name": "Rule name 3",
    "timeline": "11/9/2020 - 11/13/2020",
    "type_of_performance": "Resume product",
    "type_of_performance_text": "performance - period - operator - value"
  },
  {
    rule_id: 5,
    "rule_name": "Rule name 4",
    "timeline": "11/9/2020 - 11/14/2020",
    "type_of_performance": "Resume product",
    "type_of_performance_text": "performance - period - operator - value"
  }
]

export const main = () => {
  const [open, setOpen] = useState(true)
  return (
    <Paper>
      <Button onClick={() => setOpen(true)}>Open</Button>
      <DrawerRuleProduct
        rules={options}
        open={open}
        onClose={(e) => setOpen(e)}
      />
    </Paper>
  )
}
