import {
  Box,
  Grid,
  IconButton,
  makeStyles,
  SwipeableDrawer,
} from '@material-ui/core';
import SvgIcon from '@material-ui/core/SvgIcon';
import CloseIcon from '@material-ui/icons/Close';
import DeleteIcon from '@material-ui/icons/Delete';
import React from 'react';
import styled from 'styled-components';
import { ButtonUI } from '../button';

export interface DrawerRuleProductProps {
  rules: any;
  title?: any;
  code: string;
  allowUpdate?: boolean;
  onChange?: any;
  onClose?: any;
  open: boolean;
  onOpenAddRule?: any;
  onOpenEditRule?: any;
  onDeleteRule?: any;
}

export const DrawerRuleProduct = (props: DrawerRuleProductProps) => {
  const {
    rules,
    open,
    allowUpdate = true,
    onClose,
    onOpenAddRule,
    onOpenEditRule,
    onDeleteRule,
  } = props;
  const classes = useStyles();
  return (
    <SwipeableDrawer anchor="right" open={open} onClose={onClose}>
      <ModalContainer style={{ width: '280px' }}>
        <Grid container justify="space-between" alignItems="center">
          <Grid item xs={6}>
            <ProductName>{props.title}</ProductName>
            <ProductCode>{props.code}</ProductCode>
          </Grid>
          <Grid item>
            <IconButton
              aria-label="close"
              // className={classes.closeButton}
              onClick={() => onClose(!open)}
            >
              <CloseIcon />
            </IconButton>
          </Grid>
        </Grid>
        {allowUpdate && (
          <Grid container justify="flex-end">
            <ButtonUI
              className={classes.button}
              label="Add rule"
              onClick={onOpenAddRule}
            />
          </Grid>
        )}
        <Box pt={1} className="rule-list">
          {(rules || []).map((rule: any) => (
            <Wrapper key={rule.id} className="rule">
              <RuleName className="rule-name">
                {rule.ruleName}
              </RuleName>
              <RuleTimeline className="time-line">
                {rule.timeline}
              </RuleTimeline>
              <RuleStatus className="rule-status">
                {rule.status}
              </RuleStatus>
              {[].concat(rule.conditionStr).map((str, index) => (
                <RuleStatusName
                  key={index}
                  className="rule-status-name"
                >
                  {str}
                </RuleStatusName>
              ))}
              {allowUpdate && (
                <Grid container justify="flex-end">
                  <ButtonUI
                    className={classes.buttonAction}
                    colorButton="#F6F7F8"
                    label="Modify"
                    onClick={() => onOpenEditRule(rule)}
                  />
                  <ButtonUI
                    className={classes.buttonIcon}
                    colorButton="#F6F7F8"
                    label={
                      <SvgIcon fontSize="small">
                        <DeleteIcon />
                      </SvgIcon>
                    }
                    onClick={() => onDeleteRule(rule)}
                  />
                </Grid>
              )}
            </Wrapper>
          ))}
        </Box>
      </ModalContainer>
    </SwipeableDrawer>
  );
};

const useStyles = makeStyles(() => ({
  button: {
    marginBottom: 8,
    marginTop: 8,
    marginLeft: 8,
    fontSize: 14,
    lineHeight: '20px',
  },
  buttonAction: {
    marginTop: 20,
    marginLeft: 8,
    fontWeight: 500,
    fontSize: 12,
    lineHeight: '16px',
    color: '#253746',
  },
  buttonIcon: {
    marginTop: 20,
    marginLeft: 8,
    fontWeight: 500,
    fontSize: 12,
    lineHeight: '16px',
    color: '#253746',
    padding: 6,
    width: 'auto',
    minWidth: 'initial',
  },
}));

const ModalContainer = styled.div`
  padding: 16px;
`;

const Wrapper = styled.div`
  box-shadow: 0 1px 1px rgba(37, 55, 70, 0.25),
    0 0 1px rgba(37, 55, 70, 0.31);
  border-radius: 4px;
  margin-bottom: 16px;
  padding: 16px;
`;

const ProductName = styled.span`
  margin-bottom: 4px;
  font-weight: 600;
  font-size: 14px;
  line-height: 16px;
  color: #253746;
  display: block;
`;

const ProductCode = styled.span`
  font-size: 12px;
  line-height: 16px;
  color: #596772;
  display: block;
`;

const RuleName = styled.span`
  color: #253746;
  font-weight: 600;
  font-size: 14px;
  line-height: 16px;
  margin-bottom: 8px;
`;

const RuleTimeline = styled.span`
  font-size: 12px;
  line-height: 16px;
  color: #596772;
  margin-bottom: 16px;
  display: block;
`;

const RuleStatus = styled.span`
  background: #f6f7f8;
  border-radius: 2px 2px 0 0;
  padding: 4px 8px;
  font-weight: bold;
  font-size: 11px;
  line-height: 16px;
  color: #596772;
`;

const RuleStatusName = styled.span`
  background: #f6f7f8;
  border-radius: 0 2px 2px 2px;
  padding: 8px;
  font-size: 12px;
  line-height: 16px;
  color: #253746;
  display: block;
`;
