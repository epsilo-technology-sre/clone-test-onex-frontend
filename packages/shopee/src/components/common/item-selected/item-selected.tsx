import React from 'react';
import { Box, Grid, Typography, makeStyles } from '@material-ui/core';
import { CurrencyCode } from '../../common/currency-code';
import CancelIcon from '@material-ui/icons/Cancel';
import clsx from "clsx";

const useStyle = makeStyles({
  root: {
    background: '#F6F7F8',
    color: '#253746',
    padding: '16px 16px 40px',
    position: 'relative',
  },
  productWrapper: {
    height: 790,
    overflow: 'auto',
  },
  product: {
    position: 'relative',
    background: '#ffffff',
    fontSize: 12,
    padding: 8,
    marginBottom: 4,
    cursor: 'pointer',
    border: '2px solid #fff',
    borderRadius: 4,
    '& .line2': {
      color: '#596772',
      fontSize: 10,
    },
    '& .remove': {
      display: 'none',
      position: 'absolute',
      top: 2,
      right: 2,
      color: '#000000',
      fontSize: 20,
      cursor: 'pointer',
    },
    '&:hover .remove': {
      display: 'block',
    },
  },
  active: {
    borderColor: '#485764',
  },
  productName: {
    width: '100%',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
  keyword: {
    fontSize: 10,
    lineHeight: '12px',
    color: '#ED5C10',
  },
});

export interface ItemSlectedProps {
  name: string;
  type: string;
  code: string;
  selected?: boolean;
  keyword?: string;
  budget?: number;
  currency: string;
  onClick?: any;
  remove?: boolean;
}

export const ItemSelected = (props: ItemSlectedProps) => {
  const { name, type, budget, currency, code, keyword, onClick, selected, remove } = props;
  const classes = useStyle();
  const getBudget = () => {
    const typeBudget: any = {
      totalBudget: 'Total budget',
      dailyBudget: 'Daily budget',
      exactMatch: 'Exact match',
      broadMatch: 'Broad match',
    }
    if (budget && budget > 0) {
      return (
        <>
          <Box>
            <CurrencyCode currency={currency} />
            {budget}
          </Box>
          <Typography variant="body2" align="right" className="line2">
            {typeBudget[type]}
          </Typography>
        </>
      );
    }

    return (
      <Typography variant="body2" align="right" style={{ fontSize: 10 }}>
        No limit
      </Typography>
    );
  };

  return (
    <Box className={clsx(classes.product, { [classes.active]: selected })} onClick={() => onClick(code)}>
      <Grid container justify="space-between">
        <Grid item xs={6}>
          <Typography
            variant="body2"
            className={classes.productName}
            title={name}
          >
            {name}
          </Typography>
          <Typography variant="subtitle2" className="line2">
            {code}
          </Typography>
          {keyword && <Typography variant="subtitle2" className={classes.keyword}>
            {keyword}/200 Keyword
          </Typography>}
        </Grid>
        <Grid item xs={6}>
          <Box style={{ textAlign: 'right' }}>
            {getBudget()}
          </Box>
        </Grid>
      </Grid>
      {remove && <CancelIcon className="remove" />}
    </Box>
  );
};
