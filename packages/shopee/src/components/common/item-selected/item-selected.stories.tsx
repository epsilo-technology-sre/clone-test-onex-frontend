import React, { useState } from 'react';
import { Grid } from '@material-ui/core';
import { ProductBucket } from '../product-bucket';
import { SelectedKeyword } from '../selected-keyword';
import { ModifyKeyWords } from '../modify-keywords';
export default {
  title: 'Shopee/Item Selected',
};

const type = ['totalBudget', 'dailyBudget', 'exactMatch', 'broadMatch'];

const data = Array.from({ length: 5 }, (_, key) => ({ 
  id: key,
  name: 'product name' + key,
  type: type[Math.floor(Math.random() * 4 + 1)],
  currency: "VND",
  budget: 3000,
  code: 'SKU_PRODUCT_' + key + Math.floor(Math.random() * 4 + 1),
  totalKeyword: key + Math.floor(Math.random() * 4 + 1),
}));

const dataKeyword = Array.from({ length: 5 }, (_, key) => ({ 
  id: key,
  name: 'product name' + key,
  type: type[Math.floor(Math.random() * 4 + 1)],
  currency: "VND",
  budget: 3000,
  code: 'SKU_PRODUCT_' + key + Math.floor(Math.random() * 4 + 1),
}));

export const main = () => {
  const [keywords, setKeywords] = useState(dataKeyword)

  return (
    <>
      <Grid container spacing={1}>
        <Grid item xs={3}>
          <ProductBucket listItem={data} onChange={e => console.log('change item product', e)} />
        </Grid>
        <Grid item xs={3}>
          <SelectedKeyword listItem={keywords} onChange={e => {
            const newKeywords = keywords.filter((item: any) => item.code !== e);
            setKeywords(newKeywords)
          }} />
        </Grid>
        <Grid item xs={6}>
          <ModifyKeyWords onSubmit={e => console.log(e, 'submit keyword')} />
        </Grid>
      </Grid>
    </>
  );
};
