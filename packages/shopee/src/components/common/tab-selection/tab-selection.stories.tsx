import React, { useState } from 'react'
import { TabSelectionUI, TabPanelUI } from './tab-selection'
import { TABS } from '@ep/shopee/src/constants'

export default {
  title: 'Shopee/TabSelection'
}


export const TabSelection = () => {
  const [selectedTab, setSelectedTab] = React.useState(TABS.CAMPAIGN)
  const tabs = [
    { type: TABS.CAMPAIGN, text: 'Campaigns' },
    { type: TABS.PRODUCT, text: 'Products' },
    { type: TABS.KEYWORD, text: 'Keywords' },
  ]

  const handleChangeTab = (tab: any) => {
    setSelectedTab(tab);
  };
  return (
    <div>
      <TabSelectionUI 
        tabs={tabs} 
        selectedTab={selectedTab} 
        showIcon={true}
        onChangeTab={handleChangeTab
      }></TabSelectionUI>
      <TabPanelUI value={selectedTab} index={tabs[0].type}>
        Tab 1 content
      </TabPanelUI>
      <TabPanelUI value={selectedTab} index={tabs[1].type}>
        Tab 2 content
      </TabPanelUI>
      <TabPanelUI value={selectedTab} index={tabs[2].type}>
        Tab 3 content
      </TabPanelUI>
    </div>
  )
}
