import React from 'react';
import { connect } from 'react-redux';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { COLORS, TABS } from '@ep/shopee/src/constants';
import CampaignIcon from '@ep/shopee/src/images/speaker.svg';
import ProductIcon from '@ep/shopee/src/images/product.svg';
import KeywordIcon from '@ep/shopee/src/images/key.svg';
import CloseIcon from '@ep/shopee/src/images/white-close.svg';
import {
  UPDATE_SELECTED_CAMPAIGNS,
  UPDATE_SELECTED_PRODUCTS,
  UPDATE_SELECTED_KEYWORDS,
} from '../../../redux/actions';

const useStyles = makeStyles({
  root: {
    borderBottom: '1px solid #E4E7E9',
  },
  tab: {
    boxSizing: 'border-box',
    height: '100%',
    marginLeft: 5,
    marginBottom: -1,
    padding: '8px 16px',
    background: '#F6F7F8',
    border: '1px solid #E4E7E9',
    // borderBottom: 'none',
    cursor: 'pointer',
    textTransform: 'capitalize',
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    minWidth: 300,
    alignSelf: 'flex-end',
  },
  selected: {
    background: '#ffffff',
    textAlign: 'center',
    borderBottom: '1px solid #fff',
  },
  tabItem: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 4,
    '& img': {
      marginRight: 8,
    },
  },
  tag: {
    background: COLORS.COMMON.ORANGE,
    color: '#ffffff',
    padding: '2px 10px',
    borderRadius: 60,
    fontSize: 14,
    display: 'flex',
    alignItems: 'center',
  },
});

interface TabPanelProps {
  children?: React.ReactNode;
  dir?: string;
  index: any;
  value: any;
}

export const TabPanelUI = (props: TabPanelProps) => {
  const { children, value, index, ...other } = props;

  return (
    <div {...other}>
      {value === index && (
        <Box>
          <span>{children}</span>
        </Box>
      )}
    </div>
  );
};

export const TabSelectionUI = (props: any) => {
  const classes = useStyles();
  const {
    tabs,
    selectedTab,
    showIcon,
    onChangeTab,
    selectedCampaigns,
    selectedProducts,
    selectedKeywords,
    updateSelectedCampaigns,
    updateSelectedProducts,
    updateSelectedKeywords,
  } = props;

  const getIcon = (tab: any) => {
    let src;
    switch (tab.type) {
      case TABS.CAMPAIGN:
        src = CampaignIcon;
        break;
      case TABS.PRODUCT:
        src = ProductIcon;
        break;
      case TABS.KEYWORD:
        src = KeywordIcon;
        break;
      default:
        src = '';
        break;
    }
    return (
      <>
        <img src={src} height={12} width={12} alt="close" />
      </>
    );
  };

  const getTabContent = (tab: any) => {
    let selections;
    let handleReset;
    let iconSrc;

    switch (tab.type) {
      case TABS.CAMPAIGN:
        iconSrc = CampaignIcon;
        selections = selectedCampaigns;
        handleReset = () => {
          updateSelectedCampaigns([]);
        };
        break;
      case TABS.PRODUCT:
        iconSrc = ProductIcon;
        selections = selectedProducts;
        handleReset = () => {
          updateSelectedProducts([]);
        };
        break;
      case TABS.KEYWORD:
        iconSrc = KeywordIcon;
        selections = selectedKeywords;
        handleReset = () => {
          updateSelectedKeywords([]);
        };
        break;
      default:
        selections = null;
        handleReset = null;
        break;
    }

    return (
      <>
        <Box>
          {showIcon && getIcon(tab)}
          <Typography component="span" variant="body2">
            {tab.text}
          </Typography>
        </Box>
        {selections && selections.length > 0 && (
          <Box className={classes.tag}>
            <Box component="span" mr={1}>
              {selections.length} selected
            </Box>
            <img
              src={CloseIcon}
              height={10}
              width={10}
              onClick={handleReset}
              alt="close"
            />
          </Box>
        )}
      </>
    );
  };

  return (
    <Grid
      container
      justify="flex-start"
      alignItems="center"
      className={classes.root}
    >
      {tabs.map((item: any, index: number) => (
        <Grid
          item
          key={index}
          className={`${classes.tab} ${
            item.type === selectedTab ? classes.selected : ''
          }`}
          onClick={() => onChangeTab(item.type)}
        >
          <Box className={classes.tabItem} key={index}>
            {getTabContent(item)}
          </Box>
        </Grid>
      ))}
    </Grid>
  );
};

const mapStateToProps = ({
  selectedCampaigns,
  selectedProducts,
  selectedKeywords,
}: any) => ({
  selectedCampaigns,
  selectedProducts,
  selectedKeywords,
});

const mapDispatchToProps = (dispatch: any) => ({
  updateSelectedCampaigns: (payload: any) =>
    dispatch(UPDATE_SELECTED_CAMPAIGNS(payload)),
  updateSelectedProducts: (payload: any) =>
    dispatch(UPDATE_SELECTED_PRODUCTS(payload)),
  updateSelectedKeywords: (payload: any) =>
    dispatch(UPDATE_SELECTED_KEYWORDS(payload)),
});

export const TabSelectionContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(TabSelectionUI);
