import React, { useRef } from 'react';
import { ButtonUI, ButtonLinkUI } from '../button';
import { COLORS } from '@ep/shopee/src/constants';
import {
  Dialog,
  IconButton,
  Typography,
  Grid,
  TextField,
  FormHelperText
} from '@material-ui/core';
import { createStyles, Theme, withStyles, makeStyles, WithStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import CloseIcon from '@material-ui/icons/Close';
import { Form, Formik, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';

export interface ModalAddRuleProps {
  open: boolean,
  onClose?: any,
  onSubmit: any,
}

export const validationSchema = Yup.object().shape({
  ruleName: Yup.string().required('Please enter rule name'),
});

export function ModalAddRuleName(props: ModalAddRuleProps) {
  const { open, onClose, onSubmit } = props;

  const formRef = useRef();
  const classes = useStyles();

  const handleClose = () => {
    onClose(false);
  };

  return (
    <div>
      <Dialog
        open={open}
        classes={{ paper: classes.paper }}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose} className={classes.dialogTitle}>
          Add rule
        </DialogTitle>
        <DialogContent>
            <Formik
              validationSchema={validationSchema}
              initialValues={{ ruleName: '' }}
              onSubmit={(values: any) => onSubmit(values)}
              validateOnBlur={true}
              innerRef={formRef}
            >
              {(formik: any) => {
                return (
                  <Form
                    // className={classes.wrapForm}
                  >
                    <Typography
                      variant="body2"
                      className={classes.labelInput}
                    >
                      Rule name
                    </Typography>
                    <Field name="ruleName">
                      {({ field }: any) => {
                        return (
                          <TextField
                            className={classes.input}
                            fullWidth
                            id="outlined-size-small"
                            autoComplete="off"
                            placeholder="Enter rule name"
                            variant="outlined"
                            size="small"
                            {...field}
                          />
                        );
                      }}
                    </Field>
                    <ErrorMessage
                      component={FormHelperText}
                      name="ruleName"
                      className={classes.error}
                    />
                  </Form>
                );
              }}
            </Formik>
        </DialogContent>
        <DialogActions>
          <Grid container justify="space-between">
            <ButtonLinkUI
              label="Cancel"
              size="small"
              colortext="#f16145"
              onClick={handleClose}
            />
            <ButtonUI
              label="Save"
              size="small"
              variant="contained"
              onClick={() => {
                if (formRef.current) {
                  formRef.current.handleSubmit();
                }
              }}
            />
          </Grid>
        </DialogActions>
      </Dialog>
    </div>
  );
};

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });

const useStyles = makeStyles(() => ({
  paper: { width: 480, maxHeight: 528 },
  wrapForm: { maxHeight: 208, overflow: 'auto' },
  wrapConditions: { padding: 16, border: '2px solid #e4e7e9', borderRadius: 4, marginTop: 8 },
  labelInput: {
    marginTop: 8,
    marginBottom: 4,
    fontWeight: 'bold',
    fontSize: 11,
    lineHeight: '16px',
  },
  error: {
    color: COLORS.COMMON.RED,
    fontSize: 12,
  },
  input: {
    '& .MuiOutlinedInput-notchedOutline': {
      border: '2px solid #e4e7e9',
    },
  },
  dialogTitle: {
    padding: '16px 16px 0 16px',
  }
}));

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose?: () => void,
  classes: any;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);
