import React, { useState } from 'react'
import { Button, Paper } from '@material-ui/core'
import { ModalAddRuleName } from './modal-add-rule-name';

export default {
  title: 'Shopee/Modal add rule name'
}

export const RuleName = () => {
  const [open, setOpen] = useState(true)

  return (
    <Paper>
      <Button onClick={() => setOpen(true)}>Show modal</Button>
      <ModalAddRuleName
        open={open}
        onSubmit={(value: any) => console.log('valuev', value)}
        onClose={(e: any) => setOpen(e)}
      />
    </Paper>
  )
}
