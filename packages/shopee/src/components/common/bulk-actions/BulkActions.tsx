import React from 'react';
import { Grid } from '@material-ui/core';
import styled from 'styled-components';
import { COLORS } from "@ep/shopee/src/constants";

export interface BulkActionsUIProps {
  textSelected: string,
  buttons: JSX.Element[],
}

const WrapBulkAction = styled.div`
  background: #515F6B;
  padding: 8px;
  border-radius: 4px;
  color: ${COLORS.COMMON.GRAY};
`;

export function BulkActionsUI(props: BulkActionsUIProps) {
  const { textSelected, buttons = [] } = props;

  return (
    <WrapBulkAction>
      <Grid container spacing={1} alignItems="center">
        <Grid item>{textSelected}</Grid>
        {buttons.map((button: JSX.Element, index: number) => <Grid key={index} item>{button}</Grid>)}
      </Grid>
    </WrapBulkAction>
  );
}
