import React from 'react';
import { BulkActionsUI } from './BulkActions';
import { ButtonUI } from '../button';

export default {
  title: 'Shopee/Bulk Actions',
};

export const Primary = () => {

  const onClose = () => {
    console.log('onClose');
  }

  const onSubmit = () => {
    console.log('onSubmit');
  }

  const buttons = [
    <ButtonUI onClick={onClose} label="Modify budget" size="small" colorButton="#F6F7F8" />,
    <ButtonUI onClick={onClose} label="Activate" size="small" colorButton="#F6F7F8" />,
    <ButtonUI colortext="#D4290D" onClick={onSubmit} label="Deactivate" size="small" colorButton="#F6F7F8" />,
  ];

  return <BulkActionsUI textSelected="6 campaigns selected" buttons={buttons} />;
};
