import React from 'react';
import styled from 'styled-components';
import { COLORS } from "@ep/shopee/src/constants";

export const WrapError = styled.span`
  font-size: 0.675rem;
  color: ${COLORS.COMMON.RED};
`;

export const WrapErrorPassword = styled.span`
  font-size: 0.675rem;
  color: ${COLORS.COMMON.RED};
  margin: 9px 14px 0 14px;
`;

const WrapStyled = styled.span`
  svg {
    width: 1.25rem;
    vertical-align: middle;
    height: 1.25rem;
    position: relative;
    top: -1px;
  }
`;

export interface WrapProps {
  children?: any;
}

export const WrapSVG = ({ children, ...args }: WrapProps) => (
  <WrapStyled {...args}>{children}</WrapStyled>
);

export const WrapDashboardPageTitle = styled.div`
  margin-bottom: 1rem;
`;

export const WrapWidget = styled.div`
  margin: 1.5rem 0;
`;

export const WrapTooptipNumber = styled.span`
  background-color: ${COLORS.COMMON.MAIN_OPA_8};
  border-radius: 20px;
  vertical-align: super;
  padding: 0.3rem 0.6rem;
  font-size: 12px;
  position: relative;
  top: -3px;
  cursor: pointer;
`;
