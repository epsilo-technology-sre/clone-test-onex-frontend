import React from 'react';
import styled from 'styled-components';

const WrapStyled = styled.div`
  padding-top: 1.5rem;
`;

export interface WrapProps {
  children?: any;
}

export const WrapStory = ({ children, ...args }: WrapProps) => (
  <WrapStyled {...args}>{children}</WrapStyled>
);
