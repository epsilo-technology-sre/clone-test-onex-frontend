import React from 'react'
import { StatusPopoverUI } from './status-popover'

export default {
  title: 'Shopee/StatusPopover'
}

export const Popover = () => {
  const status = {
    name: 'running',
    children: [
      { enable: false, text: 'Budget' },
      { enable: true, text: 'Campaign state' },
      { enable: true, text: 'Account balance' },
      { enable: true, text: 'Promoted product state' },
    ]
  }
  return (
    <StatusPopoverUI status={status}>
      <button>Status</button>
    </StatusPopoverUI>
  )
}