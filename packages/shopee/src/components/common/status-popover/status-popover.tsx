import React from 'react'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Box from '@material-ui/core/Box'
import Popover from '@material-ui/core/Popover';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import EnableIcon from '@ep/shopee/src/images/enabled-status.svg'
import DisableIcon from '@ep/shopee/src/images/disabled-status.svg'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      marginLeft: 5,
    },
    popover: {
      pointerEvents: 'none',
    },
    wrapper: {
      background: '#253746',
      borderRadius: 4,
      padding: '4px 8px',
      color: '#ffffff',
      fontSize: 12,
      textTransform: 'capitalize',
    },
    statusItem: {
      display: 'flex',
      alignItems: 'center',
      padding: 4,
      '& img': {
        marginRight: 8,
      }
    },
    enableItem: {
      fontSize: 12,
    },
    disableItem: {
      color: '#9FA7AE',
      fontSize: 12,
    }
  }),
);

export const StatusPopoverUI = (props: any) => {
  const classes = useStyles();
  const { status } = props;
  const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);

  const handlePopoverOpen = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    setAnchorEl(event.currentTarget);
  };

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  
  let content = status.type;
  if (status.children && status.children.length > 0) {
    content = status.children.map((child: any, index: number) => (
      <Box className={classes.statusItem} key={index}>
        <img src={child.enable ? EnableIcon : DisableIcon} height={12} width={12}></img>
        <Typography 
          component="span" 
          variant="body2" 
          className={child.enable ? classes.enableItem : classes.disableItem}
        >{child.text}</Typography>
      </Box>
    ))
  }

  return (
    <>
      <Box component="span" onMouseEnter={handlePopoverOpen}
        onMouseLeave={handlePopoverClose}>
        {props.children}
      </Box>
      <Popover
        id="mouse-over-popover"
        open={open}
        className={classes.popover}
        anchorEl={anchorEl}
        classes={{
          root: classes.root
        }}
        anchorOrigin={{
          vertical: 'center',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'center',
          horizontal: 'left',
        }}
        onClose={handlePopoverClose}
        disableRestoreFocus
      >
        <Box className={classes.wrapper}>
          {content}
        </Box>
      </Popover>
    </>
  )
}

StatusPopoverUI.defaultProps = {
  status: {
    type: 'closed',
    children: null
  }
}
