import { Box, makeStyles, Popover } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import PopupState, {
  bindPopover,
  bindTrigger,
} from 'material-ui-popup-state';
import React from 'react';
import { ButtonUI } from '../button';
import { TableUINoCheckbox } from '../table';
import { initColumns } from './dropdown-condition-rule-columns';

export interface DropdownConditionProps {
  condition?: any;
}

export const DropdownConditionRule = (
  props: DropdownConditionProps,
) => {
  console.log('Function: DropdownConditionRule', props);
  const { condition = [] } = props;
  const classes = useStyles();

  return (
    <PopupState variant="popover" popupId="demo-popup-popover">
      {(popupState) => (
        <div>
          <ButtonUI
            size="small"
            iconRight={<ExpandMoreIcon />}
            colorButton={'#F6F7F8'}
            label={`${condition.length} condition`}
            color="primary"
            {...bindTrigger(popupState)}
          />
          <Popover
            {...bindPopover(popupState)}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'right',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
          >
            <Box className={classes.popover}>
              <TableUINoCheckbox
                columns={initColumns()}
                noData={'No data'}
                rows={condition}
                onSort={() => {}}
                className={'tableConditionRule'}
              />
            </Box>
          </Popover>
        </div>
      )}
    </PopupState>
  );
};

const useStyles = makeStyles({
  accordion: {
    boxShadow: 'none',
    '&:before': {
      display: 'none',
    },
    '&.Mui-expanded': {
      margin: 0,
    },
    '& .Mui-expanded': {
      marginTop: 0,
      marginBottom: 0,
      minHeight: 'inherit',
    },
    '& .MuiIconButton-edgeEnd': {
      marginRight: -12,
    },
    '& .MuiAccordionDetails-root': {
      padding: 0,
    },
  },
  paper: {
    padding: 8,
  },
  popover: {
    width: 648,
    padding: '8px 0 8px 8px',
  },
});
