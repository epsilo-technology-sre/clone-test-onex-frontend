import React from 'react';
import { DropdownConditionRule } from './dropdown-condition-rule';

export default {
  title: 'Shopee/Dropdown condition rule',
};

export const main = () => {
  return <DropdownConditionRule />;
};
