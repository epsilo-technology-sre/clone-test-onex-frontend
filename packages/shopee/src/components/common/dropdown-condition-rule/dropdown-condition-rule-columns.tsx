export const initColumns = () => {
  return [
    {
      Header: 'Type of performance',
      accessor: 'typeOfPerformance',
      width: 200,
      disableSortBy: true,
    },
    {
      Header: 'Period',
      id: 'period',
      accessor: 'period',
      disableSortBy: true,
      width: 104,
    },
    {
      Header: 'Operator',
      id: 'operator',
      accessor: 'operator',
      disableSortBy: true,
      width: 104,
    },
    {
      Header: 'Value',
      id: 'value',
      accessor: 'value',
      disableSortBy: true,
      width: 104,
    },
    {
      Header: 'Checkpoint',
      id: 'checkpoint',
      accessor: 'checkpoint',
      disableSortBy: true,
      width: 120,
    },
  ];
};
