import React, { useEffect, useState } from 'react';
import {
  DayPickerSingleDateController,
  isInclusivelyAfterDay,
} from 'react-dates';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import { makeStyles } from '@material-ui/core/styles';
import styled from 'styled-components';
import moment from 'moment';
import { nanoid } from 'nanoid';
import { isEmpty } from 'lodash';
import { isDiffMonthOrYear } from '@ep/shopee/src/utils/dateTimeUtility';

const WrapperContainer = styled.div`
  .DayPicker_transitionContainer {
    border-radius: 4px;
  }
  .DayPickerNavigation_button__default {
    border: 0;
    outline-color: rgb(241, 97, 69);
    box-shadow: none;
  }

  .DayPickerNavigation_button__default:active {
    background: rgba(241, 97, 69, 0.08);
  }

  .DayPicker_weekHeader {
    padding: 0 8px !important;
  }

  .CalendarMonth {
    padding: 0 8px !important;
  }

  .CalendarDay__default {
    width: 40px !important;
  }

  .CalendarDay__today {
    color: rgb(241, 97, 69);
  }

  .CalendarDay__selected {
    background: rgb(241, 97, 69);
    border: 1px solid #e4e7e7;
    color: white;
  }

  .CalendarDay__selected:active,
  .CalendarDay__selected:hover {
    background: rgb(241, 97, 69);
    border: 1px solid #e4e7e7;
  }

  .CalendarDay__selected_span {
    background: rgba(241, 97, 69, 0.08);
    color: rgba(0, 0, 0, 0.8);
    border: 1px solid #e4e7e7;
  }

  .CalendarDay__selected_span:active,
  .CalendarDay__selected_span:hover {
    background: rgba(241, 97, 69, 0.08);
    color: rgba(0, 0, 0, 0.8);
    border: 1px solid #e4e7e7;
  }

  .CalendarDay__hovered_span,
  .CalendarDay__hovered_span:active,
  .CalendarDay__hovered_span:hover {
    background: #e4e7e7;
    color: white;
    border: 1px solid #e4e7e7;
  }
`;

export const SingleDatePickerWrapper = (props: any) => {
  const [date, setDate] = useState<any>();
  const [key, setKey] = useState<any>(nanoid());
  const [initVisibleMonth, setInitVisibleMonth] = useState<any>();
  const [focused, setFocused] = useState<boolean>(false);
  const today = moment();

  useEffect(() => {
    setDate(props.date);
  }, [props.date]);

  useEffect(() => {
    if (
      moment.isMoment(date) &&
      date.isValid() &&
      isDiffMonthOrYear(date, initVisibleMonth)
    ) {
      console.log('set key');
      setKey(nanoid());
    }
  }, [date, initVisibleMonth]);

  const onDateChange = (date: any) => {
    setDate(date);
    props.onDateChange && props.onDateChange(date);
  };

  const onFocusChange = () => {
    setFocused(true);
  };

  const onInitialVisibleMonth = () => {
    const visibleMonthOfDay =
      !isEmpty(date) && date.isValid() ? date : today;

    setInitVisibleMonth(visibleMonthOfDay);
    console.log('MonthOfDay', visibleMonthOfDay)
    return visibleMonthOfDay;
  };

  return (
    <WrapperContainer>
      <DayPickerSingleDateController
        key={key}
        {...props}
        isOutsideRange={
          typeof props.isOutsideRange === 'function'
            ? props.isOutsideRange
            : (day: any) => !isInclusivelyAfterDay(day, today)
        }
        date={date}
        initialVisibleMonth={onInitialVisibleMonth}
        onDateChange={onDateChange}
        onFocusChange={onFocusChange}
        focused={focused}
      />
    </WrapperContainer>
  );
};
