import React from 'react';
import { SingleDatePickerUI } from './single-date-picker';
import moment from 'moment';

export default {
  title: 'Shopee/SingleDatePicker',
};

export const SingleDatePicker = (props: any) => {
  const date = moment().add(7, 'days');
  return (
    <>
      <SingleDatePickerUI date={date}/>
    </>
  );
};

