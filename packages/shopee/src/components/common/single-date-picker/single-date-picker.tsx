import React, { useState, useRef, useEffect } from 'react';
import styled from 'styled-components';
import { SingleDatePickerWrapper } from './single-date-picker-wrapper';
import { useOnClickOutside } from '@ep/shopee/src/utils/hooks/useClickOutSide';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import moment from 'moment';
import OutlinedInput from '@material-ui/core/OutlinedInput'
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import { COLORS, DATE_FORMAT } from '@ep/shopee/src/constants'

const Wrapper = styled.div``;

const useStyles = makeStyles((theme) => ({
  inputGroup: {
    width: '320px !important',
    position: 'relative',
    '& input': {
      borderColor: 'rgba(51, 63, 80, 0.4)',
    },
  },
  outerDatePicker: {
    border: '1px solid rgba(51, 63, 80, 0.4)',
    borderRadius: 4,
    position: 'absolute',
    zIndex: 800,
    marginTop: 4,
  },
  input: {
    width: '100%',
    background: COLORS.COMMON.GRAY,
    fontSize: '0.875rem',
    '& input': {
      padding: '8px 14px',
    },
    '& fieldset': {
      border: 'none',
    },
  },
  calendarIcon: {
    fontSize: '1rem',
  },
}));

const PreCalendar = (props: any) => {
  return <div>{props.children}</div>;
};

export const SingleDatePickerUI = (props: any) => {
  const [date, setDate] = useState<any>();
  const [showDatePicker, setShowDatePicker] = useState(false);

  const ref = useRef(null);

  useEffect(() => {
    const newDate = moment.isMoment(props.date) && props.date.isValid()
      ? props.date 
      : moment(props.date, props.dateFormat)
    setDate(newDate);
  }, [props.date]);

  const onDateChange = (date: any) => {
    setShowDatePicker(false);
    setDate(date);
    props.onChange && props.onChange(date ? date.format(props.dateFormat) : '');
  };

  useOnClickOutside(ref, () => {
    setShowDatePicker(false);
    props.onBlur && props.onBlur();
  });

  const handleOutsideRange = (day: any) => {
    if (props.minDate) {
      // console.log('handleOutsideRange', day.format(props.dateFormat), moment(props.minDate, props.dateFormat).diff(day) > 0)
      return moment(props.minDate, props.dateFormat).diff(day) > 0;
    }
    return false;
  }

  const dateString = date ? date.format(props.dateFormat) : '';
  const classes = useStyles();
  return (
    <Wrapper>
      <div style={{position: 'relative'}}>
        <OutlinedInput
          readOnly={true}
          disabled={props.disabled}
          value={dateString}
          classes={{ root: classes.input }}
          endAdornment={<CalendarTodayIcon className={classes.calendarIcon}/>}
          onClick={() => !props.disabled && setShowDatePicker((showDatePicker) => !showDatePicker)}
        />
        {showDatePicker && (
          <div
            className={clsx(
              classes.outerDatePicker,
              'outer-date-picker',
            )}
            style={{
              top: props.placement === 'top' ? -308 : 40,
              right: 0,
            }}
            ref={ref}
          >
            {props.preCalendar && (
              <PreCalendar date={dateString}>
                {props.preCalendar}
              </PreCalendar>
            )}
            <SingleDatePickerWrapper
              {...props}
              ref={ref}
              date={date}
              noBorder
              hideKeyboardShortcutsPanel
              onDateChange={onDateChange}
              isOutsideRange={handleOutsideRange}
            />
          </div>
        )}
      </div>
      
    </Wrapper>
  );
};

SingleDatePickerUI.defaultProps = {
  dateFormat: DATE_FORMAT,
};
