import React from 'react';

export function makeLazyCell(Cell) {
  function LazyCell(props) {
    let ref = React.useRef();
    let [rendered, setRendered] = React.useState(0);

    React.useEffect(() => {
      let observer = new IntersectionObserver((entries) => {
        entries.forEach((i) => {
          if (i.isIntersecting && i.intersectionRatio >= 0) {
            window.requestAnimationFrame(() => {
              if (i.isIntersecting && i.intersectionRatio >= 0) {
                setRendered(() => {
                  observer.unobserve(ref.current);
                  observer.disconnect();
                  return 1;
                });
              }
            });
          }
        });
      });

      if (ref.current) {
        observer.observe(ref.current);
      }

      return () => {
        if (ref.current) observer.unobserve(ref.current);
        observer.disconnect();
      };
    }, []);

    if (!rendered) return <div ref={ref}></div>;
    return <Cell {...props} />;
  }
  return LazyCell;
}
