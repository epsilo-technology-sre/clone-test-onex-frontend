import { DropdownPermission } from '@ep/one/src/components/shop/dropdown-permission';
import SwitchOffDisabledIcon from '@ep/one/src/images/switch-off-disabled.svg';
import SwitchOffIcon from '@ep/one/src/images/switch-off.svg';
import SwitchOnDisabledIcon from '@ep/one/src/images/switch-on-disabled.svg';
import SwitchOnIcon from '@ep/one/src/images/switch-on.svg';
import { TooltipUI } from '@ep/shopee/src/components/common/tooltip';
import { COLORS } from '@ep/shopee/src/constants';
import OpenLinkIcon from '@ep/shopee/src/images/open-link.svg';
import RuleLogIcon from '@ep/shopee/src/images/rule-log.svg';
import {
  formatCurrency,
  formatNumber,
} from '@ep/shopee/src/utils/utils';
import { Box, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Delete';
import clsx from 'clsx';
import React from 'react';
import { Link, useHistory, useRouteMatch } from 'react-router-dom';
import { ButtonIconUI, ButtonUI } from '../../common/button';
import { CurrencyCode } from '../currency-code';
import { DropdownConditionRule } from '../dropdown-condition-rule';
import { IconStatus } from '../icon-status';
import { StatusIcon } from '../status-icon';
import { StatusPopoverUI } from '../status-popover';
import { CheckboxUI } from '../table/checkbox';

const useStyles = makeStyles({
  ellipsisText: (props?: any) => ({
    width: props.width || 240,
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textDecoration: 'none',
    color: '#253746',
  }),
  switcher: {
    display: 'flex',
    alignItems: 'center',
    padding: '8px 0',
    '& img': {
      cursor: 'pointer',
      marginRight: 8,
    },
  },
  openLink: {
    position: 'absolute',
    top: '45%',
    right: 0,
    cursor: 'pointer',
  },
  productName: {
    color: '#253746',
    fontSize: 12,
    fontWeight: 600,
    '&.clickable': {
      cursor: 'pointer',
    },
  },
  subjectLink: {
    color: '#253746',
    textDecoration: 'none',
  },
  subjectName: {
    color: '#253746',
    fontSize: 12,
    fontWeight: 600,
    width: 190,
    overflow: 'hidden !important',
    textOverflow: 'ellipsis',
    '&.clickable': {
      cursor: 'pointer',
    },
    '& > div': {
      width: 'initial',
    },
  },
  subjectCode: {
    color: '#596772',
    fontSize: 10,
  },
  positivePercent: {
    fontSize: 12,
    color: '#3AA76D',
  },
  negativePercent: {
    fontSize: 12,
    color: '#CD3249',
  },
  centerContent: {
    width: 100,
    textAlign: 'center',
  },
  firstLink: {
    color: COLORS.COMMON.ORANGE,
    '&:hover': {
      color: COLORS.COMMON.ORANGE,
    },
  },
  otherLink: {
    color: COLORS.COMMON.BLUE,
    marginLeft: 16,
    '&:hover': {
      color: COLORS.COMMON.BLUE,
    },
  },
  position: {
    fontSize: 12,
    '& .green': {
      color: COLORS.COMMON.GREEN,
    },
    '& .red': {
      color: COLORS.COMMON.RED,
    },
    '& .orange': {
      color: COLORS.COMMON.ORANGE,
    },
  },
  updatedDate: {
    color: '#253746',
    fontSize: 10,
  },
  buttonIcon: {
    padding: 6,
    width: 'auto',
    minWidth: 'initial',
  },
  shopUserName: {
    fontSize: 12,
    lineHeight: '16px',
    color: '#253746',
  },
  shopTimeline: {
    fontSize: 10,
    lineHeight: '12px',
    color: '#253746',
  },
  linkButton: {
    background: COLORS.COMMON.GRAY,
    display: 'inline-block',
    fontWeight: 500,
    fontSize: 12,
    lineHeight: '16px',
    color: '#253746',
    padding: '4px 8px',
    textDecoration: 'none',
    borderRadius: 4,
  },
});

export const SubjectLinkCell = (props: any) => {
  const classes = useStyles();
  const { value, column } = props;
  const clickable = column.onClickLabel;

  const { url } = useRouteMatch();
  const history = useHistory();
  return (
    <div>
      <TooltipUI title={value.name}>
        <div
          className={clsx(
            classes.subjectName,
            clickable && 'clickable',
          )}
        >
          <Link
            to={value.url}
            className={classes.ellipsisText}
            onClick={(e) => {
              e.preventDefault();
              e.stopPropagation();
              clickable
                ? column.onClickLabel(props.row.original)
                : null;
            }}
          >
            {value.name}
          </Link>
        </div>
      </TooltipUI>
      <div className={classes.subjectCode}>{value.code}</div>
      {value.editor}
    </div>
  );
};

export const SubjectCell = (props: any) => {
  const classes = useStyles();
  const { value, column } = props;
  const clickable = column.onClickLabel;
  return (
    <div>
      <TooltipUI title={value.name}>
        <div
          className={clsx(
            classes.subjectName,
            clickable && 'clickable',
          )}
          onClick={() =>
            clickable ? column.onClickLabel(props.row.original) : null
          }
        >
          <div className={classes.ellipsisText}>{value.name}</div>
        </div>
      </TooltipUI>
      <div className={classes.subjectCode}>{value.code}</div>
      {value.editor}
    </div>
  );
};

export const ButtonCell = (props: any) => {
  const classes = useStyles();
  const { value, column } = props;
  const handleClick = () => {
    if (value.onCellClick) {
      value.onCellClick(value);
    }
    if (column.onCellClick) {
      column.onCellClick(value);
    }
  };

  if (!value.label) {
    return null;
  }

  return (
    <ButtonUI
      className={classes.buttonIcon}
      label={value.label}
      size="small"
      colorButton="#F6F7F8"
      onClick={handleClick}
    />
  );
};

export const LinkButtonCell = (props: any) => {
  const classes = useStyles();
  const { value } = props;

  return (
    <a href={value.url || '#'} className={classes.linkButton}>
      {value.label}
    </a>
  );
};

export const LogCell = (props: any) => {
  const { value, column } = props;

  const handleClick = () => {
    if (column.onCellClick) {
      column.onCellClick(value);
    }
  };

  return (
    <ButtonIconUI
      size="small"
      colorButton={COLORS.COMMON.GRAY}
      style={{ borderRadius: 4 }}
      onClick={handleClick}
    >
      <img
        src={RuleLogIcon}
        height={26}
        width={26}
        style={{ padding: 5 }}
        title="View rule log"
        alt="View rule log"
      />
    </ButtonIconUI>
  );
};

export const ShopStatusCell = ({ value }: any) => {
  const classes = useStyles();
  return (
    <Box className={classes.centerContent}>
      <StatusPopoverUI status={value}>
        <IconStatus status={value.type} />
      </StatusPopoverUI>
    </Box>
  );
};

export const DropdownCell = ({ value }: any) => {
  return <DropdownPermission permissions={value} />;
};

export const ShopDetailsActionCell = ({ value }: any) => {
  const { product = {}, onAddKeyword, onDelete } = value;
  return (
    <Grid container spacing={2}>
      <Grid item>
        <ButtonUI
          label="Edit Permission"
          colorButton={COLORS.COMMON.GRAY}
          size="small"
          onClick={() => onAddKeyword(product)}
        />
        <ButtonIconUI
          size="small"
          colorButton={COLORS.COMMON.GRAY}
          style={{ borderRadius: 4, marginLeft: 8 }}
          onClick={() => onDelete(product)}
        >
          <DeleteIcon fontSize="small" />
        </ButtonIconUI>
      </Grid>
    </Grid>
  );
};

export const ShopStatus = ({ value }: any) => {
  const classes = useStyles();
  const { userName, timeLine } = value;
  return (
    <Box>
      <Box className={classes.shopUserName}>{userName}</Box>
      <Box className={classes.shopTimeline}>{timeLine}</Box>
    </Box>
  );
};

export const StringCell = ({ value }: any) => {
  const classes = useStyles();
  return (
    <div>
      <div className={classes.subjectCode}>{value.value}</div>
      {value.editor}
    </div>
  );
};

export const ProductCell = (props: any) => {
  const classes = useStyles();
  const { value, column } = props;
  const clickable = column.onClickLabel;
  return (
    <Box pr={2}>
      <TooltipUI title={value.name}>
        <Box
          className={clsx(
            classes.productName,
            classes.ellipsisText,
            clickable && 'clickable',
          )}
        >
          <Link
            to={value.url}
            className={classes.subjectLink}
            onClick={(e) => {
              e.preventDefault();
              e.stopPropagation();
              clickable
                ? column.onClickLabel(props.row.original)
                : null;
            }}
          >
            {value.name}
          </Link>
        </Box>
      </TooltipUI>
      <Box className={classes.subjectCode}>{value.code}</Box>
      <img
        src={OpenLinkIcon}
        height={10}
        width={10}
        className={classes.openLink}
        title="Go to channel"
        alt="Go to channel"
      />
    </Box>
  );
};

export const AdsStateCell = (props: any) => {
  const classes = useStyles();
  const { isWin, updatedDate } = props.value;

  return (
    <div>
      <div className={classes.position}>
        <span className={isWin ? 'green' : 'red'}>
          {isWin ? 'Win' : 'Lose'}
        </span>
      </div>
      <div className={classes.updatedDate}>{updatedDate}</div>
    </div>
  );
};

export const PositionCell = (props: any) => {
  const classes = useStyles();
  const { position, updatedDate } = props.value;
  const positionText = !position || position > 20 ? '>20' : position;

  let color = '';
  if (!position || position > 10) {
    color = 'red';
  } else if (position > 5) {
    color = 'orange';
  } else {
    color = 'green';
  }

  return (
    <div>
      <div className={classes.position}>
        <span className={color}>{positionText}</span>
      </div>
      <div className={classes.updatedDate}>{updatedDate}</div>
    </div>
  );
};

export const SwitchCell = (props: any) => {
  const classes = useStyles();
  const { value } = props;
  const handleClick = () => {
    if (props.column.onCellClick) {
      props.column.onCellClick(props.row.original, props.value.isOn);
    }
  };
  return (
    <Box className={clsx(classes.centerContent, classes.switcher)}>
      {value.isDisabled && (
        <img
          src={
            props.value.isOn
              ? SwitchOnDisabledIcon
              : SwitchOffDisabledIcon
          }
          height={16}
          width={30}
        />
      )}
      {!value.isDisabled && (
        <img
          src={props.value.isOn ? SwitchOnIcon : SwitchOffIcon}
          height={16}
          width={30}
          onClick={handleClick}
        />
      )}
    </Box>
  );
};

export const StatusCell = (props: any) => {
  const classes = useStyles();
  return (
    <Box className={classes.centerContent}>
      <StatusPopoverUI status={props.value}>
        <StatusIcon status={props.value.type} />
      </StatusPopoverUI>
    </Box>
  );
};

export const PercentCell = (props: any) => {
  const classes = useStyles();
  const { number, percent, currency, prefix, editor } = props.value;

  // const percentTag = () => {
  //   const number = parseFloat(percent);
  //   if (!isNaN(number)) {
  //     return (
  //       <>
  //         {percent >= 0 && (
  //           <Box
  //             className={classes.positivePercent}
  //           >{`+ ${percent}`}</Box>
  //         )}
  //         {percent < 0 && (
  //           <Box className={classes.negativePercent}>{`- ${Math.abs(
  //             percent,
  //           )}`}</Box>
  //         )}
  //       </>
  //     );
  //   }
  //   return '';
  // };

  return (
    <Box className="cellNumber">
      <Box>
        <CurrencyCode currency={currency} />
        {currency ? (
          <TooltipUI title={formatCurrency(number, currency)}>
            <span style={{ paddingLeft: 5 }}>
              {formatNumber(number)}
            </span>
          </TooltipUI>
        ) : (
          <span>{formatCurrency(number)}</span>
        )}
        {prefix}
      </Box>
      {/* {percentTag()} */}
      {editor}
    </Box>
  );
};

export const BudgetCell = (props: any) => {
  const { dailyBudget, totalBudget, currency, editor } = props.value;
  return (
    <Box>
      <Box>
        {totalBudget > 0 && <CurrencyCode currency={currency} />}
        {`${
          totalBudget > 0
            ? formatCurrency(totalBudget, currency)
            : 'No limit'
        } - Total`}
      </Box>
      {dailyBudget > 0 && (
        <Box>
          <CurrencyCode currency={currency} />
          {`${formatCurrency(dailyBudget, currency)} - Daily`}
        </Box>
      )}
      {editor}
    </Box>
  );
};

export const BudgetCellOnlyDaily = (props: any) => {
  const { dailyBudget, totalBudget, currency, editor } = props.value;
  return (
    <Box>
      {dailyBudget <= 0 && <Box>No limit - Daily</Box>}
      {dailyBudget > 0 && (
        <Box>
          <CurrencyCode currency={currency} />
          {`${formatCurrency(dailyBudget, currency)} - Daily`}
        </Box>
      )}
      {editor}
    </Box>
  );
};

export const ProductBudgetCell = (props: any) => {
  const {
    dailyBudget = 0,
    totalBudget = 0,
    currency,
    editor,
  } = props.value;

  return (
    <Box>
      <Box
        display={
          totalBudget === 0 && dailyBudget === 0
            ? 'inline-block'
            : 'none'
        }
      >
        No limit
      </Box>
      <Box
        display={
          totalBudget > 0 || dailyBudget > 0 ? 'inline-block' : 'none'
        }
      >
        <CurrencyCode currency={currency} />
        {totalBudget > 0 && (
          <>
            <span className="budget-cell" value={totalBudget}>
              {formatCurrency(totalBudget, currency)}
            </span>
            {' - Total'}
          </>
        )}
        {dailyBudget > 0 && (
          <>
            <span className="budget-cell" value={dailyBudget}>
              {formatCurrency(dailyBudget, currency)}
            </span>
            {' - Daily'}
          </>
        )}
      </Box>
      {editor}
    </Box>
  );
};

export const TimelineCell = (props: any) => {
  const { fromDate, toDate, editor } = props.value;
  return (
    <Box>
      <Box>
        {fromDate} - {toDate}
      </Box>
      {editor}
    </Box>
  );
};

export const ActionCell = (props: any) => {
  const classes = useStyles();
  const links = props.value;
  return (
    <Box>
      {links.map((item: any, index: number) => (
        <a
          key={index}
          href="#"
          className={
            index === 0 ? classes.firstLink : classes.otherLink
          }
        >
          {item.text}
        </a>
      ))}
    </Box>
  );
};

export const EllipsisCell = (props: any) => {
  const { value, column } = props;
  const classes = useStyles({ width: column.width - 20 });
  return (
    <Box pr={2}>
      <TooltipUI title={value.value}>
        <div className={classes.ellipsisText}>{value.value}</div>
      </TooltipUI>
    </Box>
  );
};

export const InStockCell = (props: any) => {
  const { inStock, updatedDate } = props.value;
  return (
    <Box>
      <Box>
        <div style={{ fontSize: 12 }}>{inStock}</div>
      </Box>
      <Box>
        <div style={{ fontSize: 10 }}>{updatedDate}</div>
      </Box>
    </Box>
  );
};

export const ProductListActionCell = (props: any) => {
  const { product = {}, onAddKeyword, onDelete } = props.value;
  return (
    <Grid container justify="center" alignItems="center">
      <Grid item>
        {onAddKeyword && (
          <ButtonUI
            label="Add keyword"
            colorButton={COLORS.COMMON.GRAY}
            size="small"
            onClick={() => onAddKeyword(product)}
          />
        )}
        <ButtonIconUI
          size="small"
          colorButton={COLORS.COMMON.GRAY}
          style={{ borderRadius: 4, marginLeft: 8 }}
          onClick={() => onDelete(product)}
        >
          <DeleteIcon fontSize="small" />
        </ButtonIconUI>
      </Grid>
    </Grid>
  );
};

export const AddToBucketCell = (props: any) => {
  const { item = {}, onAddToBucket } = props.value;
  return (
    <Grid container justify="center" alignItems="center">
      <Grid item>
        <ButtonUI
          label={item.added ? 'Added' : 'Add to Bucket'}
          colorButton={COLORS.COMMON.GRAY}
          size="small"
          onClick={() => !item.added && onAddToBucket(item)}
        />
      </Grid>
    </Grid>
  );
};

export function GroupKeywordActionCell(props: any) {
  const {
    item = {},
    onAddToBucket,
    onRemove,
    isRemovable,
  } = props.value;
  return (
    <Grid container justify="center" alignItems="center">
      <Grid item>
        <ButtonUI
          label={item.added ? 'Added' : 'Add to Bucket'}
          colorButton={COLORS.COMMON.GRAY}
          size="small"
          onClick={() => !item.added && onAddToBucket(item)}
        />
        <ButtonIconUI
          size="small"
          colorButton={COLORS.COMMON.GRAY}
          style={{
            borderRadius: 4,
            marginLeft: 8,
            visibility: isRemovable ? 'visible' : 'hidden',
          }}
          onClick={() => onRemove(item)}
        >
          <DeleteIcon fontSize="small" />
        </ButtonIconUI>
      </Grid>
    </Grid>
  );
}

export const CellText = (props: any) => {
  const { text, editor } = props.value;

  return (
    <React.Fragment>
      {text}
      {editor}
    </React.Fragment>
  );
};

export const PermissionCell = (props: any) => {
  const {
    value: { checked, indeterminate },
    column,
  } = props;
  return (
    <CheckboxUI
      checked={checked > 0}
      indeterminate={indeterminate}
      disabled={checked === 2}
      onChange={() => {
        column.selectedItem({
          checked: !checked,
          value: props.value,
          userId: column.id,
          row: props.row.original,
        });
      }}
    />
  );
};

export const DeleteIconCell = (props: any) => {
  const { item = {}, onDelete } = props.value;
  return (
    <Grid container justify="center" alignItems="center">
      <Grid item>
        <ButtonIconUI
          size="small"
          colorButton={COLORS.COMMON.GRAY}
          style={{ borderRadius: 4, marginLeft: 8 }}
          onClick={() => onDelete(item)}
        >
          <DeleteIcon fontSize="small" />
        </ButtonIconUI>
      </Grid>
    </Grid>
  );
};

export const ShopCreativeCell = (props: any) => {
  const classes = useStyles(props);
  const { value, column } = props;
  const clickable = column.onClickLabel;
  return (
    <div>
      <TooltipUI title={value.name}>
        <div
          className={clsx(
            classes.subjectName,
            clickable && 'clickable',
          )}
          style={{ fontWeight: 400 }}
          onClick={() =>
            clickable ? column.onClickLabel(props.row.original) : null
          }
        >
          <div className={classes.ellipsisText}>{value.name}</div>
        </div>
      </TooltipUI>
      <div className={classes.subjectCode}>{value.code}</div>
      {value.editorEnabled && (
        <column.Editor row={props.row.original} />
      )}
    </div>
  );
};

export const ListRuleCell = ({ value }: any) => {
  return <DropdownConditionRule condition={value.condition} />;
};
