import moment from 'moment';
import * as Yup from 'yup';
import { DATE_FORMAT } from '../../../constants';

export const validationSchema = Yup.object().shape({
  existingRule: Yup.string().required('Please choose Rule'),
  action: Yup.string().required('Please choose action'),
  fromDate: Yup.string()
    .required('From date is empty')
    .test('is-greater', 'Date invalid', function (value) {
      const fieldValue = moment(value, DATE_FORMAT);
      const current = moment();
      const a = fieldValue.diff(current, 'days') < 0;
      return !a;
    }),
  toDate: Yup.string()
    .required('To date is empty')
    .test('is-greater', 'Date invalid', function (value) {
      const fieldValue = moment(value, DATE_FORMAT);
      const current = moment();
      const a = fieldValue.diff(current, 'days') < 0;
      return !a;
    }),
  rules: Yup.object().shape({
    typeOfPerformance: Yup.string(),
    conditionPeriod: Yup.string().when('typeOfPerformance', {
      is: (val) => {
        const valuePerformance = val || '';
        const disabled =
          ['percent_discount_product', 'current_stock'].indexOf(
            valuePerformance.toLowerCase(),
          ) !== -1;
        return !disabled;
      },
      then: Yup.string().optional(),
    }),
    operator: Yup.string(),
    value: Yup.number(),
  }),
});

// .when('isNoLimit', {
//   is: true,
//   then: Yup.number().optional(),
// })
export const dataSearch: any = [
  {
    name: 'Portugal',
    value: 'portugal',
    rules: {
      action: 'paused',
      fromDate: '11/11/2020',
      toDate: '12/12/2020',
      conditions: [
        {
          id: 'ads1',
          typeOfPerformance: 'Ads item sold',
          conditionPeriod: 'Last x days',
          operator: '=',
          value: '20',
        },
        {
          id: 'ads2',
          typeOfPerformance: 'Ads item sold 2',
          conditionPeriod: 'Last x days',
          operator: '=',
          value: '19',
        },
        {
          id: 'ads3',
          typeOfPerformance: 'Ads item sold 3',
          conditionPeriod: 'Last x days',
          operator: '=',
          value: '18',
        },
      ],
    },
  },
  {
    name: 'Palauan',
    value: 'palauan',
    rules: {
      action: 'resume',
      fromDate: '12/12/2020',
      toDate: '12/12/2020',
      conditions: [
        {
          id: 'ads2',
          typeOfPerformance: 'Ads item sold',
          conditionPeriod: 'Last x days',
          operator: '=',
          value: '12',
        },
        {
          id: 'ads3',
          typeOfPerformance: 'Ads item sold 2',
          conditionPeriod: 'Last x days',
          operator: '=',
          value: '13',
        },
        {
          id: 'ads4',
          typeOfPerformance: 'Ads item sold 3',
          conditionPeriod: 'Last x days',
          operator: '=',
          value: '14',
        },
      ],
    },
  },
];

export const dataAction: any = [
  { name: 'Pause Product', value: 'pause_product' },
  { name: 'Resume Product', value: 'resume_product' },
];

export const dataTypeOfPerformance: any = {
  pause_product: [
    { name: 'Ads Item Sold', value: 'Ads Item Sold' },
    { name: 'Ads GMV', value: 'Ads GMV' },
    { name: 'Cost', value: 'Cost' },
    { name: 'CPI', value: 'CPI' },
    { name: 'CTR', value: 'CTR' },
    { name: 'CR', value: 'CR' },
    { name: 'CIR', value: 'CIR' },
    { name: 'CPC', value: 'CPC' },
    { name: 'ROAS', value: 'ROAS' },
    {
      name: '% Discount of Product',
      value: 'percent_discount_product',
    },
    { name: 'Total GMV of Product', value: 'Total GMV of Product' },
    {
      name: 'Total Item Sold of Product',
      value: 'Total Item Sold of Product',
    },
    { name: '%GMV Contribution', value: '%GMV Contribution' },
    { name: 'Current Stock', value: 'current_stock' },
  ],
  resume_product: [
    { name: 'Current Stock', value: 'Current Stock' },
    { name: 'Total GMV of Product', value: 'Total GMV of Product' },
    {
      name: 'Total Item Sold of Product',
      value: 'Total Item Sold of Product',
    },
    { name: '% Discount of Product', value: '% Discount of Product' },
  ],
};

export const dataOperator: any = [
  { name: '=', value: '=' },
  { name: '>', value: '>' },
  { name: '<', value: '<' },
  { name: '>=', value: '>=' },
  { name: '<=', value: '<=' },
];

export const dataPeriod: any = [
  { name: 'Current day', value: 1 },
  { name: 'Last 2 Days + current day', value: 3 },
  { name: 'Last 6 Days + current day', value: 7 },
  { name: 'Last 13 Days + current day', value: 14 },
  { name: 'Last 29 Days + current day', value: 30 },
];
