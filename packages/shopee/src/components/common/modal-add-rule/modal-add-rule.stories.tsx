import React, { useState } from 'react'
import { Button, Paper } from '@material-ui/core'
import { ModalAddRule } from './modal-add-rule';

export default {
  title: 'Shopee/Modal add rule'
}

const dataProducts = [
  {
    product_name: 'Product',
    product_id: 'SKYPRODUCT-1',
  }, {
    product_name: 'Product 2',
    product_id: 'SKYPRODUCT-2',
  }
]

const dataRules = [
  {
    id: 'ads1',
    typeOfPerformance: 'Ads item sold',
    conditionPeriod: 'Last x days',
    operator: '=',
    value: '20',
  }, {
    id: 'ads2',
    typeOfPerformance: 'Ads item sold 2',
    conditionPeriod: 'Last x days',
    operator: '=',
    value: '19',
  }, {
    id: 'ads3',
    typeOfPerformance: 'Ads item sold 3',
    conditionPeriod: 'Last x days',
    operator: '=',
    value: '18',
  },
]

export const main = () => {
  const [open, setOpen] = useState(true)
  const [rules, setRules] = useState(dataRules)
  const [products, setProduct] = useState(dataProducts)

  return (
    <Paper>
      <Button onClick={() => setOpen(true)}>Show modal</Button>
      <ModalAddRule
        open={open}
        rules={rules}
        onAddRule={(r: any) => setRules(r)}
        onSubmit={(value: any) => console.log('valuev', value)}
        onClose={(e: any) => setOpen(e)}
        products={products}
        setProduct={(p: any) => setProduct(p)}
      />
    </Paper>
  )
}
