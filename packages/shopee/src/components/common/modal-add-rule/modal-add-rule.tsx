import { COLORS, DATE_FORMAT } from '@ep/shopee/src/constants';
import {
  Box,
  Dialog,
  FormHelperText,
  Grid,
  IconButton,
  InputBase,
  MenuItem,
  Select,
  Typography,
} from '@material-ui/core';
import MuiDialogActions from '@material-ui/core/DialogActions';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import {
  createStyles,
  makeStyles,
  Theme,
  withStyles,
} from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import moment from 'moment';
import React, { useRef } from 'react';
import styled from 'styled-components';
import { InputUI } from '../../create-campaign/common';
import { ActionIcon } from '../action-icon';
import { ButtonLinkUI, ButtonUI } from '../button';
import { SingleDatePickerUI } from '../single-date-picker';
import { TableUINoCheckbox } from '../table';
import { TooltipUI } from '../tooltip';
import { initColumns } from './columns';
import {
  dataOperator,
  dataPeriod,
  validationSchema,
} from './constants';
export interface ModalAddRuleProps {
  suffix?: string;
  open: boolean;
  onClose?: any;
  products: any;
  onSubmit: any;
  setProduct: any;
  handleCreate: any;
  shopEid: number;
  ruleList: any[];
  actionList: any[];
  metricList: any[];
}

export function ModalAddRule(props: ModalAddRuleProps) {
  const {
    suffix = 'products',
    open,
    onClose,
    products,
    onSubmit,
    setProduct,
    shopEid,
    handleCreate,
    ruleList,
    actionList,
    metricList,
  } = props;

  const formRef = useRef();
  const classes = useStyles();

  const handleClose = () => {
    onClose(false);
  };

  return (
    <div>
      <Dialog
        open={open}
        classes={{ paper: classes.paper }}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle
          id="customized-dialog-title"
          onClose={handleClose}
          className={classes.dialogTitle}
        >
          Add rule
        </DialogTitle>
        <DialogContent>
          <span>
            Add rule for{' '}
            <strong>
              {products.length} selected {suffix}
            </strong>
          </span>
          <Wrapper>
            <Wrap>
              {products.map((item: any, index: number) => {
                const keyword = item.product_name;
                const title =
                  keyword.length > 21
                    ? `${keyword.slice(0, 21)}...`
                    : keyword;
                return (
                  <TooltipUI title={keyword} key={index}>
                    <Product>
                      {title}
                      {products.length > 1 && (
                        <Delete
                          onClick={() => {
                            const newKeyword = products.filter(
                              (item1: any) =>
                                item1.productId !== item.productId,
                            );
                            setProduct(newKeyword);
                          }}
                        >
                          <ActionIcon status="remove" />
                        </Delete>
                      )}
                    </Product>
                  </TooltipUI>
                );
              })}
            </Wrap>
          </Wrapper>
          <Formik
            validationSchema={validationSchema}
            initialValues={{
              existingRule: '',
              action: 'paused',
              fromDate: moment().format(DATE_FORMAT),
              toDate: moment().format(DATE_FORMAT),
              conditions: [],
              rules: {
                typeOfPerformance: '',
                conditionPeriod: '',
                operator: '',
                value: '',
                conditionJson: '',
              },
            }}
            onSubmit={(values: any) => {
              if (values.conditions.length > 0) {
                const conditions = values.conditions.map((i) => ({
                  ...i,
                  operator: i.operator_code,
                  conditionPeriod: i.period,
                  typeOfPerformance: i.metric_code,
                }));
                const objectEids = products.map(
                  (p) => p.product_id || p.productId,
                );
                return onSubmit({
                  ...values,
                  shopEid,
                  conditions,
                  objectEids,
                  featureCode: 'M_SHP_KB',
                  objectType: 'campaign_product',
                });
              }
            }}
            validateOnBlur={true}
            innerRef={formRef}
          >
            {(formik: any) => {
              const handleRemoveRule = (props) => {
                const { rule } = props;
                const { conditions } = formik.values;
                const newRule = conditions.filter(
                  (condition) => condition.id !== rule.id,
                );
                formik.setFieldValue('conditions', newRule);
              };

              return (
                <Form
                // className={classes.wrapForm}
                >
                  <Typography
                    variant="body2"
                    className={classes.labelInput}
                  >
                    Existing rule
                  </Typography>
                  <Field name="existingRule">
                    {({ field, form }: any) => {
                      return (
                        <Select
                          id="existingRule"
                          {...field}
                          input={<BootstrapInput />}
                          displayEmpty
                          MenuProps={{
                            anchorOrigin: {
                              vertical: 'bottom',
                              horizontal: 'left',
                            },
                            transformOrigin: {
                              vertical: 'top',
                              horizontal: 'left',
                            },
                            getContentAnchorEl: null,
                          }}
                          onChange={(event: any) => {
                            const { value } = event.target;
                            form.setFieldValue(field.name, value);
                            const dataRule = ruleList.filter(
                              (rule: any) => value === rule.rule_code,
                            );
                            form.setFieldValue(
                              'action',
                              dataRule[0].action_code,
                            );
                            form.setFieldValue(
                              'fromDate',
                              moment(
                                dataRule[0].timeline_from,
                              ).format(DATE_FORMAT),
                            );
                            form.setFieldValue(
                              'toDate',
                              moment(dataRule[0].timeline_to).format(
                                DATE_FORMAT,
                              ),
                            );
                            form.setFieldValue(
                              'timelineFrom',
                              moment(
                                dataRule[0].timeline_from,
                              ).format(DATE_FORMAT),
                            );
                            form.setFieldValue(
                              'timelineTo',
                              moment(dataRule[0].timeline_to).format(
                                DATE_FORMAT,
                              ),
                            );
                            form.setFieldValue(
                              'conditions',
                              dataRule[0].condition,
                            );
                            form.setFieldValue(
                              'conditionJson',
                              dataRule[0].condition_json,
                            );
                            form.setFieldValue('ruleId', [
                              dataRule[0].rule_id,
                            ]);
                          }}
                        >
                          <MenuItem
                            style={{ display: 'none' }}
                            value={''}
                            disabled
                          >
                            Choose existing rule
                          </MenuItem>
                          {ruleList &&
                            ruleList.map((item: any) => (
                              <MenuItem
                                value={item.rule_code}
                                key={item.rule_code}
                              >
                                {item.rule_name}
                              </MenuItem>
                            ))}
                        </Select>
                      );
                    }}
                  </Field>
                  <ErrorMessage
                    component={FormHelperText}
                    name="existingRule"
                    className={classes.error}
                  />
                  {formik.values.existingRule && (
                    <>
                      <Typography
                        variant="body2"
                        className={classes.labelInput}
                      >
                        Action
                      </Typography>
                      <Field name="action">
                        {({ field }: any) => {
                          return (
                            <Select
                              id="action"
                              input={<BootstrapInput />}
                              displayEmpty
                              MenuProps={{
                                anchorOrigin: {
                                  vertical: 'bottom',
                                  horizontal: 'left',
                                },
                                transformOrigin: {
                                  vertical: 'top',
                                  horizontal: 'left',
                                },
                                getContentAnchorEl: null,
                              }}
                              {...field}
                            >
                              <MenuItem
                                style={{ display: 'none' }}
                                value={''}
                                disabled
                              >
                                Choose Action
                              </MenuItem>
                              {actionList.map((item: any) => (
                                <MenuItem
                                  value={item.value}
                                  key={item.value}
                                >
                                  {item.name}
                                </MenuItem>
                              ))}
                            </Select>
                          );
                        }}
                      </Field>
                      <ErrorMessage
                        component={FormHelperText}
                        name="action"
                        className={classes.error}
                      />
                      <Typography
                        variant="body2"
                        className={classes.labelInput}
                      >
                        From Date
                      </Typography>
                      <Field name="fromDate">
                        {({ field, form }: any) => (
                          <SingleDatePickerUI
                            date={field.value}
                            minDate={moment().format(DATE_FORMAT)}
                            onChange={(date: any) =>
                              form.setFieldValue(field.name, date)
                            }
                          />
                        )}
                      </Field>
                      <ErrorMessage
                        component={FormHelperText}
                        name="fromDate"
                        className={classes.error}
                      />
                      <Typography
                        variant="body2"
                        className={classes.labelInput}
                      >
                        To Date
                      </Typography>
                      <Field name="toDate">
                        {({ field, form }: any) => (
                          <SingleDatePickerUI
                            date={field.value}
                            placeholder="No limit"
                            minDate={moment().format(DATE_FORMAT)}
                            onChange={(date: any) =>
                              form.setFieldValue(field.name, date)
                            }
                          />
                        )}
                      </Field>
                      <ErrorMessage
                        component={FormHelperText}
                        name="toDate"
                        className={classes.error}
                      />
                      <div className={classes.wrapConditions}>
                        <Typography
                          variant="body2"
                          className={classes.labelInput}
                        >
                          Type of performance
                        </Typography>
                        <Field name="rules.typeOfPerformance">
                          {({ field, form }: any) => {
                            return (
                              <Select
                                id="existingRule"
                                {...field}
                                input={<BootstrapInput />}
                                displayEmpty
                                MenuProps={{
                                  anchorOrigin: {
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                  },
                                  transformOrigin: {
                                    vertical: 'top',
                                    horizontal: 'left',
                                  },
                                  getContentAnchorEl: null,
                                }}
                              >
                                <MenuItem
                                  style={{ display: 'none' }}
                                  value={''}
                                  disabled
                                >
                                  Choose Type Of Performance
                                </MenuItem>
                                {metricList[form.values.action].map(
                                  (item: any) => (
                                    <MenuItem
                                      value={item.value}
                                      key={item.value}
                                    >
                                      {item.name}
                                    </MenuItem>
                                  ),
                                )}
                              </Select>
                            );
                          }}
                        </Field>
                        <Typography
                          variant="body2"
                          className={classes.labelInput}
                        >
                          Condition period
                        </Typography>
                        <Field name="rules.conditionPeriod">
                          {({ field, form }: any) => {
                            const valuePerformance = form.values.rules.typeOfPerformance.toLowerCase();
                            const disabled =
                              [
                                'percent_discount_product',
                                'current_stock',
                              ].indexOf(valuePerformance) !== -1;
                            return (
                              <Select
                                id="existingRule"
                                {...field}
                                input={<BootstrapInput />}
                                displayEmpty
                                disabled={disabled}
                                MenuProps={{
                                  anchorOrigin: {
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                  },
                                  transformOrigin: {
                                    vertical: 'top',
                                    horizontal: 'left',
                                  },
                                  getContentAnchorEl: null,
                                }}
                              >
                                <MenuItem
                                  style={{ display: 'none' }}
                                  value={''}
                                  disabled
                                >
                                  Choose Period
                                </MenuItem>
                                {dataPeriod.map((item: any) => (
                                  <MenuItem
                                    value={item.value}
                                    key={item.value}
                                  >
                                    {item.name}
                                  </MenuItem>
                                ))}
                              </Select>
                            );
                          }}
                        </Field>
                        <Typography
                          variant="body2"
                          className={classes.labelInput}
                        >
                          Operator
                        </Typography>
                        <Field name="rules.operator">
                          {({ field }: any) => {
                            return (
                              <Select
                                id="existingRule"
                                {...field}
                                input={<BootstrapInput />}
                                displayEmpty
                                MenuProps={{
                                  anchorOrigin: {
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                  },
                                  transformOrigin: {
                                    vertical: 'top',
                                    horizontal: 'left',
                                  },
                                  getContentAnchorEl: null,
                                }}
                              >
                                <MenuItem
                                  style={{ display: 'none' }}
                                  value={''}
                                  disabled
                                >
                                  Choose Period
                                </MenuItem>
                                {dataOperator.map((item: any) => (
                                  <MenuItem
                                    value={item.value}
                                    key={item.value}
                                  >
                                    {item.name}
                                  </MenuItem>
                                ))}
                              </Select>
                            );
                          }}
                        </Field>
                        <Typography
                          variant="body2"
                          className={classes.labelInput}
                        >
                          Value
                        </Typography>
                        <Box>
                          <Field name="rules.value">
                            {({ field }: any) => {
                              return (
                                <InputUI
                                  id="daily-budget"
                                  type="number"
                                  fullWidth
                                  {...field}
                                  autoComplete="off"
                                  className={classes.input}
                                />
                              );
                            }}
                          </Field>
                          <ErrorMessage
                            component={FormHelperText}
                            name="rules.value"
                            className={classes.error}
                          />
                        </Box>
                        <Box style={{ marginTop: 16 }}>
                          <Grid container spacing={1}>
                            <Grid item>
                              <Field>
                                {({ field, form }: any) => {
                                  const { rules } = field.value;
                                  const {
                                    conditionPeriod,
                                    operator,
                                    typeOfPerformance,
                                    value,
                                  } = rules;
                                  const valuePerformance = form.values.rules.typeOfPerformance.toLowerCase();
                                  let disabled = [
                                    conditionPeriod,
                                    operator,
                                    typeOfPerformance,
                                    value,
                                  ].includes('');
                                  if (
                                    [
                                      'percent_discount_product',
                                      'current_stock',
                                    ].indexOf(valuePerformance) !== -1
                                  ) {
                                    disabled = false;
                                  }
                                  return (
                                    <ButtonUI
                                      disabled={disabled}
                                      label="Add condition"
                                      size="small"
                                      colorButton="#F6F7F8"
                                      onClick={() => {
                                        const { rules } = field.value;
                                        const newCondition = [
                                          ...form.values.conditions,
                                          {
                                            value: rules.value,
                                            period:
                                              rules.conditionPeriod,
                                            metric_code:
                                              rules.typeOfPerformance,
                                            operator_code:
                                              rules.operator,
                                            id: `condition${Math.floor(
                                              Math.random() * 10,
                                            )}`,
                                          },
                                        ];
                                        form.setFieldValue(
                                          'conditions',
                                          newCondition,
                                        );
                                        form.setFieldValue(
                                          'rules',
                                          form.initialValues.rules,
                                        );
                                      }}
                                    />
                                  );
                                }}
                              </Field>
                            </Grid>
                            <Grid item>
                              <Field>
                                {({ form }: any) => {
                                  return (
                                    <ButtonUI
                                      label="Reset"
                                      size="small"
                                      colorButton="#F6F7F8"
                                      onClick={() =>
                                        form.setFieldValue(
                                          'rules',
                                          form.initialValues.rules,
                                        )
                                      }
                                    />
                                  );
                                }}
                              </Field>
                            </Grid>
                          </Grid>
                        </Box>
                      </div>
                      <Field name={'conditions'}>
                        {({ field, form }: any) => {
                          const rows = field.value.map((r) => {
                            const metricCode = metricList[
                              form.values.action
                            ].filter(
                              (a) => a.value === r.metric_code,
                            );
                            const period = dataPeriod.filter(
                              (p) => p.value === r.period,
                            );
                            return {
                              ...r,
                              metric_code: metricCode[0].name || '',
                              period: period[0]?.name || '',
                            };
                          });
                          return (
                            <TableUINoCheckbox
                              key={field.value.length}
                              className="add-rule"
                              columns={initColumns({
                                onCellClick: handleRemoveRule,
                              })}
                              rows={rows}
                              noData="No data"
                              onSort={() => {}}
                            />
                          );
                        }}
                      </Field>
                    </>
                  )}
                </Form>
              );
            }}
          </Formik>
        </DialogContent>
        <DialogActions>
          <Grid container justify="space-between">
            <Grid xs={6}>
              <ButtonUI
                label="Create new rule"
                size="small"
                colorButton="#F6F7F8"
                onClick={handleCreate}
              />
            </Grid>
            <Grid xs={6}>
              <Grid container justify="flex-end">
                <ButtonLinkUI
                  label="Cancel"
                  size="small"
                  colortext="#f16145"
                  onClick={handleClose}
                />
                <ButtonUI
                  label="Save rule"
                  size="small"
                  variant="contained"
                  onClick={() => {
                    if (formRef.current) {
                      formRef.current.handleSubmit();
                    }
                  }}
                />
              </Grid>
            </Grid>
          </Grid>
        </DialogActions>
      </Dialog>
    </div>
  );
}

const Product = styled.span`
  background: #f6f7f8;
  border-radius: 4px;
  padding: 2px 6px;
  font-size: 14px;
  line-height: 20px;
  color: #253746;
  margin-right: 8px;
  margin-bottom: 8px;
  display: inline-block;
`;

const Wrap = styled.div`
  border: 2px solid #e4e7e9;
  border-radius: 4px;
  max-height: 120px;
  padding: 8px 8px 0 8px;
  width: 100%;
  overflow: auto;
`;

const Wrapper = styled.div`
  margin: 8px 0;
`;

const Delete = styled.span`
  margin-left: 9px;
  float: right;
  line-height: 14px;
`;

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });

const useStyles = makeStyles(() => ({
  paper: { width: 480, maxHeight: 528 },
  wrapForm: { maxHeight: 208, overflow: 'auto' },
  wrapConditions: {
    padding: 16,
    border: '2px solid #e4e7e9',
    borderRadius: 4,
    marginTop: 8,
  },
  labelInput: {
    marginTop: 8,
    marginBottom: 4,
    fontWeight: 'bold',
    fontSize: 11,
    lineHeight: '16px',
  },
  error: {
    color: COLORS.COMMON.RED,
    fontSize: 12,
  },
  input: {
    '& .MuiOutlinedInput-notchedOutline': {
      border: '2px solid #e4e7e9',
    },
  },
  dialogTitle: {
    padding: '16px 16px 0 16px',
  },
}));

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose?: () => void;
  classes: any;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle
      disableTypography
      className={classes.root}
      {...other}
    >
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

const BootstrapInput = withStyles({
  root: {
    width: '100%',
  },
  disabled: {
    color: '#9FA7AE !important',
    background: '#F6F7F8 !important',
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: '#ffffff',
    border: '2px solid #C2C7CB',
    fontSize: 14,
    color: '#253746',
    padding: '8px 26px 9px 8px',
  },
})(InputBase);
