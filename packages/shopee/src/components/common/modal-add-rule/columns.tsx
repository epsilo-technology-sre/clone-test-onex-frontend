import React from 'react';
import { ButtonCell } from '../table-cell';
import DeleteIcon from '@material-ui/icons/Delete';
import SvgIcon from '@material-ui/core/SvgIcon';

export const initColumns = (props: any) => {
  const { onCellClick } = props;
  return [
    {
      Header: 'Type of performance',
      id: 'metric_code',
      accessor: 'metric_code',
      disableSortBy: true,
      width: 152,
    },
    {
      Header: 'Period',
      id: 'period',
      disableSortBy: true,
      accessor: 'period',
      width: 104,
    },
    {
      Header: 'Operator',
      id: 'operator_code',
      accessor: 'operator_code',
      disableSortBy: true,
      width: 72,
    },
    {
      Header: 'Value',
      id: 'value',
      accessor: 'value',
      disableSortBy: true,
      width: 72,
    },
    {
      Header: '',
      id: 'delete',
      accessor: (row: any) => ({
        rule: row,
        label: <SvgIcon fontSize="small">
          <DeleteIcon />
        </SvgIcon>,
        onCellClick,
      }),
      disableSortBy: true,
      width: 48,
      Cell: ButtonCell,
    },
  ];
}
