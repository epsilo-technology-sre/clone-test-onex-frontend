import React from 'react'
import { Icon } from '@material-ui/core';
import RemoveIcon from '@ep/shopee/src/images/remove.svg';

import { USER_ACTIONS } from '@ep/shopee/src/constants';

export interface StatusIconProps {
  status: any,
}

export const ActionIcon = ({ status }: StatusIconProps) => {
  const icons = {
    "remove": RemoveIcon,
  }

  return (
    <Icon>
      <img src={icons[status] || ''} height={12} width={12} alt={status} />
    </Icon>
  )
}
