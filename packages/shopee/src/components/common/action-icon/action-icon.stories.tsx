import React from 'react'
import Paper from '@material-ui/core/Paper'
import Box from '@material-ui/core/Box'
import { ActionIcon } from './action-icon'

export default {
  title: 'Shopee/Actions Icon'
}

const actions = ["remove"];


export const main = () => {
  return (
    <Paper>
      {actions.map((action, index) => {
        return <Box mx={2}>
          <ActionIcon key={index} status={action} />
          <Box component="span" ml={1} style={{textTransform: 'capitalize'}}>{action}</Box>
        </Box>
      })}
    </Paper>
  )
}
