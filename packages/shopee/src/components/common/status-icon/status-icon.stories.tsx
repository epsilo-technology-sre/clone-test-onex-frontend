import React from 'react'
import Paper from '@material-ui/core/Paper'
import Box from '@material-ui/core/Box'
import { StatusIcon } from './status-icon'
import { CAMPAIGN_STATUS, PRODUCT_STATUS } from '@ep/shopee/src/constants'

export default {
  title: 'Shopee/StatusIcon'
}

export const main = () => {
  const types = [];
  for (const status in CAMPAIGN_STATUS) {
    types.push(CAMPAIGN_STATUS[status])
  }

  types.push(PRODUCT_STATUS.CLOSED)

  return (
    <Paper>
      {types.map((type, index) => (
        <Box mx={2}>
          <StatusIcon key={index} status={type}></StatusIcon>
          <Box component="span" ml={1} style={{textTransform: 'capitalize'}}>{type}</Box>
        </Box>))}
    </Paper>
  )
}