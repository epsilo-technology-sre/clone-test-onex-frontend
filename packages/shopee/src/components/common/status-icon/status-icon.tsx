import React from 'react'
import { Icon } from '@material-ui/core'
import RunningIcon from '@ep/shopee/src/images/running.svg'
import ScheduledIcon from '@ep/shopee/src/images/scheduled.svg'
import EndedIcon from '@ep/shopee/src/images/ended.svg'
import ClosedIcon from '@ep/shopee/src/images/closed.svg'
import PausedIcon from '@ep/shopee/src/images/paused.svg'
import SyncingIcon from '@ep/shopee/src/images/syncing.svg'

import { CAMPAIGN_STATUS, PRODUCT_STATUS } from '@ep/shopee/src/constants'

export interface StatusIconProps {
  status: any,
}

export const StatusIcon = ({ status }: StatusIconProps) => {
  const icons = {
    [CAMPAIGN_STATUS.ACTIVE]: RunningIcon,
    [CAMPAIGN_STATUS.RUNNING]: RunningIcon,
    [CAMPAIGN_STATUS.PAUSED]: PausedIcon,
    [CAMPAIGN_STATUS.SCHEDULED]: ScheduledIcon,
    [CAMPAIGN_STATUS.SYNCING]: SyncingIcon,
    [CAMPAIGN_STATUS.ENDED]: EndedIcon,
    [PRODUCT_STATUS.CLOSED]: ClosedIcon,
  }

  return (
    <Icon>
      <img src={icons[status] || ''} height={12} width={12} alt={status} />
    </Icon>
  )
}
