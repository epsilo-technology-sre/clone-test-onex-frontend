import {
  CURRENCY_LIMITATION,
  DATE_FORMAT,
} from '@ep/shopee/src/constants';
import {
  Box,
  FormHelperText,
  Grid,
  TextField,
  Typography,
} from '@material-ui/core';
import { ErrorMessage, Form, Formik } from 'formik';
import moment from 'moment';
import React from 'react';
import * as Yup from 'yup';
import { formatCurrency } from '../../../utils/utils';
import { CampaignBudget } from '../campaign-budget';
import { CampaignName } from '../campaign-name';
import { CampaignTimeline } from '../campaign-timeline';
import { AutoCompleteUI, ButtonLogin, useStyle } from '../common';

let inChecking: any = null;

type SetupCampaignProps = {
  shops: {
    shopId: number;
    shopName: string;
    shopCurrency: string;
  }[];
  campaignName: string;
  shop: number;
  budget: any;
  timeline: any;
  onSubmit: (form: any) => void;
  checkCampaignNameExists: (
    name: string,
    shopId: number,
  ) => Promise<boolean>;
};

const SettingCampaignSchema = (
  checkExistCampaignName: SetupCampaignProps['checkCampaignNameExists'],
) => {
  return Yup.object().shape({
    shop: Yup.number().required('Please select shop'),
    shopName: Yup.string(),
    shopCurrency: Yup.string(),
    campaignName: Yup.string()
      .required('Please enter Campaign name')
      .test(
        'contain-default',
        'Campaign Name cannot contain “Default” or “default”',
        async (value: any = '') => {
          if (value.toLowerCase().includes('default')) {
            return false;
          }
          return true;
        },
      )
      .when(['shop'], (shopId: number) => {
        if (shopId > 0) {
          return Yup.string().test(
            'Unique Campaign name',
            'Campaign Name cannot be duplicated',
            async (value: any = '') => {
              clearTimeout(inChecking);
              let campaignName = value.trim().toLowerCase();

              if (!campaignName) {
                return false;
              }

              return await new Promise((resolve, reject) => {
                inChecking = setTimeout(async () => {
                  try {
                    let result = await checkExistCampaignName(
                      value.trim(),
                      shopId,
                    );
                    resolve(result);
                  } catch (e) {
                    resolve(false);
                  }
                }, 500);
              });
            },
          );
        }
      }),
    budget: Yup.object().shape({
      currency: Yup.string(),
      total: Yup.number()
        .when(
          ['currency', 'isNoLimit'],
          (currency, isNoLimit, schema) => {
            if (isNoLimit) {
              return schema;
            } else {
              const minValue =
                CURRENCY_LIMITATION[currency]?.TOTAL_BUDGET?.MIN || 1;
              return schema.min(
                minValue,
                `Total budget must be higher ${formatCurrency(
                  minValue,
                )}`,
              );
            }
          },
        )
        .when('isNoLimit', {
          is: (val) => !val,
          then: Yup.number().required('Please enter Budget'),
        }),
      daily: Yup.number()
        .when('isOnDaily', {
          is: true,
          then: Yup.number().required('Please enter Budget'),
        })
        .when(
          ['currency', 'isNoLimit', 'isOnDaily'],
          (currency, isNoLimit, isOnDaily, schema) => {
            if (!isNoLimit && isOnDaily) {
              const minValue =
                CURRENCY_LIMITATION[currency]?.DAILY_BUDGET?.MIN || 1;
              return schema
                .min(
                  minValue,
                  `Daily budget must be higher ${formatCurrency(
                    minValue,
                  )}`,
                )
                .lessThan(
                  Yup.ref('total'),
                  'Daily Budget must be lower than Total Budget.',
                );
            } else {
              return schema;
            }
          },
        ),
    }),
    timeline: Yup.object().shape({
      startDate: Yup.string().required('Start date is empty'),
      endDate: Yup.string().required('Start date is empty'),
      isNoLimit: Yup.bool(),
    }),
  });
};

const today = moment().format(DATE_FORMAT);

export const SetupCampaign = (props: SetupCampaignProps) => {
  const classes = useStyle();

  const initialValues = {
    shop: props.shop,
    campaignName: props.campaignName,
    budget: props.budget,
    timeline: props.timeline,
    shopName: props.shopName,
    shopCurrency: props.shopCurrency,
  };

  const validationSchema = React.useMemo(
    () => SettingCampaignSchema(props.checkCampaignNameExists),
    [],
  );

  return (
    <Formik
      validationSchema={validationSchema}
      initialValues={initialValues}
      onSubmit={(values, ...rest) => {
        props.onSubmit(values);
      }}
      validateOnBlur={true}
    >
      {(formikProps) => {
        const { values, setFieldValue, handleSubmit } = formikProps;
        return (
          <Form onSubmit={handleSubmit}>
            <Grid
              container
              direction="column"
              spacing={2}
              className={classes.root}
            >
              <Grid item>
                <Typography variant="subtitle1">
                  Campaign information
                </Typography>
                <Box>
                  <Typography
                    variant="subtitle2"
                    className={classes.label}
                  >
                    Shop
                  </Typography>
                  <AutoCompleteUI
                    id="shop"
                    fullWidth
                    options={props.shops}
                    defaultValue={(props.shops || []).find(
                      (s) => s.shopId === values.shop,
                    )}
                    getOptionLabel={(option) => option.shopName}
                    renderInput={(params) => (
                      <TextField {...params} variant="outlined" />
                    )}
                    onChange={(
                      e,
                      values: SetupCampaignProps['shops'][0],
                    ) => {
                      const val = values;
                      setFieldValue('shop', val ? val.shopId : '');
                      setFieldValue(
                        'shopName',
                        val ? val.shopName : '',
                      );
                      setFieldValue(
                        'shopCurrency',
                        val ? val.shopCurrency : '',
                      );
                      setFieldValue(
                        'budget.currency',
                        val ? val.shopCurrency : '',
                      );
                    }}
                  />
                  <ErrorMessage
                    component={FormHelperText}
                    name="shop"
                  />
                </Box>
                <CampaignName></CampaignName>
              </Grid>
              <Grid item>
                <Typography variant="subtitle1">Budgets</Typography>
                <CampaignBudget
                  currency={values.shopCurrency}
                ></CampaignBudget>
              </Grid>
              <Grid item>
                <Typography variant="subtitle1">Timeline</Typography>
                <CampaignTimeline></CampaignTimeline>
              </Grid>
              <Grid item></Grid>
              <Grid item>
                <ButtonLogin
                  variant={'contained'}
                  size="medium"
                  fullWidth
                  type={'submit'}
                >
                  Next step
                </ButtonLogin>
              </Grid>
            </Grid>
          </Form>
        );
      }}
    </Formik>
  );
};
