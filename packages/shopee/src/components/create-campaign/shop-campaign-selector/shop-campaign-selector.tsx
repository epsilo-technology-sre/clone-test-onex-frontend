import { Grid, TextField, Typography } from '@material-ui/core';
import React from 'react';
import { AutoCompleteUI, useStyle } from '../common';

export const ShopCampaignSelector = (props: any) => {
  const classes = useStyle();
  const {
    shops = [],
    campaigns = [],
    selectedShopId,
    selectedCampaignId,
    onChangeShop,
    onChangeCampaign,
  } = props;

  const [selectingCampaign, setSelectingCampaign] = React.useState(
    null,
  );

  React.useEffect(() => {
    setSelectingCampaign(null);
  }, [selectedShopId]);

  React.useEffect(() => {
    setSelectingCampaign(
      (campaigns || []).find(
        (i) => i.campaignId === selectedCampaignId,
      ),
    );
  }, [selectedCampaignId]);

  const handleChangeShop = (event: any, value: any) => {
    onChangeShop(value);
  };

  const handleChangeCampaign = (event: any, value: any) => {
    onChangeCampaign(value);
  };

  return (
    <Grid container spacing={1}>
      <Grid item xs={6}>
        <Typography variant="subtitle2" className={classes.label}>
          Shop
        </Typography>
        <AutoCompleteUI
          id="shop-selector"
          options={shops}
          defaultValue={(shops || []).find(
            (i) => i.shopId === selectedShopId,
          )}
          getOptionLabel={(option) => option.shopName}
          renderInput={(params) => (
            <TextField {...params} variant="outlined" />
          )}
          onChange={handleChangeShop}
        />
      </Grid>
      <Grid item xs={6}>
        <Typography variant="subtitle2" className={classes.label}>
          Campaign
        </Typography>
        <AutoCompleteUI
          id="campaign-selector"
          options={campaigns}
          value={selectingCampaign}
          getOptionLabel={(option) => option.campaignName}
          renderInput={(params) => (
            <TextField {...params} variant="outlined" />
          )}
          onChange={handleChangeCampaign}
        />
      </Grid>
    </Grid>
  );
};
