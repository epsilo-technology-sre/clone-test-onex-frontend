import React, { useState } from 'react';
import { ShopCampaignSelector } from './shop-campaign-selector';
import { Box, Typography } from '@material-ui/core';

export default {
  title: 'Shopee / Create Campaign',
};

const shops = [
  { shop_eid: 1, shop_name: 'Shop 1' },
  { shop_eid: 2, shop_name: 'Shop 2' },
  { shop_eid: 3, shop_name: 'Shop 3' },
  { shop_eid: 4, shop_name: 'Shop 4' },
  { shop_eid: 5, shop_name: 'Shop 5' },
  { shop_eid: 6, shop_name: 'Shop 6' },
  { shop_eid: 7, shop_name: 'Shop 7' },
];

const campaigns = [
  { campaign_eid: 1, campaign_name: 'Campaign 1' },
  { campaign_eid: 2, campaign_name: 'Campaign 2' },
  { campaign_eid: 3, campaign_name: 'Campaign 3' },
  { campaign_eid: 4, campaign_name: 'Campaign 4' },
  { campaign_eid: 5, campaign_name: 'Campaign 5' },
  { campaign_eid: 6, campaign_name: 'Campaign 6' },
  { campaign_eid: 7, campaign_name: 'Campaign 7' },
];

export const ShopCampaignSelectorExample = () => {
  const [shop, setShop] = useState();
  const [campaign, setCampaign] = useState();

  return (
    <Box>
      <Box py={1}>
        <Typography variant="subtitle1">
          Select shop and campaign
        </Typography>
        <ShopCampaignSelector
          shops={shops}
          campaigns={campaigns}
          onChangeShop={(value: any) => setShop(value)}
          onChangeCampaign={(value: any) => setCampaign(value)}
        ></ShopCampaignSelector>
      </Box>
      <Box py={2}>
        <Box>Selected shop: {shop}</Box>
        <Box>Selected campaign: {campaign}</Box>
      </Box>
    </Box>
  );
};
