
import React from 'react'
import { Formik, Form, FormikConfig, FormikValues } from 'formik'


export const Stepper = ({ children, ...props }: FormikConfig<FormikValues>) => {
  const childrenArray = React.Children.toArray(children);
  const [step, setStep] = React.useState(0);
  const currentChild = childrenArray[step];

  return (
    <Formik {...props}>
      <Form autoComplete="off">{currentChild}</Form>
    </Formik>
  )
}
