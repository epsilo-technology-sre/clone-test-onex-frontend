import { Box, makeStyles, Typography } from '@material-ui/core';
import { Field, useFormikContext } from 'formik';
import React from 'react';
import { InputUI, useStyle } from '../common';

export const CampaignName = () => {
  const classes = useStyle();
  const formik = useFormikContext();

  return (
    <Box py={1}>
      <Typography variant="body2" className={classes.label}>
        Campaign name
      </Typography>
      <Field name="campaignName">
        {({ field }: any) => {
          console.info({field});
          return (
            <InputUI
              type="text"
              id="campaign-name"
              fullWidth
              error={!!formik.errors.campaignName}
              {...field}
            ></InputUI>
          );
        }}
      </Field>

      {formik.errors.campaignName ? (
        <Box className={classes.error}>
          {formik.errors.campaignName}
        </Box>
      ) : null}
    </Box>
  );
};
