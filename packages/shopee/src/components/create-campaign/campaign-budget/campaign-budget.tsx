import SwitchOffIcon from '@ep/one/src/images/switch-off.svg';
import SwitchOnIcon from '@ep/one/src/images/switch-on.svg';
import ErrorIcon from '@ep/shopee/src/images/error-icon.svg';
import { Box, InputAdornment, Typography } from '@material-ui/core';
import { Field, useFormikContext } from 'formik';
import React from 'react';
import { CurrencyCode } from '../../common/currency-code';
import { InputUI, useStyle } from '../common';

export const CampaignBudget = (props: { currency: string }) => {
  const classes = useStyle();
  const { currency } = props;
  const formik = useFormikContext();

  let formValue = formik.values.budget as any;

  return (
    <Box>
      <Box pb={1}>
        <Typography variant="body2" className={classes.label}>
          Total budget
        </Typography>
        <Field name="budget.total">
          {({ field }: any) => {
            return (
              <InputUI
                id="total-budget"
                type="number"
                disabled={formValue.isNoLimit}
                fullWidth
                {...field}
                startAdornment={
                  <InputAdornment position="start">
                    <CurrencyCode currency={currency} />
                  </InputAdornment>
                }
                error={!!formik.errors.budget?.total}
                autoComplete="off"
              ></InputUI>
            );
          }}
        </Field>
        {!(formik.values as any).isOnNoLimit &&
        formik.errors.budget?.total ? (
          <Box className={classes.error}>
            <img src={ErrorIcon} height={9} width={9} />
            {formik.errors.budget?.total}
          </Box>
        ) : null}
      </Box>

      <Field name="budget.isNoLimit">
        {({ field }: any) => {
          return (
            <Box className={classes.switcher}>
              <img
                src={field.value ? SwitchOnIcon : SwitchOffIcon}
                height={16}
                width={30}
                onClick={() => {
                  formik.setFieldValue(field.name, !field.value);
                }}
              />
              <Typography variant="body2" component="span">
                No limit
              </Typography>
            </Box>
          );
        }}
      </Field>

      <Field name="budget.isOnDaily">
        {({ field }: any) => {
          return (
            <Box className={classes.switcher}>
              <img
                src={field.value ? SwitchOnIcon : SwitchOffIcon}
                height={16}
                width={30}
                onClick={() => {
                  formik.setFieldValue(
                    'budget.isOnDaily',
                    !field.value,
                  );
                }}
              />
              <Typography variant="body2" component="span">
                Daily budget
              </Typography>
            </Box>
          );
        }}
      </Field>

      {formValue.isOnDaily && (
        <Box>
          <Typography variant="body2" className={classes.label}>
            Daily budget
          </Typography>
          <Field name="budget.daily">
            {({ field }: any) => {
              return (
                <InputUI
                  id="daily-budget"
                  type="number"
                  fullWidth
                  {...field}
                  startAdornment={
                    <InputAdornment position="start">
                      <CurrencyCode currency={currency} />
                    </InputAdornment>
                  }
                  error={!!formik.errors.budget?.daily}
                  autoComplete="off"
                ></InputUI>
              );
            }}
          </Field>
          {formValue.isOnDaily && formik.errors.budget?.daily ? (
            <Box className={classes.error}>
              <img src={ErrorIcon} height={9} width={9} />
              {formik.errors.budget?.daily}
            </Box>
          ) : null}
        </Box>
      )}
    </Box>
  );
};
