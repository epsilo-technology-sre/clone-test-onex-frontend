import { BulkActionsUI } from '@ep/shopee/src/components/common/bulk-actions';
import { ButtonUI } from '@ep/shopee/src/components/common/button';
import React from 'react';
import { LocalFilter } from '../common/local-filter';
import { TableUISticky as TableUI } from '../common/table';
import { KeywordContext } from './keyword-context';

export const KeywordView = (props: any) => {
  const {
    tableHeaders,
    keywords,
    pagination,
    statusList,
    selectedStatus,
    searchText,
    selectedIds,
    selectedKeywords = [],
    handleSelectItem,
    handleSelectAll,
    getRowId,
    onSearch,
    onChangeStatus,
    onSorting,
    onChangePage,
    onChangePageSize,
    onAddKeyword,
    handleActivator,
    handleUpdateKeyword,
    handleOpenModifyBudget,
    handleGetSuggestBiddingPrice,
    openModalAddrule,
    handleDeleteRules,
  } = props;

  const context = {
    updateKeywordBudget: (value: any) => {
      return handleUpdateKeyword({
        shopIds: [value.shopId],
        keywordIds: value.keywordId,
        biddingPrice: value.biddingPrice,
        matchType: value.matchType,
      });
    },
    updateKeywordType: (value: any) => {
      return handleUpdateKeyword({
        shopIds: [value.shopId],
        keywordIds: value.keywordId,
        biddingPrice: value.biddingPrice,
        matchType: value.matchType,
      });
    },
    getSuggestBiddingPrice: (value: any) => {
      return handleGetSuggestBiddingPrice({
        shopId: value.shopId,
        keywordId: value.keywordId,
      });
    },
  };

  const buttons = [
    <ButtonUI
      onClick={openModalAddrule}
      label="Add rule"
      size="small"
      colorButton="#F6F7F8"
    />,
    <ButtonUI
      onClick={handleOpenModifyBudget}
      label="Modify keyword"
      size="small"
      colorButton="#F6F7F8"
    />,
    <ButtonUI
      onClick={() => handleActivator('active', selectedKeywords)}
      label="Activate"
      size="small"
      colorButton="#F6F7F8"
    />,
    <ButtonUI
      colortext="#D4290D"
      onClick={() => handleActivator('deactive', selectedKeywords)}
      label="Deactivate"
      size="small"
      colorButton="#F6F7F8"
    />,
    <ButtonUI
      onClick={handleDeleteRules}
      label="Delete rules"
      size="small"
      colorButton="#F6F7F8"
    />,
  ];

  return (
    <KeywordContext.Provider value={context}>
      <LocalFilter
        search={searchText}
        statusList={statusList}
        status={selectedStatus}
        onChangeStatus={onChangeStatus}
        onChangeSearchText={onSearch}
        onAddKeyword={onAddKeyword}
      />
      {selectedKeywords.length > 0 && (
        <BulkActionsUI
          textSelected={`${selectedKeywords.length} keywords selected`}
          buttons={buttons}
        />
      )}
      <TableUI
        columns={tableHeaders}
        rows={keywords}
        resultTotal={pagination.item_count}
        page={pagination.page}
        pageSize={pagination.limit}
        selectedIds={selectedIds}
        getRowId={getRowId}
        noData="No data"
        onSelectItem={handleSelectItem}
        onSelectAll={handleSelectAll}
        onSort={onSorting}
        onChangePage={onChangePage}
        onChangePageSize={onChangePageSize}
      />
    </KeywordContext.Provider>
  );
};

KeywordView.defaultProps = {
  keywords: [],
  pagination: {
    page: 1,
    limit: 10,
    item_count: 0,
    page_count: 1,
  },
};
