import React from 'react';
import { BulkActionsUI } from '../common/bulk-actions';
import { ButtonUI } from '../common/button';
import { ButtonFileDownload } from '../common/button/button-file-download';
import { LocalFilter } from '../common/local-filter';
import { TableUISticky as TableUI } from '../common/table';

export const ShopAds = (props: any) => {
  const {
    tableHeaders,
    statusList,
    selectedStatus,
    searchText,
    shops,
    pagination,
    updateSelectedShops,
    selectedIds,
    handleSelectItem = (row, checked: boolean) => {
      return Promise.resolve(checked);
    },
    handleSelectAll = (checked: boolean) => {
      console.info({ checked });
      return Promise.resolve(checked);
    },
    getRowId = (row: any) => row.Shop_eid,
    onSearch,
    onChangeStatus,
    onSorting,
    onChangePage,
    onChangePageSize,
    handleActivator,
    handleOpenModifyBudget,
    openModalAddRule,
    handleDownloadFile,
    handleGetExportFileName,
    handleDeleteRules,
  } = props;

  const handleSelect = (rows: any) => {
    updateSelectedShops(rows);
  };

  const buttons = [
    <ButtonUI
      onClick={openModalAddRule}
      label="Add Rule"
      size="small"
      colorButton="#F6F7F8"
    />,
    <ButtonUI
      onClick={() => handleOpenModifyBudget(selectedIds)}
      label="Modify budget"
      size="small"
      colorButton="#F6F7F8"
    />,
    <ButtonUI
      onClick={() => handleActivator('active', selectedIds)}
      label="Activate"
      size="small"
      colorButton="#F6F7F8"
    />,
    <ButtonUI
      colortext="#D4290D"
      onClick={() => handleActivator('deactive', selectedIds)}
      label="Deactivate"
      size="small"
      colorButton="#F6F7F8"
    />,
    <ButtonUI
      onClick={handleDeleteRules}
      label="Delete rules"
      size="small"
      colorButton="#F6F7F8"
    />,
    <ButtonFileDownload
      downloadFetch={() => handleDownloadFile(selectedIds)}
      getFileName={() => handleGetExportFileName(selectedIds)}
      label="Download file"
      size="small"
      colorButton="#F6F7F8"
    />,
  ];

  return (
    <React.Fragment>
      <LocalFilter
        search={searchText}
        statusList={statusList}
        status={selectedStatus}
        onChangeStatus={onChangeStatus}
        onChangeSearchText={onSearch}
      />
      {selectedIds.length > 0 && (
        <BulkActionsUI
          textSelected={`${selectedIds.length} shops selected`}
          buttons={buttons}
        />
      )}
      <TableUI
        columns={tableHeaders}
        rows={shops}
        resultTotal={pagination.itemCount}
        page={pagination.page}
        pageSize={pagination.limit}
        selectedIds={selectedIds}
        getRowId={getRowId}
        onSelect={handleSelect}
        onSelectItem={handleSelectItem}
        onSelectAll={handleSelectAll}
        onSort={onSorting}
        onChangePage={onChangePage}
        onChangePageSize={onChangePageSize}
        enabledSticky={true}
      />
    </React.Fragment>
  );
};

ShopAds.defaultProps = {
  shops: [],
  pagination: {
    page: 1,
    limit: 10,
    itemCount: 0,
    pageCount: 1,
  },
};
