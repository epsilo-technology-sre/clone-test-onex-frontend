import React from 'react';
import { BulkActionsUI } from '../common/bulk-actions';
import { ButtonUI } from '../common/button';
import { ButtonFileDownload } from '../common/button/button-file-download';
import { LocalFilter } from '../common/local-filter';
import { TableUISticky as TableUI } from '../common/table';
import { CampaignContext } from './campaign-context';

export const CampaignView = (props: any) => {
  const {
    tableHeaders,
    statusList,
    selectedStatus,
    searchText,
    campaigns,
    pagination,
    updateSelectedCampaigns,
    selectedCampaigns = [],
    selectedIds,
    handleSelectItem = (row, checked: boolean) => {
      return Promise.resolve(checked);
    },
    handleSelectAll = (checked: boolean) => {
      console.info({ checked });
      return Promise.resolve(checked);
    },
    getRowId = (row: any) => row.campaign_eid,
    onSearch,
    onChangeStatus,
    onSorting,
    onChangePage,
    onChangePageSize,
    handleActivator,
    handleOpenModifyBudget,
    updateCampaign,
    handleDownloadFile,
    handleGetFileExportName,
  } = props;

  const context = {
    checkExistCampaignName: (value: any) => {
      console.log('Check exist campaign name', value);
    },
    updateCampaignName: async (value: any) => {
      return updateCampaign('name', value);
    },
    updateCampaignBudget: (value: any) => {
      return updateCampaign('budget', value);
    },
    updateCampaignTimeline: async (value: any) => {
      return updateCampaign('timeline', value);
    },
    updateBiddingPrice: async (value: any) => {
      return updateCampaign('biddingPrice', value);
    },
  };

  const handleSelect = (rows: any) => {
    updateSelectedCampaigns(rows);
  };

  const buttons = [
    <ButtonUI
      onClick={() => handleOpenModifyBudget(selectedCampaigns)}
      label="Modify budget"
      size="small"
      colorButton="#F6F7F8"
    />,
    <ButtonUI
      onClick={() => handleActivator('active', selectedCampaigns)}
      label="Activate"
      size="small"
      colorButton="#F6F7F8"
    />,
    <ButtonUI
      colortext="#D4290D"
      onClick={() => handleActivator('deactive', selectedCampaigns)}
      label="Deactivate"
      size="small"
      colorButton="#F6F7F8"
    />,
    <ButtonFileDownload
      colorButton="#F6F7F8"
      downloadFetch={() => handleDownloadFile(selectedCampaigns)}
      getFileName={() => handleGetFileExportName(selectedCampaigns)}
      label="Download File"
      size="small"
    />,
  ];

  return (
    <CampaignContext.Provider value={context}>
      <LocalFilter
        search={searchText}
        statusList={statusList}
        status={selectedStatus}
        onChangeStatus={onChangeStatus}
        onChangeSearchText={onSearch}
      />
      {selectedCampaigns.length > 0 && (
        <BulkActionsUI
          textSelected={`${selectedCampaigns.length} campaigns selected`}
          buttons={buttons}
        />
      )}
      <TableUI
        columns={tableHeaders}
        rows={campaigns}
        resultTotal={pagination.item_count}
        page={pagination.page}
        pageSize={pagination.limit}
        selectedIds={selectedIds}
        getRowId={getRowId}
        onSelect={handleSelect}
        onSelectItem={handleSelectItem}
        onSelectAll={handleSelectAll}
        onSort={onSorting}
        onChangePage={onChangePage}
        onChangePageSize={onChangePageSize}
      />
    </CampaignContext.Provider>
  );
};

CampaignView.defaultProps = {
  campaigns: [],
  pagination: {
    page: 1,
    limit: 10,
    item_count: 0,
    page_count: 1,
  },
};
