import {
  DeleteIconCell,
  PercentCell,
  ProductListActionCell,
  SubjectCell,
} from '@ep/shopee/src/components/common/table-cell';
import { MATCH_TYPE_LABEL } from '../../constants';

export const initColumns = (props: any) => {
  const { onAddKeyword, onDelete } = props;

  return [
    {
      Header: 'Keyword',
      id: 'totalKeyword',
      accessor: (row: any) => ({ name: row.keywordName }),
      Cell: SubjectCell,
      disableSortBy: true,
      width: 355,
    },
    {
      Header: 'Match Type',
      id: 'matchType',
      accessor: (row: any) => MATCH_TYPE_LABEL[row.matchType],
      width: 245,
    },
    {
      Header: 'Bidding price',
      id: 'pricePostsub',
      accessor: (row: any) => ({
        number: row.biddingPrice,
        currency: row.currency || 'VND',
      }),
      Cell: PercentCell,
      width: 210,
    },
    {
      Header: '',
      id: 'action',
      accessor: (row: any) => ({
        item: row,
        onDelete,
      }),
      width: 200,
      Cell: DeleteIconCell,
      disableSortBy: true,
    },
  ];
};
