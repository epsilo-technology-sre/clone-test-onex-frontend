import { Grid, TextField, Typography } from '@material-ui/core';
import React from 'react';
import { AutoCompleteUI, useStyle } from './common';

export const ShopAndAdsSelector = (props: any) => {
  const classes = useStyle();
  const {
    shops = [],
    shopAds = [],
    selectedShopId,
    selectedShopAdsId,
    onChangeShop,
    onChangeShopAds,
  } = props;

  const [selectingShopAds, setSelectingShopAds] = React.useState(
    null,
  );

  React.useEffect(() => {
    setSelectingShopAds(null);
  }, [selectedShopId]);

  React.useEffect(() => {
    setSelectingShopAds(
      (shopAds || []).find((i) => i.shopAdsId === selectedShopAdsId),
    );
  }, [selectedShopAdsId]);

  const handleChangeShop = (event: any, value: any) => {
    onChangeShop(value);
  };

  const handleChangeShopAds = (event: any, value: any) => {
    onChangeShopAds(value);
  };

  return (
    <Grid container spacing={1}>
      <Grid item xs={6}>
        <Typography variant="subtitle2" className={classes.label}>
          Shop
        </Typography>
        <AutoCompleteUI
          id="shop-selector"
          options={shops}
          defaultValue={(shops || []).find(
            (i) => i.id === selectedShopId,
          )}
          getOptionLabel={(option) => option.text}
          renderInput={(params) => (
            <TextField {...params} variant="outlined" />
          )}
          onChange={handleChangeShop}
        />
      </Grid>
      <Grid item xs={6}>
        <Typography variant="subtitle2" className={classes.label}>
          Shop Ads
        </Typography>
        <AutoCompleteUI
          id="campaign-selector"
          options={shopAds}
          defaultValue={(shopAds || []).find(
            (i) => i.shopAdsId === selectedShopAdsId,
          )}
          getOptionLabel={(option) => option.shopAdsName}
          renderInput={(params) => (
            <TextField {...params} variant="outlined" />
          )}
          onChange={handleChangeShopAds}
        />
      </Grid>
    </Grid>
  );
};
