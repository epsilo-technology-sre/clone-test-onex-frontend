import {
  Button,
  Select,
  makeStyles,
  OutlinedInput,
  withStyles,
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { COLORS } from '@ep/shopee/src/constants';

export const InputUI = withStyles({
  root: {
    fontSize: 14,
    '& fieldset': {
      borderColor: '#C2C7CB',
      borderWidth: 2,
    },
    '&.Mui-focused fieldset': {
      borderColor: '#485764!important',
    },
  },
  input: {
    paddingTop: 8,
    paddingBottom: 8,
  },
})(OutlinedInput);

export const AutoCompleteUI = withStyles({
  inputRoot: {
    paddingTop: '6px !important',
    paddingBottom: '6px !important',
  },
  input: {
    paddingTop: '0 !important',
    paddingBottom: '0 !important',
  },
})(Autocomplete);

export const SelectUI = withStyles({
  root: {
    padding: `0.5rem 1rem`,
    fontSize: 14,
    '& .MuiOutlinedInput-notchedOutline': {
      border: 'none',
      background: 'red',
    },
    fieldset: {
      background: 'green',
    },
  },
  selectMenu: {
    paddingTop: 7,
    paddingBottom: 7,
    textTransform: 'capitalize',
    fontSize: 14,
  },
})(Select);

export const ButtonLogin = withStyles({
  root: {
    backgroundColor: '#ED5C10',
    color: '#fff',
    paddingTop: 10,
    paddingBottom: 10,
    textTransform: 'none',
    boxShadow: 'none',
    '&:hover': {
      backgroundColor: '#F17D40',
      borderColor: '#F17D40',
      boxShadow: 'none',
    },
  },
})(Button);

export const useStyle = makeStyles(() => ({
  root: {
    width: 480,
    margin: '0 auto',
  },
  logo: {
    height: 40,
  },
  title: {
    fontSize: 35,
  },
  row: {
    marginBottom: 24,
    width: '100%',
  },
  form: {},
  link: {
    color: '#ED5C10',
    '&:hover': {
      color: '#ED5C10',
    },
  },
  linkGroup: {
    textAlign: 'center',
    color: '#ED5C10',
  },
  label: {
    color: '#596772',
    fontSize: 11,
    fontWeight: 'bold',
    padding: '5px 0',
  },
  error: {
    color: COLORS.COMMON.RED,
    fontSize: 12,
  },
  switcher: {
    display: 'flex',
    alignItems: 'center',
    padding: '8px 0',
    '& img': {
      cursor: 'pointer',
      marginRight: 8,
    },
  },
  selectWrapper: {
    background: '#F6F7F8',
    borderRadius: 4,
  },
}));
