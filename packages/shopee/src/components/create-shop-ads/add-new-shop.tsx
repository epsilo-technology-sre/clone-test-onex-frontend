import SwitchOffIcon from '@ep/one/src/images/switch-off.svg';
import SwitchOnIcon from '@ep/one/src/images/switch-on.svg';
import {
  Box,
  FormHelperText,
  InputAdornment,
  InputBase,
  makeStyles,
  MenuItem,
  Select,
  TextField,
  withStyles,
} from '@material-ui/core';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import moment from 'moment';
import React from 'react';
import styled from 'styled-components';
import * as Yup from 'yup';
import {
  COLORS,
  DATE_FORMAT,
  SHOP_ADS_DAILY_BUDGET,
  SHOP_ADS_TOTAL_BUDGET,
} from '../../constants';
import { formatCurrency } from '../../utils/utils';
import { ButtonUI } from '../common/button';
import { CurrencyCode } from '../common/currency-code';
import { SingleDatePickerUI } from '../common/single-date-picker';
import { InputUI } from '../create-campaign/common';

type SetupNewShopProps = {
  subcriptionCode: string;
  channel: string;
  country: string;
  userName: string;
  password: string;
  onSubmit: (form: any) => void;
  checkSubcriptionCodeExists: (code: string) => Promise<boolean>;
};

const validationSchema = Yup.object().shape({
  shop: Yup.string().required('Please select shop'),
  adsName: Yup.string().required('Please enter your ads'),
  currency: Yup.string().optional(),
  budget: Yup.object().shape({
    total: Yup.number()
      .test(
        'should-not-contains-e',
        'Invalid number',
        (v) => !String(v).includes('e'),
      )
      .when(
        ['isNoLimit', 'minTotalBiddingPrice'],
        (isNoLimit, minTotalBiddingPrice, schema) => {
          if (isNoLimit) {
            return Yup.number().optional();
          } else {
            return schema.min(
              minTotalBiddingPrice,
              `Total budget must be higher ${formatCurrency(
                minTotalBiddingPrice,
              )}`,
            );
          }
        },
      ),
    daily: Yup.number()
      .when(
        ['isOnDaily', 'minDailyBiddingPrice'],
        (isOnDaily, minDailyBiddingPrice, schema) => {
          if (isOnDaily) {
            return schema.min(
              minDailyBiddingPrice,
              `Daily budget must be higher ${formatCurrency(
                minDailyBiddingPrice,
              )}`,
            );
          } else {
            return Yup.number().optional();
          }
        },
      )
      .when(['isNoLimit', 'isOnDaily'], {
        is: (isNoLimit, isOnDaily) => {
          return !isNoLimit && isOnDaily;
        },
        then: Yup.number().lessThan(
          Yup.ref('total'),
          'Daily Budget must be lower than Total Budget.',
        ),
      }),
  }),
});

export function AddNewShop(props: any) {
  const { shops, onSubmit, defaultAdInfo } = props;
  const classes = useStyle();

  const [minEndDate, setMinEndDate] = React.useState(
    moment().format(DATE_FORMAT),
  );

  const handleChangeStartDate = (formik: any, date: any) => {
    formik.setFieldValue('timeline.fromDate', date);
    setMinEndDate(date);

    if (
      moment(date, DATE_FORMAT).diff(
        moment(formik.values.timeline.toDate, DATE_FORMAT),
      ) > 0
    ) {
      formik.setFieldValue('timeline.toDate', date);
    }
  };

  return (
    <>
      <Title>Add New Shop</Title>
      <ShopContainer>
        <Formik
          validationSchema={validationSchema}
          initialValues={
            defaultAdInfo || {
              shop: '',
              adsName: '',
              currency: '',
              budget: {
                total: 0,
                daily: 0,
                isNoLimit: true,
                isOnDaily: false,
                minTotalBiddingPrice: 0,
                minDailyBiddingPrice: 0,
              },
              timeline: {
                fromDate: moment().format(DATE_FORMAT),
                toDate: moment().format(DATE_FORMAT),
                isOnNoLimit: false,
              },
            }
          }
          onSubmit={(values: any) => {
            onSubmit(values);
          }}
          validateOnBlur={true}
        >
          {(formik) => {
            const {
              values,
              values: { timeline, budget },
            } = formik;
            return (
              <Form>
                <TitleSection>Shop Ads information</TitleSection>
                <Wrap>
                  <TitleField>Shop</TitleField>
                  <Field name="shop">
                    {({ field }: any) => {
                      return (
                        <Select
                          id="shop"
                          {...field}
                          onChange={(evt) => {
                            field.onChange(evt);
                            const shop = shops.find(
                              (s) =>
                                s.shopId === Number(evt.target.value),
                            );
                            formik.setFieldValue(
                              'currency',
                              shop.shopCurrency,
                            );
                            formik.setFieldValue(
                              'budget.minTotalBiddingPrice',
                              SHOP_ADS_TOTAL_BUDGET[shop.shopCurrency]
                                .MIN,
                            );
                            formik.setFieldValue(
                              'budget.minDailyBiddingPrice',
                              SHOP_ADS_DAILY_BUDGET[shop.shopCurrency]
                                .MIN,
                            );
                          }}
                          input={<BootstrapInput />}
                          displayEmpty
                          MenuProps={{
                            anchorOrigin: {
                              vertical: 'bottom',
                              horizontal: 'left',
                            },
                            transformOrigin: {
                              vertical: 'top',
                              horizontal: 'left',
                            },
                            getContentAnchorEl: null,
                          }}
                        >
                          <MenuItem value={''} disabled>
                            Choose shop
                          </MenuItem>
                          {shops.map((shop: any) => (
                            <MenuItem
                              value={shop.shopId}
                              key={shop.shopId}
                            >
                              {shop.shopName}
                            </MenuItem>
                          ))}
                        </Select>
                      );
                    }}
                  </Field>
                  <ErrorMessage
                    component={FormHelperText}
                    name="shop"
                    className={classes.error}
                  />
                </Wrap>
                <Wrap>
                  <TitleField>Shop Ads Name</TitleField>
                  <Field name="adsName">
                    {({ field }: any) => {
                      return (
                        <TextFieldUI
                          fullWidth
                          placeholder={'Enter ads name'}
                          autoComplete="off"
                          variant="outlined"
                          size="small"
                          {...field}
                        />
                      );
                    }}
                  </Field>
                  <ErrorMessage
                    component={FormHelperText}
                    name="adsName"
                    className={classes.error}
                  />
                </Wrap>
                <TitleSection marginTop={'16px'}>
                  Budgets
                </TitleSection>
                <Wrap>
                  <TitleField>Total budget</TitleField>
                  <Field name="budget.total">
                    {({ field, form }: any) => {
                      return (
                        <InputUI
                          id="total-budget"
                          type="number"
                          disabled={formik.values.budget.isNoLimit}
                          fullWidth
                          {...field}
                          startAdornment={
                            <InputAdornment position="start">
                              <CurrencyCode
                                currency={formik.values.currency}
                              />
                            </InputAdornment>
                          }
                          error={!!formik.errors.budget?.total}
                          autoComplete="off"
                        />
                      );
                    }}
                  </Field>
                  <ErrorMessage
                    component={FormHelperText}
                    name="budget.total"
                    className={classes.error}
                  />
                </Wrap>
                <Field name="budget.isNoLimit">
                  {({ field }: any) => {
                    return (
                      <Box className={classes.switcher}>
                        <img
                          src={
                            field.value ? SwitchOnIcon : SwitchOffIcon
                          }
                          height={16}
                          width={30}
                          onClick={() => {
                            formik.setFieldValue(
                              field.name,
                              !field.value,
                            );
                          }}
                        />
                        <TitleField>No limit</TitleField>
                      </Box>
                    );
                  }}
                </Field>
                <Field name="budget.isOnDaily">
                  {({ field }: any) => {
                    return (
                      <Box className={classes.switcher}>
                        <img
                          src={
                            field.value ? SwitchOnIcon : SwitchOffIcon
                          }
                          height={16}
                          width={30}
                          onClick={() => {
                            formik.setFieldValue(
                              'budget.isOnDaily',
                              !field.value,
                            );
                          }}
                          alt={'checkbox'}
                        />
                        <TitleField>Daily budget</TitleField>
                      </Box>
                    );
                  }}
                </Field>
                {budget.isOnDaily && (
                  <Wrap>
                    <TitleField>Daily budget</TitleField>
                    <Field name="budget.daily">
                      {({ field }: any) => {
                        return (
                          <InputUI
                            id="daily-budget"
                            type="number"
                            fullWidth
                            {...field}
                            startAdornment={
                              <InputAdornment position="start">
                                <CurrencyCode
                                  currency={formik.values.currency}
                                />
                              </InputAdornment>
                            }
                            error={!!formik.errors.budget?.daily}
                            autoComplete="off"
                          />
                        );
                      }}
                    </Field>
                    <ErrorMessage
                      component={FormHelperText}
                      name="budget.daily"
                      className={classes.error}
                    />
                  </Wrap>
                )}
                <TitleSection marginTop={'16px'}>
                  Timeline
                </TitleSection>
                <Wrap>
                  <TitleField>From date</TitleField>
                  <Field name="timeline.fromDate">
                    {({ field, form }: any) => {
                      return (
                        <SingleDatePickerUI
                          disabled={timeline.isOnNoLimit}
                          date={field.value}
                          minDate={moment().format(DATE_FORMAT)}
                          onChange={(date: any) =>
                            handleChangeStartDate(form, date)
                          }
                        />
                      );
                    }}
                  </Field>
                </Wrap>
                <Wrap>
                  <TitleField>To date</TitleField>
                  <Field name="timeline.toDate">
                    {({ field, form }: any) => {
                      return (
                        <SingleDatePickerUI
                          disabled={timeline.isOnNoLimit}
                          date={field.value}
                          minDate={minEndDate}
                          onChange={(date: any) =>
                            form.setFieldValue(field.name, date)
                          }
                        />
                      );
                    }}
                  </Field>
                  <Field name="timeline.isOnNoLimit">
                    {({ field }: any) => {
                      return (
                        <Box className={classes.switcher}>
                          <img
                            src={
                              field.value
                                ? SwitchOnIcon
                                : SwitchOffIcon
                            }
                            height={16}
                            width={30}
                            onClick={() => {
                              formik.setFieldValue(
                                'timeline.isOnNoLimit',
                                !field.value,
                              );
                            }}
                            alt={'checkbox'}
                          />
                          <TitleField>No limit</TitleField>
                        </Box>
                      );
                    }}
                  </Field>
                </Wrap>
                <ButtonUI
                  disabled={false}
                  type={'submit'}
                  fullWidth
                  label={'Go to setup keywords'}
                />
              </Form>
            );
          }}
        </Formik>
      </ShopContainer>
    </>
  );
}

const Title = styled.h1`
  font-style: normal;
  font-weight: 600;
  font-size: 29px;
  line-height: 32px;
  color: #253746;
  margin-bottom: 54px;
`;

const TitleSection = styled.p`
  font-weight: 600;
  font-size: 16px;
  line-height: 20px;
  color: #253746;
  margin-bottom: 16px;
  margin-top: ${({ marginTop }: any) => marginTop || 0};
`;

const TitleField = styled.p`
  font-weight: bold;
  font-size: 11px;
  line-height: 16px;
  color: #596772;
  margin-bottom: 4px;
  margin-top: 0;
`;

const ShopContainer = styled.div`
  background: #fff;
  max-width: 50%;
  margin: auto;
`;

const Wrap = styled.div`
  margin-bottom: 20px;
`;

const BootstrapInput = withStyles({
  root: {
    width: '100%',
  },
  disabled: {
    color: '#9FA7AE !important',
    background: '#F6F7F8 !important',
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: '#ffffff',
    border: '2px solid #C2C7CB',
    fontSize: 14,
    color: '#253746',
    padding: '8px 26px 9px 8px',
  },
})(InputBase);

const TextFieldUI = withStyles(() => ({
  root: {
    '& .MuiOutlinedInput-notchedOutline': {
      borderWidth: 2,
    },
  },
}))(TextField);

const useStyle = makeStyles({
  error: {
    color: COLORS.COMMON.RED,
    fontSize: 12,
  },
  switcher: {
    display: 'flex',
    alignItems: 'center',
    padding: '8px 0',
    '& img': {
      cursor: 'pointer',
      marginRight: 8,
    },
  },
  label: {
    color: '#596772',
    fontSize: 11,
    fontWeight: 'bold',
    padding: '5px 0',
  },
});
