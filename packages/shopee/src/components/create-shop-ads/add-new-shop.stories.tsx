import React from 'react';
import Paper from '@material-ui/core/Paper';
import { AddNewShop } from './add-new-shop';
import { AdCreative } from './ad-creative';
import { SetupKeyword } from './setup-keyword';
import { CreateShopAdsAddKeywords } from './add-keyword-ads';
import { initColumns } from './add-keyword-columns';
import { actions } from '../../redux/create-campaign/actions';

export default {
  title: 'Shopee/Create shop ads',
};

const dataShop = Array.from({ length: 10 }, (_, key) => ({
  key,
  shopName: `shop name ${key}`,
  shopId: `shop_id${key}`,
  products: Math.floor(Math.random() * 10 + 1),
}));


export const step1 = () => {
  return (
    <Paper>
      <AddNewShop
        shops={dataShop}
        currency={'VND'}
      />
    </Paper>
  );
};

export const step2 = () => {
  return (
    <Paper>
      <AdCreative shops={dataShop} />
    </Paper>
  );
};

export const step3 = () => {
  return (
    <Paper>
      <SetupKeyword />
    </Paper>
  );
};


export const step4 = () => {
  const pagination = {
    total: 200,
    currentPage: 1,
    pageSize: 1,
  }
  const handleAddKeywordsToBucket = (keywords: any[]) => {
    console.log('handleAddKeywordsToBucket', { keywords });
    // dispatch(actions.addKeywordToBucket({ keywords }));
  };

  const headers = React.useMemo(
    () =>
      initColumns({
        onAddToBucket: (value: any) =>
          handleAddKeywordsToBucket([value]),
      }),
    [],
  );


  return (
    <Paper>
      <CreateShopAdsAddKeywords tableHeaders={headers} pagination={pagination} openModalAddKeyword={true} />
    </Paper>
  );
};
