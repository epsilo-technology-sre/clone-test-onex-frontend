import { AddCustomKeyword } from '@ep/shopee/src/components/add-product-keyword/add-custom-keyword';
import { KeywordTable } from '@ep/shopee/src/components/add-product-keyword/keyword-table';
import { KeywordTableAction } from '@ep/shopee/src/components/add-product-keyword/keyword-table-action';
import {
  ButtonTextUI,
  ButtonUI,
} from '@ep/shopee/src/components/common/button';
import { ModifyKeyWords } from '@ep/shopee/src/components/common/modify-keywords';
import { SelectedKeyword } from '@ep/shopee/src/components/common/selected-keyword-no-product';
import {
  Box,
  Dialog,
  Divider,
  Grid,
  makeStyles,
  Typography,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import React, { useEffect, useState } from 'react';
import { SHOP_ADS_CURRENCY_LIMIT } from '../../constants';
import { ShopAndAdsSelector } from './shop-n-ads-selectors';

const useStyle = makeStyles({
  root: {
    background: '#ffffff',
    padding: 16,
  },
  dialogPaper: {
    maxHeight: 'calc(100% - 32px)',
  },
});

export const ExistingShopAdsAddKeywords = (props: any) => {
  const classes = useStyle();

  const {
    tableHeaders,
    hasPreviousStep,
    currency,
    noMatchType,
    keywords = [],
    selectedKeywords,
    searchText,
    pagination,
    keywordBucket = [],
    shopList,
    shopAdsList,
    selectedShopId,
    selectedShopAdsId,
    onChangeShop,
    onChangeShopAds,
    onUpdateSelectedKeywordPrice,
    onSearch,
    onAddKeywordsToBucket,
    onRemoveKeywordFromBucket,
    onResetKeywordBucket,
    onAddCustomKeywords,
    onSelectKeywordItem,
    onSelectAllKeywordItems,
    onChangePagination,
    onBackToPrevious,
    onSave,
    onCancel,
    openModalAddKeyword,
  } = props;

  const [selectedIds, setSelectedIds] = useState([]);

  const [isShowCustomKeyword, setIsShowCustomKeyword] = useState(
    false,
  );

  const [sortBy, setSortBy] = useState([]);

  useEffect(() => {
    setSelectedIds(
      selectedKeywords.map((item: any) => item.keywordId),
    );
  }, [selectedKeywords]);

  const handleAddCustomKeyword = (keywordNames: string[]) => {
    setIsShowCustomKeyword(!isShowCustomKeyword);
    onAddCustomKeywords(keywordNames, currency);
  };

  const handleSorting = (sortBy: any) => {
    setSortBy(sortBy);
  };

  const handleSearch = (value: any) => {
    if (value !== searchText) {
      onSearch(value, currency);
    }
  };

  const handleChangePage = (page: number) => {
    onChangePagination({
      ...pagination,
      currentPage: page,
    });
  };

  const handleChangePageSize = (value: number) => {
    onChangePagination({
      ...pagination,
      pageSize: value,
      currentPage: 1,
    });
  };

  const getRowId = (row: any) => row.keywordId;

  const columns = React.useMemo(() => {
    return tableHeaders;
  }, [tableHeaders]);

  return (
    <Dialog
      open={openModalAddKeyword}
      fullWidth
      maxWidth={'xl'}
      classes={{ paper: classes.dialogPaper }}
    >
      <Box className={classes.root}>
        <Grid container justify="space-between">
          <Grid item>
            <Typography variant="h6">Add Keywords</Typography>
          </Grid>
          <Grid item>
            <CloseIcon
              style={{ cursor: 'pointer' }}
              onClick={onCancel}
            />
          </Grid>
        </Grid>
        <Grid container>
          <Grid item xs={9}>
            <Box mb={2}>
              <Box mb={2}>
                <ShopAndAdsSelector
                  shops={shopList}
                  shopAds={shopAdsList}
                  selectedShopId={selectedShopId}
                  selectedShopAdsId={selectedShopAdsId}
                  onChangeShop={onChangeShop}
                  onChangeShopAds={onChangeShopAds}
                />
              </Box>
              <Divider />
            </Box>

            <Box px={2}>
              <Box py={2}>
                <ModifyKeyWords
                  currency={currency}
                  noMatchType={noMatchType}
                  currencyLimit={SHOP_ADS_CURRENCY_LIMIT}
                  disabled={selectedIds.length === 0}
                  onSubmit={onUpdateSelectedKeywordPrice}
                />
              </Box>
              <Box py={2}>
                <KeywordTableAction
                  searchText={searchText}
                  disableAddAll={selectedIds.length === 0}
                  onSearch={handleSearch}
                  onClickAddCustomKeyword={() =>
                    setIsShowCustomKeyword(!isShowCustomKeyword)
                  }
                  onClickAddAll={() =>
                    onAddKeywordsToBucket(selectedKeywords)
                  }
                />
              </Box>
              {isShowCustomKeyword && (
                <Box py={1}>
                  <AddCustomKeyword
                    onApply={handleAddCustomKeyword}
                  />
                </Box>
              )}
              <Box py={2}>
                <KeywordTable
                  columns={columns}
                  rows={keywords}
                  selectedIds={selectedIds}
                  resultTotal={pagination.total}
                  page={pagination.currentPage}
                  pageSize={pagination.pageSize}
                  getRowId={getRowId}
                  onSelectItem={onSelectKeywordItem}
                  onSelectAll={onSelectAllKeywordItems}
                  onSort={handleSorting}
                  onChangePage={handleChangePage}
                  onChangePageSize={handleChangePageSize}
                />
              </Box>
            </Box>
          </Grid>
          <Grid item xs={3}>
            <Box py={2}>
              <SelectedKeyword
                noMatchType={noMatchType}
                keywords={keywordBucket}
                onRemoveItem={onRemoveKeywordFromBucket}
                onRemoveAll={onResetKeywordBucket}
              />
            </Box>
          </Grid>
        </Grid>
        <Box>
          <Grid
            container
            alignItems="center"
            justify="space-between"
            spacing={1}
          >
            <Grid item>
              {hasPreviousStep && (
                <ButtonTextUI
                  size="small"
                  colortext="#000"
                  label="Back to previous step"
                  onClick={onBackToPrevious}
                />
              )}
            </Grid>
            <Grid item>
              <ButtonTextUI
                size="small"
                colortext="#000"
                label="Cancel"
                style={{ marginRight: 8 }}
                onClick={onCancel}
              />
              <ButtonUI
                size="small"
                label="Add keyword"
                disabled={
                  keywordBucket.length === 0 ||
                  !selectedShopId ||
                  !selectedShopAdsId
                }
                onClick={onSave}
              />
            </Grid>
          </Grid>
        </Box>
      </Box>
    </Dialog>
  );
};
