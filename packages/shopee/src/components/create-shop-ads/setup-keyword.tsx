import React, { useState } from 'react';
import {
  Box,
  makeStyles,
  InputAdornment,
  Typography,
  Grid,
} from '@material-ui/core';
import styled from 'styled-components';
import SearchIcon from '@material-ui/icons/Search';
import { ButtonTextUI, ButtonUI } from '../common/button';
import { COLORS } from '../../constants';
import { InputUI } from '../create-campaign/common';
import NoProduct from '../../images/no-product.svg';
import { TableUI } from '../common/table';
import { initColumns } from './shop-ads-columns';

type SetupKeywordProps = {
  keywords: any[];
  selectedIds: any[];
  getRowId: (row: any) => any;
  pageSize: number;
  page: number;
  resultTotal: number;
  openModalAddKeywords: () => void;
  onSearch: (searchText: string) => void;
  onSelectRowItem: (id: number, checked: boolean) => void;
  onSelectAllItem: (checked: boolean) => void;
  onRemoveKeyword: (id: number) => void;
  onChangePage: (page: number) => void;
  onChangePageSize: (pageSize: number) => void;
  onBackToPreviousStep: () => void;
  onSave: () => void;
};

export function SetupKeyword(props: SetupKeywordProps) {
  const [sortBy, setSortBy] = useState([]);
  const classes = useStyle();

  const handleSearchKeyUp = (event: any) => {
    if (event.key === 'Enter') {
      console.log('event.target.value', {
        value: event.target.value,
      });
      props.onSearch(event.target.value);
    }
  };

  const handleSorting = (sortBy: any) => {
    setSortBy(sortBy);
  };

  const headers = React.useMemo(
    () =>
      initColumns({
        onDelete: (value: any) => {
          props.onRemoveKeyword(value.keywordId as number);
        },
      }),
    [],
  );

  return (
    <>
      <Title>Ad Creative</Title>
      <Box mb={2} display="flex" justifyContent="space-between">
        <InputUI
          placeholder="Search"
          onKeyUp={handleSearchKeyUp}
          startAdornment={
            <InputAdornment position="start">
              <SearchIcon fontSize="small" />
            </InputAdornment>
          }
          style={{ width: 320 }}
          labelWidth={0}
          autoComplete="off"
        />
        <ButtonUI
          onClick={() => {props.openModalAddKeywords()} }
          label="Add keyword"
          size="small"
          colorButton="#F6F7F8"
        />
      </Box>
      <TableUI
        columns={headers}
        rows={props.keywords}
        onSelectItem={props.onSelectRowItem}
        onSelectAll={props.onSelectAllItem}
        selectedIds={[]}
        getRowId={props.getRowId}
        onSort={handleSorting}
        resultTotal={props.resultTotal}
        page={props.page}
        pageSize={props.pageSize}
        onChangePage={props.onChangePage}
        onChangePageSize={props.onChangePageSize}
        noData={
          <Box py={6}>
            <img src={NoProduct} width="200" height="200" />
            <Typography variant="h6" style={{ margin: 'auto' }}>
              Add your amazing keywords
            </Typography>
            <Typography
              variant="subtitle2"
              style={{ margin: '16px auto' }}
            >
              Here’s where you would add keyword to Shop Ads
            </Typography>
            <ButtonUI
              size="small"
              label="Add keyword"
              onClick={props.openModalAddKeywords}
            />
          </Box>
        }
      />

      <Box py={2}>
        <Grid
          container
          alignItems="center"
          justify="space-between"
          spacing={1}
        >
          <Grid item>
            <ButtonUI
              size="small"
              label="Back to previous step"
              colorButton={COLORS.COMMON.GRAY}
              onClick={() => props.onBackToPreviousStep()}
            ></ButtonUI>
          </Grid>
          <Grid item>
            <ButtonUI
              disabled={props.keywords.length === 0}
              size="small"
              label="Create Shop Ads"
              onClick={() => props.onSave()}
            ></ButtonUI>
          </Grid>
        </Grid>
      </Box>
    </>
  );
}

const Title = styled.h1`
  font-style: normal;
  font-weight: 600;
  font-size: 29px;
  line-height: 32px;
  color: #253746;
  margin-bottom: 54px;
`;

const useStyle = makeStyles({
  error: {
    color: COLORS.COMMON.RED,
    fontSize: 12,
  },
  switcher: {
    display: 'flex',
    alignItems: 'center',
    padding: '8px 0',
    '& img': {
      cursor: 'pointer',
      marginRight: 8,
    },
  },
  label: {
    color: '#596772',
    fontSize: 11,
    fontWeight: 'bold',
    padding: '5px 0',
  },
});
