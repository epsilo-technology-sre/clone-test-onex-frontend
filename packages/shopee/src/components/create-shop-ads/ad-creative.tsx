import React from 'react';
import {
  Grid,
  Box,
  MenuItem,
  Select,
  TextField,
  withStyles,
  InputBase,
  FormHelperText,
  makeStyles,
} from '@material-ui/core';
import styled from 'styled-components';
import { ButtonUI } from '../common/button';
import * as Yup from 'yup';
import { Field, Form, Formik, ErrorMessage } from 'formik';
import { COLORS } from '../../constants';
import SwitchOnIcon from '@ep/one/src/images/switch-on.svg';
import SwitchOffIcon from '@ep/one/src/images/switch-off.svg';
import NewTab from '../../images/new-tab.svg';

const validationSchema = Yup.object().shape({
  shopCategory: Yup.string()
    .when('isMainShop', {
      is: true,
      then: Yup.string().optional(),
    })
    .when('isMainShop', {
      is: false,
      then: Yup.string().required('Please select shop category'),
    }),
  tagline: Yup.string().required('Please enter Tagline'),
});

export function AdCreative(props: any) {
  const { categoryList, adsCreative, onSubmit } = props;
  const classes = useStyle();

  return (
    <>
      <Title>Ad Creative</Title>
      <ShopContainer>
        <Formik
          validationSchema={validationSchema}
          initialValues={
            adsCreative || {
              shopCategory: '',
              tagline: '',
              isMainShop: true,
            }
          }
          onSubmit={(values: any) => {
            console.log('values', values);
            onSubmit(values);
          }}
          validateOnBlur={true}
        >
          {(formik: any) => {
            const { values } = formik;
            return (
              <Form>
                <TitleSection>Edit Creative</TitleSection>
                <Field name="isMainShop">
                  {({ field }: any) => {
                    return (
                      <Box className={classes.switcher}>
                        <img
                          src={
                            field.value ? SwitchOnIcon : SwitchOffIcon
                          }
                          height={16}
                          width={30}
                          onClick={() => {
                            formik.setFieldValue(
                              field.name,
                              !field.value,
                            );
                          }}
                        />
                        <TitleField>No limit</TitleField>
                      </Box>
                    );
                  }}
                </Field>
                <Wrap>
                  <TitleField>Shop Category</TitleField>
                  <Field name="shopCategory">
                    {({ field, form }: any) => {
                      return (
                        <Select
                          id="shopCategory"
                          {...field}
                          disabled={form.values.isMainShop}
                          input={<BootstrapInput />}
                          displayEmpty
                          MenuProps={{
                            anchorOrigin: {
                              vertical: 'bottom',
                              horizontal: 'left',
                            },
                            transformOrigin: {
                              vertical: 'top',
                              horizontal: 'left',
                            },
                            getContentAnchorEl: null,
                          }}
                        >
                          <MenuItem value={''} disabled>
                            Choose Shop Category
                          </MenuItem>
                          {categoryList.map((shop: any) => (
                            <MenuItem
                              value={shop.shopCatId}
                              key={shop.shopCatId}
                            >
                              <Grid
                                container
                                alignItems={'center'}
                                justify={'space-between'}
                              >
                                <Grid item>
                                  <ShopCategory>
                                    {shop.shopCatName}
                                  </ShopCategory>
                                  <ShopProduct>
                                    {shop.productCount} products
                                  </ShopProduct>
                                </Grid>
                                <Grid item>
                                  <img src={NewTab} />
                                </Grid>
                              </Grid>
                            </MenuItem>
                          ))}
                        </Select>
                      );
                    }}
                  </Field>
                  <ErrorMessage
                    component={FormHelperText}
                    name="shopCategory"
                    className={classes.error}
                  />
                </Wrap>
                <Wrap>
                  <TitleField>Tagline</TitleField>
                  <Field name="tagline">
                    {({ field }: any) => {
                      return (
                        <TextFieldUI
                          fullWidth
                          placeholder={'Enter campaign name'}
                          autoComplete="off"
                          variant="outlined"
                          size="small"
                          {...field}
                        />
                      );
                    }}
                  </Field>
                  <ErrorMessage
                    component={FormHelperText}
                    name="tagline"
                    className={classes.error}
                  />
                </Wrap>
                <ButtonUI
                  disabled={!values.tagline}
                  type={'submit'}
                  fullWidth
                  label={'Next'}
                />
              </Form>
            );
          }}
        </Formik>
      </ShopContainer>
    </>
  );
}

const Title = styled.h1`
  font-style: normal;
  font-weight: 600;
  font-size: 29px;
  line-height: 32px;
  color: #253746;
  margin-bottom: 54px;
`;

const TitleSection = styled.p`
  font-weight: 600;
  font-size: 16px;
  line-height: 20px;
  color: #253746;
  margin-bottom: 16px;
  margin-top: ${({ marginTop }: any) => marginTop || 0};
`;

const TitleField = styled.p`
  font-weight: bold;
  font-size: 11px;
  line-height: 16px;
  color: #596772;
  margin-bottom: 4px;
  margin-top: 0;
`;

const ShopContainer = styled.div`
  background: #fff;
  max-width: 50%;
  margin: auto;
`;

const Wrap = styled.div`
  margin-bottom: 20px;
`;

const ShopCategory = styled.div`
  font-weight: 500;
  font-size: 12px;
  line-height: 16px;
  color: #253746;
`;
const ShopProduct = styled.div`
  font-size: 10px;
  line-height: 12px;
  color: #596772;
`;

const BootstrapInput = withStyles({
  root: {
    width: '100%',
  },
  disabled: {
    color: '#9FA7AE !important',
    background: '#F6F7F8 !important',
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: '#ffffff',
    border: '2px solid #C2C7CB',
    fontSize: 14,
    color: '#253746',
    padding: '8px 26px 9px 8px',
  },
})(InputBase);

const TextFieldUI = withStyles(() => ({
  root: {
    '& .MuiOutlinedInput-notchedOutline': {
      borderWidth: 2,
    },
  },
}))(TextField);

const useStyle = makeStyles({
  error: {
    color: COLORS.COMMON.RED,
    fontSize: 12,
  },
  switcher: {
    display: 'flex',
    alignItems: 'center',
    padding: '8px 0',
    '& img': {
      cursor: 'pointer',
      marginRight: 8,
    },
  },
  label: {
    color: '#596772',
    fontSize: 11,
    fontWeight: 'bold',
    padding: '5px 0',
  },
});
