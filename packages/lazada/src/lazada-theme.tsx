import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#ED5C10',
    },
    secondary: {
      main: '#0BA373',
    }
  },
  typography: {
    fontSize: 14
  },
});

export default theme;
