import { ThemeProvider } from '@material-ui/core/styles';
import React from 'react';
import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import LazadaTheme from './lazada-theme';
import { CampaignPage } from './pages';
import { store } from './redux/store';

export const App = () => {
  return (
    <Provider store={store}>
      <ThemeProvider theme={LazadaTheme}>
        <CampaignPage></CampaignPage>
        <ToastContainer
          closeOnClick={false}
          hideProgressBar={true}
          limit={3}
        />
      </ThemeProvider>
    </Provider>
  );
};

export default App;
