import numeral from 'numeral';

export const formatCurrency = (number, currency) => {
  if (currency === 'VND' || currency === 'IDR') {
    return numeral(number).format('0,0');
  }
  return numeral(number).format('0,0[.]00');
};

export const parseObjectToParam = (obj: any) => {
  let str = '';
  for (let key in obj) {
    if (str != '') {
      str += '&';
    }
    if (obj[key] !== undefined) {
      str += key + '=' + encodeURIComponent(obj[key]);
    }
  }
  return str;
};

export const sortList = (items, property) => {
  return items.sort((a, b) => {
    const textA = (a[property] || '').toUpperCase();
    const textB = (b[property] || '').toUpperCase();
    if (textA < textB) {
      return -1;
    }
    if (textA > textB) {
      return 1;
    }
    return 0;
  });
};
