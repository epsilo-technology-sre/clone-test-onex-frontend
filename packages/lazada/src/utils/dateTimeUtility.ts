import moment from 'moment';

export const isSameDay = (a: any, b: any) => {
  return a.isSame(b, 'day');
};

export const isDiffMonthOrYear = (date: any, anotherDay: any) => {
  return (
    date &&
    anotherDay &&
    (date.month() !== anotherDay.month() ||
      date.year() !== anotherDay.year())
  );
};
