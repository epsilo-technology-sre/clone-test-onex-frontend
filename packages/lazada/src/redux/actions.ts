import moment from 'moment';
import * as eff from 'redux-saga/effects';
import { EP } from '../api/api';
import * as fetch from '../api/fetch';
import { DATE_FORMAT } from '../constants';
import { actionWithRequest, makeAction } from './util';

export const GET_CAMPAIGNS_TYPE = {
  START: 'START_GET_CAMPAIGNS',
  SUCCESS: 'SUCCESS_GET_CAMPAIGNS',
  ERROR: 'ERROR_GET_CAMPAIGNS',
};

export const GET_PRODUCTS_TYPE = {
  START: 'START_GET_PRODUCTS',
  SUCCESS: 'SUCCESS_GET_PRODUCTS',
  ERROR: 'ERROR_GET_PRODUCTS',
};

export const GET_KEYWORDS_TYPE = {
  START: 'START_GET_KEYWORDS',
  SUCCESS: 'SUCCESS_GET_KEYWORDS',
  ERROR: 'ERROR_GET_KEYWORDS',
};

export const STATUS_KEYWORDS_TYPE = {
  START: 'START_GET_STATUS_KEYWORDS',
  SUCCESS: 'SUCCESS_GET_STATUS_KEYWORDS',
  ERROR: 'ERROR_GET_STATUS_KEYWORDS',
};

export const STATUS_CAMPAIGN_TYPE = {
  START: 'START_GET_STATUS_CAMPAIGN',
  SUCCESS: 'SUCCESS_GET_STATUS_CAMPAIGN',
  ERROR: 'ERROR_GET_STATUS_CAMPAIGN',
};

export const STATUS_PRODUCT_TYPE = {
  START: 'START_GET_STATUS_PRODUCT',
  SUCCESS: 'SUCCESS_GET_STATUS_PRODUCT',
  ERROR: 'ERROR_GET_STATUS_PRODUCT',
};

export const GET_CATEGORIES_TYPE = {
  START: 'START_GET_CATEGORIES',
  SUCCESS: 'SUCCESS_GET_CATEGORIES',
  ERROR: 'ERROR_GET_CATEGORIES',
};

export const GET_SHOPS_TYPE = {
  START: 'START_GET_SHOPS',
  SUCCESS: 'SUCCESS_GET_SHOPS',
  ERROR: 'ERROR_GET_SHOPS',
};

export const SET_PAGINATION_TYPE = 'SET_PAGINATION';
export const SET_SEARCH_CAMPAIGN_TYPE = 'SET_SEARCH_CAMPAIGN_TYPE';
export const SET_FILTER_CAMPAIGN_STATUS_TYPE =
  'SET_FILTER_CAMPAIGN_STATUS_TYPE';
export const SET_SEARCH_PRODUCT_TYPE = 'SET_SEARCH_PRODUCT_TYPE';
export const SET_FILTER_PRODUCT_STATUS_TYPE =
  'SET_FILTER_PRODUCT_STATUS_TYPE';
export const SET_SEARCH_KEYWORD_TYPE = 'SET_SEARCH_KEYWORD_TYPE';
export const SET_FILTER_KEYWORD_STATUS_TYPE =
  'SET_FILTER_KEYWORD_STATUS_TYPE';

export const UPDATE_SELECTED_CAMPAIGNS_TYPE =
  'UPDATE_SELECTED_CAMPAIGNS';
export const UPDATE_SELECTED_PRODUCTS_TYPE =
  'UPDATE_SELECTED_PRODUCTS';
export const UPDATE_SELECTED_KEYWORDS_TYPE =
  'UPDATE_SELECTED_KEYWORDS';

export const UPDATE_FILTER_DATE_RANGE_TYPE =
  'UPDATE_FILTER_DATE_RANGE';
export const UPDATE_FILTER_COUNTRY_TYPE = 'UPDATE_FILTER_COUNTRY';
export const UPDATE_FILTER_SHOP_TYPE = 'UPDATE_FILTER_SHOP';

export const CHANGE_TAB_TYPE = 'CHANGE_TAB';

export const GET_CATEGORIES = {
  START: (payload: any) => ({
    type: GET_CATEGORIES_TYPE.START,
    payload,
  }),
  SUCCESS: (payload: any) => ({
    type: GET_CATEGORIES_TYPE.SUCCESS,
    payload,
  }),
  ERROR: (payload: any) => ({
    type: GET_CATEGORIES_TYPE.ERROR,
    payload,
  }),
};

export const GET_SHOPS = {
  START: (payload: any) => ({
    type: GET_SHOPS_TYPE.START,
    payload,
  }),
  SUCCESS: (payload: any) => ({
    type: GET_SHOPS_TYPE.SUCCESS,
    payload,
  }),
  ERROR: (payload: any) => ({
    type: GET_SHOPS_TYPE.ERROR,
    payload,
  }),
};

export const GET_CAMPAIGNS = {
  START: (payload: any) => ({
    type: GET_CAMPAIGNS_TYPE.START,
    payload,
  }),
  SUCCESS: (payload: any) => ({
    type: GET_CAMPAIGNS_TYPE.SUCCESS,
    payload,
  }),
  ERROR: (payload: any) => ({
    type: GET_CAMPAIGNS_TYPE.ERROR,
    payload,
  }),
};

export const GET_PRODUCTS = {
  START: (payload: any) => ({
    type: GET_PRODUCTS_TYPE.START,
    payload,
  }),
  SUCCESS: (payload: any) => ({
    type: GET_PRODUCTS_TYPE.SUCCESS,
    payload,
  }),
  ERROR: (payload: any) => ({
    type: GET_PRODUCTS_TYPE.ERROR,
    payload,
  }),
};

export const GET_KEYWORDS = {
  START: (payload: any) => ({
    type: GET_KEYWORDS_TYPE.START,
    payload,
  }),
  SUCCESS: (payload: any) => ({
    type: GET_KEYWORDS_TYPE.SUCCESS,
    payload,
  }),
  ERROR: (payload: any) => ({
    type: GET_KEYWORDS_TYPE.ERROR,
    payload,
  }),
};

export const STATUS_KEYWORDS = {
  START: (payload: any) => ({
    type: STATUS_KEYWORDS_TYPE.START,
    payload,
  }),
  SUCCESS: (payload: any) => ({
    type: STATUS_KEYWORDS_TYPE.SUCCESS,
    payload,
  }),
  ERROR: (payload: any) => ({
    type: STATUS_KEYWORDS_TYPE.ERROR,
    payload,
  }),
};

export const STATUS_CAMPAIGN = {
  START: (payload: any) => ({
    type: STATUS_CAMPAIGN_TYPE.START,
    payload,
  }),
  SUCCESS: (payload: any) => ({
    type: STATUS_CAMPAIGN_TYPE.SUCCESS,
    payload,
  }),
  ERROR: (payload: any) => ({
    type: STATUS_CAMPAIGN_TYPE.ERROR,
    payload,
  }),
};

export const STATUS_PRODUCT = {
  START: (payload: any) => ({
    type: STATUS_PRODUCT_TYPE.START,
    payload,
  }),
  SUCCESS: (payload: any) => ({
    type: STATUS_PRODUCT_TYPE.SUCCESS,
    payload,
  }),
  ERROR: (payload: any) => ({
    type: STATUS_PRODUCT_TYPE.ERROR,
    payload,
  }),
};

export const UPDATE_FILTER_DATE_RANGE = (payload: any) => ({
  type: UPDATE_FILTER_DATE_RANGE_TYPE,
  payload,
});

export const UPDATE_FILTER_COUNTRY = (payload: any) => ({
  type: UPDATE_FILTER_COUNTRY_TYPE,
  payload,
});

export const UPDATE_FILTER_SHOP = (payload: any) => ({
  type: UPDATE_FILTER_SHOP_TYPE,
  payload,
});

export const UPDATE_SELECTED_CAMPAIGNS = (payload: any) => ({
  type: UPDATE_SELECTED_CAMPAIGNS_TYPE,
  payload,
});

export const UPDATE_SELECTED_PRODUCTS = (payload: any) => ({
  type: UPDATE_SELECTED_PRODUCTS_TYPE,
  payload,
});

export const UPDATE_SELECTED_KEYWORDS = (payload: any) => ({
  type: UPDATE_SELECTED_KEYWORDS_TYPE,
  payload,
});

export function CHANGE_TAB(tab: string) {
  return {
    type: CHANGE_TAB_TYPE,
    payload: { tab },
  };
}

export function SET_PAGINATION(payload: any) {
  return {
    type: SET_PAGINATION_TYPE,
    payload,
  };
}

export function SET_SEARCH_CAMPAIGN(payload: any) {
  return {
    type: SET_SEARCH_CAMPAIGN_TYPE,
    payload,
  };
}

export function SET_FILTER_CAMPAIGN_STATUS(payload: any) {
  return {
    type: SET_FILTER_CAMPAIGN_STATUS_TYPE,
    payload,
  };
}

export function SET_SEARCH_PRODUCT(payload: any) {
  return {
    type: SET_SEARCH_PRODUCT_TYPE,
    payload,
  };
}

export function SET_FILTER_PRODUCT_STATUS(payload: any) {
  return {
    type: SET_FILTER_PRODUCT_STATUS_TYPE,
    payload,
  };
}

export function SET_SEARCH_KEYWORD(payload: any) {
  return {
    type: SET_SEARCH_KEYWORD_TYPE,
    payload,
  };
}

export function SET_FILTER_KEYWORD_STATUS(payload: any) {
  return {
    type: SET_FILTER_KEYWORD_STATUS_TYPE,
    payload,
  };
}

const actions = {
  updateGlobalFilter: makeAction({
    actname: 'UPDATE_GLOBAL_FILTER',
    fun: function () {
      return {};
    },
  }),
  getRuleActionMetrics: actionWithRequest({
    actname: 'RULE/GET_ACTION_METRICS',
    fun: function* () {
      const rs = yield eff.call(() =>
        fetch.get(EP.RULE_GET_ACTION_METRIC),
      );
      return { actionMetrics: rs.data };
    },
  }),
  createProductRule: actionWithRequest({
    actname: 'RULES/CREATE_RULE_PRODUCT',
    fun: function* ({ rule, products, shopId, callback }) {
      const postRule = {
        rule_name: rule.ruleName,
        timeline_from: moment(rule.fromDate, DATE_FORMAT).format(
          'YYYY-MM-DD',
        ),
        timeline_to: moment(rule.toDate, DATE_FORMAT).format(
          'YYYY-MM-DD',
        ),
        action_code: rule.action,
        metric_condition: JSON.stringify(
          rule.listRules.map((r) => ({
            value: r.value,
            period: !r.conditionPeriod
              ? undefined
              : Number(r.conditionPeriod),
            metric_code: r.typeOfPerformance,
            operator_code: r.operator,
          })),
        ), // [{"value": 0, "period": 1000000, "metric_code": "current_stock", "operator_code": ">"}]
        feature_code: 'M_LZD_SS',
        object_type: 'campaign_product',
        object_eids: products
          .map((p) => p.product_id || p.campaign_product_id)
          .join(','),
        shop_eids: [shopId],
        shop_eid: shopId, // makesure we only create rule for 1 product of 1 shop
      };
      console.info(postRule);
      let rs: any = {};
      try {
        rs = yield eff.call(() =>
          fetch.post(EP.RULE_CREATE, postRule),
        );
      } catch (error) {
        rs = {
          ...rs,
          data: error.message,
        };
      }
      if (callback) {
        callback(rs);
      }
      console.info({ rs });
      return { actionMetrics: rs.data };
    },
  }),
  addExistingRule: actionWithRequest({
    actname: 'RULES/ADD_EXISTING_RULE_PRODUCT',
    fun: function* ({
      shopEid,
      featureCode,
      objectType,
      objectEids,
      ruleId,
      callback,
    }) {
      const postRule = {
        shop_eids: [shopEid],
        shop_eid: shopEid, // makesure we only create rule for 1 product of 1 shop
        feature_code: featureCode,
        object_type: objectType,
        object_eids: objectEids,
        rule_ids: ruleId,
      };
      let rs: any = {};
      try {
        rs = yield eff.call(() =>
          fetch.post(EP.ADD_EXISTING_RULE, postRule),
        );
      } catch (error) {
        rs = {
          ...rs,
          data: error.message,
        };
      }
      console.info({ rs });
      if (callback) {
        callback(rs);
      }
      return { actionMetrics: rs.data };
    },
  }),
  editProductRule: actionWithRequest({
    actname: 'RULES/EDIT_RULE_PRODUCT',
    fun: function* ({ rule, products, shopId }) {
      const postRule = {
        rule_id: rule.ruleId,
        rule_name: rule.ruleName,
        timeline_from: moment(rule.fromDate, DATE_FORMAT).format(
          'YYYY-MM-DD',
        ),
        timeline_to: moment(rule.toDate, DATE_FORMAT).format(
          'YYYY-MM-DD',
        ),
        action_code: rule.action,
        metric_condition: JSON.stringify(
          rule.listRules.map((r) => ({
            value: r.value,
            period: !r.conditionPeriod
              ? undefined
              : Number(r.conditionPeriod),
            metric_code: r.typeOfPerformance,
            operator_code: r.operator,
          })),
        ),
        feature_code: 'M_LZD_SS',
        object_type: 'campaign_product',
        object_eid: products.map((p) => p.product_id).join(','),
        shop_eids: [shopId],
        shop_eid: shopId, // makesure we only create rule for 1 product of 1 shop
      };
      const rs = yield eff.call(() =>
        fetch.put(EP.RULE_EDIT, postRule),
      );
      return { actionMetrics: rs.data };
    },
  }),
  deleteProductRule: actionWithRequest({
    actname: 'RULES/DELETE_PRODUCT_RULE',
    fun: function* ({ productId, shopEid, ruleId }) {
      const rs = yield eff.call(() =>
        fetch.deleteFetch(EP.RULE_DELETE, {
          shop_eids: [shopEid],
          shop_eid: shopEid, // makesure we only create rule for 1 product of 1 shop
          object_type: 'campaign_product',
          object_eid: productId,
          rule_id: ruleId,
        }),
      );

      return { message: rs.data.message };
    },
  }),
  deleteAllProductRules: actionWithRequest({
    actname: 'RULES/DELETE_PRODUCT_RULE_ALL',
    fun: function* ({ itemIds, shopEids, featureCode, callback }) {
      let rs: any = {};
      try {
        rs = yield eff.call(() =>
          fetch.deleteFetch(EP.RULE_DELETE_ALL, {
            shop_eids: shopEids,
            feature_code: featureCode,
            object_type: 'campaign_product',
            object_eids: itemIds,
          }),
        );
      } catch (error) {
        rs = {
          ...rs,
          data: error.message,
        };
      }

      if (callback) {
        callback(rs);
      }

      return { message: rs.data.message };
    },
  }),
  createKeywordRule: actionWithRequest({
    actname: 'RULES/CREATE_RULE_KEYWORD',
    fun: function* ({ rule, keywords, shopId }) {
      const postRule = {
        rule_name: rule.ruleName,
        timeline_from: moment(rule.fromDate, DATE_FORMAT).format(
          'YYYY-MM-DD',
        ),
        timeline_to: moment(rule.toDate, DATE_FORMAT).format(
          'YYYY-MM-DD',
        ),
        action_code: rule.action,
        metric_condition: JSON.stringify(
          rule.listRules.map((r) => ({
            value: r.value,
            period: !r.conditionPeriod
              ? undefined
              : Number(r.conditionPeriod),
            metric_code: r.typeOfPerformance,
            operator_code: r.operator,
          })),
        ), // [{"value": 0, "period": 1000000, "metric_code": "current_stock", "operator_code": ">"}]
        bidding_price_data:
          [
            'increase_bidding_price',
            'decrease_bidding_price',
          ].indexOf(rule.action) > -1
            ? JSON.stringify({
                step: rule.ruleValue.value,
                type: rule.ruleValue.type,
                maximum: rule.biddingPriceMax,
                minimum: rule.biddingPriceMin,
              })
            : undefined,
        feature_code: 'M_LZD_SS',
        object_type: 'product_keyword',
        object_eids: keywords.map((p) => p.keyword_id).join(','),
        shop_eids: [shopId],
        shop_eid: shopId, // makesure we only create rule for 1 product of 1 shop
      };
      console.info(postRule);
      const rs = yield eff.call(() =>
        fetch.post(EP.RULE_CREATE, postRule),
      );
      console.info({ rs });
      return { actionMetrics: rs.data };
    },
  }),
  editKeywordRule: actionWithRequest({
    actname: 'RULES/EDIT_RULE_KEYWORD',
    fun: function* ({ rule, keywords, shopId }) {
      const postRule = {
        rule_id: rule.ruleId,
        rule_name: rule.ruleName,
        timeline_from: moment(rule.fromDate, DATE_FORMAT).format(
          'YYYY-MM-DD',
        ),
        timeline_to: moment(rule.toDate, DATE_FORMAT).format(
          'YYYY-MM-DD',
        ),
        action_code: rule.action,
        metric_condition: JSON.stringify(
          rule.listRules.map((r) => ({
            value: r.value,
            period: !r.conditionPeriod
              ? undefined
              : Number(r.conditionPeriod),
            metric_code: r.typeOfPerformance,
            operator_code: r.operator,
          })),
        ),
        bidding_price_data:
          [
            'increase_bidding_price',
            'decrease_bidding_price',
          ].indexOf(rule.action) > -1
            ? JSON.stringify({
                step: rule.ruleValue.value,
                type: rule.ruleValue.type,
                maximum: rule.biddingPriceMax,
                minimum: rule.biddingPriceMin,
              })
            : undefined,
        feature_code: 'M_LZD_SS',
        object_type: 'product_keyword',
        object_eid: keywords.map((p) => p.keyword_id).join(','),
        shop_eids: [shopId],
        shop_eid: shopId, // makesure we only create rule for 1 product of 1 shop
      };
      console.info(postRule);
      const rs = yield eff.call(() =>
        fetch.put(EP.RULE_EDIT, postRule),
      );
      console.info({ rs });
      return { actionMetrics: rs.data };
    },
  }),
  deleteKeywordRule: actionWithRequest({
    actname: 'RULES/DELETE_KEYWORD_RULE',
    fun: function* ({ keywordId, shopEid, ruleId }) {
      const rs = yield eff.call(() =>
        fetch.deleteFetch(EP.RULE_DELETE, {
          shop_eids: [shopEid],
          shop_eid: shopEid, // makesure we only create rule for 1 product of 1 shop
          object_type: 'product_keyword',
          object_eid: keywordId,
          rule_id: ruleId,
        }),
      );

      return { message: rs.data.message };
    },
  }),
  deleteAllKeywordRules: actionWithRequest({
    actname: 'RULES/DELETE_KEYWORD_RULE_ALL',
    fun: function* ({ itemIds, shopEids, featureCode, callback }) {
      let rs: any = {};
      try {
        rs = yield eff.call(() =>
          fetch.deleteFetch(EP.RULE_DELETE_ALL, {
            shop_eids: shopEids,
            feature_code: featureCode,
            object_type: 'product_keyword',
            object_eids: itemIds,
          }),
        );
      } catch (error) {
        rs = {
          ...rs,
          data: error.message,
        };
      }

      if (callback) {
        callback(rs);
      }

      return { message: rs.data.message };
    },
  }),
  getExistProductRules: actionWithRequest({
    actname: 'RULES/GET_EXIST_RULES_PRODUCT',
    fun: function* ({ productId, shopEid }) {
      const rs = yield eff.call(() =>
        fetch.get(EP.RULE_GET_LIST_RULE, {
          shop_eids: shopEid,
          object_type: 'campaign_product',
          object_eid: productId,
        }),
      );

      let ruleList = rs.data.map((r) => ({
        ...r,
        condition: JSON.parse(r.condition_json),
        data: JSON.parse(r.data_json),
      }));
      return { ruleList: ruleList };
    },
  }),
  getExistKeywordRules: actionWithRequest({
    actname: 'RULES/GET_EXIST_RULES_KEYWORD',
    fun: function* ({ keywordId, shopEid }) {
      const rs = yield eff.call(() =>
        fetch.get(EP.RULE_GET_LIST_RULE, {
          shop_eids: shopEid,
          object_type: 'product_keyword',
          object_eid: keywordId,
        }),
      );

      let ruleList = rs.data.map((r) => ({
        ...r,
        condition: JSON.parse(r.condition_json),
        data: JSON.parse(r.data_json),
      }));
      return { ruleList: ruleList };
    },
  }),
  getProductRules: actionWithRequest({
    actname: 'RULES/GET_RULES_PRODUCT',
    fun: function* ({ productId, shopEid }) {
      const rs = yield eff.call(() =>
        fetch.get(EP.RULE_GET_LIST_RULE, {
          shop_eids: shopEid,
          object_type: 'campaign_product',
          object_eid: productId,
        }),
      );

      let ruleList = rs.data.map((r) => ({
        ...r,
        condition: JSON.parse(r.condition_json),
        data: JSON.parse(r.data_json),
      }));
      return { ruleList: ruleList };
    },
  }),
  getKeywordRules: actionWithRequest({
    actname: 'RULES/GET_RULES_KEYWORD',
    fun: function* ({ keywordId, shopEid }) {
      const rs = yield eff.call(() =>
        fetch.get(EP.RULE_GET_LIST_RULE, {
          shop_eids: shopEid,
          object_type: 'product_keyword',
          object_eid: keywordId,
        }),
      );

      let ruleList = rs.data.map((r) => ({
        ...r,
        condition: JSON.parse(r.condition_json),
        data: JSON.parse(r.data_json),
      }));
      return { ruleList: ruleList };
    },
  }),

  updateRuleLogSorting: makeAction({
    actname: 'RULES/UPDATE_RULE_LOG_SORTING',
    fun: function ({ sort }) {
      return { sort };
    },
  }),
  updateRuleLogFilter: makeAction({
    actname: 'RULES/UPDATE_RULE_LOG_FILTER',
    fun: function (payload) {
      return payload;
    },
  }),
  resetRuleLog: makeAction({
    actname: 'RULES/RESET_RULE_LOG',
    fun: function () {
      return {};
    },
  }),
  getRuleLogs: actionWithRequest({
    actname: 'RULES/GET_RULE_LOGS',
    fun: function* ({
      itemId,
      shopEid,
      itemType,
      search,
      from,
      to,
      actions,
      page,
      limit,
      orderBy,
      orderMode,
    }) {
      const rs = yield eff.call(() =>
        fetch.get(EP.RULE_GET_LIST_RULE_LOG, {
          shop_eids: shopEid,
          object_type: itemType,
          object_eid: itemId,
          search,
          from,
          to,
          action_code: actions,
          page,
          limit,
          orderBy,
          orderMode,
        }),
      );

      const logList = rs.data.map((r) => ({
        ...r,
        ruleName: r.rule_name,
        actionCode: r.action_code,
        action: r.action_name,
        ruleValue: r.rule_data.bidding_price_data?.step,
        type: r.rule_data.bidding_price_data?.type,
        minBid: r.rule_data.bidding_price_data?.minimum,
        maxBid: r.rule_data.bidding_price_data?.maximum,
        create: moment(r.created_at).format('DD/MM/YYYY'),
        condition: r.condition_checkpoint.map((c) => ({
          metricName: c.metric_name,
          metricCode: c.metric_code,
          period: c.period,
          operator: c.operator_code,
          value: c.value,
          checkpoint: c.value_checkpoint,
        })),
      }));

      const pagination = {
        limit: rs.pagination.limit,
        page: rs.pagination.page,
        itemCount: rs.pagination.item_count,
        pageCount: rs.pagination.page_count,
      };

      return { logList, pagination };
    },
  }),
};

export { actions };
