import { produce } from 'immer';
import { uniqBy } from 'lodash';
import moment from 'moment';
import {
  CAMPAIGN_STATUS,
  CHANNEL,
  PRODUCT_STATUS,
} from '../constants';
import { sortList } from '../utils/utils';
import {
  actions,
  CHANGE_TAB_TYPE,
  GET_CAMPAIGNS_TYPE,
  GET_CATEGORIES_TYPE,
  GET_KEYWORDS_TYPE,
  GET_PRODUCTS_TYPE,
  GET_SHOPS_TYPE,
  SET_FILTER_CAMPAIGN_STATUS_TYPE,
  SET_FILTER_KEYWORD_STATUS_TYPE,
  SET_FILTER_PRODUCT_STATUS_TYPE,
  SET_PAGINATION_TYPE,
  SET_SEARCH_CAMPAIGN_TYPE,
  SET_SEARCH_KEYWORD_TYPE,
  SET_SEARCH_PRODUCT_TYPE,
  STATUS_CAMPAIGN_TYPE,
  STATUS_KEYWORDS_TYPE,
  STATUS_PRODUCT_TYPE,
  UPDATE_FILTER_COUNTRY_TYPE,
  UPDATE_FILTER_DATE_RANGE_TYPE,
  UPDATE_FILTER_SHOP_TYPE,
  UPDATE_SELECTED_CAMPAIGNS_TYPE,
  UPDATE_SELECTED_KEYWORDS_TYPE,
  UPDATE_SELECTED_PRODUCTS_TYPE,
} from './actions';

export type MarketingAdvertisingState = {
  tab: 'Campaign' | 'Product' | 'Keyword';
  previousTab?: 'Campaign' | 'Product' | 'Keyword';
  // FIXME: [FE-6] Separating the pagination between different table
  pagination: {
    page?: number;
    limit?: number;
    item_count?: number;
    page_count?: number;
  };
  campaigns?: any;
  products?: any;
  keywords?: any;
  searchCampaign?: string;
  filterCampaignStatus?: string;
  searchProduct?: string;
  filterProductStatus?: string;
  searchKeyword?: string;
  filterKeywordStatus?: string;
  selectedCampaigns?: any;
  selectedProducts?: any;
  selectedKeywords?: any;
  nextLoading: {
    [key: string]: {
      status: boolean;
      error: any;
    };
  };
  existProductRuleList?: any[];
  existKeywordRuleList?: any[];
  productRuleList?: any[];
  keywordRuleList?: any[];
  rulesActionList?: any[];
  rulesMetricList?: {
    [key: string]: any[];
  };
  [key: string]: any;
  ruleLogFilters: {
    searchText: string;
    timeRange: {
      start: string;
      end: string;
    };
    actions: any[];
  };
  ruleLogs: {
    items: any[];
    sort: any;
    pagination: {
      page?: number;
      limit?: number;
      itemCount?: number;
      pageCount?: number;
    };
  };
};

const reducer = (
  state: MarketingAdvertisingState = {
    tab: 'Campaign',
    previousTab: 'Campaign',
    pagination: {},
    searchCampaign: '',
    filterCampaignStatus: 'running',
    searchProduct: '',
    filterProductStatus: 'running',
    searchKeyword: '',
    filterKeywordStatus: 'running',
    availableFilter: {
      countries: [],
      shops: [],
    },
    optionGlobalFilters: {
      countries: [],
      shops: [],
    },
    globalFilters: {
      timeRange: {
        start: moment().format('YYYY-MM-DD'),
        end: moment().format('YYYY-MM-DD'),
      },
      countries: [],
      shops: [],
      updateCount: 0,
    },
    nextLoading: {},
    ruleLogFilters: {
      searchText: '',
      timeRange: {
        start: moment().format('YYYY-MM-DD'),
        end: moment().format('YYYY-MM-DD'),
      },
      actions: [],
    },
    ruleLogs: {
      items: [],
      sort: null,
      pagination: {
        limit: 10,
        page: 1,
        itemCount: 0,
        pageCount: 1,
      },
    },
  },
  action: any,
) => {
  switch (action.type) {
    case SET_SEARCH_CAMPAIGN_TYPE:
      const { searchCampaign } = action.payload;
      return produce(state, (draft) => {
        draft.searchCampaign = searchCampaign;
      });
    case SET_FILTER_CAMPAIGN_STATUS_TYPE:
      const { filterCampaignStatus } = action.payload;
      return produce(state, (draft) => {
        draft.filterCampaignStatus = filterCampaignStatus;
      });
    case SET_SEARCH_PRODUCT_TYPE:
      const { searchProduct } = action.payload;
      return produce(state, (draft) => {
        draft.searchProduct = searchProduct;
      });
    case SET_FILTER_PRODUCT_STATUS_TYPE:
      const { filterProductStatus } = action.payload;
      return produce(state, (draft) => {
        draft.filterProductStatus = filterProductStatus;
      });
    case SET_SEARCH_KEYWORD_TYPE:
      const { searchKeyword } = action.payload;
      return produce(state, (draft) => {
        draft.searchKeyword = searchKeyword;
      });
    case SET_FILTER_KEYWORD_STATUS_TYPE:
      const { filterKeywordStatus } = action.payload;
      return produce(state, (draft) => {
        draft.filterKeywordStatus = filterKeywordStatus;
      });
    case SET_PAGINATION_TYPE:
      const { pagination } = action.payload;
      return produce(state, (draft) => {
        draft.pagination = pagination;
      });
    case GET_CAMPAIGNS_TYPE.ERROR:
    case GET_KEYWORDS_TYPE.ERROR:
    case GET_PRODUCTS_TYPE.ERROR:
    case STATUS_KEYWORDS_TYPE.ERROR:
    case STATUS_CAMPAIGN_TYPE.ERROR:
    case STATUS_PRODUCT_TYPE.ERROR:
    case GET_SHOPS_TYPE.ERROR:
    case GET_CATEGORIES_TYPE.ERROR:
      return produce(state, (draft) => {
        draft.loading = false;
        draft.error = true;
      });
    case GET_CAMPAIGNS_TYPE.START:
    case GET_PRODUCTS_TYPE.START:
    case GET_KEYWORDS_TYPE.START:
    case STATUS_KEYWORDS_TYPE.START:
    case STATUS_CAMPAIGN_TYPE.START:
    case STATUS_PRODUCT_TYPE.START:
    case GET_SHOPS_TYPE.START:
    case GET_CATEGORIES_TYPE.START:
      return produce(state, (draft) => {
        draft.loading = true;
        draft.error = false;
      });
    case GET_CATEGORIES_TYPE.SUCCESS:
      return produce(state, (draft) => {
        draft.categories = action.payload.categories;
        draft.loading = false;
        draft.error = false;
      });
    case STATUS_PRODUCT_TYPE.SUCCESS:
      return produce(state, (draft) => {
        draft.products = action.payload.products;
        draft.loading = false;
        draft.error = false;
      });
    case STATUS_CAMPAIGN_TYPE.SUCCESS:
      return produce(state, (draft) => {
        draft.campaigns = action.payload.products;
        draft.loading = false;
        draft.error = false;
      });
    case STATUS_KEYWORDS_TYPE.SUCCESS:
      return produce(state, (draft) => {
        draft.keywords = action.payload.keywords;
        draft.loading = false;
        draft.error = false;
      });
    case GET_SHOPS_TYPE.SUCCESS: {
      const { payload } = action;

      return produce(state, (draft) => {
        const shops = payload.shops.filter(
          (i: any) => i.channel_code === CHANNEL,
        );

        const countries = shops.reduce((acc, s) => {
          if (acc.find((s1) => s1.countryCode === s.country_code)) {
            return acc;
          } else {
            return acc.concat({
              countryCode: s.country_code,
              countryName: s.country_name,
            });
          }
        }, []);

        let countryOptions = countries.map((c) => ({
          id: c.countryCode,
          text: c.countryName,
        }));

        let shopOptions = shops.map((i) => ({
          id: i.shop_eid,
          text: `${i.country_code} / ${i.shop_name}`,
          countryCode: i.country_code,
        }));

        countryOptions = sortList(countryOptions, 'text');
        shopOptions = sortList(shopOptions, 'text');

        draft.shops = shops;
        draft.availableFilter = {
          countries: countryOptions,
          shops: shopOptions,
        };

        draft.optionGlobalFilters = {
          countries: countryOptions,
          shops: shopOptions,
        };

        draft.globalFilters = {
          ...state.globalFilters,
          countries: countryOptions,
          shops: shopOptions,
          updateCount: state.globalFilters.updateCount + 1,
        };
      });
    }
    case UPDATE_FILTER_DATE_RANGE_TYPE:
      return produce(state, (draft) => {
        draft.globalFilters.timeRange = action.payload.timeRange;
        draft.pagination = {
          page: 1,
        };
      });
    case UPDATE_FILTER_COUNTRY_TYPE:
      return produce(state, (draft) => {
        const shopOptions = state.availableFilter.shops.reduce(
          (acc, elem) => {
            if (
              action.payload.countries.some(
                (item: any) => item.id === elem.countryCode,
              )
            ) {
              return [...acc, elem];
            } else {
              return acc;
            }
          },
          [],
        );

        draft.globalFilters.countries = action.payload.countries;
        draft.optionGlobalFilters.shops = shopOptions;
        draft.globalFilters.shops = shopOptions;
      });
    case UPDATE_FILTER_SHOP_TYPE:
      return produce(state, (draft) => {
        draft.globalFilters.shops = action.payload.shops;
      });
    case actions.updateGlobalFilter.type(): {
      return produce(state, (draft) => {
        draft.globalFilters.updateCount =
          state.globalFilters.updateCount + 1;
        draft.pagination = {
          page: 1,
        };
      });
    }
    case GET_CAMPAIGNS_TYPE.SUCCESS: {
      console.time('get_campaign_processing');
      let nuState = produce(state, (draft) => {
        let { campaigns } = action.payload;
        draft.campaigns = campaigns.map((p) => ({
          ...p,
          _isDisabled: p.campaign_status === CAMPAIGN_STATUS.ENDED,
        }));
        (draft.pagination = action.payload.pagination),
          (draft.loading = false);
        draft.error = false;
      });
      nuState = chainSelectionEffect(nuState, 'campaign');
      console.timeEnd('get_campaign_processing');
      return nuState;
    }
    case GET_PRODUCTS_TYPE.SUCCESS: {
      console.time('get_product_processing');
      let nuState = produce(state, (draft) => {
        let { products } = action.payload;
        draft.products = products.map((p) => ({
          ...p,
          _isDisabled:
            p.status === PRODUCT_STATUS.ENDED ||
            p.status === PRODUCT_STATUS.CLOSED,
        }));
        draft.pagination = action.payload.pagination;
        draft.loading = false;
        draft.error = false;
      });
      nuState = chainSelectionEffect(nuState, 'product');
      console.timeEnd('get_product_processing');
      return nuState;
    }
    case GET_KEYWORDS_TYPE.SUCCESS:
      console.time('get_keyword_processing');
      let nuState = produce(state, (draft) => {
        let { keywords } = action.payload;
        draft.keywords = keywords.map((p) => ({
          ...p,
          _isDisabled:
            p.status === PRODUCT_STATUS.ENDED ||
            p.status === PRODUCT_STATUS.CLOSED,
        }));
        (draft.pagination = action.payload.pagination),
          (draft.loading = false);
        draft.error = false;
      });
      nuState = chainSelectionEffect(nuState, 'keyword');
      console.timeEnd('get_keyword_processing');
      return nuState;
    case UPDATE_SELECTED_CAMPAIGNS_TYPE:
      return produce(state, (draft) => {
        draft.selectedCampaigns = action.payload;
      });
    case UPDATE_SELECTED_PRODUCTS_TYPE:
      return produce(state, (draft) => {
        draft.selectedProducts = action.payload;
      });
    case UPDATE_SELECTED_KEYWORDS_TYPE:
      return produce(state, (draft) => {
        draft.selectedKeywords = action.payload;
      });
    case CHANGE_TAB_TYPE:
      return produce(state, (draft) => {
        draft.tab = action.payload.tab;
        draft.pagination = { ...state.pagination, page: 1 };
        if (state.tab !== action.payload.tab) {
          draft.previousTab = state.tab;
        }
        if (state.tab !== action.payload.tab) {
          switch (action.payload.tab) {
            case 'Campaign': {
              draft.campaigns = [];
              break;
            }
            case 'Product': {
              draft.products = [];
              break;
            }
            case 'Keyword': {
              draft.keywords = [];
              break;
            }
          }
        }
      });

    case actions.getRuleActionMetrics.fetchType(): {
      const { payload } = action;
      return produce(state, (draft) => {
        let actionList = uniqBy(
          payload.actionMetrics,
          'action_code',
        ).map((i) => ({ name: i.action_name, value: i.action_code }));

        let metricList = actionList.reduce((acc, a) => {
          return {
            ...acc,
            [a.value]: payload.actionMetrics
              .filter((i) => i.action_code === a.value)
              .map((i) => ({
                name: i.metric_name,
                value: i.metric_code,
              })),
          };
        }, {});

        draft.rulesActionList = actionList;
        draft.rulesMetricList = metricList;
      });
    }
    case actions.getExistProductRules.fetchType(): {
      return produce(state, (draft) => {
        draft.existProductRuleList = action.payload.ruleList;
      });
    }
    case actions.getExistKeywordRules.fetchType(): {
      return produce(state, (draft) => {
        draft.existKeywordRuleList = action.payload.ruleList;
      });
    }
    case actions.getProductRules.fetchType(): {
      return produce(state, (draft) => {
        draft.productRuleList = action.payload.ruleList;
      });
    }
    case actions.getKeywordRules.fetchType(): {
      return produce(state, (draft) => {
        draft.keywordRuleList = action.payload.ruleList;
      });
    }
    case actions.resetRuleLog.type(): {
      return produce(state, (draft) => {
        draft.ruleLogFilters = {
          searchText: '',
          timeRange: {
            start: moment().format('YYYY-MM-DD'),
            end: moment().format('YYYY-MM-DD'),
          },
          actions: [],
        };
        draft.ruleLogs = {
          items: [],
          sort: null,
          pagination: {
            limit: 10,
            page: 1,
            itemCount: 0,
            pageCount: 1,
          },
        };
      });
    }
    case actions.getRuleLogs.fetchType(): {
      return produce(state, (draft) => {
        const { logList, pagination } = action.payload;
        draft.ruleLogs = {
          ...state.ruleLogs,
          items: logList,
          pagination,
        };
      });
      break;
    }
    case actions.updateRuleLogSorting.type(): {
      return produce(state, (draft) => {
        draft.ruleLogs = {
          ...state.ruleLogs,
          sort: action.payload.sort,
        };
      });
    }
    case actions.updateRuleLogFilter.type(): {
      return produce(state, (draft) => {
        draft.ruleLogFilters = {
          ...state.ruleLogFilters,
          ...action.payload,
        };
      });
    }
    case 'REQUEST_ACTION_LOADING': {
      state = produce(state, (draft) => {
        draft.nextLoading[action.payload.loading.section] = {
          status: action.payload.loading.status,
          error: action.payload.loading.error,
        };
      });
      return state;
    }
    default:
      return state;
  }
};

export default reducer;

function chainSelectionEffect(
  state: MarketingAdvertisingState,
  from: 'campaign' | 'product' | 'keyword',
) {
  switch (from) {
    case 'campaign': {
      let campaignIds = state.campaigns.map(
        (i: any) => i.campaign_eid,
      );
      return produce(state, (draft) => {
        draft.previousTab = 'Campaign';
        if (campaignIds.length > 0) {
          draft.selectedCampaigns = (
            state.selectedCampaigns || []
          ).filter((c) => campaignIds.includes(c.campaign_eid));
        }
        if (state.selectedProducts) {
          draft.selectedProducts = state.selectedProducts.filter(
            (i: any) => campaignIds.indexOf(i.campaign_eid) > -1,
          );
        }
        if (state.selectedKeywords) {
          draft.selectedKeywords = state.selectedKeywords.filter(
            (i: any) => campaignIds.indexOf(i.campaign_eid) > -1,
          );
        }
      });
    }
    case 'product': {
      let parentIds = state.products.map(
        (i: any) => i.campaign_product_id,
      );
      return produce(state, (draft) => {
        draft.previousTab = 'Product';
        if (parentIds.length > 0) {
          // make sure the keyword is loaded
          draft.selectedProducts = (
            state.selectedProducts || []
          ).filter((p) => parentIds.includes(p.campaign_product_id));
        }
        if (state.selectedKeywords) {
          draft.selectedKeywords = state.selectedKeywords.filter(
            (i: any) => parentIds.indexOf(i.campaign_product_id) > -1,
          );
        }
      });
    }
    case 'keyword': {
      let keywordIds = state.keywords.map((i: any) => i.keyword_id);
      return produce(state, (draft) => {
        draft.previousTab = 'Keyword';
        if (keywordIds.length > 0) {
          draft.selectedKeywords = (
            draft.selectedKeywords || []
          ).filter((k) => keywordIds.includes(k.keyword_id));
        }
      });
    }
  }
}
