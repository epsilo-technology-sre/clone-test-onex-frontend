import { ChannelHeaderUI } from '@ep/shopee/src/components/channel-header';
import { BlockScreenUI } from '@ep/shopee/src/components/common/block-screen';
import { BreadcrumbsUI } from '@ep/shopee/src/components/common/breadcrumbs';
import {
  TabPanelUI,
  TabSelectionContainer,
} from '@ep/shopee/src/components/common/tab-selection';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import { TABS } from '../constants';
import { CampaignContainer } from '../containers/campaign-container/campaign-container';
import { GlobalFiltersContainer } from '../containers/global-filters-container';
import { KeywordContainer } from '../containers/keyword-container/keyword-container';
import { ProductContainer } from '../containers/product-container/product-container';
import {
  CHANGE_TAB,
  UPDATE_SELECTED_CAMPAIGNS,
  UPDATE_SELECTED_PRODUCTS,
} from '../redux/actions';

const LINKS = [
  { text: 'Advertising', href: '/' },
  { text: 'Lazada', href: '/' },
  { text: 'Sponsored Search', href: '/' },
];

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      background: '#ffffff',
    },
  }),
);

export const CampaignPage = () => {
  const classes = useStyles();
  const [links, setLinks] = useState(LINKS);
  const [showWaiting, setShowWaiting] = useState(false);

  const dispatch = useDispatch();

  const linkCreateCampaign = '/advertising/lazada/create-campaign';

  const { tab } = useSelector((state) => ({
    tab: state.tab,
  }));

  const tabs = [
    { type: TABS.CAMPAIGN, text: 'Campaigns' },
    { type: TABS.PRODUCT, text: 'Products' },
    { type: TABS.KEYWORD, text: 'Keywords' },
  ];

  const history = useHistory();
  const query = new URLSearchParams(useLocation().search);
  React.useEffect(() => {
    const nextTab = query.get('tab');
    const campaignIds = query.get('campaignIds');
    const productIds = query.get('productIds');

    if (nextTab && tab !== nextTab) {
      if (campaignIds) {
        const selectedCampaigns = campaignIds
          .split(',')
          .map((id) => ({
            campaign_eid: parseInt(id),
          }));
        dispatch(UPDATE_SELECTED_CAMPAIGNS(selectedCampaigns));
      }

      if (productIds) {
        const selectedProducts = productIds.split(',').map((id) => ({
          campaign_product_id: parseInt(id),
        }));
        dispatch(UPDATE_SELECTED_PRODUCTS(selectedProducts));
      }

      dispatch(CHANGE_TAB(nextTab));
    }
  }, []);

  const handleChangeTab = (tab: any) => {
    query.set('tab', tab);
    history.push({
      search: query.toString(),
    });
    dispatch(CHANGE_TAB(tab));
  };

  const handleClickLink = (link: any) => {
    console.log('Click link ', link);
  };

  return (
    <>
      <div className={classes.root}>
        <BreadcrumbsUI links={links} onClickLink={handleClickLink} />
        <ChannelHeaderUI
          title="Sponsored Search"
          calloutButtonText="Create Campaigns"
          calloutLink={linkCreateCampaign}
        />
        <GlobalFiltersContainer />
        <TabSelectionContainer
          tabs={tabs}
          selectedTab={tab}
          showIcon={true}
          onChangeTab={handleChangeTab}
        />
        <TabPanelUI value={tab} index={TABS.CAMPAIGN}>
          <CampaignContainer />
        </TabPanelUI>
        <TabPanelUI value={tab} index={TABS.PRODUCT}>
          <ProductContainer />
        </TabPanelUI>
        <TabPanelUI value={tab} index={TABS.KEYWORD}>
          <KeywordContainer />
        </TabPanelUI>
      </div>
      <BlockScreenUI isShow={showWaiting} />
    </>
  );
};

export default CampaignPage;
