import { actions } from '@ep/lazada/src/redux/create-campaign/actions';
import { CreateCampaignState } from '@ep/lazada/src/redux/create-campaign/reducers';
import { AddKeyword } from '@ep/shopee/src/components/add-product-keyword/add-keyword';
import { ModalConfirmUI } from '@ep/shopee/src/components/common/modal-confirm';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import { initColumns } from './add-keyword-columns';

export const AddKeywordContainer = () => {
  const dispatch = useDispatch();
  const state = useSelector(
    ({
      setupCampaign: { shop, shopCurrency },
      addKeyword: {
        products,
        keywordBucket,
        selectedProducts,
        selectedKeywords,
        keywords,
        searchText,
        pagination,
      },
    }: CreateCampaignState) => {
      return {
        shop,
        shopCurrency,
        products,
        keywordBucket,
        selectedProducts,
        selectedKeywords,
        keywords,
        searchText,
        pagination,
      };
    },
  );

  const {
    shop: shopId,
    shopCurrency,
    products,
    keywordBucket,
    selectedProducts,
    selectedKeywords,
    keywords,
    searchText,
    pagination,
  } = state;

  useEffect(() => {
    const productSIds = products.map((i) => i.productSId).join(',');
    dispatch(
      actions.getSuggestKeywords({
        shopId,
        productSIds,
      }),
    );
  }, []);
  const [
    confirmInvalidKeywords,
    showConfirmInvalidKeywords,
  ] = React.useState({ numInvalidKeywords: 0, display: false });

  const handleUpdateSelectedKeywordPrice = (value: any) => {
    dispatch(
      actions.updateSelectedKeywordPrice({
        biddingPrice: value.biddingPrice,
        matchType: value.matchType,
      }),
    );
  };
  const handleSearch = (searchText: any, currency: string) => {
    dispatch(actions.updateKeywordSearch({ searchText, currency }));
  };

  const handleAddKeywordsToBucket = (keywords: any[]) => {
    dispatch(
      actions.addKeywordToBucket({
        keywords,
        callback: (index: number, length: number) => {
          toast.info(
            `Validated keywords of ${index}/${length} products`,
            {
              autoClose: 3000,
            },
          );
        },
      }),
    );
  };

  const handleRemoveKeywordFromBucket = (
    keyword: any,
    product: any,
  ) => {
    dispatch(
      actions.removeKeyword({
        keyword,
        product,
      }),
    );
  };

  const handleResetKeywordBucket = () => {
    dispatch(actions.removeAllKeywords());
  };

  const handleAddCustomKeywords = (
    keywordNames: any,
    currency: any,
  ) => {
    dispatch(actions.addCustomKeywords({ keywordNames, currency }));
  };

  const handleSelectProductBucketItem = (product: any) => {
    dispatch(actions.updateSelectedProducts({ product }));
  };

  const handleSelectKeywordItem = (keyword: any, isAdd: boolean) => {
    dispatch(actions.selectKeyword({ keyword, isAdd }));
  };

  const handleSelectAllKeywordItems = (checked: boolean) => {
    dispatch(
      actions.selectAllKeywords({
        isAdd: checked,
      }),
    );
  };

  const handleChangePagination = (pagination: any) => {
    dispatch(actions.updateKeywordPagination({ pagination }));
  };

  const handleBackToPrevious = () => {
    dispatch(actions.openAddKeywordModal({ visible: false }));
    dispatch(actions.openAddProductModal({ visible: true }));
    dispatch(actions.resetAddKeyword());
  };

  const handleCancel = () => {
    dispatch(actions.openAddKeywordModal({ visible: false }));
    dispatch(actions.resetAddKeyword());
  };

  const handleSaveKeyword = () => {
    dispatch(
      actions.updateProductKeywords({
        productKeywordList: keywordBucket,
      }),
    );
    dispatch(actions.openAddKeywordModal({ visible: false }));
    dispatch(actions.resetAddKeyword());
  };

  const handleConfirmationForContainsInvalidKeywords = () => {
    const invalidKeywords = keywordBucket.filter(
      (i) => i.invalidKeywords && i.invalidKeywords.length > 0,
    );

    if (invalidKeywords.length > 0) {
      showConfirmInvalidKeywords({
        numInvalidKeywords: invalidKeywords.length,
        display: true,
      });
    } else {
      handleSaveKeyword();
    }
  };

  const handleRemoveCustomKeyword = (keyword) => {
    dispatch(
      actions.removeCustomKeywords({
        keywordIds: [keyword.keywordId],
      }),
    );
  };

  const headers = React.useMemo(() => {
    console.info({ handleRemoveCustomKeyword });
    return initColumns({
      onAddToBucket: (value: any) =>
        handleAddKeywordsToBucket([value]),
      onRemoveKeyword: (keyword) =>
        handleRemoveCustomKeyword(keyword),
    });
  }, []);

  return (
    <>
      <AddKeyword
        tableHeaders={headers}
        currency={shopCurrency}
        noMatchType={true}
        productBucket={products}
        selectedProducts={selectedProducts}
        keywords={keywords}
        selectedKeywords={selectedKeywords}
        searchText={searchText}
        pagination={pagination}
        keywordBucket={keywordBucket}
        onUpdateSelectedKeywordPrice={
          handleUpdateSelectedKeywordPrice
        }
        onSearch={handleSearch}
        onAddKeywordsToBucket={handleAddKeywordsToBucket}
        onRemoveKeywordFromBucket={handleRemoveKeywordFromBucket}
        onResetKeywordBucket={handleResetKeywordBucket}
        onAddCustomKeywords={handleAddCustomKeywords}
        onSelectProductBucketItem={handleSelectProductBucketItem}
        onSelectKeywordItem={handleSelectKeywordItem}
        onSelectAllKeywordItems={handleSelectAllKeywordItems}
        onChangePagination={handleChangePagination}
        onBackToPrevious={handleBackToPrevious}
        onSave={handleConfirmationForContainsInvalidKeywords}
        onCancel={handleCancel}
      ></AddKeyword>
      <ModalConfirmUI
        title={`Continue without invalid keywords?`}
        open={confirmInvalidKeywords.display}
        content={`
        ${confirmInvalidKeywords.numInvalidKeywords} products contain keywords which are reserved & can't be used.
        Do you want to continue without those invalid keywords?`}
        labelSubmit={'Continue'}
        onClose={() => {
          showConfirmInvalidKeywords({
            numInvalidKeywords: 0,
            display: false,
          });
        }}
        onSubmit={() => {
          showConfirmInvalidKeywords({
            numInvalidKeywords: 0,
            display: false,
          });
          handleSaveKeyword();
        }}
      />
    </>
  );
};
