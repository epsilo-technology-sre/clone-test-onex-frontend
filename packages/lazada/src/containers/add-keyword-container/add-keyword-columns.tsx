import {
  AddToBucketCell,
  EllipsisCell,
  GroupKeywordActionCell,
  PercentCell,
} from '@ep/shopee/src/components/common/table-cell';

export const initColumns = (props: any) => {
  const { onAddToBucket, onRemoveKeyword } = props;
  console.info('initColumns',{ onAddToBucket, onRemoveKeyword});
  return [
    {
      Header: 'Keyword Bidding',
      accessor: (row: any) => ({ value: row.keywordName }),
      sticky: 'left',
      width: 200,
      disableSortBy: true,
      Cell: EllipsisCell,
    },
    {
      Header: 'Search volume',
      id: 'searchVolume',
      accessor: (row: any) => ({
        number: row.searchVolume,
      }),
      disableSortBy: true,
      Cell: PercentCell,
    },
    {
      Header: 'Suggest bid',
      id: 'suggestBid',
      accessor: (row: any) => ({
        number: row.suggestBid,
        currency: row.currency,
      }),
      disableSortBy: true,
      Cell: PercentCell,
    },
    {
      Header: 'Bidding price',
      id: 'biddingPrice',
      accessor: (row: any) => ({
        number: row.biddingPrice,
        currency: row.currency,
      }),
      Cell: PercentCell,
      disableSortBy: true,
    },
    {
      Header: '',
      id: 'action',
      accessor: (row: any) => ({
        item: row,
        isRemovable: row.isCustom,
        onAddToBucket,
        onRemove: onRemoveKeyword,
      }),
      width: 200,
      Cell: GroupKeywordActionCell,
      disableSortBy: true,
    },
  ];
};
