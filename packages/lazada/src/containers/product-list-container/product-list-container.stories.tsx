import { ThemeProvider } from '@material-ui/core/styles';
import React from 'react';
import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import LazadaTheme from '../../lazada-theme';
import { store } from '../../redux/store';
import { ProductListContainer as ProductListContainerNext } from '../create-campaign/product-list-container';
import { ProductListContainer } from './product-list-container';

export default {
  title: 'Lazada / Container',
};

export const ProductList = () => {
  return (
    <Provider store={store}>
      <ThemeProvider theme={LazadaTheme}>
        <ProductListContainer />
        <ToastContainer closeOnClick={false} hideProgressBar={true} />
      </ThemeProvider>
    </Provider>
  );
};

export const ProductListNext = () => {
  return (
    <Provider store={store}>
      <ThemeProvider theme={LazadaTheme}>
        <ProductListContainerNext />
        <ToastContainer closeOnClick={false} hideProgressBar={true} />
      </ThemeProvider>
    </Provider>
  );
};
