import { COLORS } from '@ep/lazada/src/constants';
import NoProduct from '@ep/shopee/src/images/no-product.svg';
import { ButtonUI } from '@ep/shopee/src/components/common/button';
import { MultiSelectUI } from '@ep/shopee/src/components/common/multi-select';
import { TableUI } from '@ep/shopee/src/components/common/table';
import { InputUI } from '@ep/shopee/src/components/create-campaign/common';
import {
  Box,
  Divider,
  Grid,
  InputAdornment,
  Typography,
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import SearchIcon from '@material-ui/icons/Search';
import debounce from 'lodash/debounce';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  GET_CAMPAIGNS,
  GET_CATEGORIES,
  GET_PRODUCTS,
  GET_SHOPS,
} from '../../redux/actions';
import { initColumns } from './columns';

export const ProductListContainer = (props: any) => {
  const dispatch = useDispatch();
  const ref = useRef({
    selectedProducts: null,
    campaigns: null,
  });
  const {
    shops = [],
    campaigns = [],
    categories = [],
    products = [],
    selectedProducts,
  } = useSelector(
    ({
      shops,
      campaigns,
      categories,
      products,
      selectedProducts,
    }: any) => {
      ref.current = {
        selectedProducts,
        campaigns,
      };
      return {
        shops,
        campaigns,
        categories,
        products,
        selectedProducts,
      };
    },
  );

  const getCampaigns = (payload: any) =>
    dispatch(GET_CAMPAIGNS.START(payload));
  const getProducts = (payload: any) =>
    dispatch(GET_PRODUCTS.START(payload));
  const getCategories = (payload: any) =>
    dispatch(GET_CATEGORIES.START(payload));
  const getShops = (payload?: any) =>
    dispatch(GET_SHOPS.START(payload));
  // const updateSelectedProducts = (payload: any) => dispatch(UPDATE_SELECTED_PRODUCTS(payload));

  useEffect(() => {
    getShops();
    getCampaigns({
      shop_eids: 4110,
      from: '2020-10-01',
      to: '2020-10-31',
      limit: 10,
      page: 1,
      orderBy: 'sum_gmv',
      order_mode: 'desc',
    });
    handleGetProduct();
    getCategories({ shop_eid: 4110 });
  }, []);

  const [selectedShop, setSelectedShop] = useState();
  const [selectedCampaign, setSelectedCampaign] = useState();
  const [searchText, setSearchText] = useState('');
  const [selectedCategories, setSelectedCategories] = useState(
    categories,
  );

  const [tableRows, setTableRows] = useState([]);
  const [pageSize, setPageSize] = useState(10);
  const [currentPage, setCurrentPage] = useState(1);
  const [resultTotal, setResultTotal] = useState(0);
  const [sortBy, setSortBy] = useState([]);
  const [selectingRows, setSelectingRows] = useState([]);

  const refTable = React.useRef({ products: products });

  // useEffect(() => {
  //   refTable.current.products = [...products];
  //   setTableRows(products);
  //   setResultTotal(products.length);
  // }, [products]);

  useEffect(() => {
    debounce(
      () => handleGetProduct({ filter_value: searchText }),
      3000,
    );
  }, [searchText, selectedCategories, pageSize, currentPage, sortBy]);

  const handleGetProduct = (params?: any) => {
    getProducts({
      shop_eids: 4110,
      from: '2020-10-01',
      to: '2020-10-31',
      limit: 10,
      page: 1,
      orderBy: 'sum_gmv',
      order_mode: 'desc',
      ...params,
    });
  };
  // const getProducts = () => {
  //   const start = (currentPage - 1) * pageSize;
  //   const end = start + pageSize;
  //   const displayingProducts = products.splice(start, end);
  //   refTable.current.products = displayingProducts;
  //   setTableRows(displayingProducts);
  //   console.log('products', currentPage, displayingProducts);
  // };

  const removeProduct = (product: any) => {
    const newRows = refTable.current.products.filter(
      (item: any) => item.productId !== product.productId,
    );
    setTableRows(newRows);
    refTable.current.products = newRows;
  };

  const headers = initColumns({
    onAddKeyword: (value: any) => {
      console.log('On add keywords', value);
    },
    onDelete: (value: any) => {
      removeProduct(value);
    },
  });

  const handleChangeSearchText = (event: any) => {
    setSearchText(event.target.value);
  };

  const handleSearchKeyUp = (event: any) => {
    if (event.key === 'Enter') {
      console.log(`Start search with "${event.target.value}"`);
    }
  };

  const handleSorting = (sortBy: any) => {
    setSortBy(sortBy);
  };

  const handleChangePage = (page: number) => {
    setCurrentPage(page);
  };

  const handleChangePageSize = (value: number) => {
    setCurrentPage(1);
    setPageSize(value);
  };

  const handleSelect = (value: any) => {
    const selectedRows = value.map((item: any) => item.original);
    setSelectingRows(selectedRows);
  };

  const openAddProductDialog = () => {
    console.log('Open add product dialog ');
  };

  return (
    <Box p={2} style={{ background: '#ffffff' }}>
      <Typography variant="h6">Setup products</Typography>
      <Box pt={3}>
        <Grid container justify="space-between" alignItems="center">
          <Grid item>
            <Grid container>
              <Grid item>
                <InputUI
                  id="campaign-searchbox"
                  placeholder="Search"
                  value={searchText}
                  onChange={handleChangeSearchText}
                  onKeyUp={handleSearchKeyUp}
                  startAdornment={
                    <InputAdornment position="start">
                      <SearchIcon fontSize="small" />
                    </InputAdornment>
                  }
                  style={{ width: 320 }}
                  labelWidth={0}
                  autoComplete="off"
                ></InputUI>
              </Grid>
              <Grid item>
                <MultiSelectUI
                  prefix="Category"
                  suffix="categories"
                  items={categories}
                  selectedItems={selectedCategories}
                  onSaveChange={(value: any) =>
                    setSelectedCategories(value)
                  }
                />
              </Grid>
            </Grid>
          </Grid>
          <Grid item>
            {tableRows.length > 0 && (
              <ButtonUI
                size="small"
                label="Add products"
                colorButton={COLORS.COMMON.GRAY}
                iconLeft={<AddIcon />}
                onClick={openAddProductDialog}
              ></ButtonUI>
            )}
          </Grid>
        </Grid>
      </Box>
      <Box py={1}>
        <Grid container>
          <Grid item xs={12}>
            <TableUI
              columns={headers}
              rows={tableRows}
              onSelect={handleSelect}
              onSort={handleSorting}
              resultTotal={resultTotal}
              page={currentPage}
              pageSize={pageSize}
              onChangePage={handleChangePage}
              onChangePageSize={handleChangePageSize}
              noData={
                <Box py={6}>
                  <img src={NoProduct} width="200" height="200" />
                  <Typography variant="h6" style={{ margin: 'auto' }}>
                    Add your amazing products
                  </Typography>
                  <Typography
                    variant="subtitle2"
                    style={{ margin: '16px auto' }}
                  >
                    Here’s where you would add product to your
                    campaign.
                  </Typography>
                  <ButtonUI
                    size="small"
                    label="Add products"
                    colorButton={COLORS.COMMON.GRAY}
                    onClick={openAddProductDialog}
                  ></ButtonUI>
                </Box>
              }
            />
          </Grid>
        </Grid>
      </Box>
      <Divider />
      <Box py={2}>
        <Grid
          container
          alignItems="center"
          justify="space-between"
          spacing={1}
        >
          <Grid item>
            <ButtonUI
              size="small"
              label="Back to previous step"
              colorButton={COLORS.COMMON.GRAY}
            ></ButtonUI>
          </Grid>
          <Grid item>
            <ButtonUI size="small" label="Create campaign"></ButtonUI>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};
