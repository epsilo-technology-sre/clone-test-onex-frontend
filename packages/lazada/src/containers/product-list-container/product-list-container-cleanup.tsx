import { COLORS } from '@ep/lazada/src/constants';
import NoProduct from '@ep/shopee/src/images/no-product.svg';
import { ButtonUI } from '@ep/shopee/src/components/common/button';
import { MultiSelectUI } from '@ep/shopee/src/components/common/multi-select';
import { TableUI } from '@ep/shopee/src/components/common/table';
import { InputUI } from '@ep/shopee/src/components/create-campaign/common';
import {
  Box,
  Divider,
  Grid,
  InputAdornment,
  Typography,
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import SearchIcon from '@material-ui/icons/Search';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { GET_CATEGORIES } from '../../redux/actions';
import { initColumns } from './columns';

export const ProductListContainer = (props: any) => {
  const dispatch = useDispatch();
  const { categories = [] } = useSelector(({ categories }: any) => {
    return { categories };
  });

  const getCategories = (payload: any) =>
    dispatch(GET_CATEGORIES.START(payload));
  // const updateSelectedProducts = (payload: any) => dispatch(UPDATE_SELECTED_PRODUCTS(payload));

  useEffect(() => {
    getCategories({ shop_eid: 4110 });
  }, []);

  const [searchText, setSearchText] = useState('');
  const [selectedCategories, setSelectedCategories] = useState(
    categories,
  );

  const [tableRows, setTableRows] = useState([]);
  const [pageSize, setPageSize] = useState(10);
  const [currentPage, setCurrentPage] = useState(1);
  const [resultTotal, setResultTotal] = useState(0);
  const [sortBy, setSortBy] = useState([]);

  const headers = initColumns({
    onAddKeyword: (value: any) => {
      console.log('On add keywords', value);
    },
    onDelete: (value: any) => {
      console.info('delete product');
    },
  });

  const handleChangeSearchText = (event: any) => {
    setSearchText(event.target.value);
  };

  const handleSearchKeyUp = (event: any) => {
    if (event.key === 'Enter') {
      console.log(`Start search with "${event.target.value}"`);
    }
  };

  const handleSorting = (sortBy: any) => {
    setSortBy(sortBy);
  };

  const handleChangePage = (page: number) => {
    setCurrentPage(page);
  };

  const handleChangePageSize = (value: number) => {
    setCurrentPage(1);
    setPageSize(value);
  };

  const handleSelect = (value: any) => {
    const selectedRows = value.map((item: any) => item.original);
    console.info('handle select');
  };

  const openAddProductDialog = () => {
    console.log('Open add product dialog ');
  };

  return (
    <Box p={2} style={{ background: '#ffffff' }}>
      <Typography variant="h6">Setup products</Typography>
      <Box pt={3}>
        <Grid container justify="space-between" alignItems="center">
          <Grid item>
            <Grid container>
              <Grid item>
                <InputUI
                  id="campaign-searchbox"
                  placeholder="Search"
                  value={searchText}
                  onChange={handleChangeSearchText}
                  onKeyUp={handleSearchKeyUp}
                  startAdornment={
                    <InputAdornment position="start">
                      <SearchIcon fontSize="small" />
                    </InputAdornment>
                  }
                  style={{ width: 320 }}
                  labelWidth={0}
                  autoComplete="off"
                ></InputUI>
              </Grid>
              <Grid item>
                <MultiSelectUI
                  prefix="Category"
                  suffix="categories"
                  items={categories}
                  selectedItems={selectedCategories}
                  onSaveChange={(value: any) =>
                    setSelectedCategories(value)
                  }
                />
              </Grid>
            </Grid>
          </Grid>
          <Grid item>
            {tableRows.length > 0 && (
              <ButtonUI
                size="small"
                label="Add products"
                colorButton={COLORS.COMMON.GRAY}
                iconLeft={<AddIcon />}
                onClick={openAddProductDialog}
              ></ButtonUI>
            )}
          </Grid>
        </Grid>
      </Box>
      <Box py={1}>
        <Grid container>
          <Grid item xs={12}>
            <TableUI
              columns={headers}
              rows={tableRows}
              onSelect={handleSelect}
              onSort={handleSorting}
              resultTotal={resultTotal}
              page={currentPage}
              pageSize={pageSize}
              onChangePage={handleChangePage}
              onChangePageSize={handleChangePageSize}
              noData={
                <Box py={6}>
                  <img src={NoProduct} width="200" height="200" />
                  <Typography variant="h6" style={{ margin: 'auto' }}>
                    Add your amazing products
                  </Typography>
                  <Typography
                    variant="subtitle2"
                    style={{ margin: '16px auto' }}
                  >
                    Here’s where you would add product to your
                    campaign.
                  </Typography>
                  <ButtonUI
                    size="small"
                    label="Add products"
                    colorButton={COLORS.COMMON.GRAY}
                    onClick={openAddProductDialog}
                  ></ButtonUI>
                </Box>
              }
            />
          </Grid>
        </Grid>
      </Box>
      <Divider />
      <Box py={2}>
        <Grid
          container
          alignItems="center"
          justify="space-between"
          spacing={1}
        >
          <Grid item>
            <ButtonUI
              size="small"
              label="Back to previous step"
              colorButton={COLORS.COMMON.GRAY}
            ></ButtonUI>
          </Grid>
          <Grid item>
            <ButtonUI size="small" label="Create campaign"></ButtonUI>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};
