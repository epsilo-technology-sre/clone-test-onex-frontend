import { ModalCreateRule } from '@ep/shopee/src/components/common/modal-create-rule/modal-create-rule';
import { Snackbar } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { actions } from '../../redux/actions';
import { MarketingAdvertisingState } from '../../redux/reducers';

const availActionKeys = ['resume_product', 'pause_product'];

export function RuleProductCreate(props: {
  shopId: number;
  products: { product_id: number; product_name: string }[];
  isOpen: boolean;
  onClose: Function;
  onSubmitSuccess: Function;
  onOpenExistingRule: Function;
}) {
  const {
    actionList,
    metricList,
    isLoading = { status: false, error: null },
  } = useSelector((state: MarketingAdvertisingState) => {
    return {
      actionList: (state.rulesActionList || []).filter(
        (i) => availActionKeys.indexOf(i.value) > -1,
      ),
      metricList: state.rulesMetricList,
      isLoading: state.nextLoading[actions.createProductRule.type()],
    };
  });

  const dispatch = useDispatch();
  const [rules, setRules] = React.useState([]);
  const [products, setProducts] = React.useState([]);
  const [submitStatus, setSubmitStatus] = React.useState<0 | 1 | 2>(
    0,
  );

  React.useEffect(() => {
    if (props.isOpen) {
      dispatch(actions.getRuleActionMetrics());
    }
  }, [props.isOpen]);

  React.useEffect(() => {
    setProducts(props.products);
  }, [props.products]);

  React.useEffect(() => {
    if (isLoading.status) {
      setSubmitStatus(1);
    }
    if (
      submitStatus === 1 &&
      isLoading.status === false &&
      !isLoading.error
    ) {
      setSubmitStatus(2);
    }

    if (submitStatus === 2) {
      props.onSubmitSuccess();
    }
  }, [isLoading, submitStatus]);

  const onSubmitRule = React.useCallback(
    (rule: any) => {
      rule.listRules = rule.listRules.map((r) => ({
        ...r,
        conditionPeriod: r.period,
        typeOfPerformance: r.metric_code,
        operator: r.operator_code,
      }));
      dispatch(
        actions.createProductRule({
          rule,
          products: products,
          shopId: props.shopId,
        }),
      );
    },
    [products],
  );
  if (!actionList || !metricList) return null;

  return (
    <React.Fragment>
      <ModalCreateRule
        open={props.isOpen}
        rules={rules}
        products={props.products}
        onAddRule={(r: any) => {
          setRules(r);
        }}
        onSubmit={onSubmitRule}
        onClose={props.onClose}
        setProduct={(p: any[]) => {
          setProducts(p);
        }}
        actionList={actionList}
        actionMetricList={metricList}
        isSubmitting={isLoading.status}
        onOpenExistingRule={props.onOpenExistingRule}
      />
      {isLoading.error && (
        <Snackbar
          anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
          open={!!isLoading.error}
          autoHideDuration={60000}
        >
          <Alert hidden={!isLoading.error} severity="error">
            {isLoading.error.message}
          </Alert>
        </Snackbar>
      )}
    </React.Fragment>
  );
}
