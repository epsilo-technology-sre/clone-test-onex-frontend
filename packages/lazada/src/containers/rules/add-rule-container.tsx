import { ModalAddRule } from '@ep/shopee/src/components/common/modal-add-rule';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { actions } from '../../redux/actions';
import { MarketingAdvertisingState } from '../../redux/reducers';

const availActionKeys = ['resume_product', 'pause_product'];

export const AddRuleContainer = (props: any) => {
  const {
    open,
    products,
    shopEid,
    onClose,
    onSubmit,
    setProduct,
    handleCreate,
  } = props;

  const { ruleList, actionList, metricList } = useSelector(
    (state: MarketingAdvertisingState) => {
      return {
        actionList: (state.rulesActionList || []).filter(
          (i) => availActionKeys.indexOf(i.value) > -1,
        ),
        metricList: state.rulesMetricList,
        ruleList: state.existProductRuleList,
      };
    },
  );
  const dispatch = useDispatch();

  React.useEffect(() => {
    if (open) {
      dispatch(
        actions.getExistProductRules({
          productId: [],
          shopEid,
        }),
      );
    }
  }, [open]);

  return (
    <div>
      <ModalAddRule
        open={open}
        products={products}
        ruleList={ruleList}
        actionList={actionList}
        metricList={metricList}
        shopEid={shopEid}
        onSubmit={onSubmit}
        onClose={onClose}
        handleCreate={handleCreate}
        setProduct={setProduct}
      ></ModalAddRule>
    </div>
  );
};
