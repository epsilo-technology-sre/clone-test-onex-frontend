import {
  EllipsisCell,
  ListRuleCell,
} from '@ep/shopee/src/components/common/table-cell/table-cell';

export const initColumns = () => {
  return [
    {
      Header: 'Rule',
      accessor: (row: any) => ({ value: row.ruleName }),
      width: 300,
      disableSortBy: true,
      Cell: EllipsisCell,
    },
    {
      Header: 'Action',
      id: 'action',
      accessor: 'action',
      disableSortBy: true,
      width: 250,
    },
    {
      Header: 'Create',
      id: 'create',
      accessor: 'create',
      disableSortBy: true,
      width: 250,
    },
    {
      Header: 'Condition',
      id: 'condition',
      accessor: (row: any) => row,
      width: 200,
      Cell: ListRuleCell,
      disableSortBy: true,
    },
  ];
};
