import { ModalAddRuleKeywords } from '@ep/shopee/src/components/common/modal-add-rule-keywords';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { actions } from '../../redux/actions';
import { MarketingAdvertisingState } from '../../redux/reducers';

const availActionKeys = [
  'activate_product_keyword',
  'deactivate_product_keyword',
  'increase_bidding_price',
  'decrease_bidding_price',
];

export const AddRuleKeywordContainer = (props: any) => {
  const {
    open,
    shopEid,
    currency,
    products,
    keywords,
    selectedKeywords,
    onClose,
    onSubmit,
    setKeywords,
    handleCreate,
  } = props;

  const { ruleList, actionList, metricList } = useSelector(
    (state: MarketingAdvertisingState) => {
      return {
        actionList: (state.rulesActionList || []).filter(
          (i) => availActionKeys.indexOf(i.value) > -1,
        ),
        metricList: state.rulesMetricList,
        ruleList: state.existKeywordRuleList,
      };
    },
  );

  const dispatch = useDispatch();

  React.useEffect(() => {
    if (open) {
      dispatch(
        actions.getExistKeywordRules({
          productId: [],
          shopEid,
        }),
      );
    }
  }, [open]);

  return (
    <div>
      <ModalAddRuleKeywords
        open={open}
        onSubmit={onSubmit}
        handleCreate={handleCreate}
        onClose={onClose}
        keywords={keywords}
        products={products}
        setKeywords={setKeywords}
        selectedKeywords={selectedKeywords}
        currency={currency}
        shopEid={shopEid}
        matchType={'BROAD_MATCH'}
        ruleList={ruleList}
        actionList={actionList}
        metricList={metricList}
        // matchType={matchType.length > 0 ? 'BROAD_MATCH' : 'EXACT_MATCH'}
      />
    </div>
  );
};
