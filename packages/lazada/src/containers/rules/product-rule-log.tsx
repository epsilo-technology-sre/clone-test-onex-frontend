import { LogsRule } from '@ep/shopee/src/components/common/logs-rule';
import { differenceWith, isEqual } from 'lodash';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RULE_PERIODS } from '../../constants';
import { actions } from '../../redux/actions';
import { MarketingAdvertisingState } from '../../redux/reducers';
import { initColumns } from './logs-rule-columns';

const actionList = [
  { id: 'resume_product', text: 'Resume Product' },
  { id: 'pause_product', text: 'Pause Product' },
  { id: 'modify_rule', text: 'Modify Rule' },
  { id: 'delete_rule', text: 'Delete Rule' },
];

export const ProductRuleLog = (props: {
  open: boolean;
  shopEid: number;
  parentName: string;
  itemId: number;
  itemName: string;
  onClose: Function;
  currency?: string;
}) => {
  const {
    open,
    shopEid,
    parentName,
    itemId,
    itemName,
    onClose,
    currency = 'VND',
  } = props;

  const {
    searchText,
    timeRange,
    selectedActions,
    logList,
    sort,
    pagination,
  } = useSelector((state: MarketingAdvertisingState) => {
    return {
      searchText: state.ruleLogFilters.searchText,
      timeRange: {
        startDate: state.ruleLogFilters.timeRange.start,
        endDate: state.ruleLogFilters.timeRange.end,
      },
      selectedActions: state.ruleLogFilters.actions,
      logList: state.ruleLogs.items,
      sort: state.ruleLogs.sort,
      pagination: state.ruleLogs.pagination,
    };
  });
  const dispatch = useDispatch();

  React.useEffect(() => {
    if (open) {
      dispatch(actions.resetRuleLog());
      dispatch(
        actions.updateRuleLogFilter({
          actions: actionList,
        }),
      );
      getLogsData({ actions: actionList.map((i) => i.id) });
    }
  }, [open]);

  const getLogsData = (argParams: any = {}) => {
    dispatch(
      actions.getRuleLogs({
        itemId,
        shopEid,
        itemType: 'campaign_product',
        search: searchText,
        from: timeRange.startDate,
        to: timeRange.endDate,
        actions: selectedActions.map((i) => i.id),
        // orderBy: sort.sortBy,
        // orderMode: sort.sortMode,
        page: pagination.page,
        limit: pagination.limit,
        ...argParams,
      }),
    );
  };

  const ruleLogs = React.useMemo(() => {
    return (logList || []).map((l) => {
      return {
        ...l,
        currency,
        condition: l.condition.map((c) => ({
          ...c,
          typeOfPerformance: c.metricName,
          period: RULE_PERIODS.find((p) => p.value === c.period)
            ?.name,
        })),
      };
    });
  }, [logList]);

  const handleSearchKeyUp = (event: any) => {
    if (event.key === 'Enter') {
      dispatch(
        actions.updateRuleLogFilter({
          searchText: event.target.value,
        }),
      );
      getLogsData({ search: event.target.value });
    }
  };

  const handleChangeDateRange = ({ startDate, endDate }: any) => {
    const newTimeRange = {
      start: startDate.format('YYYY-MM-DD'),
      end: endDate.format('YYYY-MM-DD'),
    };
    dispatch(
      actions.updateRuleLogFilter({ timeRange: newTimeRange }),
    );
    getLogsData({ from: newTimeRange.start, to: newTimeRange.end });
  };

  const handleChangeActions = (selected: any) => {
    dispatch(
      actions.updateRuleLogFilter({
        actions: selected,
      }),
    );
    getLogsData({ actions: selected.map((i) => i.id) });
  };

  const handleSorting = (sortBy: any) => {
    const isDifference =
      differenceWith(sortBy, sort?.sortBy, isEqual).length > 0;
    if (isDifference) {
      getLogsData();
    }
  };

  const handleChangePage = (page: number) => {
    getLogsData({ page });
  };

  const handleChangePageSize = (value: number) => {
    getLogsData({ limit: value });
  };

  return (
    <div>
      <LogsRule
        open={open}
        handleClose={onClose}
        handleChangeDateRange={handleChangeDateRange}
        handleChangeActions={handleChangeActions}
        actions={actionList}
        selectedTimeRange={timeRange}
        selectedActions={selectedActions}
        tableHeaders={initColumns()}
        logRule={ruleLogs}
        pagination={pagination}
        onSorting={handleSorting}
        onChangePage={handleChangePage}
        onChangePageSize={handleChangePageSize}
        handleSearchKeyUp={handleSearchKeyUp}
        ruleInfo={{
          parentItem: {
            name: 'Shop',
            value: parentName,
          },
          item: {
            id: itemId,
            name: 'Product',
            value: itemName,
          },
        }}
      />
    </div>
  );
};
