import {
  activatorProducts,
  updateProductBudget,
  updateProductsPrice,
} from '@ep/lazada/src/api/api';
import { LoadingUI } from '@ep/one/src/components/common/Loading';
import { ModalAddRuleName } from '@ep/shopee/src/components/common/modal-add-rule-name';
import { ModalBulkActionProduct } from '@ep/shopee/src/components/common/modal-bulk-actions/modal-bulk-action-products';
import { ModalConfirmUI } from '@ep/shopee/src/components/common/modal-confirm';
import { ProductView } from '@ep/shopee/src/components/products';
import { Snackbar } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import { differenceWith, get, isEqual, union } from 'lodash';
import React, { useEffect, useState } from 'react';
import { Provider, useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import { PRODUCT_STATUS, TABS } from '../../constants';
import {
  actions,
  CHANGE_TAB,
  GET_PRODUCTS,
  SET_FILTER_PRODUCT_STATUS,
  SET_PAGINATION,
  SET_SEARCH_PRODUCT,
  UPDATE_SELECTED_PRODUCTS,
} from '../../redux/actions';
import { rootSaga } from '../../redux/create-campaign/actions';
import { reducer } from '../../redux/create-campaign/reducers';
import { AddProductKeywordContainer } from '../add-product-keyword-container';
import { AddRuleContainer } from '../rules/add-rule-container';
import { RuleProductCreate } from '../rules/product-create';
import { ListRuleProduct } from '../rules/product-list';
import { ProductRuleLog } from '../rules/product-rule-log';
import { PRODUCT_COLUMNS } from './product-columns';

export const ProductContainer = () => {
  const addKeywordStore = makeStore();

  const ref = React.useRef({
    selectedCampaigns: [],
    selectedProducts: null,
    products: null,
    pagination: null,
    sortBy: [],
  });
  const {
    products,
    pagination,
    globalFilters,
    selectedCampaigns,
    selectedProducts,
    loading,
    searchProduct,
    filterProductStatus,
  } = useSelector((state: any) => {
    const {
      products,
      pagination,
      globalFilters,
      selectedProducts,
      selectedCampaigns,
      loading,
      searchProduct,
      filterProductStatus,
    } = state;
    ref.current = {
      ...ref.current,
      selectedProducts,
      selectedCampaigns,
      products,
      pagination,
    };
    return {
      products,
      pagination,
      globalFilters,
      selectedCampaigns,
      selectedProducts,
      loading,
      searchProduct,
      filterProductStatus,
    };
  });

  useEffect(() => {
    if (globalFilters && globalFilters.shops?.length > 0) {
      getProductData();
    }
  }, [globalFilters.timeRange, globalFilters.updateCount]);

  const [openModalRule, setOpenModalRule] = useState(false);
  const [newRule, setNewRules] = useState<any>({
    open: false,
  });

  const [ruleLog, setRuleLog] = React.useState({
    isOpen: false,
    shopEid: 0,
    parentName: '',
    itemId: 0,
    itemName: '',
    currency: '',
  });

  const [deleteRulesModal, setDeleteRulesModal] = React.useState({
    open: false,
    items: [],
  });

  const dispatch = useDispatch();
  const [ruleProductCreate, setRuleProductCreate] = React.useState({
    isOpen: false,
    products: [],
    shopId: null,
  });

  const [addProductModal, setAddKeywordModal] = React.useState({
    isOpen: false,
    products: [],
    shopId: null,
  });

  const [
    openModalBulkAction,
    setOpenModalBulkAction,
  ] = React.useState({
    open: false,
    products: [],
    currency: '',
  });

  const [showActivator, setShowActivator] = React.useState<any>({
    show: false,
    focusProduct: null,
  });

  const [notification, setNotification] = React.useState<any>({
    open: false,
  });

  const [addModalType, setAddModalType] = React.useState('');

  const [listProductRules, setListProductRules] = React.useState({
    isOpen: false,
    allowUpdate: true,
    shopEid: 0,
    productId: 0,
    productName: '',
  });

  const getProducts = (payload: any) =>
    dispatch(GET_PRODUCTS.START(payload));
  const updateSelectedProducts = (payload: any) =>
    dispatch(UPDATE_SELECTED_PRODUCTS(payload));
  const changeTab = (tab: string) => {
    dispatch(CHANGE_TAB(tab));
  };

  const updatePagination = (paging: any) =>
    dispatch(SET_PAGINATION({ pagination: paging }));

  const updateSearchProduct = (query: any) =>
    dispatch(SET_SEARCH_PRODUCT({ searchProduct: query }));

  const updateFilterProductStatus = (status: any) =>
    dispatch(
      SET_FILTER_PRODUCT_STATUS({ filterProductStatus: status }),
    );

  const handleSelectAll = React.useCallback((checked) => {
    if (
      checked &&
      get(ref.current, 'selectedProducts.length', 0) === 0
    ) {
      updateSelectedProducts(
        (ref.current.selectedProducts || []).concat(
          ref.current.products,
        ),
      );
    } else {
      updateSelectedProducts([]);
    }
  }, []);
  const getRowId = React.useCallback((row) => {
    return String(row.campaign_product_id);
  }, []);

  const handleSelectItem = React.useMemo(() => {
    return (item: any, checked: boolean) => {
      if (checked) {
        updateSelectedProducts(
          (ref.current.selectedProducts || []).concat(item),
        );
      } else {
        updateSelectedProducts(
          (ref.current.selectedProducts || []).filter(
            (c: any) => getRowId(c) !== getRowId(item),
          ),
        );
      }
    };
  }, [selectedProducts]);

  const selectedIds = React.useMemo(() => {
    return (selectedProducts || []).map(getRowId);
  }, [selectedProducts]);

  const onClickSubjectLabel = (product: any) => {
    let newProduct = ref.current.selectedProducts || [];
    const checkExist = newProduct.findIndex(
      (item: any) => item.product_eid === product.product_eid,
    );
    if (checkExist === -1) {
      newProduct = newProduct.concat(product);
    }
    updateSelectedProducts(newProduct);
    changeTab(TABS.KEYWORD);
  };

  const getProductData = (argParams?: any) => {
    if (globalFilters) {
      let campaignEids = '';
      if (ref.current.selectedCampaigns) {
        campaignEids = ref.current.selectedCampaigns
          .map((item: any) => item.campaign_eid)
          .join(',');
      }

      const urlParams = new URLSearchParams(location.search);
      let shopIds = urlParams.get('shopId');
      if (globalFilters.shops) {
        shopIds = globalFilters.shops
          .map((item: any) => item.id)
          .join(',');
      }

      const params: any = {
        shop_eids: shopIds || 4110,
        from: globalFilters.timeRange.start,
        to: globalFilters.timeRange.end,
        limit: ref.current.pagination.limit,
        page: ref.current.pagination.page,
        value_filter: searchProduct,
        campaign_eids: campaignEids,
        status: filterProductStatus,
        ...argParams,
      };

      if (ref.current.sortBy.length > 0) {
        params.order_by = ref.current.sortBy[0].id;
        params.order_mode = ref.current.sortBy[0].desc
          ? 'desc'
          : 'asc';
      }

      if (params.status === 'All') {
        params.status = '';
      }

      getProducts(params);
    }
  };

  const handleViewRuleLog = (row) => {
    setRuleLog({
      isOpen: true,
      shopEid: row.shop_eid,
      parentName: row.shop_name,
      itemId: row.productId,
      itemName: row.productName,
      currency: row.currency,
    });
  };

  const handleChangeSearchText = (value: any) => {
    updateSearchProduct(value);
    updatePagination({
      page: 1,
      limit: pagination.limit,
      item_count: pagination.item_count,
      page_count: pagination.page_count,
    });
    getProductData({ value_filter: value });
  };

  const handleChangeStatus = (value: any) => {
    updatePagination({
      page: 1,
      limit: pagination.limit,
      item_count: pagination.item_count,
      page_count: pagination.page_count,
    });
    updateFilterProductStatus(value);
    getProductData({ status: value });
  };

  const handleSorting = (sortBy: any) => {
    const isDifference =
      differenceWith(sortBy, ref.current.sortBy, isEqual).length > 0;
    if (isDifference) {
      ref.current.sortBy = sortBy;
      getProductData();
    }
  };

  const handleChangePage = (page: number) => {
    updatePagination({
      page: page,
      limit: pagination.limit,
      item_count: pagination.item_count,
      page_count: pagination.page_count,
    });
    getProductData();
  };

  const handleChangePageSize = (value: number) => {
    updatePagination({
      page: 1,
      limit: value,
      item_count: pagination.item_count,
      page_count: pagination.page_count,
    });
    getProductData();
  };

  const statusList = React.useMemo(() => {
    const items = ['All'];
    for (const property in PRODUCT_STATUS) {
      items.push(PRODUCT_STATUS[property]);
    }
    return items;
  }, []);

  const handleAddNewRule = (product: any) => {
    setRuleProductCreate({
      ...ruleProductCreate,
      shopId: product.shop_eid,
      products: [
        {
          product_id: product.productId,
          product_name: product.productName,
        },
      ],
    });
    setOpenModalRule(true);
  };

  const handleAddingSuccess = () => {
    updatePagination({
      page: 1,
      limit: pagination.limit,
      item_count: pagination.item_count,
      page_count: pagination.page_count,
    });
    getProductData();
    handleCloseDialog();
  };

  const handleOpenAddKeyword = () => {
    const allowItems = selectedProducts.filter((i) => !i._isDisabled);
    if (allowItems.length > 0) {
      if (checkDiffCurrency(allowItems)) {
        toast.error('Selected products have the difference currency');
      } else {
        setAddModalType('AddKeyword');
        setAddKeywordModal({
          isOpen: true,
          shopId: allowItems[0].shop_eid,
          products: allowItems,
        });
      }
    } else {
      toast.error('Can not add keyword for ended products');
    }
  };

  const handleOpenAddProduct = () => {
    setAddModalType('AddProduct');
    setAddKeywordModal({
      isOpen: true,
      shopId: null,
      products: [],
    });
  };

  const handleCloseDialog = () => {
    setAddKeywordModal((state) => ({
      ...state,
      isOpen: false,
    }));
  };

  const updateProduct = async (field: string, value: any) => {
    const updateFn: any = {
      budget: updateProductBudget,
    };

    try {
      const response = await updateFn[field](value);
      const result = response.success;
      if (response.success) {
        toast.success(response.message);
        getProductData();
      } else {
        toast.error(response.message);
      }
      return result;
    } catch (e) {
      toast.error(e.message);
      return false;
    }
  };

  const handleActivator = (type: string, products) => {
    const isAllowed = products.every((i) => !i._isDisabled);
    if (isAllowed) {
      const activator: any = {
        active: {
          labelSubmit: 'Activate',
          title: 'Activate Product(s)?',
          content:
            'Activating Product will resume its Promoted ads and bidding keywords.',
          status: 1,
        },
        deactive: {
          labelSubmit: 'Deactivate',
          title: 'Deactivate Product(s)?',
          content:
            'Deactivating Product will pause its Promoted ads and bidding keywords.',
          status: 0,
        },
      };
      setShowActivator({
        ...showActivator,
        ...activator[type],
        focusProduct: products,
        type,
        show: true,
      });
    } else {
      toast.error('Can not modify ended products');
    }
  };

  const handleSubmit = async (selectedProducts: any[]) => {
    const shopEids = selectedProducts.map((item) => item.shop_eid);
    const campaignProductIds = selectedProducts.map(
      (item: any) => item.campaign_product_id,
    );

    setShowActivator({ ...showActivator, show: false });
    try {
      const res = await activatorProducts({
        shop_eids: [...new Set(shopEids)],
        campaign_product_ids: campaignProductIds,
        status: showActivator.status,
      });

      setNotification({ ...notification, ...res, open: true });
    } catch (error) {
      notification.message = error.message;
      setNotification({
        ...notification,
        open: true,
        success: false,
      });
    }
    await getProductData();
  };

  const handleSubmitModifyProducts = async ({
    total_budget,
    daily_budget,
  }: any) => {
    const shopEids = selectedProducts.map((item) => item.shop_eid);
    const campaignProductIds = selectedProducts.map(
      (item) => item.campaign_product_id,
    );
    try {
      const res = await updateProductsPrice({
        shop_eids: [...new Set(shopEids)],
        campaign_product_ids: campaignProductIds,
        total_budget,
        daily_budget,
      });
      setNotification({ ...notification, ...res, open: true });
    } catch (error) {
      notification.message = error.message;
      setNotification({ ...notification, open: true });
    }
    setOpenModalBulkAction({ ...openModalBulkAction, open: false });
    await getProductData();
  };

  const checkDiffCurrency = (items: any) => {
    return items.some((i) => i.currency !== items[0].currency);
  };

  const handleOpenModifyBudget = () => {
    const allowItems = selectedProducts.filter((i) => !i._isDisabled);
    if (allowItems.length > 0) {
      if (checkDiffCurrency(allowItems)) {
        toast.error('Selected products have the difference currency');
      } else {
        setOpenModalBulkAction({
          open: true,
          products: allowItems,
          currency: allowItems[0].currency,
        });
      }
    } else {
      toast.error('Can not modify ended products');
    }
  };

  const handleCloseNotification = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setNotification({ ...notification, open: false });
  };

  const showStatusPopup = (product: any, status: boolean) => {
    handleActivator(
      status ? 'deactive' : 'active',
      [].concat(product),
    );
  };

  const handleViewRule = (row) => {
    const allowStatus = [
      PRODUCT_STATUS.RUNNING,
      PRODUCT_STATUS.PAUSED,
      PRODUCT_STATUS.SCHEDULED,
    ];
    setListProductRules({
      isOpen: true,
      allowUpdate: allowStatus.includes(row.status),
      shopEid: row.shop_eid,
      productId: row.productId,
      productName: row.productName,
    });
  };

  const handleAddRuleSuccess = () => {
    getProductData();
    getRuleList();
    setRuleProductCreate((state) => ({
      ...state,
      isOpen: false,
    }));
  };

  const handleCloseAddRule = () => {
    setRuleProductCreate((state) => ({
      ...state,
      isOpen: false,
    }));
  };

  const getRuleList = () => {
    if (listProductRules.isOpen) {
      dispatch(
        actions.getProductRules({
          productId: listProductRules.productId,
          shopEid: listProductRules.shopEid,
        }),
      );
    }
  };

  const onSubmitRule = (rule: any) => {
    const objectEids = selectedProducts.map((p) => p.productId);
    const conditionJson = JSON.parse(rule.conditionJson);
    const changeDate =
      rule.fromDate === rule.timelineFrom &&
      rule.toDate === rule.timelineTo;
    const changeCondition =
      JSON.stringify(conditionJson) ===
      JSON.stringify(rule.conditions);
    if (changeCondition && changeDate) {
      dispatch(
        actions.addExistingRule({
          shopEid: rule.shopEid,
          featureCode: rule.featureCode,
          objectType: rule.objectType,
          objectEids,
          ruleId: rule.ruleId,
          callback: (rs) => {
            setNotification({
              open: true,
              message: rs.data,
              success: rs.success,
            });
            if (rs.success) {
              getProductData();
              getRuleList();
            }
          },
        }),
      );
    } else {
      setNewRules({
        open: true,
        rule: {
          ...rule,
          ruleName: rule.ruleName,
          listRules: rule.conditions,
        },
      });
    }
    setOpenModalRule(false);
  };

  const createNewRule = (rule: any) => {
    const postRule = {
      ...newRule.rule,
      ruleName: rule.ruleName,
    };
    const products = ruleProductCreate.products.map((p) => ({
      ...p,
      product_id: p.productId || p.product_id,
    }));
    dispatch(
      actions.createProductRule({
        rule: postRule,
        products,
        shopId: postRule.shopEid,
        callback: (rs) => {
          setNotification({
            open: true,
            message: rs.data,
            success: rs.success,
          });
        },
      }),
    );

    setNewRules({
      ...newRule,
      open: false,
    });
  };

  const handleOpenModalAddRule = () => {
    const allowItems = selectedProducts.filter((i) => !i._isDisabled);
    if (allowItems.length > 0) {
      let shopEid = allowItems.map((p) => p.shop_eid);
      shopEid = [...new Set(shopEid)];

      if (shopEid.length > 1) {
        setNotification({
          open: true,
          success: false,
          message: 'Please choose the products in one shop',
        });
      } else {
        setRuleProductCreate({
          ...ruleProductCreate,
          products: allowItems,
          shopId: shopEid[0],
        });
        setOpenModalRule(true);
      }
    } else {
      toast.error('Can not add rule with ended products');
    }
  };

  const handleDeleteRules = () => {
    setDeleteRulesModal({
      open: true,
      items: selectedProducts,
    });
  };

  const onSubmitDeleteRules = () => {
    setDeleteRulesModal({ ...deleteRulesModal, open: false });
    dispatch(
      actions.deleteAllProductRules({
        featureCode: 'M_LZD_SS',
        shopEids: union(
          deleteRulesModal.items.map((i) => i.shop_eid),
        ),
        itemIds: deleteRulesModal.items.map((i) => i.productId),
        callback: (rs) => {
          setNotification({
            open: true,
            message: rs.data,
            success: rs.success,
          });
          if (rs.success) {
            getProductData();
          }
        },
      }),
    );
  };

  const headers = React.useMemo(() => {
    const columns = [...PRODUCT_COLUMNS];
    columns[0].onCellClick = showStatusPopup;
    columns[1].onClickLabel = (row) => {
      onClickSubjectLabel(row);
    };

    const ruleCol = columns.find((i) => i.id === 'rule');
    ruleCol.onCellClick = (row: any) => {
      if (row.ruleCount) {
        handleViewRule(row);
      } else {
        handleAddNewRule(row);
      }
    };

    const logCol = columns.find((i) => i.id === 'ruleLog');
    logCol.onCellClick = (row: any) => {
      handleViewRuleLog(row);
    };

    return columns;
  }, [PRODUCT_COLUMNS]);

  return (
    <React.Fragment>
      <ProductView
        openModalAddrule={handleOpenModalAddRule}
        tableHeaders={headers}
        products={products}
        pagination={pagination}
        searchText={searchProduct}
        statusList={statusList}
        selectedStatus={filterProductStatus}
        selectedProducts={selectedProducts}
        handleSelectItem={handleSelectItem}
        handleSelectAll={handleSelectAll}
        getRowId={getRowId}
        selectedIds={selectedIds}
        onSearch={handleChangeSearchText}
        onChangeStatus={handleChangeStatus}
        onSorting={handleSorting}
        onChangePage={handleChangePage}
        onChangePageSize={handleChangePageSize}
        onAddProduct={handleOpenAddProduct}
        handleActivator={handleActivator}
        updateProduct={updateProduct}
        handleOpenAddKeyword={handleOpenAddKeyword}
        handleOpenModifyBudget={handleOpenModifyBudget}
        handleDeleteRules={handleDeleteRules}
      />
      {showActivator.show && (
        <ModalConfirmUI
          open={showActivator.show}
          onSubmit={() =>
            handleSubmit([].concat(showActivator.focusProduct))
          }
          onClose={() =>
            setShowActivator({ ...showActivator, show: false })
          }
          labelSubmit={showActivator.labelSubmit}
          title={showActivator.title}
          content={showActivator.content}
        />
      )}
      {deleteRulesModal.open && (
        <ModalConfirmUI
          open={deleteRulesModal.open}
          onSubmit={onSubmitDeleteRules}
          onClose={() =>
            setDeleteRulesModal({ ...deleteRulesModal, open: false })
          }
          labelSubmit="OK"
          title="Delete all rules?"
          content="All rules that you added on these products will be deleted."
        />
      )}
      {openModalBulkAction.open && (
        <ModalBulkActionProduct
          open={openModalBulkAction.open}
          setOpenModalBulkAction={(e: any) =>
            handleSubmitModifyProducts(e)
          }
          currency={openModalBulkAction.currency}
          products={openModalBulkAction.products}
          updateSelected={(row: any) => {
            updateSelectedProducts(row);
            setOpenModalBulkAction({
              ...openModalBulkAction,
              products: row,
            });
          }}
          onClose={() =>
            setOpenModalBulkAction({
              ...openModalBulkAction,
              open: false,
            })
          }
        />
      )}

      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={notification.open}
        onClose={handleCloseNotification}
        autoHideDuration={3000}
      >
        <Alert
          onClose={handleCloseNotification}
          elevation={6}
          variant="filled"
          severity={notification.success ? 'success' : 'error'}
        >
          {notification.message}
        </Alert>
      </Snackbar>

      <RuleProductCreate
        isOpen={ruleProductCreate.isOpen}
        shopId={ruleProductCreate.shopId}
        products={ruleProductCreate.products}
        onSubmitSuccess={handleAddRuleSuccess}
        onClose={handleCloseAddRule}
        onOpenExistingRule={() => {
          setOpenModalRule(true);
          setRuleProductCreate({
            ...ruleProductCreate,
            isOpen: false,
          });
        }}
      />
      <LoadingUI loading={loading} />
      <AddRuleContainer
        onSubmit={onSubmitRule}
        open={openModalRule}
        onClose={(e: any) => setOpenModalRule(e)}
        handleCreate={() => {
          setOpenModalRule(false);
          setRuleProductCreate({
            ...ruleProductCreate,
            isOpen: true,
          });
        }}
        products={ruleProductCreate.products}
        setProduct={(p: any) => {
          updateSelectedProducts(p);
          setRuleProductCreate({
            ...ruleProductCreate,
            products: p,
          });
        }}
        shopEid={ruleProductCreate.shopId}
      />
      <ModalAddRuleName
        open={newRule.open}
        onSubmit={createNewRule}
        onClose={(e: any) => setNewRules({ ...newRule, open: e })}
      />
      <ListRuleProduct
        allowUpdate={listProductRules.allowUpdate}
        isOpen={listProductRules.isOpen}
        shopEid={listProductRules.shopEid}
        productId={listProductRules.productId}
        productName={listProductRules.productName}
        onUpdateSuccess={getProductData}
        onClose={() => {
          setListProductRules((state) => ({
            ...state,
            isOpen: false,
          }));
        }}
        onAddRule={(keyword) => handleAddNewRule(keyword)}
      />

      <ProductRuleLog
        open={ruleLog.isOpen}
        shopEid={ruleLog.shopEid}
        parentName={ruleLog.parentName}
        itemId={ruleLog.itemId}
        itemName={ruleLog.itemName}
        onClose={() => {
          setRuleLog((state) => ({
            ...state,
            isOpen: false,
          }));
        }}
      ></ProductRuleLog>

      <Provider store={addKeywordStore}>
        <AddProductKeywordContainer
          addType={addModalType}
          isOpen={addProductModal.isOpen}
          shopId={addProductModal.shopId}
          products={addProductModal.products}
          onClose={handleCloseDialog}
          onAddingSuccess={handleAddingSuccess}
        ></AddProductKeywordContainer>
      </Provider>
    </React.Fragment>
  );
};

function makeStore() {
  const sagaMiddleware = createSagaMiddleware();
  let composeEnhancers = composeWithDevTools({
    name: 'lazada/create-campaign',
  });

  const store = createStore(
    reducer,
    composeEnhancers(applyMiddleware(sagaMiddleware)),
  );

  sagaMiddleware.run(rootSaga);

  return store;
}
