import { actions } from '@ep/lazada/src/redux/create-campaign/actions';
import { CreateCampaignState } from '@ep/lazada/src/redux/create-campaign/reducers';
import { Dialog, makeStyles } from '@material-ui/core';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AddKeywordExistCampaignContainer } from '../add-keyword-container';
import { AddProductExistCampaignContainer } from '../add-product-container';

const useStyle = makeStyles({
  dialogPaper: {
    maxHeight: 'calc(100% - 32px)',
  },
});

export const AddProductKeywordContainer = (props: any) => {
  const classes = useStyle();

  const dispatch = useDispatch();
  let { setupProduct, addKeyword } = useSelector(
    (state: CreateCampaignState) => {
      return {
        setupProduct: state.setupProduct,
        addKeyword: state.addKeyword,
      };
    },
  );

  const [hasPreviousStep, setHasPreviousStep] = React.useState(true);
  const [enableModifyBudget, setEnableModifyBudget] = React.useState(
    true,
  );

  React.useEffect(() => {
    dispatch(actions.openAddProductModal({ visible: false }));
    dispatch(actions.openAddKeywordModal({ visible: false }));
  }, []);

  React.useEffect(() => {
    if (props.isOpen) {
      if (props.addType === 'AddProduct') {
        dispatch(actions.setFlow({ flow: 'addProduct' }));
        dispatch(actions.openAddProductModal({ visible: true }));
      } else {
        dispatch(actions.setFlow({ flow: 'addKeyword' }));

        if (props.products.length > 0) {
          dispatch(
            actions.setProductsForAddKeyword({
              products: props.products,
            }),
          );
          dispatch(
            actions.addProductSelectShop({ shopId: props.shopId }),
          );
          dispatch(
            actions.setShopCurrency({
              currency: props.products[0].currency,
            }),
          );
          dispatch(actions.openAddKeywordModal({ visible: true }));
          setHasPreviousStep(false);
        } else {
          setEnableModifyBudget(false);
          dispatch(actions.openAddProductModal({ visible: true }));
        }
      }
    }
  }, [props.isOpen]);

  React.useEffect(() => {
    if (
      !setupProduct.modalAddProductVisible &&
      !addKeyword.modalAddKeywordVisible
    ) {
      props.onClose();
    }
  }, [
    setupProduct.modalAddProductVisible,
    addKeyword.modalAddKeywordVisible,
  ]);

  return (
    <>
      {setupProduct.modalAddProductVisible && (
        <Dialog
          open={setupProduct.modalAddProductVisible}
          fullWidth
          maxWidth={'xl'}
          classes={{ paper: classes.dialogPaper }}
        >
          <AddProductExistCampaignContainer
            enableModifyBudget={enableModifyBudget}
          ></AddProductExistCampaignContainer>
        </Dialog>
      )}
      {addKeyword.modalAddKeywordVisible && (
        <Dialog
          open={addKeyword.modalAddKeywordVisible}
          fullWidth
          maxWidth={'xl'}
          classes={{ paper: classes.dialogPaper }}
        >
          <AddKeywordExistCampaignContainer
            hasPreviousStep={hasPreviousStep}
            onAddingSuccess={props.onAddingSuccess}
          ></AddKeywordExistCampaignContainer>
        </Dialog>
      )}
    </>
  );
};
