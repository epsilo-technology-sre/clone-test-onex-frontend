import LazadaTheme from '@ep/lazada/src/lazada-theme';
import { startMock } from '@ep/one/src/mock';
import { ThemeProvider } from '@material-ui/core/styles';
import React, { useState } from 'react';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import { rootSaga } from '../../redux/create-campaign/actions';
import {
  CreateCampaignState,
  reducer,
} from '../../redux/create-campaign/reducers';
import { CreateCampaignContainer } from './campaign-creating';
import { storyResource } from './stories-resources';
import { ToastContainer } from 'react-toastify';

export default {
  title: 'Lazada / Create Campaign Full Flow',
};

export function Primary() {
  let [mocked, setMocked] = useState(0);
  startMock().then(() => {
    setMocked(1);
  });
  const store = makeStore();
  if (mocked) {
    return (
      <Provider store={store}>
        <ThemeProvider theme={LazadaTheme}>
          <CreateCampaignContainer />
        </ThemeProvider>
        <ToastContainer
          closeOnClick={false}
          hideProgressBar={true}
          limit={3}
        />
      </Provider>
    );
  } else {
    return 'loading...';
  }
}

function makeStore() {
  const sagaMiddleware = createSagaMiddleware();
  let composeEnhancers = composeWithDevTools({
    name: 'lazada/create-campaign',
  });

  const store = createStore(
    reducer,
    demoState(),
    composeEnhancers(applyMiddleware(sagaMiddleware)),
  );

  sagaMiddleware.run(rootSaga);

  return store;
}

function demoState(): CreateCampaignState {
  return storyResource;
}
