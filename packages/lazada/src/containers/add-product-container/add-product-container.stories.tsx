import { ThemeProvider } from '@material-ui/core/styles';
import React from 'react';
import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import LazadaTheme from '../../lazada-theme';
import { store } from '../../redux/store';
import { AddProductContainer } from './add-product-container';

export default {
  title: 'Lazada / Container',
};

export const AddProductDialog = () => {
  return (
    <Provider store={store}>
      <ThemeProvider theme={LazadaTheme}>
        <AddProductContainer />
        <ToastContainer closeOnClick={false} hideProgressBar={true} />
      </ThemeProvider>
    </Provider>
  );
};
