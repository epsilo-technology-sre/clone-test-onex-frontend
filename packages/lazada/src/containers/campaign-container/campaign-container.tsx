import {
  activatorCampaigns,
  downloadFile,
  updateCampaignBudget,
  updateCampaignName,
  updateCampaignPrice,
  updateCampaignTimeline,
} from '@ep/lazada/src/api/api';
import { CampaignView } from '@ep/shopee/src/components/campaigns';
import { ModalBulkActionCampaign } from '@ep/shopee/src/components/common/modal-bulk-actions/modal-bulk-action-campaigns';
import { ModalConfirmUI } from '@ep/shopee/src/components/common/modal-confirm';
import { Box, Snackbar } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import { differenceWith, get, isEqual } from 'lodash';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import { CAMPAIGN_STATUS, TABS } from '../../constants';
import {
  CHANGE_TAB,
  GET_CAMPAIGNS,
  SET_FILTER_CAMPAIGN_STATUS,
  SET_PAGINATION,
  UPDATE_SELECTED_CAMPAIGNS,
} from '../../redux/actions';
import { CAMPAIGN_COLUMNS } from './campaign-columns';

export function CampaignContainer() {
  const ref = React.useRef({
    selectedCampaigns: null,
    campaigns: null,
    pagination: null,
    searchText: '',
    sortBy: [],
  });

  const {
    campaigns,
    pagination,
    globalFilters,
    selectedCampaigns,
    filterCampaignStatus,
  } = useSelector(
    ({
      campaigns,
      pagination,
      globalFilters,
      selectedCampaigns,
      filterCampaignStatus,
    }: any) => {
      ref.current = {
        ...ref.current,
        selectedCampaigns,
        campaigns,
        pagination,
      };
      return {
        campaigns,
        pagination,
        globalFilters,
        selectedCampaigns,
        filterCampaignStatus,
      };
    },
  );

  const [showActivator, setShowActivator] = React.useState({
    show: false,
    focusCampaign: null,
  });

  const [
    openModalBulkAction,
    setOpenModalBulkAction,
  ] = React.useState({
    open: false,
    items: [],
  });

  const [notification, setNotification] = React.useState({
    open: false,
  });

  const statusList = React.useMemo(() => {
    const items = ['All'];
    for (const property in CAMPAIGN_STATUS) {
      items.push(CAMPAIGN_STATUS[property]);
    }
    return items;
  }, []);

  useEffect(() => {
    if (globalFilters && globalFilters.shops?.length > 0) {
      getCampaignData();
    }
  }, [globalFilters.timeRange, globalFilters.updateCount]);

  const dispatch = useDispatch();

  const getCampaigns = (payload: any) =>
    dispatch(GET_CAMPAIGNS.START(payload));
  const updateSelectedCampaigns = (payload: any) =>
    dispatch(UPDATE_SELECTED_CAMPAIGNS(payload));

  const changeTab = (tab: string) => {
    dispatch(CHANGE_TAB(tab));
  };

  const updatePagination = (paging: any) =>
    dispatch(SET_PAGINATION({ pagination: paging }));

  const updateFilterCampaignStatus = (status: any) =>
    dispatch(
      SET_FILTER_CAMPAIGN_STATUS({ filterCampaignStatus: status }),
    );

  const handleSelectAll = React.useCallback((checked) => {
    if (
      checked &&
      get(ref.current, 'selectedCampaigns.length', 0) === 0
    ) {
      updateSelectedCampaigns(
        (ref.current.selectedCampaigns || []).concat(
          ref.current.campaigns,
        ),
      );
    } else {
      updateSelectedCampaigns([]);
    }
  }, []);

  const handleSelectItem = React.useMemo(() => {
    return (item: any, checked: boolean) => {
      if (checked) {
        updateSelectedCampaigns(
          (ref.current.selectedCampaigns || []).concat(item),
        );
      } else {
        updateSelectedCampaigns(
          (ref.current.selectedCampaigns || []).filter(
            (c: any) => c.campaign_eid !== item.campaign_eid,
          ),
        );
      }
    };
  }, [selectedCampaigns]);

  const getRowId = React.useCallback((row) => {
    return String(row.campaign_eid);
  }, []);

  const selectedIds = React.useMemo(() => {
    return (selectedCampaigns || []).map((i) =>
      String(i.campaign_eid),
    );
  }, [selectedCampaigns]);

  const onClickSubjectLabel = (campaign: any) => {
    updateSelectedCampaigns([campaign]);
    changeTab(TABS.PRODUCT);
  };

  const getCampaignData = (argParams?: any) => {
    if (globalFilters) {
      const urlParams = new URLSearchParams(location.search);
      let shopIds = urlParams.get('shopId');
      if (globalFilters.shops) {
        shopIds = globalFilters.shops
          .map((item: any) => item.id)
          .join(',');
      }

      const params = {
        shop_eids: shopIds || 4110,
        from: globalFilters.timeRange.start,
        to: globalFilters.timeRange.end,
        limit: ref.current.pagination.limit,
        page: ref.current.pagination.page,
        value_filter: ref.current.searchText,
        status: filterCampaignStatus,
        ...argParams,
      };

      if (ref.current.sortBy.length > 0) {
        params.order_by = ref.current.sortBy[0].id;
        params.order_mode = ref.current.sortBy[0].desc
          ? 'desc'
          : 'asc';
      }

      if (params.status === 'All') {
        params.status = '';
      }

      getCampaigns(params);
    }
  };

  const handleChangeSearchText = (value: any) => {
    ref.current.searchText = value;
    updatePagination({
      page: 1,
      limit: pagination.limit,
      item_count: pagination.item_count,
      page_count: pagination.page_count,
    });
    getCampaignData();
  };

  const handleChangeStatus = (value: any) => {
    updatePagination({
      page: 1,
      limit: pagination.limit,
      item_count: pagination.item_count,
      page_count: pagination.page_count,
    });
    updateFilterCampaignStatus(value);
    getCampaignData({ status: value });
  };

  const handleSorting = (sortBy: any) => {
    const isDifference =
      differenceWith(sortBy, ref.current.sortBy, isEqual).length > 0;
    if (isDifference) {
      ref.current.sortBy = sortBy;
      getCampaignData();
    }
  };

  const handleChangePage = (page: number) => {
    updatePagination({
      page: page,
      limit: pagination.limit,
      item_count: pagination.item_count,
      page_count: pagination.page_count,
    });
    getCampaignData();
  };

  const handleChangePageSize = (value: number) => {
    updatePagination({
      page: 1,
      limit: value,
      item_count: pagination.item_count,
      page_count: pagination.page_count,
    });
    getCampaignData();
  };

  const handleActivator = (type: string, campaigns: any) => {
    const isAllowed = campaigns.every((i) => !i._isDisabled);
    if (isAllowed) {
      const activator: any = {
        active: {
          labelSubmit: 'Activate',
          title: 'Activate Campaign(s)?',
          content:
            'Activating Campaigns will resume ads and some Promoted Products inside it.',
          status: 1,
        },
        deactive: {
          labelSubmit: 'Deactivate',
          title: 'Deactivate Campaigns?',
          content:
            'Deactivating Campaigns will pause ads and some Promoted Products inside it.',
          status: 0,
        },
      };
      setShowActivator({
        ...showActivator,
        ...activator[type],
        focusCampaign: campaigns,
        type,
        show: true,
      });
    } else {
      toast.error('Can not modify ended campaigns');
    }
  };

  const showStatusPopup = (campaign: any, status: boolean) => {
    handleActivator(
      status ? 'deactive' : 'active',
      [].concat(campaign),
    );
  };

  const updateCampaign = async (field: string, value: any) => {
    const updateFn = {
      name: updateCampaignName,
      budget: updateCampaignBudget,
      timeline: updateCampaignTimeline,
    };

    try {
      const response = await updateFn[field](value);
      const result = response.success;
      if (response.success) {
        toast.success(response.message);
        getCampaignData();
      } else {
        toast.error(response.message);
      }
      return result;
    } catch (e) {
      if (
        !e.message ||
        e.message.length === 0 ||
        !String([].concat(e.message).join('')).trim()
      ) {
        toast.error(
          'An unexpected error occurred. Please try again in a few moments',
        );
      } else {
        toast.error([].concat(e.message).join('\n'));
      }
      return false;
    }
  };

  const handleSubmit = async (selectedCampaigns: any[]) => {
    const shopEids = selectedCampaigns.map((item) => item.shop_eid);
    const campaignEids = selectedCampaigns.map(
      ({ campaign_eid }: any) => campaign_eid,
    );
    setShowActivator({ ...showActivator, show: false });
    try {
      const res = await activatorCampaigns({
        shop_eids: [...new Set(shopEids)],
        campaign_eids: campaignEids,
        status: showActivator.status,
      });
      setNotification({ ...notification, ...res, open: true });
    } catch (error) {
      notification.message = error.message;
      setNotification({
        ...notification,
        open: true,
        success: false,
      });
    }

    getCampaignData();
  };

  const handleSubmitModifyProducts = async ({
    total_budget,
    daily_budget,
  }: any) => {
    const campaignEids = selectedCampaigns.map(
      ({ campaign_eid }) => campaign_eid,
    );
    const shopEids = selectedCampaigns.map(
      ({ shop_eid }) => shop_eid,
    );

    try {
      const res = await updateCampaignPrice({
        campaign_eids: campaignEids,
        shop_eids: shopEids,
        total_budget,
        daily_budget,
      });
      setNotification({ ...notification, ...res, open: true });
    } catch (error) {
      notification.message = error.message;
      setNotification({
        ...notification,
        open: true,
        success: false,
      });
    }
    setOpenModalBulkAction({ ...openModalBulkAction, open: false });
    getCampaignData();
  };

  const handleOpenModifyBudget = (campaigns: any) => {
    const isDiffCurrency = campaigns.some(
      (i) => i.currency !== campaigns[0].currency,
    );
    if (isDiffCurrency) {
      toast.error('Selected campaigns have the difference currency');
    } else {
      const allowItems = selectedCampaigns.filter(
        (i) => !i._isDisabled,
      );
      if (allowItems.length > 0) {
        setOpenModalBulkAction({ open: true, items: allowItems });
      } else {
        toast.error('Can not modify ended campaigns');
      }
    }
  };

  const handleCloseNotification = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setNotification({ ...notification, open: false });
  };

  const handleDownloadFile = (selectedCampaigns) => {
    return downloadFile({
      campaign_eids: selectedCampaigns.map((c) => c.campaign_eid),
      shop_eids: selectedCampaigns.map((c) => c.shop_eid),
      from: globalFilters.timeRange.start,
      to: globalFilters.timeRange.end,
      limit: 10000,
    });
  };

  const handleGetFileExportName = (selectedCampaigns) => {
    return 'epsilo_lazada_campaigns.xlsx';
  };

  const headers = React.useMemo(() => {
    const columns = [...CAMPAIGN_COLUMNS];
    columns[0].onCellClick = showStatusPopup;
    columns[1].onClickLabel = (row) => {
      onClickSubjectLabel(row);
    };
    return columns;
  }, [CAMPAIGN_COLUMNS]);

  return (
    <Box>
      <CampaignView
        tableHeaders={headers}
        campaigns={campaigns}
        pagination={pagination}
        statusList={statusList}
        selectedStatus={filterCampaignStatus}
        searchText={ref.current.searchText}
        selectedCampaigns={selectedCampaigns}
        updateSelectedCampaigns={updateSelectedCampaigns}
        handleSelectItem={handleSelectItem}
        handleSelectAll={handleSelectAll}
        getRowId={getRowId}
        selectedIds={selectedIds}
        onSearch={handleChangeSearchText}
        onChangeStatus={handleChangeStatus}
        onSorting={handleSorting}
        onChangePage={handleChangePage}
        onChangePageSize={handleChangePageSize}
        handleActivator={handleActivator}
        handleOpenModifyBudget={handleOpenModifyBudget}
        updateCampaign={updateCampaign}
        handleDownloadFile={handleDownloadFile}
        handleGetFileExportName={handleGetFileExportName}
      />
      {showActivator.show && (
        <ModalConfirmUI
          open={showActivator.show}
          onSubmit={() =>
            handleSubmit([].concat(showActivator.focusCampaign))
          }
          onClose={() =>
            setShowActivator({ ...showActivator, show: false })
          }
          labelSubmit={showActivator.labelSubmit}
          title={showActivator.title}
          content={showActivator.content}
        />
      )}
      {openModalBulkAction.open && (
        <ModalBulkActionCampaign
          open={openModalBulkAction.open}
          setOpenModalBulkAction={(e: any) =>
            handleSubmitModifyProducts(e)
          }
          onClose={() =>
            setOpenModalBulkAction({
              ...openModalBulkAction,
              open: false,
            })
          }
          campaigns={openModalBulkAction.items}
          currency={openModalBulkAction.items[0]?.currency}
          updateSelected={(row: any) => {
            updateSelectedCampaigns(row);
            setOpenModalBulkAction({
              ...openModalBulkAction,
              items: row,
            });
          }}
        />
      )}
      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={notification.open}
        onClose={handleCloseNotification}
        autoHideDuration={3000}
      >
        <Alert
          onClose={handleCloseNotification}
          elevation={6}
          variant="filled"
          severity={notification.success ? 'success' : 'error'}
        >
          {notification.message}
        </Alert>
      </Snackbar>
    </Box>
  );
}
