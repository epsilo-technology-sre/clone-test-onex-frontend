// aim means access identity management
const TOKEN_KEY = 'eptoken';
export function setLoginToken(token: string | null) {
  if (token === null) {
    window.localStorage.removeItem(TOKEN_KEY);
  } else {
    console.debug('update token', getLoginToken().token !== token);
    window.localStorage.setItem(
      TOKEN_KEY,
      JSON.stringify({ token, lastUpdated: Date.now() }),
    );
  }
  return token;
}

export function getLoginToken(): {
  token: string | null;
  lastUpdated: number | null;
} {
  let token = window.localStorage.getItem(TOKEN_KEY);
  let parsedToken;
  try {
    parsedToken = token
      ? JSON.parse(token)
      : { token: null, lastUpdated: null };
  } catch (error) {
    parsedToken = { token: null, lastUpdated: null };
  }
  return parsedToken;
}

export type UserSettings = {
  profile: {
    userName: string;
    userEmail: string;
  };
  permissions?: {
    byShopId: {
      [shopId: string]: { [featureCode: string]: string[] };
    };
    byCode: {
      [permCode: string]: string[];
    };
  };
  lastUpdated?: number;
};

const USER_SETTINGS_KEY = 'epusersettings';
export function setUserSettings(settings: UserSettings | null) {
  if (settings === null) {
    clearUserSettings();
  }
  window.localStorage.setItem(
    USER_SETTINGS_KEY,
    JSON.stringify({ ...settings, lastUpdated: Date.now() }),
  );
}
export function getUserSettings(): UserSettings | null {
  const str = window.localStorage.getItem(USER_SETTINGS_KEY);

  return str ? JSON.parse(str) : null;
}

export function clearUserSettings() {
  window.localStorage.removeItem(USER_SETTINGS_KEY);
}

let authEvt: EventTarget;
export function getAuthEvent() {
  if (!authEvt) {
    authEvt = new EventTarget();
    authEvt.addEventListener('onExpired', () => setLoginToken(null));
  }

  return {
    onExpired: function (fn: EventListener) {
      authEvt.addEventListener('onExpired', fn);
    },
    triggerExpired: function () {
      authEvt.dispatchEvent(new Event('onExpired'));
    },
  };
}


