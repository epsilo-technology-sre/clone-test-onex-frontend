let apiPassportUrl;
let apiUrl;
let apiInsightUrl;

if (process.env.API_URL) {
  apiUrl = process.env.API_URL;
} else if (process.env.STORYBOOK_API_URL) {
  apiUrl = process.env.STORYBOOK_API_URL;
} else {
  apiUrl = '/staging-onex-api.epsilo.io';
}

if (process.env.API_PASSPORT_URL) {
  apiPassportUrl = process.env.API_PASSPORT_URL;
} else if (process.env.STORYBOOK_PASSPORT_URL) {
  apiPassportUrl = process.env.STORYBOOK_PASSPORT_URL;
} else {
  apiPassportUrl = '/staging-passportx.epsilo.io';
}

if (process.env.API_INSIGHT_URL) {
  apiInsightUrl = process.env.API_INSIGHT_URL;
} else {
  apiInsightUrl = '/staging-insight-api.epsilo.io';
}

export const API_URL = apiUrl;
export const API_PASSPORT_URL = apiPassportUrl;
export const API_INSIGHT_URL = apiInsightUrl; 
