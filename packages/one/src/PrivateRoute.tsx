import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export const PrivateRoute = ({ children, authen, ...rest }: any) => {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        authen ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: '/sign-in',
              state: { from: location },
            }}
          />
        )
      }
    />
  );
};

export default PrivateRoute;
