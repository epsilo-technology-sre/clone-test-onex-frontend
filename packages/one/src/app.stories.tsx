import React from 'react';
import OneApp from './app';

import { startMock } from './mock';

export default {
  title: 'One/App (click here)',
};

export function Primary() {
  startMock();
  return <OneApp />;
}
