import React from 'react';
import { Redirect } from 'react-router';
import { LoadingUI } from './components/common/Loading';
import ContainerInternalPage from './container/internal-page';
import { useAuth } from './hooks/use-auth';
import DashboardIcon from './images/dashboard-24px.svg';
import DashboardIconLight from './images/dashboard-light.svg';
import LazadaActive from './images/lazada-active.svg';
import LazadaInactive from './images/lazada-inactive.svg';
import { ReactComponent as ShopIcon } from './images/shop-icon.svg';
import ShopeeActive from './images/shopee-active.svg';
import ShopeeInactive from './images/shopee-inactive.svg';
import TokoActive from './images/toko-active.svg';
import TokoInactive from './images/toko-inactive.svg';

function PagePublic(LazyComponent: React.ComponentType) {
  function PageWithLazy() {
    const { token } = useAuth();
    if (token) {
      return <Redirect to="/advertising" />;
    }
    return (
      <React.Suspense fallback={<LoadingUI loading />}>
        <LazyComponent />
      </React.Suspense>
    );
  }

  PageWithLazy.displayName = `PageWithLazy(${LazyComponent.displayName})`;

  return PageWithLazy;
}

export function PageWithInternalMenu(
  LazyComponent: React.ComponentType,
) {
  function PageWithLazy() {
    return (
      <React.Suspense fallback={<LoadingUI loading />}>
        <ContainerInternalPage>
          <LazyComponent />
        </ContainerInternalPage>
      </React.Suspense>
    );
  }

  PageWithLazy.displayName = `Pwlz(${LazyComponent.displayName})`;

  return PageWithLazy;
}

export function internalMenu(
  canAccess: (permissionCode: string[]) => boolean,
) {
  let allMenus = [advertising(canAccess), shops(canAccess)];

  if (canAccess(['__INTERNAL__'])) {
    allMenus.push(next(canAccess));
  }

  console.info({ allMenus });
  return allMenus;
}

export function publicMenu() {
  return [
    {
      title: 'Login',
      url: '/login',
      page: PagePublic(React.lazy(() => import('./page/login'))),
    },
    {
      title: 'Reset password',
      url: '/login/reset-password',
      page: PagePublic(
        React.lazy(() => import('./page/login-reset')),
      ),
    },
    {
      title: 'Forget password',
      url: '/login/forget-password',
      page: PagePublic(
        React.lazy(() => import('./page/login-forget')),
      ),
    },
    {
      title: 'Registration',
      url: '/register',
      page: PagePublic(
        React.lazy(() => import('./page/registration')),
      ),
    },
  ];
}

export const overView = {
  title: 'Overview',
  url: '/dashboard',
  children: [
    {
      icon: <img src={DashboardIcon} />,
      iconActive: <img src={DashboardIconLight} />,
      url: '#',
      title: 'Dashboard',
    },
  ],
};
export const orders = {
  title: 'Orders',
  url: '/orders',
  children: [
    {
      icon: <img src={DashboardIcon} />,
      iconActive: <img src={DashboardIconLight} />,
      url: '#',
      title: 'Dashboard',
    },
  ],
};
export const pricing = {
  title: 'Pricing',
  url: '/pricing',
  children: [
    {
      icon: <img src={DashboardIcon} />,
      iconActive: <img src={DashboardIconLight} />,
      url: '#',
      title: 'Dashboard',
    },
  ],
};

const advertisingTokopedia = (canAccess) => ({
  icon: <img src={TokoInactive} />,
  iconActive: <img src={TokoActive} />,
  url: '/advertising/tokopedia',
  title: 'Dashboard',
  access: canAccess(['M_TOK_PA']),
  page: () => <Redirect to={'/advertising/tokopedia/campaign'} />,
  children: [
    {
      title: 'Product Ads',
      url: '/advertising/tokopedia/campaign',
      page: PageWithInternalMenu(
        React.lazy(() => import('@ep/toko/src/App')),
      ),
    },
    {
      title: 'Create campaign',
      url: '/advertising/tokopedia/create-campaign',
      hidden: true,
      page: PageWithInternalMenu(
        React.lazy(
          () => import('@ep/toko/src/pages/create-campaign'),
        ),
      ),
    },
  ],
});

export const advertising = (canAccess) => ({
  title: 'Advertising',
  url: '/advertising',
  page: () => <Redirect to={'/advertising/dashboard'} />,
  children: [
    {
      icon: <img src={DashboardIcon} />,
      iconActive: <img src={DashboardIconLight} />,
      url: '/advertising/dashboard',
      title: 'Dashboard',
      page: PageWithInternalMenu(
        React.lazy(() => import('./page/advertising-dashboard')),
      ),
    },
    {
      icon: <img src={ShopeeInactive} />,
      iconActive: <img src={ShopeeActive} />,
      url: '/advertising/shopee',
      access: canAccess(['M_SHP_KB']),
      title: 'Dashboard',
      page: () => <Redirect to={'/advertising/shopee/campaign'} />,
      children: [
        {
          title: 'Keyword Bidding',
          url: '/advertising/shopee/campaign',
          page: PageWithInternalMenu(
            React.lazy(() => import('@ep/shopee/src/App')),
          ),
        },
        {
          title: 'Shop Ads',
          url: '/advertising/shopee/shop-ads',
          access: canAccess(['M_SHP_SA']),
          page: PageWithInternalMenu(
            React.lazy(() => import('@ep/shopee/src/pages/shop-ads')),
          ),
        },
        {
          title: 'Create campaign',
          url: '/advertising/shopee/create-campaign',
          access: canAccess(['M_SHP_KB_EDIT_CPK']),
          hidden: true,
          page: PageWithInternalMenu(
            React.lazy(
              () => import('@ep/shopee/src/pages/create-campaign'),
            ),
          ),
        },
        {
          title: 'Create Shop Ads',
          url: '/advertising/shopee/create-shop-ads',
          // access: canAccess(['M_SHP_SA_EDIT_SAKWD']),
          hidden: true,
          page: PageWithInternalMenu(
            React.lazy(
              () => import('@ep/shopee/src/pages/shop-ads-create'),
            ),
          ),
        },
      ],
    },
    {
      icon: <img src={LazadaInactive} />,
      iconActive: <img src={LazadaActive} />,
      url: '/advertising/lazada',
      access: canAccess(['M_LZD_SS']),
      title: 'Dashboard',
      page: () => <Redirect to={'/advertising/lazada/campaign'} />,
      children: [
        {
          title: 'Sponsored Search',
          url: '/advertising/lazada/campaign',
          access: canAccess(['M_LZD_SS_VIEW_CPK']),
          page: PageWithInternalMenu(
            React.lazy(() => import('@ep/lazada/src/App')),
          ),
        },
        {
          title: 'Create campaign',
          url: '/advertising/lazada/create-campaign',
          hidden: true,
          access: canAccess(['M_LZD_SS_EDIT_CPK']),
          page: PageWithInternalMenu(
            React.lazy(
              () => import('@ep/lazada/src/pages/create-campaign'),
            ),
          ),
        },
      ],
    },
    advertisingTokopedia(canAccess),
  ],
});

export const shops = (canAccess) => ({
  title: 'Shops',
  url: '/shop',
  page: () => <Redirect to={'/shop/shop-management'} />,
  children: [
    {
      icon: <ShopIcon fill={'#8D979E'} width={18} height={18} />,
      iconActive: <ShopIcon fill={'#fff'} width={18} height={18} />,
      url: '/shop/shop-management',
      title: 'Shop management',
      exact: false,
      page: PageWithInternalMenu(
        React.lazy(() => import('./page/shop/shop-management')),
      ),
    },
  ],
});

export const next = (canAccess) => ({
  title: 'Insights',
  url: '/next',
  hidden: true,
  page: () => <Redirect to={'/next/dashboard'} />,
  children: [
    {
      icon: <img src={DashboardIcon} />,
      iconActive: <img src={DashboardIconLight} />,
      url: '/next/dashboard',
      title: 'Dashboard',
      exact: false,
      page: PageWithInternalMenu(
        React.lazy(() => import('@epi/next/src/page/main')),
      ),
    },
  ],
});

export function Todo() {
  return <h1>Todo</h1>;
}
