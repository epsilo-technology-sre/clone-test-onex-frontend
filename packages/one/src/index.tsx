import './appnew.css';
import React from 'react';
import ReactDOM from 'react-dom';
import OneApp from './app';

const root = document.getElementById('root');

ReactDOM.render(<OneApp />, root);
