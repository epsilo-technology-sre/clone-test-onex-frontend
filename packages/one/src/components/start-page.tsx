import { Grid, makeStyles } from '@material-ui/core';
import React from 'react';

export function StartPage({ children, ...rest }) {
  let classes = useStyles();
  return (
    <div className={classes.root} {...rest}>
      {children}
      <div className={classes.footer}>
        <div>
          <Grid
            container
            justify="space-between"
            spacing={3}
            alignItems="center"
          >
            <Grid item md={6} xs={6}>
              © 2020 Epsilo. All Rights Reserved
            </Grid>
            <Grid item md={6} xs={6}>
              <Grid container spacing={4} justify={'flex-end'}>
                <Grid item>FAQ</Grid>
                <Grid item>Privacy Policy</Grid>
                <Grid item>Terms and condition</Grid>
              </Grid>
            </Grid>
          </Grid>
        </div>
      </div>
    </div>
  );
}

let useStyles = makeStyles(() => ({
  root: {
    boxSizing: 'border-box',
    position: 'relative',
    width: '100%',
    paddingTop: '44px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    minHeight: '100vh',
    paddingBottom: 60,
    backgroundColor: '#fff',
  },
  footer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    height: 60,
    width: '100%',
    padding: 16,
  },
}));
