import { Tooltip } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import React from 'react';

export interface TooltipUIProps {
  children: JSX.Element;
  title: any;
  placement?: string;
}

const useStylesBootstrap = makeStyles((theme) => ({
  arrow: {
    color: theme.palette.common.black,
  },
  tooltip: {
    backgroundColor: theme.palette.common.black,
  },
}));

function BootstrapTooltip(props: any) {
  const classes = useStylesBootstrap();
  return <Tooltip arrow interactive classes={classes} {...props} />;
}

export function TooltipUI(props: TooltipUIProps) {
  const { placement, title } = props;
  return (
    <BootstrapTooltip title={title} placement={placement}>
      {props.children}
    </BootstrapTooltip>
  );
}

TooltipUI.defaultProps = {
  placement: 'top',
};
