import { Box } from '@material-ui/core'
import React from 'react'
import { CurrencyCode } from './currency-code'
import { formatCurrency } from '@ep/shopee/src/utils/utils'

export default {
  title: 'shopee / Currency code'
}

export const Primary = () => {
  const currencies = [
    { amount: 11111111, currency: 'VND' },
    { amount: 11111111, currency: 'SGD' },
    { amount: 11111111, currency: 'PHP' },
    { amount: 11111111, currency: 'IDR' },
    { amount: 11111111, currency: 'MYR' },
    { amount: 11111111, currency: 'TWD' },
    { amount: 11111111, currency: 'USD' },
    { amount: 11111111, currency: 'THB' },
  ]

  return (
    <Box>
      {currencies.map(item => (
        <Box key={item.currency}>
          <b>{item.currency}:</b>{'  '}
          <CurrencyCode currency={item.currency}/>
          <Box component="span" style={{color: 'blue'}}>{formatCurrency(item.amount, item.currency)}</Box>
        </Box>
      ))}
    </Box>
  )
}