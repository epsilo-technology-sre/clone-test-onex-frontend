import React, { useRef, useState, useEffect } from 'react';
import { get } from 'lodash';
import moment from 'moment';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import makeStyles from '@material-ui/core/styles/makeStyles';
import {
  InputAdornment,
  OutlinedInput,
} from '@material-ui/core';
import DateRangeIcon from '@material-ui/icons/DateRange';
import { useOnClickOutside } from '@ep/shopee/src/utils/hooks/useClickOutSide';
import { DateRangePickerWrapper } from './date-range-picker-wrapper';
import { isSameDay } from '@ep/shopee/src/utils/dateTimeUtility';
import { COLORS } from '@ep/shopee/src/constants'

const DATE_PICKER_WIDTH = 610;

const useStyles = makeStyles((theme) => ({
  root: {
    width: 352,
    '&:first-child': {
      position: 'relative',
    },
  },
  outerDateRangePicker: (props: any) => ({
    width: DATE_PICKER_WIDTH,
    marginTop: theme.spacing(0.5),
    position: 'absolute',
    zIndex: 800,
  }),
  input: {
    width: '100%',
    background: COLORS.COMMON.GRAY,
    fontSize: '0.875rem',
    '& input': {
      padding: '8px 14px',
    },
    '& fieldset': {
      border: 'none',
    },
    // '&:hover fieldset': {
    //   borderColor: 'rgb(241, 97, 69) !important',
    // },
    // '&.Mui-focused fieldset': {
    //   borderColor: 'rgb(241, 97, 69) !important',
    // },
  },
}));

export const DateRangePickerUI = (props: any) => {
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [startingDate, setStartingDate] = useState(null);
  const [endingDate, setEndingDate] = useState(null);
  const [preSetRangeOptions, setPreSetRangeOptions] = useState<any>(
    null,
  );
  const [selectedRange, setSelectedRange] = useState<any>(null);
  const [dateRangeString, setDateRangeString] = useState('');

  useEffect(() => {
    const { rangeOptions, startDate, endDate, dateFormat } = props;
    const today = moment();
    const yesterday = moment().subtract(1, 'day');
    const defaultOptions: any = [
      {
        label: 'Today',
        value: { start: today, end: today },
      },
      {
        label: 'Yesterday',
        value: { start: yesterday, end: yesterday },
      },
      {
        label: 'Last 7 days',
        value: { start: moment().subtract(7, 'day'), end: yesterday },
      },
      {
        label: 'Last 30 days',
        value: {
          start: moment().subtract(30, 'day'),
          end: yesterday,
        },
      },
      {
        label: 'Week to date',
        value: { start: moment().startOf('isoWeek'), end: today },
      },
      {
        label: 'Month to date',
        value: { start: moment().startOf('month'), end: today },
      },
    ];

    if (rangeOptions) {
      setPreSetRangeOptions(rangeOptions);
    } else {
      setPreSetRangeOptions(defaultOptions);
    }
    setStartingDate(props.startDate);
    setEndingDate(props.endDate);
    const opt = (props.rangeOptions || defaultOptions).find(
      (o: any) =>
        isSameDay(o.value.start, props.startDate) &&
        isSameDay(o.value.end, props.endDate),
    );
    opt && setSelectedRange(opt);

    setDateRangeString(
      `${!opt || opt.label === 'Custom' ? '' : opt.label} ${moment(
        startDate,
      ).format(dateFormat)} - ${moment(endDate).format(dateFormat)}`,
    );
  }, [props.rangeOptions, props.startDate, props.endDate]);

  const handleDateRangeChange = (selectedItem: any) => {
    setSelectedRange(selectedItem);
  };

  const handleOnApply = ({ startDate, endDate }: any) => {
    const { dateFormat } = props;
    setShowDatePicker(false);
    setStartingDate(startDate);
    setEndingDate(endDate);
    setDateRangeString(
      `${
        !selectedRange || selectedRange.label === 'Custom'
          ? ''
          : selectedRange.label
      } ${moment(startDate).format(dateFormat)} - ${moment(
        endDate,
      ).format(dateFormat)}`,
    );
    props.onApply &&
      props.onApply({
        range: get(selectedRange, 'label', ''),
        startDate,
        endDate,
      });
  };
  const handleOnCancel = () => {
    setShowDatePicker(false);
  };

  const inputRef = useRef<HTMLDivElement>(null);
  const [offsetRight, setOffsetRight] = useState(0);
  const [offsetTop, setOffsetTop] = useState(40);
  const ref = useRef(null);

  useEffect(() => {
    if (inputRef.current) {
      const topPos = inputRef.current.getBoundingClientRect().top;
      const heightWrapper =
        props.rangeOptions && props.rangeOptions.length === 0
          ? 527 - 66 // 527: height outer wrapper, 66: height range
          : 527;

      const adjustTop =
        window.innerHeight - heightWrapper - topPos + 16;
      if (adjustTop < 0) setOffsetTop(adjustTop);
      setOffsetRight(
        -(DATE_PICKER_WIDTH - inputRef.current.offsetWidth),
      );
    }
  }, [inputRef, props.rangeOptions]);

  useOnClickOutside(ref, () => {
    setShowDatePicker(false);
  });

  const classes = useStyles(props);
  if (!props.showInputs) return null;
  return (
    <div className={classes.root} style={props.style}>
      <OutlinedInput
        ref={inputRef}
        id="text"
        type="text"
        readOnly={true}
        value={dateRangeString}
        classes={{ root: classes.input }}
        onClick={() =>
          setShowDatePicker((showDatePicker) => !showDatePicker)
        }
        endAdornment={
          <InputAdornment position="end">
            <DateRangeIcon fontSize="small" />
          </InputAdornment>
        }
      />
      {showDatePicker && (
        <div
          className={classes.outerDateRangePicker}
          style={{
            left: props.position === 'right' ? offsetRight : 0,
            top: offsetTop,
          }}
          ref={ref}
        >
          <DateRangePickerWrapper
            ref={ref}
            startDate={startingDate}
            endDate={endingDate}
            defaultRange={selectedRange}
            rangeOptions={preSetRangeOptions}
            onDateRangeChange={handleDateRangeChange}
            onApply={handleOnApply}
            onCancel={handleOnCancel}
            isOutsideRange={props.isOutsideRange}
          />
        </div>
      )}
    </div>
  );
};

DateRangePickerUI.defaultProps = {
  showInputs: true,
  startDate: moment(),
  endDate: moment(),
  dateFormat: 'DD/MM/YYYY',
};
