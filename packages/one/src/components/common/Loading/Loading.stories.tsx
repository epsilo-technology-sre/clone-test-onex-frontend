import React from 'react';
import { LoadingUI } from './Loading';

export default {
  title: 'Z_Legacy/Loading',
};

export const Primary = () => {
  return <LoadingUI loading={true} />;
};
