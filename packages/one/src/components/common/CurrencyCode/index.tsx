import React from 'react';
import ThaiBahtIcon from '@ep/one/src/images/currency_thai_baht.svg';

const CurrencyCode = ({ currency }: any) => {
  const formatMoney: any = {
    VND: '₫',
    SGD: 'S$',
    THB: (
      <img src={ThaiBahtIcon} width={12} height={15} alt={currency} />
    ),
    PHP: '₱',
    IDR: 'Rp',
    MYR: 'RM',
    TWD: 'NT$',
    USD: '$',
  };
  return formatMoney.currency || '';
};

export default CurrencyCode;
