import React from 'react';
import { Typography, TypographyProps } from '@material-ui/core';
import { Trans } from '../Translator';

export interface TextUIProps extends TypographyProps {
  message?: any;
}

export const TextUI = (props: TextUIProps) => {
  return (
    <Typography component={`span`} {...props}>
      {props.message ? <Trans {...props.message} /> : props.children}
    </Typography>
  );
};

TextUI.defaultProps = {};
