import React from 'react';
import imageLogo from './images/epsilo-logo.png';
import { Link } from 'react-router-dom';

export const Logo = (props: any) => (
  <Link to={'/dashboard/main'}>
    <span>
      <img src={imageLogo} alt={`logo`} {...props} />
    </span>
  </Link>
);
