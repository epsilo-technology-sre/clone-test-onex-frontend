import React from 'react';
import styled from 'styled-components';
import lazadaLogo from './images/lazadaLogo.svg';

const WrapLogo = styled.div`
  width: 25px;
`;

export const LazadaLogo = () => {
  return (
    <WrapLogo>
      <img src={lazadaLogo} alt={`Logo Lazada`} />
    </WrapLogo>
  );
};
