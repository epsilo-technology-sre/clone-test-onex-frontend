import styled from 'styled-components';
import React from 'react';

const Wrap = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;

  .loading-circle {
    & .svg-wrapper {
      width: 4em;
      height: 4em;
      margin: 0 Auto;
      border-radius: 50%;
      display: flex;
      align-items: center;
      justify-content: center;
      box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.1);
    }
    .circle {
      stroke: #000066;
      fill: none;
      stroke-width: 4px;
      stroke-dashoffset: -5.652em;
      stroke-dasharray: 5.652em;
      animation: spin 1.2s linear infinite, color1 10s linear infinite;
      transform: rotate(-90deg);
      transform-origin: 50%;
      position: relative;
    }
    img {
      position: absolute;
      z-index: 100;
    }
    image {
      position: absolute;
      z-index: 100;
    }
    @keyframes spin {
      0% {
        transform: rotate(0);
        stroke-dashoffset: 5.652em;
      }
      50% {
        stroke-dashoffset: 0;
      }
      100% {
        transform: rotate(360deg);
        stroke-dashoffset: -5.652em;
      }
    }

    @keyframes color1 {
      0% {
        stroke: rgb(241, 97, 69);
      }
      10% {
        stroke: #fc6e51;
      }
      20% {
        stroke: #ffce54;
      }
      30% {
        stroke: #a0d468;
      }
      40% {
        stroke: #a0cecb;
      }
      50% {
        stroke: #4fc1e9;
      }
      60% {
        stroke: #8067b7;
      }
      70% {
        stroke: #baa286;
      }
      80% {
        stroke: #ccd1d9;
      }
      90% {
        stroke: #2ecc71;
      }
      100% {
        stroke: #d8334a;
      }
    }
  }
`;

const epsiloCircleLogo =
  'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAMAAADVRocKAAAABGdBTUEAALGPC/xhBQAAACBjSFJN AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAB5lBMVEUAAAAVMkcUMUUTL0cT MEYTMEYSMEYUMEYUMEcULkYVNUoTMEYTMUUTMEYTMEYTL0YTMEcYMUkVKkASMUYTL0YSL0ccOTkV MEUUL0YTMEYTMEYTMEcRL0QSMEUUMEcUMEcRMUYSMUkTMEYSN0kSMEYTMEYTMEcaM00UMkYTMEYA AAAUMEYTMEYSL0UWLEMTMEYTMEYTMEYSL0cSL0QUMUUTMUcTMEYTMEYSL0YUMUcTL0UUL0YTMEYU MEgTL0YTL0cTMEYTMEYRLkYQMEATMEYSMEYPLUsUMUYTMEUNNkMNM0ATMUUkJEkTMEYTL0YAVVUT MEcSMkcTMEYSMkQXLkYAMzMUMUYTMEYgQEAUMEYTMUcTMEYTMEUTMEcOK0cTMEYTMUYTL0YUMUUU J04UL0cAAH8TMEYTMEUTMEYVMEUSMUgUMEcTMEYTMUYTMEYWM0kTMEcSLEYUMEUTMEYSLkkTLkYT MUcTMEYTMUYVMEUVL0QSL0YTMEYUMEYQMEgTMUYTMEYTMEYvNkZ0RkWeT0UhM0ZmQkWQTEWsUkWC SUU9OUbwYUTjXkRYP0XHWETVW0S5VURKPEUiPVJOZHSJmKOnsrrFzNHU2d3////i5ulsfow/V2kx Sl17i5fx8/S2v8ZdcYCYpa5b2nm2AAAAgXRSTlMAJE53oMDQ3utNGFCI9fSHTxUMY8dhCTCc8/CU PLRLw0kqxQ6K+4kKM9wBdPpyF7nk4ytHP178+GJoXEz9QPc28uwsEN/dEahgExRDB9jXA9lIyDgL BViwCIC46OCQEsvNzDQNQQK+v9olOWXv0tQjnx2pqhxCXdO9SjGnoY8gaWplDtFWAAAAAWJLR0SY dtEGPgAAAAd0SU1FB+QHGwQwNI3ClSAAAAUSSURBVGjetVr5YxNFFJ6WBIqpVSsFKSCHVKFGENBU pa3xqIRqQRpqWioeCAh434qKTu82UNPNZrPZXM1/6uac2dl5M7Pb9Ptx5r3vy8x7c7zZIKSGjs4d geDOXV0Yd+3aGQzs2N2h6KiAx0LdwcexCz3BJ558auvsvU/vwQLs6du7Jfp9z+zHEvQf2Oeb/uAh GXsdhw76on/2sBp9FYePeKY/ekydvopjRz3RP3d8wBs/xgPPv6DOf+KkV/oqTp5QpB98Mez0nJtf WJhfdDQtLa+srrEK4ZcGVfhPnWb8lpJVPHxEWtbWa03LrkGcPqUQ3ZcZp0fJBlq/ePFho8WtcOas jP9cP+vzX1Mg1WxZbbYk51wK/a+I+V+NsB5rLbZks2m91TLvDvXQayL+190OG0Rgo9FEWlZ52fQG zH9+WCjgHgFXYKAT4h8Z5dk3I5pcb7YstATWeA549E0+/9ko13yenSE819RcwXy89TaP/513AfPl OtsSFfi6QmoR8MBj73EELkDWeGMhlVp2ZOTifCq1soRhXHDzx3BbEWP5L0qPLm+IjjMCgfbyYxxg MrTd/Bi/T/N/MNF+gUuXty/CdVBxHvxQwV5L6xnDyJo5wzB0Ky93mCDHT0hqXChmTSdKmbTM6UpL YFJimS6bPGQ3xW5BxRQqZEwIhnim4kprwCqZAhRFro21cLVHZJQxxcgIfKNTNYGPBCaaYcogUgjV BBICCzm/UCFRW8VDqvNTLtYyM79ZyTnaLdB/dFqcQ0WaRteonjQ9tFIBZBixBWbA3gKVP1mWhE6u MkgxYwsEVQLAmec8pQAu6qAoBGlJHNMKQ7CDEAcHQCKZ5RvoRAGMQhzeqakfCGwIGpkkcEHH4BiT DU6HTHTZGKtRhm4rGhmAhv3bBNA1oMdSWakkz6Cd+xp4FpSlvo45gqZxEkHHPQkgzE8lApSoE2iW 35FvuRoCgYLUahYB64yEoCIQwKZsnEOQQEWe4ooC/fwOkh/Cy4NUYBaKAbmlZA0BpAIfQ1lkegUg MAGtg3YJTEIncrsEEqh7ewW6oe3aKz+00GLQgeORPwvdIeNoelQikNHlAA+06r1lUiIgvaWLUL1g X99OgRmw+iAClkdOB6o1yMVhXg/ZBHSvpBSGP0HQzYsIVLYgEIRLTHIYGl5ZKdQLzV7eKwI5cEzP tC30XIVLKHJkmgoVK4CAqAhUuLVJMdIsM3lhJteWnF/+VhmLrnB6qSD4XWqkEB/81N1LXQt95tFn 1Es2r9Ckqnt/UQhRry2XOUOgru++Eulz+jkH3Yi4LagrQ0mokOcFKcJ8TeDsqfQQSuANuKDnuHN4 nXmz+6JLGAX7dss7VDTLANLgTC/77HjTvalqzmeQMjOKdNEA82x4BLnAqaXSJgOjoltpG7qecTwy uARm3Pzo1m23gmWqgRX48hZHAI3f8a3ACNwZR1zc5QR6s6Qi4Dz1uu4iAPfCboWCwouO4agxw/cQ iD5OKkre1Gx6Zp31IQG+4n1i1KwcyJ6tMOsjIvxIhNB5fkmVr2Q55BnLtfqGjiAJbn6N+dDs7G9V Nhl7PfCMZJ/Rarn0DfaNb7+T8yO093u//AemVfhtxKJ+6PeHFOlt/PCjd/6fflbnt/GLx2/WlxSi 68TUrx6C/Vtsyit/Fed+V6O//Ycf9hru/xmRsYf/uu+bvob432Mw+9jxG1tjr+PBPwlO3kYT/z5o B3sDHbudf//pVP37z//kPrfYLnK7aAAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAyMC0wNy0yN1QwNDo0 ODo1MiswMDowMNOvw6YAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMjAtMDctMjdUMDQ6NDg6NTIrMDA6 MDCi8nsaAAAAAElFTkSuQmCC';

export const CircleSpinner = ({ withLogo = true }: any) => {
  return (
    <Wrap>
      <div className="loading-circle">
        <div className="svg-wrapper">
          <svg width="4em" height="4em">
            <circle cx="2em" cy="2em" r="1.8em" className="circle" />
            {withLogo && (
              <image
                id="image0"
                width="2.25em"
                height="2.25em"
                x="0.9em"
                y="0.9em"
                href={epsiloCircleLogo}
              />
            )}
          </svg>
        </div>
      </div>
    </Wrap>
  );
};
