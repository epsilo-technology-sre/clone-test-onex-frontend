import { Box } from '@material-ui/core';
import React from 'react';
import { CurrencySelection } from './index';

export default {
  title: 'One/Currency Selection',
};

export function Primary() {
  return (
    <Box width="250px">
      <CurrencySelection/>
    </Box>
  );
}
