import {
  MenuItem,
  Select,
  withStyles,
  InputBase,
  makeStyles,
} from '@material-ui/core';
import React from 'react';
import { sortBy } from 'lodash';
import { CURRENCY_LIST } from '@ep/one/src/constants';
import FLAG_IDR from './flag-idr.svg';
import FLAG_MYR from './flag-myr.svg';
import FLAG_PHP from './flag-php.svg';
import FLAG_SGD from './flag-sgd.svg';
import FLAG_THB from './flag-thb.svg';
import FLAG_VND from './flag-vnd.svg';
import FLAG_USD from './flag-usd.svg';

const FLAGS: { [key: string]: string } = {
  IDR: FLAG_IDR,
  MYR: FLAG_MYR,
  PHP: FLAG_PHP,
  SGD: FLAG_SGD,
  THB: FLAG_THB,
  VND: FLAG_VND,
  USD: FLAG_USD,
};

export function CurrencySelection({
  value = 'VND',
  onCurrencyChange,
}: {
  value: string;
  onCurrencyChange: (currency: string) => void;
}) {
  const classes = useStyle();

  let currencyList = [].concat(CURRENCY_LIST);
  let currencyUSD = currencyList.find((i) => i.currency === 'USD');

  currencyList = sortBy(currencyList, (i) => i.name).filter(
    (i) => i.currency !== 'USD',
  );
  currencyList.unshift(currencyUSD);

  return (
    <Select
      id="existingRule"
      value={value}
      input={<BootstrapInput />}
      displayEmpty
      MenuProps={{
        anchorOrigin: { vertical: 'bottom', horizontal: 'left' },
        transformOrigin: { vertical: 'top', horizontal: 'left' },
        getContentAnchorEl: null,
      }}
      className={classes.root}
      onChange={(evt) => {
        onCurrencyChange(evt.target.value);
      }}
    >
      {currencyList
        .filter((i) => FLAGS[i.currency])
        .map((item: any) => (
          <MenuItem
            value={item.currency}
            key={item.currency}
            className={classes.menuItem}
          >
            <img
              className={classes.imgFlag}
              src={FLAGS[item.currency]}
              title={item.currency}
            />{' '}
            {item.name}
          </MenuItem>
        ))}
    </Select>
  );
}

const useStyle = makeStyles(() => ({
  imgFlag: {
    marginRight: '0.7em',
  },
  menuItem: {
    display: 'flex',
  },
  root: {},
}));

const BootstrapInput = withStyles({
  root: {
    width: '100%',
  },
  disabled: {
    color: '#9FA7AE !important',
    background: '#F6F7F8 !important',
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: '#ffffff',
    border: '2px solid #C2C7CB',
    fontSize: 14,
    color: '#253746',
    padding: '8px 26px 9px 8px',
    display: 'flex',
    '&:focus': {
      borderRadius: 4,
      background: '#253746',
      color: '#fff',
      borderColor: '#253746',
    },
  },
})(InputBase);
