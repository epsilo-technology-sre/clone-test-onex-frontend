import { COLORS } from '@ep/one/src/constants';
import Box from '@material-ui/core/Box';
import Checkbox from '@material-ui/core/Checkbox';
import InputAdornment from '@material-ui/core/InputAdornment';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Popover from '@material-ui/core/Popover';
import {
  createStyles,
  makeStyles,
  Theme,
  withStyles,
} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import SearchIcon from '@material-ui/icons/Search';
import PopupState, {
  bindPopover,
  bindTrigger,
} from 'material-ui-popup-state';
import React, { useEffect, useState } from 'react';
import { ButtonUI } from '../button';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    body: {
      padding: '8px 8px 0 8px',
    },
    checkboxWrapper: {
      maxHeight: 300,
      overflow: 'auto',
    },
    footer: {
      display: 'flex',
      justifyContent: 'flex-end',
      padding: theme.spacing(1),
      borderTop: '2px solid #E4E7E9',
    },
  }),
);

const SearchBox = withStyles({
  root: {
    marginRight: 5,
    width: '100%',
    '&:hover .MuiOutlinedInput-notchedOutline': {
      borderColor: '#C2C7CB',
    },
  },
  notchedOutline: {
    border: '2px solid #C2C7CB',
  },
  input: {
    padding: 8,
    fontSize: 14,
  },
})(OutlinedInput);

export const MultiSelectUI = (props: any) => {
  const classes = useStyles();
  const [checkAll, setCheckAll] = useState(false);
  const [searchText, setSearchText] = useState('');
  const [selectingItems, setSelectingItems] = useState([]);
  const [displayingItems, setDisplayingItems] = useState([]);

  const {
    prefix = '',
    suffix = '',
    items,
    selectedItems,
    onSaveChange,
    hasFooter = false,
  } = props;

  useEffect(() => {
    const result = searchText
      ? items.filter((item) =>
          item.text.toLowerCase().includes(searchText.toLowerCase()),
        )
      : items;
    setDisplayingItems(result);
  }, [items, searchText]);

  useEffect(() => {
    setSelectingItems(selectedItems);
    setCheckAll(selectedItems.length === items.length);
  }, [selectedItems]);

  const handleChangeSearchText = (e: any) => {
    e.stopPropagation();
    setSearchText(e.target.value);
  };

  const handleCheckAll = () => {
    const nexState = !checkAll;
    const newSelectingItems = nexState ? items : [];
    setSelectingItems(newSelectingItems);
    setCheckAll(nexState);
    if (!hasFooter) {
      onSaveChange(newSelectingItems);
    }
  };

  const handleCheckItem = (menuItem: any) => {
    const newSelectingItems = [...selectingItems];

    const index = selectingItems.findIndex(
      (item) => item.id === menuItem.id,
    );
    if (index === -1) {
      newSelectingItems.push(menuItem);
    } else {
      newSelectingItems.splice(index, 1);
    }

    setSelectingItems(newSelectingItems);
    setCheckAll(newSelectingItems.length === items.length);

    if (!hasFooter) {
      onSaveChange(newSelectingItems);
    }
  };

  const applyChange = (popupState: any) => {
    popupState.close();
    onSaveChange(selectingItems);
  };

  const TriggerButton = () => {
    let display = '';
    if (selectedItems.length === items.length) {
      display = `All (${selectedItems.length})`;
    } else {
      display =
        selectedItems.length === 1
          ? selectedItems[0].text
          : `${selectedItems.length} selected ${suffix}`;
    }
    return (
      <Box>
        {prefix && (
          <Typography
            variant="body2"
            component="span"
          >{`${prefix}: `}</Typography>
        )}
        <b>{display}</b>
      </Box>
    );
  };

  const isChecked = (target: any) => {
    return selectingItems.some((item) => item.id === target.id);
  };

  return (
    <PopupState variant="popover" popupId="demo-popup-popover">
      {(popupState) => {
        const openIcon = popupState.isOpen ? (
          <ExpandLessIcon />
        ) : (
          <ExpandMoreIcon />
        );
        return (
          <Box ml={1}>
            <ButtonUI
              label={<TriggerButton></TriggerButton>}
              size="small"
              variant="contained"
              colorButton={COLORS.COMMON.GRAY}
              iconRight={openIcon}
              {...bindTrigger(popupState)}
            />
            <Popover
              {...bindPopover(popupState)}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
              }}
            >
              <Box className={classes.body}>
                <Box>
                  <SearchBox
                    id="campaign-searchbox"
                    placeholder="Search"
                    value={searchText}
                    onChange={handleChangeSearchText}
                    startAdornment={
                      <InputAdornment position="start">
                        <SearchIcon fontSize="small" />
                      </InputAdornment>
                    }
                    labelWidth={0}
                    autoComplete="off"
                  ></SearchBox>
                </Box>
                <Box className={classes.checkboxWrapper}>
                  <Box onClick={handleCheckAll}>
                    <Checkbox checked={checkAll} />
                    <Typography
                      component="span"
                      variant="body2"
                    >{`All ${suffix}`}</Typography>
                  </Box>
                  {displayingItems.map((item: any, index: number) => (
                    <Box
                      key={index}
                      onClick={() => handleCheckItem(item)}
                    >
                      <Checkbox checked={isChecked(item)} />
                      <Typography component="span" variant="body2">
                        {item.text}
                      </Typography>
                    </Box>
                  ))}
                </Box>
              </Box>
              {hasFooter && (
                <Box className={classes.footer}>
                  <ButtonUI
                    label="Apply"
                    size="small"
                    variant="contained"
                    onClick={() => applyChange(popupState)}
                  />
                </Box>
              )}
            </Popover>
          </Box>
        );
      }}
    </PopupState>
  );
};
