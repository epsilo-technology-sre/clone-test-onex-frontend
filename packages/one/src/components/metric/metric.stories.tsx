import { Grid } from '@material-ui/core';
import { action, actions } from '@storybook/addon-actions';
import React from 'react';
import { MetricCard } from './metric';
import { MetricCardGroup } from './metric-group';
import { MetricListPopup } from './metric-popup';
import moment from 'moment';

export default {
  title: 'One / Dashboard',
};

export function SingleMetric() {
  return (
    <Grid container spacing={3} justify="center">
      <Grid item md={2}>
        <MetricCard
          metricId={''}
          title={''}
          value={600}
          percentChange={15}
          previousPeriodRange={{
            from: moment('06/11/2020', 'DD-MM-YYYY').valueOf(),
            to: moment('12/11/2020', 'DD-MM-YYYY').valueOf(),
          }}
          onRemove={action('onRemove')}
        />
      </Grid>
    </Grid>
  );
}

export function MetricList() {
  const items = Array(8)
    .fill(0)
    .map((_, i) => ({
      metricId: Math.random().toString().slice(2),
      title: 'Metric ' + i,
      value: 600,
      percentChange: 15,
      previousPeriodRange: {
        from: moment('06/11/2020', 'DD-MM-YYYY').valueOf(),
        to: moment('12/11/2020', 'DD-MM-YYYY').valueOf(),
      },
    }));

  return (
    <MetricCardGroup
      metricItems={items}
      onRemove={action('onRemove')}
    />
  );
}

export function MetricPopup() {
  const items = Array(8)
    .fill(0)
    .map((_, i) => ({
      metricId: Math.random().toString().slice(2),
      title: 'Metric ' + i,
      value: 600,
      percentChange: 15,
      previousPeriodRange: '06/11/2020 - 12/11/2020',
    }));

  return (
    <MetricListPopup
      metricItems={items}
      onClose={action('onClose')}
      onSubmit={action('onSubmit')}
      selectedList={items.slice(0, 3).map((i) => i.metricId)}
      resetList={items.slice(0, 4).map((i) => i.metricId)}
      open
    />
  );
}
