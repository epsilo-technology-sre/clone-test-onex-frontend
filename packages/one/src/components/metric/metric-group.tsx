import {
  Grid,
  Card,
  CardContent,
  makeStyles,
} from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { MetricCard } from './metric';
import {
  SortableContainer,
  SortableElement,
} from 'react-sortable-hoc';

const reorder = (
  list: any[],
  startIndex: number,
  endIndex: number,
) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

export function MetricCardGroup(props: {
  metricItems: Omit<
    React.ComponentProps<typeof MetricCard>,
    'onRemove'
  >[];
  colors: string[];
  highlightMetrics: string[];
  onRemove: (metricId: string) => void;
  onClickItem: (metricId: string) => void;
  onClickAdd: () => void;
  onSort: (metricIds: string[]) => void;
}) {
  const { metricItems, onRemove, highlightMetrics } = props;
  const classes = useStyle();

  const [items, setItems] = useState(Array.from(metricItems));

  useEffect(() => {
    setItems(
      metricItems.map((i) => {
        const color =
          props.colors[highlightMetrics.indexOf(i.metricId)];
        return {
          ...i,
          color,
        };
      }),
    );
  }, [metricItems]);

  function onRemoveItem(metricId: string) {
    onRemove(metricId);
  }

  function onDragEnd({
    oldIndex,
    newIndex,
  }: {
    oldIndex: number;
    newIndex: number;
  }) {
    const nuItems = reorder(items, oldIndex, newIndex);
    setItems(nuItems);
    props.onSort(nuItems.map(i => i.metricId));
  }

  return (
    <SortableList
      pressDelay={200}
      axis="xy"
      items={items}
      onRemove={onRemoveItem}
      onSortEnd={onDragEnd}
      onClickItem={props.onClickItem}
      onClickAdd={props.onClickAdd}
    />
  );
}

const SortableItem = SortableElement(
  (props: React.ComponentProps<typeof MetricCard>) => (
    <Grid item md={3} xs={12} lg={2}>
      <MetricCard {...props} />
    </Grid>
  ),
);

const SortableList = SortableContainer(
  (props: {
    items: Omit<
      React.ComponentProps<typeof MetricCard>,
      'onRemove'
    >[];
    onRemove: (metricId: string) => void;
    onClickItem: (metricId: string) => void;
    onClickAdd: () => void;
  }) => {
    const classes = useStyle();
    return (
      <Grid container spacing={1}>
        {props.items.map((item, index) => (
          <SortableItem
            key={item.metricId}
            {...item}
            index={index}
            onRemove={props.onRemove}
            onClick={props.onClickItem}
          />
        ))}
        <Grid item md={3} xs={12} lg={2} onClick={props.onClickAdd}>
          <Card className={classes.addMetric}>
            <CardContent>+ Add metric</CardContent>
          </Card>
        </Grid>
      </Grid>
    );
  },
);

const useStyle = makeStyles({
  addMetric: {
    color: '#253746',
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    boxShadow: 'none',
    border: '2px dashed #C2C7CB',
    fontSize: '12px',
    cursor: 'pointer',
  },
});
