import { TooltipUI } from '@ep/one/src/components/common/tooltip';
import { Box } from '@material-ui/core';
import MuiCard from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import MuiCardHeader from '@material-ui/core/CardHeader';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import CheckIcon from '@material-ui/icons/Check';
import ClearIcon from '@material-ui/icons/Clear';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import clsx from 'clsx';
import moment from 'moment';
import numeral from 'numeral';
import React from 'react';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { MdContentCopy } from 'react-icons/md';

const Card = withStyles({
  root: {
    boxShadow:
      '0px 1px 1px rgba(37, 55, 70, 0.25), 0px 0px 1px rgba(37, 55, 70, 0.31);',
  },
})(MuiCard);

const CardHeader = withStyles({
  root: {
    padding: 8,
    paddingBottom: 2,
  },
  title: {
    fontSize: '12px',
    fontWeight: 400,
    color: '#596772',
  },
  action: {
    marginTop: 0,
    marginRight: 0,
    cursor: 'pointer',
  },
})(MuiCardHeader);

const MetricAbbreviation = ['cost', 'gmv', 'total_gmv', 'direct_gmv'];

let formatCurrency = (value, pattern, currency) => {
  return numeral(value).format(pattern) + ' ' + currency;
};

export function MetricCard(props: {
  metricId: string;
  title: string;
  value: number;
  percentChange: number;
  percentChangePositive: boolean;
  previousPeriodRange: {
    from: number;
    to: number;
  };
  isPercent: boolean;
  onRemove: (metricId: string) => void;
  onClick: (metricId: string) => void;
  color?: string;
  currency?: string;
  currencySymbol?: string;
  definition: string;
}) {
  const { currency } = props;
  const classes = useStyles(props);
  const pc = props.percentChange;

  const numberFmtPattern = MetricAbbreviation.includes(props.metricId)
    ? '0[,]0[.]00a'
    : '0,0[.]00';

  let numberDisplay = props.currency
    ? formatCurrency(
        props.value,
        numberFmtPattern,
        props.currencySymbol,
      )
    : numeral(props.value).format('0,0[.]00');
  let numberDisplayTitle = props.currency
    ? formatCurrency(props.value, '0,0[.]00', props.currencySymbol)
    : numeral(props.value).format('0,0[.]00');

  const titleTooltip = (
    <div
      dangerouslySetInnerHTML={{
        __html: (props.definition || '').replaceAll('\\n', '<br/>'),
      }}
    ></div>
  );

  return (
    <Card
      className={classes.root}
      onClick={() => {
        props.onClick(props.metricId);
      }}
    >
      <Box>
        <Box className={classes.cardHeader}>
          <Box>
            <span>{props.title}</span>
            <TooltipUI title={titleTooltip}>
              <InfoOutlinedIcon className="metric-description"></InfoOutlinedIcon>
            </TooltipUI>
          </Box>

          <ClearIcon
            className={classes.closeIcon}
            onClick={(evt) => {
              evt.preventDefault();
              evt.stopPropagation();
              props.onRemove(props.metricId);
            }}
          />
        </Box>
      </Box>

      <CardContent className={classes.cardContent}>
        <TooltipUI title={numberDisplayTitle}>
          <div className={clsx(classes.value)}>
            <span>
              {numberDisplay}
              {props.isPercent ? '%' : ''}
            </span>
            <CopyToClipboard text={numberDisplayTitle}>
              <IconButton
                className="copy"
                title="Copy metric"
                onClick={(e) => e.stopPropagation()}
              >
                <MdContentCopy
                  className={classes.copyIcon}
                ></MdContentCopy>
              </IconButton>
            </CopyToClipboard>
          </div>
        </TooltipUI>
        <div className={classes.vsPrevious}>
          <span
            className={clsx(
              'percent',
              pc < 0 && 'negative',
              pc === 0 && 'neutral',
              pc > 0 && 'positive',
              props.percentChangePositive && 'neg-improved',
            )}
          >
            {numeral(pc).format('0.00')}
          </span>
          <span style={{ whiteSpace: 'nowrap' }}>
            {' vs '}
            <span>
              {moment(props.previousPeriodRange.from).format(
                'DD/MM/YYYY',
              )}
              {' - '}
              {moment(props.previousPeriodRange.to).format(
                'DD/MM/YYYY',
              )}
            </span>
          </span>
        </div>
      </CardContent>
    </Card>
  );
}

export function MetricCardItemPopup(props: {
  metricId: string;
  title: string;
  value: number;
  percentChange: number;
  percentChangePositive: boolean;
  previousPeriodRange: {
    from: number;
    to: number;
  };
  isPercent: boolean;
  selected: boolean;
  onClick: (...args: any) => any;
  currency?: string;
  currencySymbol?: string;
}) {
  const classes = useStyles(props);
  const { currency } = props;
  const pc = props.percentChange;
  const numberFmtPattern = MetricAbbreviation.includes(props.metricId)
    ? '0[,]0[.]00a'
    : '0,0[.]00';
  let numberDisplay = props.currency
    ? formatCurrency(
        props.value,
        numberFmtPattern,
        props.currencySymbol,
      )
    : numeral(props.value).format('0,0[.]00');
  let numberDisplayTitle = props.currency
    ? formatCurrency(props.value, '0,0[.]00', props.currencySymbol)
    : numeral(props.value).format('0,0[.]00');

  return (
    <Card
      className={clsx(
        classes.root,
        classes.popup,
        props.selected && 'selected',
      )}
      onClick={() => props.onClick(props.metricId)}
    >
      <div className={classes.popupWrapper}>
        <div className={'content'}>
          <CardHeader title={props.title} className={classes.title} />
          <CardContent className={classes.cardContent}>
            <TooltipUI title={numberDisplayTitle}>
              <Typography variant="h2" className={classes.value}>
                {numberDisplay}
                {props.isPercent ? '%' : ''}
              </Typography>
            </TooltipUI>
            <div className={classes.vsPrevious}>
              <span
                className={clsx(
                  'percent',
                  pc < 0 && 'negative',
                  pc === 0 && 'neutral',
                  pc > 0 && 'positive',
                  props.percentChangePositive && 'neg-improved',
                )}
              >
                {numeral(pc).format('0[.]00')}
              </span>
            </div>
          </CardContent>
        </div>
        <div className={'status'}>
          {props.selected && (
            <span>
              Added <CheckIcon className={classes.iconCheck} />
            </span>
          )}
          {!props.selected && <span>Available</span>}
        </div>
      </div>
    </Card>
  );
}

const useStyles = makeStyles((theme) => ({
  root: (props: any) => {
    if (props.color) {
      return {
        borderColor: props.color,
        borderWidth: 2,
        borderStyle: 'solid',
        cursor: 'pointer',
        height: '100%',
      };
    }
    return {
      padding: '2px',
      cursor: 'pointer',
      height: '100%',
    };
  },
  popup: {
    border: '2px solid #D3D7DA',
    cursor: 'pointer',
    '& .MuiCardHeader': {
      '&-title': {
        color: '#596772',
        fontWeight: 600,
      },
    },
    '& $cardContent': {
      display: 'flex',
    },
    '& $vsPrevious': {
      marginLeft: 7,
      '& .positive': {
        color: '#0BA373',
      },
      '& .neutral': {
        color: '#0BA373',
      },
      '& .negative': {
        color: '#E03C21',
      },
      '& .negative.neg-improved': {
        color: '#0BA373',
      },
      '& .postive.neg-improved': {
        color: '#E03C21',
      },
    },
    '&.selected': {
      border: '2px solid #485764',
      boxShadow:
        '0px 3px 5px rgba(37, 55, 70, 0.2), 0px 0px 1px rgba(37, 55, 70, 0.31)',
      '& .MuiCardHeader-title': {
        color: '#253746',
        fontWeight: 600,
      },
      '& $popupWrapper .status': {
        fontStyle: 'italic',
      },
    },
  },

  popupWrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    '& .content': {},
    '& .status': {
      fontSize: 10,
      marginRight: theme.spacing(1),
    },
  },

  iconCheck: {
    color: '#0BA373',
    fontSize: '12px',
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  value: {
    fontWeight: 500,
    fontSize: 24,
    whiteSpace: 'nowrap',
    position: 'relative',
    '& .copy': {
      display: 'none',
      color: '#596772',
      textTransform: 'none',
      padding: 0,
      marginLeft: 10,
    },
    '&:hover .copy': {
      display: 'inline-block',
    },
  },
  copyIcon: {
    fontSize: '1rem',
  },
  cardHeader: {
    color: '#596772',
    fontSize: 12,
    fontWeight: 400,
    padding: '8px 20px 0 8px',
    position: 'relative',
    '& .metric-description': {
      display: 'none',
      fontSize: 14,
      color: 'darkgrey',
      marginLeft: 5,
    },
    '&:hover .metric-description': {
      verticalAlign: 'middle',
      display: 'inline-block',
    },
  },
  cardContent: {
    padding: 8,
    paddingTop: 2,
    '&:last-child': {
      paddingBottom: 8,
    },
  },
  vsPrevious: {
    marginTop: '8px',
    fontSize: '10px',
    color: '#596772',
    '& .percent': {
      '&::after': {
        content: '"%"',
      },
    },
    '& .positive': {
      color: '#0BA373',
    },
    '& .neutral': {
      color: '#0BA373',
    },
    '& .negative': {
      color: '#E03C21',
    },
    '& .negative.neg-improved': {
      color: '#0BA373',
    },
    '& .postive.neg-improved': {
      color: '#E03C21',
    },
  },
  closeIcon: {
    fontSize: '16px',
    position: 'absolute',
    top: 8,
    right: 6,
  },
}));
