import React from 'react';
import {
  DialogTitle,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  Grid,
  Button as MuiButton,
  withStyles,
  createStyles,
  Typography,
  makeStyles,
  IconButton,
} from '@material-ui/core';
import { MetricCardItemPopup } from './metric';
import CloseIcon from '@material-ui/icons/Close';

const Button = withStyles({
  label: {
    textTransform: 'none',
  },
})(MuiButton);

interface MetricPopupProps {
  open: boolean;
  onClose: () => any;
  onSubmit: (selected: string[]) => any;
  metricItems: Omit<
    React.ComponentProps<typeof MetricCardItemPopup>,
    'onClick' | 'selected'
  >[];
  selectedList: string[];
  resetList: string[];
}

export function MetricListPopup(props: MetricPopupProps) {
  const { open, onSubmit, selectedList, onClose } = props;
  const classes = useStyle();

  const [userPicks, setUserPicks] = React.useState(selectedList);

  React.useEffect(() => {
    // if (!open) {
      setUserPicks(selectedList);
    // }
  }, [open]);


  function toggleMetric(metricId: string) {
    const foundIndex = userPicks.indexOf(metricId);

    if (foundIndex === -1) {
      setUserPicks(userPicks.concat(metricId));
    } else if(userPicks.length > 1) {
      const ls = Array.from(userPicks);
      ls.splice(foundIndex, 1);
      setUserPicks(ls);
    }
  }

  return (
    <Dialog aria-labelledby="simple-dialog-title" open={open}>
      <DialogTitle id="simple-dialog-title">
        Metric
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <Grid container spacing={2}>
          {props.metricItems.map((item) => {
            return (
              <Grid key={item.metricId} item xs={6}>
                <MetricCardItemPopup
                  {...item}
                  selected={userPicks.indexOf(item.metricId) > -1}
                  onClick={(metricId) => toggleMetric(metricId)}
                />
              </Grid>
            );
          })}
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button variant="text" size="small" onClick={onClose}>
          Cancel
        </Button>{' '}
        <Button
          onClick={() => onSubmit(userPicks)}
          color="primary"
          size="small"
          variant="contained"
          className={classes.btnSubmit}
        >
          Add metric
        </Button>
      </DialogActions>
    </Dialog>
  );
}

const useStyle = makeStyles((theme) => ({
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  dialogActions: {
    justifyContent: 'space-between',
    padding: theme.spacing(2),
  },
  btnSubmit: {
    backgroundColor: '#ED5C10',
    '&:hover': {
      backgroundColor: '#ED5C10',
    },
  },
}));
