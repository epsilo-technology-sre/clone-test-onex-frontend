import React from 'react';
import {
  Grid,
  FormHelperText,
  TextField,
  withStyles,
  makeStyles,
  Radio,
  RadioGroup,
  FormControlLabel,
} from '@material-ui/core';
import styled from 'styled-components';
import { ButtonUI } from '../common/button';
import * as Yup from 'yup';
import clsx from 'clsx';
import { Field, Form, Formik, ErrorMessage } from 'formik';
import InfoIcon from '@material-ui/icons/Info';
import { TooltipUI } from '../common/tooltip';
import { COLORS } from '../../constants';

const validationSchema = Yup.object().shape({
  phoneNumber: Yup.string().when('radios', {
    is: (val) => val === 'phone',
    then: Yup.string().required('Please enter phone number'),
  }),
  codeOTP: Yup.string().when('radios', {
    is: (val) => val === 'code',
    then: Yup.string().required('Please enter code OTP'),
  }),
});

export function AddNewShopOTP(props: any) {
  const { onSubmit } = props;
  const classes = useStyle();

  return (
    <>
      <Title>Add New Shop</Title>
      <ShopContainer>
        <Formik
          validationSchema={validationSchema}
          initialValues={{
            phoneNumber: '',
            codeOTP: '',
            radios: 'phone',
          }}
          onSubmit={(values: any) => {
            return onSubmit(values);
          }}
          validateOnBlur={true}
        >
          {(formik) => {
            console.log('formik', formik);
            return (
              <Form>
                <Grid container>
                  <Grid item xs={12}>
                    <Grid container>
                      <Grid item>
                        <TitleSection>
                          OTP Number Required
                        </TitleSection>
                      </Grid>
                      <Grid item>
                        <TooltipUI
                          placement="right-end"
                          title={
                            'Your shop need OTP number to login on <channel> for authorizing to Epsilo'
                          }
                        >
                          <InfoIcon
                            color={'primary'}
                            fontSize={'small'}
                          />
                        </TooltipUI>
                      </Grid>
                    </Grid>
                  </Grid>
                  <Grid item xs={12}>
                    <Field name="radios">
                      {({ field }: any) => {
                        return (
                          <RadioGroup
                            aria-label="OTP"
                            name="otp"
                            {...field}
                          >
                            <FormControlLabel
                              value="phone"
                              control={<StyledRadio />}
                              label="Epsilo provided mobile number"
                            />
                            <Wrap>
                              <TitleField>Phone Number</TitleField>
                              <Field name="phoneNumber">
                                {({ field, form }: any) => {
                                  return (
                                    <TextFieldUI
                                      fullWidth
                                      disabled={
                                        form.values.radios === 'code'
                                      }
                                      placeholder={
                                        'Enter phone number'
                                      }
                                      id="outlined-size-small"
                                      autoComplete="off"
                                      variant="outlined"
                                      size="small"
                                      {...field}
                                    />
                                  );
                                }}
                              </Field>
                              <ErrorMessage
                                component={FormHelperText}
                                name="phoneNumber"
                                className={classes.error}
                              />
                            </Wrap>
                            <FormControlLabel
                              value="code"
                              control={<StyledRadio />}
                              label="My own phone number"
                            />
                            <Wrap>
                              <TitleField>
                                Verification Code (OTP)
                              </TitleField>
                              <Field name="codeOTP">
                                {({ field, form }: any) => {
                                  return (
                                    <TextFieldUI
                                      fullWidth
                                      disabled={
                                        form.values.radios === 'phone'
                                      }
                                      placeholder={
                                        'Enter verification code'
                                      }
                                      id="outlined-size-small"
                                      autoComplete="off"
                                      variant="outlined"
                                      size="small"
                                      {...field}
                                    />
                                  );
                                }}
                              </Field>
                              <ErrorMessage
                                component={FormHelperText}
                                name="codeOTP"
                                className={classes.error}
                              />
                            </Wrap>
                          </RadioGroup>
                        );
                      }}
                    </Field>
                  </Grid>
                  <Grid item xs={12}>
                    <ButtonUI
                      disabled={
                        formik.values.phoneNumber.length === 0 &&
                        formik.values.codeOTP.length === 0
                      }
                      type={'submit'}
                      fullWidth
                      label={'Submit'}
                    />
                  </Grid>
                </Grid>
              </Form>
            );
          }}
        </Formik>
      </ShopContainer>
    </>
  );
}

const Title = styled.h1`
  font-style: normal;
  font-weight: 600;
  font-size: 29px;
  line-height: 32px;
  color: #253746;
  margin-bottom: 54px;
`;

const TitleSection = styled.p`
  font-weight: 600;
  font-size: 16px;
  line-height: 20px;
  color: #253746;
  margin-bottom: 16px;
  margin-top: 0;
`;

const TitleField = styled.p`
  font-weight: bold;
  font-size: 11px;
  line-height: 16px;
  color: #596772;
  margin-bottom: 4px;
  margin-top: 16px;
`;

const ShopContainer = styled.div`
  background: #fff;
  max-width: 50%;
  margin: auto;
`;

const Wrap = styled.div`
  margin-bottom: 20px;
`;

const useStyle = makeStyles({
  error: {
    color: COLORS.COMMON.RED,
    fontSize: 12,
  },
  root: {
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  icon: {
    borderRadius: '50%',
    width: 16,
    height: 16,
    boxShadow:
      'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
    backgroundColor: '#f5f8fa',
    backgroundImage:
      'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
    '$root.Mui-focusVisible &': {
      outline: '2px auto rgba(19,124,189,.6)',
      outlineOffset: 2,
    },
    'input:hover ~ &': {
      backgroundColor: '#ebf1f5',
    },
    'input:disabled ~ &': {
      boxShadow: 'none',
      background: 'rgba(206,217,224,.5)',
    },
  },
  checkedIcon: {
    backgroundColor: '#0BA373',
    backgroundImage:
      'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
    '&:before': {
      display: 'block',
      width: 16,
      height: 16,
      backgroundImage:
        'radial-gradient(#fff,#fff 28%,transparent 32%)',
      content: '""',
    },
    'input:hover ~ &': {
      backgroundColor: '#0BA373',
    },
  },
});

const TextFieldUI = withStyles(() => ({
  root: {
    '& .MuiOutlinedInput-notchedOutline': {
      borderWidth: 2,
    },
    '& .Mui-disabled': {
      background: '#F6F7F8',
    },
  },
}))(TextField);

function StyledRadio(props: RadioProps) {
  const classes = useStyle();
  return (
    <Radio
      className={classes.root}
      disableRipple
      color="default"
      checkedIcon={
        <span className={clsx(classes.icon, classes.checkedIcon)} />
      }
      icon={<span className={classes.icon} />}
      {...props}
    />
  );
}
