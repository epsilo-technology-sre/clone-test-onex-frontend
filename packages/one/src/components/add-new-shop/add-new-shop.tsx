import React from 'react';
import {
  Grid,
  MenuItem,
  Select,
  TextField,
  withStyles,
  InputBase,
  FormHelperText,
  makeStyles,
  InputAdornment,
} from '@material-ui/core';
import styled from 'styled-components';
import { ButtonUI } from '../common/button';
import * as Yup from 'yup';
import { Field, Form, Formik, ErrorMessage } from 'formik';
import InfoIcon from '@material-ui/icons/Info';
import { TooltipUI } from '../common/tooltip';
import { COLORS } from '../../constants';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';

type SetupNewShopProps = {
  subcriptionCode: string;
  channel: string;
  country: string;
  userName: string;
  password: string;
  onSubmit: (form: any) => void;
  checkSubcriptionCodeExists: (code: string) => Promise<boolean>;
};

let inChecking: any = null;

const SettingShopSchema = (
  checkExistSubcriptionCode: SetupNewShopProps['checkSubcriptionCodeExists'],
) => {
  return Yup.object().shape({
    channel: Yup.string().required('Please select channel'),
    country: Yup.string().required('Please select country'),
    userName: Yup.string().required('Please enter userName'),
    password: Yup.string().required('Please enter password'),
    subcriptionCode: Yup.string()
      .required('Please enter Subcription Code')
      .test(
        'Unique Subcription code',
        'Invalid verification code, please try another one.',
        async (value: any = '') => {
          clearTimeout(inChecking);
          let codeSubcription = value.trim().toLowerCase();

          if (!codeSubcription) {
            return false;
          }

          return await new Promise((resolve, reject) => {
            inChecking = setTimeout(async () => {
              try {
                let result = await checkExistSubcriptionCode(value.trim());
                resolve(result);
              } catch (e) {
                resolve(false);
              }
            }, 500);
          });
        },
      )
  });
};

export function AddNewShop(props: any) {
  const { channels, countries, onSubmit } = props;
  const classes = useStyle()
  const validationSchema = React.useMemo(
    () => SettingShopSchema(props.checkSubcriptionCodeExists),
    [],
  );


  return <>
    <Title>Add New Shop</Title>
    <ShopContainer>
      <Formik
        validationSchema={validationSchema}
        initialValues={{
          subcriptionCode: '',
          channel: '',
          country: '',
          userName: '',
          password: '',
        }}
        onSubmit={(values: any) => {
          console.log('values', values);
          onSubmit(values);
        }}
        validateOnBlur={true}
      >
        {(formik: any) => {
          const { values } = formik;
          return (
            <Form>
              <Grid container>
                <Grid item xs={12}>
                  <Grid container>
                    <Grid item>
                      <TitleSection>Contract Information</TitleSection>
                    </Grid>
                    <Grid item>
                      <TooltipUI placement="right-end"
                                 title={'The code to let us know which features of Epsilo you prepare to use on the shop. Don’t have any code? Please contact your company account Administrator.'}>
                        <InfoIcon color={'primary'} fontSize={'small'} />
                      </TooltipUI>
                    </Grid>
                  </Grid>
                  <Field name="subcriptionCode">
                    {({ field, meta }: any) => {
                      return (
                        <TextFieldUI
                          fullWidth
                          placeholder={'Enter subcription code'}
                          autoComplete="off"
                          variant="outlined"
                          size="small"
                          {...field}
                          InputProps={{
                            endAdornment: (
                              <InputAdornment position="end">
                                {field.value && (meta.error ? <HighlightOffIcon color={'error'} fontSize={'small'} /> : <CheckCircleIcon htmlColor={'green'} fontSize={'small'} />)}
                              </InputAdornment>
                            ),
                          }}
                        />
                      );
                    }}
                  </Field>
                  <ErrorMessage
                    component={FormHelperText}
                    name="subcriptionCode"
                    className={classes.error}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TitleSection marginTop={'16px'}>Shop Information</TitleSection>
                  <Wrap>
                    <TitleField>Channel</TitleField>
                    <Field name="channel">
                      {({ field }: any) => {
                        return (
                          <Select
                            id="channel"
                            {...field}
                            input={<BootstrapInput />}
                            displayEmpty
                            MenuProps={{
                              anchorOrigin: { vertical: 'bottom', horizontal: 'left' },
                              transformOrigin: { vertical: 'top', horizontal: 'left' },
                              getContentAnchorEl: null,
                            }}
                          >
                            <MenuItem value={''} disabled>Choose Channel</MenuItem>
                            {channels.map((channel: any) => <MenuItem value={channel.channelId} key={channel.channelId}>
                              {channel.channelName}
                            </MenuItem>)}
                          </Select>
                        );
                      }}
                    </Field>
                    <ErrorMessage
                      component={FormHelperText}
                      name="channel"
                      className={classes.error}
                    />
                  </Wrap>
                </Grid>
                <Grid item xs={12}>
                  <Wrap>
                    <TitleField>Country</TitleField>
                    <Field name="country">
                      {({ field }: any) => {
                        return (
                          <Select
                            id="country"
                            {...field}
                            input={<BootstrapInput />}
                            displayEmpty
                            MenuProps={{
                              anchorOrigin: { vertical: 'bottom', horizontal: 'left' },
                              transformOrigin: { vertical: 'top', horizontal: 'left' },
                              getContentAnchorEl: null,
                            }}
                          >
                            <MenuItem value={''} disabled>Choose country</MenuItem>
                            {countries.map((country: any) => <MenuItem value={country.countryId} key={country.countryId}>
                              {country.countryName}
                            </MenuItem>)}
                          </Select>
                        );
                      }}
                    </Field>
                    <ErrorMessage
                      component={FormHelperText}
                      name="country"
                      className={classes.error}
                    />
                  </Wrap>
                </Grid>
                <Grid item xs={12}>
                  <Wrap>
                    <TitleField>Username</TitleField>
                    <Field name="userName">
                      {({ field }: any) => {
                        return (
                          <TextFieldUI
                            fullWidth
                            placeholder={'Enter username on E-commerce platform'}
                            autoComplete="off"
                            variant="outlined"
                            size="small"
                            {...field}
                          />
                        );
                      }}
                    </Field>
                    <ErrorMessage
                      component={FormHelperText}
                      name="userName"
                      className={classes.error}
                    />
                  </Wrap>
                </Grid>
                <Grid item xs={12}>
                  <Wrap>
                    <TitleField>Password</TitleField>
                    <Field name="password">
                      {({ field }: any) => {
                        return (
                          <TextFieldUI
                            fullWidth
                            placeholder={'Enter password on E-commerce platform'}
                            autoComplete="off"
                            variant="outlined"
                            size="small"
                            type="password"
                            {...field}
                          />
                        );
                      }}
                    </Field>
                    <ErrorMessage
                      component={FormHelperText}
                      name="password"
                      className={classes.error}
                    />
                  </Wrap>
                </Grid>
                <Grid item xs={12}>
                  <ButtonUI disabled={formik.errors.subcriptionCode || formik.values.subcriptionCode === 0 || values.subcriptionCode.length === 0 || values.channel.length === 0 || values.country.length === 0 || values.userName.length === 0 || values.password.length === 0} type={'submit'} fullWidth label={'Submit'} />
                </Grid>
              </Grid>
            </Form>
          );
        }}
      </Formik>
    </ShopContainer>
  </>;
}

const Title = styled.h1`
  font-style: normal;
  font-weight: 600;
  font-size: 29px;
  line-height: 32px;
  color: #253746;
  margin-bottom: 54px;
`;

const TitleSection = styled.p`
  font-weight: 600;
  font-size: 16px;
  line-height: 20px;
  color: #253746;
  margin-bottom: 16px;
  margin-top: ${({ marginTop }: any) => marginTop || 0};
`;

const TitleField = styled.p`
  font-weight: bold;
  font-size: 11px;
  line-height: 16px;
  color: #596772;
  margin-bottom: 4px;
  margin-top: 0;
`;

const ShopContainer = styled.div`
  background: #fff;
  max-width: 50%;
  margin: auto;
`;

const Wrap = styled.div`
  margin-bottom: 20px;
`

const BootstrapInput = withStyles({
  root: {
    width: '100%',
  },
  disabled: {
    color: '#9FA7AE !important',
    background: '#F6F7F8 !important',
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: '#ffffff',
    border: '2px solid #C2C7CB',
    fontSize: 14,
    color: '#253746',
    padding: '8px 26px 9px 8px',
  },
})(InputBase);

const TextFieldUI = withStyles(() => ({
  root: {
    '& .MuiOutlinedInput-notchedOutline': {
      borderWidth: 2,
    },
  },
}))(TextField);

const useStyle = makeStyles({
  error: {
    color: COLORS.COMMON.RED,
    fontSize: 12,
  },
});
