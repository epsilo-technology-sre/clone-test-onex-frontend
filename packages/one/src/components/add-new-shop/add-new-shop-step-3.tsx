import React from 'react';
import {
  Grid,
  FormHelperText,
  TextField,
  withStyles,
  makeStyles,
} from '@material-ui/core';
import styled from 'styled-components';
import { ButtonUI } from '../common/button';
import * as Yup from 'yup';
import { Field, Form, Formik, ErrorMessage } from 'formik';
import { COLORS } from '../../constants';

const validationSchema = Yup.object().shape({
  email: Yup.string().required('Please enter email'),
});

export function AddNewShopEmail(props: any) {
  const { onSubmit } = props;
  const classes = useStyle();

  return <>
    <Title>Add New Shop</Title>
    <ShopContainer>
      <Formik
        validationSchema={validationSchema}
        initialValues={{
          email: '',
        }}
        onSubmit={(values: any) => {
          return onSubmit(values);
        }}
        validateOnBlur={true}
      >
        {({ values }: any) => {
          return (
            <Form>
              <Grid container>
                <Grid item xs={12}>
                  <TitleSection>Email Required</TitleSection>
                </Grid>
                <Grid item xs={12}>
                  <Wrap>
                    <TitleField>Email</TitleField>
                    <Field name="email">
                      {({ field }: any) => {
                        return (
                          <TextFieldUI
                            fullWidth
                            placeholder={'Enter email'}
                            id="outlined-size-small"
                            autoComplete="off"
                            variant="outlined"
                            size="small"
                            {...field}
                          />
                        );
                      }}
                    </Field>
                    <ErrorMessage
                      component={FormHelperText}
                      name="email"
                      className={classes.error}
                    />
                  </Wrap>
                </Grid>
                <Grid item xs={12}>
                  <ButtonUI disabled={values.email.length === 0} type={'submit'} fullWidth label={'Submit'} />
                </Grid>
              </Grid>
            </Form>
          );
        }}
      </Formik>
    </ShopContainer>
  </>;
}

const Title = styled.h1`
  font-style: normal;
  font-weight: 600;
  font-size: 29px;
  line-height: 32px;
  color: #253746;
  margin-bottom: 54px;
`;

const TitleSection = styled.p`
  font-weight: 600;
  font-size: 16px;
  line-height: 20px;
  color: #253746;
  margin-bottom: 16px;
  margin-top: 0;
`;

const TitleField = styled.p`
  font-weight: bold;
  font-size: 11px;
  line-height: 16px;
  color: #596772;
  margin-bottom: 4px;
  margin-top: 0;
`;

const ShopContainer = styled.div`
  background: #fff;
  max-width: 50%;
  margin: auto;
`;

const Wrap = styled.div`
  margin-bottom: 20px;
`

const useStyle = makeStyles({
  error: {
    color: COLORS.COMMON.RED,
    fontSize: 12,
  },
});

const TextFieldUI = withStyles(() => ({
  root: {
    '& .MuiOutlinedInput-notchedOutline': {
      borderWidth: 2,
    },
  },
}))(TextField);
