import React from 'react';
import Paper from '@material-ui/core/Paper';
import { AddNewShop } from './add-new-shop';
import { AddNewShopOTP } from './add-new-shop-step-2';
import { AddNewShopEmail } from './add-new-shop-step-3';
// import { checkExistSubcriptionCode } from '../../api/api';

export default {
  title: 'Shopee/Add new shop',
};

const dataShop = Array.from({ length: 10 }, (_, key) => ({
  key,
  channelName: `channel name ${key}`,
  channelId: `channel_id${key}`,
}));

const countries = Array.from({ length: 10 }, (_, key) => ({
  key,
  countryName: `country name ${key}`,
  countryId: `country_id${key}`,
}));

export const step1 = () => {
  function checkSubcriptionCodeExists(code: string) {
    // return checkExistSubcriptionCode({ registration_code: code }).then((rs) => {
    //   return rs.success as boolean;
    // });
    return Promise.resolve(true);
  }

  return (
    <Paper>
      <AddNewShop
        channels={dataShop}
        countries={countries}
        checkSubcriptionCodeExists={checkSubcriptionCodeExists}
      />
    </Paper>
  );
};

export const step2 = () => {
  return (
    <Paper>
      <AddNewShopOTP onSubmit={(e) => console.log('eeeeeee', e)} />
    </Paper>
  );
};

export const step3 = () => {
  return (
    <Paper>
      <AddNewShopEmail
        channels={dataShop}
        countries={countries}
        onSubmit={(e) => console.log('eeeeeee', e)}
      />
    </Paper>
  );
};
