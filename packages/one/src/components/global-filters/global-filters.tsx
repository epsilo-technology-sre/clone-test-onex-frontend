import { makeStyles } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import moment from 'moment';
import React from 'react';
import { ButtonUI } from '../common/button';
import { DateRangePickerUI } from '../common/date-range-picker';
import { MultiSelectUI } from '../common/multi-select';

export const GlobalFilters = (props: any) => {
  const {
    channels,
    features,
    countries,
    shops,
    selectedDateRange,
    selectedChannels,
    selectedFeatures,
    selectedCountries,
    selectedShops,
    onChangeDateRange,
    onChangeChannels,
    onChangeFeatures,
    onChangeCountries,
    onChangeShops,
    onSubmitChange,
    hideChannel = false,
  } = props;

  const { filterItem } = useStyle();

  return (
    <Box my={2}>
      <Grid container justify="flex-start" alignItems="center">
        <Grid item className={filterItem}>
          <DateRangePickerUI
            startDate={moment(selectedDateRange.from)}
            endDate={moment(selectedDateRange.to)}
            onApply={onChangeDateRange}
            isOutsideRange="future"
          />
        </Grid>
        {hideChannel === true ? null : (
          <Grid item className={filterItem}>
            <MultiSelectUI
              prefix="Channel"
              suffix="channels"
              items={channels}
              selectedItems={selectedChannels}
              onSaveChange={onChangeChannels}
            />
          </Grid>
        )}
        <Grid item className={filterItem}>
          <MultiSelectUI
            prefix="Tool"
            suffix="tools"
            items={features}
            selectedItems={selectedFeatures}
            onSaveChange={onChangeFeatures}
          />
        </Grid>
        <Grid item className={filterItem}>
          <MultiSelectUI
            prefix="Country"
            suffix="countries"
            items={countries}
            selectedItems={selectedCountries}
            onSaveChange={onChangeCountries}
          />
        </Grid>
        <Grid item className={filterItem}>
          <MultiSelectUI
            prefix="Shop"
            suffix="shops"
            items={shops}
            selectedItems={selectedShops}
            onSaveChange={onChangeShops}
          />
        </Grid>
        <Grid item>
          <Box pl={1}>
            <ButtonUI
              label="Apply"
              size="small"
              variant="contained"
              onClick={onSubmitChange}
            />
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};

const useStyle = makeStyles((theme) => ({
  filterItem: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));
