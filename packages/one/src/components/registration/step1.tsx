import {
  Divider,
  FormControl,
  FormHelperText,
  Grid,
  Typography,
} from '@material-ui/core';
import { ErrorMessage, Formik } from 'formik';
import React from 'react';
import * as Yup from 'yup';
import {
  ButtonLoginWithProgress as ButtonLogin,
  Input,
  useStyle,
} from './common';
import logo from './logo.png';

const loginSchema = Yup.object().shape({
  email: Yup.string()
    .required('Email is required.')
    .email('Please enter valid email.'),
});

export function RegStep({
  email = '',
  upstreamError = '',
  isLoading = false,
  onSubmit = Function.prototype,
  urlLogin='#',
}) {
  const classes = useStyle();

  return (
    <Grid
      container
      direction="column"
      alignItems="center"
      className={classes.root}
    >
      <Grid item>
        <img src={logo} alt="Epsilo logo" className={classes.logo} />
      </Grid>
      <Grid item xs={12} style={{ marginBottom: 64, marginTop: 64 }}>
        <Typography variant="h4" className={classes.title}>Welcome to Epsilo</Typography>
      </Grid>
      <Grid item xs={12} style={{ width: '100%' }}>
        <Formik
          initialValues={{ email }}
          validationSchema={loginSchema}
          onSubmit={(values, actions) => {
            onSubmit(values);
          }}
        >
          {({ errors, handleChange, handleBlur, handleSubmit }) => (
            <form className={classes.form} onSubmit={handleSubmit}>
              <FormControl
                variant="outlined"
                className={classes.row}
                error={!!errors.email}
                style={{ marginBottom: 24 }}
              >
                <Input
                  placeholder="Please enter your email"
                  type="email"
                  name="email"
                  id="email"
                  onBlur={handleBlur}
                  onChange={handleChange}
                ></Input>
                <ErrorMessage
                  component={FormHelperText}
                  name="email"
                />
                {!errors.email && upstreamError && (
                  <FormHelperText error>
                    {upstreamError}
                  </FormHelperText>
                )}
              </FormControl>
              <FormControl className={classes.row}>
                <ButtonLogin
                  type="submit"
                  color="inherit"
                  variant="contained"
                  isLoading={isLoading}
                >
                  Continue
                </ButtonLogin>
              </FormControl>
            </form>
          )}
        </Formik>
      </Grid>
      <Grid item xs={12} style={{ width: '100%' }} justify={'center'}>
        <Divider
          variant="fullWidth"
          style={{ marginTop: 20, marginBottom: 20 }}
        />
        <div className={classes.linkGroup}>
          <a href={urlLogin} className={classes.link}>
            Already have an Epsilo account? Log in
          </a>
        </div>
      </Grid>
    </Grid>
  );
}
