import React from 'react';

export function PasswordStrengthIndicator({
  colorList = ['#E86F5B', '#F3905D', '#529FDE', '#3CBB93', '#098F65'],
  strength = 5,
}) {
  return (
    <svg
      width="414"
      height="10"
      viewBox="0 0 414 10"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M0 5C0 4.44772 0.447715 4 1 4H78C78.5523 4 79 4.44772 79 5C79 5.55228 78.5523 6 78 6H1C0.447716 6 0 5.55228 0 5Z"
        fill={strength > 0 ? colorList[strength - 1] : '#E4E7E9'}
      />
      <path
        d="M84 5C84 4.44772 84.4477 4 85 4H162C162.552 4 163 4.44772 163 5C163 5.55228 162.552 6 162 6H85C84.4477 6 84 5.55228 84 5Z"
        fill={strength > 1 ? colorList[strength - 1] : '#E4E7E9'}
      />
      <path
        d="M168 5C168 4.44772 168.448 4 169 4H245C245.552 4 246 4.44772 246 5C246 5.55228 245.552 6 245 6H169C168.448 6 168 5.55228 168 5Z"
        fill={strength > 2 ? colorList[strength - 1] : '#E4E7E9'}
      />
      <path
        d="M251 5C251 4.44772 251.448 4 252 4H329C329.552 4 330 4.44772 330 5C330 5.55228 329.552 6 329 6H252C251.448 6 251 5.55228 251 5Z"
        fill={strength > 3 ? colorList[strength - 1] : '#E4E7E9'}
      />
      <path
        d="M335 5C335 4.44772 335.448 4 336 4H413C413.552 4 414 4.44772 414 5C414 5.55228 413.552 6 413 6H336C335.448 6 335 5.55228 335 5Z"
        fill={strength > 4 ? colorList[strength - 1] : '#E4E7E9'}
      />
    </svg>
  );
}
