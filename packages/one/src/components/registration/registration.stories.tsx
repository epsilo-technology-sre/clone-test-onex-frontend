import React from 'react';
import { withDesign } from 'storybook-addon-designs';
import { StartPage } from '../start-page';
import { RegStep as RegStep1 } from './step1';
import { RegStep as RegStep2 } from './step2';
import { RegStep as RegStep3 } from './step3';
import { RegStep as RegStep4 } from './step4';

export default {
  title: 'One / Registration',
  decorators: [withDesign],
  parameters: {
    design: {
      type: 'figma',
      url:
        'https://www.figma.com/file/GbYpnOQTIIHfcHBRzXlU0C/Sign-in-Sign-up?node-id=46%3A4707',
    },
  },
};

export function Step1() {
  return (
    <div
      style={{
        background: '#fff',
        minHeight: '100vh',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      }}
    >
      <StartPage>
        <RegStep1 />
      </StartPage>
    </div>
  );
}

export function Step2() {
  return (
    <div
      style={{
        background: '#fff',
        minHeight: '100vh',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      }}
    >
      <StartPage>
        <RegStep2 onSubmit={() => {}} upstreamError="Sever error..."/>
      </StartPage>
    </div>
  );
}

export function Step3() {
  return (
    <div
      style={{
        background: '#fff',
        minHeight: '100vh',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      }}
    >
      <StartPage>
        <RegStep3 />
      </StartPage>
    </div>
  );
}

export function Step4() {
  return (
    <div
      style={{
        background: '#fff',
        minHeight: '100vh',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      }}
    >
      <StartPage>
        <RegStep4 />
      </StartPage>
    </div>
  );
}
