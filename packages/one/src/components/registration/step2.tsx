import {
  Divider,
  FormControl,
  FormHelperText,
  Grid,
  Typography,
} from '@material-ui/core';
import clsx from 'clsx';
import { ErrorMessage, Formik } from 'formik';
import React from 'react';
import * as Yup from 'yup';
import {
  ButtonLoginWithProgress as ButtonLogin,
  Input,
  useStyle,
} from './common';
import logo from './logo.png';
import { MultipleSingleCharInput } from './multiple-single-char-input';

const loginSchema = Yup.object().shape({
  code: Yup.string().required('Registration code is required.'),
});

export function RegStep({
  email = 'abc@gmail.com',
  onSubmit = Function.prototype,
  onResendCode = Function.prototype,
  isLoading = false,
  upstreamError = '',
}) {
  const classes = useStyle();
  const [countDown, setCountDown] = React.useState(0);

  function cooldown() {
    setCountDown(60);
    let tid = window.setInterval(() => {
      setCountDown((countDown) => {
        if (countDown > 0) {
          return countDown - 1;
        } else {
          window.clearInterval(tid);
          return countDown;
        }
      });
    }, 1 * 1000);
  }

  return (
    <Grid
      container
      direction="column"
      alignItems="center"
      className={classes.root}
    >
      <Grid item>
        <img src={logo} alt="Epsilo logo" className={classes.logo} />
      </Grid>
      <Grid item xs={12} style={{ marginBottom: 52, marginTop: 64 }}>
        <Typography
          variant="h4"
          className={classes.title}
          align="center"
        >
          Check your email for a code
        </Typography>
        <Typography
          variant="body1"
          component={'p'}
          style={{ marginTop: 24 }}
          align="center"
        >
          We’ve sent a 6-characters code to <strong>{email}</strong>. This
          code expires shortly, so please enter it soon.
        </Typography>
      </Grid>
      <Grid item xs={12} style={{ width: '100%' }}>
        <Formik
          initialValues={{ code: '' }}
          validationSchema={loginSchema}
          onSubmit={(values, actions) => {
            onSubmit(values);
          }}
        >
          {({ handleSubmit, setValues, errors }) => {
            return (
              <form className={classes.form} onSubmit={handleSubmit}>
                <div
                  className={classes.row}
                  style={{ marginBottom: 48 }}
                >
                  <MultipleSingleCharInput
                    onChange={(val) => {
                      setValues({ code: val });
                    }}
                  />
                  {!errors.code && upstreamError && (
                    <FormHelperText error>
                      {upstreamError}
                    </FormHelperText>
                  )}
                  <ErrorMessage
                    component={FormHelperText}
                    name="code"
                  />
                </div>
                <FormControl className={classes.row}>
                  <ButtonLogin
                    type="submit"
                    color="inherit"
                    variant="contained"
                    isLoading={isLoading}
                  >
                    Continue
                  </ButtonLogin>
                </FormControl>
              </form>
            );
          }}
        </Formik>
      </Grid>
      <Grid
        item
        xs={12}
        style={{ width: '100%', marginTop: 12 }}
        justify={'center'}
      >
        <Typography component={'p'} align="center">
          Can't find your code? Check your spam folder!
        </Typography>
        <div className={classes.linkGroup} style={{ marginTop: 16 }}>
          <a
            href={'#'}
            className={clsx(
              classes.link,
              countDown > 0 && 'disabled',
            )}
            onClick={(evt) => {
              evt.preventDefault();
              onResendCode();
              cooldown();
            }}
          >
            Resend code {countDown > 0 && <span>({countDown})</span>}
          </a>
        </div>
      </Grid>
    </Grid>
  );
}
