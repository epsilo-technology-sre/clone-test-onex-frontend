import React from 'react';
import {
  Button,
  makeStyles,
  OutlinedInput,
  withStyles,
  CircularProgress,
} from '@material-ui/core';

export const Input = withStyles({
  root: {
    '& fieldset': {
      borderColor: '#C2C7CB',
      borderWidth: 2,
    },
    '&.Mui-focused fieldset': {
      borderColor: '#485764!important',
    },
  },
  input: {
    paddingTop: 14,
    paddingBottom: 14,
  },
})(OutlinedInput);

export const ButtonLogin = withStyles({
  root: {
    backgroundColor: '#ED5C10',
    color: '#fff',
    paddingTop: 10,
    paddingBottom: 10,
    textTransform: 'none',
    boxShadow: 'none',
    '&:hover': {
      backgroundColor: '#F17D40',
      borderColor: '#F17D40',
      boxShadow: 'none',
    },
  },
})(Button);

export function ButtonLoginWithProgress({
  isLoading = false,
  ...rest
}) {
  const classes = useStyle();
  return (
    <div className={classes.btnLoadingWrapper}>
      <ButtonLogin disabled={isLoading} {...rest} />
      {isLoading && (
        <CircularProgress
          size={24}
          className={classes.buttonProgress}
        />
      )}
    </div>
  );
}

export const useStyle = makeStyles((theme) => ({
  root: {
    width: 415,
  },
  logo: {
    height: 40,
  },
  row: {
    width: '100%',
  },
  title: {
    fontSize: 35,
    whiteSpace: 'nowrap',
  },
  form: {},
  link: {
    color: '#ED5C10',
    textDecoration: 'none',
    '&:hover': {
      color: '#ED5C10',
      textDecoration: 'underline'
    },
    '&.disabled': {
      color: '#C2C7CB',
      pointerEvents: 'none',
    }
  },
  linkGroup: {
    textAlign: 'center',
    color: '#ED5C10',
  },
  btnLoadingWrapper: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
  },
  buttonProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
}));

export function simplePasswordScore(str: string) {
  // reference: https://www.notion.so/phucpnt/Use-RegEx-To-Test-Password-Strength-In-JavaScript-871c4d5f5a5b445f8e2ad1c3555c6d03
  let patterns = [
    /^(?=.*[a-z])/,
    /^(?=.*[A-Z])/,
    /^(?=.*[0-9])/,
    /^(?=.*[!@#$%^&*])/,
    /^(?=.{8,})/,
  ];

  let score = 0;
  for (let patt of patterns) {
    score += patt.test(str) ? 1 : 0;
  }

  return score;
}
