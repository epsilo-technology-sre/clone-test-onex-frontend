import {
  FormControl,
  FormHelperText,
  Grid,
  IconButton,
  InputAdornment,
  Typography,
} from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import { ErrorMessage, Formik } from 'formik';
import React from 'react';
import * as Yup from 'yup';
import {
  ButtonLoginWithProgress as ButtonLogin,
  Input,
  simplePasswordScore,
  useStyle,
} from './common';
import logo from './logo.png';
import { PasswordStrengthIndicator } from './password-strength-indicator';

const loginSchema = Yup.object().shape({
  email: Yup.string(),
  firstName: Yup.string().required('First name is required.'),
  lastName: Yup.string().required('Last name is required.'),
  phone: Yup.string().optional(),
  password: Yup.string()
    .required('Password is required.')
    .min(6, 'Password is too short - should be 6 chars minimum.')
    .notOneOf(
      [Yup.ref('email')],
      'Password and email cannot be the same, please try another password',
    ),
});

export function RegStep({
  email = '',
  firstName = '',
  lastName = '',
  phone = '',
  password = '',
  onSubmit = Function.prototype,
  isLoading = false,
  upstreamError = '',
}) {
  const classes = useStyle();
  const [ctrls, setCtrls] = React.useState({
    showPassword: false,
  });

  const handleClickShowPassword = () => {
    setCtrls({ ...ctrls, showPassword: !ctrls.showPassword });
  };

  const handleMouseDownPassword = (event: any) => {
    event.preventDefault();
  };

  return (
    <Grid
      container
      direction="column"
      alignItems="center"
      className={classes.root}
    >
      <Grid item>
        <img src={logo} alt="Epsilo logo" className={classes.logo} />
      </Grid>
      <Grid item xs={12} style={{ marginBottom: 64, marginTop: 64 }}>
        <Typography
          variant="h4"
          className={classes.title}
          align="center"
        >
          Set up personal information
        </Typography>
      </Grid>
      <Grid item xs={12} style={{ width: '100%' }}>
        <Formik
          initialValues={{
            lastName,
            firstName,
            phone,
            password,
            email,
          }}
          validationSchema={loginSchema}
          onSubmit={(values, actions) => {
            onSubmit(values);
          }}
        >
          {({
            values,
            errors,
            handleChange,
            handleBlur,
            handleSubmit,
          }) => (
            <form className={classes.form} onSubmit={handleSubmit}>
              <Grid container spacing={3}>
                <Grid item md={6} xs={12}>
                  <FormControl
                    variant="outlined"
                    className={classes.row}
                    error={!!errors.firstName}
                  >
                    <Input
                      placeholder="Enter first name"
                      type="text"
                      name="firstName"
                      id="firstName"
                      onBlur={handleBlur}
                      onChange={handleChange}
                    ></Input>
                    <ErrorMessage
                      component={FormHelperText}
                      name="firstName"
                    />
                  </FormControl>
                </Grid>
                <Grid item md={6} xs={12}>
                  <FormControl
                    variant="outlined"
                    className={classes.row}
                    error={!!errors.lastName}
                  >
                    <Input
                      placeholder="Enter last name"
                      type="text"
                      name="lastName"
                      id="lastName"
                      onBlur={handleBlur}
                      onChange={handleChange}
                    ></Input>
                    <ErrorMessage
                      component={FormHelperText}
                      name="lastName"
                    />
                  </FormControl>
                </Grid>
                <Grid item md={12} xs={12}>
                  <FormControl
                    variant="outlined"
                    className={classes.row}
                    error={!!errors.phone}
                  >
                    <Input
                      placeholder="Enter phone number (optional)"
                      type="text"
                      name="phone"
                      id="phone"
                      onBlur={handleBlur}
                      onChange={handleChange}
                    ></Input>
                    <ErrorMessage
                      component={FormHelperText}
                      name="phone"
                    />
                  </FormControl>
                </Grid>

                <Grid item xs={12}>
                  <FormControl
                    variant="outlined"
                    className={classes.row}
                    error={!!errors.password}
                  >
                    <Input
                      placeholder="Your password"
                      name="password"
                      type={ctrls.showPassword ? 'text' : 'password'}
                      id="password"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={handleClickShowPassword}
                            onMouseDown={handleMouseDownPassword}
                          >
                            {ctrls.showPassword ? (
                              <Visibility />
                            ) : (
                              <VisibilityOff />
                            )}
                          </IconButton>
                        </InputAdornment>
                      }
                    ></Input>
                    <PasswordStrengthIndicator
                      strength={simplePasswordScore(values.password)}
                    />
                    <ErrorMessage
                      component={FormHelperText}
                      name="password"
                    />

                    {!Object.keys(errors).some((i: string) => {
                      // @ts-ignore
                      return errors[i];
                    }) &&
                      upstreamError && (
                        <FormHelperText error>
                          {upstreamError}
                        </FormHelperText>
                      )}
                  </FormControl>
                </Grid>
              </Grid>
              <Grid
                item
                xs={12}
                style={{ marginTop: 24, marginBottom: 24 }}
              >
                <Typography component={'p'}>
                  By signing up, I accept the Epsilo{' '}
                  <a href="#" className={classes.link}>
                    Terms of Service
                  </a>{' '}
                  and acknowledge the{' '}
                  <a href="#" className={classes.link}>
                    Privacy Policy
                  </a>
                  .
                </Typography>
              </Grid>
              <FormControl className={classes.row}>
                <ButtonLogin
                  type="submit"
                  color="inherit"
                  variant="contained"
                  isLoading={isLoading}
                >
                  Done
                </ButtonLogin>
              </FormControl>
            </form>
          )}
        </Formik>
      </Grid>
    </Grid>
  );
}
