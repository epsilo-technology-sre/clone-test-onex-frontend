import {
  Divider,
  FormControl,
  FormHelperText,
  Grid,
  Typography,
} from '@material-ui/core';
import { ErrorMessage, Formik } from 'formik';
import React from 'react';
import * as Yup from 'yup';
import {
  ButtonLoginWithProgress as ButtonLogin,
  Input,
  useStyle,
} from './common';
import logo from './logo.png';

const loginSchema = Yup.object().shape({
  code: Yup.string().required('Registration code is required.'),
});

export function RegStep({
  email = '',
  code = '',
  onSubmit = Function.prototype,
  isLoading = false,
  upstreamError = '',
}) {
  const classes = useStyle();

  return (
    <Grid
      container
      direction="column"
      alignItems="center"
      className={classes.root}
    >
      <Grid item>
        <img src={logo} alt="Epsilo logo" className={classes.logo} />
      </Grid>
      <Grid item xs={12} style={{ marginBottom: 44, marginTop: 64 }}>
        <Typography
          variant="h4"
          className={classes.title}
          align="center"
        >
          Insert Registration code
        </Typography>
        <Typography
          variant="body1"
          component={'p'}
          style={{ marginTop: 24 }}
          align="center"
        >
          Registration code is used to prove if a User is a member of
          a company. If you do not have this code, please contact your
          company administrator to require one
        </Typography>
      </Grid>
      <Grid item xs={12} style={{ width: '100%' }}>
        <Formik
          initialErrors={{ code }}
          initialValues={{ code }}
          validationSchema={loginSchema}
          onSubmit={(values, actions) => {
            onSubmit(values);
          }}
        >
          {({ errors, handleChange, handleBlur, handleSubmit }) => (
            <form className={classes.form} onSubmit={handleSubmit}>
              <FormControl
                variant="outlined"
                className={classes.row}
                error={!!errors.code}
                style={{ marginBottom: 24 }}
              >
                <Input
                  placeholder="Enter Registration code"
                  type="text"
                  name="code"
                  id="code"
                  onBlur={handleBlur}
                  onChange={handleChange}
                ></Input>
                {!errors.code && upstreamError && (
                  <FormHelperText error>
                    {upstreamError}
                  </FormHelperText>
                )}
                <ErrorMessage
                  component={FormHelperText}
                  name="code"
                />
              </FormControl>
              <FormControl className={classes.row}>
                <ButtonLogin
                  type="submit"
                  color="inherit"
                  variant="contained"
                  isLoading={isLoading}
                >
                  Continue
                </ButtonLogin>
              </FormControl>
            </form>
          )}
        </Formik>
      </Grid>
    </Grid>
  );
}
