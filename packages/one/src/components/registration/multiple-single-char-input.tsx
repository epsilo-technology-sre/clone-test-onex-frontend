import {
  makeStyles,
  OutlinedInput,
  withStyles,
} from '@material-ui/core';
import React, { SyntheticEvent, useEffect } from 'react';

const Input = withStyles(() => ({
  input: {
    fontSize: '48px',
    lineHeight: '48px',
    textAlign: 'center',
  },
}))(OutlinedInput);

export function MultipleSingleCharInput({
  onChange: comOnChange = (val: string) => {
    console.info('change', val);
  },
}) {
  const refRoot = React.useRef(null);
  const [chars, setChars] = React.useState(['', '', '', '', '', '']);

  useEffect(() => {
    comOnChange(chars.join(''));
  }, [chars]);

  function selectAll(evt: React.FocusEvent<HTMLInputElement>) {
    evt.currentTarget.select();
  }

  const onPaste = (evt: React.ClipboardEvent<HTMLInputElement>) => {
    let paste = evt.clipboardData.getData('text');
    setChars(paste.slice(0, 6).split(''));
    evt.preventDefault();
  };

  let onChange = (index: number) => (
    evt: React.ChangeEvent<HTMLInputElement>,
  ) => {
    chars.splice(index, 1, evt.target.value);
    setChars([...chars]);
    if (refRoot.current === null) return;
    // @ts-ignore
    let inputList = refRoot.current.querySelectorAll('input');

    let next: HTMLElement | null = null;
    if (index + 1 < chars.length) {
      next = inputList ? inputList[index + 1] : null;
    }
    window.setTimeout(() => {
      if (next !== null) {
        next.focus();
      }
    }, 50);
  };

  const classes = useStyles();
  return (
    <div className={classes.root} ref={refRoot}>
      {chars.map((i, index) => {
        if (index === 3) {
          return (
            <React.Fragment>
              <div key={'sep' + index} className={classes.separator}>
                {' '}
              </div>
              <Input
                inputProps={{ maxLength: 1 }}
                key={index}
                onFocus={selectAll}
                onPaste={onPaste}
                className={classes.unit}
                onChange={onChange(index)}
                value={i}
              />
            </React.Fragment>
          );
        }
        return (
          <Input
            inputProps={{ maxLength: 1 }}
            key={index}
            onFocus={selectAll}
            onPaste={onPaste}
            className={classes.unit}
            onChange={onChange(index)}
            value={i}
          />
        );
      })}
    </div>
  );
}

let useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    alignItems: 'center',
  },
  unit: {
    width: 64,
    height: 80,
  },
  separator: {
    width: 12,
    height: 2,
    backgroundColor: '#C2C7CB',
    margin: '0 13px',
  },
}));
