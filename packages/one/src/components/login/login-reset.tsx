import {
  Divider,
  FormControl,
  FormHelperText,
  Grid,
  IconButton,
  InputAdornment,
  Typography,
} from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import { ErrorMessage, Formik } from 'formik';
import React from 'react';
import * as Yup from 'yup';
import { ButtonLogin, Input, useStyle } from './common';
import logo from './logo.png';

const loginSchema = Yup.object().shape({
  password: Yup.string()
    .required('Password is required.')
    .min(6, 'Password is too short - should be 6 chars minimum.'),
  passwordConfirm: Yup.string()
    .required('Password is required.')
    .min(6, 'Password is too short - should be 6 chars minimum.'),
});

export function LoginReset({ password = '', urlLogin='#', urlRegister='#' }) {
  const classes = useStyle();

  const [values, setValues] = React.useState({
    showPassword: false,
    showPasswordConfirm: false,
  });

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };
  const handleClickShowPasswordConfirm = () => {
    setValues({
      ...values,
      showPasswordConfirm: !values.showPasswordConfirm,
    });
  };

  const handleMouseDownPassword = (event: any) => {
    event.preventDefault();
  };

  return (
    <Grid
      container
      direction="column"
      alignItems="center"
      className={classes.root}
    >
      <Grid item>
        <img src={logo} alt="Epsilo logo" className={classes.logo} />
      </Grid>
      <Grid item xs={12} style={{ marginBottom: 64, marginTop: 64 }}>
        <Typography variant="h4">Welcome back to Epsilo</Typography>
      </Grid>
      <Grid item xs={12} style={{ width: '100%' }}>
        <Formik
          initialValues={{ password, passwordConfirm: '' }}
          validationSchema={loginSchema}
          onSubmit={(values, actions) => {
            console.info(values);
          }}
        >
          {({ errors, handleChange, handleBlur, handleSubmit }) => (
            <form className={classes.form} onSubmit={handleSubmit}>
              <FormControl
                variant="outlined"
                className={classes.row}
                error={!!errors.password}
              >
                <Input
                  placeholder="Your new password"
                  name="password"
                  type={values.showPassword ? 'text' : 'password'}
                  id="password"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                      >
                        {values.showPassword ? (
                          <Visibility />
                        ) : (
                          <VisibilityOff />
                        )}
                      </IconButton>
                    </InputAdornment>
                  }
                ></Input>
                <ErrorMessage
                  component={FormHelperText}
                  name="password"
                />
              </FormControl>
              <FormControl
                variant="outlined"
                className={classes.row}
                error={!!errors.passwordConfirm}
              >
                <Input
                  placeholder="Confirm new password"
                  name="password"
                  type={values.showPassword ? 'text' : 'password'}
                  id="password"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPasswordConfirm}
                        onMouseDown={handleMouseDownPassword}
                      >
                        {values.showPassword ? (
                          <Visibility />
                        ) : (
                          <VisibilityOff />
                        )}
                      </IconButton>
                    </InputAdornment>
                  }
                ></Input>
                <ErrorMessage
                  component={FormHelperText}
                  name="password"
                />
              </FormControl>
              <FormControl className={classes.row}>
                <ButtonLogin
                  type="submit"
                  color="inherit"
                  variant="contained"
                >
                  Continue
                </ButtonLogin>
              </FormControl>
            </form>
          )}
        </Formik>
      </Grid>
      <Grid item xs={12} style={{ width: '100%' }} justify={'center'}>
        <Divider variant="fullWidth" style={{ marginBottom: 20 }} />
        <div className={classes.linkGroup}>
          <a href={urlLogin} className={classes.link}>
            Back to Sign in
          </a>
          {' • '}
          <a href={urlRegister} className={classes.link}>
            Sign up for new account
          </a>
        </div>
      </Grid>
    </Grid>
  );
}
