import {
  Divider,
  FormControl,
  FormHelperText,
  Grid,
  IconButton,
  InputAdornment,
  Typography,
} from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import { ErrorMessage, Formik } from 'formik';
import React from 'react';
import * as Yup from 'yup';
import { ButtonLogin, Input, useStyle } from './common';
import logo from './logo.png';

const loginSchema = Yup.object().shape({
  email: Yup.string()
    .required('Email is required.')
    .email('Invalid email address. The correct address might be: abc@xyz.com'),
  password: Yup.string()
    .required('Password is required.')
    .min(6, 'Password is too short - should be 6 chars minimum.'),
});

export function Login({
  email = '',
  password = '',
  urlForgotPassword = '#',
  urlRegister = '#',
  onSubmit,
}: {
  email?: string;
  password?: string;
  urlForgotPassword: string;
  urlRegister: string;
  onSubmit: (val: any) => Promise<string>;
}) {
  const classes = useStyle();

  const [values, setValues] = React.useState({
    showPassword: false,
  });

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event: any) => {
    event.preventDefault();
  };

  return (
    <Grid
      container
      direction="column"
      alignItems="center"
      className={classes.root}
    >
      <Grid item>
        <img src={logo} alt="Epsilo logo" className={classes.logo} />
      </Grid>
      <Grid item xs={12} style={{ marginBottom: 64, marginTop: 64 }}>
        <Typography variant="h4">Welcome back to Epsilo</Typography>
      </Grid>
      <Grid item xs={12} style={{ width: '100%' }}>
        <Formik
          initialValues={{ email, password }}
          validationSchema={loginSchema}
          onSubmit={(values, actions) => {
            onSubmit(values).catch((error) => {
              actions.setErrors({
                email: error.message,
                password: error.message,
              });
            });
          }}
        >
          {({ errors, handleChange, handleBlur, handleSubmit }) => (
            <form className={classes.form} onSubmit={handleSubmit}>
              <FormControl
                variant="outlined"
                className={classes.row}
                error={!!errors.email}
              >
                <Input
                  placeholder="Your email"
                  type="email"
                  name="email"
                  id="email"
                  onBlur={handleBlur}
                  onChange={handleChange}
                ></Input>
                <ErrorMessage
                  component={FormHelperText}
                  name="email"
                />
              </FormControl>
              <FormControl
                variant="outlined"
                className={classes.row}
                error={!!errors.password}
              >
                <Input
                  placeholder="Your password"
                  name="password"
                  type={values.showPassword ? 'text' : 'password'}
                  id="password"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                      >
                        {values.showPassword ? (
                          <Visibility />
                        ) : (
                          <VisibilityOff />
                        )}
                      </IconButton>
                    </InputAdornment>
                  }
                ></Input>
                <ErrorMessage
                  component={FormHelperText}
                  name="password"
                />
              </FormControl>
              <FormControl className={classes.row}>
                <ButtonLogin
                  type="submit"
                  color="inherit"
                  variant="contained"
                >
                  Continue
                </ButtonLogin>
              </FormControl>
            </form>
          )}
        </Formik>
      </Grid>
      <Grid item xs={12} style={{ width: '100%' }} justify={'center'}>
        <Divider variant="fullWidth" style={{ marginBottom: 20 }} />
        <div className={classes.linkGroup}>
          {/* <a href={urlForgotPassword} className={classes.link}>
            Forgot your password
          </a> */}
          {/* {' • '} */}
          <a href={urlRegister} className={classes.link}>
            Sign up for new account
          </a>
        </div>
      </Grid>
    </Grid>
  );
}
