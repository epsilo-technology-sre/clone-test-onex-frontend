import React from 'react';
import { Login } from './login';
import { withDesign } from 'storybook-addon-designs';
import { LoginForget } from './login-forget';
import { LoginReset } from './login-reset';
import { StartPage } from '../start-page';

export default {
  title: 'One / Login',
  decorators: [withDesign],
};

export function Primary() {
  return (
    <div
      style={{
        background: '#fff',
        height: '100vh',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      }}
    >
      <StartPage>
        <Login />
      </StartPage>
    </div>
  );
}

Primary.parameters = {
  design: {
    type: 'figma',
    url:
      'https://www.figma.com/file/GbYpnOQTIIHfcHBRzXlU0C/Sign-in-%2F-Sign-up?node-id=2%3A6186',
  },
};

export function ForgetPassword() {
  return (
    <div
      style={{
        background: '#fff',
        height: '100vh',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      }}
    >
      <StartPage>
        <LoginForget />
      </StartPage>
    </div>
  );
}

export function ResetPassword() {
  return (
    <div
      style={{
        background: '#fff',
        height: '100vh',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      }}
    >
      <StartPage>
        <LoginReset />
      </StartPage>
    </div>
  );
}
