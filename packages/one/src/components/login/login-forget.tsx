import {
  Divider,
  FormControl,
  FormHelperText,
  Grid,
  Typography,
} from '@material-ui/core';
import { ErrorMessage, Formik } from 'formik';
import React from 'react';
import * as Yup from 'yup';
import { ButtonLogin, Input, useStyle } from './common';
import logo from './logo.png';

const loginSchema = Yup.object().shape({
  email: Yup.string()
    .required('Email is required.')
    .email('Please enter valid email.'),
});

export function LoginForget({ email = '', urlLogin='', urlRegister = '' }) {
  const classes = useStyle();

  return (
    <Grid
      container
      direction="column"
      alignItems="center"
      className={classes.root}
    >
      <Grid item>
        <img src={logo} alt="Epsilo logo" className={classes.logo} />
      </Grid>
      <Grid item xs={12} style={{ marginBottom: 64, marginTop: 64 }}>
        <Typography variant="h4">Forgotten your password?</Typography>
        <Typography
          variant="body1"
          component={'p'}
          style={{ marginTop: 24 }}
          align="center"
        >
          Enter your email address you’re using for your account below
          and we will send you a password reset link.
        </Typography>
      </Grid>
      <Grid item xs={12} style={{ width: '100%' }}>
        <Formik
          initialValues={{ email }}
          validationSchema={loginSchema}
          onSubmit={(values, actions) => {
            console.info(values);
          }}
        >
          {({ errors, handleChange, handleBlur, handleSubmit }) => (
            <form className={classes.form} onSubmit={handleSubmit}>
              <FormControl
                variant="outlined"
                className={classes.row}
                error={!!errors.email}
              >
                <Input
                  placeholder="Your email"
                  type="email"
                  name="email"
                  id="email"
                  onBlur={handleBlur}
                  onChange={handleChange}
                ></Input>
                <ErrorMessage
                  component={FormHelperText}
                  name="email"
                />
              </FormControl>
              <FormControl className={classes.row}>
                <ButtonLogin
                  type="submit"
                  color="inherit"
                  variant="contained"
                >
                  Continue
                </ButtonLogin>
              </FormControl>
            </form>
          )}
        </Formik>
      </Grid>
      <Grid item xs={12} style={{ width: '100%' }} justify={'center'}>
        <Divider variant="fullWidth" style={{ marginBottom: 20 }} />
        <div className={classes.linkGroup}>
          <a href={urlLogin} className={classes.link}>
            Back to Sign in
          </a>
          {' • '}
          <a href={urlRegister} className={classes.link}>
            Sign up for new account
          </a>
        </div>
      </Grid>
    </Grid>
  );
}
