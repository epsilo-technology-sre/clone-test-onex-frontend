import {
  Button,
  makeStyles,
  OutlinedInput,
  withStyles,
} from '@material-ui/core';

export const Input = withStyles({
  root: {
    '& fieldset': {
      borderColor: '#C2C7CB',
      borderWidth: 2,
    },
    '&.Mui-focused fieldset': {
      borderColor: '#485764!important',
    },
  },
  input: {
    paddingTop: 14,
    paddingBottom: 14,
  },
})(OutlinedInput);

export const ButtonLogin = withStyles({
  root: {
    backgroundColor: '#ED5C10',
    color: '#fff',
    paddingTop: 10,
    paddingBottom: 10,
    textTransform: 'none',
    boxShadow: 'none',
    '&:hover': {
      backgroundColor: '#F17D40',
      borderColor: '#F17D40',
      boxShadow: 'none',
    },
  },
})(Button);

export const useStyle = makeStyles(() => ({
  root: {
    width: 415,
  },
  logo: {
    height: 40,
  },
  title: {
    fontSize: 35,
  },
  row: {
    marginBottom: 24,
    width: '100%',
  },
  form: {},
  link: {
    color: '#ED5C10',
    '&:hover': {
      color: '#ED5C10',
    },
  },
  linkGroup: {
    textAlign: 'center',
    color: '#ED5C10',
  },
}));
