import {
  Avatar,
  Box,
  Divider,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Menu,
  MenuItem,
} from '@material-ui/core';
import MuiAppBar from '@material-ui/core/AppBar';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import MuiToolbar from '@material-ui/core/Toolbar';
import clsx from 'clsx';
import React from 'react';
import { Link } from 'react-router-dom';
import { useOneMenu } from '../../hooks/use-menu';
import ChevronLeft from './chevron-left.svg';
import ChevronRight from './chevron-right.svg';
import DashboardIcon from './dashboard-24px.svg';
import DashboardIconLight from './dashboard-light.svg';
import Logo from './logo-beta.svg';
import ShopeeActive from './shopee-active.svg';
import ShopeeInactive from './shopee-inactive.svg';

console.info('logo', Logo);

const AppBar = withStyles(() => ({
  colorPrimary: {
    backgroundColor: '#253746',
  },
}))(MuiAppBar);

const Toolbar = withStyles(() => ({
  regular: {
    height: 56,
    minHeight: 56,
  },
}))(MuiToolbar);

const defaultTopMenuItems = [
  { title: 'Overview', url: '#' },
  { title: 'Orders', url: '#' },
  { title: 'Pricing', url: '#' },
  { title: 'Advertising', url: '#' },
  { title: 'Shops', url: '#' },
];

const defaultLevel1Items = [
  {
    icon: <img src={DashboardIcon} />,
    iconActive: <img src={DashboardIconLight} />,
    url: '#',
    title: 'Dashboard',
  },
  {
    icon: <img src={ShopeeInactive} />,
    iconActive: <img src={ShopeeActive} />,
    url: '#',
    title: 'Dashboard',
  },
];

export const OnePageContext = React.createContext({
  userFullName: 'User Fullname',
  userEmail: 'user@email.com',
  onLogout: () => {
    console.info('logout');
  },
});

export function TopAppBar({
  topMenuItems = defaultTopMenuItems,
  activeIndex = 0,
}) {
  const classes = useStyles();
  const menuClasses = useStyleMenu();

  const { userEmail, onLogout } = React.useContext(OnePageContext);
  const [openProfileMenu, setOpenProfileMenu] = React.useState(0);
  const [anchorEl, setAnchorEl] = React.useState(null);

  const openProfile = (evt) => {
    setOpenProfileMenu(1);
    setAnchorEl(evt.currentTarget);
  };

  const handleMenuClose = (reason) => {
    if (reason === 'logout') {
      onLogout();
      setOpenProfileMenu(0);
    } else {
      setOpenProfileMenu(0);
    }
  };

  return (
    <div className={classes.appBarRoot} id="epsilo-appbar">
      <AppBar position="fixed">
        <Toolbar>
          <img src={Logo} />
          <div className={classes.menuWrapper}>
            {topMenuItems.map((i, index) => (
              <Link
                key={index}
                to={i.url}
                className={clsx(
                  classes.item,
                  index === activeIndex && 'active',
                )}
              >
                {i.title}
              </Link>
            ))}
          </div>
          <div className={classes.grow}></div>
          <IconButton color="inherit" onClick={openProfile}>
            <Avatar className={menuClasses.avatar}>
              {userEmail.slice(0, 1)}
            </Avatar>
          </IconButton>
        </Toolbar>
        <UserMenu
          anchorEl={anchorEl}
          isMenuOpen={openProfileMenu}
          handleMenuClose={handleMenuClose}
        />
      </AppBar>
    </div>
  );
}

const useStyleMenu = makeStyles((theme) => ({
  menuWrapper: {
    fontSize: '14px',
    '& *:focus': {
      outline: 'none',
    },
  },
  root: {
    width: 240,
  },
  avatar: {
    width: theme.spacing(4),
    height: theme.spacing(4),
    fontSize: '0.7em',
    textTransform: 'uppercase',
    border: '1px solid #fff',
    background: '#253746',
  },
  avatarLarge: {
    width: theme.spacing(5),
    height: theme.spacing(5),
    fontSize: '1.2em',
    textTransform: 'uppercase',
    border: '1px solid #fff',
    background: '#253746',
  },
  logoutLink: {
    color: '#D4290D',
  },
}));
function UserMenu({ anchorEl, isMenuOpen, handleMenuClose }) {
  const classes = useStyleMenu();
  const { userEmail, userFullName } = React.useContext(
    OnePageContext,
  );
  return (
    <Menu
      anchorEl={anchorEl}
      className={classes.menuWrapper}
      id={'top-user-profile'}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'right',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      open={!!isMenuOpen}
      onClose={() => handleMenuClose()}
      getContentAnchorEl={null}
      classes={{
        paper: classes.root,
      }}
      autoFocus={false}
      disableAutoFocus={true}
    >
      <List>
        <ListItem>
          <ListItemAvatar>
            <Avatar className={classes.avatarLarge}>
              {userEmail.slice(0, 1)}
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary={userFullName}
            secondary={userEmail}
          />
        </ListItem>
      </List>
      <Divider />
      <MenuItem
        className={classes.logoutLink}
        onClick={() => handleMenuClose('logout')}
      >
        Logout
      </MenuItem>
    </Menu>
  );
}

function LeftPanel1({ items = defaultLevel1Items, activeIndex = 0 }) {
  let classes = useStyles();
  return (
    <div className={classes.leftPanelRoot} id="epsilo-left-menu">
      {items
        .filter((i) => i.access !== false)
        .map((i, index) => {
          const isActive = index === activeIndex;
          return (
            <Link key={index} to={i.url}>
              <div
                key={index}
                className={clsx(
                  classes.level1Item,
                  isActive && 'active',
                )}
              >
                {isActive ? i.iconActive : i.icon}
              </div>
            </Link>
          );
        })}
    </div>
  );
}

function LeftPanel2({
  items = [
    { title: 'Dashboard', url: '#', hidden: false },
    { title: 'Campaign management', url: '#' },
    { title: 'Keywords analytic', url: '#' },
    { title: 'Rule management', url: '#' },
  ],
  activeIndex = 0,
  expanded = true,
  toggleExpanded = Function.prototype,
}) {
  let classes = useStyles();
  let { customLeftPanel } = useOneMenu();

  let panelContent = customLeftPanel
    ? customLeftPanel
    : items.map((i, index) => {
        const isActive = index === activeIndex;
        if (i.hidden || i.access === false) {
          return null;
        }
        return (
          <Link
            to={i.url}
            key={index}
            className={clsx(classes.level2Item, isActive && 'active')}
          >
            <div>{i.title}</div>
          </Link>
        );
      });

  return (
    <div
      className={clsx(
        classes.leftPanel2Root,
        !expanded && 'collapsed',
      )}
    >
      <div className={'wrapper'}>
        <div className={'menuList'}>{panelContent}</div>
        <div
          className={classes.toggleExpanded}
          onClick={() => toggleExpanded()}
        >
          <img src={expanded ? ChevronLeft : ChevronRight} />
        </div>
      </div>
    </div>
  );
}

type navigationItem = {
  title: string;
  url: string;
  hidden?: boolean;
  access?: boolean;
  children: {
    icon: JSX.Element;
    iconActive: JSX.Element;
    url: string;
    title: string;
    hidden?: boolean;
    access?: boolean;
    children?: {
      title: string;
      url: string;
      hidden?: boolean;
      access?: boolean;
    }[];
  }[];
};

export function InternalPage(props: {
  children: JSX.Element;
  navigation: navigationItem[];
  activePath: number[];
}) {
  const classes = useStyles();

  const [lp2Expanded, setLp2Expanded] = React.useState(true);

  const navigation = props.navigation;
  const topIndex = props.activePath[0];
  const lvl1Index = props.activePath[1];
  const lvl2Index = props.activePath[2];

  let lvl1Items;
  if (topIndex > -1) lvl1Items = navigation[topIndex].children || [];

  let lvl2Items;
  if (lvl1Index > -1) {
    lvl2Items =
      navigation[topIndex].children[lvl1Index].children || [];
  }

  return (
    <div
      className={clsx(
        classes.pageRoot,
        lvl2Items && lvl2Items.length > 0 ? 'haveLvl2' : 'noLvl2',
        lp2Expanded && 'lp2Visible',
      )}
    >
      <TopAppBar topMenuItems={navigation} activeIndex={topIndex} />
      <LeftPanel1 items={lvl1Items || []} activeIndex={lvl1Index} />
      {lvl2Items && lvl2Items.length > 0 && (
        <LeftPanel2
          expanded={lp2Expanded}
          items={lvl2Items || []}
          toggleExpanded={() => {
            setLp2Expanded((expanded) => !expanded);
          }}
          activeIndex={lvl2Index}
        />
      )}
      <Box
        display={'none'}
        textAlign={'center'}
        alignItems={'center'}
        color={'rgb(11, 163, 115)'}
        padding={'12px'}
        bgcolor={'#ECF8F4'}
      >
        Key performance metrics for Shopee are audited and
        auto-recovered twice a day if any discrepancy arises, this
        include Ads Spend, GMV, Impression, Click, and their
        derivative KPIs. The "Total item sold" is under monitoring
        until <strong>March 31st</strong>.
        <br />
        Due to the migration period of Sponsored Discovery from
        Lazada, the data from Lazada might have discrepancies until{' '}
        <strong>March 31st</strong>. The Sponsored Search will be
        ready to operate from Epsilo since <strong>March 22nd</strong>
        .
      </Box>
      <div className={classes.contentWrapper}>{props.children}</div>
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
  pageRoot: {
    marginTop: 56,
    marginLeft: 48,
    '&.noLvl2': {
      marginLeft: 48,
    },
    '&.haveLvl2.lp2Visible': {
      marginLeft: 288,
    },
  },
  grow: {
    flexGrow: 1,
  },
  appBarRoot: {
    flexGrow: 1,
    fontSize: 14,
  },
  contentWrapper: {
    boxSizing: 'border-box',
    padding: 40,
    background: '#fff',
    minHeight: '100vh',
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  menuWrapper: {
    marginLeft: 26,
    height: '100%',
    display: 'flex',
    flexDirection: 'row',
  },
  item: {
    height: '100%',
    color: '#fff',
    padding: '6px 8px',
    marginLeft: 16,
    display: 'flex',
    alignItems: 'center',
    textDecoration: 'none',

    '&.active': {
      position: 'relative',
      '&::after': {
        position: 'absolute',
        content: '""',
        left: 0,
        bottom: 0,
        width: '100%',
        height: 2,
        backgroundColor: '#ED5C10',
        borderRadius: 2,
      },
    },
  },
  leftPanelRoot: {
    userSelect: 'none',
    position: 'fixed',
    left: 0,
    bottom: 0,
    width: 48,
    height: '100%',
    backgroundColor: '#F6F7F8',
    borderRight: '2px solid #E4E7E9',
    paddingTop: 72,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    zIndex: 9,
  },
  level1Item: {
    width: 32,
    height: 32,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    marginBottom: 12,
    cursor: 'pointer',

    '&.active, &.active:hover': {
      backgroundColor: '#253746',
    },
    '&:hover': {
      backgroundColor: '#E4E7E9',
    },
  },

  leftPanel2Root: {
    userSelect: 'none',
    position: 'fixed',
    width: 240,
    top: 0,
    left: 48,
    bottom: 0,
    backgroundColor: '#F6F7F8',
    borderRight: '2px solid #E4E7E9',
    paddingTop: 72,
    transitionProperty: 'width',
    transitionDuration: '0.5s',
    '& .wrapper': {
      paddingLeft: 16,
      paddingRight: 16,
      width: '100%',
      height: '100%',
      position: 'relative',
    },
    '&.collapsed': {
      width: 16,
      '& .menuList': {
        width: 0,
        minWidth: 0,
        overflow: 'hidden',
      },
    },
    '& .menuList': {
      minWidth: 208,
    },
  },
  level2Item: {
    display: 'block',
    padding: '12px 8px',
    borderRadius: 4,
    color: '#253746',
    textDecoration: 'none',
    '&.active': {
      color: '#ED5C10',
    },
    '&:hover': {
      backgroundColor: '#E4E7E9',
    },
  },
  toggleExpanded: {
    width: 24,
    height: 24,
    opacity: 0,
    '$leftPanel2Root:hover &': {
      opacity: 1,
    },
    transitionProperty: 'opacity',
    transitionDuration: '0.1s',
    position: 'absolute',
    borderRadius: '50%',
    background: '#fff',
    boxShadow:
      '0px 8px 12px rgba(37, 55, 70, 0.15), 0px 0px 1px rgba(37, 55, 70, 0.31)',
    right: -12,
    top: 12,
    cursor: 'pointer',
    textAlign: 'center',
    lineHeight: '24px',
    '&:hover': {
      backgroundColor: '#D3D7DA',
    },
    '.collapsed &': {
      right: 4,
      opacity: '1!important',
    },
  },
}));
