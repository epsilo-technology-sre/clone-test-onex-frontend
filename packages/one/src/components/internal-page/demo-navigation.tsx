import React from 'react';
import DashboardIcon from '../../images/dashboard-24px.svg';
import DashboardIconLight from '../../images/dashboard-light.svg';
import ShopeeActive from '../../images/shopee-active.svg';
import ShopeeInactive from '../../images/shopee-inactive.svg';

export const navigation = [
  {
    title: 'Overview',
    url: '#',
    children: [
      {
        icon: <img src={DashboardIcon} />,
        iconActive: <img src={DashboardIconLight} />,
        url: '#',
        title: 'Dashboard',
      },
    ],
  },
  {
    title: 'Orders',
    url: '#',
    children: [
      {
        icon: <img src={DashboardIcon} />,
        iconActive: <img src={DashboardIconLight} />,
        url: '#',
        title: 'Dashboard',
      },
    ],
  },
  {
    title: 'Pricing',
    url: '#',
    children: [
      {
        icon: <img src={DashboardIcon} />,
        iconActive: <img src={DashboardIconLight} />,
        url: '#',
        title: 'Dashboard',
      },
    ],
  },
  {
    title: 'Advertising',
    url: '#',
    children: [
      {
        icon: <img src={DashboardIcon} />,
        iconActive: <img src={DashboardIconLight} />,
        url: '#',
        title: 'Dashboard',
      },
      {
        icon: <img src={ShopeeInactive} />,
        iconActive: <img src={ShopeeActive} />,
        url: '#',
        title: 'Dashboard',
        children: [
          { title: 'Dashboard', url: '#' },
          { title: 'Campaign management', url: '#' },
          { title: 'Keywords analytic', url: '#' },
          { title: 'Rule management', url: '#' },
        ],
      },
    ],
  },
  {
    title: 'Shops',
    url: '#',
    children: [
      {
        icon: <img src={DashboardIcon} />,
        iconActive: <img src={DashboardIconLight} />,
        url: '#',
        title: 'Dashboard',
      },
    ],
  },
];
