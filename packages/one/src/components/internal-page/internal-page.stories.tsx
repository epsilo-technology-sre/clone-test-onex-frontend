import React from 'react';
import { navigation } from './demo-navigation';
import { InternalPage } from './internal-page';
import { HashRouter as Router } from 'react-router-dom';
import { MenuContext, useOneMenu } from '../../hooks/use-menu';
import { Button } from '@material-ui/core';

export default {
  title: 'One / Site Menu',
};

export function NoLevel1Children() {
  let contents = Array(100).fill(
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  );

  return (
    <Router>
      <InternalPage navigation={navigation} activePath={[0, 0, 0]}>
        <React.Fragment>
          <h1>Hello page</h1>
          {contents.map((c, index) => (
            <p key={index}>{c}</p>
          ))}
        </React.Fragment>
      </InternalPage>
    </Router>
  );
}

export function HaveLevel2Children() {
  let contents = Array(100).fill(
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  );

  return (
    <Router>
      <InternalPage navigation={navigation} activePath={[3, 1, 0]}>
        <React.Fragment>
          <h1>Hello page</h1>
          {contents.map((c, index) => (
            <p key={index}>{c}</p>
          ))}
        </React.Fragment>
      </InternalPage>
    </Router>
  );
}

export function HaveLevel2WithCustomLeftPanel() {
  const [customLeftPanel, setCustomLeftPanel ] = React.useState(null);

  return (
    <Router>
      <MenuContext.Provider
        value={{ customLeftPanel, setCustomLeftPanel, resetCustomLeftPanel: () => setCustomLeftPanel(null) }}
      >
        <InternalPage navigation={navigation} activePath={[3, 1, 0]}>
          <React.Fragment>
            <h1>Hello page With custom Left panel</h1>

            <PagePushCustomLeftPanel></PagePushCustomLeftPanel>
          </React.Fragment>
        </InternalPage>
      </MenuContext.Provider>
    </Router>
  );
}

function PagePushCustomLeftPanel() {
  const { setCustomLeftPanel, resetCustomLeftPanel } = useOneMenu();
  let contents = Array(100).fill(
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  );

  React.useEffect(
    () => setCustomLeftPanel(<h1>Hello custom</h1>),
    [],
  );

  return (
    <React.Fragment>
      <Button onClick={() => {resetCustomLeftPanel()}}>Reset menu</Button>
      {contents.map((c, index) => (
        <p key={index}>{c}</p>
      ))}
    </React.Fragment>
  );
}
