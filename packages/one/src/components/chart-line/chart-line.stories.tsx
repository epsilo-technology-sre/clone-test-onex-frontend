import React from 'react';
import { ChartLine } from './chart-line';
import { demoDates, demoData } from './demo-data';
import { random } from 'lodash';

const demos = demoDates.map((i, index) => [i, demoData[index]]);

export default {
  title: 'One / Dashboard Chart',
};

export function Primary() {
  let series = [
    {
      title: 'Metric 1',
      color: '#EE6923',
      data: demoData,
      unitFormat: '',
    },
    {
      title: 'Metric 2',
      color: '#0BA373',
      data: demoData.map((i) => i + random(-1, 1, true)),
      unitFormat: '',
    },
    {
      title: 'Metric 3',
      color: '#147DD2',
      data: demoData.map((i) => i + random(-1, 1, true)),
      unitFormat: '',
    },
    {
      title: 'Metric 4',
      color: '#485764',
      data: demoData.map((i) => i + random(-1, 1, true)),
      unitFormat: '',
    },
  ];

  return (
    <ChartLine series={series} dates={demoDates} maxSeriesCount={4} />
  );
}
