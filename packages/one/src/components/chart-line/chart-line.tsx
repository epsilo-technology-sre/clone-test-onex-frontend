import React from 'react';
import * as Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import { Box, makeStyles, Typography } from '@material-ui/core';
import moment from 'moment';
import numeral from 'numeral';

type ChartLineProps = {
  series: {
    title: string;
    color: string;
    currency: string;
    isPercent: boolean;
    data: [number, number][];
    unitFormat: string;
  }[];
  dates: number[];
  maxSeriesCount: number;
};

(function useitlater(H) {
  H.addEvent(H.Axis, 'afterInit', function () {
    const logarithmic = this.logarithmic;

    if (logarithmic && this.options.custom.allowNegativeLog) {
      // Avoid errors on negative numbers on a log axis
      this.positiveValuesOnly = false;

      // Override the converter functions
      logarithmic.log2lin = (num) => {
        const isNegative = num < 0;

        let adjustedNum = Math.abs(num);

        if (adjustedNum < 10) {
          adjustedNum += (10 - adjustedNum) / 10;
        }

        let result = Math.log(adjustedNum) / Math.LN10;
        // console.info('log2lin', num, result);

        if (result > 7) {
          result -= 6;
        }

        return isNegative ? -result : result;
      };

      logarithmic.lin2log = (num) => {
        const isNegative = num < 0;

        let result = Math.pow(10, Math.abs(num));
        if (result > Math.pow(10, 1)) {
          result = Math.pow(10, 6) * result;
        }

        if (result < 10) {
          result = (10 * (result - 1)) / (10 - 1);
        }
        // console.info('lin2log', num, result);
        return isNegative ? -result : result;
      };
    }
  });
});

(function (H) {
  H.addEvent(H.Axis, 'afterInit', function () {
    const logarithmic = this.logarithmic;

    if (logarithmic && this.options.custom.allowNegativeLog) {
      // Avoid errors on negative numbers on a log axis
      this.positiveValuesOnly = false;

      // Override the converter functions
      logarithmic.log2lin = (num) => {
        if (num === null) return null;
        const isNegative = num < 0;

        let adjustedNum = Math.abs(num);

        if (adjustedNum < 10) {
          adjustedNum += (10 - adjustedNum) / 10;
        }

        const result = Math.log(adjustedNum) / Math.LN10;
        return isNegative ? -result : result;
      };

      logarithmic.lin2log = (num) => {
        if (num === null) return null;
        const isNegative = num < 0;

        let result = Math.pow(10, Math.abs(num));
        if (result < 10) {
          result = (10 * (result - 1)) / (10 - 1);
        }
        return isNegative ? -result : result;
      };
    }
  });
})(Highcharts);

export function ChartLine(props: ChartLineProps) {
  const classes = useStyle();

  const options: Highcharts.Options = {
    chart: {
      type: 'spline',
    },
    credits: {
      enabled: false,
    },
    legend: {
      enabled: false,
    },
    title: {
      text: '',
    },
    yAxis: props.series.map((s, index) => ({
      labels: {
        enabled: false,
      },
      title: {
        text: null,
      },
      custom: {
        allowNegativeLog: true,
      },
      tickPixelInterval: 50,
      alignTicks: false,
      gridLineWidth: index > 0 ? 0 : 1,
    })),
    xAxis: {
      type: 'datetime',
      tickLength: 0,
      lineColor: '#D3D7DA',
      lineWidth: 2,
      gridLineColor: '#F6F7F8',
      gridLineWidth: 1,
      showFirstLabel: true,
      showLastLabel: true,
      crosshair: true,
      labels: {
        format: '{value: %d/%m}',
      },
      min: props.dates[0],
      max: props.dates.slice(-1)[0],
    },
    tooltip: {
      shared: true,
      borderRadius: 4,
      borderWidth: 0,
      padding: 0,
      backgroundColor: '#fff',
      useHTML: true,

      formatter: function (tooltip) {
        let self = this;
        let points =
          self.points?.map((i) => {
            let {
              metricId,
              isPercent,
              currency,
            } = i.series.userOptions.custom;

            return {
              name: i.series.name,
              color: i.color,
              value: i.y,
              metricId,
              isPercent,
              currency,
            };
          }) || [];
        return `
        <div class="${classes.tooltip}">
        <div class="${classes.ttTitle}">${moment(self.x).format(
          'dddd, DD/MM/YYYY',
        )}</div>
        <div class="${classes.ttContent}">
        <table>
          <tbody>
            ${points
              .map(
                (p, index) =>
                  `<tr>
                  <td>
                    <div class="name">
                    <span class="${
                      classes.indicator
                    }" style="background-color:${p.color}"></span>
                    <span>${p.name}</span>
                    </div>
                  </td>
                  <td class="value">${numeral(p.value).format(
                    '0,0[.]00',
                  )}&nbsp;${p.isPercent ? '%' : p.currency || ''}</td>
                </tr>`,
              )
              .join('')}
          </tbody>
        </table>
        </div>
        </div>
        `;
      },
    },
    series: props.series.map(
      (i, index): Highcharts.SeriesOptionsType => {
        return {
          type: 'spline',
          name: i.title,
          data: i.data.map(([d, v]) => [d, v]),
          custom: {
            metricId: i.metricId,
            isPercent: i.isPercent,
            currency: i.currency,
          },
          lineWidth: 1,
          yAxis: index,
          marker: {
            enabled: false,
            symbol: 'circle',
          },
          color: i.color,
          zoneAxis: 'y',
        };
      },
    ),
  };

  const conRef = React.useRef<HTMLDivElement>(null);
  const chartRef = React.useRef<any>(null);
  React.useEffect(() => {
    const observer = new window.ResizeObserver((els: any) => {
      for (let el of els) {
        if (el.target === conRef.current) {
          chartRef.current.chart.reflow();
        }
      }
    });

    if (conRef.current) {
      observer.observe(conRef.current);
    }

    return () => {
      observer.unobserve(conRef.current);
    };
  }, []);

  return (
    <div className={classes.root} ref={conRef}>
      <Box display="flex" marginBottom={2}>
        <Box>
          <span style={{ color: '#596772' }}>Selected</span>{' '}
          {props.series.length}/{props.maxSeriesCount}
        </Box>
        {props.series.map((i) => (
          <Box
            key={i.title}
            display="flex"
            alignItems="center"
            marginLeft={2}
          >
            <span
              className={classes.indicator}
              style={{ backgroundColor: i.color }}
            ></span>
            {i.title}
          </Box>
        ))}
      </Box>
      <HighchartsReact
        ref={chartRef}
        highcharts={Highcharts}
        options={options}
        {...props}
      />
    </div>
  );
}

const useStyle = makeStyles(() => ({
  root: {},
  indicator: {
    display: 'inline-block',
    height: 4,
    width: 12,
    borderRadius: 2,
    marginRight: 6,
    flexShrink: 0,
  },
  ttTitle: {
    backgroundColor: '#F6F7F8',
    padding: 8,
    fontSize: 11,
    fontWeight: 700,
    color: '#253746',
  },
  ttContent: {
    padding: 8,
    '& .name': {
      display: 'flex',
      alignItems: 'center',
      marginRight: 30,
      color: '#596772',
      marginBottom: 8,
    },
    '& .value': {
      verticalAlign: 'initial',
    },
  },
  tooltip: {
    color: '#253746',
    fontSize: 12,
    minWidth: 160,
  },
}));
