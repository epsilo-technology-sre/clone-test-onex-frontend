import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import MuiCard from '@material-ui/core/Card';
import MuiCardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import { ArrowBack, ArrowBackIos } from '@material-ui/icons';
import {Grid, Link} from '@material-ui/core';
import { formatNumber } from '../../../utils';

const Card = withStyles({
  root: {
    boxShadow:
      '0px 1px 1px rgba(37, 55, 70, 0.25), 0px 0px 1px rgba(37, 55, 70, 0.31);',
  },
})(MuiCard);

const CardHeader = withStyles({
  root: {
    padding: 8,
    paddingBottom: 2,
  },
  title: {
    fontSize: '10px',
    fontWeight: 400,
    color: '#596772',
  },
  action: {
    marginTop: 0,
    marginRight: 0,
    cursor: 'pointer',
  },
})(MuiCardHeader);

export function AssessmentCard(props: {
  metricId: string;
  title: string;
  content: any[];
  onCloseAssessment: (metricId: string) => void;
}) {
  const classes = useStyles(props);

  return (
    <Grid item md={3} xs={12} lg={2}>
      <Card style={{ position: 'relative', height: 100 }}>
        <CardHeader
          action={
            <ArrowBack
              className={classes.closeIcon}
              onClick={(evt) => {
                evt.preventDefault();
                evt.stopPropagation();
                props.onCloseAssessment(props.metricId);
              }}
            />
          }
          title={props.title}
        />
        <CardContent className={classes.cardContent}>
          <div>
            {props.content?.map((item, index) => (
              <div
                key={index}
                className={classes.vsPrevious}
                style={{ marginTop: 0 }}
              >
                <span style={{ fontWeight: 'bold', fontSize: 13 }}>
                  {item.label}
                </span>
                <span
                  className={classes.value}
                  style={{
                    color:
                      item.color == 'red' ? '#E03C21' : '#0BA373',
                  }}
                >
                  {formatNumber(item.value)} {item.unit}
                </span>{' '}
                <span
                  className={classes.percent}
                  style={{
                    color:
                      item.color == 'red' ? '#E03C21' : '#0BA373',
                  }}
                >
                  {item.percent}%
                </span>
              </div>
            ))}
          </div>
        </CardContent>
      </Card>
    </Grid>
  );
}

const useStyles = makeStyles((theme) => ({
  root: (props: any) => {
    if (props.color) {
      return {
        borderColor: props.color,
        borderWidth: 2,
        borderStyle: 'solid',
        cursor: 'pointer',
        height: '100%',
      };
    }
    return {
      padding: '2px',
      cursor: 'pointer',
      height: '100%',
    };
  },

  iconCheck: {
    color: '#0BA373',
    fontSize: '12px',
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  cardContent: {
    padding: 8,
    paddingTop: 2,
    '&:last-child': {
      paddingBottom: 8,
    },
  },
  vsPrevious: {
    marginTop: '8px',
    fontSize: '10px',
    color: '#596772',
    '& .percent': {
      '&::after': {
        content: '"%"',
      },
    },
    '& .positive': {
      color: '#0BA373',
    },
    '& .neutral': {
      color: '#0BA373',
    },
    '& .negative': {
      color: '#E03C21',
    },
    '& .negative.neg-improved': {
      color: '#0BA373',
    },
    '& .postive.neg-improved': {
      color: '#E03C21',
    },
  },
  closeIcon: {
    fontSize: '16px',
  },
  percent: {
    fontSize: 14,
    position: 'absolute',
    right: 7,
  },
  value: {
    fontWeight: 'bold',
    fontSize: 14,
    position: 'absolute',
    right: 60,
  },
}));
