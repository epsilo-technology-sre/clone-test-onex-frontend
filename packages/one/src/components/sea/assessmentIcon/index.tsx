import React from 'react';
import WarningIcon from '@material-ui/icons/Warning';
import {
  Box, Popover
} from '@material-ui/core';

const AssessmentIcon = ({ assessment }: any) => {
  const formatAssessment: any = {
    WARNING: <WarningIcon />,
  };
  return <Box>
    {formatAssessment[assessment]}
  </Box>;
};

export default AssessmentIcon;
