import { ButtonUI } from '@ep/shopee/src/components/common/button';
import { MultiSelectUI } from '@ep/shopee/src/components/common/multi-select';
import {
  Box,
  Grid,
  InputAdornment,
  OutlinedInput,
  withStyles,
} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import React, { useState } from 'react';
import styled from 'styled-components';

export const ShopFilters = (props: any) => {
  const {
    countries,
    channels,
    selectedCountries,
    selectedChannels,
    onChangeCountries,
    onChangeChannels,
    onChangeSearchText,
    onSubmitChange,
  } = props;
  const [searchText, setSearchText] = useState('');

  return (
    <Box my={2}>
      <Grid container justify="flex-start" alignItems="center">
        <Grid item>
          <MultiSelectUI
            prefix="Channel"
            suffix="channels"
            items={channels}
            selectedItems={selectedChannels}
            onSaveChange={onChangeChannels}
          />
        </Grid>
        <Grid item>
          <MultiSelectUI
            prefix="Country"
            suffix="countries"
            items={countries}
            selectedItems={selectedCountries}
            onSaveChange={onChangeCountries}
          />
        </Grid>
        <Grid item>
          <Divider />
          <ButtonUI
            size={'small'}
            label="Apply"
            onClick={onSubmitChange}
          />
        </Grid>
        <Grid item xs={12}>
          <SearchBox
            id="campaign-searchbox"
            placeholder="Search"
            value={searchText}
            onChange={(event: any) =>
              setSearchText(event.target.value)
            }
            onKeyUp={(event: any) => {
              if (event.key === 'Enter') {
                onChangeSearchText(event.target.value);
              }
            }}
            startAdornment={
              <InputAdornment position="start">
                <SearchIcon fontSize="small" />
              </InputAdornment>
            }
            labelWidth={0}
            autoComplete="off"
          />
        </Grid>
      </Grid>
    </Box>
  );
};

const SearchBox = withStyles({
  root: {
    marginTop: 40,
    marginBottom: 32,
    marginRight: 5,
    width: 320,
    '&:hover .MuiOutlinedInput-notchedOutline': {
      borderColor: '#C2C7CB',
    },
  },
  notchedOutline: {
    border: '2px solid #C2C7CB',
  },
  input: {
    padding: 8,
    fontSize: 14,
  },
})(OutlinedInput);

const Divider = styled.div`
  width: 1px;
  background: #ccc;
  height: 30px;
  float: left;
  margin: 0 8px;
`;
