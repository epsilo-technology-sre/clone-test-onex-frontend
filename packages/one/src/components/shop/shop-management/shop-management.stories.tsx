import React from 'react';
import { ShopManagement } from './shop-management';
import { ThemeProvider } from '@material-ui/core/styles';
import { Provider } from 'react-redux';
import ShopeeTheme from '@ep/shopee/src/shopee-theme';
import { ToastContainer } from 'react-toastify';
import { store } from '@ep/shopee/src/redux/store';
import 'react-toastify/dist/ReactToastify.css';

export default {
  title: 'One/Shop Management',
};

const status = ['active', 'error', 'warning', 'syncing'];

const shops = Array.from({ length: 10 }, (_, key) => {
  return {
    key,
    shopName: 'Shop name',
    status: status[Math.floor(Math.random() * Math.floor(4))],
    channel: 'Channel name',
    feature: 6,
    user: Math.floor(Math.random() * Math.floor(100)),
    created: '11/11/2020 10:45 am',
    updated: '11/11/2020 10:45 am',
    userCreate: 'User name',
    userUpdate: 'User name',
  };
});

export const Primary = () => (
  <Provider store={store}>
    <ThemeProvider theme={ShopeeTheme}>
      <ShopManagement shops={shops} />
      <ToastContainer closeOnClick={false} hideProgressBar={true} />
    </ThemeProvider>
  </Provider>
);
