import {
  LinkButtonCell,
  ShopStatus,
  ShopStatusCell,
} from '@ep/shopee/src/components/common/table-cell';

export const SHOP_COLUMNS = [
  {
    Header: '',
    id: 'countryCode',
    accessor: 'countryCode',
    disableSortBy: true,
    width: 50,
  },
  {
    Header: 'Shop',
    id: 'shopName',
    accessor: 'shopName',
    disableSortBy: true,
    width: 250,
  },
  {
    Header: 'Status',
    id: 'status',
    disableSortBy: true,
    width: 60,
    accessor: (row: any) => {
      const status = row.shops_states;
      const shopState = {
        Init: 'Preparing to sync shop data',
        Syncing:
          'Syncing data from ChannelName This may take a while',
        Pulled: 'Pulling data from ChannelName This may take a while',
        Good: 'All is well. The shop is ready to use',
      };

      return { type: status, children: shopState };
    },
    Cell: ShopStatusCell,
  },
  {
    Header: 'Channel',
    id: 'channel',
    accessor: 'channel',
    disableSortBy: true,
    // width: 168,
  },
  {
    Header: 'Feature',
    id: 'feature',
    accessor: 'feature',
    disableSortBy: true,
    // width: 100,
  },
  {
    Header: 'User',
    id: 'user',
    accessor: 'user',
    disableSortBy: true,
    // width: 100,
  },
  {
    Header: 'Created',
    id: 'created',
    accessor: (row: any) => ({
      userName: row.userCreate,
      timeLine: row.created,
    }),
    disableSortBy: true,
    // width: 168,
    Cell: ShopStatus,
  },
  {
    Header: 'Last updated',
    id: 'updated',
    accessor: (row: any) => ({
      userName: row.userUpdate,
      timeLine: row.updated,
    }),
    disableSortBy: true,
    // width: 168,
    Cell: ShopStatus,
  },
  {
    Header: '',
    id: 'delete',
    accessor: (row: any) => ({
      ...row,
      label: 'View details',
    }),
    disableSortBy: true,
    Cell: LinkButtonCell,
    // width: 120,
  },
];
