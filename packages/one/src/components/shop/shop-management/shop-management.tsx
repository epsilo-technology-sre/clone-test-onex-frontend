import { ButtonUI } from '@ep/shopee/src/components/common/button';
import { TableUINoCheckbox } from '@ep/shopee/src/components/common/table';
import { Grid } from '@material-ui/core';
import React, { useState } from 'react';
import styled from 'styled-components';
import { SHOP_COLUMNS } from './columns';
import { ShopFilters } from './shop-filters';

export interface TooltipUIProps {
  children: JSX.Element;
  title: string;
  placement?: string;
}

type TableType = React.ComponentProps<typeof TableUINoCheckbox>;

type ShopManagementProps = {
  shops: any[];
  getShopId: (row: any) => any;
  countries: any[];
  channels: any[];
  selectedCountries: any[];
  selectedChannels: any[];
  onChangeCountries: Function;
  onChangeChannels: Function;
  onSubmitChange?: Function;
  onChangeSearchText: Function;
  onAddNewShop: Function;
} & Omit<TableType, 'rows' | 'columns' | 'getRowId'>;

export function ShopManagement(props: ShopManagementProps) {
  // const dispatch = useDispatch();
  const [search, setSearch] = useState('');

  const { placement, title, shops, onAddNewShop } = props;

  console.info('resultTotal', props.resultTotal);
  return (
    <ShopContainer>
      <Grid
        container
        justify="space-between"
        alignItems="center"
        style={{ marginBottom: 30 }}
      >
        <Title>Shop management</Title>
        <ButtonUI onClick={onAddNewShop} label={'Add new shop'} />
      </Grid>
      <ShopFilters
        countries={props.countries}
        channels={props.channels}
        selectedCountries={props.selectedCountries}
        selectedChannels={props.selectedChannels}
        onChangeCountries={props.onChangeCountries}
        onChangeChannels={props.onChangeChannels}
        onSubmitChange={props.onSubmitChange}
        onChangeSearchText={(e: any) => props.onChangeSearchText(e)}
      />
      <TableUINoCheckbox
        className="add-rule"
        columns={SHOP_COLUMNS}
        rows={shops}
        noData="No data"
        onSort={() => {}}
        resultTotal={props.resultTotal}
        page={props.page}
        pageSize={props.pageSize}
        getRowId={props.getShopId}
        onChangePage={props.onChangePage}
        onChangePageSize={props.onChangePageSize}
      />
    </ShopContainer>
  );
}

const Title = styled.h1`
  font-style: normal;
  font-weight: 600;
  font-size: 29px;
  line-height: 32px;
  color: #253746;
  margin: 0;
`;

const ShopContainer = styled.div`
  background: #fff;
`;
