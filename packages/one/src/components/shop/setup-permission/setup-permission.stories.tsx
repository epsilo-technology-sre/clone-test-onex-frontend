import React, { useState, useRef } from 'react';
import { SetupPermission } from './setup-permission';

export default {
  title: 'One/Shop management details',
};

const rows = [
  // { permission: 'Shopee - Keyword Bidding', permissionCode: "ALL_KB" },
  {
    permission: 'Shopee - Keyword Bidding',
    permissionCode: 'M_SHP_KB',
    isHead: true,
    permissionChildCode: [
      'M_SHP_KB_VIEW_CPK',
      'M_SHP_KB_EDIT_CPK',
      'M_SHP_KB_EDIT_RULE',
      'M_SHP_KB_MASSUPLOAD',
      'M_SHP_KB_EDIT_DASHBOARD',
      'M_SHP_KB_DOWNLOAD',
    ],
  },
  {
    permission: 'View Campaign, Products, Keywords',
    permissionCode: 'M_SHP_KB_VIEW_CPK',
  },
  {
    permission: 'Edit/Create Campaign, Products, Keywords',
    permissionCode: 'M_SHP_KB_EDIT_CPK',
  },
  {
    permission: 'Add/Create Rules to Products, Keywords.',
    permissionCode: 'M_SHP_KB_EDIT_RULE',
  },
  { permission: 'MassUpload', permissionCode: 'M_SHP_KB_MASSUPLOAD' },
  {
    permission: 'Create, Edit, Remove monthly target of Dashboard',
    permissionCode: 'M_SHP_KB_EDIT_DASHBOARD',
  },
  {
    permission: 'Download selected Objects',
    permissionCode: 'M_SHP_KB_DOWNLOAD',
  },

  // { permission: 'Shopee - Shop Ads', permissionCode: "ALL_SA" },
  {
    permission: 'Shopee - Shop Ads',
    permissionCode: 'M_SHP_SA',
    isHead: true,
    permissionChildCode: [
      'M_SHP_SA_VIEW_SAKWD',
      'M_SHP_SA_DOWNLOAD',
      'M_SHP_SA_EDIT_SAKWD',
      'M_SHP_SA_EDIT_DASHBOARD',
    ],
  },
  {
    permission: 'View Shop Ads, Keywords',
    permissionCode: 'M_SHP_SA_VIEW_SAKWD',
  },
  {
    permission: 'Download selected Objects',
    permissionCode: 'M_SHP_SA_DOWNLOAD',
  },
  {
    permission: 'Edit/Create Shop Ads, Keywords',
    permissionCode: 'M_SHP_SA_EDIT_SAKWD',
  },
  {
    permission: 'Create, Edit, Remove monthly target of Dashboard',
    permissionCode: 'M_SHP_SA_EDIT_DASHBOARD',
  },

  // { permission: 'Lazada - Sponsored Search', permissionCode: "ALL_SS" },
  // {
  //   permission: 'Lazada - Sponsored Search',
  //   permissionCode: [
  //     'M_LZD_SS_VIEW_CPK',
  //     'M_LZD_SS_DOWNLOAD',
  //     'M_LZD_SS_EDIT_CPK',
  //     'M_LZD_SS_EDIT_RULE',
  //     'M_LZD_SS_MASSUPLOAD',
  //     'M_LZD_SS_EDIT_DASHBOARD',
  //   ],
  // },
  // {
  //   permission: 'View Campaign, Products, Keywords',
  //   permissionCode: 'M_LZD_SS_VIEW_CPK',
  // },
  // {
  //   permission: 'Download selected Objects',
  //   permissionCode: 'M_LZD_SS_DOWNLOAD',
  // },
  // {
  //   permission: 'Edit/Create Campaign, Products, Keywords',
  //   permissionCode: 'M_LZD_SS_EDIT_CPK',
  // },
  // {
  //   permission: 'Add/Create Rules to Products, Keywords.',
  //   permissionCode: 'M_LZD_SS_EDIT_RULE',
  // },
  // { permission: 'MassUpload', permissionCode: 'M_LZD_SS_MASSUPLOAD' },
  // {
  //   permission: 'Create, Edit, Remove monthly target of Dashboard',
  //   permissionCode: 'M_LZD_SS_EDIT_DASHBOARD',
  // },

  // // { permission: 'Product', permissionCode: "ALL_PRD" },
  // { permission: 'Product', permissionCode: ['O_PRD_VIEW'] },
  // { permission: 'View Product List', permissionCode: 'O_PRD_VIEW' },

  // // { permission: 'Pricing', permissionCode: "ALL_PRICE" },
  // {
  //   permission: 'Pricing',
  //   permissionCode: [
  //     'O_PRICE_VIEW_PRICE',
  //     'O_PRICE_EDIT_PRICE',
  //     'O_PRICE_VIEW_PROMOTION_TRACKER&DETAIL',
  //     'O_PRICE_EDIT_PROMOTION',
  //     'O_PRICE_APPROVE_PROMOTION',
  //   ],
  // },
  // {
  //   permission: 'View Price Tracker',
  //   permissionCode: 'O_PRICE_VIEW_PRICE',
  // },
  // {
  //   permission: 'Edit Price Tracker',
  //   permissionCode: 'O_PRICE_EDIT_PRICE',
  // },
  // {
  //   permission: 'View Promotion Tracker, Promotion Details',
  //   permissionCode: 'O_PRICE_VIEW_PROMOTION_TRACKER&DETAIL',
  // },
  // {
  //   permission: 'Edit Promotion',
  //   permissionCode: 'O_PRICE_EDIT_PROMOTION',
  // },
  // {
  //   permission: 'Approve/ Reject Promotion',
  //   permissionCode: 'O_PRICE_APPROVE_PROMOTION',
  // },
];

export const setupPermission = () => {
  const [selectedPermissions, setSelectedPermissions] = useState([]);
  const [permissionByUserId, setPermissionByUserId] = useState({
    1: {
      M_SHP_KB_VIEW_CPK: true,
      M_SHP_KB_EDIT_CPK: true,
    },
  });

  const permissions = rows.map((i) => ({
    permissionCode: i.permissionCode,
    name: i.permission,
    isHead: i.isHead,
  }));

  const handleSubmit = () => {
    console.info(permissionByUserId);
  };
  const handleBackToPreviousStep = () =>
    console.log('handleBackToPreviousStep', { abc: 1111 });
  console.log('selectedPermissions', { selectedPermissions });

  return (
    <SetupPermission
      permissions={permissions}
      users={[
        { userId: 1, name: 'User 1' },
        { userId: 2, name: 'User 3' },
      ]}
      onSelectItem={(item) => {
        setPermissionByUserId((p) => {
          if (p[item.userId]) {
            return {
              ...p,
              [item.userId]: {
                ...p[item.userId],
                [item.permission.permissionCode]: item.checked,
              },
            };
          } else {
            return {
              ...p,
              [item.userId]: {
                [item.permission.permissionCode]: item.checked,
              },
            };
          }
        });
      }}
      permissionsByUserId={permissionByUserId}
      onSubmit={handleSubmit}
      onBackToPreviousStep={handleBackToPreviousStep}
    />
  );
};
