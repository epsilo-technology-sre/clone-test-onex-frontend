import React from 'react';
import { Paper, Grid, Box } from '@material-ui/core';
import styled from 'styled-components';
import { ButtonUI } from '@ep/shopee/src/components/common/button';
import { TableUIPermission } from '@ep/shopee/src/components/common/table';
import { PermissionCell } from '@ep/shopee/src/components/common/table-cell';
import { get } from 'lodash';

type SetupPermissionType = {
  permissions: {
    permissionCode: string;
    name: string;
  }[];
  users: {
    userId: number;
    name: string;
  }[];
  permissionsByUserId: {
    [userId: number]: {
      [code: string]: boolean;
    };
  };

  onSelectItem: Function;
  onSubmit: Function;
  onBackToPreviousStep: Function;
};

export function SetupPermission(props: SetupPermissionType) {
  const {
    permissions,
    onSubmit,
    onBackToPreviousStep,
    users,
    permissionsByUserId,
    onSelectItem,
  } = props;

  const rows = React.useMemo(() => {
    return permissions.map((i) => ({ ...i, permissionsByUserId }));
  }, [permissions, permissionsByUserId]);

  console.info({ rows });

  const columns = React.useMemo(() => {
    return [
      {
        Header: 'Permission',
        id: 'permission',
        accessor: (row: any) => ({
          name: row.name,
          permissionCode: row.permissionCode,
          isHead: row.isHead,
        }),
        disableSortBy: true,
        width: 500,
        sticky: 'left',
        Cell: ({ value }: any) => value.name,
      },
      ...users.map((user, key) => {
        let userId = user.userId;
        return {
          Header: user.name,
          id: userId,
          accessor: (row: any) => {
            let checked = get(
              row,
              ['permissionsByUserId', userId, row.permissionCode],
              0,
            );
            return {
              checked: checked,
              indeterminate: row.isHead && checked,
              userId,
              permissionCode: row.permissionCode,
            };
          },
          disableSortBy: true,
          Cell: PermissionCell,
          selectedItem: ({
            checked,
            value,
            userId,
            row,
          }: {
            checked: 0 | 1 | 2;
            value: any;
            userId: any;
            row: any;
          }) => {
            onSelectItem({ checked, permission: value, userId, row });
          },
        };
      }),
    ];
  }, []);

  console.info({ columns });

  return (
    <Paper elevation={0}>
      <Title>Setup Permission</Title>
      <TableUIPermission
        className={'table-permission'}
        columns={columns}
        rows={rows}
        onSort={() => {}}
      />
      <Box mt={2}>
        <Grid
          container
          alignItems={'center'}
          justify={'space-between'}
        >
          <ButtonUI
            colorButton={'#F6F7F8'}
            label={'Back to previous step'}
            onClick={onBackToPreviousStep}
          />
          <ButtonUI label={'Done'} onClick={onSubmit} />
        </Grid>
      </Box>
    </Paper>
  );
}

const Title = styled.h1`
  font-style: normal;
  font-weight: 600;
  font-size: 29px;
  line-height: 32px;
  color: #253746;
  margin-bottom: 54px;
`;
