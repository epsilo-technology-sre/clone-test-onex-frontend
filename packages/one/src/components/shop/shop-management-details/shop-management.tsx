import React, { useState } from 'react';
import { TableUINoCheckbox } from '@ep/shopee/src/components/common/table';
import { initColumns } from './columns';
import {
  Grid,
  OutlinedInput,
  withStyles,
  InputAdornment,
} from '@material-ui/core';
import styled from 'styled-components';
import { ButtonUI } from '@ep/shopee/src/components/common/button';
import { TooltipUI } from '@ep/shopee/src/components/common/tooltip';
import SearchIcon from '@material-ui/icons/Search';
import { UserDetails } from '../user-details';

export interface ShopManagementProps {
  shopInfo: any;
  permissions: any;
  onChangeSearchText?: any;
  onAddNewMember: Function;
  onSubmit: Function;
  onRemoveAllocate: Function;
}

export function ShopManagement(props: ShopManagementProps) {

  const {
    shopInfo,
    shopInfo: { features },
    onChangeSearchText,
    onAddNewMember,
    permissions,
    onSubmit,
  } = props;
  const featureName = features
    .map((feature: any) => feature.featureName)
    .join(', ');


  const [userDetail, setUserDetail] = React.useState({
    open: false,
    features: [],
    userInfo: {
      email: '',
      firstName: '',
      lastName: '',
      userId: ''
    }
  });

  const handleClose = () => {
    setUserDetail({
      ...userDetail,
      open: false,
    });
  };


  const headers = React.useMemo(
    () =>
      initColumns({
        onAddKeyword: (value: any) => {
          setUserDetail({
            userInfo: {
              email: value.email,
              firstName: value.firstName,
              lastName: value.lastName,
              userId: value.key,
            },
            features: value.features,
            open: true,
          })
        },
        onDelete: (value) => props.onRemoveAllocate(value.key),
      }),
    [],
  );

  return (
    <ShopContainer>
      <Grid container justify="space-between" alignItems="center">
        <Title>Shop details</Title>
        <ButtonUI onClick={onAddNewMember} label={'Add new member'} />
      </Grid>
      <TitleSection>Business information</TitleSection>
      <Grid container spacing={2}>
        <Grid item xs={3}>
          <ManagementItemHeader>Shop name</ManagementItemHeader>
          <ManagementItemContent>
            {shopInfo.shopName}
          </ManagementItemContent>
        </Grid>
        <Grid item xs={3}>
          <ManagementItemHeader>Country</ManagementItemHeader>
          <ManagementItemContent>
            {shopInfo.country}
          </ManagementItemContent>
        </Grid>
        <Grid item xs={3}>
          <ManagementItemHeader>Channel</ManagementItemHeader>
          <ManagementItemContent>
            {shopInfo.channel}
          </ManagementItemContent>
        </Grid>
        <Grid item xs={3}>
          <ManagementItemHeader>Created by</ManagementItemHeader>
          <ManagementItemContent>
            {shopInfo.createdBy}
          </ManagementItemContent>
        </Grid>
        <Grid item xs={3}>
          <ManagementItemHeader>Feature</ManagementItemHeader>
          <TooltipUI title={featureName}>
            <ManagementItemContent>
              {featureName}
            </ManagementItemContent>
          </TooltipUI>
        </Grid>
        <Grid item xs={3}>
          <ManagementItemHeader>EID</ManagementItemHeader>
          <ManagementItemContent>
            {shopInfo.eid}
          </ManagementItemContent>
        </Grid>
        <Grid item xs={3}>
          <ManagementItemHeader>
            Subcription code
          </ManagementItemHeader>
          <ManagementItemContent>
            {shopInfo.subcriptionCode}
          </ManagementItemContent>
        </Grid>
        <Grid item xs={3}>
          <ManagementItemHeader>Updated by</ManagementItemHeader>
          <ManagementItemContent>
            {shopInfo.updatedBy}
          </ManagementItemContent>
        </Grid>
      </Grid>
      <TitleSection>Membership</TitleSection>
      <SearchBox
        id="campaign-searchbox"
        placeholder="Search"
        onKeyUp={(event: any) => {
          if (event.key === 'Enter') {
            onChangeSearchText(event.target.value);
          }
        }}
        startAdornment={
          <InputAdornment position="start">
            <SearchIcon fontSize="small" />
          </InputAdornment>
        }
        labelWidth={0}
        autoComplete="off"
      />
      <TableUINoCheckbox
        className="add-rule"
        columns={headers}
        rows={shopInfo.users}
        noData="No data"
        onSort={() => {}}
        resultTotal={shopInfo.users.length}
        page={10}
        pageSize={10}
      />
      <UserDetails onSubmit={onSubmit} permissions={permissions} open={userDetail.open} handleClose={handleClose} userInfo={userDetail.userInfo} permissionsList={userDetail.features} />
    </ShopContainer>
  );
}

const Title = styled.h1`
  font-style: normal;
  font-weight: 600;
  font-size: 29px;
  line-height: 32px;
  color: #253746;
  margin: 0;
`;

const TitleSection = styled.p`
  font-weight: 600;
  font-size: 16px;
  line-height: 20px;
  color: #253746;
`;

const ManagementItemHeader = styled.div`
  font-size: 12px;
  line-height: 16px;
  color: #596772;
`;

const ManagementItemContent = styled.div`
  font-weight: 600;
  font-size: 14px;
  line-height: 20px;
  color: #253746;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const ShopContainer = styled.div`
  background: #fff;
`;

const SearchBox = withStyles({
  root: {
    marginBottom: 32,
    marginRight: 5,
    width: 320,
    '&:hover .MuiOutlinedInput-notchedOutline': {
      borderColor: '#C2C7CB',
    },
  },
  notchedOutline: {
    border: '2px solid #C2C7CB',
  },
  input: {
    padding: 8,
    fontSize: 14,
  },
})(OutlinedInput);
