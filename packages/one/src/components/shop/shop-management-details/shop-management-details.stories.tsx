import React from 'react';
import { ShopManagement } from './shop-management';
import { ThemeProvider } from '@material-ui/core/styles';
import { Provider } from 'react-redux';
import ShopeeTheme from '@ep/shopee/src/shopee-theme';
import { ToastContainer } from 'react-toastify';
import { store } from '@ep/shopee/src/redux/store';
import 'react-toastify/dist/ReactToastify.css';


const features = ['Keyword bidding', 'Shop ads', 'Targetting Ads', 'Sponsored Search']

const shopInfo = {
  shopName: 'Shop name A',
  country: 'Vietnam',
  channel: 'Shopee',
  createdBy: 'User name - 11/11/2020 • 10:45 pm',
  features: Array.from({ length: 4 }, (_, key) =>
    ({
      key,
      featureName: features[Math.floor(Math.random() * Math.floor(4))],
      featureCode: Math.floor(Math.random() * Math.floor(4))
    })
  ),
  users: Array.from({ length: 10 }, (_, key) =>
    ({
      key,
      firstName: `User`,
      lastName: `Name ${key}`,
      email: `email${key}@epsilo.io`,
      userId: key * Math.floor(Math.random() * Math.floor(4)),
      features: Array.from({ length: 4 }, (_, key) =>
        ({
          key,
          featureName: features[Math.floor(Math.random() * Math.floor(4))],
          featureCode: Math.floor(Math.random() * Math.floor(4))
        })
      )
    })
  ),
  eid: '1',
  subcriptionCode: '1234',
  updatedBy: 'User name - 11/12/2020 • 10:45 pm',
};

export default {
  title: 'One/Shop management details',
};

export const Primary = () => <Provider store={store}>
  <ThemeProvider theme={ShopeeTheme}>
    <ShopManagement shopInfo={shopInfo} />
    <ToastContainer closeOnClick={false} hideProgressBar={true} />
  </ThemeProvider>
</Provider>;
