import React from 'react';
import {
  AddToBucketCell,
  DropdownCell,
  InStockCell, PercentCell, ProductBudgetCell,
  ShopDetailsActionCell,
  SubjectCell,
} from '@ep/shopee/src/components/common/table-cell';

export const initColumns = (props: any) => {
  const { onAddKeyword, onDelete } = props;
  return [
    {
      Header: 'First name',
      id: 'firstName',
      accessor: 'firstName',
      disableSortBy: true,
      width: 280,
    },
    {
      Header: 'Last name',
      id: 'lastName',
      accessor: 'lastName',
      disableSortBy: true,
      width: 280,
    },
    {
      Header: 'Email',
      id: 'email',
      accessor: 'email',
      disableSortBy: true,
      width: 280,
    },
    {
      Header: 'Feature',
      id: 'feature',
      accessor: (row: any) => row,
      disableSortBy: true,
      Cell: DropdownCell,
      width: 168,
    },
    {
      Header: '',
      id: 'delete',
      accessor: (row: any) => ({ product: row, onAddKeyword, onDelete }),
      disableSortBy: true,
      Cell: ShopDetailsActionCell,
      width: 200,
    },
  ];
};
