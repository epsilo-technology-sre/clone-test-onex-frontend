import React from 'react';
import { UserDetails } from './user-details';
import Button from '@material-ui/core/Button';
import ShopeeTheme from '@ep/shopee/src/shopee-theme';
import { ThemeProvider } from '@material-ui/core/styles';
import { Provider } from 'react-redux';
import { store } from '@ep/shopee/src/redux/store';

export default {
  title: 'One/Shop management details',
};

const permissions = [
  {
    label: 'Shopee - Keyword bidding',
    roleName: 'Admin',
    permissions: [
      { permissionName: 'View keywords', permissionsEid: 'shopee-view', isSelected: true },
      { permissionName: 'Download selected Objects', permissionsEid: 'shopee-download', isSelected: false },
      { permissionName: 'Edit/Create Shop keywords', permissionsEid: 'shopee-edit', isSelected: true },
      { permissionName: 'Create, edit, remove', permissionsEid: 'shopee-create', isSelected: false },
    ],
  },
  {
    label: 'Shopee - Shop ads',
    roleName: 'Admin',
    permissions: [
      { permissionName: 'View keywords', permissionsEid: 'shopee-view-shop', isSelected: true },
      { permissionName: 'Edit/Create Shop keywords', permissionsEid: 'shopee-edit-shop', isSelected: true },
      { permissionName: 'Create, edit, remove', permissionsEid: 'shopee-create-shop', isSelected: false },
    ],
  },
  {
    label: 'Lazada',
    roleName: 'Member',
    permissions: [
      { permissionName: 'View keywords', permissionsEid: 'lazada-view', isSelected: true },
    ],
  },
];

export const userDetail = () => {
  const [open, setOpen] = React.useState(true);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };


  return <Provider store={store}>
    <ThemeProvider theme={ShopeeTheme}>
      <UserDetails open={open} handleClose={handleClose} permissionsList={permissions} />
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        Open dialog
      </Button>
    </ThemeProvider>
  </Provider>;
};
