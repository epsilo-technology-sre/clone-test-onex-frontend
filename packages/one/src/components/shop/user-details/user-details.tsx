import React from 'react';
import {
  createStyles,
  makeStyles,
  Theme,
  WithStyles,
  withStyles,
} from '@material-ui/core/styles';
import {
  Dialog,
  Grid,
  IconButton,
  Typography,
} from '@material-ui/core';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import CloseIcon from '@material-ui/icons/Close';
import { ButtonUI } from '@ep/shopee/src/components/common/button';
import { COLORS } from '@ep/shopee/src/constants';
import { UserDetailsPermission } from './user-details-permission';
import flatten from 'lodash/flatten';

export interface UserDetailsProps {
  open: boolean;
  handleClose: any;
  permissionsList: any;
  permissions: any;
  userInfo: any;
  onSubmit: Function;
}

export function UserDetails(props: UserDetailsProps) {
  const classes = makeStyles(styles)();
  const ref = React.useRef({
    permission: {},
  });

  const {
    open,
    onSubmit,
    handleClose,
    permissionsList,
    userInfo,
    permissions,
  } = props;

  const handleAddPermission = (userPermission) => {
    ref.current.permission = {
      ...ref.current.permission,
      ...userPermission,
    };
  };

  return (
    <div>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle
          id="customized-dialog-title"
          onClose={handleClose}
        >
          User details
        </DialogTitle>
        <DialogContent>
          <Grid container>
            <Grid item xs={5}>
              <Typography
                component={'span'}
                className={classes.label}
              >
                First name:
              </Typography>
              <Typography
                component={'span'}
                className={classes.content}
              >
                {userInfo.firstName}
              </Typography>
            </Grid>
            <Grid item xs={7}>
              <Typography
                component={'span'}
                className={classes.label}
              >
                Last name:
              </Typography>
              <Typography
                component={'span'}
                className={classes.content}
              >
                {userInfo.lastName}
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography
                component={'span'}
                className={classes.label}
              >
                Email:
              </Typography>
              <Typography
                component={'span'}
                className={classes.content}
              >
                {userInfo.email}
              </Typography>
            </Grid>
          </Grid>
          {permissions.map((item: any) => {
            return (
              <UserDetailsPermission
                onAddPermission={handleAddPermission}
                permissionsList={permissionsList}
                permissions={item}
              />
            );
          })}
        </DialogContent>
        <DialogActions>
          <ButtonUI
            size="small"
            label="Cancel"
            colorButton={COLORS.COMMON.GRAY}
            onClick={handleClose}
          />
          <ButtonUI
            size="small"
            label="Save changes"
            onClick={() => {
              const { permission } = ref.current;
              const postPermission = flatten(
                Object.keys(permission).map((key) =>
                  permission[key].map((item) => item.permissionCode),
                ),
              );
              const uniquePostPermission = [
                ...new Set(postPermission),
              ];
              onSubmit({ [userInfo.userId]: uniquePostPermission });
              handleClose();
            }}
          />
        </DialogActions>
      </Dialog>
    </div>
  );
}

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
    modalTitle: {
      fontWeight: 500,
      fontSize: 20,
      lineHeight: '24px',
      color: '#253746',
    },
    label: {
      fontSize: 14,
      lineHeight: '20px',
      color: '#515F6B',
    },
    content: {
      fontWeight: 600,
      color: '#253746',
      fontSize: 14,
      lineHeight: '20px',
      marginLeft: 4,
    },
    wrapPermission: {
      padding: 16,
      border: '2px solid #ccc',
      marginTop: 16,
      borderRadius: 4,
    },
    labelPermission: {
      fontWeight: 600,
      fontSize: 14,
      lineHeight: '16px',
      color: '#253746',
    },
    rolePermission: {
      fontWeight: 600,
      fontSize: 12,
      lineHeight: '16px',
      color: '#253746',
      textTransform: 'uppercase',
      background: '#E4E7E9',
      borderRadius: 4,
      padding: '4px 6px',
      display: 'inline-block',
      marginTop: 4,
    },
  });

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle
      disableTypography
      className={classes.root}
      {...other}
    >
      <Typography variant="h6" className={classes.modalTitle}>
        {children}
      </Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    minWidth: 480,
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);
