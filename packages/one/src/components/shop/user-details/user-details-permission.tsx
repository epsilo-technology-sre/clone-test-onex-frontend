import React from 'react';
import {
  createStyles,
  Theme,
  makeStyles,
} from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import { TableUICheckboxRight } from '@ep/shopee/src/components/common/table';
import get from 'lodash/get';

export interface UserDetailsPermission {
  permissionsList: any;
  permissions: any;
  onAddPermission: Function;
}

export function UserDetailsPermission(props: UserDetailsPermission) {
  const classes = makeStyles(styles)();

  const { permissionsList, permissions } = props;
  const permission = [];
  permissionsList.map((item) =>
    (item.permission || []).map((item) =>
      permission.push({
        permissionCode: item.permission_code,
        permissionId: item.permission_id,
        permissionName: item.permission_name,
      }),
    ),
  );

  const [
    selectedPermissions,
    setSelectedPermissions,
  ] = React.useState([]);

  React.useEffect(() => {
    setSelectedPermissions(permission);
  }, []);

  const updateSelectedPermissions = (payload: any) => {
    setSelectedPermissions(payload);
    props.onAddPermission({ [permissions.permissionCode]: payload });
  };

  const handleSelect = (rows: any) => updateSelectedPermissions(rows);

  const handleSelectItem = React.useMemo(() => {
    return (item: any, checked: boolean) => {
      if (checked) {
        updateSelectedPermissions(
          (selectedPermissions || []).concat(item),
        );
      } else {
        updateSelectedPermissions(
          (selectedPermissions || []).filter(
            (c: any) => c.permissionCode !== item.permissionCode,
          ),
        );
      }
    };
  }, [selectedPermissions]);

  const handleSelectAll = React.useCallback((checked) => {
    if (checked && selectedPermissions.length === 0) {
      updateSelectedPermissions(
        (selectedPermissions || []).concat(permissions.permission),
      );
    } else {
      updateSelectedPermissions([]);
    }
  }, []);

  const getRowId = React.useCallback(
    (row) => String(row.permissionCode),
    [],
  );

  const selectedIds = React.useMemo(() => {
    return (selectedPermissions || []).map((i) =>
      String(i.permissionCode),
    );
  }, [selectedPermissions]);

  const userRole = permissionsList.filter(
    (item) => item.featureCode === permissions.permissionCode,
  );

  return (
    <div
      className={classes.wrapPermission}
      key={permissions.permissionName}
    >
      <Typography className={classes.labelPermission}>
        {permissions.permissionName}
      </Typography>
      <Typography
        component={'span'}
        className={classes.rolePermission}
      >
        {get(userRole, '[0].userRole', 'Member')}
      </Typography>
      <TableUICheckboxRight
        key={selectedPermissions.length}
        className={'user-table'}
        columns={[
          {
            Header: 'Permission',
            id: 'permissionName',
            accessor: 'permissionName',
            sticky: 'left',
            width: 370,
            disableSortBy: true,
          },
        ]}
        rows={permissions.permission.map(p => ({...p, _isDisabled:  get(userRole, '[0].userRole', 'Member') === 'admin'}))}
        onSort={() => {}}
        selectedIds={selectedIds}
        getRowId={getRowId}
        onSelect={handleSelect}
        onSelectItem={handleSelectItem}
        onSelectAll={handleSelectAll}
        noData={'No data'}
      />
    </div>
  );
}

const styles = (theme: Theme) =>
  createStyles({
    wrapPermission: {
      padding: 16,
      border: '2px solid #ccc',
      marginTop: 16,
      borderRadius: 4,
    },
    labelPermission: {
      fontWeight: 600,
      fontSize: 14,
      lineHeight: '16px',
      color: '#253746',
    },
    rolePermission: {
      fontWeight: 600,
      fontSize: 12,
      lineHeight: '16px',
      color: '#253746',
      textTransform: 'uppercase',
      background: '#E4E7E9',
      borderRadius: 4,
      padding: '4px 6px',
      display: 'inline-block',
      marginTop: 4,
    },
  });
