import React from 'react'
import { DropdownPermission } from './dropdown-permission'

export default {
  title: 'One/Shop management details'
}

const permissions = [
  {
    label: 'Admin',
    permissions: [
      {
        label: 'Shopee - Keyword bidding',
        permissions: ['View keywords', 'Download selected Objects', 'Edit/Create Shop keywords', 'Create, edit, remove'],
      },
      {
        label: 'Shopee - Shop ads',
        permissions: ['View Shop Ads, keywords', 'Download selected Objects', 'Edit/Create Shop Ads, keywords', 'Create, edit, remove monthly target of Dashboard'],
      }
    ]
  },
  {
    label: 'Member',
    permissions: [
      {
        label: 'Lazada',
        permissions: ['View keywords', 'Download selected Objects'],
      }
    ]
  },
]

export const dropdownPermission = () => {
  return <DropdownPermission permissions={permissions} />
}
