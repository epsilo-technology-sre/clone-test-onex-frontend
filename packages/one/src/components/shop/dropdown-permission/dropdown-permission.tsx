import React from 'react';
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  makeStyles,
  Box,
  Popover
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import styled from 'styled-components';
import { ButtonUI } from '../../common/button';
import PopupState, { bindTrigger, bindPopover } from 'material-ui-popup-state';

export interface DropdownPermissionProps {
  permissions?: any,
}

export const DropdownPermission = (props: DropdownPermissionProps) => {
  const { permissions: { features, featuresGroup } } = props;
  const classes = useStyles();

  return <PopupState variant="popover" popupId="demo-popup-popover">
    {(popupState) => (
      <div>
        <ButtonUI
          size="small"
          iconRight={<ExpandMoreIcon />}
          colorButton={'#F6F7F8'}
          label={`${features.length} features`}
          color="primary"
          {...bindTrigger(popupState)}
        />
        <Popover
          {...bindPopover(popupState)}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
        >
          <Box className={classes.popover}>
            {Object.keys(featuresGroup).map((key) => {
              return <>
                <TitleLevel>{key}</TitleLevel>
                {featuresGroup[key].map((item: { featureCode: string, featureName: string, key: string, permission: any[] }) =>
                  <Accordion expanded classes={{ root: classes.accordion }} key={item.featureName}>
                    <AccordionSummary
                      expandIcon={<ExpandMoreIcon />}
                      aria-controls="panel1a-content"
                      id="panel1a-header"
                    >
                      <TitleRole>{item.featureName}</TitleRole>
                    </AccordionSummary>
                    <AccordionDetails>
                      <div>
                        {item.permission.map(p => <ContentRole key={p.permission_name}>{p.permission_name}</ContentRole>)}
                      </div>
                    </AccordionDetails>
                  </Accordion>
                )}
              </>;
            })}
          </Box>
        </Popover>
      </div>
    )}
  </PopupState>;
};

const TitleLevel = styled.div`
  font-weight: bold;
  font-size: 11px;
  line-height: 16px;
  color: #596772;
  text-transform: capitalize;
`;

const TitleRole = styled.div`
  font-weight: 600;
  font-size: 14px;
  line-height: 20px;
  color: #253746;
`;

const ContentRole = styled.div`
  font-size: 14px;
  line-height: 20px;
  color: #253746;
  margin-bottom: 22px;
  padding-left: 40px;
  min-width: 400px;
`;

const useStyles = makeStyles({
  accordion: {
    boxShadow: 'none',
    '&:before': {
      display: 'none',
    },
    '&.Mui-expanded': {
      margin: 0,
    },
    '& .Mui-expanded': {
      marginTop: 0,
      marginBottom: 0,
      minHeight: 'inherit',
    },
    '& .MuiIconButton-edgeEnd': {
      marginRight: -12
    },
    '& .MuiAccordionDetails-root': {
      padding: 0,
    }
  },
  paper: {
    padding: 8,
  },
  popover: {
    width: 400,
    maxHeight: 400,
    padding: '8px 0 8px 8px',
  }
});
