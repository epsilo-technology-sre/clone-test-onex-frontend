import React, { useState } from 'react';
import { AddNewMember } from './add-new-member';
import { useDispatch, useSelector } from 'react-redux';
import get from 'lodash/get';
import { UPDATE_FILTER_COUNTRY } from '@ep/shopee/src/redux/actions';
import { store } from '@ep/shopee/src/redux/store';
import ShopeeTheme from '@ep/shopee/src/shopee-theme';
import { ThemeProvider } from '@material-ui/core/styles';
import { Provider } from 'react-redux';

export default {
  title: 'One/Shop management details',
};

const companies: any = [{"shop_eid":4131,"shop_sid":"7024","shop_name":"\u0e01\u0e32\u0e23\u0e4c\u0e19\u0e34\u0e40\u0e22\u0e48","shop_allowed_pull":1,"shop_allowed_push":1,"channel_code":"LAZADA","channel_name":"Lazada","country_code":"TH","country_name":"Thailand","country_timezone":"Asia\/Bangkok","country_exchange":"THB","country_format_right":2,"features":[{"feature_code":"M_LZD_SS","feature_name":"Lazada - Sponsored Search"}]},{"shop_eid":4127,"shop_sid":"1043314","shop_name":"P&G Official Shop","shop_allowed_pull":1,"shop_allowed_push":1,"channel_code":"LAZADA","channel_name":"Lazada","country_code":"TH","country_name":"Thailand","country_timezone":"Asia\/Bangkok","country_exchange":"THB","country_format_right":2,"features":[{"feature_code":"M_LZD_SS","feature_name":"Lazada - Sponsored Search"}]},{"shop_eid":4125,"shop_sid":"135193","shop_name":"Garnier","shop_allowed_pull":1,"shop_allowed_push":1,"channel_code":"LAZADA","channel_name":"Lazada","country_code":"MY","country_name":"Malaysia","country_timezone":"Asia\/Kuala_Lumpur","country_exchange":"MYR","country_format_right":2,"features":[{"feature_code":"M_LZD_SS","feature_name":"Lazada - Sponsored Search"}]},{"shop_eid":4117,"shop_sid":"941825","shop_name":"Pampers.","shop_allowed_pull":1,"shop_allowed_push":1,"channel_code":"LAZADA","channel_name":"Lazada","country_code":"SG","country_name":"Singapore","country_timezone":"Asia\/Singapore","country_exchange":"SGD","country_format_right":2,"features":[{"feature_code":"M_LZD_SS","feature_name":"Lazada - Sponsored Search"}]},{"shop_eid":4111,"shop_sid":"517974","shop_name":"Ch\u0103m s\u00f3c T\u00f3c P&G","shop_allowed_pull":1,"shop_allowed_push":1,"channel_code":"LAZADA","channel_name":"Lazada","country_code":"VN","country_name":"Vietnam","country_timezone":"Asia\/Ho_Chi_Minh","country_exchange":"VND","country_format_right":0,"features":[{"feature_code":"M_LZD_SS","feature_name":"Lazada - Sponsored Search"}]},{"shop_eid":4110,"shop_sid":"232460059","shop_name":"Chee.anvat","shop_allowed_pull":1,"shop_allowed_push":1,"channel_code":"SHOPEE","channel_name":"Shopee","country_code":"VN","country_name":"Vietnam","country_timezone":"Asia\/Ho_Chi_Minh","country_exchange":"VND","country_format_right":0,"features":[{"feature_code":"M_SHP_KB","feature_name":"Shopee - Keyword Bidding"}]},{"shop_eid":3921,"shop_sid":"265272667","shop_name":"XYZ Shop","shop_allowed_pull":1,"shop_allowed_push":1,"channel_code":"SHOPEE","channel_name":"Shopee","country_code":"VN","country_name":"Vietnam","country_timezone":"Asia\/Ho_Chi_Minh","country_exchange":"VND","country_format_right":0,"features":[{"feature_code":"M_SHP_KB","feature_name":"Shopee - Keyword Bidding"}]},{"shop_eid":3920,"shop_sid":"26234218","shop_name":"Lysol","shop_allowed_pull":1,"shop_allowed_push":1,"channel_code":"SHOPEE","channel_name":"Shopee","country_code":"PH","country_name":"Philippines","country_timezone":"Asia\/Manila","country_exchange":"PHP","country_format_right":2,"features":[{"feature_code":"M_SHP_KB","feature_name":"Shopee - Keyword Bidding"}]},{"shop_eid":3862,"shop_sid":"100513","shop_name":"Lysol","shop_allowed_pull":1,"shop_allowed_push":1,"channel_code":"LAZADA","channel_name":"Lazada","country_code":"PH","country_name":"Philippines","country_timezone":"Asia\/Manila","country_exchange":"PHP","country_format_right":2,"features":[{"feature_code":"M_LZD_SS","feature_name":"Lazada - Sponsored Search"}]},{"shop_eid":3860,"shop_sid":"156421","shop_name":"RB Home","shop_allowed_pull":1,"shop_allowed_push":1,"channel_code":"LAZADA","channel_name":"Lazada","country_code":"MY","country_name":"Malaysia","country_timezone":"Asia\/Kuala_Lumpur","country_exchange":"MYR","country_format_right":2,"features":[{"feature_code":"M_LZD_SS","feature_name":"Lazada - Sponsored Search"}]},{"shop_eid":3820,"shop_sid":"44437739","shop_name":"P&G Official Store","shop_allowed_pull":1,"shop_allowed_push":1,"channel_code":"SHOPEE","channel_name":"Shopee","country_code":"MY","country_name":"Malaysia","country_timezone":"Asia\/Kuala_Lumpur","country_exchange":"MYR","country_format_right":2,"features":[{"feature_code":"M_SHP_KB","feature_name":"Shopee - Keyword Bidding"}]},{"shop_eid":504,"shop_sid":"201399809","shop_name":"Upspring Official Store","shop_allowed_pull":1,"shop_allowed_push":1,"channel_code":"SHOPEE","channel_name":"Shopee","country_code":"SG","country_name":"Singapore","country_timezone":"Asia\/Singapore","country_exchange":"SGD","country_format_right":2,"features":[{"feature_code":"M_SHP_KB","feature_name":"Shopee - Keyword Bidding"}]},{"shop_eid":503,"shop_sid":"201399010","shop_name":"Upspring","shop_allowed_pull":1,"shop_allowed_push":1,"channel_code":"SHOPEE","channel_name":"Shopee","country_code":"MY","country_name":"Malaysia","country_timezone":"Asia\/Kuala_Lumpur","country_exchange":"MYR","country_format_right":2,"features":[{"feature_code":"M_SHP_KB","feature_name":"Shopee - Keyword Bidding"}]},{"shop_eid":405,"shop_sid":"23325172","shop_name":"Enfa Official Shop","shop_allowed_pull":1,"shop_allowed_push":1,"channel_code":"SHOPEE","channel_name":"Shopee","country_code":"TH","country_name":"Thailand","country_timezone":"Asia\/Bangkok","country_exchange":"THB","country_format_right":2,"features":[{"feature_code":"M_SHP_KB","feature_name":"Shopee - Keyword Bidding"}]},{"shop_eid":400,"shop_sid":"11487927","shop_name":"P&G Official Store","shop_allowed_pull":1,"shop_allowed_push":1,"channel_code":"SHOPEE","channel_name":"Shopee","country_code":"ID","country_name":"Indonesia","country_timezone":"Asia\/Jakarta","country_exchange":"IDR","country_format_right":0,"features":[{"feature_code":"M_SHP_KB","feature_name":"Shopee - Keyword Bidding"}]},{"shop_eid":28,"shop_sid":"1589144","shop_name":"Chee \u0102n V\u1eb7t","shop_allowed_pull":1,"shop_allowed_push":1,"channel_code":"LAZADA","channel_name":"Lazada","country_code":"VN","country_name":"Vietnam","country_timezone":"Asia\/Ho_Chi_Minh","country_exchange":"VND","country_format_right":0,"features":[{"feature_code":"M_LZD_SS","feature_name":"Lazada - Sponsored Search"}]},{"shop_eid":26,"shop_sid":"202166093","shop_name":"RB Hygiene Home","shop_allowed_pull":1,"shop_allowed_push":1,"channel_code":"SHOPEE","channel_name":"Shopee","country_code":"PH","country_name":"Philippines","country_timezone":"Asia\/Manila","country_exchange":"PHP","country_format_right":2,"features":[{"feature_code":"M_SHP_KB","feature_name":"Shopee - Keyword Bidding"}]},{"shop_eid":25,"shop_sid":"61783644","shop_name":"RB Home","shop_allowed_pull":1,"shop_allowed_push":1,"channel_code":"SHOPEE","channel_name":"Shopee","country_code":"PH","country_name":"Philippines","country_timezone":"Asia\/Manila","country_exchange":"PHP","country_format_right":2,"features":[{"feature_code":"M_SHP_KB","feature_name":"Shopee - Keyword Bidding"}]},{"shop_eid":21,"shop_sid":"156008447","shop_name":"dettol_officialstore","shop_allowed_pull":1,"shop_allowed_push":1,"channel_code":"SHOPEE","channel_name":"Shopee","country_code":"TH","country_name":"Thailand","country_timezone":"Asia\/Bangkok","country_exchange":"THB","country_format_right":2,"features":[{"feature_code":"M_SHP_KB","feature_name":"Shopee - Keyword Bidding"}]},{"shop_eid":20,"shop_sid":"40841302","shop_name":"Durex Official Shop","shop_allowed_pull":1,"shop_allowed_push":1,"channel_code":"SHOPEE","channel_name":"Shopee","country_code":"TH","country_name":"Thailand","country_timezone":"Asia\/Bangkok","country_exchange":"THB","country_format_right":2,"features":[{"feature_code":"M_SHP_KB","feature_name":"Shopee - Keyword Bidding"}]},{"shop_eid":16,"shop_sid":"242580604","shop_name":"testsingapore","shop_allowed_pull":1,"shop_allowed_push":1,"channel_code":"SHOPEE","channel_name":"Shopee","country_code":"SG","country_name":"Singapore","country_timezone":"Asia\/Singapore","country_exchange":"SGD","country_format_right":2,"features":[{"feature_code":"M_SHP_KB","feature_name":"Shopee - Keyword Bidding"}]}].reduce((acc, s) => {
  if (acc.find((s1) => s1.countryCode === s.country_code)) {
    return acc;
  } else {
    return acc.concat({
      countryCode: s.country_code,
      countryName: s.country_name,
      id: s.country_code,
      text: s.country_name,
    });
  }
}, []);

const selectedCompanies: any = [];

const users: any = Array.from({ length: 10 }, (_, key) => ({
  userId: `user-${key}`,
  userName: `Banana ${key}`,
  email: `email${key}@gmail.com`,
  avatar: 'https://material-ui.com/static/images/avatar/1.jpg',
  isAdded: key < 3
}))

export const addNewMember = () => {
  // const dispatch = useDispatch();

  // const { companies, selectedCompanies } = useSelector((state: any) => {
  //   return {
  //     companies: get(state, 'companies', []),
  //     selectedCompanies: get(state, 'selectedCompanies', []),
  //   };
  // });

  const handleChangeCompanies = (selected: any) => {
    console.log('handleChangeCompanies selected', selected);
    // dispatch(UPDATE_FILTER_COUNTRY({ countries: selected }));
  };

  const [searchText, setSearchText] = useState('');
  const [page, setPage] = useState(1);
  const [userSelected, setUserSelected] = useState(['user-3', 'user-4']);

  const handleChangeSearchText = (event: any) => setSearchText(event.target.value)

  const handleSearchKeyUp = (event: any) => {
    if (event.key === 'Enter') {
      console.log('event.target.value', { event: event.target.value });
    }
  };

  const handleIncreasePage = () => setPage(page + 1)

  const handleDescreasePage = () => setPage(page - 1)

  const handleSelected = (user: any) => {
    let newUserSelect = null
    if(!user.isAdded) {
      if (userSelected.includes(user.userId)) {
        newUserSelect = userSelected.filter(id => id !== user.userId);
      } else {
        newUserSelect = userSelected.concat(user.userId);
      }
      setUserSelected(newUserSelect)
    }
  }

  const handleClearAll = () => setUserSelected([])

  return <AddNewMember
    page={page}
    totalPage={10}
    users={users}
    userSelect={userSelected}
    onSelected={handleSelected}
    onClearAll={handleClearAll}
    companies={companies}
    selectedCompanies={selectedCompanies}
    onChangeCompanies={handleChangeCompanies}
    searchText={searchText}
    onChangeSearchText={handleChangeSearchText}
    onSearchKeyUp={handleSearchKeyUp}
    onIncreasePage={handleIncreasePage}
    onDescreasePage={handleDescreasePage}
  />

  // return <Provider store={store}>
  //   <ThemeProvider theme={ShopeeTheme}>
  //     <AddNewMember
  //       companies={companies}
  //       selectedCompanies={selectedCompanies}
  //       onChangeCompanies={handleChangeCompanies}
  //     />
  //   </ThemeProvider>
  // </Provider>;
};
