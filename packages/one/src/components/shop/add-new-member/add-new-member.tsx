import React from 'react';
import {
  Paper,
  OutlinedInput,
  withStyles,
  makeStyles,
  InputAdornment,
  Avatar,
  Grid,
  Divider,
  IconButton,
  Box,
} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import SortDescending from '@ep/shopee/src/images/sort-descending.svg';
import styled from 'styled-components';
import {
  ButtonUI,
  ButtonLinkUI,
} from '@ep/shopee/src/components/common/button';
import clsx from 'clsx';
import { MultiSelectUI } from '@ep/shopee/src/components/common/multi-select';

export function AddNewMember(props: any) {
  const {
    pageSize,
    page,
    totalPage,
    totalUsers,
    users,
    userSelect,
    onSelected,
    companies,
    selectedCompanies,
    onChangeCompanies,
    onSearchKeyUp,
    onClearAll,
    onIncreasePage,
    onDescreasePage,
    onSubmit,
  } = props;
  const classes = useStyle();

  return (
    <Paper elevation={0}>
      <Title>Add New Member</Title>
      <ShopContainer>
        <MultiSelectUI
          className={classes.MUIselect}
          prefix="Company"
          suffix="companies"
          items={companies}
          selectedItems={selectedCompanies}
          onSaveChange={onChangeCompanies}
        />
        <SearchBox
          id="campaign-searchbox"
          placeholder="Search"
          onKeyUp={onSearchKeyUp}
          startAdornment={
            <InputAdornment position="start">
              <SearchIcon fontSize="small" />
            </InputAdornment>
          }
          labelWidth={0}
          autoComplete="off"
          fullWidth
        />
        <UserContainer>
          <Grid
            container
            alignItems={'center'}
            justify={'space-between'}
            className={classes.sortUser}
          >
            <Grid item>
              <Grid container alignItems={'center'}>
                <UserSelected>
                  {userSelect.length} Selected
                </UserSelected>
                <DividerUI orientation="vertical" flexItem />
                <ButtonLinkUI
                  label={'Clear all'}
                  onClick={onClearAll}
                />
              </Grid>
            </Grid>
            {/*<Grid item>
              <IconButton aria-label="delete" size="small">
                <img src={SortDescending} />
              </IconButton>
            </Grid>*/}
          </Grid>
          {users.map(
            (user: {
              avatar: string;
              userName: string;
              email: string;
              isAdded: boolean;
              userId: string;
            }) => {
              return (
                <UserItem
                  key={user.userId}
                  onClick={() => onSelected(user)}
                  className={clsx({
                    userSelected: userSelect.includes(user.userId),
                  })}
                >
                  <Grid
                    container
                    alignItems={'center'}
                    justify={'space-between'}
                  >
                    <Grid item>
                      <Grid container spacing={1}>
                        <Grid item>
                          <Avatar
                            alt={user.userName}
                            src={user.avatar}
                          />
                        </Grid>
                        <Grid item>
                          <UserName color={user.isAdded && '#C2C7CB'}>
                            {user.userName}
                          </UserName>
                          <UserEmail
                            color={user.isAdded && '#C2C7CB'}
                          >
                            {user.email}
                          </UserEmail>
                        </Grid>
                      </Grid>
                    </Grid>
                    <Grid item>
                      <Available color={user.isAdded && '#C2C7CB'}>
                        {user.isAdded ? 'Added' : 'AVAILABLE'}
                      </Available>
                    </Grid>
                  </Grid>
                </UserItem>
              );
            },
          )}
          <Box mt={2}>
            <Grid
              container
              alignItems={'center'}
              justify={'space-between'}
            >
              <ButtonLinkUI
                disabled={page === 1}
                label={<ChevronLeftIcon />}
                onClick={onDescreasePage}
              />
              <Pagination>{`${1 + pageSize * (page - 1)} - ${
                page * pageSize
              } of ${totalUsers}`}</Pagination>
              <ButtonLinkUI
                disabled={page === totalPage}
                label={<ChevronRightIcon />}
                onClick={onIncreasePage}
              />
            </Grid>
          </Box>
        </UserContainer>
        <ButtonUI
          disabled={userSelect.length === 0}
          label={'Go to setup permission'}
          fullWidth
          onClick={onSubmit}
        />
      </ShopContainer>
    </Paper>
  );
}

const Title = styled.h1`
  font-style: normal;
  font-weight: 600;
  font-size: 29px;
  line-height: 32px;
  color: #253746;
  margin-bottom: 54px;
`;

const ShopContainer = styled.div`
  background: #fff;
  max-width: 50%;
  margin: auto;
`;

const UserContainer = styled.div`
  background: rgba(37, 55, 70, 0.02);
  padding: 16px;
`;

const UserItem = styled.div`
  background: #ffffff;
  border-radius: 4px;
  padding: 8px;
  margin-bottom: 4px;
  border: 2px solid transparent;
  cursor: pointer;
  &.userSelected {
    border-color: #485764;
  }
`;

const UserName = styled.div`
  font-weight: 500;
  font-size: 14px;
  line-height: 20px;
  color: ${({ color }: string) => color || '#253746'};
`;

const UserSelected = styled.span`
  font-size: 14px;
  line-height: 20px;
  color: #596772;
`;

const UserEmail = styled.div`
  font-size: 10px;
  line-height: 12px;
  color: ${({ color }: string) => color || '#596772'};
`;

const Available = styled.div`
  font-weight: bold;
  font-size: 11px;
  line-height: 16px;
  color: ${({ color }: string) => color || '#0BA373'};
  padding: 2px 6px;
  background: #e4e7e9;
  border-radius: 4px;
`;

const Pagination = styled.div`
  font-weight: 500;
  font-size: 14px;
  line-height: 20px;
  color: #7c8790;
`;

const useStyle = makeStyles({
  MUIselect: {
    marginBottom: 16,
    marginLeft: 0,
    textAlign: 'left',
    '& .MuiGrid-container': {
      justifyContent: 'flex-start',
    },
  },
  sortUser: {
    marginBottom: 16,
  },
});

const SearchBox = withStyles({
  root: {
    width: '100%',
    marginBottom: 16,
    '&:hover .MuiOutlinedInput-notchedOutline': {
      borderColor: '#C2C7CB',
    },
  },
  notchedOutline: {
    border: '2px solid #C2C7CB',
  },
  input: {
    padding: 8,
    fontSize: 14,
  },
})(OutlinedInput);

const DividerUI = withStyles({
  root: {
    marginLeft: 16,
    marginRight: 8,
  },
})(Divider);
