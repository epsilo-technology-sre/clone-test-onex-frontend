export const COLORS = {
  CHART: {
    ORANGE: `#dd7604`,
    BLUE: `#2e8cb8`,
  },
  COMMON: {
    MAIN_RGBA: '51, 63, 80',
    MAIN: '#333f50', //Note: design name is Ink1
    MAIN_OPA_70: 'rgba(51, 63, 80, 0.7)',
    MAIN_OPA_40: 'rgba(51, 63, 80, 0.4)',
    MAIN_OPA_20: 'rgba(51, 63, 80, 0.2)',
    MAIN_OPA_8: 'rgba(51, 63, 80, 0.08)',
    ORANGE_RGBA: '241, 97, 69',
    ORANGE: '#f16145',
    ORANGE_OPA_70: 'rgba(241, 97, 69, 0.7)',
    ORANGE_OPA_40: 'rgba(241, 97, 69, 0.4)',
    ORANGE_OPA_20: 'rgba(241, 97, 69, 0.2)',
    ORANGE_OPA_8: 'rgba(241, 97, 69, 0.08)',
    BLUE_RGBA: '32, 104, 237',
    BLUE: '#2068ED',
    BLUE_OPA_70: 'rgba(32, 104, 237, 0.7)',
    BLUE_OPA_40: 'rgba(32, 104, 237, 0.4)',
    BLUE_OPA_30: 'rgba(32, 104, 237, 0.3)',
    BLUE_OPA_20: 'rgba(32, 104, 237, 0.2)',
    BLUE_OPA_8: 'rgba(32, 104, 237, 0.08)',
    GREEN_RGBA: '58, 167, 109',
    GREEN: '#3AA76D',
    GREEN_OPA_70: 'rgba(58, 167, 109, 0.7)',
    GREEN_OPA_40: 'rgba(58, 167, 109,0.4)',
    GREEN_OPA_20: 'rgba(58, 167, 109, 0.2)',
    GREEN_OPA_8: 'rgba(58, 167, 109, 0.08)',
    RED_RGBA: '205, 50, 73',
    RED: '#CD3249',
    RED_OPA_70: 'rgba(205, 50, 73, 0.7)',
    RED_OPA_40: 'rgba(205, 50, 73, 0.4)',
    RED_OPA_20: 'rgba(205, 50, 73, 0.2)',
    RED_OPA_8: 'rgba(205, 50, 73, 0.08)',
    GRAY_RGBA: '246, 247, 248',
    GRAY: '#F6F7F8',
    GRAY_OPA_70: 'rgba(246, 247, 248, 0.7)',
    GRAY_OPA_40: 'rgba(246, 247, 248, 0.4)',
    GRAY_OPA_20: 'rgba(246, 247, 248, 0.2)',
    GRAY_OPA_8: 'rgba(246, 247, 248, 0.08)',
    WHITE_RGBA: '255, 255, 255',
    WHITE: '#FFFFFF',
    WHITE_OPA_70: 'rgba(255, 255, 255, 0.7)',
    WHITE_OPA_40: 'rgba(255, 255, 255, 0.4)',
    WHITE_OPA_20: 'rgba(255, 255, 255, 0.2)',
    WHITE_OPA_8: 'rgba(255, 255, 255, 0.08)',
    BG_LIGHT: '#F8FAFD',
    BG_DARK: '#F2F3F4',
  },
};

export const CURRENCY_LIST = [
  { currency: 'VND', name: 'VND' },
  { currency: 'SGD', name: 'SGD' },
  { currency: 'PHP', name: 'PHP' },
  { currency: 'IDR', name: 'IDR' },
  { currency: 'MYR', name: 'MYR' },
  { currency: 'THB', name: 'THB' },
  { currency: 'TWD', name: '-' },
  { currency: 'USD', name: 'USD' },
];

export const CHANNEL_ID = {
  shopee: 'SHOPEE',
  lazada: 'LAZADA',
};

export const LOCAL_STORAGE_KEY = {
  DASHBOARD_CURRENCY: 'ep_db_currency',
};
