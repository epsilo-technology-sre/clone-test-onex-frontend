import { CssBaseline } from '@material-ui/core';
import React, { useEffect, useMemo } from 'react';
import {
  BrowserRouter,
  HashRouter,
  Switch,
  Route,
  Redirect,
  useRouteMatch,
  useLocation,
} from 'react-router-dom';
import {
  publicMenu,
  internalMenu,
  PageWithInternalMenu,
} from './menu-list';
import { AuthContext, useAuth } from './hooks/use-auth';

const Router =
  process.env.NODE_ENV === 'production' ? BrowserRouter : HashRouter;
// const Router = HashRouter;
export default function OneApp() {
  const { publicRoutes: publicRoutes } = React.useMemo(() => {
    return {
      publicRoutes: publicMenu(),
    };
  }, []);

  return (
    <Router>
      <CssBaseline />
      <Switch>
        <Route
          exact
          path="/"
          component={() => <Redirect to="/login" />}
        />
        {publicRoutes.map((i) => (
          <Route key={i.url} exact path={i.url} component={i.page} />
        ))}
        <Route path="*">
          <AuthenticatedRoutes />
        </Route>
      </Switch>
      <ReleaseNote></ReleaseNote>
    </Router>
  );
}

// FIXME: [FE-8] check if the token is valid
function AuthenticatedRoutes() {
  const { token } = useAuth();
  if (token) {
    return <PageWithPermissions />;
  } else {
    window.location.href = '/';
    return null;
  }
}

function flatternMenuHierarchy(navList: any[]) {
  let items: any[] = [];

  if (!navList || navList.length === 0) return items;

  for (let menu of navList) {
    items = items.concat(menu);
    items = items.concat(flatternMenuHierarchy(menu.children));
  }

  return items;
}
function PageWithPermissions() {
  let { userSettings } = useAuth();
  let permByCode = userSettings.permissions?.byCode;

  if (!permByCode)
    return (
      <p style={{ marginLeft: '1em' }}>
        Loading your profile and settings...
      </p>
    );

  let canInternalAccess = () => {
    return userSettings.profile.userEmail.indexOf('@epsilo.io') > -1;
  };

  let canAccess = (requiredPermCodes: string[]) => {
    const accessable = requiredPermCodes.every((code: string) =>
      code === '__INTERNAL__'
        ? canInternalAccess()
        : permByCode![code] && permByCode![code].length > 0,
    );
    return accessable;
  };

  const privateRoutes = flatternMenuHierarchy(
    internalMenu(canAccess),
  );

  return (
    <AuthContext.Provider value={{ canAccess, canInternalAccess }}>
      <Switch>
        {privateRoutes
          .filter((i) => i.access !== false)
          .map((menuItem) => (
            <Route
              key={menuItem.url}
              exact={menuItem.exact === false ? false : true}
              path={menuItem.url}
              component={menuItem.page}
            />
          ))}
        <Route component={PageWithInternalMenu(Page404)} />
      </Switch>
    </AuthContext.Provider>
  );
}

function Page404() {
  return <h2>Not found.</h2>;
}

function ReleaseNote() {
  const {pathname} = useLocation();
  const branchName = process.env.STAGING_RELEASE_BRANCH;
  if (
    process.env.STAGING_RELEASE_BRANCH &&
    process.env.STAGING_RELEASE_BRANCH !== 'master'
  ) {
    return (
      <div
        style={{
          position: 'fixed',
          bottom: 0,
          right: 10,
          fontSize: '10px',
          background: 'rgb(37 55 70 / 39%)',
          color: '#eee',
          padding: 3,
          borderTopLeftRadius: 3,
          borderTopRightRadius: 3,
        }}
        title={'This would not be displayed on the production'}
      >
        Internal: {branchName}
      </div>
    );
  } else if(pathname.indexOf('/next') === 0) {
    return (
      <div
        style={{
          position: 'fixed',
          bottom: 0,
          right: 10,
          fontSize: '10px',
          background: 'rgb(37 55 70 / 39%)',
          color: '#eee',
          padding: 3,
          borderTopLeftRadius: 3,
          borderTopRightRadius: 3,
        }}
        title={'This would not be displayed on the production'}
      >
        Internal: insight/release-0.1
      </div>
    );
  }
  return null;
}
