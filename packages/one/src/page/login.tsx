import { API_URL } from '@ep/one/global';
import { LoadingUI } from '@ep/one/src/components/common/Loading';
import React from 'react';
import { useHistory } from 'react-router-dom';
import * as aim from '../../aim';
import { Login } from '../components/login/login';
import { StartPage } from '../components/start-page';
import * as request from '../utils/fetch';
import { mx } from '../utils/mixpanel';

const LOGIN_URL = `${API_URL}/api/v1/login`;

export default function PageLogin() {
  const history = useHistory();
  const [isLoading, setLoading] = React.useState(false);
  return (
    <StartPage>
      <Login
        onSubmit={(values) => {
          setLoading(true);
          return request
            .post(LOGIN_URL, {
              email: values.email,
              password: values.password,
            })
            .then((result: { data: { access_token: string } }) => {
              setLoading(false);
              aim.setLoginToken(result.data.access_token);
              mx.identify(values.email);
              history.push('/advertising/dashboard');
              return result.data.access_token;
            })
            .catch((error) => {
              setLoading(false);
              throw error;
            });
        }}
        urlForgotPassword={history.createHref({
          pathname: '/login/forget-password',
        })}
        urlRegister={history.createHref({ pathname: '/register' })}
      />
      <LoadingUI loading={isLoading} />
    </StartPage>
  );
}
