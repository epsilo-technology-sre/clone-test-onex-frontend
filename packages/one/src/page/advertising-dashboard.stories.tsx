import React from 'react';
import { InternalPage } from '@ep/one/src/components/internal-page/internal-page';
import { action } from '@storybook/addon-actions';
import { makeStore } from '@ep/one/src/container/dashboard/store';
import { setupWorker, rest } from 'msw';
import moment from 'moment';
import { range, random } from 'lodash';

import {
  Box,
  createMuiTheme,
  ThemeProvider,
  Typography,
} from '@material-ui/core';

import {
  ContainerLineCharts,
  ContainerMetricGroup,
} from '../container/dashboard/container';
import { navigation } from './navigation/demo-navigation';
import { GlobalFilters } from '../components/global-filters';
import { Provider } from 'react-redux';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#ED5C10',
    },
    secondary: {
      main: '#0BA373',
    },
  },
  typography: {
    fontSize: 14,
  },
});

export default {
  title: 'One / Page / Advertising dashboard',
};

export function Primary() {
  startMock();

  const store = makeStore();
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <InternalPage navigation={navigation} activePath={[3, 0, 0]}>
          <React.Fragment>
            <Typography variant="h4">Dashboard</Typography>
            <Box marginTop={2}>
              <GlobalFilters
                onChangeGlobalFilters={action('onChangeGlobalFilter')}
              />
            </Box>
            <Box marginTop={4}>
              <ContainerMetricGroup />
            </Box>
            <Box marginTop={2}>
              <ContainerLineCharts />
            </Box>
          </React.Fragment>
        </InternalPage>
      </ThemeProvider>
    </Provider>
  );
}

function startMock() {
  const demoData = [
    0,
    3.826743137732259,
    0,
    1.251256501027053,
    0,
    0.2748453161221461,
    2.642762284196547,
    0,
    1.7255772646536414,
    0,
    0.00001693784868149434,
    // 0.000008476970895733259,
    1000000000,
    0.8650408986248393,
    0.6187060284177128,
    0.48551278335485115,
    2.523794160348443,
    0.33565087951785355,
    0.38481070603466155,
    2.3872259886807474,
    0.7596648818095746,
    3.199784507201996,
    0.3325022579650732,
    1.7087516195482975,
    1.3645386871193323,
    3.3921896900794266,
    2.2876372984342135,
    3.5918446471313046,
    1.525000685701747,
    2.53180357389521,
    2.494733340725136,
  ];
  const availMetrics = Array(8)
    .fill(0)
    .map((i) => Math.random().toString().slice(2));

  const worker = setupWorker(
    rest.get('/mock/get-metrics', (req, res, ctx) => {
      const from = req.url.searchParams.get('from');
      const to = req.url.searchParams.get('to');
      return res(
        ctx.json({
          data: availMetrics.map((id, i) => ({
            metric_id: id,
            title: id,
            value: 600,
            percent_change: 15,
            previous_period_range: {
              from: Number(from),
              to: Number(to),
            },
          })),
        }),
      );
    }),

    rest.get('/mock/get-charts', (req, res, ctx) => {
      const from = req.url.searchParams.get('from');
      const to = req.url.searchParams.get('to');

      const metricId = req.url.searchParams.get('metric_id');

      const dates = range(0, 30).map((i) =>
        moment().subtract(i, 'day').valueOf(),
      );

      return res(
        ctx.json({
          data: {
            metric_id: metricId,
            label: metricId,
            series: demoData.map((i) => i + random(-1, 1, true)),
            dates: dates,
          },
        }),
      );
    }),

    rest.get('/mock/get-settings', (req, res, ctx) => {
      return res(
        ctx.json({
          data: {
            selected_charts: availMetrics.slice(0, 1),
            selected_metrics: availMetrics.slice(0, 5),
          },
        }),
      );
    }),
  );

  worker.start();
}
