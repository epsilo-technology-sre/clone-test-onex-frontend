import React from 'react';
import { Redirect, useRouteMatch } from 'react-router-dom';
import { ContainerShopAddNewMemberSelectUser } from '../../container/shop/shop-add-member-select-user';
import { ContainerShopAddNewMemberSetupPermission } from '../../container/shop/shop-add-member-permission';
import { useLoading } from '@ep/one/src/hooks/use-loading';
import { actions } from '../../container/shop/redux';
import { useDispatch, useSelector } from 'react-redux';
import { ShopState } from '../../container/shop/reducer';

export default function PageShopAddMember() {
  const {
    params: { shopEid },
  } = useRouteMatch();

  const [selectedMembers, setSelectedMembers] = React.useState<any>(
    [],
  );

  const [currentStep, setCurrentStep] = React.useState(1);

  const dispatch = useDispatch();

  const handleSubmitPermission = React.useCallback(
    (userPermission) => {
      console.info({ userPermission });
      dispatch(
        actions.saveUserPermission({
          users: userPermission,
          shopEid,
          redirectUrl: `/shop/shop-management/${shopEid}/details`,
        }),
      );
    },
    [],
  );
  const loading = useLoading();

  const toastRedirection = useSelector((state: ShopState) => {
    return state.toastRedirection;
  });

  let content = null;
  if (currentStep === 1) {
    content = (
      <React.Fragment>
        <ContainerShopAddNewMemberSelectUser
          shopEid={shopEid}
          onSelectUser={(users: any[]) => {
            setSelectedMembers(users);
            setCurrentStep(2);
          }}
        />
        {loading}
      </React.Fragment>
    );
  } else {
    content = (
      <React.Fragment>
        <ContainerShopAddNewMemberSetupPermission
          users={selectedMembers.map((i: any) => ({
            ...i,
            name: i.firstName,
          }))}
          shopEid={shopEid}
          handleSubmitPermission={handleSubmitPermission}
          handleBackToPreviousStep={() => {
            setCurrentStep(1);
          }}
        />
        {loading}
        {toastRedirection?.active && (
          <Redirect
            to={{
              pathname: toastRedirection.url,
              state: {
                message: toastRedirection.message,
                messageType: toastRedirection.messageType,
              },
            }}
          />
        )}
      </React.Fragment>
    );
  }

  return content;
}
