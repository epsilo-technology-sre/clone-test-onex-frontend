import React from 'react';
import { useRouteMatch } from 'react-router-dom';
import { ContainerShopDetails } from '../../container/shop/shop-details';

export default function PageShopDetails() {
  const {
    params: { shopEid },
  } = useRouteMatch();

  return <ContainerShopDetails shopEid={shopEid} />;
}
