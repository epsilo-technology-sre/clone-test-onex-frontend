import React from 'react';
import { Provider } from 'react-redux';
import { ContainerShopManagement } from '../../container/shop/shop-management';

import { makeStore } from '@ep/one/src/utils/store';
import { reducer } from '../../container/shop/reducer';
import { rootSaga } from '../../container/shop/redux';
import {
  useRouteMatch,
  Switch,
  Route,
  useHistory,
  useLocation,
} from 'react-router-dom';
import PageShopDetails from './shop-details';
import PageShopAddMember from './shop-add-member';
import PageLinkShop from '../link-shop';
import { useOneAlert } from '../../hooks';

export default function PageShopManagement() {
  const location = useLocation();
  const store = makeStore({
    storeName: 'One/Shop-mgt',
    reducer,
    rootSaga,
  });
  const { path } = useRouteMatch();

  const searchQuery = new URLSearchParams(location.search);

  let messageType = searchQuery.get('messageType') || 'success';
  let messageContent = searchQuery.get('message');
  const { alert, showAlert } = useOneAlert();

  React.useEffect(() => {
    if (messageContent) {
      showAlert(messageContent, messageType);
    }
  }, [messageContent]);

  return (
    <Provider store={store}>
      <Switch>
        <Route
          exact
          path={'/shop/shop-management/link-shop'}
          component={PageLinkShop}
        />
        <Route
          exact
          path={path}
          component={ContainerShopManagement}
        ></Route>
        <Route
          exact
          path={`${path}/:shopEid/details`}
          component={PageShopDetails}
        />
        <Route
          exact
          path={`${path}/:shopEid/add-member`}
          component={PageShopAddMember}
        />
      </Switch>
      {alert}
    </Provider>
  );
}
