import { makeStore } from '@ep/one/src/container/dashboard/store';
import { useOneFullPageLoading } from '@ep/one/src/hooks/use-loading';
import { BreadcrumbsUI } from '@ep/shopee/src/components/common/breadcrumbs';
import { Box, Typography } from '@material-ui/core';
import React from 'react';
import { Provider, useDispatch, useSelector } from 'react-redux';
import { CurrencySelection } from '../components/common/currency-selection';
import { CHANNEL_ID } from '../constants';
import {
  ContainerLineCharts,
  ContainerMetricGroup,
} from '../container/dashboard/container';
import { GlobalFiltersContainer } from '../container/dashboard/global-filters-container';
import {
  DashboardState,
  initState,
} from '../container/dashboard/reducer';
import { actions } from '../container/dashboard/redux';

export default function AdvertisingDashboard() {
  const store = makeStore(
    {
      ...initState,
      optionFilter: {
        ...initState.optionFilter,
        hideChannel: true,
      },
      filter: {
        ...initState.filter,
        channels: [{ id: CHANNEL_ID.lazada, text: 'default' }],
      },
    },
    'dashboard/shopee',
  );

  return (
    <Provider store={store}>
      <AdvertisingDashboardInside />
    </Provider>
  );
}

function AdvertisingDashboardInside() {
  const LINKS = [
    { text: 'Advertising', href: '/' },
    { text: 'Lazada', href: '/' },
    { text: 'Dashboard', href: '/' },
  ];
  const loading = useOneFullPageLoading({
    exclude: [actions.saveSettings.type()],
  });

  let { currency } = useSelector((state: DashboardState) => {
    return {
      currency: state.filter.currency,
    };
  });
  const dispatch = useDispatch();

  const handleChangeCurrency = (currency: string) => {
    dispatch(actions.updateFilterCurrency({ currency }));
  };

  return (
    <React.Fragment>
      <Box marginBottom={1}>
        <BreadcrumbsUI links={LINKS} onClickLink={() => {}} />
      </Box>
      <Box display="flex">
        <Typography variant="h4">Dashboard</Typography>
        <Box flexGrow="1"></Box>
        <Box width="240px">
          <CurrencySelection
            value={currency}
            onCurrencyChange={handleChangeCurrency}
          />
        </Box>
      </Box>
      <Box marginTop={2}>
        <GlobalFiltersContainer />
      </Box>
      <Box marginTop={4}>
        <ContainerMetricGroup />
      </Box>
      <Box marginTop={5}>
        <ContainerLineCharts />
      </Box>
      {loading}
    </React.Fragment>
  );
}
