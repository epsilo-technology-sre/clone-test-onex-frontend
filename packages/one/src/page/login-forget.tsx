import React from 'react';
import { useHistory } from 'react-router-dom';
import { LoginForget } from '../components/login/login-forget';
import { StartPage } from '../components/start-page';

export default function PageLogin() {
  const history = useHistory();
  return (
    <StartPage>
      <LoginForget
        urlLogin={history.createHref({
          pathname: '/login',
        })}
        urlRegister={history.createHref({
          pathname: '/register',
        })}
      />
    </StartPage>
  );
}
