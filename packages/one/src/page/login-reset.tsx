import React from 'react';
import { useHistory } from 'react-router-dom';
import { LoginReset } from '../components/login/login-reset';
import { StartPage } from '../components/start-page';

export default function PageLogin() {
  const history = useHistory();
  return (
    <StartPage>
      <LoginReset
        urlLogin={history.createHref({pathname: '/login'})}
        urlRegister={history.createHref({pathname: '/register'})}
      />
    </StartPage>
  );
}
