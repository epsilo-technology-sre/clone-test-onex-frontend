import React from 'react';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import { createStore, applyMiddleware } from 'redux';
import ContainerRegister from '../container/registration/container';
import { reducer, rootSaga } from '../container/registration/redux';
import { StartPage } from '../components/start-page';

export function PageRegistration() {
  const sagaMiddleware = createSagaMiddleware();
  let composeEnhancers = composeWithDevTools({
    name: 'register/full-flow',
  });

  let initState = undefined;
  function quickCheck() {
    initState = {
      email: 'phuc.phan@epsilo.io',
      isEmailValid: false,
      token: '',
      isTokenValid: false,
      regCode: 'RXSBGREFF',
      isRegCodeValid: false,
      currentStep: 'step4',
      loading: {
        emailValid: {
          status: true,
        },
        regCodeValid: {
          status: false,
          error: null,
        },
        tokenValid: {
          status: false,
          error: null,
        },
      },
    };
  }
  // quickCheck();

  let store = createStore(
    reducer,
    initState,
    composeEnhancers(applyMiddleware(sagaMiddleware)),
  );

  sagaMiddleware.run(rootSaga);

  return (
    <Provider store={store}>
      <StartPage>
        <ContainerRegister urlLogin={'/login'} />
      </StartPage>
    </Provider>
  );
}

export default PageRegistration;
