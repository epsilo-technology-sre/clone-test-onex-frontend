import { makeStore } from '@ep/one/src/container/dashboard/store';
import { Box, Typography } from '@material-ui/core';
import React from 'react';
import { Provider, useDispatch, useSelector } from 'react-redux';
import {
  ContainerLineCharts,
  ContainerMetricGroup,
} from '../container/dashboard/container';
import { GlobalFiltersContainer } from '../container/dashboard/global-filters-container';
import { useOneFullPageLoading } from '@ep/one/src/hooks/use-loading';
import { CurrencySelection } from '../components/common/currency-selection';
import { DashboardState } from '../container/dashboard/reducer';
import { actions } from '../container/dashboard/redux';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';

export default function AdvertisingDashboard() {
  const store = makeStore();

  return (
    <Provider store={store}>
      <AdvertisingDashboardInside />
    </Provider>
  );
}

function AdvertisingDashboardInside() {
  const loading = useOneFullPageLoading({
    exclude: [actions.saveSettings.type()],
  });

  let currency = useSelector((state: DashboardState) => {
    return state.filter.currency;
  });
  const dispatch = useDispatch();

  const handleChangeCurrency = (currency: string) => {
    dispatch(actions.updateFilterCurrency({ currency }));
  };

  return (
    <React.Fragment>
      <Box display="flex">
        <Typography variant="h4">Dashboard</Typography>
        <Box flexGrow="1" flexBasis={24} flexShrink={0}></Box>
        <Box width="120px">
          <CurrencySelection
            value={currency}
            onCurrencyChange={handleChangeCurrency}
          />
        </Box>
      </Box>
      <Box marginTop={2}>
        <GlobalFiltersContainer />
      </Box>
      <Box marginTop={4}>
        <ContainerMetricGroup />
      </Box>
      <Box marginTop={5}>
        <ContainerLineCharts />
      </Box>
      {loading}
      <ToastContainer closeOnClick={false} hideProgressBar={true} />
    </React.Fragment>
  );
}
