import React from 'react';
import { AddNewShop } from '../components/add-new-shop';
import * as request from '../utils/fetch';
import { ONE_EP } from '../../endpoint';
import { Alert, AlertTitle } from '@material-ui/lab';
import { Snackbar } from '@material-ui/core';
import { get } from 'lodash';
import { AddNewShopOTP } from '../components/add-new-shop/add-new-shop-step-2';
import { Redirect } from 'react-router';
import { useLoading } from '../use-loading';

type LinkShopStep = 'step1' | 'step2';

let cachedCode = { code: null, result: null, error: null };
function cachedRequestCode(code) {
  console.info({ cachedCode });
  if (cachedCode.code !== code) {
    return request
      .post(ONE_EP.CHECK_EXIST_SUBCRIPTION_CODE(), {
        code: code,
      })
      .then((rs) => {
        let result = rs.success as boolean;
        cachedCode.code = code;
        cachedCode.result = result;
        cachedCode.error = null;
        return result;
      })
      .catch((err) => {
        cachedCode.code = code;
        cachedCode.error = err;
        cachedCode.result = null;
        throw err;
      });
  } else {
    if (cachedCode.error) return Promise.reject(cachedCode.error);
    return Promise.resolve(cachedCode.result);
  }
}

export default function PageLinkShop() {
  const [shopInfo, setShopInfo] = React.useState({});
  const [countries, setCountries] = React.useState([]);
  const [channels, setChannels] = React.useState([]);
  const [error, setError] = React.useState('');
  const [currentStep, setCurrentStep] = React.useState<LinkShopStep>(
    // 'step2',
    'step1',
  );
  const [redirect, setRedirect] = React.useState('');

  React.useEffect(() => {
    request
      .get(ONE_EP.GET_COUNTRIES(), { is_active: 1 })
      .then((rs) => {
        let allCountryChannel = rs.data;
        const countryList = allCountryChannel.reduce((acc, c) => {
          if (acc.find((i: any) => i.countryId === c.country_code)) {
            return acc;
          } else {
            return acc.concat({
              countryId: c.country_code,
              countryName: c.country_name,
            });
          }
        }, []);

        const channelList = allCountryChannel
          .filter((i) => i.channel_is_active === 1)
          .reduce((acc, c) => {
            if (
              acc.find((i: any) => i.channelId === c.channel_code)
            ) {
              return acc;
            } else {
              return acc.concat({
                channelId: c.channel_code,
                channelName: c.channel_name,
              });
            }
          }, []);

        setCountries(countryList);
        setChannels(channelList);
      });
  }, []);

  // FIXME: prevent checking existing code everytime the form is updated
  const checkExistSubcriptionCode = React.useCallback(
    (code: string) => {
      return cachedRequestCode(code);
    },
    [],
  );

  const requestLinkShop = React.useCallback((values) => {
    setShopInfo((state) => ({ ...state, ...values }));

    return request.post(ONE_EP.LINK_SHOP(), {
      country_code: values.country,
      channel_code: values.channel,
      username: values.userName,
      password: values.password,
      subscription_code: values.subcriptionCode,
      phone: values.phone ?? undefined,
      otp: values.otp ?? undefined,
    });
  }, []);

  const onSubmitStep1 = React.useCallback((values) => {
    requestLinkShop(values)
      .then((res) => {
        console.info(res);
        if (get(res, 'data.redirect_auth_url', false)) {
          window.location.href = get(res, 'data.redirect_auth_url');
        } else if (
          get(res, 'data.success') === false &&
          get(res, 'data.message.code') === 481
        ) {
          setCurrentStep('step2');
        }
      })
      .catch((error) => {
        console.error(error);
        if (get(error, 'message.code') === 481) {
          setCurrentStep('step2');
        } else {
          setError(error.message);
        }
      });
  }, []);

  const onCloseError = React.useCallback(() => {
    setError('');
  }, []);

  const onSubmitStep2 = React.useCallback(
    (values) => {
      let data = { ...shopInfo };
      if (values.radios === 'phone') {
        data = { ...data, phone: values.phoneNumber };
      } else if (values.radios === 'code') {
        data = { ...data, otp: values.codeOTP };
      }

      requestLinkShop(data)
        .then(() => {
          setRedirect('/dashboard');
        })
        .catch((error) => {
          setError(error.message);
        });
    },
    [shopInfo],
  );

  let content = null;
  if (currentStep === 'step1') {
    content = (
      <AddNewShop
        channels={channels}
        countries={countries}
        checkSubcriptionCodeExists={checkExistSubcriptionCode}
        onSubmit={onSubmitStep1}
      />
    );
  } else if (currentStep === 'step2') {
    content = <AddNewShopOTP onSubmit={onSubmitStep2} />;
  }

  return (
    <React.Fragment>
      {content}
      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
        open={!!error}
        onClose={onCloseError}
        autoHideDuration={60000}
      >
        <Alert
          hidden={!error}
          onClose={onCloseError}
          severity="error"
        >
          {error}
        </Alert>
      </Snackbar>
    </React.Fragment>
  );
}
