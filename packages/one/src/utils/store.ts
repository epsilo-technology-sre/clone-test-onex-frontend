import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import { applyMiddleware, createStore } from 'redux';

export function makeStore({
  storeName,
  reducer,
  rootSaga,
  initState,
}: {
  storeName: string;
  reducer: (state: any, action: any) => any;
  rootSaga: Function;
  initState?: any;
}) {
  const sagaMiddleware = createSagaMiddleware();
  let composeEnhancers = composeWithDevTools({
    name: storeName,
  });

  let store = createStore(
    reducer,
    initState,
    composeEnhancers(applyMiddleware(sagaMiddleware)),
  );

  sagaMiddleware.run(rootSaga);

  return store;
}

export { makeStore as makeOneStore };
