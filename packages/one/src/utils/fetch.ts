import * as aim from '@ep/one/aim';
import axios from 'axios';
import { nanoid } from 'nanoid';
import qs from 'qs';

function urlBag(limit = 5) {
  let items: any[] = [];

  function push(item: any) {
    items.unshift(item);
    items = items.slice(0, limit);
  }

  return {
    push,
    get: () => items,
  };
}

export const lastUrls = urlBag(5);

axios.interceptors.request.use((config) => {
  let url = String(config.url);

  // if (url.indexOf('?') > -1) {
  //   url += '&_jsmx=' + nanoid();
  // } else {
  //   url += '?_jsmx=' + nanoid();
  // }

  return { ...config, url: url };
});

axios.interceptors.response.use((response) => {
  lastUrls.push(response.config.url);
  return response;
});

class ErrorRetryRequest extends Error {
  constructor(message: string) {
    super(message);
  }
}

const handleHeaders = () => {
  const headers: any = {
    'Content-Type': 'application/json',
  };
  if (aim.getLoginToken() && aim.getLoginToken().token) {
    headers['Authorization'] = `Bearer ${aim.getLoginToken().token}`;
  }
  return { ...headers };
};

const handleError = (res: any) => {
  if (!res) {
    throw { message: 'Network error.' };
  }
  const { status, data } = res;
  switch (status) {
    case 401: {
      aim.getAuthEvent().triggerExpired();
    }
    case 500:
      throw { message: 'Internal Server Error' };
    default:
      throw data;
  }
};

export const get = (url: string, body: any = null) => {
  const newHeaders = { ...handleHeaders() };
  const token = aim.getLoginToken();
  const config: any = {
    method: 'get',
    url,
    headers: newHeaders,
  };
  if (body) {
    config[`url`] = `${url}?${qs.stringify(body, {
      arrayFormat: 'comma',
    })}`;
  }
  return axios(config)
    .then(refreshLoginToken)
    .then((rs) => validForRetryToken(rs, token))
    .then((rs) => rs.data)
    .catch((e) => {
      if (e instanceof ErrorRetryRequest) {
        return async () => {
          await sleep();
          return post(url, body);
        };
      } else {
        return handleError(e.response);
      }
    });
};

export const post = (url: string, body: any) => {
  const newHeaders = { ...handleHeaders() };
  const token = aim.getLoginToken();
  const config: any = {
    method: 'post',
    url,
    data: body,
    headers: newHeaders,
  };
  return axios(config)
    .then(refreshLoginToken)
    .then((rs) => validForRetryToken(rs, token))
    .then((rs) => rs.data)
    .catch((e) => {
      if (e instanceof ErrorRetryRequest) {
        return async () => {
          await sleep();
          return post(url, body);
        };
      } else {
        return handleError(e.response);
      }
    });
};

export const put = (url: string, body: any) => {
  const newHeaders = { ...handleHeaders() };
  const token = aim.getLoginToken();
  const config: any = {
    method: 'put',
    url,
    data: body,
    headers: newHeaders,
  };
  return axios(config)
    .then(refreshLoginToken)
    .then((rs) => validForRetryToken(rs, token))
    .then((rs) => rs.data)
    .catch((e) => {
      if (e instanceof ErrorRetryRequest) {
        return async () => {
          await sleep();
          return put(url, body);
        };
      } else {
        return handleError(e.response);
      }
    });
};

export const deleteFetch = (url: string, body: any = null) => {
  const newHeaders = { ...handleHeaders() };
  const token = aim.getLoginToken();
  const config: any = {
    method: 'delete',
    url,
    headers: newHeaders,
    data: body,
  };
  // if (body) {
  //   config[`url`] = `${url}?${qs.stringify(body, {
  //     arrayFormat: 'comma',
  //   })}`;
  // }
  return axios(config)
    .then(refreshLoginToken)
    .then((rs) => validForRetryToken(rs, token))
    .then((rs) => rs.data)
    .catch((e) => {
      if (e instanceof ErrorRetryRequest) {
        return async () => {
          await sleep();
          return deleteFetch(url, body);
        };
      } else {
        return handleError(e.response);
      }
    });
};

export const download = (url: string, body: any = null, method='GET') => {
  const newHeaders = { ...handleHeaders() };
  const token = aim.getLoginToken();

  const config: any = {
    method: method,
    url,
    headers: { ...newHeaders, 'Content-Type': undefined },
    responseType: 'blob',
  };
  if (body && method === 'GET') {
    config[`url`] = `${url}?${qs.stringify(body, {
      arrayFormat: 'comma',
    })}`;
  } else {
    config.data = body;
  }
  return axios(config)
    .then(refreshLoginToken)
    .then((rs) => validForRetryToken(rs, token))
    .then((rs) => rs.data)
    .catch(async (e) => {
      if (e instanceof ErrorRetryRequest) {
        return async () => {
          await sleep();
          return post(url, body);
        };
      } else {
        const message = JSON.parse(await e.response.data.text())
          .message;
        throw { message };
      }
    });
};

export const upload = (url:string, formData: FormData) => {
  const newHeaders = { ...handleHeaders() };
  const token = aim.getLoginToken();

  const config: any = {
    method: 'POST',
    url,
    headers: { ...newHeaders, 'Content-Type': undefined },
    responseType: 'json',
  };

  return axios.post(url, formData, config);
}

function refreshLoginToken(rs: any) {
  console.debug('%c[refresh-token]', 'color:red;', rs.config.url);
  if (rs.headers['x-token']) {
    aim.setLoginToken(rs.headers['x-token']);
  }
  return rs;
}

function validForRetryToken(
  rs: any,
  requestToken: { token: string | null; lastUpdated: number | null },
) {
  let authToken = aim.getLoginToken();
  console.debug('%c[refresh-token]', 'color:red;', rs.config.url);
  console.debug('%c[refresh-token]', 'color:red;', {
    authToken,
    requestToken,
  });
  if (
    rs.status === 401 &&
    authToken.lastUpdated &&
    authToken.lastUpdated > requestToken.lastUpdated
  ) {
    console.warn(
      'valid for retry toke',
      authToken,
      rs.headers['x-token'],
    );
    throw new ErrorRetryRequest('token change');
  } else {
    return rs;
  }
}

function sleep(ms = 50) {
  return new Promise((resolve) => {
    window.setTimeout(() => {
      resolve(ms);
    }, ms);
  });
}
