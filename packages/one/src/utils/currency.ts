import numeral from 'numeral';

export const formatCurrency = (number, currency) => {
  if (currency === 'VND' || currency === 'IDR') {
    return numeral(number).format('0,0');
  }
  return numeral(number).format('0,0[.]00');
};

export const formatNumber = (number) => {
  return numeral(number).format('0[,]0[.]00a');
};

