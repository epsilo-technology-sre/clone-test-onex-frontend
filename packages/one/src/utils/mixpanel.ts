import { lastUrls } from './fetch';

export function trackError(error: Error) {
  let urls = lastUrls.get().map((i) => {
    return new URL(
      i,
      /^(http|https)/.test(i) === false
        ? 'http://localhost'
        : undefined,
    );
  });

  window.mixpanel &&
    window.mixpanel.track('WebError', {
      message: error.toString(),
      lastApis: urls.map((i) => i.pathname),
      errTrace: error.stack?.toString(),
      errRecordIds: urls.map((i) => {
        const sp = new URLSearchParams(i.search);
        return sp.get('_jsmx');
      }),
      errTs: 60 * 60 * Math.floor(Date.now() / (1000 * 60 * 60)),
    });
}

function track(eventName: string, payload: any = {}) {
  window.mixpanel && window.mixpanel.track(eventName, payload);
}

export const mx = {
  identify: (id: string) => {
    if (window.mixpanel) {
      window.mixpanel.identify(id);
    }
  },
  track,
};
