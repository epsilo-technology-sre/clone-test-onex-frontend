import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AddNewMember } from '../../components/shop/add-new-member';
import { ShopState } from './reducer';
import { actions } from './redux';

const empty: any[] = [];

export function ContainerShopAddNewMemberSelectUser({
  shopEid,
  onSelectUser,
}: {
  shopEid: number;
}) {
  const dispatch = useDispatch();

  const availCompanies = useSelector((state: ShopState) => {
    return state.availCompanies
      ? state.availCompanies.map((i) => ({
          ...i,
          id: i.companyId,
          text: i.name,
        }))
      : empty;
  });

  const companies = React.useMemo(() => availCompanies, [
    availCompanies.map((i) => i.companyId).join(''),
  ]);

  const allMembers = useSelector((state: ShopState) => {
    return state.members
      ? state.members.map((i) => ({
          ...i,
          userName: i.firstName + ' ' + i.lastName,
          isAdded: i.isAllocated,
        }))
      : empty;
  });

  React.useEffect(() => {
    dispatch(actions.loadAvailCompanies());
  }, [shopEid]);

  React.useEffect(() => {
    if (companies && companies.length > 0) {
      dispatch(
        actions.loadUsersForAllocate({
          shopEid,
          companyIds: companies.map((i) => i.companyId),
        }),
      );
      setSelectedCompanies(companies);
    }
  }, [companies, shopEid]); // FIXME: [FE-11] infinite loop without React.memo on `companies`

  const limit = 10;
  const [searchText, setSearchText] = React.useState('');
  const [page, setPage] = React.useState(1);
  const [userSelected, setUserSelected] = React.useState([]);
  const [selectedCompanies, setSelectedCompanies] = React.useState(
    [],
  );

  const { members, allFilteredMembers } = React.useMemo(() => {
    let selectedComIds = selectedCompanies.map((c) => c.companyId);
    let allFilteredMembers = allMembers
      .filter((m) => selectedComIds.indexOf(m.companyId) > -1)
      .filter(
        (m) =>
          (m.firstName || '').includes(searchText) ||
          (m.lastName || '').includes(searchText) ||
          (m.email || '').includes(searchText),
      );
    let members = allFilteredMembers.slice(
      (page - 1) * limit,
      page * limit,
    );
    return { members, allFilteredMembers };
  }, [allMembers, page, selectedCompanies, searchText]);

  const handleChangeSearchText = (event: any) =>
    setSearchText(event.target.value);

  const handleSearchKeyUp = (event: any) => {
    if (event.key === 'Enter') {
      setSearchText(event.target.value);
    }
  };

  const handleIncreasePage = () => setPage(page + 1);

  const handleDescreasePage = () => setPage(page - 1);

  const handleSelected = (user: any) => {
    let newUserSelect = null;
    if (!user.isAdded) {
      if (userSelected.includes(user.userId)) {
        newUserSelect = userSelected.filter(
          (id) => id !== user.userId,
        );
      } else {
        newUserSelect = userSelected.concat(user.userId);
      }
      setUserSelected(newUserSelect);
    }
  };

  const handleClearAll = () => setUserSelected([]);
  const handleSubmit = () => {
    onSelectUser(
      userSelected.map((id) => {
        return allMembers.find((u) => u.userId === id);
      }),
    );
  };

  return (
    <AddNewMember
      companies={companies}
      userSelect={userSelected}
      pageSize={limit}
      page={page}
      totalPage={Math.ceil(allFilteredMembers.length / limit)}
      users={members}
      totalUsers={allFilteredMembers.length}
      onSelected={handleSelected}
      onClearAll={handleClearAll}
      selectedCompanies={selectedCompanies}
      onChangeCompanies={(companies: any[]) =>
        setSelectedCompanies(companies)
      }
      searchText={searchText}
      onChangeSearchText={handleChangeSearchText}
      onSearchKeyUp={handleSearchKeyUp}
      onIncreasePage={handleIncreasePage}
      onDescreasePage={handleDescreasePage}
      onSubmit={handleSubmit}
    />
  );
}
