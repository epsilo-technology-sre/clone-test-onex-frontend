import React from 'react';
import { Provider } from 'react-redux';
import { ContainerShopManagement } from './shop-management';
import { ContainerShopDetails } from './shop-details';
import { makeStore } from '@ep/one/src/utils/store';
import { reducer } from './reducer';
import { rootSaga } from './redux';
import { ContainerShopAddNewMemberSelectUser } from './shop-add-member-select-user';
import { ContainerShopAddNewMemberSetupPermission } from './shop-add-member-permission';
import { HashRouter as Router } from 'react-router-dom';

export default {
  title: 'One / Container Shop',
};

export function Primary() {
  const store = makeStore({
    storeName: 'One/Shop-mgt',
    reducer,
    rootSaga,
  });

  return (
    <Router>
      <Provider store={store}>
        <ContainerShopManagement />
      </Provider>
    </Router>
  );
}

export function ShopDetails() {
  const store = makeStore({
    storeName: 'One/Shop-mgt',
    reducer,
    rootSaga,
  });

  return (
    <Router>
      <Provider store={store}>
        <ContainerShopDetails shopEid={4134} />
      </Provider>
    </Router>
  );
}

export function ShopAddNewMembers() {
  const store = makeStore({
    storeName: 'One/Shop-mgt',
    reducer,
    rootSaga,
  });

  return (
    <Router>
      <Provider store={store}>
        <ContainerShopAddNewMemberSelectUser shopEid={4134} />
      </Provider>
    </Router>
  );
}

export function ShopAssignPermission() {
  const store = makeStore({
    storeName: 'One/Shop-mgt',
    reducer,
    rootSaga,
  });

  const users = [
    {
      userId: 395,
      name: 'XYZ',
      email: 'trung.kieu@epsilo.io',
      isAllocated: 0,
      companyId: '1',
      isAdded: 0,
    },
    {
      userId: 406,
      name: '(Solution) - Phuc Nguyen',
      email: 'phuc.nguyen@epsilo.io',
      isAllocated: 0,
      companyId: '1',
      isAdded: 0,
    },
  ];

  return (
    <Router>
      <Provider store={store}>
        <ContainerShopAddNewMemberSetupPermission
          shopEid={4134}
          users={users}
        />
      </Provider>
    </Router>
  );
}
