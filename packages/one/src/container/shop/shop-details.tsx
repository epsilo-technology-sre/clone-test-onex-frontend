import { useLoading } from '@ep/one/src/hooks/use-loading';
import { groupBy, mapValues, omit } from 'lodash';
import moment from 'moment';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import { OneToastAlert } from '../../components/common/alert';
import { ShopManagement as ShopManagementDetails } from '../../components/shop/shop-management-details';
import { ShopState } from './reducer';
import { actions } from './redux';

export function ContainerShopDetails({
  shopEid,
}: {
  shopEid: number;
}) {
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();
  const loading = useLoading();

  const [alertMessage, setAlertMessage] = React.useState<{
    isOpen: boolean;
    message?: string;
    messageType?: 'error' | 'success';
  }>(location.state || {});

  const permissions = useSelector((state: ShopState) => {
    const featureList = state.featureList || [];
    const permissionList = state.permissionList || [];
    return featureList.map((item) => {
      const permission = permissionList
        .filter((p) => p.featureCode === item.permissionCode)
        .map((i) => ({ ...i, name: i.permissionName }));
      return {
        ...item,
        permission,
      };
    });
  });

  const shopInfo = useSelector((state: ShopState) => {
    if (!state.shopDetailsFocus) return null;

    let shopInfo = state.shopDetailsFocus;
    let shopFeatures = shopInfo.features_name;
    let membership = shopInfo.Membership;

    return {
      shopName: shopInfo.shops_name,
      shopId: shopInfo.shops_eid,
      country: shopInfo.countries_name,
      channel: shopInfo.channels_name,
      createdBy: `${shopInfo.shops_created_by} - ${moment(
        shopInfo.shops_created_at * 1000,
      ).format('DD/MM/YYYY')}`,
      updatedBy: shopInfo.shops_updated_by
        ? `${shopInfo.shops_updated_by} - ${moment(
            shopInfo.shops_updated_at * 1000,
          ).format('DD/MM/YYYY')}`
        : '',
      eid: shopInfo.shops_eid,
      subcriptionCode: Object.values(
        shopInfo.company_subscription_code_value,
      ).join(','),

      features: Object.keys(shopFeatures).map((k) => ({
        key: k,
        featureName: shopFeatures[k],
        featureCode: k,
      })),
      users: membership.map((item) => {
        let shopFeatures = item.features_name;
        return {
          key: item.users_id,
          firstName: item.users_first_name,
          lastName: item.users_last_name,
          email: item.users_email,
          featuresGroup: omit(
            mapValues(
              groupBy(shopFeatures, 'shop_user_role'),
              (clist) =>
                clist.map((f) => ({
                  key: f.features_id,
                  featureName: f.features_name,
                  featureCode: f.features_code,
                  permission: f.permission,
                  userRole: f.shop_user_role,
                })),
            ),
            'undefined',
          ),
          features: shopFeatures.map((k) => ({
            key: k.features_id,
            featureName: k.features_name,
            featureCode: k.features_code,
            permission: k.permission,
            userRole: k.shop_user_role,
          })),
        };
      }),
    };
  });

  React.useEffect(() => {
    dispatch(actions.loadShopDetails({ shopEid }));
    dispatch(actions.loadShopPermission({ shopEid }));
  }, [shopEid]);

  const handleAddNewMember = React.useCallback(() => {
    history.push(`/shop/shop-management/${shopEid}/add-member`);
  }, []);

  const handleSubmit = React.useCallback((userPermission) => {
    dispatch(
      actions.updateUserPermission({
        postAcl: userPermission,
        shopEid,
        callback: (res) => {
          setAlertMessage({
            isOpen: true,
            message: res.message,
            messageType: res.success ? 'success' : 'error',
          });
          if (res.success) {
            dispatch(actions.loadShopDetails({ shopEid }));
          }
        },
      }),
    );
  }, []);

  const handleRemoveAllocate = React.useCallback((userId) => {
    dispatch(
      actions.removeUserAllocate({
        userId,
        shopEid,
        callback: (res) => {
          setAlertMessage({
            isOpen: true,
            message: res.message,
            messageType: res.success ? 'success' : 'error',
          });
          if (res.success) {
            dispatch(actions.loadShopDetails({ shopEid }));
          }
        },
      }),
    );
  }, []);

  const handleChangeSearchText = (value) => {
    dispatch(actions.loadShopDetails({ shopEid, search: value }));
  };

  if (!shopInfo) return loading;

  return (
    <React.Fragment>
      <ShopManagementDetails
        onSubmit={handleSubmit}
        permissions={permissions}
        shopInfo={shopInfo}
        onAddNewMember={handleAddNewMember}
        onRemoveAllocate={handleRemoveAllocate}
        onChangeSearchText={handleChangeSearchText}
      />
      {alertMessage.isOpen && (
        <OneToastAlert
          message={alertMessage.message}
          messageType={alertMessage.messageType}
          isOpen={alertMessage.isOpen}
          onClose={() => {
            setAlertMessage({
              ...alertMessage,
              isOpen: false,
            });
          }}
        />
      )}
    </React.Fragment>
  );
}
