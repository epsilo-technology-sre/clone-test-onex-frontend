import { produce } from 'immer';
import { actions } from './redux';

type loading = {
  [key: string]: {
    status: boolean;
    error: any;
  };
};

type pagination = {
  resultTotal: number;
  page: number;
  pageSize: number;
};

export type ShopState = {
  loading: loading;
  toastRedirection?: {
    url: string;
    message: string;
    messageType: 'success' | 'error';
    active: boolean;
  };
  shops: any[];
  pagination: {
    shops?: pagination;
  };
  allChannelsCountries: any[];
  optionCountries: any[];
  optionChannels: any[];
  selectedCountries: any[];
  selectedChannels: any[];
  shopDetailsFocus?: any;
  availCompanies?: any[];
  members?: any[];
  featureList?: any[];
  permissionList?: any[];
  permissionByUserId: {
    [key: number]: { [code: string]: number };
  };
  featureByUserId: {
    [key: number]: string[];
  };
  countryCode: any;
  channelCode: any;
  searchText: string;
};

const initState: ShopState = {
  loading: {},
  shops: [],
  pagination: {
    shops: {
      resultTotal: 0,
      page: 1,
      pageSize: 10,
    },
  },
  allChannelsCountries: [],
  optionCountries: [],
  optionChannels: [],
  selectedCountries: [],
  selectedChannels: [],
  permissionByUserId: {},
  featureByUserId: {},
  countryCode: '',
  channelCode: '',
  searchText: '',
};

export function reducer(
  state: ShopState = initState,
  action: { type: string; payload: any },
) {
  const { payload } = action;
  switch (action.type) {
    case 'REQUEST_ACTION_LOADING': {
      state = produce(state, (draft) => {
        draft.loading[action.payload.loading.section] = {
          status: action.payload.loading.status,
          error: action.payload.loading.error,
        };
      });
      break;
    }
    case 'TOAST_AND_REDIRECT': {
      state = produce(state, (draft) => {
        draft.toastRedirection = {
          url: payload.url,
          message: payload.message,
          messageType: payload.messageType,
          active: true,
        };
      });
      break;
    }
    case actions.loadShops.fetchType(): {
      console.info('loadShops', payload);
      state = produce(state, (draft) => {
        draft.shops = payload.shops;
        draft.pagination.shops = payload.pagination;
      });
      break;
    }
    case actions.loadChannelCountries.fetchType(): {
      state = produce(state, (draft) => {
        const allCC = payload.allChannelsCountries;
        draft.allChannelsCountries = payload.allChannelsCountries;
        const countryList = allCC.reduce((acc, c) => {
          if (
            acc.find((i: any) => i.countryCode === c.country_code)
          ) {
            return acc;
          } else {
            return acc.concat({
              countryCode: c.country_code,
              countryName: c.country_name,
            });
          }
        }, []);
        const channelList = allCC
          .filter((i) => i.channel_is_active === 1)
          .reduce((acc, c) => {
            if (
              acc.find((i: any) => i.channelCode === c.channel_code)
            ) {
              return acc;
            } else {
              return acc.concat({
                channelCode: c.channel_code,
                channelName: c.channel_name,
              });
            }
          }, []);

        draft.optionChannels = channelList;
        draft.optionCountries = countryList;
        draft.selectedChannels = channelList.map((i) => ({
          id: i.channelCode,
          text: i.channelName,
        }));
        draft.selectedCountries = countryList.map((i) => ({
          id: i.countryCode,
          text: i.countryName,
        }));
      });
      break;
    }
    case actions.changeSelectedCountries.type(): {
      state = produce(state, (draft) => {
        draft.selectedCountries = payload.countries;
      });
      break;
    }
    case actions.changeSelectedChannels.type(): {
      state = produce(state, (draft) => {
        const channelCodes = payload.channels.map((i) => i.id);
        draft.selectedChannels = payload.channels;
        let optionCountries = state.allChannelsCountries
          .filter((i) => channelCodes.indexOf(i.channel_code) > -1)
          .reduce((acc, c) => {
            if (
              acc.find((i: any) => i.countryCode === c.country_code)
            ) {
              return acc;
            } else {
              return acc.concat({
                countryCode: c.country_code,
                countryName: c.country_name,
              });
            }
          }, []);
        draft.optionCountries = optionCountries;
        draft.selectedCountries = optionCountries.map((i) => ({
          id: i.countryCode,
          text: i.countryName,
        }));
      });
      break;
    }
    case actions.loadShopDetails.fetchType(): {
      state = produce(state, (draft) => {
        draft.shopDetailsFocus = payload.shopDetails;
      });
      break;
    }
    case actions.loadAvailCompanies.fetchType(): {
      state = produce(state, (draft) => {
        draft.availCompanies = payload.availCompanies;
      });
      break;
    }
    case actions.loadUsersForAllocate.fetchType(): {
      state = produce(state, (draft) => {
        draft.members = payload.users;
      });
      break;
    }
    case actions.loadShopPermission.fetchType(): {
      state = produce(state, (draft) => {
        draft.featureList = payload.featureList;
        draft.permissionList = payload.permissionList;
        draft.permissionByUserId = {};
      });
      break;
    }
    case actions.updatePermission.type(): {
      let { payload } = action;
      state = produce(state, (draft) => {
        let flist = state.featureList!;
        let plist = state.permissionList!;

        let userPerm = Object.assign(
          {},
          state.permissionByUserId[payload.userId],
        );
        let feature = flist.find(
          (f) => f.permissionCode === payload.permissionCode,
        );

        if (feature) {
          userPerm = plist
            .filter((p) => p.featureCode === feature.permissionCode)
            .reduce(
              (accum, i) => {
                return {
                  ...accum,
                  [i.permissionCode]: payload.checked ? 1 : 0,
                };
              },
              { [feature.permissionCode]: payload.checked ? 1 : 0 },
            );
        } else {
          userPerm[payload.permissionCode] = payload.checked ? 1 : 0;
        }

        Object.keys(userPerm).reduce((accum: string[], pCode) => {
          let perm = plist.find((p) => p.permissionCode === pCode);
          let includeCode = perm
            ? perm.permissionMustIncludeCode
            : undefined;
          if (
            includeCode &&
            (userPerm[pCode] || accum.includes(includeCode))
          ) {
            userPerm[includeCode] = 2;
            return accum.concat(includeCode);
          } else if (includeCode && userPerm[includeCode]) {
            userPerm[includeCode] = 1;
          }
          return accum;
        }, []);

        draft.permissionByUserId[payload.userId] = {
          ...state.permissionByUserId[payload.userId],
          ...userPerm
        };

        const permList = Object.keys(userPerm).filter((p) => {
          return userPerm[p] > 0;
        });

        draft.featureByUserId[payload.userId] = [
          ...new Set(
            plist
              .filter((i) => permList.indexOf(i.permissionCode))
              .map((i) => i.featureCode),
          ),
        ];
      });
      break;
    }
  }
  return state;
}
