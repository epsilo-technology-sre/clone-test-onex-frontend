import { API_URL } from '@ep/one/global';
import * as efetch from '@ep/one/src/utils/fetch';
import { sortedUniqBy } from 'lodash';
import * as eff from 'redux-saga/effects';
import { ONE_EP } from '../../../endpoint';
import { actionWithRequest, makeAction } from '../../utils/redux';
import { ShopState } from './reducer';

const ep = {
  GET_USER_SHOPS: () => `${API_URL}/api/v1/user/shop-management`,
};

const actions = {
  loadShops: actionWithRequest({
    actname: 'SHOP/LOAD_USER_SHOPS',
    fun: function* loadShops({
      limit,
      page,
      countryCode,
      channelCode,
      searchText,
    }) {
      console.info('loading shops...');
      const rs = yield eff.call(() => {
        return efetch.get(ep.GET_USER_SHOPS(), {
          limit,
          page,
          country_code: countryCode,
          channel_code: channelCode,
          search: searchText,
        });
      });
      return {
        shops: rs.data,
        pagination: {
          resultTotal: rs.pagination.item_count,
          page: page,
          pageSize: limit,
        },
      };
    },
  }),
  loadShopPermission: actionWithRequest({
    actname: 'SHOP/LOAD_SHOP_PERMISSION',
    fun: function* loadShops({ shopEid }) {
      const rs = yield eff.call(() => {
        return efetch.get(ONE_EP.GET_SHOP_PERMISSIONS(), {
          shop_eid: shopEid,
        });
      });

      let featureList = rs.data.map((i) => ({
        permissionName: i.features_name,
        permissionCode: i.features_code,
        channelCode: i.channels_code,
        channelName: i.channels_name,
        isFeature: true,
      }));

      featureList = sortedUniqBy(
        featureList,
        (i) => i.permissionName,
      );
      let permissionList = rs.data.map((p) => ({
        permissionName: p['permission_name'],
        permissionCode: p['permission_code'],
        featureCode: p['features_code'],
        permissionMustIncludeCode: p['permission_inherit_code'],
      }));

      return {
        featureList: featureList,
        permissionList: permissionList,
      };
    },
  }),
  loadChannelCountries: actionWithRequest({
    actname: 'SHOP/LOAD_CHANNELS_COUNTRIES',
    fun: function* loadChannelsCountries() {
      const rs = yield eff.call(() => {
        return efetch.get(ONE_EP.GET_COUNTRIES());
      });

      return {
        allChannelsCountries: rs.data,
      };
    },
  }),
  changeSelectedCountries: makeAction({
    actname: 'SHOP/CHANGE_SELECTED_COUNTRIES',
    fun: ({ countries }) => ({ countries }),
  }),
  changeSelectedChannels: makeAction({
    actname: 'SHOP/CHANGE_SELECTED_CHANNELS',
    fun: ({ channels }) => ({ channels }),
  }),
  loadShopDetails: actionWithRequest({
    actname: 'SHOP/LOAD_SHOP_DETAILS',
    fun: function* loadShopDetails({ shopEid, search = '' }) {
      const rs = yield eff.call(() => {
        return efetch.get(ONE_EP.GET_SHOP_DETAILS(), {
          shop_eid: shopEid,
          search,
        });
      });

      return { shopDetails: rs.data };
    },
  }),
  loadAvailCompanies: actionWithRequest({
    actname: 'SHOP/LOAD_AVAIL_COMPANIES',
    fun: function* loadAvailCompanies() {
      const rs = yield eff.call(() => {
        return efetch.get(ONE_EP.GET_AVAIL_COMPANIES(), {
          is_partner: 1,
        });
      });

      return {
        availCompanies: rs.data.map((i) => ({
          companyId: i.companies_id,
          name: i.companies_name,
          isPartner: i.is_partner,
        })),
      };
    },
  }),
  loadUsersForAllocate: actionWithRequest({
    actname: 'SHOP/LOAD_USERS_BY_SHOP',
    fun: function* loadUsers({ shopEid, companyIds }) {
      const rs = yield eff.call(() => {
        return efetch.get(ONE_EP.GET_USERS_FOR_ALLOCATE(), {
          list_company_id: companyIds,
          shop_eid: shopEid,
        });
      });

      return {
        users: rs.data.map((i) => ({
          userId: i.users_id,
          firstName: i.users_first_name,
          lastName: i.users_last_name,
          email: i.users_email,
          isAllocated: i.is_allocated,
          companyId: i.companies_id,
        })),
      };
    },
  }),

  updatePermission: makeAction({
    actname: 'SHOP/UPDATE_USER_PERMISSIONS',
    fun: function updatePermission({
      userId,
      permissionCode,
      checked,
      permissionRow,
    }) {
      console.info({
        userId,
        permissionCode,
        checked,
        permissionRow,
      });
      return { userId, permissionCode, checked, permissionRow };
    },
  }),

  saveUserPermission: actionWithRequest({
    actname: 'SHOP/POST_USER_PERMISSION',
    fun: function* saveUserPermission({
      users,
      shopEid,
      redirectUrl,
    }) {
      const fByUser = yield eff.select((state: ShopState) => {
        return state.featureByUserId;
      });

      const userIds = Object.keys(fByUser);

      for (let userId of userIds) {
        yield eff.call(() => {
          return efetch.post(ONE_EP.ALLOCATE_SHOP_MEMBER(), {
            shop_eid: shopEid,
            list_user_id: [userId],
            list_feature_code: fByUser[userId],
          });
        });
      }

      let postAcl = Object.keys(users).reduce((acl, userId) => {
        let aclByUser = {
          [userId]: Object.keys(users[userId]).filter(
            (i) => users[userId][i] > 0,
          ),
        };

        if (aclByUser[userId].length === 0) {
          return acl;
        }

        aclByUser[userId] = aclByUser[userId].filter(
          (pCode) => fByUser[userId].indexOf(pCode) === -1,
        );

        return {
          ...acl,
          ...aclByUser,
        };
      }, {});

      let rs = yield eff.call(() => {
        return efetch.post(ONE_EP.POST_USER_PERMISSION(), {
          shop_eid: shopEid,
          acl: postAcl,
        });
      });

      yield eff.put({
        type: 'TOAST_AND_REDIRECT',
        payload: {
          url: redirectUrl,
          message: rs.message,
          messageType: 'success',
        },
      });

      return { data: rs.data };
    },
  }),
  updateUserPermission: actionWithRequest({
    actname: 'SHOP/POST_UPDATE_USER_PERMISSION',
    fun: function* updateUserPermission({
      postAcl,
      shopEid,
      callback,
    }) {
      let rs = yield eff.call(() => {
        return efetch.post(ONE_EP.POST_USER_PERMISSION(), {
          shop_eid: shopEid,
          acl: postAcl,
        });
      });

      yield eff.put({
        type: 'TOAST_AND_REDIRECT',
        payload: {
          message: rs.message,
          messageType: 'success',
        },
      });

      callback(rs);
      return { data: rs.data };
    },
  }),
  removeUserAllocate: actionWithRequest({
    actname: 'SHOP/POST_REMOVE_USER_ALLOCATE',
    fun: function* removeUserAllocate({ userId, shopEid, callback }) {
      let rs: any = {};
      try {
        rs = yield eff.call(() => {
          return efetch.post(ONE_EP.USER_REMOVE_ALLOCATE(), {
            shop_eid: shopEid,
            users_id: userId,
          });
        });
      } catch (error) {
        rs = {
          ...error,
          data: error.message,
        };
      }

      callback(rs);
      return { data: rs.data };
    },
  }),
};

export { actions };

export function* rootSaga() {
  yield eff.all([
    actions.loadShops.saga(),
    actions.loadChannelCountries.saga(),
    actions.loadShopDetails.saga(),
    actions.loadAvailCompanies.saga(),
    actions.loadUsersForAllocate.saga(),
    actions.loadShopPermission.saga(),
    actions.saveUserPermission.saga(),
    actions.updateUserPermission.saga(),
    actions.removeUserAllocate.saga(),
  ]);
}
