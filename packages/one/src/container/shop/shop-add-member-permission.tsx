import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { SetupPermission } from '../../components/shop/setup-permission';
import { ShopState } from './reducer';
import { actions } from './redux';
import { get } from 'lodash';
import { OneFullPageLoading } from '../../components/common/Loading';

export function ContainerShopAddNewMemberSetupPermission({
  users,
  shopEid,
  handleSubmitPermission,
  handleBackToPreviousStep,
}: {
  users: any[];
  shopEid: number;
  handleSubmitPermission: Function;
  handleBackToPreviousStep: Function;
}) {
  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(actions.loadShopPermission({ shopEid }));
  }, [shopEid]);

  const { status: loadingStatus, error: loadingError } = useSelector(
    (state: ShopState) => {
      return get(
        state,
        ['loading', actions.loadShopPermission.type()],
        {},
      );
    },
  );

  const permissions = useSelector((state: ShopState) => {
    const featureList = state.featureList || [];
    const permissionList = state.permissionList || [];

    let permission: any[] = [];

    for (let feature of featureList) {
      let availPermissions = permissionList
        .filter((p) => p.featureCode === feature.permissionCode)
        .map((i) => ({ ...i, name: i.permissionName }));

      if (availPermissions.length > 0) {
        permission = permission.concat({
          ...feature,
          name: feature.permissionName,
          isHead: true,
        });
        permission = permission.concat(availPermissions);
      }
    }
    return permission;
  });

  const permissionByUserId = useSelector((state: ShopState) => {
    return state.permissionByUserId || {};
  });

  const updatePermission = React.useCallback((item) => {
    dispatch(
      actions.updatePermission({
        userId: item.userId,
        permissionCode: item.permission.permissionCode,
        checked: item.checked,
        permissionRow: item.row,
      }),
    );
  }, []);

  const handleSubmit = () => {
    handleSubmitPermission(permissionByUserId);
  };

  return (
    <React.Fragment>
      <SetupPermission
        permissions={permissions}
        users={users}
        permissionsByUserId={permissionByUserId}
        onSelectItem={updatePermission}
        onSubmit={handleSubmit}
        onBackToPreviousStep={handleBackToPreviousStep}
      />
      <OneFullPageLoading loading={loadingStatus} />
    </React.Fragment>
  );
}
