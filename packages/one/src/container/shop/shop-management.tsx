import { useLoading } from '@ep/one/src/hooks/use-loading';
import moment from 'moment';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { ShopManagement } from '../../components/shop/shop-management/shop-management';
import { ShopState } from './reducer';
import { actions } from './redux';

export function ContainerShopManagement() {
  const dispatch = useDispatch();
  const history = useHistory();
  const {
    shops,
    pagination,
    channelList = [],
    countryList = [],
    selectedChannels = [],
    selectedCountries = [],
    countryCode,
    channelCode,
    searchText,
  } = useSelector((state: ShopState) => {
    // FIXME: API where to get shop status

    let shops = state.shops.map((s) => ({
      ...s,
      shopName: s.shops_name,
      countryCode: s.countries_code,
      shopEid: s.shops_eid,
      channel: s.channels_name,
      channelCode: s.channels_code,
      feature: s.features.length,
      userCreate: s.shops_created_by,
      created: moment
        .unix(s.shops_created_at)
        .format('DD/MM/YYYY HH:mm:ss'),
      userUpdate: s.shops_updated_by || '',
      updated: s.shops_updated_at
        ? moment
            .unix(s.shops_updated_at)
            .format('DD/MM/YYYY HH:mm:ss')
        : '-',
      user: s.countUser, // FIXME: API convention snake case
      url: history.createHref({
        pathname: '/shop/shop-management/' + s.shops_eid + '/details',
      }),
    }));

    return {
      shops,
      pagination: state.pagination.shops!,
      countryList: state.optionCountries.map((i) => ({
        text: i.countryName,
        id: i.countryCode,
      })),
      channelList: state.optionChannels.map((i) => ({
        text: i.channelName,
        id: i.channelCode,
      })),
      selectedCountries: state.selectedCountries,
      selectedChannels: state.selectedChannels,
      countryCode: state.countryCode,
      channelCode: state.channelCode,
      searchText: state.searchText,
    };
  });

  let loading = useLoading();

  React.useEffect(() => {
    getShop();
    dispatch(actions.loadChannelCountries());
  }, []);

  let getShopId = React.useCallback((row) => {
    return row.shopEid;
  }, []);

  const getShop = (params?: {}) => {
    dispatch(
      actions.loadShops({
        limit: pagination.pageSize,
        page: pagination.page,
        countryCode,
        channelCode,
        searchText,
        ...params,
      }),
    );
  };

  const handleChangePage = (page: number) => {
    getShop({ page });
  };
  const handleChangePageSize = (pageSize: number) => {
    getShop({ ...pagination, limit: pageSize });
  };

  const handleChangeCountries = (countries) => {
    dispatch(actions.changeSelectedCountries({ countries }));
  };

  const handleChangeChannel = (channels) => {
    dispatch(actions.changeSelectedChannels({ channels }));
  };

  const handleSubmitChange = () => {
    const countryCode = selectedCountries
      .map((country: { id: string; text: string }) => country.id)
      .join(',');
    const channelCode = selectedChannels
      .map((channel: { id: string; text: string }) => channel.id)
      .join(',');
    getShop({ countryCode, channelCode });
  };

  const handleChangeSearchText = (search) => {
    getShop({ searchText: search });
  };
  const handleAddNewShop = () => {
    history.push({
      pathname: '/shop/shop-management/link-shop',
    });
  };

  return (
    <React.Fragment>
      <ShopManagement
        onAddNewShop={handleAddNewShop}
        shops={shops}
        countries={countryList}
        channels={channelList}
        selectedChannels={selectedChannels}
        selectedCountries={selectedCountries}
        onChangeChannels={handleChangeChannel}
        onChangeCountries={handleChangeCountries}
        onSubmitChange={handleSubmitChange}
        onChangeSearchText={handleChangeSearchText}
        getShopId={getShopId}
        onChangePage={handleChangePage}
        onChangePageSize={handleChangePageSize}
        onSort={() => {}}
        noData="No shop..."
        {...pagination}
      />
      {loading}
    </React.Fragment>
  );
}
