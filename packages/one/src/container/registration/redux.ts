import { produce } from 'immer';
import * as eff from 'redux-saga/effects';
import {
  fetchJson,
  makeAction,
  makeReducer,
  actionWithRequest,
} from './util';
import { API_URL } from '@ep/one/global';

const endpoints = {
  CHECK_EMAIL_VALID: () => `${API_URL}/api/v1/user/verify-email`,
  CHECK_EMAIL_TOKEN_VALID: () => `${API_URL}/api/v1/user/verify-otp`,
  CHECK_REG_CODE_VALID: () =>
    `${API_URL}/api/v1/user/verify-registration-code`,
  CREATE_ACCOUNT: () => `${API_URL}/api/v1/user/register-account`,
};

type loading = {
  emailValid: { status: boolean; error: any };
  tokenValid: { status: boolean; error: any };
  regCodeValid: { status: boolean; error: any };
  createAccount: { status: boolean; error: any };
  [key: string]: {
    status: boolean;
    error: any;
  };
};

type RegisterState = {
  email?: string;
  isEmailValid: boolean;
  token?: string;
  isTokenValid?: boolean;
  regCode?: string;
  isRegCodeValid?: boolean;
  currentStep: 'step1' | 'step2' | 'step3' | 'step4' | 'final';
  loading: loading;
};

/**
 * =================
 */

export const actions = {
  checkEmailValid: actionWithRequest('CHECK_EMAIL_VALID', function* ({
    email,
  }) {
    const rs = yield eff.call(() =>
      fetchJson(endpoints.CHECK_EMAIL_VALID(), {
        query: { email },
      }),
    );

    return {
      email,
      isEmailValid: rs.success,
    };
  }),

  checkTokenValid: actionWithRequest('CHECK_TOKEN_VALID', function* ({
    email,
    token,
  }) {
    const rs = yield eff.call(() =>
      fetchJson(endpoints.CHECK_EMAIL_TOKEN_VALID(), {
        query: {
          email: email,
          otp: token,
        },
      }),
    );

    return {
      email,
      isTokenValid: rs.success,
    };
  }),
  checkRegCodeValid: actionWithRequest(
    'CHECK_REG_CODE_VALID',
    function* ({ email, code }) {
      const rs = yield eff.call(() =>
        fetchJson(endpoints.CHECK_REG_CODE_VALID(), {
          query: {
            email: email,
            registration_code: code,
          },
        }),
      );

      return {
        email: email,
        regCode: code,
        isRegCodeValid: rs.success,
      };
    },
  ),
  createAccount: actionWithRequest('CREATE_ACCOUNT', function* ({
    firstName,
    lastName,
    email,
    password,
    phone,
    regCode,
  }) {
    const rs = yield eff.call(() =>
      fetchJson(
        endpoints.CREATE_ACCOUNT(),
        {
          data: {
            email: email,
            first_name: firstName,
            last_name: lastName,
            phone: String(phone).replace(/[^\d]/g, ''),
            password: password,
            registration_code: regCode,
          },
        },
        'POST',
      ),
    );
    return {
      email: email,
      isSuccess: rs.success,
    };
  }),
  requestAPI: makeAction(
    'REQUEST_LOADING',
    (loadingStatus) => loadingStatus,
  ),
};

const reducerBuilder = makeReducer<RegisterState>({
  email: '',
  isEmailValid: false,
  token: '',
  isTokenValid: false,
  regCode: '',
  isRegCodeValid: false,
  currentStep: 'step1',
  loading: {
    emailValid: { status: false, error: null },
    regCodeValid: { status: false, error: null },
    tokenValid: { status: false, error: null },
    createAccount: { status: false, error: null },
  },
});
reducerBuilder.add(
  'REQUEST_LOADING',
  (
    state: RegisterState,
    action: {
      type: any;
      payload: {
        section: keyof loading;
        status: boolean;
        error: any;
      };
    },
  ) => {
    return produce(state, (draft) => {
      draft.loading[action.payload.section] = {
        status: action.payload.status,
        error: action.payload.error,
      };
    });
  },
);

reducerBuilder.add(
  'REQUEST_ACTION_LOADING',
  (
    state: RegisterState,
    action: {
      type: any;
      payload: {
        loading: {
          section: keyof loading;
          status: boolean;
          error: any;
        };
      };
    },
  ) => {
    return produce(state, (draft) => {
      draft.loading[action.payload.loading.section] = {
        status: action.payload.loading.status,
        error: action.payload.loading.error,
      };
    });
  },
);

reducerBuilder.add(
  actions.checkEmailValid.fetchType(),
  (state: RegisterState, action: { type: any; payload?: any }) => {
    let { payload } = action;
    return produce(state, (draft) => {
      if (payload.isEmailValid) {
        (draft.email = payload.email), (draft.currentStep = 'step2');
        draft.isTokenValid = true;
      }
      return draft;
    });
  },
);
reducerBuilder.add(
  actions.checkTokenValid.fetchType(),
  (state: RegisterState, action: { type: string; payload: any }) => {
    let { payload } = action;
    return produce(state, (draft) => {
      if (payload.isTokenValid) {
        draft.currentStep = 'step3';
        draft.isTokenValid = true;
      }
      return draft;
    });
  },
);
reducerBuilder.add(
  actions.checkRegCodeValid.fetchType(),
  (state: RegisterState, action: { type: string; payload: any }) => {
    let { payload } = action;
    return produce(state, (draft) => {
      if (payload.isRegCodeValid) {
        draft.regCode = payload.regCode;
        draft.currentStep = 'step4';
        draft.isRegCodeValid = true;
      }
      return draft;
    });
  },
);

reducerBuilder.add(
  actions.createAccount.fetchType(),
  (state: RegisterState, action: { type: string; payload: any }) => {
    let { payload } = action;
    return produce(state, (draft) => {
      if (payload.isSuccess) {
        draft.currentStep = 'final';
      }
      return draft;
    });
  },
);

export const reducer = reducerBuilder.process;

export const rootSaga = function* sagas() {
  yield eff.all([
    actions.checkEmailValid.saga(),
    actions.checkTokenValid.saga(),
    actions.checkRegCodeValid.saga(),
    actions.createAccount.saga(),
  ]);
};
