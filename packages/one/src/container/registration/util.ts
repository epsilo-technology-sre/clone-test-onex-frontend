import qs from 'qs';
import * as eff from 'redux-saga/effects';

export function fetchJson(
  url: string,
  params: { query?: any; data?: any; form?: any },
  method: 'POST' | 'GET' = 'GET',
) {
  let finurl = url;
  if (params.query) finurl = url + '?' + qs.stringify(params.query);

  return fetch(finurl, {
    method,
    body:
      method === 'POST' && params.data
        ? JSON.stringify(params.data)
        : undefined,
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then((r) => {
      if (r.status !== 200) {
        return r
          .json()
          .then((rj) => {
            throw new FetchError(r.status, {
              url,
              message: [].concat(rj.message).join('\n'),
            });
          })
          .catch((err) => {
            if (err instanceof FetchError) {
              throw err;
            } else {
              throw new FetchError(r.status, {
                url,
              });
            }
          });
      }
      return r.json();
    })
    .then((r) => {
      if (r.success === false) {
        throw new FetchError(r.status, {
          url,
          message: [].concat(r.message).join('\n'),
        });
      }
      return r;
    });
}

export class FetchError extends Error {
  status: number;
  details: any;
  constructor(status: number, details: any) {
    super(`error fetching ${status} - ${details.url}`);

    this.status = status;
    this.details = details;
  }
}

type enhancedAction<T extends any, A extends T[], R> = {
  (...args: A): { type: string; payload: R } | any;
  type: () => string;
  fetchType: () => string;
};

export const makeAction = <T extends any, A extends T[], R>(
  actname: string,
  fn: (...args: A) => R,
): enhancedAction<T, A, R> => {
  function action(...args: A) {
    return { type: actname, payload: fn(...args) };
  }
  action.type = () => actname;
  action.fetchType = () => actname + '_FETCH';

  return action;
};

type enSagaAction<T extends any, A extends T[], R> = enhancedAction<
  T,
  A,
  R
> & { saga: any };

export function actionWithRequest<T extends any, A extends T[], R>(
  actname: string,
  fun: (...args: A) => R,
): enSagaAction<T, A, R> {
  let action = makeAction(
    actname,
    (...args: A): A => {
      return args;
    },
  );
  function fn(...a: A) {
    return action(...a);
  }

  function* request(act: { type: string; payload: A }) {
    yield eff.put({
      type: 'REQUEST_ACTION_LOADING',
      payload: {
        loading: {
          section: action.type(),
          status: true,
        },
      },
    });
    try {
      let result = yield eff.call(fun, ...act.payload);
      yield eff.put({
        type: action.fetchType(),
        payload: result,
      })
      yield eff.put({
        type: 'REQUEST_ACTION_LOADING',
        payload: {
          loading: {
            section: action.type(),
            status: false,
          },
        },
      });
    } catch (error) {
      yield eff.put({
        type: 'REQUEST_ACTION_LOADING',
        payload: {
          loading: {
            section: action.type(),
            status: false,
            error,
          },
        },
      });
    }
  }

  function* saga() {
    yield eff.takeLatest(action.type(), request);
  }

  fn.type = action.type;
  fn.fetchType = action.fetchType;
  fn.saga = saga;

  return fn;
}

export function makeReducer<T>(initState: T): any {
  let map: { [key: string]: (...args: any) => any } = {};

  function add(
    action: string,
    stateUpdate: (
      state: T,
      action: { type: string; payload?: any },
    ) => T,
  ) {
    map[action] = stateUpdate;
  }

  function process(
    state: T = initState,
    action: { type: string; payload?: any },
  ) {
    if (map[action.type]) {
      return map[action.type](state, action);
    } else {
      return state;
    }
  }

  return {
    process,
    add,
  };
}
