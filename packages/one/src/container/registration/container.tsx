import React, { useState } from 'react';
import { get } from 'lodash';
import { useDispatch, useSelector } from 'react-redux';
import { actions } from './redux';
import { RegStep as RegStep1 } from '@ep/one/src/components/registration/step1';
import { RegStep as RegStep2 } from '@ep/one/src/components/registration/step2';
import { RegStep as RegStep3 } from '@ep/one/src/components/registration/step3';
import { RegStep as RegStep4 } from '@ep/one/src/components/registration/step4';
import MuiAlert from '@material-ui/lab/Alert';
import { Snackbar } from '@material-ui/core';
import { useHistory } from 'react-router';

export default function ContainerRegister({ urlLogin = '#' }) {
  const history = useHistory();

  let hUrlLogin = history.createHref({ pathname: urlLogin });

  const currentStep = useSelector((state: any) => {
    return state.currentStep;
  });

  const step1 = useSelector((state: any) => {
    return {
      email: state?.email,
      isEmailValid: state?.isEmailValid,
      isLoading: get(
        state,
        ['loading', actions.checkEmailValid.type(), 'status'],
        false,
      ),
      error: get(state, [
        'loading',
        actions.checkEmailValid.type(),
        'error',
        'details',
        'message',
      ]),
    };
  });
  const step2 = useSelector((state: any) => {
    return {
      isLoading: get(state, [
        'loading',
        actions.checkTokenValid.type(),
        'status',
      ]),
      error: get(state, [
        'loading',
        actions.checkTokenValid.type(),
        'error',
        'details',
        'message',
      ]),
    };
  });
  const step3 = useSelector((state: any) => {
    return {
      regCode: state?.regCode,
      isRegCodeValid: state?.isRegCodeValid,
      isLoading: get(state, [
        'loading',
        actions.checkRegCodeValid.type(),
        'status',
      ]),
      error: get(state, [
        'loading',
        actions.checkRegCodeValid.type(),
        'error',
        'details',
        'message',
      ]),
    };
  });
  const step4 = useSelector((state: any) => {
    return {
      isLoading: get(state, [
        'loading',
        actions.createAccount.type(),
        'status',
      ]),
      error: get(state, [
        'loading',
        actions.createAccount.type(),
        'errors',
        'details',
        'message',
      ]),
    };
  });
  const dispatch = useDispatch();

  const [message, setMessage] = useState({
    content: '',
    display: false,
    type: 'info',
  });

  function onValidateEmail(values: { email: string }) {
    dispatch(actions.checkEmailValid({ email: values.email }));
  }

  function onValidateToken(values: { code: string }) {
    dispatch(
      actions.checkTokenValid({
        email: step1.email,
        token: values.code,
      }),
    );
  }

  function onValidateRegCode(values: { code: string }) {
    dispatch(
      actions.checkRegCodeValid({
        email: step1.email,
        code: values.code,
      }),
    );
  }

  function onCreateAccount(values: {
    firstName: string;
    lastName: string;
    phone: string;
    password: string;
  }) {
    dispatch(
      actions.createAccount({
        email: step1.email,
        regCode: step3.regCode,
        ...values,
      }),
    );
  }

  let content = null;
  switch (currentStep) {
    case 'step1': {
      content = (
        <RegStep1
          onSubmit={onValidateEmail}
          upstreamError={step1.error}
          isLoading={step1.isLoading}
          urlLogin={hUrlLogin}
        />
      );
      break;
    }
    case 'step2': {
      content = (
        <RegStep2
          email={step1.email}
          onSubmit={onValidateToken}
          onResendCode={() => {
            onValidateEmail({ email: step1.email });
            setMessage({
              content:
                'New code is on the way. Please recheck your email.',
              display: true,
              type: 'info',
            });
          }}
          isLoading={step2.isLoading}
          upstreamError={step2.error}
        />
      );
      break;
    }
    case 'step3': {
      content = (
        <RegStep3
          onSubmit={onValidateRegCode}
          isLoading={step3.isLoading}
          upstreamError={step3.error}
        />
      );
      break;
    }
    case 'step4': {
      content = (
        <RegStep4
          email={step1.email}
          onSubmit={onCreateAccount}
          isLoading={step4.isLoading}
          upstreamError={step4.error}
        />
      );
      break;
    }
    case 'final': {
      window.location.href = '/';
    }
  }

  return (
    <React.Fragment>
      {content}
      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
        open={message.display}
        autoHideDuration={6000}
        onClose={() => {
          setMessage((state) => ({ ...state, display: false }));
        }}
      >
        <MuiAlert
          onClose={() =>
            setMessage((state) => ({ ...state, display: false }))
          }
          severity="info"
        >
          {message.content}
        </MuiAlert>
      </Snackbar>
    </React.Fragment>
  );
}
