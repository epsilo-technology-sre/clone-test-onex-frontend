import {
  InternalPage,
  OnePageContext,
} from '@ep/one/src/components/internal-page/internal-page';
import { createMuiTheme, ThemeProvider } from '@material-ui/core';
import React from 'react';
import { Route, useLocation, useRouteMatch } from 'react-router';
import { internalMenu } from '../menu-list';
import { MenuContext } from '../hooks/use-menu';
import { AuthContext, useAuth } from '../hooks';
import { trackError } from '../utils/mixpanel';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#ED5C10',
    },
    secondary: {
      main: '#0BA373',
    },
  },
  typography: {
    fontSize: 14,
  },
});

export class PageErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    window.requestAnimationFrame(() => trackError(error));
  }

  render() {
    if (this.state.hasError) {
      return (
        <h1>
          Oops, something was wrong.{' '}
          <a href="#" onClick={() => window.location.reload()}>
            Click here
          </a>{' '}
          to refresh the page
        </h1>
      );
    }

    return this.props.children;
  }
}

export default function ContainerInternalPage({
  children,
}: {
  children: JSX.Element;
}) {
  const { userSettings, logout } = useAuth();
  const match = useRouteMatch();
  const { canAccess } = React.useContext(AuthContext);
  const navigation = React.useMemo(() => {
    return internalMenu(canAccess);
  }, []);
  const [customLeftPanel, setCustomLeftPanel] = React.useState(null);

  let index1 = -1;
  let index2 = -1;
  let index3 = -1;

  index1 = navigation.findIndex(
    (i) => i.access !== false && match.path.indexOf(i.url) === 0,
  );
  if (index1 > -1) {
    index2 = navigation[index1].children.findIndex(
      (i) => i.access !== false && match.path.indexOf(i.url) === 0,
    );
  }
  if (index2 > -1) {
    index3 = (
      navigation[index1].children[index2].children || []
    ).findIndex(
      (i) => i.access !== false && match.path.indexOf(i.url) === 0,
    );
  }

  return (
    <ThemeProvider theme={theme}>
      <MenuContext.Provider
        value={{
          customLeftPanel,
          setCustomLeftPanel,
          resetCustomLeftPanel: () => setCustomLeftPanel(null),
        }}
      >
        <OnePageContext.Provider
          value={{
            userFullName: userSettings.profile.userName,
            userEmail: userSettings.profile.userEmail,
            onLogout: () => {
              logout();
            },
          }}
        >
          <InternalPage
            navigation={navigation}
            activePath={[index1, index2, index3]}
          >
            <PageErrorBoundary>{children}</PageErrorBoundary>
          </InternalPage>
        </OnePageContext.Provider>
      </MenuContext.Provider>
    </ThemeProvider>
  );
}
