import { produce } from 'immer';
import { union } from 'lodash';
import moment from 'moment';
import { LOCAL_STORAGE_KEY } from '../../constants';
import { sortList } from '../../utils';
import { actions } from './redux';

type loading = {
  [key: string]: {
    status: boolean;
    error: any;
  };
};
type metricItem = {
  metricId: string;
  title: string;
  value: number;
  percentChange: number;
  isPercent: boolean;
  previousPeriodRange: {
    from: number;
    to: number;
  };
};

type series = {
  metricId: string;
  title: string;
  color: string;
  data: number[];
  dates: number[];
  unitFormat: string;
};

export type DashboardState = {
  availableFilter: {
    channels: any[];
    features: any[];
    countries: any[];
    shops: any[];
  };
  optionFilter: {
    hideChannel: boolean;
    channels?: any[];
    features?: any[];
    countries?: any[];
    shops?: any[];
  };
  filter: {
    from: string;
    to: string;
    channels?: any[];
    features?: any[];
    countries?: any[];
    shops?: any[];
    updateCount: number;
    currency: string;
  };
  selectedMetrics: string[] | null;
  selectedCharts: string[] | null;
  metrics: metricItem[] | null;
  charts: series[] | null;
  chartDates: number[] | null;
  loading: loading;
  availColors: string[];
  colors: { [key: string]: string };
};

const prevSelection = window.localStorage.getItem(LOCAL_STORAGE_KEY.DASHBOARD_CURRENCY);

export const initState: DashboardState = {
  availableFilter: {
    channels: [],
    features: [],
    countries: [],
    shops: [],
  },
  optionFilter: {
    hideChannel: false,
    channels: [],
    features: [],
    countries: [],
    shops: [],
  },
  filter: {
    from: moment().subtract(60, 'day').format('YYYY-MM-DD'),
    to: moment().format('YYYY-MM-DD'),
    channels: [],
    shops: [],
    features: [],
    updateCount: 0,
    currency: prevSelection || 'USD',
  },
  selectedMetrics: null,
  selectedCharts: null,
  metrics: null,
  charts: null,
  chartDates: null,
  loading: {},
  availColors: ['#EE6923', '#0BA373', '#147DD2', '#485764'],
  colors: {},
};

export function reducer(
  state: DashboardState = initState,
  action: { type: string; payload: any },
) {
  const { payload } = action;
  switch (action.type) {
    case 'REQUEST_ACTION_LOADING': {
      state = produce(state, (draft) => {
        draft.loading[action.payload.loading.section] = {
          status: action.payload.loading.status,
          error: action.payload.loading.error,
        };
      });
      break;
    }
    case actions.loadMetrics.fetchType(): {
      state = produce(state, (draft) => {
        draft.metrics = action.payload.metrics;
      });
      break;
    }
    case actions.loadShops.fetchType(): {
      state = produce(state, (draft) => {
        const { data } = action.payload;

        let channelOptions = data.reduce((acc, s) => {
          if (acc.find((s1) => s1.id === s.channel_code)) {
            return acc;
          } else {
            return acc.concat({
              id: s.channel_code,
              text: s.channel_name,
            });
          }
        }, []);

        let featureOptions = data.reduce((acc, s) => {
          let newFeatures: any[] = [];
          s.features.forEach((feature: any) => {
            const isExist = acc.some((s1: any) => s1.id === feature.feature_code);
            if (!isExist) {
              newFeatures = newFeatures.concat({
                id: feature.feature_code,
                text: feature.feature_name,
                channelCode: s.channel_code,
              })
            }
          });
          return acc.concat(newFeatures);
        }, []);

        let countryOptions = data.reduce((acc, s) => {
          const exist = acc.find((s1) => s1.id === s.country_code);
          if (exist) {
            return acc.map((i) => {
              if (i.id === exist.id) {
                return {
                  ...i,
                  featureCodes: union(exist.featureCodes.concat(s.features.map((i) =>i.feature_code))),
                }
              } else {
                return i;
              }
            })
          } else {
            return acc.concat({
              id: s.country_code,
              text: s.country_name,
              featureCodes: s.features.map((i) =>i.feature_code),
            });
          }
        }, []);

        let shopOptions = data.map((item) => ({
          id: item.shop_eid,
          text: `${item.country_code} / ${item.shop_name}`,
          countryCode: item.country_code,
          featureCodes: item.features.map((i) => i.feature_code),
          channelCode: item.channel_code,
        }));

        channelOptions = sortList(channelOptions, 'text');
        featureOptions = sortList(featureOptions, 'text');
        countryOptions = sortList(countryOptions, 'text');
        shopOptions = sortList(shopOptions, 'text');

        draft.availableFilter = {
          channels: channelOptions,
          features: featureOptions,
          countries: countryOptions,
          shops: shopOptions,
        };

        draft.optionFilter = {
          hideChannel: state.optionFilter.hideChannel,
          channels: channelOptions,
          features: featureOptions,
          countries: countryOptions,
          shops: shopOptions,
        };

        draft.filter = {
          ...state.filter,
          channels: channelOptions,
          features: featureOptions,
          countries: countryOptions,
          shops: shopOptions.slice(0, 1),
        };
      });
      break;
    }
    case actions.updateFilterDateRange.type(): {
      state = produce(state, (draft) => {
        const { dateRange } = action.payload;
        draft.filter.from = dateRange.from;
        draft.filter.to = dateRange.to;
      });
      break;
    }
    case actions.updateFilterChannel.type(): {
      state = produce(state, (draft) => {
        const { channels } = action.payload;
        const featureOptions = state.availableFilter.features.reduce(
          (acc, elem) => {
            if (
              channels.some(
                (item: any) => item.id === elem.channelCode,
              )
            ) {
              return [...acc, elem];
            } else {
              return acc;
            }
          },
          [],
        );
        const channelIds = new Set(channels.map((i) => i.id));
        const featureIds = new Set(featureOptions.map((i) => i.id));

        const countryOptions = state.availableFilter.countries.filter(
          (c) => c.featureCodes.some((f) => featureIds.has(f))
        );
        
        const countryIds = new Set(countryOptions.map((i) => i.id));

        const shopOptions = state.availableFilter.shops.filter((s) => {
          const hasSelectedChannels = channelIds.has(s.channelCode);
          const hasSelectedFeatures = s.featureCodes.some((f) => featureIds.has(f));
          const hasSelectedCountries = countryIds.has(s.countryCode);
          return hasSelectedChannels && hasSelectedFeatures && hasSelectedCountries;
        })

        draft.optionFilter.features = featureOptions;
        draft.optionFilter.countries = countryOptions;
        draft.optionFilter.shops = shopOptions;

        draft.filter.channels = channels;
        draft.filter.features = featureOptions;
        draft.filter.countries = countryOptions;
        draft.filter.shops = shopOptions;
      });
      break;
    }
    case actions.updateFilterFeature.type(): {
      state = produce(state, (draft) => {
        const { features } = action.payload;
        const channelIds = new Set(state.filter.channels.map((i) => i.id));
        const featureIds = new Set(features.map((i) => i.id));

        const countryOptions = state.availableFilter.countries.filter(
          (c) => c.featureCodes.some((f) => featureIds.has(f))
        );

        const countryIds = new Set(countryOptions.map((i) => i.id));

        const shopOptions = state.availableFilter.shops.filter((s) => {
          const hasSelectedChannels = channelIds.has(s.channelCode);
          const hasSelectedFeatures = s.featureCodes.some((f) => featureIds.has(f));
          const hasSelectedCountries = countryIds.has(s.countryCode);
          return hasSelectedChannels && hasSelectedFeatures && hasSelectedCountries;
        })

        draft.optionFilter.countries = countryOptions;
        draft.optionFilter.shops = shopOptions;

        draft.filter.features = features;
        draft.filter.countries = countryOptions;
        draft.filter.shops = shopOptions;
      });
      break;
    }
    case actions.updateFilterCountry.type(): {
      state = produce(state, (draft) => {
        const { countries } = action.payload;

        const channelIds = new Set(state.filter.channels.map((i) => i.id));
        const featureIds = new Set(state.filter.features.map((i) => i.id));
        const countryIds = new Set(countries.map((i) => i.id));

        const shopOptions = state.availableFilter.shops.filter((s) => {
          const hasSelectedChannels = channelIds.has(s.channelCode);
          const hasSelectedFeatures = s.featureCodes.some((f) => featureIds.has(f));
          const hasSelectedCountries = countryIds.has(s.countryCode);
          return hasSelectedChannels && hasSelectedFeatures && hasSelectedCountries;
        })

        draft.optionFilter.shops = shopOptions;

        draft.filter.countries = countries;
        draft.filter.shops = shopOptions;
      });
      break;
    }
    case actions.updateFilterShop.type(): {
      state = produce(state, (draft) => {
        const { shops } = action.payload;
        draft.filter.shops = shops;
      });
      break;
    }
    case actions.updateGlobalFilter.type(): {
      return produce(state, (draft) => {
        draft.filter.updateCount = state.filter.updateCount + 1;
      });
    }
    case actions.updateFilterCurrency.type(): {
      state = produce(state, (draft) => {
        draft.filter.currency = action.payload.currency;
        localStorage.setItem(LOCAL_STORAGE_KEY.DASHBOARD_CURRENCY, action.payload.currency);
      });
      break;
    }
    case actions.loadSettings.fetchType(): {
      state = produce(state, (draft) => {
        draft.selectedCharts = payload.settings.selectedCharts;
        draft.selectedMetrics = payload.settings.selectedMetrics;
        let { availColors, colors } = colorAssign({
          metricIds: payload.settings.selectedCharts,
          isRemoved: false,
          colors: state.colors,
          availColors: state.availColors,
        });
        draft.colors = colors;
        draft.availColors = availColors;
        draft.filter.currency = window.localStorage.getItem(
          LOCAL_STORAGE_KEY.DASHBOARD_CURRENCY,
        ) || 'USD';
      });
      break;
    }
    case actions.loadCharts.fetchType(): {
      state = produce(state, (draft) => {
        const start = moment
          .utc(state.filter.from, 'YYYY-MM-DD')
          .startOf('day');
        const end = moment
          .utc(state.filter.to, 'YYYY-MM-DD')
          .startOf('day');

        let dateSeeds: number[] = [];
        while (!start.isAfter(end)) {
          const tms = start.valueOf();
          dateSeeds = dateSeeds.concat(tms);
          start.add(1, 'day');
        }

        draft.charts = payload.charts.map((i: any, index: number) => {
          const m = (state.metrics || []).find(
            (m) => m.metricId === i.metricId,
          );
          return {
            ...i,
            data: i.data
              .filter((i, index) => payload.dates[index])
              .map((i, index) => [payload.dates[index], i]),
            title: m?.title,
            isPercent: m?.isPercent,
            currency: m?.currency,
          };
        });
        draft.chartDates = payload.dates;
      });
      break;
    }
    case actions.removeMetric.type(): {
      state = produce(state, (draft) => {
        draft.selectedMetrics = state.selectedMetrics!.filter(
          (i) => i !== payload.metricId,
        );
        draft.selectedCharts = state.selectedCharts!.filter(
          (i) => i !== payload.metricId,
        );

        draft.charts = state.charts!.filter(
          (i) => i.metricId !== payload.metricId,
        );
        let { availColors, colors } = colorAssign({
          metricIds: [payload.metricId],
          isRemoved: true,
          colors: state.colors,
          availColors: state.availColors,
        });
        draft.colors = colors;
        draft.availColors = availColors;
      });
      break;
    }
    case actions.toggleHighlightMetric.type(): {
      state = produce(state, (draft) => {
        if (state.selectedCharts?.includes(payload.metricId)) {
          draft.selectedCharts = state.selectedCharts.filter(
            (i) => i !== payload.metricId,
          );
          draft.charts = state.charts!.filter(
            (i) => i.metricId !== payload.metricId,
          );
          let { availColors, colors } = colorAssign({
            metricIds: [payload.metricId],
            isRemoved: true,
            colors: state.colors,
            availColors: state.availColors,
          });
          draft.colors = colors;
          draft.availColors = availColors;
        } else {
          draft.selectedCharts = (state.selectedCharts || []).concat(
            payload.metricId,
          );
          let { availColors, colors } = colorAssign({
            metricIds: [payload.metricId],
            isRemoved: false,
            colors: state.colors,
            availColors: state.availColors,
          });
          draft.colors = colors;
          draft.availColors = availColors;
        }
      });
      break;
    }
    case actions.setSelectedMetric.type(): {
      state = produce(state, (draft) => {
        draft.selectedMetrics = payload.metricIds;
        draft.selectedCharts = state.selectedCharts!.filter((i) =>
          payload.metricIds.includes(i),
        );
        draft.charts = (state.charts! || []).filter((i) =>
          payload.metricIds.includes(i.metricId),
        );
        let { availColors, colors } = colorAssign({
          metricIds: (state.selectedCharts || []).filter(
            (c) => payload.metricIds.indexOf(c) === -1,
          ),
          isRemoved: true,
          colors: state.colors,
          availColors: state.availColors,
        });
        draft.colors = colors;
        draft.availColors = availColors;
      });
      break;
    }
  }
  return state;
}

function colorAssign({
  metricIds,
  isRemoved,
  colors,
  availColors,
}: {
  metricIds: string[];
  isRemoved: boolean;
  colors: DashboardState['colors'];
  availColors: DashboardState['availColors'];
}) {
  if (isRemoved) {
    const freeColors = metricIds.map((i) => colors[i]);
    console.info({ metricIds, colors });
    return {
      colors: Object.keys(colors).reduce((acc, id) => {
        if (metricIds.indexOf(id) === -1) {
          return { ...acc, [id]: colors[id] };
        }
        return acc;
      }, {}),
      availColors: availColors.concat(freeColors),
    };
  } else {
    return {
      colors: metricIds.reduce((accum, id, index) => {
        return {
          ...accum,
          [id]: availColors[index],
        };
      }, colors),
      availColors: availColors.slice(metricIds.length),
    };
  }
}
