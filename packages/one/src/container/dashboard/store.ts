import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import { applyMiddleware, createStore } from 'redux';
import { reducer, rootSaga } from './redux';
import { initState as storeInitState } from './reducer';

export function makeStore(
  initState = storeInitState,
  name = 'dashboard',
) {
  const sagaMiddleware = createSagaMiddleware();
  let composeEnhancers = composeWithDevTools({
    name,
  });

  function quickCheck() {
    const initState = {
      email: 'phuc.phan@epsilo.io',
      isEmailValid: false,
      token: '',
      isTokenValid: false,
      regCode: 'RXSBGREFF',
      isRegCodeValid: false,
      currentStep: 'step4',
      loading: {
        emailValid: {
          status: true,
        },
        regCodeValid: {
          status: false,
          error: null,
        },
        tokenValid: {
          status: false,
          error: null,
        },
      },
    };
  }
  // quickCheck();

  let store = createStore(
    reducer,
    initState,
    composeEnhancers(applyMiddleware(sagaMiddleware)),
  );

  sagaMiddleware.run(rootSaga);

  return store;
}
