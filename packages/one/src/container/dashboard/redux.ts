import { API_URL } from '@ep/one/global';
import * as efetch from '@ep/one/src/utils/fetch';
import moment from 'moment';
import * as eff from 'redux-saga/effects';
import { LOCAL_STORAGE_KEY } from '../../constants';
import { actionWithRequest, makeAction } from '../../utils/redux';
import { DashboardState, reducer } from './reducer';

export { reducer };
export { deveriedActions as actions };

const ep = {
  GET_SETTINGS: () =>
    API_URL + '/api/v1/dashboard/get-chart-settings',
  GET_SHOPS: () => API_URL + '/api/v1/user/shops',
  SAVE_METRICS: () =>
    API_URL + '/api/v1/dashboard/save-chart-settings',
  GET_METRICS: () => API_URL + '/api/v1/dashboard/metrics',
  GET_CHARTS: () => API_URL + '/api/v1/dashboard/charts',
};

const actions = {
  loadShops: actionWithRequest({
    actname: 'LOAD_SHOPS',
    fun: function* loadShops() {
      console.info('loading shops...');
      let channels = yield eff.select((state: DashboardState) => {
        return state.filter.channels;
      });
      const rs = yield eff.call(() => {
        return efetch.get(ep.GET_SHOPS(), {
          channel_code:
            channels.length > 0
              ? channels.map((c) => c.id)
              : undefined,
        });
      });
      return { data: rs.data };
    },
  }),
  loadSettings: actionWithRequest({
    actname: 'LOAD_SETTINGS',
    fun: function* loadSettings() {
      console.info('loading settings...');
      const rs = yield eff.call(() => {
        return efetch.get(ep.GET_SETTINGS());
      });

      return {
        settings: {
          selectedMetrics: rs.data.displayed_widgets,
          selectedCharts: rs.data.selected_widgets,
        },
      };
    },
  }),
  updateSettings: actionWithRequest({
    actname: 'UPDATE_SETTINGS',
    fun: function* updateSettings(payload: { metricIds: string[] }) {
      console.info(updateSettings);
    },
  }),
  loadMetrics: actionWithRequest({
    actname: 'LOAD_METRIC',
    fun: function* loadMetrics(payload: {
      from: string;
      to: string;
      channels?: number[];
      features?: string[];
      countries?: string[];
      shops?: number[];
      currency?: string;
    }) {
      const rs = yield eff.call(() => {
        return efetch.get(ep.GET_METRICS(), {
          from: payload.from,
          to: payload.to,
          features: payload.features,
          shop_eids: payload.shops,
          currency: payload.currency,
        });
      });

      return {
        metrics: rs.data.map((i: any) => ({
          metricId: i.code,
          currency: i.unit !== '%' ? i.unit : '',
          currencySymbol: i.unit ? i.unit : '',
          title: i.title,
          value: i.value,
          valueCompare: i.value_compare,
          percentChange: i.percent_change,
          percentChangePositive: i.percent_positive,
          isPercent: i.unit === '%',
          previousPeriodRange: i.previous_period_range,
          definition: i.definition,
        })),
      };
    },
  }),
  loadCharts: actionWithRequest({
    actname: 'LOAD_CHARTS',
    fun: function* loadMetrics(payload: {
      from: string;
      to: string;
      channels?: number[];
      features?: string[];
      countries?: string[];
      shops?: number[];
      metricIds: string[];
      currency: string;
    }) {
      let charts = [];
      for (let metricId of payload.metricIds) {
        let result = yield eff.call(() => {
          return efetch.get(ep.GET_CHARTS(), {
            from: payload.from,
            to: payload.to,
            metrics: metricId,
            features: payload.features,
            shop_eids: payload.shops,
            currency: payload.currency,
            timing: 'daily'
          });
        });

        const { data } = result;

        const chartData = {
          metricId: metricId,
          title: data.label,
          data: data[metricId],
          dates: data.date.map((i: string) => {
            return moment
              .utc(i, 'YYYY-MM-DD HH:mm:ss')
              .startOf('day')
              .valueOf();
          }),
        };

        if (!chartData.data) {
          chartData.data = chartData.dates.map((i: number) => null);
        } else {
          chartData.data = chartData.data.map((i: string) =>
            i === null ? null : Number(i),
          );
        }

        charts.push(chartData);
      }

      return {
        charts: charts,
        dates: charts[0].dates,
      };
    },
  }),
  updateFilterDateRange: makeAction({
    actname: 'UPDATE_FILTER_DATE_RANGE',
    fun: function ({ dateRange }) {
      return {
        dateRange,
      };
    },
  }),
  updateFilterChannel: makeAction({
    actname: 'UPDATE_FILTER_CHANNEL',
    fun: function ({ channels }) {
      return {
        channels,
      };
    },
  }),
  updateFilterFeature: makeAction({
    actname: 'UPDATE_FILTER_FEATURE',
    fun: function ({ features }) {
      return {
        features,
      };
    },
  }),
  updateFilterCountry: makeAction({
    actname: 'UPDATE_FILTER_COUNTRY',
    fun: function ({ countries }) {
      return {
        countries,
      };
    },
  }),
  updateFilterShop: makeAction({
    actname: 'UPDATE_FILTER_SHOP',
    fun: function ({ shops }) {
      return {
        shops,
      };
    },
  }),
  updateGlobalFilter: makeAction({
    actname: 'UPDATE_GLOBAL_FILTERS',
    fun: function () {
      return {};
    },
  }),
  updateFilterCurrency: makeAction({
    actname: 'UPDATE_FILTER_CURRENCY',
    fun: function ({ currency }) {
      return { currency };
    },
  }),
  removeMetric: makeAction({
    actname: 'METRIC_REMOVE',
    fun: function ({ metricId }) {
      return {
        metricId,
      };
    },
  }),
  toggleHighlightMetric: makeAction({
    actname: 'METRIC_HIGHTLIGHT',
    fun: function ({ metricId }) {
      return { metricId };
    },
  }),
  setSelectedMetric: makeAction({
    actname: 'METRICS_SET_SELECTED',
    fun: function ({ metricIds }) {
      return {
        metricIds,
      };
    },
  }),
};

const deveriedActions = {
  ...actions,
  init: actionWithRequest({
    actname: 'INIT',
    fun: function* init(payload: {
      from: string;
      to: string;
      shops: number[];
      features: string[];
    }) {
      const prevSelection = window.localStorage.getItem(
        LOCAL_STORAGE_KEY.DASHBOARD_CURRENCY,
      );
      yield eff.put({ type: actions.loadSettings.type() });
      yield eff.put({
        type: actions.loadMetrics.type(),
        payload: [
          {
            from: payload.from,
            to: payload.to,
            features: payload.features,
            shops: payload.shops,
            currency: prevSelection || 'USD',
          },
        ],
      });
    },
  }),
  saveSettings: actionWithRequest({
    actname: 'SAVE_SETTINGS',
    fun: function* (payload: {
      selectedMetrics: string[];
      selectedCharts: string[];
    }) {
      yield eff.put(
        actions.setSelectedMetric({
          metricIds: payload.selectedMetrics,
        }),
      );
      const rs = yield eff.call(() =>
        efetch.post(ep.SAVE_METRICS(), {
          displayed_widgets: payload.selectedMetrics,
          selected_widgets: payload.selectedCharts,
        }),
      );
    },
  }),
};

type loading = {
  [key: string]: {
    status: boolean;
    error: any;
  };
};
type metricItem = {
  metricId: string;
  title: string;
  value: number;
  percentChange: number;
  isPercent: boolean;
  previousPeriodRange: {
    from: number;
    to: number;
  };
};

type series = {
  metricId: string;
  title: string;
  color: string;
  data: number[];
  dates: number[];
  unitFormat: string;
};

function* reloadCharts() {
  const { filter, selectedCharts } = yield eff.select(
    (state: DashboardState) => ({
      filter: state.filter,
      selectedCharts: state.selectedCharts,
    }),
  );
  let options = {
    from: filter.from,
    to: filter.to,
    channels: filter.channel,
    features: filter.features.map((f) => f.id),
    countries: filter.features.map((c) => c.id),
    shops: filter.shops.map((s) => s.id),
    currency: filter.currency,
  };
  yield eff.put(actions.loadMetrics(options));
  yield eff.take(actions.loadMetrics.fetchType());
  yield eff.put(
    actions.loadCharts({ ...options, metricIds: selectedCharts }),
  );
}

function* onChangeFilter() {
  yield eff.takeLatest(
    deveriedActions.updateFilterDateRange.type(),
    reloadCharts,
  );
  yield eff.takeLatest(
    deveriedActions.updateGlobalFilter.type(),
    reloadCharts,
  );
  yield eff.takeLatest(
    deveriedActions.updateFilterCurrency.type(),
    function* () {
      const filter = yield eff.select(
        (state: DashboardState) => state.filter,
      );
      yield eff.put(
        actions.loadMetrics({
          ...filter,
          from: filter.from,
          to: filter.to,
        }),
      );

      yield reloadCharts();
    },
  );
}

export function* rootSaga() {
  yield eff.all([
    actions.loadMetrics.saga(),
    actions.loadCharts.saga(),
    actions.loadShops.saga(),
    actions.loadSettings.saga(),
    deveriedActions.init.saga(),
    deveriedActions.saveSettings.saga(),
    onChangeFilter(),
  ]);
}
