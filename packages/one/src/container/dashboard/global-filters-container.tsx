import { Box, Button, CircularProgress } from '@material-ui/core';
import { get } from 'lodash';
import moment from 'moment';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import { ONE_EP } from '../../../endpoint';
import { GlobalFilters } from '../../components/global-filters';
import { ReactComponent as DownloadIcon } from '../../images/download.svg';
import { request } from '../../utils';
import { DashboardState } from './reducer';
import { actions } from './redux';

export function GlobalFiltersContainer() {
  const dispatch = useDispatch();
  const [loading, setLoading] = React.useState(false);

  const {
    channels,
    features,
    countries,
    shops,
    selectedDateRange,
    selectedChannels,
    selectedFeatures,
    selectedCountries,
    selectedShops,
    hideChannel,
  } = useSelector((state: DashboardState) => {
    const startDate = get(state, 'filter.from', '');
    const endDate = get(state, 'filter.to', '');
    return {
      hideChannel: get(state, 'optionFilter.hideChannel', false),
      channels: get(state, 'optionFilter.channels', []),
      features: get(state, 'optionFilter.features', []),
      countries: get(state, 'optionFilter.countries', []),
      shops: get(state, 'optionFilter.shops', []),
      selectedChannels: get(state, 'filter.channels', []),
      selectedFeatures: get(state, 'filter.features', []),
      selectedCountries: get(state, 'filter.countries', []),
      selectedShops: get(state, 'filter.shops', []),
      selectedDateRange: { from: startDate, to: endDate },
    };
  });

  useEffect(() => {
    dispatch(actions.loadShops());
  }, []);

  const handleChangeDateRange = ({ startDate, endDate }: any) => {
    console.info(startDate, endDate);
    const dateRange = {
      from: moment(startDate).format('YYYY-MM-DD'),
      to: moment(endDate).format('YYYY-MM-DD'),
    };
    dispatch(actions.updateFilterDateRange({ dateRange }));
  };

  const handleChangeChannels = (selected: any) => {
    dispatch(actions.updateFilterChannel({ channels: selected }));
  };

  const handleChangeFeatures = (selected: any) => {
    dispatch(actions.updateFilterFeature({ features: selected }));
  };

  const handleChangeCountries = (selected: any) => {
    dispatch(actions.updateFilterCountry({ countries: selected }));
  };

  const handleChangeShops = (selected: any) => {
    dispatch(actions.updateFilterShop({ shops: selected }));
  };

  const handleSubmitChange = () => {
    dispatch(actions.updateGlobalFilter());
  };

  const handleRequestExportData = React.useCallback(() => {
    setLoading(true);
    request
      .get(ONE_EP.GET_REQUEST_DASHBOARD_DOWNLOAD(), {
        shop_eids: selectedShops.map((i) => i.id),
        features: selectedFeatures.map((i) => i.id),
        from: selectedDateRange.from,
        to: selectedDateRange.to,
      })
      .then(() => {
        setLoading(false);
        toast.success(
          'Your request is currently being processed. The data will be shared with your email address once it is done',
        );
      });
  }, [
    selectedShops,
    selectedFeatures,
    selectedDateRange.from,
    selectedDateRange.to,
  ]);

  console.info('for download>>>', [
    selectedShops,
    selectedFeatures,
    selectedDateRange.from,
    selectedDateRange.to,
  ]);

  return (
    <Box display="flex" alignItems="center">
      <GlobalFilters
        hideChannel={hideChannel}
        channels={channels}
        features={features}
        countries={countries}
        shops={shops}
        selectedDateRange={selectedDateRange}
        selectedChannels={selectedChannels}
        selectedFeatures={selectedFeatures}
        selectedCountries={selectedCountries}
        selectedShops={selectedShops}
        onChangeDateRange={handleChangeDateRange}
        onChangeChannels={handleChangeChannels}
        onChangeFeatures={handleChangeFeatures}
        onChangeCountries={handleChangeCountries}
        onChangeShops={handleChangeShops}
        onSubmitChange={handleSubmitChange}
      />
      <Box flexGrow={1}></Box>
      <Box>
        <Button
          variant="text"
          style={{ textTransform: 'none' }}
          onClick={handleRequestExportData}
        >
          Export
          {loading ? (
            <CircularProgress
              size={20}
              color="inherit"
              style={{ marginLeft: '0.5em' }}
            />
          ) : (
            <DownloadIcon style={{ marginLeft: '0.5em' }} />
          )}
        </Button>
      </Box>
    </Box>
  );
}
