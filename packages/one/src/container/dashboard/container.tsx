import { ChartLine } from '@ep/one/src/components/chart-line/chart-line';
import { MetricCardGroup } from '@ep/one/src/components/metric/metric-group';
import { Box } from '@material-ui/core';
import { get } from 'lodash';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { MetricListPopup } from '../../components/metric/metric-popup';
import { DashboardState } from './reducer';
import { actions } from './redux';

export function ContainerMetricGroup() {
  const {
    metrics,
    selectedMetrics,
    filter,
    colors,
    highlightMetrics,
  } = useSelector((state: DashboardState) => {
    return {
      colors: (state.selectedCharts || []).map(
        (id) => state.colors[id],
      ),
      highlightMetrics: state.selectedCharts,
      selectedMetrics: state.selectedMetrics,
      metrics: state.metrics,
      filter: state.filter,
    };
  });
  const dispatch = useDispatch();

  React.useEffect(() => {
    if (
      !selectedMetrics &&
      !metrics &&
      get(filter, 'shops.length') &&
      get(filter, 'features.length')
    ) {
      dispatch(
        actions.init({
          from: filter.from,
          to: filter.to,
          shops: filter.shops.map((s) => s.id),
          features: filter.features.map((f) => f.id),
        }),
      );
    }
  }, [metrics, selectedMetrics, filter]);

  const [popupVisible, setPopupVisible] = React.useState(false);

  if (!metrics || !selectedMetrics) return null;

  let visibleMetrics: any[] = selectedMetrics.map((s) => {
    return metrics.find((i) => i.metricId === s);
  });

  function onRemoveMetric(metricId: string) {
    if (selectedMetrics!.length > 1) {
      dispatch(actions.removeMetric({ metricId }));
      dispatch(
        actions.saveSettings({
          selectedMetrics: selectedMetrics!.filter(
            (i) => i !== metricId,
          ),
          selectedCharts: highlightMetrics!.filter(
            (i) => i !== metricId,
          ),
        }),
      );
    }
  }

  function onHighlightMetric(metricId: string) {
    switch (true) {
      case !highlightMetrics!.includes(metricId) &&
        highlightMetrics!.length === 4:
      case highlightMetrics!.includes(metricId) &&
        highlightMetrics!.length === 1:
        break;
      default: {
        dispatch(actions.toggleHighlightMetric({ metricId }));
        dispatch(
          actions.saveSettings({
            selectedMetrics: selectedMetrics!,
            selectedCharts: highlightMetrics!.includes(metricId)
              ? highlightMetrics!.filter((i) => i != metricId)
              : highlightMetrics!.concat(metricId),
          }),
        );
      }
    }
  }

  function onSubmitMetricList(metricIds: string[]) {
    dispatch(actions.setSelectedMetric({ metricIds }));
    dispatch(
      actions.saveSettings({
        selectedMetrics: metricIds,
        selectedCharts: highlightMetrics!.filter(
          (i) => metricIds.indexOf(i) > -1,
        ),
      }),
    );
    setPopupVisible(false);
  }

  return (
    <Box>
      <MetricCardGroup
        colors={colors}
        highlightMetrics={highlightMetrics!}
        metricItems={visibleMetrics}
        onClickAdd={() => {
          setPopupVisible(true);
        }}
        onClickItem={onHighlightMetric}
        onRemove={onRemoveMetric}
        onSort={(metricIds) =>
          dispatch(
            actions.saveSettings({
              selectedMetrics: metricIds,
              selectedCharts: highlightMetrics!,
            }),
          )
        }
      />
      <MetricListPopup
        open={popupVisible}
        onClose={() => setPopupVisible(false)}
        metricItems={metrics}
        selectedList={selectedMetrics}
        onSubmit={onSubmitMetricList}
        resetList={[]}
      />
    </Box>
  );
}

export function ContainerLineCharts() {
  const dispatch = useDispatch();
  const prevRef = React.useRef<string[] | null>([]);
  const {
    selectedCharts,
    filter,
    charts,
    chartDates,
    metrics,
  } = useSelector((state: DashboardState) => {
    return {
      selectedCharts: state.selectedCharts,
      filter: state.filter,
      charts: (state.charts || []).map((c) => ({
        ...c,
        color: state.colors[c.metricId],
      })),
      chartDates: state.chartDates,
      metrics: state.metrics,
    };
  });

  let prevSeletedCharts = prevRef.current;

  React.useEffect(() => {
    if (metrics && !charts && selectedCharts) {
      dispatch(
        actions.loadCharts({
          from: filter.from,
          to: filter.to,
          metricIds: selectedCharts,
          features: filter.features.map((f) => f.id),
          shops: filter.shops.map((s) => s.id),
          currency: filter.currency,
        }),
      );
    }
  }, [selectedCharts, charts, filter, metrics]);

  React.useEffect(() => {
    let diff = selectedCharts?.filter(
      (i) => !prevSeletedCharts?.includes(i),
    );
    if (metrics && metrics.length > 0 && selectedCharts) {
      if (diff && diff.length > 0) {
        dispatch(
          actions.loadCharts({
            from: filter.from,
            to: filter.to,
            metricIds: selectedCharts,
            features: filter.features.map((f) => f.id),
            shops: filter.shops.map((s) => s.id),
            currency: filter.currency,
          }),
        );
      }
      prevRef.current = selectedCharts;
    }
  }, [selectedCharts, filter, metrics]);

  if (!charts || charts.length === 0) return null;

  return (
    <ChartLine
      series={charts}
      dates={chartDates}
      maxSeriesCount={4}
    />
  );
}
