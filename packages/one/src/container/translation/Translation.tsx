import React, { ReactElement } from 'react';
import { IntlProvider } from 'react-intl';
import messages_fr from './locales/fr.json';
import messages_en from './locales/en.json';

const messages: any = {
  en: messages_en,
  fr: messages_fr,
};

type langCode = 'en' | 'fr';

export const ProviderTranslator = ({
  children,
  lang,
}: {
  children: React.ReactNode;
  lang: langCode;
}) => (
  <IntlProvider
    locale={lang}
    defaultLocale={lang}
    messages={messages[lang]}
    textComponent={React.Fragment}
  >
    {children}
  </IntlProvider>
);
