const manageTranslations = require('react-intl-translations-manager')
  .default;

const LANGUAGE_FILE = {
  EN: `en`,
  FR: `fr`,
};

manageTranslations({
  messagesDirectory: 'src/messages/i18n/defaultMessage/src/messages',
  translationsDirectory: 'src/messages/locales/',
  // singleMessagesFile: true,
  languages: [...Object.values(LANGUAGE_FILE)], // any language you need
});
