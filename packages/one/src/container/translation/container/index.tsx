import { connect } from 'react-redux';
import { ProviderTranslator } from '../Translation';

const mapDispatchToProps = (dispatch: any) => ({});

const mapStateToProps = ({ lang }: any) => ({
  ...lang,
});

export const ProviderTranslatorContainer = connect<any, any, any>(
  mapStateToProps,
  mapDispatchToProps,
)(ProviderTranslator);
