import moment from 'moment';
import { random, range } from 'lodash';
import { setupWorker, rest } from 'msw';

export function startMock() {
  const demoData = [
    0,
    3.826743137732259,
    0,
    1.251256501027053,
    0,
    0.2748453161221461,
    2.642762284196547,
    0,
    1.7255772646536414,
    0,
    0.00001693784868149434,
    // 0.000008476970895733259,
    1000000000,
    0.8650408986248393,
    0.6187060284177128,
    0.48551278335485115,
    2.523794160348443,
    0.33565087951785355,
    0.38481070603466155,
    2.3872259886807474,
    0.7596648818095746,
    3.199784507201996,
    0.3325022579650732,
    1.7087516195482975,
    1.3645386871193323,
    3.3921896900794266,
    2.2876372984342135,
    3.5918446471313046,
    1.525000685701747,
    2.53180357389521,
    2.494733340725136,
  ];
  const availMetrics = Array(8)
    .fill(0)
    .map((i) => Math.random().toString().slice(2));

  const worker = setupWorker(
    rest.get('/mock/get-metrics', (req, res, ctx) => {
      const from = req.url.searchParams.get('from');
      const to = req.url.searchParams.get('to');
      return res(
        ctx.json({
          data: availMetrics.map((id, i) => ({
            metric_id: id,
            title: id,
            value: 600,
            percent_change: 15,
            previous_period_range: {
              from: Number(from),
              to: Number(to),
            },
          })),
        }),
      );
    }),

    rest.get('/mock/get-charts', (req, res, ctx) => {
      const from = req.url.searchParams.get('from');
      const to = req.url.searchParams.get('to');

      const metricId = req.url.searchParams.get('metric_id');

      const dates = range(0, 30).map((i) =>
        moment().subtract(i, 'day').valueOf(),
      );

      return res(
        ctx.json({
          data: {
            metric_id: metricId,
            label: metricId,
            series: demoData.map((i) => i + random(-1, 1, true)),
            dates: dates,
          },
        }),
      );
    }),

    rest.get('/mock/get-settings', (req, res, ctx) => {
      return res(
        ctx.json({
          data: {
            selected_charts: availMetrics.slice(0, 1),
            selected_metrics: availMetrics.slice(0, 5),
          },
        }),
      );
    }),

    rest.get('/mock/tokopedia/shops', (req, res, ctx) => {
      return res(
        ctx.json({
          success: true,
          data: [
            {
              shop_eid: 4162,
              shop_sid: '10258079',
              shop_name: 'EpsillionShop',
              created_by: 'Nh\u01a1n Tr\u01b0\u01a1ng',
              created_at: 1608884023,
              updated_by: '',
              updated_at: null,
              shop_state: 'Init',
              shop_allowed_pull: 1,
              shop_allowed_push: 1,
              channel_code: 'TOKOPEDIA',
              channel_name: 'TOKOPEDIA',
              country_code: 'ID',
              country_name: 'Indonesia',
              country_timezone: 'Asia/Jakarta',
              country_exchange: 'IDR',
              country_format_right: 0,
              features: [
                {
                  feature_code: 'M_TOK_PA',
                  feature_name: 'Tokopedia - Product Ads',
                },
              ],
            },
          ],
          message: '',
          error_code: '',
        }),
      );
    }),
    rest.get(
      '/mock/api/v1/toko/product-ads/product-of-shop',
      (req, res, ctx) => {
        return res(
          ctx.json(
            JSON.parse(
              '{"success":true,"data":[{"shop_eid":4162,"campaign_eid":1,"max_cpc":"450.000000","product_eid":31879403,"product_name":"Mary Decor Table Runner","product_sid":"1444813135","status":"running","child_status":{"is_promoted_product_status":1},"campaign_name":"Ads Perf","campaign_code":"CAMPTOKO600928992C02A","discount":0,"budget_config":{"value_daily":0,"value_total":0},"product_stock":10,"campaign_product_id":1,"sum_total_views":0,"sum_views_at_top":0,"sum_click":0,"sum_cost":0,"sum_item_sold":0,"sum_total_ads_item_sold":0,"sum_gmv":0,"shop_name":"EpsillionShop","currency":"IDR"},{"shop_eid":4162,"campaign_eid":2,"max_cpc":"500.000000","product_eid":31879401,"product_name":"Nordic ins net red rectangular tablecloth","product_sid":"1450420781","status":"running","child_status":{"is_promoted_product_status":1},"campaign_name":"Test 2-Q1","campaign_code":"CAMPTOKO600928992C0C3","discount":0,"budget_config":{"value_daily":0,"value_total":0},"product_stock":10,"campaign_product_id":2,"sum_total_views":0,"sum_views_at_top":0,"sum_click":0,"sum_cost":0,"sum_item_sold":0,"sum_total_ads_item_sold":0,"sum_gmv":0,"shop_name":"EpsillionShop","currency":"IDR"}],"message":"","error_code":"","pagination":{"page":1,"limit":10,"item_count":2,"page_count":1}}',
            ),
          ),
        );
      },
    ),
    rest.get(
      '/mock/api/v1/toko/product-ads/suggest-kw',
      (req, res, ctx) => {
        return res(
          ctx.json(
            JSON.parse(
              '{"success":true,"data":[{"keyword_id":1,"campaign_eid":1,"keyword_name":"vyvyvy","bidding_price":1,"keyword_status":"running","max_cpc":"1.000000","shop_eid":4162,"campaign_name":"Ads Perf","child_status":{"campaign_product_keyword_state":1},"match_type":"Broad Match","sum_total_views":0,"sum_views_at_top":0,"sum_click":0,"sum_cost":0,"sum_item_sold":0,"sum_total_ads_item_sold":0,"sum_gmv":0,"shop_name":"EpsillionShop","currency":"IDR"},{"keyword_id":2,"campaign_eid":1,"keyword_name":"table cloth","bidding_price":1,"keyword_status":"running","max_cpc":"1.000000","shop_eid":4162,"campaign_name":"Ads Perf","child_status":{"campaign_product_keyword_state":1},"match_type":"Exact Match","sum_total_views":0,"sum_views_at_top":0,"sum_click":0,"sum_cost":0,"sum_item_sold":0,"sum_total_ads_item_sold":0,"sum_gmv":0,"shop_name":"EpsillionShop","currency":"IDR"},{"keyword_id":3,"campaign_eid":1,"keyword_name":"vyvy","bidding_price":1,"keyword_status":"running","max_cpc":"1.000000","shop_eid":4162,"campaign_name":"Ads Perf","child_status":{"campaign_product_keyword_state":1},"match_type":"Broad Match","sum_total_views":0,"sum_views_at_top":0,"sum_click":0,"sum_cost":0,"sum_item_sold":0,"sum_total_ads_item_sold":0,"sum_gmv":0,"shop_name":"EpsillionShop","currency":"IDR"},{"keyword_id":5,"campaign_eid":2,"keyword_name":"vyvy","bidding_price":1,"keyword_status":"running","max_cpc":"1.000000","shop_eid":4162,"campaign_name":"Test 2-Q1","child_status":{"campaign_product_keyword_state":1},"match_type":"Broad Match","sum_total_views":0,"sum_views_at_top":0,"sum_click":0,"sum_cost":0,"sum_item_sold":0,"sum_total_ads_item_sold":0,"sum_gmv":0,"shop_name":"EpsillionShop","currency":"IDR"},{"keyword_id":6,"campaign_eid":2,"keyword_name":"lego table","bidding_price":1,"keyword_status":"running","max_cpc":"1.000000","shop_eid":4162,"campaign_name":"Test 2-Q1","child_status":{"campaign_product_keyword_state":1},"match_type":"Broad Match","sum_total_views":0,"sum_views_at_top":0,"sum_click":0,"sum_cost":0,"sum_item_sold":0,"sum_total_ads_item_sold":0,"sum_gmv":0,"shop_name":"EpsillionShop","currency":"IDR"},{"keyword_id":7,"campaign_eid":2,"keyword_name":"table mat","bidding_price":1,"keyword_status":"running","max_cpc":"1.000000","shop_eid":4162,"campaign_name":"Test 2-Q1","child_status":{"campaign_product_keyword_state":1},"match_type":"Broad Match","sum_total_views":0,"sum_views_at_top":0,"sum_click":0,"sum_cost":0,"sum_item_sold":0,"sum_total_ads_item_sold":0,"sum_gmv":0,"shop_name":"EpsillionShop","currency":"IDR"},{"keyword_id":8,"campaign_eid":2,"keyword_name":"table runner","bidding_price":1,"keyword_status":"running","max_cpc":"1.000000","shop_eid":4162,"campaign_name":"Test 2-Q1","child_status":{"campaign_product_keyword_state":1},"match_type":"Broad Match","sum_total_views":0,"sum_views_at_top":0,"sum_click":0,"sum_cost":0,"sum_item_sold":0,"sum_total_ads_item_sold":0,"sum_gmv":0,"shop_name":"EpsillionShop","currency":"IDR"},{"keyword_id":9,"campaign_eid":2,"keyword_name":"table runner imlek","bidding_price":1,"keyword_status":"running","max_cpc":"4.000000","shop_eid":4162,"campaign_name":"Test 2-Q1","child_status":{"campaign_product_keyword_state":1},"match_type":"Exact Match","sum_total_views":0,"sum_views_at_top":0,"sum_click":0,"sum_cost":0,"sum_item_sold":0,"sum_total_ads_item_sold":0,"sum_gmv":0,"shop_name":"EpsillionShop","currency":"IDR"}],"message":"","error_code":"","pagination":{"page":1,"limit":10,"item_count":8,"page_count":1}}',
            ),
          ),
        );
      },
    ),
    
  );

  return worker.start();
}
