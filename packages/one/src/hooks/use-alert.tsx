import { Snackbar } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import React from 'react';

type Notification = {
  open: boolean;
  type?: 'success' | 'error';
  message?: string;
};

export function useOneAlert() {
  const [
    notification,
    setNotification,
  ] = React.useState<Notification>({ open: false });

  const handleCloseNotification = () => {
    setNotification((state) => ({ ...state, open: false }));
  };

  const showRequestAlert = (rs) => {
    setNotification((state) => ({
      open: true,
      message: rs.message,
      type: rs.success ? 'success' : 'error',
    }));
  };

  const showAlert = (message: string, type: Notification['type']) => {
    setNotification((state) => ({
      open: true,
      message,
      type,
    }));
  };

  const alert = (
    <Snackbar
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={notification.open}
      onClose={handleCloseNotification}
      autoHideDuration={3000}
    >
      <Alert
        onClose={handleCloseNotification}
        elevation={6}
        variant="filled"
        severity={notification.type}
      >
        {notification.message}
      </Alert>
    </Snackbar>
  );

  return {
    showAlert,
    showRequestAlert,
    alert,
  };
}
