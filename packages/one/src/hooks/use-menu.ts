import { context } from 'msw/lib/types';
import React from 'react';

export const MenuContext = React.createContext<{
  customLeftPanel: any;
  setCustomLeftPanel: Function;
  resetCustomLeftPanel: Function;
}>({
  customLeftPanel: null,
  setCustomLeftPanel: () => console.warn('no provider'),
  resetCustomLeftPanel: () => console.warn('no provider'),
});

export function useOneMenu() {
  const { customLeftPanel, setCustomLeftPanel, resetCustomLeftPanel } = React.useContext(
    MenuContext,
  );

  return {
    customLeftPanel,
    setCustomLeftPanel,
    resetCustomLeftPanel
  };
}
