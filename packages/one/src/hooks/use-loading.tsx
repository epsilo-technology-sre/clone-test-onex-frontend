import React from 'react';
import * as ReactDOM from 'react-dom';
import { get } from 'lodash';
import { useSelector } from 'react-redux';
import { OneFullPageLoading } from '../components/common/Loading';

type LoadingProps = {
  exclude?: string[];
};

export function useLoading(props: LoadingProps = { exclude: [] }) {
  let isLoading = useSelector((state) => {
    let isLoading = Object.keys(state.loading || {}).some((key) => {
      if (props.exclude.indexOf(key) > -1) return false;
      return get(state, ['loading', key, 'status'], false);
    });
    return isLoading;
  });
  const ref = React.useRef();

  React.useEffect(() => {
    let el = document.getElementById('ep-global-loading');
    if (!el) {
      el = document.createElement('div');
      el.id = 'ep-global-loading';
      document.body.append(el);
    }
    ref.current = el;
  }, []);

  if (!ref.current) return null;

  return ReactDOM.createPortal(
    <OneFullPageLoading loading={isLoading} />,
    ref.current,
    'ep-global-loading',
  );
}

export { useLoading as useOneFullPageLoading };
