import * as aim from '../../aim';
import * as request from '../utils/fetch';
import React, { useEffect } from 'react';
import { ONE_EP } from '../../endpoint';

export const AuthContext = React.createContext<{
  canAccess: (codes: string[]) => boolean;
  canInternalAccess: () => boolean;
}>({
  canAccess: (codes: string[]) => true,
  canInternalAccess: () => false,
});

export function useAuth() {
  const [token, setToken] = React.useState<string | null>(
    aim.getLoginToken().token,
  );

  const [
    userSettings,
    setUserSettings,
  ] = React.useState<aim.UserSettings>({
    profile: { userEmail: '-', userName: '-' },
  });

  useEffect(() => {
    setToken(aim.getLoginToken().token);
    aim.getAuthEvent().onExpired(() => {
      console.info('expired....');
      setToken(null);
      aim.clearUserSettings();
    });

    if (aim.getLoginToken().token) {
      retrieveUserProfile().then((data) => {
        // FIXME: potential race condition, double firing api request
        setUserSettings(data);
      });
    }
  }, []);

  const logout = () => {
    console.info('user logout...');
    request
      .post(ONE_EP.USER_LOGOUT(), {})
      .then(() => {
        aim.clearUserSettings();
        aim.setLoginToken(null);
        window.location.href = '/';
      })
      .catch(() => {
        aim.clearUserSettings();
        aim.setLoginToken(null);
        window.location.href = '/';
      });
  };

  return { token, userSettings, logout };
}

async function retrieveUserProfile() {
  let userSettings = aim.getUserSettings();

  if (
    userSettings === null ||
    !userSettings.permissions ||
    !userSettings.profile
  ) {
    let profile = await request
      .get(ONE_EP.GET_USER_PROFILE())
      .then((rs) => {
        console.info('user profile', rs.data);
        let { data } = rs;
        return {
          userName: data.name,
          userEmail: data.email,
        };
      });
    let permissions = await getPermission();
    aim.setUserSettings({ profile, permissions });
    return { profile, permissions };
  } else {
    return Promise.resolve(userSettings);
  }
}

async function getPermission() {
  let rs = await request.get(ONE_EP.GET_USER_PERMISSIONS_ALL_SHOP());
  let permByShopId: {
    [key: string]: {
      [feature: string]: string[];
    };
  } = rs.data;
  let permByCode = Object.keys(permByShopId).reduce(
    (accum: { [permCode: string]: string[] }, shop: string) => {
      let featureList = Object.keys(permByShopId[shop]);
      let rs = { ...accum };
      for (let feat of featureList) {
        for (let permCode of permByShopId[shop][feat]) {
          rs[permCode] = rs[permCode]
            ? rs[permCode].concat(shop)
            : [shop];
        }

        if (!rs[feat]) {
          rs[feat] = [];
        }
        if (permByShopId[shop][feat].length > 0) {
          rs[feat] = rs[feat].concat(shop);
        }
      }

      return rs;
    },
    {},
  );

  return { byCode: permByCode, byShopId: permByShopId };
}
