const HtmlWebPackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');
const path = require('path');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
  .BundleAnalyzerPlugin;
const TerserPlugin = require('terser-webpack-plugin');

const distPath = path.resolve(__dirname, 'build');
const apps = ['one'];

const isEnvProduction =
  process.env.NODE_ENV === 'production' ? true : false;

console.info(
  'STAGING_BRANCH_RELEASE....',
  process.env.STAGING_RELEASE_BRANCH,
);

module.exports = (env, args) => ({
  devtool: 'source-map',
  entry: {
    ...apps.reduce(
      (a, k) => ({ ...a, [k]: `./packages/${k}/src/index.tsx` }),
      {},
    ),
  },
  output: {
    path: distPath,
    filename: '[name].[contenthash].js',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
    alias: {
      '@ep/lazada': path.resolve(__dirname, './packages/lazada'),
      '@ep/shopee': path.resolve(__dirname, './packages/shopee'),
      '@ep/one': path.resolve(__dirname, './packages/one'),
      '@ep/toko': path.resolve(__dirname, './packages/toko'),
      '@epi/next': path.resolve(__dirname, './packages/next'),
    },
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              insertAt: 'top',
            },
          },
          {
            loader: 'css-loader',
          },
        ],
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              insertAt: 'top',
            },
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sassOptions: {
                includePaths: [
                  'node_modules',
                  path.resolve(
                    __dirname,
                    './packages/next/src/assets',
                  ),
                ],
              },
              sourceMap: true,
            },
          },
        ],
        include: path.resolve(__dirname, '../'),
      },
      {
        test: /\.mjs$/,
        include: /node_modules/,
        type: 'javascript/auto',
      },
      {
        test: /\.(js|jsx|ts|tsx)$/,
        loader: 'babel-loader',
        options: {
          sourceType: 'unambiguous',
          presets: [
            ['@babel/preset-env', { modules: 'auto' }],
            '@babel/preset-react',
            '@babel/preset-typescript',
            'react-app',
          ],
          plugins: [
            '@babel/plugin-transform-modules-commonjs',
            [
              require.resolve('babel-plugin-named-asset-import'),
              {
                loaderMap: {
                  svg: {
                    ReactComponent:
                      '@svgr/webpack?-svgo,+titleProp,+ref![path]',
                  },
                },
              },
            ],
          ],
          // This is a feature of `babel-loader` for webpack (not Babel itself).
          // It enables caching results in ./node_modules/.cache/babel-loader/
          // directory for faster rebuilds.
          cacheDirectory: true,
          // See #6846 for context on why cacheCompression is disabled
          cacheCompression: false,
          compact: isEnvProduction,
        },
      },
      {
        test: /\.svg$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name].[contenthash].[ext]',
              outputPath: 'static',
              publicPath: '/static',
            },
          },
        ],
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name].[contenthash].[ext]',
              outputPath: 'static',
              publicPath: '/static',
            },
          },
        ],
      },
    ],
  },

  plugins: [
    ...apps.map(
      (proj) =>
        new HtmlWebPackPlugin({
          template: './public/index-template.html',
          filename: `./index.html`,
          chunks: ['vendors', proj],
        }),
    ),
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[name][id].css',
    }),
    new webpack.DefinePlugin({
      DEBUG: args.mode == 'development',
      PUBLIC_URL: JSON.stringify(
        args.mode == 'production'
          ? 'ABSOLUTE_PATH_HERE_IF_NEEDED'
          : '',
      ),
      'process.env.API_URL': JSON.stringify(process.env.API_URL),
      'process.env.API_PASSPORT_URL': JSON.stringify(
        process.env.API_PASSPORT_URL,
      ),
      'process.env.API_INSIGHT_URL': JSON.stringify(
        process.env.API_INSIGHT_URL,
      ),
      'process.env.STAGING_RELEASE_BRANCH': JSON.stringify(
        process.env.STAGING_RELEASE_BRANCH,
      ),
    }),
    new CopyWebpackPlugin({
      patterns: [
        {
          context: __dirname,
          from: 'public',
          to: path.join(distPath),
        },
      ],
    }),
    new BundleAnalyzerPlugin({
      analyzerMode: 'static',
      defaultSizes: 'gzip',
    }),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
  ],
  optimization: {
    splitChunks: {
      maxSize: 500 * 1000,
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all',
        },
      },
    },
    minimizer: [
      new TerserPlugin({
        sourceMap: true,
        terserOptions: {
          sourceMap: true, // Must be set to true if using source-maps in production
          compress: {
            // drop_console: true,
          },
        },
      }),
    ],
  },
});
