module.exports = {
  verbose: true,
  setupFilesAfterEnv: ['jest-extended'],
  globals: {
    'ts-jest': {
      isolatedModules: true,
    },
  },
  testPathIgnorePatterns: [
    '/node_modules/',
    '/packages/one/',
    '/packages/lazada/',
    '/packages/shopee/',
    '/packages/toko/',
  ],
  testMatch: [
    '**/__tests__/**/*.test.[jt]s?(x)',
    '**/?(*.)+(spec|test).[jt]s?(x)',
  ],
  transform: {
    '^.+\\.(ts|tsx)$': 'ts-jest',
  },
  moduleNameMapper: {
    '@ep/one/(.*)$': '<rootDir>/packages/one/$1',
  },
};
